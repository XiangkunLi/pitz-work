classdef Generator < Module
    % Generator a special Module, whose `name` is 'Input'
    % By default, don't pause at the end of the run
    properties
        obj.name = 'Input';
        obj.kv = containers.Map();
        obj.kv('Lprompt') = false;
    end
    
    methods
        function obj = Generator(varargin)
            % No need to set `name` here
            if nargin > 0
                obj.kv = containers.Map();
                
                % Get the key-value pairs from `varargin`
                nargs = length(varargin);
                keys = varargin(1:2:nargs);
                values = varargin(2:2:nargs);
                % Set the key-value pairs to `kv`
                for i = 1:length(keys)
                    obj.kv(keys{i}) = values{i};
                end
                % Generating the output string
                obj.build();
            end
        end
    end
end


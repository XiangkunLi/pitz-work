function [ QuadPos, QuadGrad ] = read_sco( z0, fbeamline, foptimized )
% Read beam line and optimized quadrupole gradients into arrays
% Parameters
%   z0        : starting position of the SCO elements in PITZ beamline
%   fbeamline : beamline file for SCO optimizaiton
%   foptimized: file that stores optimized quadrupoles gradients
% Returns
%   QuadPos : the positions of the quadrupoles
%   QaudGrad: the gradients of the quadrupoles

    % Ratio of the gradient used in astra to that in SCO
    astra_to_sco = 1.590444459638447;
    
    % Prepare input files for the beam line and optimized quadrupole
    % gradients
    Lret = false;
    if nargin > 0
        z0 = z0;
        if nargin > 1
            fbeamline = fbeamline;
            if exist(fbeamline, 'file')
            % Read the beamline file into a table
                bl = readtable(fbeamline);
            else
            % If beamline file doesn't exist, then return
                fprintf('NB: file %s does not exists!\n', fbeamline);
                QuadPos = [];
                QuadGrad = [];
                Lret = true;
            end
            % If optimized result exists, use it; if not, then use the
            % initial quad gradients in the beamline file later
            Lopt = false;
            if nargin > 2
                foptimized = foptimized;
                if exist(foptimized, 'file')
                % Read the optimized file into a table if existing
                    Lopt = true;
                    opt = readtable(foptimized);
                    grad_opt = tail(opt,1);
                else
                    fprintf('NB: file %s does not exists!\n', foptimized);
                end
            end
        else % No beamline filename is given, return too
            QuadPos = [];
            QuadGrad = [];
            Lret = true;
        end
    else
        QuadPos = [];
        QuadGrad = [];
        Lret = true;
    end
    
    if Lret
        disp('NB: No input files are given or files do not exist!\n');
        return;
    end
    j = 1;
    Pos = [];
    Grad = [];
    for i = 1:size(bl,1)
        % disp(i);
        if contains(bl{i,1}, "O")
            z0 = z0+bl{i,2};
        elseif contains(bl{i,1}, "Q")
            z0 = z0+bl{i,2}/2.0;
            if bl{i,6} == 1 && Lopt
                gr = grad_opt{1, j}*astra_to_sco;
                j = j+1;
            else
                gr = bl{i,3}*astra_to_sco;
            end

            Pos = [Pos z0];
            Grad = [Grad gr];

            z0 = z0+bl{i,2}/2.0;
        end
    end
    QuadPos = Pos;
    QuadGrad = Grad;
end


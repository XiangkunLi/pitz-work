function r = test_astra(varargin)
    % Define the parameter list for each module here
    newrun_list = upper({'Head', 'RUN', 'LOOP', 'NLoop', 'Distribution', ...
        'ion_mass', 'N_red', 'Xoff', 'Yoff', 'xp', 'yp', 'Zoff', ...
        'Toff', 'Xrms', 'Yrms', 'Zrms', 'Trms', 'Tau', 'cor_py', ...
        'cor_py', 'Qbunch', 'SRT_Q_Schottky', 'Q_Schottky', 'debunch', ...
        'Track_All', 'Track_On_Axis', 'Auto_Phase', 'Phase_Scan', ...
        'check_ref_part', 'L_rm_back', 'Z_min', 'Z_Cathode', ...
        'H_max', 'H_min', 'Max_step', 'Lmonitor', 'Lprompt'});
    output_list = upper({'ZSTART', 'ZSTOP', 'Zemit', 'Zphase', 'Screen', ...
        'Scr_xrot', 'Scr_yrot', 'Step_width', 'Step_max', ...
        'Lproject_emit', 'Local_emit', 'Lmagnetized', 'Lsub_rot', ...
        'Lsub_Larmor', 'Rot_ang', 'Lsub_cor', 'RefS', 'EmitS', ...
        'C_EmitS', 'C99_EmitS', 'Tr_EmitS', 'Sub_EmitS', 'Cross_start', ...
        'Cross_end', 'PhaseS', 'T_PhaseS', 'High_res', 'Binary', ...
        'TrackS', 'TcheckS', 'SigmaS', 'CathodeS', 'LandFS', 'LarmorS'});
    charge_list = upper({'LOOP', 'LSPCH', 'LSPCH3D', 'L2D_3D', 'Lmirror', ...
        'L_Curved_Cathode', 'Cathode_Contour', 'R_zero', 'Nrad', ...
        'Cell_var', 'Nlong_in', 'N_min', 'min_grid', 'Merge_1', ...
        'Merge_2', 'Merge_3', 'Merge_4', 'Merge_5', 'Merge_6', ...
        'Merge_7', 'Merge_8', 'Merge_9', 'Merge_10', 'z_trans', ...
        'min_grid_trans', 'Nxf', 'Nx0', 'Nyf', 'Ny0', 'Nzf', 'Nz0', ...
        'Smooth_x', 'Smooth_y', 'Smooth_z', 'Max_scale', 'Max_count', ...
        'Exp_control'});
    aperture_list = upper({'LOOP', 'LApert', 'File_Aperture', 'Ap_Z1', ...
        'Ap_Z2', 'Ap_R', 'Ap_GR', 'A_pos', 'A_xoff', 'A_yoff', ...
        'A_xrot', 'A_yrot', 'A_zrot', 'SE_d0', 'SE_Epm', 'SE_fs', ...
        'SE_Tau', 'SE_Esc', 'SE_ff1', 'SE_ff2', 'Max_Secondary' ...
        'LClean_Stack'});
    cavity_list = upper({'LOOP', 'LEfield', 'File_Efield', 'C_noscale', ...
        'C_smooth', 'Com_grid', 'C_higher_order', 'Nue', 'K_wave', ...
        'MaxE', 'Ex_stat', 'Ey_stat', 'Bx_stat', 'By_stat', 'Bz_stat', ...
        'Flatness', 'Phi', 'C_pos', 'C_numb', 'T_dependence', 'T_null', ...
        'C_Tau', 'E_stored', 'C_xoff', 'C_yoff', 'C_xrot', 'C_yrot', ...
        'C_zrot', 'C_zkickmin', 'C_zkickmax', 'C_Xkick', 'C_Ykick', ...
        'File_A0', 'P_Z1', 'P_R1', 'P_Z2', 'P_R2', 'P_n', 'E_a0', ...
        'E_Z0', 'E_sig', 'E_sigz', 'E_Zr', 'E_Eps', 'E_lam', 'zeta'});
    solenoid_list = upper({'LOOP', 'LBfield', 'File_Bfield', 'S_noscale', ...
        'S_smooth', 'S_higher_order', 'MaxB', 'S_pos', 'S_xoff', ...
        'S_yoff', 'S_xrot', 'S_yrot'});
    quadrupole_list = upper({'LOOP', 'Lquad', 'Q_type', 'Q_grad', 'Q_K', ...
        'Q_noscale', 'Q_length', 'Q_smooth', 'Q_bore', 'Q_dist', ...
        'Q_mult_a', 'Q_mult_b', 'Q_pos', 'Q_xoff', 'Q_yoff', 'Q_zoff', ...
        'Q_xrot', 'Q_yrot', 'Q_zrot'});
    input_list = upper({'Fname', 'Add', 'N_add', 'Ipart', 'Species', ...
        'ion_mass', 'Probe', 'Passive', 'Noise_reduc', 'Cathode', ...
        'R_Cathode', 'High_res', 'Binary', 'Q_total', 'Type', 'Rad', ...
        'Tau', 'Ref_zpos', 'Ref_clock', 'Ref_Ekin', 'Dist_z', 'sig_z', ...
        'C_sig_z', 'Lz', 'rz', 'sig_clock', 'C_sig_clcok', 'Lt', 'rt', ...
        'Dist_pz', 'sig_Ekin', 'C_sig_Ekin', 'LE', 'rE', 'emit_z', ...
        'cor_Ekin', 'E_photon', 'phi_eff', 'Dist_x', 'sig_x', ...
        'C_sig_x', 'Lx', 'rx', 'x_off', 'Disp_x', 'Dist_px', 'Nemit_x', ...
        'sig_px', 'C_sig_px', 'Lpx', 'rpx', 'cor_px', 'Dist_y', 'sig_y', ...
        'C_sig_y', 'Ly', 'ry', 'y_off', 'Disp_y', 'Dist_py', 'Nemit_y', ...
        'sig_py', 'C_sig_py', 'Lpy', 'rpy', 'cor_py'});
    
    
    True = true;
    False = false;

    % Get the key-value pairs from `varargin`
    nargs = length(varargin);
    keys = upper(varargin(1:2:nargs)); % upper case
    values = varargin(2:2:nargs);
    % Set the key-value pairs to `kv` for further use
    kv = containers.Map();
    for i = 1:length(keys)
        kv(keys{i}) = values{i};
    end

    % If another beamline excel is given then use it as input for
    % positions of Quadrupoles and Screens
    if isKey(kv, 'beamline')
        filename = kv('beamline');
    else
        filename = 'beamline.xlsx';
    [QuadPos, ScrPos] = beamline(filename);

    % Default inputs for Generator and Astra, more modules could be
    % added to Astra and more input parameters could be added to each 
    % module
    input = Module('Input', 'FNAME', 'beam.ini', 'IPart', 2000, ...
        'Species', 'electrons', 'Q_total', -4, ...
        'Ref_Ekin', 0.0e-6, 'LE', 0.55e-3, 'dist_pz', 'i', ...
        'Dist_z', 'p', 'Lt', 21.5e-3, 'rt', 2e-3, 'Cathode', True,...
        'Dist_x', 'r', 'sig_x', 1.0, 'Dist_px', 'g', 'Nemit_x', 0, ...
        'Dist_y', 'r', 'sig_y', 1.0, 'Dist_py', 'g', 'Nemit_y', 0);

    newrun = Module('Newrun', 'Run', 1, 'Head', 'PITZ beamline', ...
        'Distribution', 'beam.ini', 'Auto_Phase', true, ...
        'Track_All', true, 'check_ref_part', false, 'Lprompt', false, ...
        'Max_step', 200000);
    newrun.reset('Run', 2);

    charge = Module('Charge', 'LSPCH', False, 'Lmirror', False, ...
        'Nrad', 50, 'Nlong_in', 50, 'N_min', 10, 'Max_scale', 0.05, ...
        'Max_count', 20);

    cavity = Module('Cavity', 'LEfield', True, ...
        'File_Efield', {'gun45cavity.txt', 'CDS14_15mm.txt'}, ...
        'MaxE', {60, 12}, 'C_pos', {0., 2.675}, 'Nue', {1.3, 1.3}, ...
        'Phi', {0, 0});

    solenoid = Module('Solenoid', 'LBfield', True, ...
        'File_Bfield', {'gunsolenoidsPITZ.txt'}, 'MaxB', {0.2}, 'S_pos', {0});

    output = Module('Output', 'Zstart', 0, 'Zstop', 25.0, 'Zemit', 2500, ...
        'Zphase', 1, 'RefS', True, 'EmitS', True, 'PhaseS', True, ...
        'TrackS', False, 'LandFS', True, 'Screen', num2cell(ScrPos));

    aperture = Module('Aperture', 'LApert', True, 'File_Aperture', {'app.txt'});

    QuadGrad = zeros(size(QuadPos));
    QuadType = cell(size(QuadPos));
    QuadType(:) = {'Q3.dat'};
    quadrupole = Module('Quadrupole', 'Lquad', True, 'Q_pos', num2cell(QuadPos), ...
        'Q_grad', num2cell(QuadGrad), 'Q_type', QuadType);

    % If other parameters or other values for existing parameters are
    % given then add the parameters or update the values
    keys = kv.keys;
    values = kv.values;
    for i = 1:length(keys)
        if any(strcmp(input_list, keys{i}))
            input.reset(keys{i}, values{i});
        elseif any(strcmp(newrun_list, keys{i}))
            newrun.reset(keys{i}, values{i});
        elseif any(strcmp(output_list, keys{i}))
            output.reset(keys{i}, values{i});
        elseif any(strcmp(charge_list, keys{i}))
            charge.reset(keys{i}, values{i});
        elseif any(strcmp(cavity_list, keys{i}))
            cavity.reset(keys{i}, values{i});
        elseif any(strcmp(solenoid_list, keys{i}))
            solenoid.reset(keys{i}, values{i});
        elseif any(strcmp(quadrupole_list, keys{i}))
            quadrupole.reset(keys{i}, values{i});
        elseif any(strcmp(aperture_list, keys{i}))
            aperture.reset(keys{i}, values{i});
        else
            fprintf('Note: keyword %s is not updated!\n', keys{i})
        end
    end
    
    % After adding new parameters and updating values for existing
    % parameters, generate input files for Generator and Astra
    gen = Astra();
    gen.add_module(input);
    astra = Astra();
    astra.add_modules({newrun, charge, cavity, solenoid, output, ...
        aperture, quadrupole});

    gen.write('gen.in');
    astra.write('ast.in');
end

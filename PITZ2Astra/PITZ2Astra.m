function varargout = PITZ2Astra(varargin)
% PITZ2ASTRA MATLAB code for PITZ2Astra.fig
%      PITZ2ASTRA, by itself, creates a new PITZ2ASTRA or raises the existing
%      singleton*.
%
%      H = PITZ2ASTRA returns the handle to a new PITZ2ASTRA or the handle to
%      the existing singleton*.
%
%      PITZ2ASTRA('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PITZ2ASTRA.M with the given input arguments.
%
%      PITZ2ASTRA('Property','Value',...) creates a new PITZ2ASTRA or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before PITZ2Astra_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to PITZ2Astra_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help PITZ2Astra

% Last Modified by GUIDE v2.5 29-Oct-2018 12:28:56

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @PITZ2Astra_OpeningFcn, ...
                   'gui_OutputFcn',  @PITZ2Astra_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before PITZ2Astra is made visible.
function PITZ2Astra_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to PITZ2Astra (see VARARGIN)

% Create the key-value map for further use
handles.kv = containers.Map();

% handles.gen_name = 'gen.in';
% handles.ast_name = 'ast.in';

handles.field_maps = '.\';

handles.pbeamline = '';
handles.z0 = 5.277;
handles.sbeamline = '';
handles.soptimized = '';

set(handles.EventsList, 'String', 'List of Events:');

% Choose default command line output for PITZ2Astra
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes PITZ2Astra wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = PITZ2Astra_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in Generate_Pushbutton.
function Generate_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to Generate_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
gen_name = get(handles.gen_name, 'String');
ast_name = get(handles.ast_name, 'String');

keys = handles.kv.keys;
vals = handles.kv.values;

nkeys = length(keys);
vargs = {};
for i = 1:nkeys
    tmp = {keys{i} vals{i}};
    vargs = {vargs{:}, tmp{:}};
end
gen_astra(gen_name, ast_name, ...
    'FIELD_MAPS', handles.field_maps, ...
    'PITZ.BEAMLINE', handles.pbeamline, ...
    'SCO.Z0', handles.z0, ...
    'SCO.BEAMLINE', handles.sbeamline, ...
    'SCO.OPTIMIZED', handles.soptimized, ...
    vargs{:});
old_str = get(handles.EventsList, 'String');
new_str = ['Generating output successfully!'];
set(handles.EventsList, 'String', strvcat(old_str, new_str));

% --- Executes on button press in LoadPITZ_Pushbutton.
function LoadPITZ_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to LoadPITZ_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[file,path] = uigetfile('*.xlsx');
old_str = get(handles.EventsList, 'String');
if isequal(file, 0)
    new_str = 'User selected Cancel';
else
    new_str = ['User selected: ', fullfile(path,file)];
end
set(handles.EventsList, 'String', strvcat(old_str, new_str));
handles.pbeamline = fullfile(path,file);
guidata(hObject, handles);

% --- Executes on button press in LoadSCO_Pushbutton.
function LoadSCO_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to LoadSCO_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[file,path] = uigetfile('.txt');
old_str = get(handles.EventsList, 'String');
if isequal(file, 0)
    new_str = 'User selected Cancel';
else
    new_str = ['User selected: ', fullfile(path,file)];
end
set(handles.EventsList, 'String', strvcat(old_str, new_str));
handles.sbeamline = fullfile(path,file);
guidata(hObject, handles);

% --- Executes on button press in LoadOPT_Pushbutton.
function LoadOPT_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to LoadOPT_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[file,path] = uigetfile('.dat');
old_str = get(handles.EventsList, 'String');
if isequal(file, 0)
    new_str = 'User selected Cancel';
else
    new_str = ['User selected: ', fullfile(path,file)];
end
set(handles.EventsList, 'String', strvcat(old_str, new_str));
handles.soptimized = fullfile(path,file);
guidata(hObject, handles);

function EventList_Callback(hObject, eventdata, handles)
% hObject    handle to EventList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EventList as text
%        str2double(get(hObject,'String')) returns contents of EventList as a double


% --- Executes during object creation, after setting all properties.
function EventList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EventList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in EventsList.
function EventsList_Callback(hObject, eventdata, handles)
% hObject    handle to EventsList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns EventsList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from EventsList


% --- Executes during object creation, after setting all properties.
function EventsList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EventsList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in FieldMaps_Pushbutton.
function FieldMaps_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to FieldMaps_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
folder = uigetdir();

old_str = get(handles.EventsList, 'String');
if isequal(folder, 0)
    new_str = 'User selected Cancel';
else
    handles.field_maps = folder;
    new_str = ['User selected: ', folder];
end
set(handles.EventsList, 'String', strvcat(old_str, new_str));
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function EditZ0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditZ0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function EditZ0_Callback(hObject, eventdata, handles)
% hObject    handle to EditZ0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EditZ0 as text
%        str2double(get(hObject,'String')) returns contents of EditZ0 as a double

a = get(handles.EditZ0, 'String');
handles.z0 = str2num(a);

old_str = get(handles.EventsList, 'String');
new_str = ['Set SCO.Z0 to: ', a];

set(handles.EventsList, 'String', strvcat(old_str, new_str));
set(handles.EditZ0, 'String', a);
guidata(hObject, handles);



function gen_name_Callback(hObject, eventdata, handles)
% hObject    handle to gen_name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of gen_name as text
%        str2double(get(hObject,'String')) returns contents of gen_name as a double
a = get(handles.gen_name, 'String');

old_str = get(handles.EventsList, 'String');
new_str = ['Set name to: ', a];

set(handles.EventsList, 'String', strvcat(old_str, new_str));
set(handles.gen_name, 'String', a);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function gen_name_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gen_name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ast_name_Callback(hObject, eventdata, handles)
% hObject    handle to ast_name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ast_name as text
%        str2double(get(hObject,'String')) returns contents of ast_name as a double
a = get(handles.ast_name, 'String');

old_str = get(handles.EventsList, 'String');
new_str = ['Set name to: ', a];

set(handles.EventsList, 'String', strvcat(old_str, new_str));
set(handles.ast_name, 'String', a);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function ast_name_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ast_name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ChoosePath_Pushbutton.
function ChoosePath_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to ChoosePath_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
folder = uigetdir();

old_str = get(handles.EventsList, 'String');
if isequal(folder, 0)
    new_str = 'User selected Cancel';
else
    new_str = ['User selected: ', folder];
end
set(handles.EventsList, 'String', strvcat(old_str, new_str));
%set(handles.Write_path, 'String', folder);
gen_name = get(handles.gen_name, 'String');
set(handles.gen_name, 'String', fullfile(folder, gen_name));
ast_name = get(handles.ast_name, 'String');
set(handles.ast_name, 'String', fullfile(folder, ast_name));
disp(fullfile(folder, ast_name));
guidata(hObject, handles);


% --- Executes when uipanel1 is resized.
function uipanel1_SizeChangedFcn(hObject, eventdata, handles)
% hObject    handle to uipanel1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in ParasList.
function ParasList_Callback(hObject, eventdata, handles)
% hObject    handle to ParasList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ParasList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ParasList


% --- Executes during object creation, after setting all properties.
function ParasList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ParasList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ChooseModule_Popup.
function ChooseModule_Popup_Callback(hObject, eventdata, handles)
% hObject    handle to ChooseModule_Popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ChooseModule_Popup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ChooseModule_Popup

input_list = sort({'Fname', 'Add', 'N_add', 'Ipart', 'Species', ...
    'ion_mass', 'Probe', 'Passive', 'Noise_reduc', 'Cathode', ...
    'R_Cathode', 'High_res', 'Binary', 'Q_total', 'Type', 'Rad', ...
    'Tau', 'Ref_zpos', 'Ref_clock', 'Ref_Ekin', 'Dist_z', 'sig_z', ...
    'C_sig_z', 'Lz', 'rz', 'sig_clock', 'C_sig_clcok', 'Lt', 'rt', ...
    'Dist_pz', 'sig_Ekin', 'C_sig_Ekin', 'LE', 'rE', 'emit_z', ...
    'cor_Ekin', 'E_photon', 'phi_eff', 'Dist_x', 'sig_x', ...
    'C_sig_x', 'Lx', 'rx', 'x_off', 'Disp_x', 'Dist_px', 'Nemit_x', ...
    'sig_px', 'C_sig_px', 'Lpx', 'rpx', 'cor_px', 'Dist_y', 'sig_y', ...
    'C_sig_y', 'Ly', 'ry', 'y_off', 'Disp_y', 'Dist_py', 'Nemit_y', ...
    'sig_py', 'C_sig_py', 'Lpy', 'rpy', 'cor_py'});
newrun_list = sort({'LOOP', 'Head', 'RUN', 'Distribution', ...
    'ion_mass()', 'N_red', 'Xoff', 'Yoff', 'xp', 'yp', 'Zoff', ...
    'Toff', 'Xrms', 'Yrms', 'Zrms', 'Trms', 'Tau', 'cor_py', ...
    'cor_py', 'Qbunch', 'SRT_Q_Schottky', 'Q_Schottky', 'debunch', ...
    'Track_All', 'Track_On_Axis', 'Auto_Phase', 'Phase_Scan', ...
    'check_ref_part', 'L_rm_back', 'Z_min', 'Z_Cathode', ...
    'H_max', 'H_min', 'Max_step', 'Lmonitor', 'Lprompt'});
output_list = sort({'ZSTART', 'ZSTOP', 'Zemit', 'Zphase', 'Screen()', ...
    'Scr_xrot()', 'Scr_yrot()', 'Step_width', 'Step_max', ...
    'Lproject_emit', 'Local_emit', 'Lmagnetized', 'Lsub_rot', ...
    'Lsub_Larmor', 'Rot_ang', 'Lsub_cor', 'RefS', 'EmitS', ...
    'C_EmitS', 'C99_EmitS', 'Tr_EmitS', 'Sub_EmitS', 'Cross_start', ...
    'Cross_end', 'PhaseS', 'T_PhaseS', 'High_res', 'Binary', ...
    'TrackS', 'TcheckS', 'SigmaS', 'CathodeS', 'LandFS', 'LarmorS'});
charge_list = sort({'LOOP', 'LSPCH', 'LSPCH3D', 'L2D_3D', 'Lmirror', ...
    'L_Curved_Cathode', 'Cathode_Contour', 'R_zero', 'Nrad', ...
    'Cell_var', 'Nlong_in', 'N_min', 'min_grid', 'Merge_1', ...
    'Merge_2', 'Merge_3', 'Merge_4', 'Merge_5', 'Merge_6', ...
    'Merge_7', 'Merge_8', 'Merge_9', 'Merge_10', 'z_trans', ...
    'min_grid_trans', 'Nxf', 'Nx0', 'Nyf', 'Ny0', 'Nzf', 'Nz0', ...
    'Smooth_x', 'Smooth_y', 'Smooth_z', 'Max_scale', 'Max_count', ...
    'Exp_control'});
aperture_list = sort({'LOOP', 'LApert', 'File_Aperture()', 'Ap_Z1()', ...
    'Ap_Z2()', 'Ap_R()', 'Ap_GR', 'A_pos()', 'A_xoff()', 'A_yoff()', ...
    'A_xrot()', 'A_yrot()', 'A_zrot()', 'SE_d0()', 'SE_Epm()', ...
    'SE_fs()', 'SE_Tau()', 'SE_Esc()', 'SE_ff1()', 'SE_ff2()', ...
    'Max_Secondary', 'LClean_Stack'});
cavity_list = sort({'LOOP', 'LEfield', 'File_Efield()', 'C_noscale()', ...
    'C_smooth()', 'Com_grid()', 'C_higher_order()', 'Nue()', ...
    'K_wave()', 'MaxE()', 'Ex_stat()', 'Ey_stat()', 'Bx_stat()', ...
    'By_stat()', 'Bz_stat()', 'Flatness()', 'Phi()', 'C_pos()', ...
    'C_numb()', 'T_dependence()', 'T_null()', 'C_Tau()', 'E_stored()', ...
    'C_xoff()', 'C_yoff()', 'C_xrot()', 'C_yrot()', 'C_zrot()', ...
    'C_zkickmin()', 'C_zkickmax()', 'C_Xkick()', 'C_Ykick()', ...
    'File_A0()', 'P_Z1()', 'P_R1()', 'P_Z2()', 'P_R2', 'P_n', 'E_a0', ...
    'E_Z0', 'E_sig', 'E_sigz', 'E_Zr', 'E_Eps', 'E_lam', 'zeta'});
solenoid_list = sort({'LOOP', 'LBfield', 'File_Bfield()', 'S_noscale()', ...
    'S_smooth()', 'S_higher_order()', 'MaxB()', 'S_pos()', 'S_xoff()', ...
    'S_yoff()', 'S_xrot()', 'S_yrot()'});
quadrupole_list = sort({'LOOP', 'Lquad', 'Q_type()', 'Q_grad()', ...
    'Q_noscale()', 'Q_length()', 'Q_smooth()', 'Q_bore()', 'Q_dist()', ...
    'Q_mult_a', 'Q_mult_b', 'Q_pos()', 'Q_xoff()', 'Q_yoff()', ...
    'Q_zoff()', 'Q_xrot()', 'Q_yrot()', 'Q_zrot()', 'Q_K()'});

str = get(hObject, 'String');
val = get(hObject, 'Value');

switch str{val}
    case 'Input'
        set(handles.ChoosePara_Popup, 'String', input_list);
    case 'Newrun'
        set(handles.ChoosePara_Popup, 'String', newrun_list);
    case 'Charge'
        set(handles.ChoosePara_Popup, 'String', charge_list);
    case 'Cavity'
        set(handles.ChoosePara_Popup, 'String', cavity_list);
    case 'Solenoid'
        set(handles.ChoosePara_Popup, 'String', solenoid_list);
    case 'Quadrupole'
        set(handles.ChoosePara_Popup, 'String', quadrupole_list);
    case 'Output'
        set(handles.ChoosePara_Popup, 'String', output_list);
    case 'Aperture'
        set(handles.ChoosePara_Popup, 'String', aperture_list);
end
set(handles.ValueEdit, 'String', '');



% --- Executes during object creation, after setting all properties.
function ChooseModule_Popup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ChooseModule_Popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ChoosePara_Popup.
function ChoosePara_Popup_Callback(hObject, eventdata, handles)
% hObject    handle to ChoosePara_Popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ChoosePara_Popup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ChoosePara_Popup
set(handles.ValueEdit, 'String', '');

% --- Executes during object creation, after setting all properties.
function ChoosePara_Popup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ChoosePara_Popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on key press with focus on ChooseModule_Popup and none of its controls.
function ChooseModule_Popup_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to ChooseModule_Popup (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)



function ValueEdit_Callback(hObject, eventdata, handles)
% hObject    handle to ValueEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ValueEdit as text
%        str2double(get(hObject,'String')) returns contents of ValueEdit as a double


% --- Executes during object creation, after setting all properties.
function ValueEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ValueEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in AddPara_Pushbutton.
function AddPara_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to AddPara_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
old_str = get(handles.ParasList, 'String');

str = get(handles.ChoosePara_Popup, 'String');
val = get(handles.ChoosePara_Popup, 'Value');
key = str{val};


value = get(handles.ValueEdit, 'String')
if isempty(value)
    disp('Please enter Value before clicking Add button!');
else
    %new_str = strcat(key, ' = ', value);
    %set(handles.ParasList, 'String', strvcat(old_str, new_str));

    out = regexp(key, '[()]'); 
    if isempty(out) % The parameter is not a list
        if length(str2num(value))
            value = str2num(value);
            new_str = sprintf('%s = %f', key, value);
        else
            new_str = sprintf('%s = %s', key, value);
        end
    else            % If a list is expected for the parameter
        % Convert from nx1 array to 1xn cell
        dim = size(value);
        value = transpose(mat2cell(value, ones(dim(1),1)))
        
        new_str = sprintf('%s = (', key)
        if length(str2num(value{1}))
            for i = 1:length(value)
                value{i} = str2num(value{i});
                new_str = sprintf('%s%f, ', new_str, value{i})
            end
        else
            for i = 1:length(value)
                value{i} = strtrim(value{i});
                new_str = sprintf('%s%s, ', new_str, value{i})
            end
        end
        new_str = sprintf('%s)', new_str)
        key = key(1:length(key)-2);
    end
    handles.kv(upper(key)) = value;
    set(handles.ParasList, 'String', strvcat(old_str, new_str));
end
guidata(hObject, handles);


% --- Executes on button press in DeletePara_Pushbutton.
function DeletePara_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to DeletePara_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = get(handles.ParasList, 'Value');
str = get(handles.ParasList, 'String');

if isempty(str)
    disp('The list is already empty!');
else
    tmp = strsplit(str(value,:));   

    if isKey(handles.kv, upper(tmp{1}))
        remove(handles.kv, upper(tmp{1}));
    end
    disp(handles.kv.keys);
    disp(handles.kv.values);

    str(value,:) = [];
    set(handles.ParasList, 'String', str);
end
guidata(hObject, handles);



function edit9_Callback(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit9 as text
%        str2double(get(hObject,'String')) returns contents of edit9 as a double


% --- Executes during object creation, after setting all properties.
function edit9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Help_Pushbutton.
function Help_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to Help_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
winopen('PITZ2Astra.pdf');

function ImainEdit_Callback(hObject, eventdata, handles)
% hObject    handle to ImainEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ImainEdit as text
%        str2double(get(hObject,'String')) returns contents of ImainEdit as a double


% --- Executes during object creation, after setting all properties.
function ImainEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ImainEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Imain2MaxB_Pushbutton.
function Imain2MaxB_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to Imain2MaxB_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
var = get(handles.ImainEdit, 'String');
Imain = str2num(var);
if ~isempty(Imain)
    MaxB = -(0.0000372+0.000588*Imain);
    set(handles.ValueEdit, 'String', num2str(MaxB));
else
    disp('Please input the current first.');
end
guidata(hObject, handles);


% --------------------------------------------------------------------
function Help_Menu_Callback(hObject, eventdata, handles)
% hObject    handle to Help_Menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in ViewGen_Pushbutton.
function ViewGen_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to ViewGen_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
filename = get(handles.gen_name, 'String');
winopen(filename);

% --- Executes on button press in ViewAst_Pushbutton.
function ViewAst_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to ViewAst_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
filename = get(handles.ast_name, 'String');
winopen(filename);

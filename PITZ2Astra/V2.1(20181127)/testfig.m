function testfig
clear
clc

hFig = figure;
handles.CB1 = uicontrol('style','checkbox','units','pixels',...
    'position',[40,12,200,30],'string','try1','fontsize',16,...
    'callback',{@CB_1_Callback});

guidata(hFig,handles);

%// Don't consider the arguments. 
    function CB_1_Callback(~,~)

        if get(handles.CB1,'Value')==1
            disp('Value=1');
        elseif get(handles.CB1,'Value')==0
            disp('Value=0');
        end
    end
end
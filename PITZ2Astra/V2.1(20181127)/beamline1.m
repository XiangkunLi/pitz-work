function [ output_args ] = beamline1( input_args )

zend = 20;
zmid = zend/2;
ax_to_fig = zend/(zend+2.0);
ratio = 1.5;

f = figure;
axes = gca;
set(axes, 'units', 'centimeter', 'YTick', []);
axis([0 zend 0 5*0.75]);
xticks([0:1:zend]);
xlabel('z (m)');
axes.YAxis.Visible = 'off';
axes.XMinorTick = 'on';

check_list2_Callback(f);


function check_list2_Callback(f)

cla;

zend = 20;
zmid = zend/2;
ax_to_fig = zend/(zend+2.0);
ratio = 1.5;

gca = axes2;


element_list = upper({'Quad', 'Scr'});
color_list = {'r', 'b', 'g', 'm', 'c', 'k'};
get_color = containers.Map(element_list, color_list);

for i = length(checked):-1:1
    if checked(i) == 0
        element_list(i) = [];
    end
end

eles = Elements;
for k = 1:length(element_list)
    [E_name, E_pos] = eles.get_data(element_list{k});
    for i =1:length(E_pos)
        hold on;
        ls = '-'; y0 = 0; y1 = 0;
        if strcmp(element_list{k}, 'FC')
            ls = ':';
            y1 = 1;
        end
        if strcmp(element_list{k}, 'QUAD') || strcmp(element_list{k}, 'ST')
            y0 = 1; y1 = 1;
        end
        col = get_color(element_list{k});
        stem(E_pos(i), y0+1.5, 'linestyle', ls, 'color', col, 'Marker', 'none');
        text(E_pos(i), y1+1.6, E_name{i}, 'color', col, 'FontSize', 8, ...
            'horizontal', 'left', 'vertical', 'middle', 'rotation', 90);
    end
end



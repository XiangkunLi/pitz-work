function r = merge_modules(modules)
% Merge modules of the same kind into one Astra module
% Parameters
%   modules: a list of Module, which have the same Module.name
% Returns
%   merged_module: a Module
% Example:
% c1 = Module('cavity', 'File_Efield()', {'gun.txt'}, 'C_pos()', {0}, 'phi()', {0})
% c2 = Module('cavity', 'File_Efield()', {'booster.txt'}, 'C_pos()', {2.675}, 'phi()', {-10})
% c = merge([c1 c2]);
% c.build()

merged_module = modules(1);
m = length(modules);
for i = 2:m
    module = modules(i);
    keys = module.kv.keys;
    for key = keys
        key = key{1};
        if module.isList(key)
            cur_value = [merged_module.kv(key) module.kv(key)];
            merged_module.reset(key, cur_value);
        end
    end
end

r = merged_module;
end
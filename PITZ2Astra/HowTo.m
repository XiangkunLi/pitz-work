function varargout = HowTo(varargin)
% HOWTO MATLAB code for HowTo.fig
%      HOWTO, by itself, creates a new HOWTO or raises the existing
%      singleton*.
%
%      H = HOWTO returns the handle to a new HOWTO or the handle to
%      the existing singleton*.
%
%      HOWTO('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in HOWTO.M with the given input arguments.
%
%      HOWTO('Property','Value',...) creates a new HOWTO or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before HowTo_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to HowTo_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help HowTo

% Last Modified by GUIDE v2.5 24-Oct-2018 17:40:28

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @HowTo_OpeningFcn, ...
                   'gui_OutputFcn',  @HowTo_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before HowTo is made visible.
function HowTo_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to HowTo (see VARARGIN)

% Choose default command line output for HowTo
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes HowTo wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = HowTo_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

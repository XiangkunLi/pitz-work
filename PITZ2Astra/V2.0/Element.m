classdef Element < handle
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        section;
        name; % unique
        element;
        startposition;
        endposition;
        middleposition;
        length;
        remarks;
    end
    
    methods
        function obj = Element(varargin)
            %if nargin>0
            %    obj.name = name;
            %end
            nargs = length(varargin);
            % Convert keys into upper cases
            keys = varargin(1:2:nargs); 
            values = varargin(2:2:nargs);

            for i = 1:length(keys)
                key = keys{i};
                val = values{i};
                switch key
                    case 'section'
                        obj.section = val;
                    case 'name'
                        obj.name = upper(val);
                    case 'element'
                        obj.element = val;
                    case 'startposition'
                        if ~isempty(val)
                            obj.startposition = val;
                        end
                    case 'startposition'
                        if ~isempty(val)
                            obj.startposition = val;
                        end
                    case 'endposition' 
                        if ~isempty(val)
                            obj.endposition = val;
                        end
                    case 'middleposition'
                        if ~isempty(val)
                            obj.middleposition = val;
                        end
                    case 'length'
                        if ~isempty(val)
                            obj.length = val;
                        end
                end
            end
            
        end
        
        
    end
    
end


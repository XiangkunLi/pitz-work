classdef Astra < handle
% Define the class for generating Astra/Generator inputs
% Note that an Astra input file includes many modules while a Generator
% input includes only one module
% The modules are stored also as a map, for the convenience of
% updating the module by its name. 
    properties
    % modules: a map, consisting of key-value pairs, which is defined as
    %          the module name and the module itself, respectively,
    %          or (Module.name, Module)
        modules = containers.Map(); % list of modules
    end
    
    methods
        function obj = Astra(modules)
        % Class construcator
            obj.modules = containers.Map();
            if nargin > 0
                if ~iscell(modules) % If not a cell array
                    obj.modules(modules.name) = modules;
                else
                    for module = modules
                        mod = module{1};
                        obj.modules(mod.name) = mod;
                    end
                end
            end
        end
        
        
        function r = add_module(obj, module)
        % Add one module, could also be used to update an
        % existing module
            obj.modules(module.name) = module;
        end
        
        
        function r = add_modules(obj, modules)
        % Add multiple modules to the list. If one or more modules
        % already exist, then update them
            for module = modules
                mod = module{1};
                obj.modules(mod.name) = mod;
            end
        end
        
        
        function r = update_parameter(obj, name, key ,value)
        % Allow to modify a module's parameter from this class 
            name = upper(name);
            key = upper(key);
            if isKey(obj.modules, name)
                module = obj.modules(name);
                if isKey(module.kv, key)
                    module.reset(key, value);
                    obj.add_module(module);
                else
                    module.reset(key, value);
                    obj.add_module(module);
                end
            end
        end
        
        function r = append_parameter(obj, name, key ,value)
        % Allow to append new values to an existing module's parameter
            name = upper(name);
            key = upper(key);
            if isKey(obj.modules, name)
                module = obj.modules(name);
                if isKey(module.kv, key)
                    module.reset(key, value, '+');
                    obj.add_module(module);
                else
                    module.reset(key, value);
                    obj.add_module(module);
                end
            end
        end
        
        function r = delete_parameter(obj, name, key)
        % Allow to delete a module's parameter from this class 
            name = upper(name);
            key = upper(key);
            if isKey(obj.modules, name)
                module = obj.modules(name);
                if isKey(module.kv, key)
                    module.delete(key);
                    obj.add_module(module);
                end
            end
        end
        
        
        function r = read(obj, filename)
            % Initiating the class by reading from an input file
            fid = fopen(filename);
            
            flag = 0;
            %line = fgetl(fid);
            while ~feof(fid)
                line = fgetl(fid);
                if contains(line, '&')
                    if ~isempty(sscanf(line, '&%s'))
                        % add a new module here
                        name = sscanf(line, '&%s');
                        module = Module(name);
                        flag = 1;
                        continue
                    else
                        flag = 0;
                    end
                end
                
                if flag == 1 && ~isempty(line)
                    keyValue = regexp(line, '\s*[=,;]\s*', 'split');
                    
                    noc = length(keyValue);
                    keys = keyValue(1:2:noc);
                    values = keyValue(2:2:noc);
                    
                    keys = upper(replace(keys, ' ', ''));
                    %values = replace(values, {'"', ''''}, '');

                    for i = 1:length(keys)
                        value = values{i};
                        if ~isempty(str2num(value))
                            value = str2num(value);
                        elseif any(strcmp({'T.', 'T', 'True', 'true', 'TRUE'}, value))
                            value = true;
                        elseif any(strcmp({'F.', 'F', 'False', 'false', 'FALSE'}, value))
                            value = false;
                        end
                        key = regexp(keys{i}, '\([0-9]+\)', 'split');
                        if length(key)>1
                            key = [key{1} '()'];
                            value = {value};
                        else
                            key = key{1};
                        end
                        if isKey(module.kv, key)
                            %module.reset(key, [module.kv(key) value])
                            module.reset(key, value, '+');
                        else
                            module.reset(key, value);
                        end
                    end
                end
                
                if flag == 0
                    obj.add_module(module);
                end
            end
            fclose(fid); 
        end
        
        
        function r = write(obj, filename)
        % Write the output into a file
            if nargin == 0
                filename = 'ast.in';
            end
            % Prepare for output
            r = '';
            for key = obj.modules.keys
                mod = obj.modules(key{1});
                r = sprintf('%s%s', r, mod.output);
            end
            fid = fopen(filename,'wt');
            r = strrep(r, '\', '\\')
            fprintf(fid, r);
            fclose(fid);
        end
    end
end


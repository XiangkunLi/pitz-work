classdef Elements < handle
    % "Elements" is a list of "Element", including everything defined in
    % the Excel file
    properties
        elements;
    end
    
    methods
        %% Read from an Excel file the elements in the beamline
        function obj = Elements(filename, sheetname)
            if nargin > 0 && ~isempty(filename)
                filename = filename;
            else
                %filename = 'PITZbeamlines.xlsx';
                filename = 'PITZ3-Koordinaten_15-10-15beamlines_THz.xlsx';
            end

            if nargin > 1 && ~isempty(sheetname)
                sheetname = sheetname;
            else
                sheetname = 'PITZ3.0-Koordinatenliste';
            end
            % Check if the file exists. If not, return NULL
            if ~exist(filename, 'file')
                fprintf('NB: file %s does not exists!\n', filename);
                obj = [];
            end
            % The contents are read into three formats: 'num' for numerical, 'txt' for text and 'raw' as what it
            % originally is.
            if isempty(sheetname)
                [num, txt, raw] = xlsread(filename);
            else
                [num, txt, raw] = xlsread(filename, sheetname);
            end
            % Convert raw from cell to array
            raw = string(raw);

            % Get the dimension of the data
            [nrows, ncols] = size(num);
            % fprintf('size: %i %i\n', nrows, ncols);

            j = 0;
            % elements = containers.Map();
            CC = 1;
            for i = 6:nrows
                kv = {};
                if ~ismissing(raw(i,2))
                    j = j+1;
                    name = raw{i,2};
                    kv = [kv {'name', name}];
                elseif ~ismissing(raw(i, 3)) && strcmp(raw(i, 3), 'Gun')
                    kv = [kv {'name', 'Gun'}];
                elseif ~ismissing(raw(i, 3)) && strcmp(raw(i, 3), 'CDS-Booster')
                    kv = [kv {'name', 'CDS-Booster'}];
                else
                    continue;
                end
                    
                if ~ismissing(raw(i, 3))
                    element = raw{i,3};
                    kv = [kv {'element', element}];
                end

                if ~ismissing(raw(i,4))
                    startposition = raw{i,4};
                    kv = [kv {'startposition', str2num(startposition)/1000.0}];
                end
                if ~ismissing(raw(i,5))
                    endposition = raw{i,5};
                    kv = [kv {'endposition', str2num(endposition)/1000.0}];
                end
                if ~ismissing(raw(i,6))
                    middleposition = raw{i,6};
                    kv = [kv {'middleposition', str2num(middleposition)/1000.0}];
                elseif contains(upper(raw(i,2)), ".SCR") && ~ismissing(raw(i+1,6))
                    middleposition = raw{i+1,6};
                    kv = [kv {'middleposition', str2num(middleposition)/1000.0}];
                elseif contains(upper(raw(i,2)), ".ICT") && ~ismissing(raw(i,4)) && ~ismissing(raw(i,5))
                    kv = [kv {'middleposition', (str2num(startposition)+str2num(endposition))/2.0/1000.0}];
                end
                if ~ismissing(raw(i,7))
                    length = raw{i,7};
                    kv = [kv {'length', str2num(length)/1000.0}];
                end
                %disp(kv); %disp('hello');
                names = regexp(kv{2}, '\s*[,;]\s*', 'split');
                if max(size(names))>1
                    len = max(size(kv));
                    for j=1:max(size(names))
                        kvj = ['name', names{j}, {kv{3:len}}];
                        element = Element(kvj{:});
                        %elements(upper(names{j})) = element;
                        
                        elements(CC) = element;
                        CC = CC+1;
                        
                        %elements = [elements {element}];
                        %disp(kvj);
                    end
                else
                    element = Element(kv{:});                  
                    %elements(upper(kv{2})) = element;
                    
                    elements(CC) = element;
                    CC = CC+1;
                        
                    %elements = [elements {element}];
                    %disp(kv);
                end
               
            end
            obj.elements = elements;
        end
        
        %% Get the elements specified by the keyword. keyword could also 
        %  be the name of the element, e.g., 'High1.Scr1'
        function r = get(obj, keyword)
            % Parameters
            %   keyword: a string representing a group of the elements,
            %   such as 'Quad', 'Scr'
            % Returns
            %   r: a list of elements having the keyword
            
            keyword = upper(keyword);
            % r = containers.Map();
            %keys = obj.elements.keys;
            elements = obj.elements;
            
            CC = 1; %r = [];
            switch keyword
                case 'GUN'
                    for i = 1:length(elements)
                       element = elements(i);
                       key = element.name;
                       %element = obj.elements(key);
                       s1 = regexp(key, 'GUN');
                       s2 = regexp(key, 'GUN.');
                       if ~isempty(s1) && isempty(s2)
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
                case {'BOOSTER', 'CDS-BOOSTER'}
                    for i = 1:length(elements)
                       element = elements(i);
                       key = element.name;
                       s1 = regexp(key, 'BOOSTER');
                       if ~isempty(s1)
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
                case {'TDS', 'RFD.TDS'}
                    for i = 1:length(elements)
                       element = elements(i);
                       key = element.name;
                       s1 = regexp(key, 'RFD.TDS');
                       if ~isempty(s1)
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
                case {'UND', 'U1'}
                    for i = 1:length(elements)
                       element = elements(i);
                       key = element.name;
                       s1 = regexp(key, 'U1');
                       if ~isempty(s1)
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
                case {'DIPOLE', 'DIPOL'}
                    for i = 1:length(elements)
                       element = elements(i);
                       key = element.name;
                       s1 = regexp(key, '.DIPOL');
                       if ~isempty(s1)
                           r(CC) = element;
                           CC = CC+1;
                       end
                       s1 = regexp(key, '.D1');
                       if ~isempty(s1)
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
                case 'SCR'
                    for i = 1:length(elements)
                       element = elements(i);
                       key = element.name;
                       s1 = regexp(key, '\.SCR');
                       s2 = regexp(key, 'DISP');
                       if ~isempty(s1) && isempty(s2)
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
                case 'QUAD'
                    for i = 1:length(elements)
                       element = elements(i);
                       key = element.name;
                       s1 = regexp(key, '\.Q');
                       s2 = regexp(key, 'DISP');
                       if ~isempty(s1) && isempty(s2)
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
                case 'ST'
                    for i = 1:length(elements)
                       element = elements(i);
                       key = element.name;
                       s1 = regexp(key, '\.ST');
                       s2 = regexp(key, '\.STREAK');
                       if ~isempty(s1) && isempty(s2)
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
                case 'ICT'
                    for i = 1:length(elements)
                       element = elements(i);
                       key = element.name;
                       s1 = regexp(key, '\.ICT');
                       s2 = regexp(key, 'DISP');
                       if ~isempty(s1) && isempty(s2)
                           r(CC) = element;
                           CC = CC+1;
                       end
                   end
                case 'FC'
                   for i = 1:length(elements)
                       element = elements(i);
                       key = element.name;
                       s1 = regexp(key, '\.FC');
                       s2 = regexp(key, 'DISP');
                       if ~isempty(s1) && isempty(s2)
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
                case 'BPM'
                    for i = 1:length(elements)
                       element = elements(i);
                       key = element.name;
                       s1 = regexp(key, '\.BPM');
                       s2 = regexp(key, 'DISP');
                       if ~isempty(s1) && isempty(s2)
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
                otherwise
                    for i = 1:length(elements)
                       element = elements(i);
                       key = element.name;
                       s1 = strcmp(key, keyword);
                       if s1
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
            end
            if CC == 1
                r = [];
            end
        end
        
        %% Get the names and positions of a group of elements, specified by
        %  the keyword
        function [names, positions] = get_data(obj, keyword, centered)
            % Parameters
            %   keyword: a string representing a group of the elements,
            %   such as 'Quad', 'Scr'
            %   centered: true by default. if true, return the middle
            %   position of the elements; if false, return the starting and
            %   ending positions of the elements
            % Returns
            %   names: a list of names of the elements having the keyword
            %   positions: midddle positions of the elements or starting
            %   and ending positions of the elements
            if nargin>2
                centered = centered;
            else
                centered = true;
            end
            eles = obj.get(keyword);
            %keys = eles.keys;
            pp = []; kk = {};
            for i=1:length(eles)
                key = eles(i).name;
                pos = eles(i).middleposition;
                ff = 0;
                if ~isempty(pos) && centered
                    pp = [pp [pos]];
                    kk = [kk {key}];
                    ff = 1;
                end
                startpos = eles(i).startposition;
                endpos = eles(i).endposition;
                if ~isempty(startpos) && ~isempty(endpos) && ~centered
                    pp = [pp; [startpos endpos]];
                    kk = [kk {key}];
                    ff = 2;
                end
            end
            [positions, index] = sort(pp);
            if ff == 1
                names = kk(index); 
            elseif ff == 2
                names = kk(index(:,1)); 
            end
        end
        
        %% Get the boundary of elements, specified by the keyword.
        %  The bounds are defined by the startposition and endposition.
        function [names, bounds] = get_bounds(obj, keyword)
            eles = obj.get(keyword);
            %keys = eles.keys;
            pp = []; kk = {};
            for i=1:length(eles)
                key = eles(i).name;
                pos1 = eles(i).startposition;
                pos2 = eles(i).endposition;
                if ~isempty(pos1) && ~isempty(pos2)
                    pp = [pp; [pos1 pos2]];
                    kk = [kk {key}];
                end
            end
            [~, index] = sort(pp(:,1));
            b1 = pp(:,1);
            b2 = pp(:,2);
            bounds = [b1(index); b2(index)];
            names = kk(index);
            
        end
        
        %% Print the name and position of the elements
        function r = print(obj, keyword)
            if nargin>1 
                eles = obj.get(keyword);
            else
                % NB: didn't work though
                eles = obj.elements;
            end
            %keys = eles.keys;
            pp = []; kk = {};
            for i=1:length(eles)
                key = eles(i).name;
                pos = eles(i).middleposition;
                if ~isempty(pos) && ~isempty(key)
                    pp = [pp [pos]];
                    kk = [kk {key}];
                end
            end
            [pp, index] = sort(pp);
            kk = kk(index);
            
            for i=1:length(kk)
                key = kk{i};
                pos = pp(i);
                if ~isempty(pos)
                    %fprintf('%s: %f\n', key, pos);
                    fprintf('%s | %f | m\n', key, pos);
                end
            end
        end
        
        %% Make a plot with the selected elements
        function r = plot(obj, varargin)
            % Make a plot of the elements in the list including:
            % 'Quad', 'Scr', 'St', 'ICT', 'FC', 'BPM'
            % Example: plot('Quad', 'Scr')
            zend = 20;
            zmid = zend/2;
            ax_to_fig = zend/(zend+2.0);
            ratio = 1.5;

            
            r = figure('name', 'PITZ beamline', 'numbertitle', 'off', ...
                'units', 'centimeter', 'position', [2, 2, zend+2, 13]*ratio, 'color', 'w');
            %r = fig;
            ax1 = subplot(2, 1, 1);
            ax2 = subplot(2, 1, 2);
            set(ax1, 'units', 'centimeter', 'position', [1 7 zend 5]*ratio, 'YTick', []);
            set(ax2, 'units', 'centimeter', 'position', [1 1 zend 5]*ratio, 'YTick', []);

            subplot(ax1)
            axis([0 zmid 0 5*0.75]);
            xticks([0:1:zmid]);
            ax1.YAxis.Visible = 'off';

            subplot(ax2)
            axis([zmid zend 0 5*0.75]);
            xticks([zmid:1:zend]);
            ax2.YAxis.Visible = 'off';
            xlabel('z (m)');
            
            element_list = upper({'Quad', 'Scr', 'St', 'ICT', 'FC', 'BPM'});
            color_list = {'r', 'b', 'g', 'm', 'c', 'k'};
            get_color = containers.Map(element_list, color_list);
            
            varargin = upper(varargin);
            for k = 1:length(varargin)
                %varargin(k) = upper(varargin(k));
                [E_name, E_pos] = obj.get_data(varargin{k});
                for i =1:length(E_pos)
                    if (E_pos(i)<=zmid)
                        subplot(ax1);
                    else
                        subplot(ax2);
                    end
                    hold on;
                    ls = '-'; y0 = 0; y1 = 0;
                    if strcmp(upper(varargin{k}), 'FC')
                        ls = ':';
                        y1 = 1;
                    end
                    if strcmp(upper(varargin{k}), 'QUAD') || strcmp(upper(varargin{k}), 'ST')
                        y0 = 1; y1 = 1;
                    end
                    col = get_color(varargin{k});
                    stem(E_pos(i), y0+1.5, 'linestyle', ls, 'color', col, 'Marker', 'none');
                    text(E_pos(i), y1+1.6, E_name{i}, 'color', col, 'FontSize', 8, ...
                        'horizontal', 'left', 'vertical', 'middle', 'rotation', 90);
                end
            end
        end
        
        %% Generate input file for SCO software by giving the list of quads
        function r = write_sco(obj, Q_list, varargin)
            % Parameters
            %   Q_list: a list of quads, if not given, use all quads
            %   varargin: optional
            %      Z0: start position of the optimization
            %      lower, higher: bounds of the quads grads
            %      bounds: same as before
            %      zstop: stop position of the optimization
            %      init: initial guess of the quads grads
            %      fbeamline: filename
            nargs = length(varargin);
            % Convert keys into upper cases
            keys = upper(varargin(1:2:nargs)); 
            values = varargin(2:2:nargs);
            % Set the key-value pairs to `kv`
            if nargs>0
                kv = containers.Map(keys, values);
            else
                kv = containers.Map();
            end
            
            if isKey(kv, 'Z0')
                z0 = kv('Z0');
            else
                z0 = 5.277;
            end
            
            if isKey(kv, 'HIGHER')
                higher = kv('HIGHER');
            else
                higher = 1.5;
            end
            
            if isKey(kv, 'LOWER')
                lower = kv('LOWER');
            else
                lower = -higher;
            end
            
            if isKey(kv, 'BOUNDS')
                bounds = kv('BOUNDS');
            else
                bounds = {};
                for i = 1:length(Q_list)
                    bounds = [bounds {[lower higher]}];
                end
            end
            
            if isKey(kv, 'ZSTOP')
                zstop = kv('ZSTOP');
            else
                zstop = 18;
            end
            
            if isKey(kv, 'INIT')
                init = kv('INIT');
            else
                init = zeros(1,length(Q_list));
            end
            
            if isKey(kv, 'FILENAME')
                fbeamline = kv('FILENAME');
            else
                fbeamline = 'beamline.txt';
            end
            
            eles = obj.get('quad');
            %keys = eles.keys;
            
            pp = []; kk = {};
            for i=1:length(eles)
                key = eles(i).name;
                pos = eles(i).middleposition;
                if ~isempty(pos)
                    pp = [pp [pos]];
                    kk = [kk {key}];
                end
            end
            [pp, index] = sort(pp);
            kk = kk(index);
            
            nQuads = length(Q_list);
            for k=nQuads:-1:1
                Q_name = Q_list(k);
                select = cellfun(@(x)isequal(x, upper(Q_name)), kk);
                zk = pp(select);
                if zk<z0
                    Q_list(k) = [];
                end
            end
            
            k = 1;
            n = length(Q_list);
            r = [];
            for i = 1:length(kk);
                pos = pp(i)-0.0675/2;
                if pos > z0 && pos < zstop
                    r = [r sprintf('O%12.6f%12.6f%12.6f%12.6f%6d\n', pos-z0, ...
                                0, 0, 0, 0)];
                    if k<=n && strcmp(Q_list(k), kk{i})
                        r = [r sprintf('Q%12.6f%12.6f%12.6f%12.6f%6d\n', 0.0675, ...
                            init(k), bounds{k}(1), bounds{k}(2), 1)];
                        k = k+1;
                    else
                        r = [r sprintf('Q%12.6f%12.6f%12.6f%12.6f%6d\n', 0.0675, ...
                            0, 0, 0, 0)];
                    end
                    z0 = pp(i)+0.0675/2;
                end
            end
            r = [r sprintf('O%12.6f%12.6f%12.6f%12.6f%6d\n', zstop-z0, ...
                0, 0, 0, 0)];
            
            fid = fopen(fbeamline,'wt');
            fprintf(fid, r);
            fclose(fid);
            
        end
        
        %% Convert the elements to a list of Modules. Note that each element
        %  corresponds to a Module
        function r = to_modules(obj)
            % Get Gun and Booster
            gun = obj.get('Gun'); gun.startposition = 0;
            solenoid = obj.get('Main'); solenoid.middleposition = 0;
            booster = obj.get('Booster');
            cavity_list = {gun booster};
            solenoid_list = {solenoid};
            
            [Q_name, Q_pos] = obj.get_data('Quad');
            [S_name, S_pos] = obj.get_data('Scr');
            
            k = 1;
            modules(k) = Module('Newrun', 'Run', 1, 'Head', 'PITZ beamline', ...
                'Distribution', 'beam.ini', 'Auto_Phase', true, ...
                'Track_All', true, 'check_ref_part', false, 'Lprompt', false, ...
                'Max_step', 200000); 
            modules(k).set_tag('NEWRUN');
            k = k+1;

            modules(k) = Module('Charge', 'LSPCH', true, 'Lmirror', true, ...
                'L2d_3d', true, 'z_trans', 4.5,...
                'Nrad', 50, 'Nlong_in', 50, 'N_min', 10, 'Max_scale', 0.05, ...
                'Max_count', 20, 'Nxf', 16, 'Nyf', 16, 'Nzf', 16);
            modules(k).set_tag('CHARGE');
            k = k+1;

            modules(k) = Module('Output', 'Zstart', 0, 'Zstop', 20.0, 'Zemit', 200, ...
                'Zphase', 1, 'RefS', true, 'EmitS', true, 'PhaseS', true, ...
                'TrackS', false, 'LandFS', true, 'Screen()', {0.5});
            modules(k).set_tag('OUTPUT');
            k = k+1;
            
            for i = 1:length(S_pos)
                modules(k) = Module('Output', 'Zstart', 0, 'Zstop', 20.0, 'Zemit', 200, ...
                    'Zphase', 1, 'RefS', true, 'EmitS', true, 'PhaseS', true, ...
                    'TrackS', false, 'LandFS', true, 'Screen()', {S_pos(i)});
                modules(k).set_tag(S_name{i});
                k = k+1;
            end       
            
            cavity_file = {'gun45cavity.txt', 'CDS14_15mm.txt'};
            for i = 1:length(cavity_list)
                cav = cavity_list{i};
                modules(k) = Module('Cavity', 'LEfield', true, ...
                    'File_Efield()', cavity_file(i), ...
                    'MaxE()', {0}, 'C_pos()', {cav.startposition}, ...
                    'Nue()', {1.3}, 'Phi()', {0});
                modules(k).set_tag(cav.name);
                k = k+1;
            end

            solenoid_file = {'gunsolenoidsPITZ.txt'};
            for i = 1:length(solenoid_list)
                sol = solenoid_list{i};
                modules(k) = Module('Solenoid', 'LBfield', true, ...
                    'File_Bfield()', solenoid_file(i), ...
                    'MaxB()', {0}, 'S_pos()', {sol.middleposition});
                modules(k).set_tag(sol.name);
                k = k+1;
            end
            
            Q_type = cell(size(Q_pos));
            Q_type(:) = {'Q3.data'};
            %Q_type(:) = {fullfile(field_maps,'Q3.data')};
            Q_grad = cell(size(Q_pos));
            Q_grad(:) = {0};
            
            Q_pos = num2cell(Q_pos);
            for i = 1:length(Q_name)
                modules(k) = Module('Quadrupole', 'Lquad', true, 'Q_pos()', Q_pos(i), ...
                    'Q_grad()', Q_grad(i), 'Q_type()', Q_type(i));
                modules(k).set_tag(Q_name{i});
                k = k+1;
            end   
            
            r = modules;
            
        end
        
        %% Sort a list of names according to the positions of the corresponding
        %  element
        function [name1 name2] = sort(obj, names)
            pos1 = []; name1 = {}; name2 = {};
            for i = 1:length(names)
                name = names{i};
                element = obj.get(name);
                if ~isempty(element)
                    if ~isempty(element.middleposition)
                        pos1 = [pos1 element.middleposition];
                    else
                        pos1 = [pos1 element.startposition];
                        %name2 = [name2 {name}];
                        %continue;
                    end
                    name1 = [name1 {name}];
                else
                    name2 = [name2 {name}];
                end
            end
            [pos1, index] = sort(pos1);
            name1 = name1(index);
        end
        %% Generate input file for Astra simulation
        function r = write_astra(obj)
            
            modules = obj.to_modules();

            astra = Astra();
            astra.merge_to_write(modules);
            r = astra;
        end
        
        %% Also generate input file for Astra, but in a straightforward way
        function r = write_astra0(obj)
            % Get Gun and Booster
            gun = obj.get('Gun'); gun.startposition = 0;
            booster = obj.get('Booster');
            cavity_list = {gun booster};
            
            [Q_name, Q_pos] = obj.get_data('Quad');
            [S_name, S_pos] = obj.get_data('Scr');
            
            newrun = Module('Newrun', 'Run', 1, 'Head', 'PITZ beamline', ...
                'Distribution', 'beam.ini', 'Auto_Phase', true, ...
                'Track_All', true, 'check_ref_part', false, 'Lprompt', false, ...
                'Max_step', 200000);

            charge = Module('Charge', 'LSPCH', true, 'Lmirror', true, ...
                'L2d_3d', true, 'z_trans', 4.5,...
                'Nrad', 50, 'Nlong_in', 50, 'N_min', 10, 'Max_scale', 0.05, ...
                'Max_count', 20, 'Nxf', 16, 'Nyf', 16, 'Nzf', 16);
            
            Q_type = cell(size(Q_pos));
            Q_type(:) = {'Q3.data'};
            %Q_type(:) = {fullfile(field_maps,'Q3.data')};
            Q_grad = cell(size(Q_pos));
            Q_grad(:) = {0};
            
            Q_pos = num2cell(Q_pos);
      
            cavity = Module('Cavity', 'LEfield', true, ...
                'File_Efield()', {'gun45cavity.txt', 'CDS14_15mm.txt'}, ...
                'MaxE()', {0, 0}, 'C_pos()', {0., booster.startposition}, ...
                'Nue()', {1.3, 1.3}, 'Phi()', {0, 0});
            solenoid = Module('Solenoid', 'LBfield', true, ...
                'File_Bfield()', {'gunsolenoidsPITZ.txt'}, ...
                'MaxB()', {0.}, 'S_pos()', {0});
            quadrupole = Module('Quadrupole', 'Lquad', true, 'Q_pos()', num2cell(Q_pos), ...
                'Q_grad()', Q_grad, 'Q_type()', Q_type);
            output = Module('Output', 'Zstart', 0, 'Zstop', 20.0, 'Zemit', 200, ...
                'Zphase', 1, 'RefS', true, 'EmitS', true, 'PhaseS', true, ...
                'TrackS', false, 'LandFS', true, 'Screen()', num2cell(S_pos));
          
            astra = Astra();
            astra.add_modules({newrun, charge, cavity, solenoid, output, ...
                quadrupole});
            %astra.write('ast.in');
            r = astra;
            
            [file,path,indx] = uiputfile('ast.in', 'Save as');
            if indx>0
                astra.write(fullfile(path, file));
            else
                astra.write('ast.in');
                disp('NB: Save as ast.in in the current folder!');
            end
            
        end
        
        %% Not finished yet
        function r = find_modules(obj)
            eles = obj.elements;
            r = eles;
        end
        
    end
end

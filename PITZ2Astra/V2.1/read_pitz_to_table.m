function elements = read_pitz_to_table(filename)
% Read PITZ beamlines (in Excel format) into Matlab.
% Parameters
%   filename: the name of the file that stores the beam line coordinates
%             and length
% Returns
%   QuadPos : the positions of the quadrupoles
%   ScrPos  : the positions of the screens
    
    if nargin > 0
        filename = filename;
    else
        filename = 'PITZbeamlines.xlsx';
    end
    
    % Check if the file exists. If not, return NULL
    if ~exist(filename, 'file')
        QuadPos = [];
        ScrPos = [];
        fprintf('NB: file %s does not exists!\n', filename);
        return;
    end
    % The contents are read into three formats: 'num' for numerical, 'txt' for text and 'raw' as what it
    % originally is.
    [num, txt, raw] = xlsread(filename);
    % Convert raw from cell to array
    raw = string(raw);

    % Get the dimension of the data
    [nrows, ncols] = size(num);
    % fprintf('size: %i %i\n', nrows, ncols);

    j = 0;
    elements = containers.Map();
    for i = 6:nrows
        kv = {};
        if ~ismissing(raw(i,2))
            j = j+1;
            name = raw{i,2};
            kv = [kv {'name', name}];
            if ~ismissing(raw(i, 3))
                element = raw{i,3};
                kv = [kv {'element', element}];
                if ~ismissing(raw(i,4))
                    startposition = raw{i,4};
                    kv = [kv {'startposition', startposition}];
                end
                if ~ismissing(raw(i,5))
                    endposition = raw{i,5};
                    kv = [kv {'endposition', endposition}];
                end
                if ~ismissing(raw(i,6))
                    middleposition = raw{i,6};
                    kv = [kv {'middleposition', middleposition}];
                elseif contains(upper(raw(i,2)), ".SCR") && ~ismissing(raw(i+1,6))
                    middleposition = raw{i+1,6};
                    kv = [kv {'middleposition', middleposition}];
                end
                if ~ismissing(raw(i,7))
                    length = raw{i,7};
                    kv = [kv {'length', length}];
                end
            end
            names = regexp(kv{2}, '\s*[,;]\s*', 'split')
            if max(size(names))>1
                len = max(size(kv));
                for j=1:max(size(names))
                    kvj = ['name', names{j}, {kv{3:len}}];
                    element = Element(kvj{:});
                    elements(upper(names{j})) = element;
                    %elements = [elements {element}];
                    %disp(kvj);
                end
            else
                element = Element(kv{:});
                elements(upper(kv{2})) = element;
                %elements = [elements {element}];
                %disp(kv);
            end
        end        
    end

end


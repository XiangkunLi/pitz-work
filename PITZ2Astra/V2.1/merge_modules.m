function r = merge_modules(modules)
% Merge modules of the same kind into one Astra module
% Parameters
%   modules: a list of Module, which have the same Module.name
% Returns
%   merged_module: a Module
% Example:
% c1 = Module('cavity', 'File_Efield()', {'gun.txt'}, 'C_pos()', {0}, 'phi()', {0})
% c2 = Module('cavity', 'File_Efield()', {'booster.txt'}, 'C_pos()', {2.675}, 'phi()', {-10})
% c = merge_modules([c1 c2]);
% c.build()

merged_module = Module(modules(1).name);
m = length(modules);
for i = 1:m
    module = modules(i);
    if i > 1
        merged_module.tag = {merged_module.tag module.tag};
    else
        merged_module.tag = module.tag;
    end
    keys = module.kv.keys;
    for key = keys
        key = key{1};
        if module.isList(key)
            if i > 1
                cur_value = [merged_module.kv(key) module.kv(key)];
            else
                cur_value = [module.kv(key)];
            end
            merged_module.reset(key, cur_value);
        end
    end
end

r = merged_module;
end
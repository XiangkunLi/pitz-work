function [QuadPos, ScrPos] = read_pitz(filename)
% Read PITZ beamlines (in Excel format) into Matlab.
% Parameters
%   filename: the name of the file that stores the beam line coordinates
%             and length
% Returns
%   QuadPos : the positions of the quadrupoles
%   ScrPos  : the positions of the screens
    
    if nargin > 0
        filename = filename;
    else
        filename = 'PITZbeamline.xlsx';
    end
    
    % Check if the file exists. If not, return NULL
    if ~exist(filename, 'file')
        QuadPos = [];
        ScrPos = [];
        fprintf('NB: file %s does not exists!\n', filename);
        return;
    end
    % The contents are read into three formats: 'num' for numerical, 'txt' for text and 'raw' as what it
    % originally is.
    [num, txt, raw] = xlsread(filename);
    % Convert raw from cell to array
    raw = string(raw);

    % Get the dimension of the data
    [nrows, ncols] = size(num);
    % fprintf('size: %i %i\n', nrows, ncols);

    j = 0;
    QuadPos = []; % Get the positions of quadrupoles in the straight line
    for i = 1:nrows
        % disp(i)
        % Check if a quadrupole exists for the i-th row
        % The rows with empty "Name" or belonging to "DISP" sections should
        % be excluded. If "Element" of the row is "Quadrupol", its central
        % position is recorded.
        if ~ismissing(raw(i,2)) && ~contains(raw(i,2), "DISP") && ...
                contains(raw(i,3), "Quadrupol")
            j = j+1;
            pos = str2double(raw(i, 6))/1e3; % unit: meter
            QuadPos = [QuadPos pos];
            %fprintf('%i: %.6f\n', j, pos);
        end
    end

    j = 0;
    ScrPos = []; % Get the positions of screens in the straight line
    for i = 1:nrows
        % disp(i)
        % Check if a screen exists for the i-th row:
        % The rows with empty "Name" or belonging to "DISP" sections should
        % be excluded. If the string "Name" contains ".Scr" or ".SCR", its
        % central position is recorded
        if ~ismissing(raw(i,2)) && ~contains(raw(i,2), "DISP") && ... 
                contains(raw(i,2), ".Scr") || contains(raw(i,2), ".SCR")
            j = j+1;
            if ismissing(raw(i, 6))
                pos = str2double(raw(i+1, 6))/1e3; % unit: meter
            else
                pos = str2double(raw(i, 6))/1e3;   % unit: meter
            end
            ScrPos = [ScrPos pos];
            % fprintf('%i: %.6f\n', j, pos);
        end
    end

end



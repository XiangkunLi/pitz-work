% ans = Elements;
% ans.write_sco({'HIGH1.Q8' 'HIGH1.Q9' 'PST.QM1' 'PST.QT4' 'PST.QT5' 'HIGH2.Q1'})
% 
classdef Elements < handle
    properties
        elements = containers.Map();
    end
    
    methods
        function obj = Elements(filename)
            if nargin > 0
                filename = filename;
            else
                filename = 'PITZbeamlines.xlsx';
            end

            % Check if the file exists. If not, return NULL
            if ~exist(filename, 'file')
                fprintf('NB: file %s does not exists!\n', filename);
                obj = [];
            end
            % The contents are read into three formats: 'num' for numerical, 'txt' for text and 'raw' as what it
            % originally is.
            [num, txt, raw] = xlsread(filename);
            % Convert raw from cell to array
            raw = string(raw);

            % Get the dimension of the data
            [nrows, ncols] = size(num);
            % fprintf('size: %i %i\n', nrows, ncols);

            j = 0;
            elements = containers.Map();
            for i = 6:nrows
                kv = {};
                if ~ismissing(raw(i,2))
                    j = j+1;
                    name = raw{i,2};
                    kv = [kv {'name', name}];
                    if ~ismissing(raw(i, 3))
                        element = raw{i,3};
                        kv = [kv {'element', element}];
                        if ~ismissing(raw(i,4))
                            startposition = raw{i,4};
                            kv = [kv {'startposition', str2num(startposition)/1000.0}];
                        end
                        if ~ismissing(raw(i,5))
                            endposition = raw{i,5};
                            kv = [kv {'endposition', str2num(endposition)/1000.0}];
                        end
                        if ~ismissing(raw(i,6))
                            middleposition = raw{i,6};
                            kv = [kv {'middleposition', str2num(middleposition)/1000.0}];
                        elseif contains(upper(raw(i,2)), ".SCR") && ~ismissing(raw(i+1,6))
                            middleposition = raw{i+1,6};
                            kv = [kv {'middleposition', str2num(middleposition)/1000.0}];
                        end
                        if ~ismissing(raw(i,7))
                            length = raw{i,7};
                            kv = [kv {'length', str2num(length)/1000.0}];
                        end
                    end
                    names = regexp(kv{2}, '\s*[,;]\s*', 'split');
                    if max(size(names))>1
                        len = max(size(kv));
                        for j=1:max(size(names))
                            kvj = ['name', names{j}, {kv{3:len}}];
                            element = Element(kvj{:});
                            elements(upper(names{j})) = element;
                            %elements = [elements {element}];
                            %disp(kvj);
                        end
                    else
                        element = Element(kv{:});
                        elements(upper(kv{2})) = element;
                        %elements = [elements {element}];
                        %disp(kv);
                    end
                end        
            end
            obj.elements = elements;
        end
        
        
        function r = get_screens(obj)
           r = containers.Map();
           keys = obj.elements.keys;
           for i = 1:length(keys)
               key = keys{i};
               element = obj.elements(key);
               s1 = regexp(key, '\.SCR');
               s2 = regexp(key, 'DISP');
               if ~isempty(s1) && isempty(s2)
                   r(key) = element;
               end
           end
        end
        
        
        function r = get_quadrupoles(obj)
           r = containers.Map();
           keys = obj.elements.keys;
           for i = 1:length(keys)
               key = keys{i};
               element = obj.elements(key);
               s1 = regexp(key, '\.Q');
               s2 = regexp(key, 'DISP');
               if ~isempty(s1) && isempty(s2)
                   r(key) = element;
               end
           end
        end
        
        
        function r = get_steerers(obj)
           r = containers.Map();
           keys = obj.elements.keys;
           for i = 1:length(keys)
               key = keys{i};
               element = obj.elements(key);
               s1 = regexp(key, '\.ST');
               s2 = regexp(key, '\.STREAK');
               if ~isempty(s1) && isempty(s2)
                   r(key) = element;
               end
           end
        end
        
        
        function r = get_ICTs(obj)
           r = containers.Map();
           keys = obj.elements.keys;
           for i = 1:length(keys)
               key = keys{i};
               element = obj.elements(key);
               s1 = regexp(key, '\.ICT');
               s2 = regexp(key, 'DISP');
               if ~isempty(s1) && isempty(s2)
                   r(key) = element;
               end
           end
        end
        
        
        function r = get_FCs(obj)
           r = containers.Map();
           keys = obj.elements.keys;
           for i = 1:length(keys)
               key = keys{i};
               element = obj.elements(key);
               s1 = regexp(key, '\.FC');
               s2 = regexp(key, 'DISP');
               if ~isempty(s1) && isempty(s2)
                   r(key) = element;
               end
           end
        end 
        
        function r = print_elements(obj, keyword)
            keyword = upper(keyword);
            switch keyword
                case 'SCR'
                    eles = obj.get_screens;
                case 'QUAD'
                    eles = obj.get_quadrupoles;
                case 'ST'
                    eles = obj.get_steerers;
                case 'ICT'
                    eles = obj.get_ICTs;
                case 'FC'
                    eles = obj.get_FCs;
            end
            
            keys = eles.keys;
            pp = []; kk = {};
            for i=1:length(keys)
                key = keys{i};
                pos = eles(key).middleposition;
                if ~isempty(pos)
                    pp = [pp [pos]];
                    kk = [kk {key}];
                end
            end
            [pp, index] = sort(pp);
            kk = kk(index);
            
            for i=1:length(keys)
                key = kk{i};
                pos = pp(i);
                if ~isempty(pos)
                    %fprintf('%s: %f\n', key, pos);
                    fprintf('%s | %f | m\n', key, pos);
                end
            end
        end
        
        
        function r = write_sco(obj, keywords, varargin)
            
            nargs = length(varargin);
            % Convert keys into upper cases
            keys = upper(varargin(1:2:nargs)); 
            values = varargin(2:2:nargs);
            % Set the key-value pairs to `kv`
            if nargs>0
                kv = containers.Map(keys, values);
            else
                kv = containers.Map();
            end
            
            if isKey(kv, 'z0')
                z0 = kv('z0');
            else
                z0 = 5.277;
            end
            
            if isKey(kv, 'higher')
                higher = kv('higher');
            else
                higher = 1;
            end
            
            if isKey(kv, 'lower')
                lower = kv('lower');
            else
                lower = -higher;
            end
            
            if isKey(kv, 'bounds')
                bounds = kv('bounds');
            else
                bounds = {};
                for i = 1:length(keywords)
                    bounds = [bounds {[lower higher]}];
                end
            end
            
            if isKey(kv, 'zstop')
                zstop = kv('zstop');
            else
                zstop = 18;
            end
            
            if isKey(kv, 'init')
                init = kv('init');
            else
                init = zeros(1,length(keywords));
            end
            
            if isKey(kv, 'fbeamline')
                fbeamline = kv('fbeamline');
            else
                fbeamline = 'beamline.txt';
            end
            
            eles = obj.get_quadrupoles;
            keys = eles.keys;
            
            pp = []; kk = {};
            for i=1:length(keys)
                key = keys{i};
                pos = eles(key).middleposition;
                if ~isempty(pos)
                    pp = [pp [pos]];
                    kk = [kk {key}];
                end
            end
            [pp, index] = sort(pp);
            kk = kk(index);
            
            n = length(keywords);
            for k=1:n
                if eles(keywords{n+1-k}).middleposition<z0
                    keywords(n+1-k) = [];
                end
            end
            
            k = 1;
            n = length(keywords);
            r = [];
            for i = 1:length(keys);
                pos = pp(i)-0.0675/2;
                if pos > z0 && pos < zstop
                    r = [r sprintf('D%12.6f%12.6f%12.6f%12.6f%6d\n', pos-z0, ...
                                0, 0, 0, 0)];
                    if k<=n && strcmp(keywords{k}, kk{i})
                        r = [r sprintf('Q%12.6f%12.6f%12.6f%12.6f%6d\n', 0.0675, ...
                            init(k), bounds{k}(1), bounds{k}(2), 1)];
                        k = k+1;
                    else
                        r = [r sprintf('Q%12.6f%12.6f%12.6f%12.6f%6d\n', 0.0675, ...
                            0, 0, 0, 0)];
                    end
                    z0 = pp(i)+0.0675/2;
                end
            end
            r = [r sprintf('D%12.6f%12.6f%12.6f%12.6f%6d\n', zstop-z0, ...
                0, 0, 0, 0)];
            
            fid = fopen(fbeamline,'wt');
            fprintf(fid, r);
            fclose(fid);
            
        end
        

    end
end

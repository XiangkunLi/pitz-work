clear all;
clc;
close all;

Q_pos(1) = 4.7900 ;        %HIGH1.Q1
Q_pos(2) = 5.0050 ;        %HIGH1.Q2
Q_pos(3) = 5.6025 ;        %HIGH1.Q3
Q_pos(4) = 5.8525 ;        %HIGH1.Q4
Q_pos(5) = 6.6475 ;        %HIGH1.Q5
Q_pos(6) = 6.8925 ;        %HIGH1.Q6
Q_pos(7) = 8.1800 ;        %HIGH1.Q7
Q_pos(8) = 8.6550 ;        %HIGH1.Q8
Q_pos(9) = 10.208 ;        %HIGH1.Q9
Q_pos(10) = 10.388 ;       %HIGH1.Q10

Q_pos(11) = 12.088 ;       %PST.QM1
Q_pos(12) = 12.468 ;       %PST.QM2
Q_pos(13) = 12.848 ;       %PST.QM3
Q_pos(14) = 13.228 ;       %PST.QT1
Q_pos(15) = 13.608 ;       %PST.QT2
Q_pos(16) = 13.988 ;       %PST.QT3
Q_pos(17) = 14.368 ;       %PST.QT4
Q_pos(18) = 14.748 ;       %PST.QT5
Q_pos(19) = 15.128 ;       %PST.QT6
Q_pos(20) = 16.635 ;       %HIGH2.Q1
Q_pos(21) = 16.735 ;       %HIGH2.Q2
% Q_pos(22) = 21.400 ;       %THZ.Q1
% Q_pos(23) = 21.800 ;       %THZ.Q2
% Q_pos(24) = 22.200 ;       %THZ.Q3

S_pos(1) = 0.803 ;        %LOW.S1
S_pos(2) = 1.379 ;        %LOW.S2
S_pos(3) = 1.708 ;        %LOW.S3
S_pos(4) = 5.277 ;        %HIGH1.S1
S_pos(5) = 7.125 ;        %HIGH1.S3
S_pos(6) = 8.410 ;        %HIGH1.S4
S_pos(7) = 8.920 ;        %HIGH1.S5
S_pos(8) = 12.278 ;        %PST.S1
S_pos(9) = 13.038 ;        %PST.S2
S_pos(10) = 13.798 ;        %PST.S3
S_pos(11) = 14.558 ;        %PST.S4
S_pos(12) = 15.318 ;        %PST.S5
S_pos(13) = 16.303 ;        %HIGH2.S1
S_pos(14) = 18.262 ;        %HIGH2.S2
% S_pos(8) = 21.300 ;        %THZ.S1
% S_pos(9) = 22.500 ;        %THZ.S2

%% QM position loop
QPosMat = [];
QH = [];
 for i = 1:length(Q_pos)
     QPosMat = [QPosMat; Q_pos(i) Q_pos(i)];
     QH = [QH; 0 1.5];
 end
 
 
 SPosMat = [];
 SH = [];
 for j = 1:length(S_pos)
     SPosMat = [SPosMat; S_pos(j) S_pos(j)];
     SH = [SH; 0 2];
 end



figure(2);

set(gcf, 'units','inch','position', [1 1 25 5],'color','w');
Graph = plot(QPosMat',QH','r',SPosMat',SH','b');
set(Graph, 'LineWidth', 2);
set(gca, 'FontSize',32, 'FontWeight', 'bold', 'FontName', 'Arial','yticklabel',[],'xTick',(1:23));
 axis([0 19 0 2]);
box on;
grid on;
xlabel('Z [m]' );
saveas(gcf,'PITZ3_SaQ.jpg')


classdef test < containers.Map
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        name;
        tag;
    end
    
    methods
        function obj = test(name, tag, varargin)
           if nargin > 0
                obj.name = upper(name);
                if nargin > 1
                    obj.tag = tag;
                    
                    % Get the key-value pairs from `varargin`
                    nargs = length(varargin);
                    % Convert keys into upper cases
                    keys = upper(varargin(1:2:nargs)); 
                    values = varargin(2:2:nargs);
                    % Set the key-value pairs to `kv`
                    for i = 1:length(keys)
                        obj(keys{i}) = values{i};
                    end
                    %obj.set_len();
                    %obj.set_tag();
                    % Generating the output string
                    %obj.build();
                end
           end     
        end
    end
    
end


classdef Astra < handle
% Define the class for generating Astra/Generator inputs
% Note that an Astra input file includes many modules while a Generator
% input includes only one module
% The modules are stored also as a map, for the convenience of
% updating the module by its name. 
    properties
    % modules: a map, consisting of key-value pairs, which is defined as
    %          the module name and the module itself, respectively,
    %          or (Module.name, Module)
        modules = containers.Map(); % list of modules
    end
    
    methods
        %% Construcator
        function obj = Astra(modules)
            %obj.modules = containers.Map();
            if nargin > 0
                
                newrun = obj.merge(modules, 'NEWRUN');
                charge = obj.merge(modules, 'CHARGE');
                output = obj.merge(modules, 'OUTPUT');
                cavity = obj.merge(modules, 'CAVITY');
                solenoid   = obj.merge(modules, 'SOLENOID');
                quadrupole = obj.merge(modules, 'QUADRUPOLE');
                
                modules = {newrun charge output cavity solenoid quadrupole};
            
                if ~iscell(modules) % If not a cell array
                    obj.modules(modules.name) = modules;
                else
                    for module = modules
                        mod = module{1};
                        obj.modules(mod.name) = mod;
                    end
                end
            end
        end
        
        %% If the object is null or not
        function r = isNull(obj)
            if isempty(obj.modules)
                r = true;
            else
                r = false;
            end
        end
        
        function r = add_module(obj, module)
        % Add one module, could also be used to update an
        % existing module
            obj.modules(module.name) = module;
        end
                
        function r = add_modules(obj, modules)
        % Add multiple modules to the list. If one or more modules
        % already exist, then update them
            for module = modules
                mod = module{1};
                obj.modules(mod.name) = mod;
            end
        end       
        
        function r = update_parameter(obj, name, key ,value)
        % Allow to modify a module's parameter from this class 
            name = upper(name);
            key = upper(key);
            if isKey(obj.modules, name)
                module = obj.modules(name);
                if isKey(module.kv, key)
                    module.reset(key, value);
                    obj.add_module(module);
                else
                    module.reset(key, value);
                    obj.add_module(module);
                end
            end
        end
        
        function r = append_parameter(obj, name, key, value)
        % Allow to append new values to an existing module's parameter
            name = upper(name);
            key = upper(key);
            if isKey(obj.modules, name)
                module = obj.modules(name);
                if isKey(module.kv, key)
                    module.reset(key, value, '+');
                    obj.add_module(module);
                else
                    module.reset(key, value);
                    obj.add_module(module);
                end
            end
        end
        
        function r = delete_parameter(obj, name, key)
        % Allow to delete a module's parameter from this class 
            name = upper(name);
            key = upper(key);
            if isKey(obj.modules, name)
                module = obj.modules(name);
                if isKey(module.kv, key)
                    module.delete(key);
                    obj.add_module(module);
                end
            end
        end
               
        function r = read(obj, filename)
            % Initiating the class by reading from an input file
            fid = fopen(filename);
            
            flag = 0;
            %line = fgetl(fid);
            while ~feof(fid)
                line = fgetl(fid);
                if contains(line, '&')
                    if ~isempty(sscanf(line, '&%s'))
                        % add a new module here
                        name = sscanf(line, '&%s');
                        module = Module(name);
                        flag = 1;
                        continue
                    else
                        flag = 0;
                    end
                end
                
                if flag == 1 && ~isempty(line)
                    keyValue = regexp(line, '\s*[=,;]\s*', 'split');
                    
                    noc = length(keyValue);
                    keys = keyValue(1:2:noc);
                    values = keyValue(2:2:noc);
                    
                    keys = upper(replace(keys, ' ', ''));
                    %values = replace(values, {'"', ''''}, '');

                    for i = 1:length(keys)
                        value = values{i};
                        if ~isempty(str2num(value))
                            value = str2num(value);
                        elseif any(strcmp({'T.', 'T', 'True', 'true', 'TRUE'}, value))
                            value = true;
                        elseif any(strcmp({'F.', 'F', 'False', 'false', 'FALSE'}, value))
                            value = false;
                        end
                        key = regexp(keys{i}, '\([0-9]+\)', 'split');
                        if length(key)>1
                            key = [key{1} '()'];
                            value = {value};
                        else
                            key = key{1};
                        end
                        if isKey(module.kv, key)
                            %module.reset(key, [module.kv(key) value])
                            module.reset(key, value, '+');
                        else
                            module.reset(key, value);
                        end
                    end
                end
                
                if flag == 0
                    module.set_tag();
                    obj.add_module(module);
                end
            end
            fclose(fid); 
        end        
        
        function r = read_to_modules(obj, filename)
            obj.read(filename);
            
            r = [];
            keys = obj.modules.keys;
            for i = 1:length(keys)
                key = keys{i};
                module = obj.modules(key);
                r = [r obj.split(module)];
            end
        end
        
        function r = to_modules(obj)
            
            r = [];
            keys = obj.modules.keys;
            for i = 1:length(keys)
                key = keys{i};
                module = obj.modules(key);
                r = [r obj.split(module)];
            end
        end
        
        function [r, varargout] = filter(obj, modules, names)
            
            idx = [];
            for i = 1:length(names)
                name = names{i};
                for k = 1:length(modules)
                    module = modules(k);
                    if strcmp(module.tag, name)
                        idx = [idx k];
                    end
                end
            end
            
            r = modules(idx);
            if nargout>1
                varargout{1} = idx;
            end
            
        end
        
        function r = split(obj, module)
            name = module.name;
            keys = module.kv.keys;

            len = module.len;
            for i = 1:len     
                modules(i) = Module(name);
                if ~isempty(module.tag)
                    modules(i).tag = module.tag{i};
                else
                    modules(i).tag = [name, num2str(i)];
                end
            end

            for k = 1:length(keys)
                key = keys{k};
                if module.isList(key)
                    vals = module.kv(key);
                    for i = 1:length(vals)
                        modules(i).reset(key, vals(i))
                    end
                else
                    val = module.kv(key);
                    for i = 1:length(modules)
                        modules(i).reset(key, val);
                    end
                end
            end
            % disp(modules);
            if ~isempty(keys)
                r = modules;
            else
                r = [];
            end
        end
        
        function r = merge(obj, modules, name)
            if nargin == 1
                name = [];
            end
            
            if ~isempty(name)
                n = length(modules);
                for i = n:-1:1
                    if ~strcmp(modules(i).name, name)
                        modules(i) = [];
                    end
                end
            end
            % r = merge_modules(modules);
            
            merged_module = Module(name);
            if ~isempty(modules)
                m = length(modules);
                for i = 1:m
                    module = modules(i);
                    if i > 1
                        merged_module.tag = [merged_module.tag {module.tag}];
                    else
                        merged_module.tag = {module.tag};
                    end
                    keys = module.kv.keys;
                    for k = 1:length(keys)
                        key = keys{k};
                        if module.isList(key)
                            if i > 1
                                cur_value = [merged_module.kv(key) module.kv(key)];
                            else
                                cur_value = [module.kv(key)];
                            end
                            merged_module.reset(key, cur_value);
                        else
                            merged_module.reset(key, module.kv(key));
                        end

                    end

                end
                merged_module.set_len();
            end
            r = merged_module;

        end
        
        function r = merge_to_write(obj, modules)
            
            newrun = obj.merge(modules, 'NEWRUN');
            charge = obj.merge(modules, 'CHARGE');
            output = obj.merge(modules, 'OUTPUT');
            cavity = obj.merge(modules, 'CAVITY');
            solenoid   = obj.merge(modules, 'SOLENOID');
            quadrupole = obj.merge(modules, 'QUADRUPOLE');
            
            %astra = Astra();
            obj.add_modules({newrun, charge, cavity, solenoid, output, ...
                quadrupole});
            %obj.write('ast.in');
            
            [file,path,indx] = uiputfile('ast.in', 'Save as');
            if indx>0
                obj.write(fullfile(path, file));
            else
                obj.write('ast.in');
                disp('NB: Save as ast.in in the current folder!');
            end
        end
        
        %% Set scan parameters
        function r = parameter_scan(obj, varargin)
            % varargin: one or more sequences of [module name, parameter name,
            % from, to, step]
            if length(varargin)>1
                n = length(varargin)
                module_names = varargin(1:5:n)
                para_names = varargin(2:5:n)
                para_froms = varargin(3:5:n)
                para_tos   = varargin(4:5:n)
                para_step2 = varargin(5:5:n)
                
                
            end
        end
        function r = write(obj, filename)
        % Write the output into a file
            if nargin == 0
                filename = 'ast.in';
            end
            % Prepare for output
            r = '';
            for key = obj.modules.keys
                mod = obj.modules(key{1});
                r = sprintf('%s%s', r, mod.output);
            end
            fid = fopen(filename,'wt');
            r = strrep(r, '\', '\\')
            fprintf(fid, r);
            fclose(fid);
        end
    end
end


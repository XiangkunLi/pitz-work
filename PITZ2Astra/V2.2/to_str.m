function [r, varargout] = to_str(val)
    if iscell(val)
        val = val{1};
    end
    if ischar(val) || isstring(val)          
        % if the value is char or string
        str = strrep(val, '''', '');
        r = sprintf('"%s"', str);
    end
    if islogical(val)      
        % if the value is logical
        r = sprintf('%s', string(val));
    end
    if isnumeric(val)       
        % if the value is a number
        if abs(round(val)-val)<1e-9 % integer
            r = sprintf('%d', val);
        else                        % float
            r = sprintf('%.6f', val);
        end
    end
    if nargout > 1
        for k = 1:nargout-1
            varargout{k} = k+1;
        end
    end
    
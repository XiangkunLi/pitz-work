classdef Elements < handle
    % "Elements" is a list of "Element", including everything defined in
    % the Excel file
    properties
        elements;
    end
    
    methods
        %% Read from an Excel file the elements in the beamline
        function obj = Elements(filename, sheetname)
            if nargin > 0 && ~isempty(filename)
                filename = filename;
            else
                %filename = 'PITZbeamlines.xlsx';
                filename = 'PITZ3-Koordinaten_15-10-15beamlines_THz.xlsx';
            end

            if nargin > 1 && ~isempty(sheetname)
                sheetname = sheetname;
            else
                sheetname = 'PITZ3.0-Koordinatenliste (THz)';
            end
            % Check if the file exists. If not, return NULL
            if ~exist(filename, 'file')
                fprintf('NB: file %s does not exists!\n', filename);
                obj = [];
            end
            % The contents are read into three formats: 'num' for numerical, 'txt' for text and 'raw' as what it
            % originally is.
            if isempty(sheetname)
                [num, txt, raw] = xlsread(filename);
            else
                [num, txt, raw] = xlsread(filename, sheetname);
            end
            % Convert raw from cell to array
            raw = string(raw);

            % Get the dimension of the data
            [nrows, ncols] = size(num);
            % fprintf('size: %i %i\n', nrows, ncols);

            j = 0;
            % elements = containers.Map();
            CC = 1;
            for i = 6:nrows
                kv = {};
                if ~ismissing(raw(i,2))
                    j = j+1;
                    name = raw{i,2};
                    kv = [kv {'name', name}];
                elseif ~ismissing(raw(i, 3)) && strcmp(raw(i, 3), 'Gun')
                    kv = [kv {'name', 'Gun'}];
                elseif ~ismissing(raw(i, 3)) && strcmp(raw(i, 3), 'CDS-Booster')
                    kv = [kv {'name', 'CDS-Booster'}];
                else
                    continue;
                end
                    
                if ~ismissing(raw(i, 3))
                    element = raw{i,3};
                    kv = [kv {'element', element}];
                end

                if ~ismissing(raw(i,4))
                    startposition = raw{i,4};
                    kv = [kv {'startposition', str2num(startposition)/1000.0}];
                end
                if ~ismissing(raw(i,5))
                    endposition = raw{i,5};
                    kv = [kv {'endposition', str2num(endposition)/1000.0}];
                end
                if ~ismissing(raw(i,6))
                    middleposition = raw{i,6};
                    kv = [kv {'middleposition', str2num(middleposition)/1000.0}];
                elseif contains(upper(raw(i,2)), ".SCR") && ~ismissing(raw(i+1,6))
                    middleposition = raw{i+1,6};
                    kv = [kv {'middleposition', str2num(middleposition)/1000.0}];
                elseif contains(upper(raw(i,2)), ".ICT") && ~ismissing(raw(i,4)) && ~ismissing(raw(i,5))
                    kv = [kv {'middleposition', (str2num(startposition)+str2num(endposition))/2.0/1000.0}];
                end
                if ~ismissing(raw(i,7))
                    length = raw{i,7};
                    kv = [kv {'length', str2num(length)/1000.0}];
                end
                %disp(kv); %disp('hello');
                names = regexp(kv{2}, '\s*[,;]\s*', 'split');
                if max(size(names))>1
                    len = max(size(kv));
                    for j=1:max(size(names))
                        kvj = ['name', names{j}, {kv{3:len}}];
                        element = Element(kvj{:});
                        %elements(upper(names{j})) = element;
                        
                        elements(CC) = element;
                        CC = CC+1;
                        
                        %elements = [elements {element}];
                        %disp(kvj);
                    end
                else
                    element = Element(kv{:});                  
                    %elements(upper(kv{2})) = element;
                    
                    elements(CC) = element;
                    CC = CC+1;
                        
                    %elements = [elements {element}];
                    %disp(kv);
                end
               
            end
            obj.elements = elements;
        end
        
        %% Get the elements specified by the keyword
        function r = get(obj, keyword)
            % Parameters
            %   keyword: a string representing a group of the elements,
            %   such as 'Quad', 'Scr'
            % Returns
            %   r: a list of names of the elements having the keyword
            
            keyword = upper(keyword);
            % r = containers.Map();
            %keys = obj.elements.keys;
            elements = obj.elements;
            
            CC = 1;
            switch keyword
                case 'GUN'
                    for i = 1:length(elements)
                       element = elements(i);
                       key = element.name;
                       %element = obj.elements(key);
                       s1 = regexp(key, 'GUN');
                       s2 = regexp(key, 'GUN.');
                       if ~isempty(s1) && isempty(s2)
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
                case {'BOOSTER', 'CDS-BOOSTER'}
                    for i = 1:length(elements)
                       element = elements(i);
                       key = element.name;
                       s1 = regexp(key, 'BOOSTER');
                       if ~isempty(s1)
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
                case {'TDS', 'RFD.TDS'}
                    for i = 1:length(elements)
                       element = elements(i);
                       key = element.name;
                       s1 = regexp(key, 'RFD.TDS');
                       if ~isempty(s1)
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
                case {'UND', 'U1'}
                    for i = 1:length(elements)
                       element = elements(i);
                       key = element.name;
                       s1 = regexp(key, 'U1');
                       if ~isempty(s1)
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
                case {'DIPOLE', 'DIPOL'}
                    for i = 1:length(elements)
                       element = elements(i);
                       key = element.name;
                       s1 = regexp(key, '.DIPOL');
                       if ~isempty(s1)
                           r(CC) = element;
                           CC = CC+1;
                       end
                       s1 = regexp(key, '.D1');
                       if ~isempty(s1)
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
                case 'SCR'
                    for i = 1:length(elements)
                       element = elements(i);
                       key = element.name;
                       s1 = regexp(key, '\.SCR');
                       s2 = regexp(key, 'DISP');
                       if ~isempty(s1) && isempty(s2)
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
                case 'QUAD'
                    for i = 1:length(elements)
                       element = elements(i);
                       key = element.name;
                       s1 = regexp(key, '\.Q');
                       s2 = regexp(key, 'DISP');
                       if ~isempty(s1) && isempty(s2)
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
                case 'ST'
                    for i = 1:length(elements)
                       element = elements(i);
                       key = element.name;
                       s1 = regexp(key, '\.ST');
                       s2 = regexp(key, '\.STREAK');
                       if ~isempty(s1) && isempty(s2)
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
                case 'ICT'
                    for i = 1:length(elements)
                       element = elements(i);
                       key = element.name;
                       s1 = regexp(key, '\.ICT');
                       s2 = regexp(key, 'DISP');
                       if ~isempty(s1) && isempty(s2)
                           r(CC) = element;
                           CC = CC+1;
                       end
                   end
                case 'FC'
                   for i = 1:length(elements)
                       element = elements(i);
                       key = element.name;
                       s1 = regexp(key, '\.FC');
                       s2 = regexp(key, 'DISP');
                       if ~isempty(s1) && isempty(s2)
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
                case 'BPM'
                    for i = 1:length(elements)
                       element = elements(i);
                       key = element.name;
                       s1 = regexp(key, '\.BPM');
                       s2 = regexp(key, 'DISP');
                       if ~isempty(s1) && isempty(s2)
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
                otherwise
                    for i = 1:length(elements)
                       element = elements(i);
                       key = element.name;
                       s1 = regexp(key, keyword);
                       if ~isempty(s1)
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
            end
        end
        
        %% Get the names and positions of a group of elements
        function [names, positions] = get_data(obj, keyword, centered)
            % Parameters
            %   keyword: a string representing a group of the elements,
            %   such as 'Quad', 'Scr'
            %   centered: true by default. if true, return the middle
            %   position of the elements; if false, return the starting and
            %   ending positions of the elements
            % Returns
            %   names: a list of names of the elements having the keyword
            %   positions: midddle positions of the elements or starting
            %   and ending positions of the elements
            if nargin>2
                centered = centered;
            else
                centered = true;
            end
            eles = obj.get(keyword);
            %keys = eles.keys;
            pp = []; kk = {};
            for i=1:length(eles)
                key = eles(i).name;
                pos = eles(i).middleposition;
                ff = 0;
                if ~isempty(pos) && centered
                    pp = [pp [pos]];
                    kk = [kk {key}];
                    ff = 1;
                end
                startpos = eles(i).startposition;
                endpos = eles(i).endposition;
                if ~isempty(startpos) && ~isempty(endpos) && ~centered
                    pp = [pp; [startpos endpos]];
                    kk = [kk {key}];
                    ff = 2;
                end
            end
            [positions, index] = sort(pp);
            if ff == 1
                names = kk(index); 
            elseif ff == 2
                names = kk(index(:,1)); 
            end
        end
        
        function [names, bounds] = get_bounds(obj, keyword)
            eles = obj.get(keyword);
            %keys = eles.keys;
            pp = []; kk = {};
            for i=1:length(eles)
                key = eles(i).name;
                pos1 = eles(i).startposition;
                pos2 = eles(i).endposition;
                if ~isempty(pos1) && ~isempty(pos2)
                    pp = [pp; [pos1 pos2]];
                    kk = [kk {key}];
                end
            end
            [~, index] = sort(pp(:,1));
            b1 = pp(:,1);
            b2 = pp(:,2);
            bounds = [b1(index); b2(index)];
            names = kk(index);
            
        end
        
        
        function r = print(obj, keyword)
            eles = obj.get(keyword);
            %keys = eles.keys;
            pp = []; kk = {};
            for i=1:length(eles)
                key = eles(i).name;
                pos = eles(i).middleposition;
                if ~isempty(pos)
                    pp = [pp [pos]];
                    kk = [kk {key}];
                end
            end
            [pp, index] = sort(pp);
            kk = kk(index);
            
            for i=1:length(keys)
                key = kk{i};
                pos = pp(i);
                if ~isempty(pos)
                    %fprintf('%s: %f\n', key, pos);
                    fprintf('%s | %f | m\n', key, pos);
                end
            end
        end
        
        
        function r = plot(obj, varargin)
            % Make a plot of the elements in the list including:
            % 'Quad', 'Scr', 'St', 'ICT', 'FC', 'BPM'
            zend = 20;
            zmid = zend/2;
            ax_to_fig = zend/(zend+2.0);
            ratio = 1.5;

            
            r = figure('name', 'PITZ beamline', 'numbertitle', 'off', ...
                'units', 'centimeter', 'position', [2, 2, zend+2, 13]*ratio, 'color', 'w');
            %r = fig;
            ax1 = subplot(2, 1, 1);
            ax2 = subplot(2, 1, 2);
            set(ax1, 'units', 'centimeter', 'position', [1 7 zend 5]*ratio, 'YTick', []);
            set(ax2, 'units', 'centimeter', 'position', [1 1 zend 5]*ratio, 'YTick', []);

            subplot(ax1)
            axis([0 zmid 0 5*0.75]);
            xticks([0:1:zmid]);
            ax1.YAxis.Visible = 'off';

            subplot(ax2)
            axis([zmid zend 0 5*0.75]);
            xticks([zmid:1:zend]);
            ax2.YAxis.Visible = 'off';
            xlabel('z (m)');
            
            element_list = upper({'Quad', 'Scr', 'St', 'ICT', 'FC', 'BPM'});
            color_list = {'r', 'b', 'g', 'm', 'c', 'k'};
            get_color = containers.Map(element_list, color_list);
            
            varargin = upper(varargin);
            for k = 1:length(varargin)
                [E_name, E_pos] = obj.get_data(varargin{k});
                for i =1:length(E_pos)
                    if (E_pos(i)<=zmid)
                        subplot(ax1);
                    else
                        subplot(ax2);
                    end
                    hold on;
                    ls = '-'; y0 = 0; y1 = 0;
                    if strcmp(upper(varargin{k}), 'FC')
                        ls = ':';
                        y1 = 1;
                    end
                    if strcmp(upper(varargin{k}), 'QUAD') || strcmp(upper(varargin{k}), 'ST')
                        y0 = 1; y1 = 1;
                    end
                    col = get_color(varargin{k});
                    stem(E_pos(i), y0+1.5, 'linestyle', ls, 'color', col, 'Marker', 'none');
                    text(E_pos(i), y1+1.6, E_name{i}, 'color', col, 'FontSize', 8, ...
                        'horizontal', 'left', 'vertical', 'middle', 'rotation', 90);
                end
            end
        end
           

        function r = write_sco(obj, Q_list, varargin)
            
            nargs = length(varargin);
            % Convert keys into upper cases
            keys = upper(varargin(1:2:nargs)); 
            values = varargin(2:2:nargs);
            % Set the key-value pairs to `kv`
            if nargs>0
                kv = containers.Map(keys, values);
            else
                kv = containers.Map();
            end
            
            if isKey(kv, 'Z0')
                z0 = kv('Z0');
            else
                z0 = 5.277;
            end
            
            if isKey(kv, 'HIGHER')
                higher = kv('HIGHER');
            else
                higher = 1.5;
            end
            
            if isKey(kv, 'LOWER')
                lower = kv('LOWER');
            else
                lower = -higher;
            end
            
            if isKey(kv, 'BOUNDS')
                bounds = kv('BOUNDS');
            else
                bounds = {};
                for i = 1:length(Q_list)
                    bounds = [bounds {[lower higher]}];
                end
            end
            
            if isKey(kv, 'ZSTOP')
                zstop = kv('ZSTOP');
            else
                zstop = 18;
            end
            
            if isKey(kv, 'INIT')
                init = kv('INIT');
            else
                init = zeros(1,length(Q_list));
            end
            
            if isKey(kv, 'FILENAME')
                fbeamline = kv('FILENAME');
            else
                fbeamline = 'beamline.txt';
            end
            
            eles = obj.get('quad');
            %keys = eles.keys;
            
            pp = []; kk = {};
            for i=1:length(eles)
                key = eles(i).name;
                pos = eles(i).middleposition;
                if ~isempty(pos)
                    pp = [pp [pos]];
                    kk = [kk {key}];
                end
            end
            [pp, index] = sort(pp);
            kk = kk(index);
            
            nQuads = length(Q_list);
            for k=nQuads:-1:1
                Q_name = Q_list(k);
                select = cellfun(@(x)isequal(x, upper(Q_name)), kk);
                zk = pp(select);
                if zk<z0
                    Q_list(k) = [];
                end
            end
            
            k = 1;
            n = length(Q_list);
            r = [];
            for i = 1:length(kk);
                pos = pp(i)-0.0675/2;
                if pos > z0 && pos < zstop
                    r = [r sprintf('O%12.6f%12.6f%12.6f%12.6f%6d\n', pos-z0, ...
                                0, 0, 0, 0)];
                    if k<=n && strcmp(Q_list(k), kk{i})
                        r = [r sprintf('Q%12.6f%12.6f%12.6f%12.6f%6d\n', 0.0675, ...
                            init(k), bounds{k}(1), bounds{k}(2), 1)];
                        k = k+1;
                    else
                        r = [r sprintf('Q%12.6f%12.6f%12.6f%12.6f%6d\n', 0.0675, ...
                            0, 0, 0, 0)];
                    end
                    z0 = pp(i)+0.0675/2;
                end
            end
            r = [r sprintf('O%12.6f%12.6f%12.6f%12.6f%6d\n', zstop-z0, ...
                0, 0, 0, 0)];
            
            fid = fopen(fbeamline,'wt');
            fprintf(fid, r);
            fclose(fid);
            
        end
        
        
        function r = find_modules(obj)
            eles = obj.elements;
            r = eles;
        end
        

    end
end

function r = split_module1(module)
% Split modules of the same kind into Astra modules
% Parameters
%   module: an Astra Module
% Returns
%   split_modules: a list of Module with the same Module.name
% Example:
% c = Module('cavity', 'LEfile', true, 'File_Efield()', {'gun.txt', 'booster.txt'}, 'C_pos()', {0, 2.675}, 'phi()', {0, 0})
% c = split_module(c);

name = module.name;
keys = module.kv.keys;

for k = 1:length(keys)
    key = keys{k};
    if module.isList(key)
        vals = module.kv(key);
        for i = 1:length(vals)     
            modules(i) = Module(name);
            if ~isempty(module.tag{i})
                modules(i).tag = module.tag{i};
            else
                modules(i).tag = [name, num2str(i)];
            end
        end
    end
end

for k = 1:length(keys)
    key = keys{k};
    if module.isList(key)
        vals = module.kv(key);
        for i = 1:length(vals)
            modules(i).reset(key, vals(i))
        end
    else
        val = module.kv(key);
        for i = 1:length(modules)
            modules(i).reset(key, val);
        end
    end
end

r = modules;
end
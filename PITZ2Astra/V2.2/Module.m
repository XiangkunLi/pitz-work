classdef Module < handle
% Revised on April 4 2019
% Define a `Module` class, which accepts key-value syntax as arguments. 
% This class is the basis for Generator and/or Astra.
% Example:
% To build a NEWRUN module for Astra, call
% -----------------------------------------------------------------------
% newrun = Module('Newrun', 'Run', 1, 'Head', 'PITZ beamline simulation',
%                 'Distribution', 'beam.ini', 'Auto_phase', true, ...
%                 'Track_All', true, 'check_ref_part', false, ...
%                 'Lprompt', false, 'Max_step', 20000);
% 
% cavity = Module('Cavity', 'LEfield', true, 'File_Efield()', ...
% {'gun45cavity.txt', 'CDS14_15mm.txt'},'MaxE()', {60, 12}, 'C_pos()', ...
% {0., 2.675}, 'Nue()', {1.3, 1.3}, 'Phi()', {0, 0});
% -----------------------------------------------------------------------
    properties
    % name  : a string, the name of the module, which is in the Astra
    %         namelist, for example, 'Input', 'Newrun', 'Charge', ...
    % tag   : a string used to differ from the module of the same kind 
    %         if used seperately. For example, an rf gun may have the tag 
    %         of 'GUN' and a booster may have the tag of 'BOOSTER', both 
    %         of which belong to the CAVITY module
    % attributes :
    %         a map, consisting of key-value pairs, which defines the
    %         input parameters for Astra modules, e.g., ('LEfield', TRUE)
    % output: a string, the output to be written in a file as Astra or
    %         Generator inputs
        name = 'SomeModule';
        tag = [];  % 
        counter = 0; % number of instances
        attributes = containers.Map();
        output;
    end
    methods
        %% Constructor
        function obj = Module(name, varargin)
        % Class constructor, which accepts the `name` of the module and 
        % key-value pairs for the mudole. The use of `varargin` allows to 
        % give as many parameters (in format of key-value pairs) as the 
        % user wants.
            if nargin > 0
                obj.name = upper(name);
                if nargin > 1
                    %obj.tag = upper(tag);
                
                    obj.attributes = containers.Map();

                    % Get the key-value pairs from `varargin`
                    nargs = length(varargin);
                    % Convert keys into upper cases
                    keys = varargin(1:2:nargs); 
                    values = varargin(2:2:nargs);
                    % Set the key-value pairs to `attributes`
                    for i = 1:length(keys)
                        key = upper(keys{i});
                        obj.attributes(key) = values{i};
                    end
                    obj.counting();
                    obj.set_tag();
                    % Generating the output string
                    obj.build();
                end
            end
        end
        
        %% Make a copy of the module
        function r = copy(obj)
            r = Module(obj.name, obj.tag);
            r.counter = obj.counter;
            
            attributes = containers.Map();
            keys = obj.attributes.keys;
            for i = 1:length(keys)
                key = keys{i};
                value = obj.attributes(key);
                attributes(key) = value;
            end
            r.attributes = attributes;
      
            r.build();
        end
        
        %% If the object is null or not
        function r = isNull(obj)
            if isempty(obj.attributes)
                r = true;
            else
                r = false;
            end
        end
        
        function r = set_tag(obj, tag)
            if nargin > 1
                obj.tag = tag;  
            else
                obj.counting();
                name = obj.name;
                obj.tag = cell(1, obj.counter);
                for i = 1:obj.counter
                    obj.tag(i) = {[name num2str(i)]};
                end
            end
        end
        
        function r = counting(obj)
            name = obj.name;
            keys = obj.attributes.keys;

            obj.counter = 1;
            for k = 1:length(keys)
                key = keys{k};
                if obj.isList(key)
                    vals = obj.attributes(key);
                    tmp = length(vals);
                    if tmp > obj.counter
                        obj.counter = tmp;
                    end
                end
            end
        end
        
        %% Reset attributes of the Module
        function r = reset(obj, varargin)
            if nargin > 0
                % Get the key-value pairs from `varargin`
                nargs = length(varargin);
                keys = upper(varargin(1:2:nargs));
                values = varargin(2:2:nargs);
                % Set the key-value pairs to `attributes`
                for i = 1:length(keys)
                    obj.attributes(keys{i}) = values{i};
                end
                % Regenerating the output string
                obj.build();
            else
                disp('NB: There is no input argument!\n');
            end
        end        
        
        %% Add a new instance to the Module
        function r = add(obj, varargin)
            if nargin > 0
                %obj.attributes = containers.Map();
                
                % Get the key-value pairs from `varargin`
                nargs = length(varargin);
                keys = upper(varargin(1:2:nargs));
                values = varargin(2:2:nargs);
                % Set the key-value pairs to `attributes`
                for i = 1:length(keys)
                    key = keys{i};
                    if isKey(obj.attributes, key)
                        obj.attributes(upper(key)) = [obj.attributes(key) value];
                    else
                        obj.attributes(upper(key)) = [value];
                    end
                end
                % Regenerating the output string
                obj.build();
            else
                disp('NB: There is no input argument!\n');
            end
        end   
        
        %% Reset attributes of the Module
        function r = reset2(obj, key, value, append)
        % Reset the keyValue pair, e.g., 
        % `reset('Run', 2)` would change 'Run' to 2
        % If `append` is given and is true, then the value will be appended to
        % the original one, e.g.,
        % `reset('File_Efield()', 5, true) will add an additional field data
        % file to File_Efield()
            key = upper(key);
            if nargin<4
                append = false;
            end
            if isKey(obj.attributes, key) && obj.isList(key) && append
                obj.attributes(upper(key)) = [obj.attributes(key) value];
            elseif isKey(obj.attributes, key) && obj.isList(key)
                obj.attributes(upper(key)) = [value];
            else
               obj.attributes(upper(key)) = value;
            end
            % Regenerating the output string
            obj.build();
        end
        
        %% Delete the key
        function r = delete(obj, key)
        % Delete the keyValue pair, e.g., 
            key = upper(key);
            if isKey(obj.attributes, key)
                remove(obj.attributes, key);
            end
            % Regenerating the output string
            obj.build();
        end
        
        %% Edit the parameters of a Module
        function r = edit(obj)
            name = obj.name;
            
            [s, v] = listdlg('PromptString','Select the object to edit:',...
                'SelectionMode','single',...
                'ListString', obj.tag);
            if v
                handles.s = s;
            end
            
            switch upper(name)
                case {'INPUT'}
                    list = obj.get_list(name);
                case {'NEWRUN'}
                    list = obj.get_list(name);
                case {'CHARGE'}
                    list = obj.get_list(name);
                case {'CAVITY'}
                    list = obj.get_list(name);
                case {'SOLENOID'}
                    list = obj.get_list(name);
                case {'QUADRUPOLE'}
                    list = obj.get_list(name);
                case {'OUTPUT'}
                    list = obj.get_list(name);
                case {'Aperture', 'APERTURE'}
                    list = obj.get_list(name);
            end

            fig = figure('name', 'Input the value and press Enter to save', 'numbertitle', 'off', ...
                'units', 'centimeter', 'position', [2, 2, 30, 15]);
            axis off;

            check_width = 30/10.5;
            y0 = 15; disp(length(list));
            for i = 1:length(list)
                x0 = (mod(i-1, 5)+0.05)*check_width*2; 
                if mod(i-1, 5) == 0
                    y0 = y0-1;
                end
                handles.text_list(i) = uicontrol('style', 'text', 'units', 'centimeters',...
                    'position', [x0, y0, check_width, 0.6], ...
                    'string', list{i}, 'fontsize', 10,...
                    'tag', sprintf('%s', list{i}), 'value', 0);
                handles.edit_list(i) = uicontrol('style', 'edit', 'units', 'centimeters',...
                    'position', [x0+check_width, y0, check_width, 0.6], ...
                    'foregroundcol', 'r', ...
                    'fontsize', 10, 'tag', sprintf('%s', list{i}), 'value', 0);
                set(handles.edit_list(i), 'callback', @(src, event)obj.edit_list_Callback(src, event, handles));

                key = upper(list{i});
                if isKey(obj.attributes, key)
                    value = obj.attributes(key);
                    if obj.isList(key)
                        value = value(s);
                    end
                    set(handles.edit_list(i), 'String', to_str(value));
                end
            end
        end
        
        %% Callback function for editting the Module
        function edit_list_Callback(obj, src, event, handles)
            key = upper(get(src, 'Tag'));
            str = get(src, 'String');
            if length(str2num(str))
                val = str2num(str);
            else
                val = str;
            end
            if ~isempty(val)
                if isKey(obj.attributes, key)
                    value = obj.attributes(key); 
                    if obj.isList(key)
                        s = handles.s;
                        value{s} = val;
                    else
                        value = val;
                    end
                else
                    if obj.isList(key)
                        value = {val};
                    else
                        value = val;
                    end
                end
                obj.reset(key, value);
            else
                % obj.delete(key);
            end
        end

        %% Obsolete build function
        function r = build0(obj)
        % Formatting the output of the modules
            r = sprintf('&%s\n', obj.name);
            for k = obj.attributes.keys
                key = k{1};
                val = obj.attributes(key);
                
                if ~iscell(val) 
                % if not a cell list, e.g., LEfield
                    if ischar(val) || isstring(val)          
                    % if the value is char or string
                        str = strrep(val, '''', '');
                        r = sprintf('%s %s="%s"\n', r, key, str);
                    end
                    if islogical(val)      
                    % if the value is logical
                        r = sprintf('%s %s=%s\n', r, key, upper(string(val)));
                    end
                    if isnumeric(val)       
                    % if the value is a number
                        if abs(round(val)-val)<1e-9 % integer
                            r = sprintf('%s %s=%d\n', r, key, val);
                        else                        % float
                            r = sprintf('%s %s=%.6f\n', r, key, val);
                        end
                    end
                else
                % if `val` is a cell list, e.g., File_Efield()
                    for j = 1:length(val)
                        if ischar(val{j}) || isstring(val{j})  
                        % if the value is char or string
                            str = strrep(val{j}, '''', '');
                            r = sprintf('%s %s(%d)="%s"\n', r, key, j, str);
                        end
                        if islogical(val)    
                        % if the value is logical
                            r = sprintf('%s %s(%d)=%s\n', r, key, j, upper(string(val)));
                        end
                        if isnumeric(val{j}) 
                        % if the value is a number
                            if abs(round(val{j})-val{j})<1e-6 % integer
                                r = sprintf('%s %s(%d)=%d\n', r, key, j, val{j});
                            else                              % float
                                r = sprintf('%s %s(%d)=%.6f\n', r, key, j, val{j});
                            end
                        end
                    end
                end
            end
            r = sprintf('%s&\n\n', r);
            obj.output = r;
        end
        
        %% If a key is a list type
        function r = isList(obj, key)
            startIndex = regexp(key, '\(\)');
            if isempty(startIndex)
                r = false;
            else
                r = true;
            end
        end
        
        %% Prepare for the output to be written to a file
        function r = build(obj)
        % Formatting the output of the modules
            r = sprintf('&%s\n', obj.name);
            for k = obj.attributes.keys
                key = k{1};
                val = obj.attributes(key);
                
                startIndex = regexp(key, '\(\)');
                
                if isempty(startIndex) %~iscell(val) 
                % if not a cell list, e.g., LEfield
                    if ischar(val) || isstring(val)          
                    % if the value is char or string
                        str = strrep(val, '''', '');
                        r = sprintf('%s %s="%s"\n', r, key, str);
                    end
                    if islogical(val)      
                    % if the value is logical
                        r = sprintf('%s %s=%s\n', r, key, upper(string(val)));
                    end
                    if isnumeric(val)       
                    % if the value is a number
                        if abs(round(val)-val)<1e-9 % integer
                            r = sprintf('%s %s=%d\n', r, key, val);
                        else                        % float
                            r = sprintf('%s %s=%.6f\n', r, key, val);
                        end
                    end
                else
                % if `val` is a cell list, e.g., File_Efield()
                    key = key(1:startIndex-1);
                    for j = 1:length(val)
                        if ischar(val{j}) || isstring(val{j})  
                        % if the value is char or string
                            str = strrep(val{j}, '''', '');
                            r = sprintf('%s %s(%d)="%s"\n', r, key, j, str);
                        end
                        if islogical(val)    
                        % if the value is logical
                            r = sprintf('%s %s(%d)=%s\n', r, key, j, upper(string(val)));
                        end
                        if isnumeric(val{j}) 
                        % if the value is a number
                            if abs(round(val{j})-val{j})<1e-6 % integer
                                r = sprintf('%s %s(%d)=%d\n', r, key, j, val{j});
                            else                              % float
                                r = sprintf('%s %s(%d)=%.6f\n', r, key, j, val{j});
                            end
                        end
                    end
                end
            end
            r = sprintf('%s&\n\n', r);
            obj.output = r;
        end        
        
        %% Split the merged module into a list of modules
        function r = split(obj, module)
            
            if nargin>1
                module = module;
            else
                module = obj;
            end
            
            name = module.name;
            keys = module.attributes.keys;

            counter = module.counter;
            for i = 1:counter     
                modules(i) = Module(name);
                if ~isempty(module.tag)
                    modules(i).tag = module.tag{i};
                else
                    modules(i).tag = [name, num2str(i)];
                end
            end

            for k = 1:length(keys)
                key = keys{k};
                if module.isList(key)
                    vals = module.attributes(key);
                    for i = 1:length(vals)
                        modules(i).reset(key, vals(i))
                    end
                else
                    val = module.attributes(key);
                    for i = 1:length(modules)
                        modules(i).reset(key, val);
                    end
                end
            end
            % disp(modules);
            if ~isempty(keys)
                r = modules;
            else
                r = [];
            end
        end
        
        %% Write the module to a file
        function r = write(obj, filename)
        % Write the output into a file
            fid = fopen(filename,'wt');
            fprintf(fid, obj.output);
            fclose(fid);
        end
        
        %% Get the list of parameters for the given Module name
        function r = get_list(obj, name, varargin)
            disp(nargin);
            input_list = {'Fname', 'Add', 'N_add', 'Ipart', 'Species', ...
                'ion_mass', 'Probe', 'Passive', 'Noise_reduc', 'Cathode', ...
                'R_Cathode', 'High_res', 'Binary', 'Q_total', 'Type', 'Rad', ...
                'Tau', 'Ref_zpos', 'Ref_clock', 'Ref_Ekin', 'Dist_z', 'sig_z', ...
                'C_sig_z', 'Lz', 'rz', 'sig_clock', 'C_sig_clcok', 'Lt', 'rt', ...
                'Dist_pz', 'sig_Ekin', 'C_sig_Ekin', 'LE', 'rE', 'emit_z', ...
                'cor_Ekin', 'E_photon', 'phi_eff', 'Dist_x', 'sig_x', ...
                'C_sig_x', 'Lx', 'rx', 'x_off', 'Disp_x', 'Dist_px', 'Nemit_x', ...
                'sig_px', 'C_sig_px', 'Lpx', 'rpx', 'cor_px', 'Dist_y', 'sig_y', ...
                'C_sig_y', 'Ly', 'ry', 'y_off', 'Disp_y', 'Dist_py', 'Nemit_y', ...
                'sig_py', 'C_sig_py', 'Lpy', 'rpy', 'cor_py'};
            newrun_list = {'LOOP', 'Head', 'RUN', 'Distribution', ...
                'ion_mass()', 'N_red', 'Xoff', 'Yoff', 'xp', 'yp', 'Zoff', ...
                'Toff', 'Xrms', 'Yrms', 'Zrms', 'Trms', 'Tau', 'cor_py', ...
                'cor_py', 'Qbunch', 'SRT_Q_Schottky', 'Q_Schottky', 'debunch', ...
                'Track_All', 'Track_On_Axis', 'Auto_Phase', 'Phase_Scan', ...
                'check_ref_part', 'L_rm_back', 'Z_min', 'Z_Cathode', ...
                'H_max', 'H_min', 'Max_step', 'Lmonitor', 'Lprompt'};
            output_list = {'ZSTART', 'ZSTOP', 'Zemit', 'Zphase', 'Screen()', ...
                'Scr_xrot()', 'Scr_yrot()', 'Step_width', 'Step_max', ...
                'Lproject_emit', 'Local_emit', 'Lmagnetized', 'Lsub_rot', ...
                'Lsub_Larmor', 'Rot_ang', 'Lsub_cor', 'RefS', 'EmitS', ...
                'C_EmitS', 'C99_EmitS', 'Tr_EmitS', 'Sub_EmitS', 'Cross_start', ...
                'Cross_end', 'PhaseS', 'T_PhaseS', 'High_res', 'Binary', ...
                'TrackS', 'TcheckS', 'SigmaS', 'CathodeS', 'LandFS', 'LarmorS'};
            charge_list = {'LOOP', 'LSPCH', 'LSPCH3D', 'L2D_3D', 'Lmirror', ...
                'L_Curved_Cathode', 'Cathode_Contour', 'R_zero', 'Nrad', ...
                'Cell_var', 'Nlong_in', 'N_min', 'min_grid', 'Merge_1', ...
                'Merge_2', 'Merge_3', 'Merge_4', 'Merge_5', 'Merge_6', ...
                'Merge_7', 'Merge_8', 'Merge_9', 'Merge_10', 'z_trans', ...
                'min_grid_trans', 'Nxf', 'Nx0', 'Nyf', 'Ny0', 'Nzf', 'Nz0', ...
                'Smooth_x', 'Smooth_y', 'Smooth_z', 'Max_scale', 'Max_count', ...
                'Exp_control'};
            aperture_list = {'LOOP', 'LApert', 'File_Aperture()', 'Ap_Z1()', ...
                'Ap_Z2()', 'Ap_R()', 'Ap_GR', 'A_pos()', 'A_xoff()', 'A_yoff()', ...
                'A_xrot()', 'A_yrot()', 'A_zrot()', 'SE_d0()', 'SE_Epm()', ...
                'SE_fs()', 'SE_Tau()', 'SE_Esc()', 'SE_ff1()', 'SE_ff2()', ...
                'Max_Secondary', 'LClean_Stack'};
            cavity_list = {'LOOP', 'LEfield', 'File_Efield()', 'C_noscale()', ...
                'C_smooth()', 'Com_grid()', 'C_higher_order()', 'Nue()', ...
                'K_wave()', 'MaxE()', 'Ex_stat()', 'Ey_stat()', 'Bx_stat()', ...
                'By_stat()', 'Bz_stat()', 'Flatness()', 'Phi()', 'C_pos()', ...
                'C_numb()', 'T_dependence()', 'T_null()', 'C_Tau()', 'E_stored()', ...
                'C_xoff()', 'C_yoff()', 'C_xrot()', 'C_yrot()', 'C_zrot()', ...
                'C_zkickmin()', 'C_zkickmax()', 'C_Xkick()', 'C_Ykick()', ...
                'File_A0()', 'P_Z1()', 'P_R1()', 'P_Z2()', 'P_R2', 'P_n', 'E_a0', ...
                'E_Z0', 'E_sig', 'E_sigz', 'E_Zr', 'E_Eps', 'E_lam', 'zeta'};
            solenoid_list = {'LOOP', 'LBfield', 'File_Bfield()', 'S_noscale()', ...
                'S_smooth()', 'S_higher_order()', 'MaxB()', 'S_pos()', 'S_xoff()', ...
                'S_yoff()', 'S_xrot()', 'S_yrot()'};
            quadrupole_list = {'LOOP', 'Lquad', 'Q_type()', 'Q_grad()', ...
                'Q_noscale()', 'Q_length()', 'Q_smooth()', 'Q_bore()', 'Q_dist()', ...
                'Q_mult_a', 'Q_mult_b', 'Q_pos()', 'Q_xoff()', 'Q_yoff()', ...
                'Q_zoff()', 'Q_xrot()', 'Q_yrot()', 'Q_zrot()', 'Q_K()'};

            if nargin == 2
                switch name
                    case {'newrun', 'Newrun', 'NEWRUN'}
                        r = sort(newrun_list);
                    case {'input', 'Input', 'INPUT'}
                        r = sort(input_list); 
                    case {'charge', 'Charge', 'CHARGE'}
                        r = sort(charge_list);
                    case {'cavity', 'Cavity', 'CAVITY'}
                        r = sort(cavity_list);
                    case {'solenoid', 'Solenoid', 'SOLENOID'}
                        r = sort(solenoid_list);
                    case {'quadrupole', 'Quadrupole', 'QUADRUPOLE'}
                        r = sort(quadrupole_list);
                    case {'output', 'Output', 'OUTPUT'}
                        r = sort(output_list);
                    case {'aperture', 'Aperture', 'APERTURE'}
                        r = sort(aperture_list);
                end
                [~,idx]=sort(upper(r));
                r = r(idx);
            elseif nargin>2 && strcmp(varargin, 'upper')
                switch upper(name)
                    case {'NEWRUN'}
                        r = upper(newrun_list);
                    case {'INPUT'}
                        r = upper(input_list); 
                    case {'CHARGE'}
                        r = upper(charge_list);
                    case {'CAVITY'}
                        r = upper(cavity_list);
                    case {'SOLENOID'}
                        r = upper(solenoid_list);
                    case {'QUADRUPOLE'}
                        r = upper(quadrupole_list);
                    case {'OUTPUT'}
                        r = upper(output_list);
                    case {'APERTURE'}
                        r = upper(aperture_list);
                end
            end
        end
    end
end


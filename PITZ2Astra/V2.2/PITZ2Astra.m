function varargout = PITZ2Astra(varargin)
% PITZ2ASTRA MATLAB code for PITZ2Astra.fig
%      Version 1.1, by Xiangkun Li, Nov 1 2018
%      PITZ2ASTRA, by itself, creates a new PITZ2ASTRA or raises the existing
%      singleton*.
%
%      H = PITZ2ASTRA returns the handle to a new PITZ2ASTRA or the handle to
%      the existing singleton*.
%
%      PITZ2ASTRA('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PITZ2ASTRA.M with the given input arguments.
%
%      PITZ2ASTRA('Property','Value',...) creates a new PITZ2ASTRA or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before PITZ2Astra_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to PITZ2Astra_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help PITZ2Astra

% Last Modified by GUIDE v2.5 11-Jan-2019 12:25:15

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @PITZ2Astra_OpeningFcn, ...
                   'gui_OutputFcn',  @PITZ2Astra_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before PITZ2Astra is made visible.
function PITZ2Astra_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to PITZ2Astra (see VARARGIN)


% handles.gen_name = 'gen.in';
% handles.ast_name = 'ast.in';

handles.default_beamline = 'PITZbeamlines.xlsx';

handles.field_path = [];
handles.write_path = [];

handles.pbeamline = [];
handles.z0 = 5.277;
handles.sbeamline = [];
handles.soptimized = [];

%[gen, ast] = init_modules();
%handles.generator = gen;
%handles.astra = ast;
handles.elements = Elements;
handles.modules = handles.elements.to_modules();

names = {handles.modules.tag};
[name1 name2] = handles.elements.sort(names);
set(handles.AvailableList, 'String', [name2 name1]);

handles.astra = Astra;
handles.generator = [];

handles.current_modules = [];

handles.ast_name = [];
handles.gen_name = [];

handles.cwd = pwd;

%set(handles.ChoosePara_Popup, 'String', get_list('Input'));

check_list2_Callback(handles);
%EditElement_Popupmenu_Callback(handles.CreateModule_Popup, eventdata, handles);

% Show position on the plot
callback = ['pt = get(gca,''CurrentPoint'');' ...
    'xbox = findobj(''Tag'',''PositionAt_Edit'');' ...
    'set(xbox,''String'',[num2str(pt(1,1))]);'];
set(gcf,'WindowButtonMotionFcn',callback);

% Choose default command line output for PITZ2Astra
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes PITZ2Astra wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = PITZ2Astra_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in SaveAstra_Pushbutton.
function SaveAstra_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to SaveAstra_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% [file,path,indx] = uiputfile('ast.in');
% if indx>0
%     handles.ast_name = fullfile(path, file);
%     handles.astra.write(fullfile(path, file));
% else
%     disp('NB: Please choose a file first!');
% end
handles.astra.merge_to_write(handles.current_modules);
guidata(hObject, handles);

% gen_name = get(handles.gen_name, 'String');
% ast_name = get(handles.ast_name, 'String');
% 
% handles.generator.write(fullfile(handles.write_path, gen_name));
% handles.astra.write(fullfile(handles.write_path, ast_name));
% 
% old_str = get(handles.AvailableList, 'String');
% new_str = ['Generating output successfully!'];
% set(handles.AvailableList, 'String', strvcat(old_str, new_str));

% --- Executes on button press in LoadPITZ_Pushbutton.
function LoadPITZ_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to LoadPITZ_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% [file,path] = uigetfile({'*.xlsx'; '*.xls'; '*.*'}, 'Select the PITZ beamline', handles.cwd);
% 
% if isequal(file, 0)
%     disp(['User selected Cancel']);
% else
%     disp(['User selected: ', fullfile(path,file)]);
%     
%     handles.cwd = path;
%     handles.pbeamline = fullfile(path,file);
%     
%     handles.eles = Elements(handles.pbeamline);
%     handles.elements = Elements(handles.pbeamline);
%     handles.modules = handles.elements.to_modules();
%     handles.astra = Astra(handles.modules);
%     
%     names = {handles.modules.tag};
%     [name1 name2] = handles.elements.sort(names);
%     
%     set(handles.AvailableList, 'String', [name2 name1]);
%     %set(handles.AlreadyHaveList, 'String', [name2]);
%     disp('hi');
%     
% end

handles.elements = Elements;
handles.modules = handles.elements.to_modules();
handles.astra = Astra;

names = {handles.modules.tag};
[name1 name2] = handles.elements.sort(names);

set(handles.AvailableList, 'String', [name2 name1]);
    
guidata(hObject, handles);

% --- Executes on button press in LoadSCO_Pushbutton.
function LoadSCO_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to LoadSCO_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
z0 = get_position('Please give the position of the beamline entrance first!', handles);

handles.z0 = z0;
set(handles.EditZ0, 'string', num2str(z0));

[file,path] = uigetfile({'*.txt'; '*.dat'; '*.*'}, ...
    'Select the beamline used in Space Charge Optimizer', handles.cwd);

if ~isequal(file, 0)
    handles.cwd = path;
    handles.sbeamline = fullfile(path,file);
    disp(['User selected: ', handles.sbeamline]);
    
    [file,path] = uigetfile({'*.dat';'*.txt';'*.*'}, ...
        'Select the optimization result', handles.cwd);
    if ~isequal(file, 0)
        handles.cwd = path;
        handles.soptimized = fullfile(path,file);
        disp(['User selected: ', handles.soptimized]);
    else
        disp('NB: the default values in the SCO input will be used!');
    end
    
    [QuadPos, QuadGrad] = read_sco(z0, handles.sbeamline, handles.soptimized);
    QuadType = cell(size(QuadPos));
    %QuadType(:) = {'Q3.data'};
    QuadType(:) = {fullfile(handles.field_path,'Q3.data')};
    quadrupole = Module('Quadrupole', 'Lquad', true, 'Q_pos()', num2cell(QuadPos), ...
        'Q_grad()', num2cell(QuadGrad), 'Q_type()', QuadType);
    
    %% Find the names of quadrupoles
    tag = quadrupole.tag;
    for i = 1:length(QuadPos)
        zpos = QuadPos(i);
        found = handles.elements.find_element(zpos);
        if ~isempty(found)
            tag(i) = found(1);
        end
    end
    quadrupole.set_tag(tag);
    %%
    modules = handles.astra.split(quadrupole);
    for i = 1:length(modules)
        module = modules(i);
        [~, idy] = handles.astra.filter(handles.current_modules, {module.tag});
        if ~isempty(idy)
            handles.current_modules(idy) = module;
        else
            handles.current_modules = [handles.current_modules module];
        end
    end
    
    set(handles.AlreadyHaveList, 'String', {handles.current_modules.tag});
    %handles.astra.add_module(quadrupole);
    
end

guidata(hObject, handles);

% --- Executes on button press in LoadOPT_Pushbutton.
function LoadOPT_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to LoadOPT_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[file,path] = uigetfile({'*.dat';'*.txt';'*.*'}, ...
    'Select the optimization result', handles.cwd);
old_str = get(handles.AvailableList, 'String');
if isequal(file, 0)
    new_str = 'User selected Cancel';
else
    new_str = ['User selected: ', fullfile(path,file)];
    handles.cwd = path;
    
    handles.soptimized = fullfile(path,file);
    [QuadPos, QuadGrad] = read_sco(handles.z0, handles.sbeamline, handles.soptimized);
    QuadType = cell(size(QuadPos));
    %QuadType(:) = {'Q3.data'};
    QuadType(:) = {fullfile(handles.field_path,'Q3.data')};
    quadrupole = Module('Quadrupole', 'Lquad', true, 'Q_pos()', num2cell(QuadPos), ...
        'Q_grad()', num2cell(QuadGrad), 'Q_type()', QuadType);
    tag = quadrupole.tag;
    for i = 1:length(QuadPos)
        zpos = QuadPos(i);
        found = handles.elements.find_element(zpos);
        if ~isempty(found)
            tag(i) = found(1);
        end
    end
    quadrupole.set_tag(tag);
    
    modules = handles.astra.split(quadrupole);
    for i = 1:length(modules)
        module = modules(i);
        [~, idy] = handles.astra.filter(handles.current_modules, {module.tag});
        if ~isempty(idy)
            handles.current_modules(idy) = module;
        else
            handles.current_modules = [handles.current_modules module];
        end
    end
    
    set(handles.AlreadyHaveList, 'String', {handles.current_modules.tag});
end
guidata(hObject, handles);

function EventList_Callback(hObject, eventdata, handles)
% hObject    handle to EventList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EventList as text
%        str2double(get(hObject,'String')) returns contents of EventList as a double


% --- Executes during object creation, after setting all properties.
function EventList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EventList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in AvailableList.
function AvailableList_Callback(hObject, eventdata, handles)
% hObject    handle to AvailableList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns AvailableList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from AvailableList
if strcmp(get(gcf,'selectiontype'),'open')  
  % here you write write code, which you wanna be executed afer double-click
  str = get(hObject, 'String');
  idx = get(hObject, 'Value');
  val = str(idx);
  disp(['NB: double clicked ' val]);
  
  module = handles.astra.filter(handles.modules, val);
  module.edit();
end
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function AvailableList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to AvailableList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in FieldMaps_Pushbutton.
function FieldMaps_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to FieldMaps_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
folder = uigetdir(handles.cwd);

old_str = get(handles.AvailableList, 'String');
if isequal(folder, 0)
    new_str = 'User selected Cancel';
else
    handles.field_path = folder;
    handles.cwd = folder;
    
    names = handles.astra.modules.keys; 
    for i = 1:length(names)
        module_name = names{i};
        module = handles.astra.modules(module_name);
        keys = module.kv.keys;
        for j = 1:length(keys)
            key = keys{j};
            if contains(key, {'FILE' 'Q_TYPE'})
                value = module.kv(key);
                for k = 1:length(value)
                    val = value{k};
                    [pathstr, name, ext] = fileparts(val);
                    value{k} = fullfile(folder, [name ext]);
                end
                handles.astra.update_parameter(module_name, key, value);
            end
        end
    end
    
    new_str = ['User selected: ', folder];
end
set(handles.AvailableList, 'String', strvcat(old_str, new_str));
ChoosePara_Popup_Callback(handles.ChoosePara_Popup, eventdata, handles);
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function EditZ0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditZ0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function EditZ0_Callback(hObject, eventdata, handles)
% hObject    handle to EditZ0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EditZ0 as text
%        str2double(get(hObject,'String')) returns contents of EditZ0 as a double

z0 = get(handles.EditZ0, 'String');
if ~isempty(str2num(z0))
    handles.z0 = str2num(z0);

    old_str = get(handles.AvailableList, 'String');
    new_str = ['Set SCO.Z0 to: ', z0];

    set(handles.AvailableList, 'String', strvcat(old_str, new_str));
    set(handles.EditZ0, 'String', z0);
else
    set(handles.EditZ0, 'String', []);
end
guidata(hObject, handles);



function gen_name_Callback(hObject, eventdata, handles)
% hObject    handle to gen_name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of gen_name as text
%        str2double(get(hObject,'String')) returns contents of gen_name as a double
a = get(handles.gen_name, 'String');

old_str = get(handles.AvailableList, 'String');
new_str = ['Set name to: ', a];

set(handles.AvailableList, 'String', strvcat(old_str, new_str));
set(handles.gen_name, 'String', a);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function gen_name_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gen_name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ast_name_Callback(hObject, eventdata, handles)
% hObject    handle to ast_name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ast_name as text
%        str2double(get(hObject,'String')) returns contents of ast_name as a double
a = get(handles.ast_name, 'String');

old_str = get(handles.AvailableList, 'String');
new_str = ['Set name to: ', a];

set(handles.AvailableList, 'String', strvcat(old_str, new_str));
set(handles.ast_name, 'String', a);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function ast_name_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ast_name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ChoosePath_Pushbutton.
function ChoosePath_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to ChoosePath_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
folder = uigetdir(handles.cwd);

old_str = get(handles.AvailableList, 'String');
if isequal(folder, 0)
    new_str = 'User selected Cancel';
else
    new_str = ['User selected path: ', folder];
    handles.cwd = folder;
end
set(handles.AvailableList, 'String', strvcat(old_str, new_str));
handles.write_path = folder;

%gen_name = get(handles.gen_name, 'String');
%set(handles.gen_name, 'String', fullfile(folder, gen_name));
%ast_name = get(handles.ast_name, 'String');
%set(handles.ast_name, 'String', fullfile(folder, ast_name));
%disp(fullfile(folder, ast_name));

guidata(hObject, handles);


% --- Executes when uipanel1 is resized.
function uipanel1_SizeChangedFcn(hObject, eventdata, handles)
% hObject    handle to uipanel1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in ParasList.
function ParasList_Callback(hObject, eventdata, handles)
% hObject    handle to ParasList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ParasList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ParasList


% --- Executes during object creation, after setting all properties.
function ParasList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ParasList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ChooseModule_Popup.
function ChooseModule_Popup_Callback(hObject, eventdata, handles)
% hObject    handle to ChooseModule_Popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ChooseModule_Popup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ChooseModule_Popup

str = get(hObject, 'String');
val = get(hObject, 'Value');
module_name = str{val};
set(handles.ChoosePara_Popup, 'Value', 1);
switch module_name
    case 'Input'
        set(handles.ChoosePara_Popup, 'String', get_list(module_name));
    case 'Newrun'
        set(handles.ChoosePara_Popup, 'String', get_list(module_name));
    case 'Charge'
        set(handles.ChoosePara_Popup, 'String', get_list(module_name));
    case 'Cavity'
        set(handles.ChoosePara_Popup, 'String', get_list(module_name));
    case 'Solenoid'
        set(handles.ChoosePara_Popup, 'String', get_list(module_name));
    case 'Quadrupole'
        set(handles.ChoosePara_Popup, 'String', get_list(module_name));
    case 'Output'
        set(handles.ChoosePara_Popup, 'String', get_list(module_name));
    case 'Aperture'
        set(handles.ChoosePara_Popup, 'String', get_list(module_name));
end
set(handles.ValueEdit, 'String', []);


% --- Executes during object creation, after setting all properties.
function ChooseModule_Popup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ChooseModule_Popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ChoosePara_Popup.
function ChoosePara_Popup_Callback(hObject, eventdata, handles)
% hObject    handle to ChoosePara_Popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ChoosePara_Popup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ChoosePara_Popup

str = get(handles.ChooseModule_Popup, 'String');
val = get(handles.ChooseModule_Popup, 'Value');
module_name = upper(str{val});

str = get(hObject, 'String');
val = get(hObject, 'Value');
parameter = upper(str{val});

if isKey(handles.astra.modules, module_name)
    mod = handles.astra.modules(module_name);
    if isKey(mod.kv, parameter)
        value = mod.kv(parameter);
    else
        value = [];
    end
elseif isKey(handles.generator.modules, module_name)
    mod = handles.generator.modules(module_name);
    if isKey(mod.kv, parameter)
        value = mod.kv(parameter);
    else
        value = [];
    end
else
    value = [];
end

set(handles.ValueEdit, 'String', string(value));

% --- Executes during object creation, after setting all properties.
function ChoosePara_Popup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ChoosePara_Popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on key press with focus on ChooseModule_Popup and none of its controls.
function ChooseModule_Popup_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to ChooseModule_Popup (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)



function ValueEdit_Callback(hObject, eventdata, handles)
% hObject    handle to ValueEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ValueEdit as text
%        str2double(get(hObject,'String')) returns contents of ValueEdit as a double


% --- Executes during object creation, after setting all properties.
function ValueEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ValueEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in UpdatePara_Pushbutton.
function UpdatePara_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to UpdatePara_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
old_str = get(handles.ParasList, 'String');

str = get(handles.ChoosePara_Popup, 'String');
val = get(handles.ChoosePara_Popup, 'Value');
key = str{val};

str = get(handles.ChooseModule_Popup, 'String');
val = get(handles.ChooseModule_Popup, 'Value');
module_name = upper(str{val});

value = get(handles.ValueEdit, 'String')
if isempty(value)
    disp('Please enter Value before clicking Add button!');
else
    startIndex = regexp(key, '\(\)'); 
    if isempty(startIndex) % The parameter is not a list
        if length(str2num(value))
            value = str2num(value);
            new_str = sprintf('%s = %f', key, value);
        else
            new_str = sprintf('%s = %s', key, value);
        end
    else            % If a list is expected for the parameter
        % Convert from nx1 array to 1xn cell
        dim = size(value);
        if ~iscell(value)
            value = mat2cell(value, ones(dim(1),1));
        end
        %value = transpose(mat2cell(value, ones(dim(1),1)));
        
        new_str = sprintf('%s = (', key);
        if length(str2num(value{1}))
            for i = 1:length(value)
                value{i} = str2num(value{i});
                new_str = sprintf('%s%f, ', new_str, value{i})
            end
        else
            for i = 1:length(value)
                value{i} = strtrim(value{i});
                new_str = sprintf('%s%s, ', new_str, value{i})
            end
        end
        new_str = sprintf('%s)', new_str)
    end
    set(handles.ParasList, 'String', strvcat(old_str, new_str));
end

if isKey(handles.astra.modules, module_name)
    handles.astra.update_parameter(module_name, key, value);
elseif isKey(handles.generator.modules, module_name)
    handles.generator.update_parameter(module_name, key, value);
end

guidata(hObject, handles);


% --- Executes on button press in DeletePara_Pushbutton.
function DeletePara_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to DeletePara_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

old_str = get(handles.ParasList, 'String');

str = get(handles.ChoosePara_Popup, 'String');
val = get(handles.ChoosePara_Popup, 'Value');
key = str{val};

str = get(handles.ChooseModule_Popup, 'String');
val = get(handles.ChooseModule_Popup, 'Value');
module_name = upper(str{val});

value = get(handles.ValueEdit, 'String')
if isempty(value)
    disp('This parameter is already empty!');
else
    if isKey(handles.astra.modules, module_name)
        handles.astra.delete_parameter(module_name, key);
    elseif isKey(handles.generator.modules, module_name)
        handles.generator.delete_parameter(module_name, key);
    end
    set(handles.ValueEdit, 'String', []);
end

guidata(hObject, handles);



function ImainEdit_Callback(hObject, eventdata, handles)
% hObject    handle to ImainEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ImainEdit as text
%        str2double(get(hObject,'String')) returns contents of ImainEdit as a double


% --- Executes during object creation, after setting all properties.
function ImainEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ImainEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Imain2MaxB_Pushbutton.
function Imain2MaxB_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to Imain2MaxB_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
var = get(handles.ImainEdit, 'String');
Imain = str2num(var);
if ~isempty(Imain)
    MaxB = -(0.0000372+0.000588*Imain);
    set(handles.MaxBEdit, 'String', num2str(MaxB));
    
else
    disp('Please input the current first.');
end
guidata(hObject, handles);


% --- Executes on button press in Help_Pushbutton.
function Help_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to Help_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
winopen('PITZ2Astra.pdf');

% --- Executes on button press in ViewGen_Pushbutton.
function ViewGen_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to ViewGen_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% filename = get(handles.gen_name, 'String');
[file,path] = uigetfile({'*.in'; '*.*'}, 'Select the Astra file', handles.cwd);
if ~isequal(file, 0)
    winopen(fullfile(path, file));
end

% --- Executes on button press in ViewAst_Pushbutton.
function ViewAst_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to ViewAst_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% filename = get(handles.ast_name, 'String')
[file,path] = uigetfile({'*.in'; '*.*'}, 'Select the Generator file', handles.cwd);
if ~isequal(file, 0)
    winopen(fullfile(path, file));
end


% --- Executes on button press in Reset_Pushbutton.
function Reset_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to Reset_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.field_path = [];
handles.write_path = [];

handles.pbeamline = [];

handles.sbeamline = [];
handles.soptimized = [];

[gen, ast] = init_modules();
handles.generator = gen;
handles.astra = ast;

handles.cwd = [];

handles.z0 = 5.277;
set(handles.EditZ0, 'String', '5.277');

set(handles.gen_name, 'String', 'gen.in');
set(handles.ast_name, 'String', 'ast.in');

set(handles.ChoosePara_Popup, 'String', get_list('Input'));
set(handles.ChoosePara_Popup, 'Value', 1);

set(handles.AvailableList, 'String', 'List of Events:');

set(handles.ParasList, 'String', []);
set(handles.ValueEdit, 'String', []);
set(handles.ImainEdit, 'String', []);

% Choose default command line output for PITZ2Astra
handles.output = hObject;

ChoosePara_Popup_Callback(handles.ChoosePara_Popup, eventdata, handles);
% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in LoadGenerator_Pushbutton.
function LoadGenerator_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to LoadGenerator_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[file,path] = uigetfile('*.in', 'Select the Generator input file', handles.cwd);

if ~isequal(file, 0)
    new_str = ['User selected: ', fullfile(path,file)];
    handles.cwd = path;

    handles.generator = Astra;
    handles.generator.read(fullfile(path,file));
end
guidata(hObject, handles);


% --- Executes on button press in LoadAstra_Pushbutton.
function LoadAstra_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to LoadAstra_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[file,path] = uigetfile('*.in', 'Select the Astra input file', handles.cwd);
old_str = get(handles.AvailableList, 'String');
if isequal(file, 0)
    disp('User selected Cancel');
else
    disp(['User selected: ', fullfile(path,file)]);
    handles.cwd = path;
end
astfile = fullfile(path,file);
handles.astra = Astra;
handles.astra.read(astfile);

%% Find the names of quads
quadrupole = handles.astra.modules('QUADRUPOLE');
if isKey(quadrupole.kv, 'Q_POS()')
    QuadPos = quadrupole.kv('Q_POS()');
    tag = quadrupole.tag;
    for i = 1:length(QuadPos)
        zpos = QuadPos{i};
        found = handles.elements.find_element(zpos);
        if ~isempty(found)
            tag(i) = found(1);
        end
    end
    quadrupole.set_tag(tag);
end

%% Find names of screens
output = handles.astra.modules('OUTPUT');
if isKey(output.kv, 'SCREEN()')
    ScrPos = output.kv('SCREEN()');
    tag = output.tag;
    for i = 1:length(ScrPos)
        zpos = ScrPos{i};
        found = handles.elements.find_element(zpos);
        if ~isempty(found)
            tag(i) = found(1);
        end
    end
    output.set_tag(tag);
end
%%
handles.current_modules = handles.astra.to_modules();
names = {handles.current_modules.tag};
%[name1 name2] = handles.elements.sort(names);

%set(handles.AvailableList, 'String', [name1]);
set(handles.AlreadyHaveList, 'String', [names]);
%end
guidata(hObject, handles);

% --- Executes on button press in AppendPara_Pushbutton.
function AppendPara_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to AppendPara_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

old_str = get(handles.ParasList, 'String');

str = get(handles.ChoosePara_Popup, 'String');
val = get(handles.ChoosePara_Popup, 'Value');
key = str{val};

str = get(handles.ChooseModule_Popup, 'String');
val = get(handles.ChooseModule_Popup, 'Value');
module_name = upper(str{val});

value = get(handles.ValueEdit, 'String')
if isempty(value)
    disp('Please enter Value before clicking Add button!');
else
    startIndex = regexp(key, '\(\)'); 
    if isempty(startIndex) % The parameter is not a list
        if length(str2num(value))
            value = str2num(value);
            new_str = sprintf('%s = %f', key, value);
        else
            new_str = sprintf('%s = %s', key, value);
        end
    else            % If a list is expected for the parameter
        % Convert from nx1 array to 1xn cell
        dim = size(value);
        value = transpose(mat2cell(value, ones(dim(1),1)))
        
        new_str = sprintf('%s = (', key)
        if length(str2num(value{1}))
            for i = 1:length(value)
                value{i} = str2num(value{i});
                new_str = sprintf('%s%f, ', new_str, value{i})
            end
        else
            for i = 1:length(value)
                value{i} = strtrim(value{i});
                new_str = sprintf('%s%s, ', new_str, value{i})
            end
        end
        new_str = sprintf('%s)', new_str)
    end
    set(handles.ParasList, 'String', strvcat(old_str, new_str));
end

if isKey(handles.astra.modules, module_name)
    handles.astra.append_parameter(module_name, key, value);
elseif isKey(handles.generator.modules, module_name)
    handles.generator.append_parameter(module_name, key, value);
end

guidata(hObject, handles);



function r = get_list(module_name, varargin)

input_list = {'Fname', 'Add', 'N_add', 'Ipart', 'Species', ...
    'ion_mass', 'Probe', 'Passive', 'Noise_reduc', 'Cathode', ...
    'R_Cathode', 'High_res', 'Binary', 'Q_total', 'Type', 'Rad', ...
    'Tau', 'Ref_zpos', 'Ref_clock', 'Ref_Ekin', 'Dist_z', 'sig_z', ...
    'C_sig_z', 'Lz', 'rz', 'sig_clock', 'C_sig_clcok', 'Lt', 'rt', ...
    'Dist_pz', 'sig_Ekin', 'C_sig_Ekin', 'LE', 'rE', 'emit_z', ...
    'cor_Ekin', 'E_photon', 'phi_eff', 'Dist_x', 'sig_x', ...
    'C_sig_x', 'Lx', 'rx', 'x_off', 'Disp_x', 'Dist_px', 'Nemit_x', ...
    'sig_px', 'C_sig_px', 'Lpx', 'rpx', 'cor_px', 'Dist_y', 'sig_y', ...
    'C_sig_y', 'Ly', 'ry', 'y_off', 'Disp_y', 'Dist_py', 'Nemit_y', ...
    'sig_py', 'C_sig_py', 'Lpy', 'rpy', 'cor_py'};
newrun_list = {'LOOP', 'Head', 'RUN', 'Distribution', ...
    'ion_mass()', 'N_red', 'Xoff', 'Yoff', 'xp', 'yp', 'Zoff', ...
    'Toff', 'Xrms', 'Yrms', 'Zrms', 'Trms', 'Tau', 'cor_py', ...
    'cor_py', 'Qbunch', 'SRT_Q_Schottky', 'Q_Schottky', 'debunch', ...
    'Track_All', 'Track_On_Axis', 'Auto_Phase', 'Phase_Scan', ...
    'check_ref_part', 'L_rm_back', 'Z_min', 'Z_Cathode', ...
    'H_max', 'H_min', 'Max_step', 'Lmonitor', 'Lprompt'};
output_list = {'ZSTART', 'ZSTOP', 'Zemit', 'Zphase', 'Screen()', ...
    'Scr_xrot()', 'Scr_yrot()', 'Step_width', 'Step_max', ...
    'Lproject_emit', 'Local_emit', 'Lmagnetized', 'Lsub_rot', ...
    'Lsub_Larmor', 'Rot_ang', 'Lsub_cor', 'RefS', 'EmitS', ...
    'C_EmitS', 'C99_EmitS', 'Tr_EmitS', 'Sub_EmitS', 'Cross_start', ...
    'Cross_end', 'PhaseS', 'T_PhaseS', 'High_res', 'Binary', ...
    'TrackS', 'TcheckS', 'SigmaS', 'CathodeS', 'LandFS', 'LarmorS'};
charge_list = {'LOOP', 'LSPCH', 'LSPCH3D', 'L2D_3D', 'Lmirror', ...
    'L_Curved_Cathode', 'Cathode_Contour', 'R_zero', 'Nrad', ...
    'Cell_var', 'Nlong_in', 'N_min', 'min_grid', 'Merge_1', ...
    'Merge_2', 'Merge_3', 'Merge_4', 'Merge_5', 'Merge_6', ...
    'Merge_7', 'Merge_8', 'Merge_9', 'Merge_10', 'z_trans', ...
    'min_grid_trans', 'Nxf', 'Nx0', 'Nyf', 'Ny0', 'Nzf', 'Nz0', ...
    'Smooth_x', 'Smooth_y', 'Smooth_z', 'Max_scale', 'Max_count', ...
    'Exp_control'};
aperture_list = {'LOOP', 'LApert', 'File_Aperture()', 'Ap_Z1()', ...
    'Ap_Z2()', 'Ap_R()', 'Ap_GR', 'A_pos()', 'A_xoff()', 'A_yoff()', ...
    'A_xrot()', 'A_yrot()', 'A_zrot()', 'SE_d0()', 'SE_Epm()', ...
    'SE_fs()', 'SE_Tau()', 'SE_Esc()', 'SE_ff1()', 'SE_ff2()', ...
    'Max_Secondary', 'LClean_Stack'};
cavity_list = {'LOOP', 'LEfield', 'File_Efield()', 'C_noscale()', ...
    'C_smooth()', 'Com_grid()', 'C_higher_order()', 'Nue()', ...
    'K_wave()', 'MaxE()', 'Ex_stat()', 'Ey_stat()', 'Bx_stat()', ...
    'By_stat()', 'Bz_stat()', 'Flatness()', 'Phi()', 'C_pos()', ...
    'C_numb()', 'T_dependence()', 'T_null()', 'C_Tau()', 'E_stored()', ...
    'C_xoff()', 'C_yoff()', 'C_xrot()', 'C_yrot()', 'C_zrot()', ...
    'C_zkickmin()', 'C_zkickmax()', 'C_Xkick()', 'C_Ykick()', ...
    'File_A0()', 'P_Z1()', 'P_R1()', 'P_Z2()', 'P_R2', 'P_n', 'E_a0', ...
    'E_Z0', 'E_sig', 'E_sigz', 'E_Zr', 'E_Eps', 'E_lam', 'zeta'};
solenoid_list = {'LOOP', 'LBfield', 'File_Bfield()', 'S_noscale()', ...
    'S_smooth()', 'S_higher_order()', 'MaxB()', 'S_pos()', 'S_xoff()', ...
    'S_yoff()', 'S_xrot()', 'S_yrot()'};
quadrupole_list = {'LOOP', 'Lquad', 'Q_type()', 'Q_grad()', ...
    'Q_noscale()', 'Q_length()', 'Q_smooth()', 'Q_bore()', 'Q_dist()', ...
    'Q_mult_a', 'Q_mult_b', 'Q_pos()', 'Q_xoff()', 'Q_yoff()', ...
    'Q_zoff()', 'Q_xrot()', 'Q_yrot()', 'Q_zrot()', 'Q_K()'};

if nargin == 1
    switch module_name
        case {'newrun', 'Newrun', 'NEWRUN'}
            r = sort(newrun_list);
        case {'input', 'Input', 'INPUT'}
            r = sort(input_list); 
        case {'charge', 'Charge', 'CHARGE'}
            r = sort(charge_list);
        case {'cavity', 'Cavity', 'CAVITY'}
            r = sort(cavity_list);
        case {'solenoid', 'Solenoid', 'SOLENOID'}
            r = sort(solenoid_list);
        case {'quadrupole', 'Quadrupole', 'QUADRUPOLE'}
            r = sort(quadrupole_list);
        case {'output', 'Output', 'OUTPUT'}
            r = sort(output_list);
        case {'aperture', 'Aperture', 'APERTURE'}
            r = sort(aperture_list);
    end
    [~,idx]=sort(upper(r));
    r = r(idx);
elseif nargin>1 && varargin == 'upper'
    switch module_name
        case {'newrun', 'Newrun', 'NEWRUN'}
            r = upper(newrun_list);
        case {'input', 'Input', 'INPUT'}
            r = upper(input_list); 
        case {'charge', 'Charge', 'CHARGE'}
            r = upper(charge_list);
        case {'cavity', 'Cavity', 'CAVITY'}
            r = upper(cavity_list);
        case {'solenoid', 'Solenoid', 'SOLENOID'}
            r = upper(solenoid_list);
        case {'quadrupole', 'Quadrupole', 'QUADRUPOLE'}
            r = upper(quadrupole_list);
        case {'output', 'Output', 'OUTPUT'}
            r = upper(output_list);
        case {'aperture', 'Aperture', 'APERTURE'}
            r = upper(aperture_list);
    end
end

function [generator, astra] = init_modules(varargin)
field_maps = [];
ScrPos = [5.28];
input = Module('Input', 'FNAME', 'beam.ini', 'IPart', 20000, ...
    'Species', 'electrons', 'Q_total', -4, 'LPrompt', false, ...
    'Ref_Ekin', 0.0e-6, 'LE', 0.55e-3, 'dist_pz', 'i', ...
    'Dist_z', 'p', 'Lt', 21.5e-3, 'rt', 2e-3, 'Cathode', true,...
    'Dist_x', 'r', 'sig_x', 1.0, 'Dist_px', 'g', 'Nemit_x', 0, ...
    'Dist_y', 'r', 'sig_y', 1.0, 'Dist_py', 'g', 'Nemit_y', 0);

newrun = Module('Newrun', 'Run', 1, 'Head', 'PITZ beamline', ...
    'Distribution', 'beam.ini', 'Auto_Phase', true, ...
    'Track_All', true, 'check_ref_part', false, 'Lprompt', false, ...
    'Max_step', 200000);

charge = Module('Charge', 'LSPCH', true, 'Lmirror', true, ...
    'Nrad', 50, 'Nlong_in', 50, 'N_min', 10, 'Max_scale', 0.05, ...
    'Max_count', 20, 'Nxf', 16, 'Nyf', 16, 'Nzf', 16);

cavity = Module('Cavity', 'LEfield', true, ...
    'File_Efield()', {fullfile(field_maps, 'gun45cavity.txt'), ...
    fullfile(field_maps, 'CDS14_15mm.txt')}, ...
    'MaxE()', {60, 12}, 'C_pos()', {0., 2.675}, 'Nue()', {1.3, 1.3}, ...
    'Phi()', {0, 0});

solenoid = Module('Solenoid', 'LBfield', true, ...
    'File_Bfield()', {fullfile(field_maps, 'gunsolenoidsPITZ.txt')}, ...
    'MaxB()', {0.2}, 'S_pos()', {0});

output = Module('Output', 'Zstart', 0, 'Zstop', 20.0, 'Zemit', 200, ...
    'Zphase', 1, 'RefS', true, 'EmitS', true, 'PhaseS', true, ...
    'TrackS', false, 'LandFS', true, 'Screen()', num2cell(ScrPos));

aperture = Module('Aperture', 'LApert', true, 'File_Aperture()', ...
    {fullfile(field_maps, 'app.txt')});

quadrupole = Module('Quadrupole', 'LQuad', true);

generator = Astra();
generator.add_module(input);
astra = Astra();
astra.add_modules({newrun, charge, cavity, solenoid, output, ...
    aperture, quadrupole});


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1


% --- Executes during object creation, after setting all properties.
function checkbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in ViewBeamline_Pushbutton.
function ViewBeamline_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to ViewBeamline_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

PITZbeamline;
guidata(hObject, handles);

function SCOinput_Pushbutton_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SCOinput_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --- Executes on button press in SCOinput_Pushbutton.
function SCOinput_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to SCOinput_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

eles = handles.elements;
[Q_name, Q_pos] = eles.get_data('Quad');
[indx ,tf] = listdlg('ListString', Q_name);

if tf>0

    Q_list = {};
    for i = 1:length(indx)
        Q_list = [Q_list {Q_name{indx(i)}}];
    end

    prompt = {'Enter the starting position or the name of screen:', ...
        'Enter the ending position or the name of screen:'};
    dlg_title = 'Input';
    num_lines = [1 40; 1 40]; % 1;
    defaultans = {'5.277', '18.262'};
    answer = inputdlg(prompt, dlg_title, num_lines, defaultans);
    
    if ~isempty(answer)

        [S_name, S_pos] = eles.get_data('Scr');
        
        z1 = str2num(answer{1});
        if isempty(z1)
            select = cellfun(@(x)isequal(x, upper(answer{1})), S_name);
            z1 = S_pos(select)
        end
        
        z2 = str2num(answer{2});
        if isempty(z2)
            select = cellfun(@(x)isequal(x, upper(answer{2})), S_name);
            z2 = S_pos(select)
        end
        
        if isempty(z1) || isempty(z2)
           disp("Please input a number or a valid name such as High1.Scr1!") 
        else
            disp(['hi ' handles.cwd]);
            [file,path,indx] = uiputfile('beamline.txt', 'Save as', handles.cwd);
            disp('there');
            if indx>0
                eles.write_sco(Q_list, 'z0', z1, 'zstop', z2, 'filename', fullfile(path, file));
                handles.cwd = path;
            else
                disp('NB: Please choose a file first!');
            end
        end
    else
        disp('NB: Please input the start and end position!');
    end
else
    disp('NB: Please choose the quadruploes!');
end

guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function axes2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes2

zend = 20;
zmid = zend/2;
ax_to_fig = zend/(zend+2.0);
ratio = 1.5;

set(hObject, 'units', 'centimeter', 'YTick', []);
axis([0 zend 0 5*0.75]);
xticks([0:1:zend]);
xlabel('z (m)');
hObject.YAxis.Visible = 'off';
hObject.XMinorTick = 'on';


% --- Executes during object creation, after setting all properties.
function check_quadrupole_CreateFcn(hObject, eventdata, handles)
% hObject    handle to check_quadrupole (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
set(hObject, 'value', 1);
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function check_screen_CreateFcn(hObject, eventdata, handles)
% hObject    handle to check_screen (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
%set(hObject, 'callback', @(src, event)check_list2_Callback(src, event, handles));
%guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function check_steerer_CreateFcn(hObject, eventdata, handles)
% hObject    handle to check_steerer (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
%set(hObject, 'callback', @(src, event)check_list2_Callback(src, event, handles));
%guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function check_ict_CreateFcn(hObject, eventdata, handles)
% hObject    handle to check_ict (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
%set(hObject, 'callback', @(src, event)check_list2_Callback(src, event, handles));
%guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function check_bpm_CreateFcn(hObject, eventdata, handles)
% hObject    handle to check_bpm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
%set(hObject, 'callback', @(src, event)check_list2_Callback(src, event, handles));
%guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function check_fc_CreateFcn(hObject, eventdata, handles)
% hObject    handle to check_fc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
%set(hObject, 'callback', @(src, event)check_list2_Callback(src, event, handles));
%guidata(hObject, handles);

function check_quadrupole_Callback(hObject, eventdata, handles)
check_list2_Callback(handles);
guidata(hObject, handles);

function check_screen_Callback(hObject, eventdata, handles)
check_list2_Callback(handles);
guidata(hObject, handles);

function check_steerer_Callback(hObject, eventdata, handles)
check_list2_Callback(handles);
guidata(hObject, handles);

function check_ict_Callback(hObject, eventdata, handles)
check_list2_Callback(handles);
guidata(hObject, handles);

function check_fc_Callback(hObject, eventdata, handles)
check_list2_Callback(handles);
guidata(hObject, handles);

function check_bpm_Callback(hObject, eventdata, handles)
check_list2_Callback(handles);
guidata(hObject, handles);


function check_list2_Callback(handles)

vals(1) = get(handles.check_quadrupole, 'Value');
vals(2) = get(handles.check_screen, 'Value');
vals(3) = get(handles.check_steerer, 'Value');
vals(4) = get(handles.check_ict, 'Value');
vals(5) = get(handles.check_fc, 'Value');
vals(6) = get(handles.check_bpm, 'Value');

checked = vals
if isempty(checked)
   checked = 'none';
end

cla;

zend = 20;
zmid = zend/2;
ax_to_fig = zend/(zend+2.0);
ratio = 1.5;

gca = handles.axes2;


element_list = upper({'Quad', 'Scr', 'St', 'ICT', 'FC', 'BPM'});
color_list = {'r', 'b', 'g', 'm', 'c', 'k'};
get_color = containers.Map(element_list, color_list);

for i = length(checked):-1:1
    if checked(i) == 0
        element_list(i) = [];
    end
end


% [file,path] = uigetfile({'*.xlsx'; '*.*'}, 'Select the PITZ beamline', '.');
% if ~isequal(file, 0)
%     file = fullfile(path, file);
% else
%     disp('NB: The default beam line is used!');
%     file = handles.default_beamline;
% end
%eles = Elements(file);
eles = handles.elements;

for k = 1:length(element_list)
    [E_name, E_pos] = eles.get_data(element_list{k});
    for i =1:length(E_pos)
        hold on;
        ls = '-'; y0 = 0; y1 = 0;
        if strcmp(element_list{k}, 'FC')
            ls = ':';
            y1 = 1;
        end
        if strcmp(element_list{k}, 'QUAD') || strcmp(element_list{k}, 'ST')
            y0 = 1; y1 = 1;
        end
        col = get_color(element_list{k});
        stem(E_pos(i), y0+1.5, 'linestyle', ls, 'color', col, 'Marker', 'none');
        text(E_pos(i), y1+1.6, E_name{i}, 'color', col, 'FontSize', 8, ...
            'horizontal', 'left', 'vertical', 'middle', 'rotation', 90);
    end
end

% --- Executes on selection change in CreateModule_Popup.
function CreateModule_Popup_Callback(hObject, eventdata, handles)
% hObject    handle to CreateModule_Popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns CreateModule_Popup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from CreateModule_Popup

str = get(hObject, 'String');
val = get(hObject, 'Value');
name = upper(str{val});

n = length(handles.current_modules);
new_tag = [name '_' num2str(n)];

module = Module(name);
module.set_tag(new_tag);

switch name
    case 'INPUT'
        if ~isempty(handles.generator)
            disp('NB: There is a Input module for Generator!');
            
            choice = questdlg('Would you like overwrite the current Input module?', ...
                'Warning', 'Yes', 'No','No');
            switch choice
                case 'Yes'
                    module.edit();
                    handles.generator.add_module(module);
            end 
        else
            module.edit();
            handles.generator = Astra;
            handles.generator.add_module(module);
        end
    otherwise
        module.edit();
        handles.current_modules = [handles.current_modules module];

        name2 = get(handles.AlreadyHaveList, 'String');
        set(handles.AlreadyHaveList, 'String', [{new_tag} name2']);
end

guidata(hObject, handles);

function fn_Callback(src, event, handles)
[file,path,indx] = uigetfile();
if indx == 0
    disp('NB: Please choose a file first!');
end
set(handles.fn, 'string', fullfile(path,file));


% --- Executes during object creation, after setting all properties.
function CreateModule_Popup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to CreateModule_Popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ChooseBelow_Pushbutton.
function ChooseBelow_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to ChooseBelow_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

eles = handles.elements;
[E_name, E_pos] = eles.get_data('Quad');
[indx ,tf] = listdlg('PromptString', 'Choose the quadrupoles for Astra', ...
    'ListString', E_name, 'ListSize', [200 300]);

if tf>0
    for i = 1:length(indx)
        Q_name(i) = {E_name{indx(i)}};
        Q_pos(i) = E_pos(indx(i));
    end
end

Q_type = cell(size(Q_pos));
Q_type(:) = {'Q3.data'};
%Q_type(:) = {fullfile(field_maps,'Q3.data')};

Q_grad = cell(size(Q_pos));
Q_grad(:) = {0};

% Input the gradients of quadrupoles
prompt = Q_name;
dlg_title = 'Enter the gradients';
num_lines = 1;
Q_grad(:) = {num2str(0)};
defaultans = Q_grad;
x = inputdlg(prompt, dlg_title, [1 40], defaultans);

if ~isempty(x)
    for i = 1:size(x)
        Q_grad(i) = {str2num(x{i})};
    end
end

% for i = 1:length(Q_name)
%     for k = 1:length(handles.modules)
%         tag = handles.modules(k).tag;
%         pos = handles.modules(k).kv('Q_pos');
%         if tag == Q_name(i) || pos{1} == Q_pos(i)
%             
%             handles.modules(k).reset('Q_grad()', {Q_grad(i)});
%         end
%     end
% end

quadrupole = Module('Quadrupole', 'Lquad', true, 'Q_pos()', num2cell(Q_pos), ...
    'Q_grad()', Q_grad, 'Q_type()', Q_type);

handles.astra.add_module(quadrupole);

guidata(hObject, handles);


% --- Executes on button press in Delete_Pushbutton.
function Delete_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to Delete_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in OK_pushbutton.
function OK_pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to OK_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function PositionAt_Edit_Callback(hObject, eventdata, handles)
% hObject    handle to PositionAt_Edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PositionAt_Edit as text
%        str2double(get(hObject,'String')) returns contents of PositionAt_Edit as a double


% --- Executes during object creation, after setting all properties.
function PositionAt_Edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PositionAt_Edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ChooseElement_Popup.
function ChooseElement_Popup_Callback(hObject, eventdata, handles)
% hObject    handle to ChooseElement_Popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ChooseElement_Popup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ChooseElement_Popup
str = get(handles.CreateModule_Popup, 'string');
idx = get(handles.CreateModule_Popup, 'value');
name = upper(str{idx});

str = get(hObject, 'string');
idx = get(hObject, 'value');
tag = str(idx);

module = handles.astra.filter(handles.current_modules, tag);
module.edit();

% module_name = name;
% switch module_name
%     case 'Input'
%         list = get_list(module_name);
%     case {'Newrun', 'NEWRUN'}
%         list = get_list(module_name);
%     case {'Charge', 'CHARGE'}
%         list = get_list(module_name);
%     case {'Cavity', 'CAVITY'}
%         list = get_list(module_name);
%     case {'Solenoid', 'SOLENOID'}
%         list = get_list(module_name);
%     case {'Quadrupole', 'QUADRUPOLE'}
%         list = get_list(module_name);
%     case {'Output', 'OUTPUT'}
%         list = get_list(module_name);
%     case {'Aperture', 'APERTURE'}
%         list = get_list(module_name);
% end
% 
% fig = figure('name', 'Enter the values', 'numbertitle', 'off', ...
%     'units', 'centimeter', 'position', [2, 2, 30, 12]);
% axis off;

% check_width = 30/10.5;
% y0 = 12; disp(length(list));
% for i = 1:length(list)
%     x0 = (mod(i-1, 5)+0.05)*check_width*2; 
%     if mod(i-1, 5) == 0
%         y0 = y0-1;
%     end
%     handles.text_list(i) = uicontrol('style', 'text', 'units', 'centimeters',...
%         'position', [x0, y0, check_width, 0.6], ...
%         'string', list{i}, 'fontsize', 10,...
%         'tag', sprintf('%s', list{i}), 'value', 0);
%     handles.edit_list(i) = uicontrol('style', 'edit', 'units', 'centimeters',...
%         'position', [x0+check_width, y0, check_width, 0.6], ...
%         'foregroundcol', 'r', ...
%         'fontsize', 10, 'tag', sprintf('%s', list{i}), 'value', 0);
%     set(handles.edit_list(i), 'callback', @(src, event)edit_list_Callback(src, event, handles));
%     
%     key = upper(list{i});
%     if isKey(module.kv, key)
%         value = module.kv(key);
%         set(handles.edit_list(i), 'String', to_str(value));
%     end
% end

% values = {}; idx = [];
% for i = 1:length(list)
%     str = get(handles.edit_list(i), 'String');
%     if ~isempty(str)
%         idx = [idx i];
%         values = [values str];
%     end
% end
% disp(list(idx));
% disp(values);

guidata(hObject, handles);

% function edit_list_Callback(src, event, handles)
% key = get(src, 'Tag');
% str = get(src, 'String');
% if length(str2num(str))
%     val = str2num(str);
% else
%     val = str;
% end
% if handles.current_module.isList(key)
%     val = {val};
% end
% handles.current_module.reset(key, val);
% 
% % Save the current module to handles.astra
% modules = handles.astra.to_modules();
% tag = handles.current_module.tag;
% for i = 1:length(modules)
%     if strcmp(modules(i).tag, tag)
%         modules(i) = handles.current_module;
%     end
% end
% handles.astra = Astra(modules);
% 
% guidata(src, handles);

% --- Executes during object creation, after setting all properties.
function ChooseElement_Popup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ChooseElement_Popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function Element_Edit1_Callback(hObject, eventdata, handles)
% hObject    handle to Element_Edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Element_Edit1 as text
%        str2double(get(hObject,'String')) returns contents of Element_Edit1 as a double


% --- Executes during object creation, after setting all properties.
function Element_Edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Element_Edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in Element_Popup2.
function Element_Popup2_Callback(hObject, eventdata, handles)
% hObject    handle to Element_Popup2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns Element_Popup2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Element_Popup2


% --- Executes during object creation, after setting all properties.
function Element_Popup2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Element_Popup2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Element_Edit2_Callback(hObject, eventdata, handles)
% hObject    handle to Element_Edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Element_Edit2 as text
%        str2double(get(hObject,'String')) returns contents of Element_Edit2 as a double


% --- Executes during object creation, after setting all properties.
function Element_Edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Element_Edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in Element_Popup3.
function Element_Popup3_Callback(hObject, eventdata, handles)
% hObject    handle to Element_Popup3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns Element_Popup3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Element_Popup3


% --- Executes during object creation, after setting all properties.
function Element_Popup3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Element_Popup3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Element_Edit3_Callback(hObject, eventdata, handles)
% hObject    handle to Element_Edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Element_Edit3 as text
%        str2double(get(hObject,'String')) returns contents of Element_Edit3 as a double


% --- Executes during object creation, after setting all properties.
function Element_Edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Element_Edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in Element_Popup4.
function Element_Popup4_Callback(hObject, eventdata, handles)
% hObject    handle to Element_Popup4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns Element_Popup4 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Element_Popup4


% --- Executes during object creation, after setting all properties.
function Element_Popup4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Element_Popup4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Element_Edit4_Callback(hObject, eventdata, handles)
% hObject    handle to Element_Edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Element_Edit4 as text
%        str2double(get(hObject,'String')) returns contents of Element_Edit4 as a double


% --- Executes during object creation, after setting all properties.
function Element_Edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Element_Edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in Element_Popup5.
function Element_Popup5_Callback(hObject, eventdata, handles)
% hObject    handle to Element_Popup5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns Element_Popup5 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Element_Popup5


% --- Executes during object creation, after setting all properties.
function Element_Popup5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Element_Popup5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Element_Edit5_Callback(hObject, eventdata, handles)
% hObject    handle to Element_Edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Element_Edit5 as text
%        str2double(get(hObject,'String')) returns contents of Element_Edit5 as a double


% --- Executes during object creation, after setting all properties.
function Element_Edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Element_Edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in Element_Popup6.
function Element_Popup6_Callback(hObject, eventdata, handles)
% hObject    handle to Element_Popup6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns Element_Popup6 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Element_Popup6


% --- Executes during object creation, after setting all properties.
function Element_Popup6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Element_Popup6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Element_Edit6_Callback(hObject, eventdata, handles)
% hObject    handle to Element_Edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Element_Edit6 as text
%        str2double(get(hObject,'String')) returns contents of Element_Edit6 as a double


% --- Executes during object creation, after setting all properties.
function Element_Edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Element_Edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu18.
function popupmenu18_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu18 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu18


% --- Executes during object creation, after setting all properties.
function popupmenu18_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit20_Callback(hObject, eventdata, handles)
% hObject    handle to edit20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit20 as text
%        str2double(get(hObject,'String')) returns contents of edit20 as a double


% --- Executes during object creation, after setting all properties.
function edit20_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in AlreadyHaveList.
function AlreadyHaveList_Callback(hObject, eventdata, handles)
% hObject    handle to AlreadyHaveList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns AlreadyHaveList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from AlreadyHaveList
if strcmp(get(gcf,'selectiontype'),'open')  
  % here you write write code, which you wanna be executed afer double-click
  str = get(hObject, 'String');
  idx = get(hObject, 'Value');
  val = str(idx);
  disp(['NB: double clicked ' val]);
  
  module = handles.astra.filter(handles.current_modules, val);
  module.edit();
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function AlreadyHaveList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to AlreadyHaveList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Add_Pushbutton.
function Add_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to Add_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

str = get(handles.AvailableList, 'String');
idx = get(handles.AvailableList, 'Value');
selected = str(idx);

modules = handles.astra.filter(handles.modules, selected);
handles.current_modules = [handles.current_modules modules];

name2 = get(handles.AlreadyHaveList, 'String');
name2 = unique([name2' selected']);
%[tmp1 tmp2] = handles.elements.sort(names);

set(handles.AlreadyHaveList, 'Value', []);
set(handles.AlreadyHaveList, 'String', [name2]);
guidata(hObject, handles);

% --- Executes on button press in Remove_Pushbutton.
function Remove_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to Remove_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
str = get(handles.AlreadyHaveList, 'String');
idx = get(handles.AlreadyHaveList, 'Value');

[~, idy] = handles.astra.filter(handles.current_modules, str(idx));
handles.current_modules(idy) = [];

str(idx) = [];
set(handles.AlreadyHaveList, 'Value', []);
set(handles.AlreadyHaveList, 'String', str);
guidata(hObject, handles);

% --- Executes on button press in SaveGenerator_Pushbutton.
function SaveGenerator_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to SaveGenerator_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[file,path,indx] = uiputfile('gen.in');
if indx>0
    handles.gen_name = fullfile(path, file);
    handles.generator.write(fullfile(path, file));
else
    disp('NB: Please choose a file first!');
end
guidata(hObject, handles);



function r = get_position(question, handles)

r = 0;

if nargin == 0
    question = 'Please enter the position'
end

choice = questdlg(question, ...
	'Input', ...
	'Input a number', 'Select a screen', 'Input a number');

switch choice
    case 'Input a number'
        x = inputdlg('Enter the position here',...
             'Input', [1 40],  {'18.262'});
        if ~isempty(x)
            pos = str2num(x{1}); 
            if ~isempty(pos)
                r = pos
            end
        end
    case 'Select a screen'
        eles = handles.elements;
        [S_name, S_pos] = eles.get_data('Scr');
        [indx ,tf] = listdlg('ListString', S_name);

        if tf>0
            r = S_pos(indx)
        end
end


% --- Executes on button press in ChooseScreen_Pushbutton.
function ChooseScreen_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to ChooseScreen_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

eles = handles.elements;

[E_name, E_pos] = eles.get_data('Scr');
[indx ,tf] = listdlg('PromptString', 'Choose the screens for Astra', 'ListString', E_name, ...
    'ListSize', [200 300]);

if tf>0
    for i = 1:length(indx)
        S_name(i) = {E_name{indx(i)}};
        S_pos(i) = E_pos(indx(i));
    end
end

guidata(hObject, handles);


% --- Executes on button press in DoPushbutton.
function DoPushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to DoPushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

str = get(handles.AlreadyHaveList, 'String');
modules = handles.astra.filter(handles.modules, str);
handles.current_modules = modules;
handles.astra = Astra(modules);

guidata(hObject, handles);


% --- Executes on button press in LoadField_Pushbutton.
function LoadField_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to LoadField_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in GeneratorEdit.
function GeneratorEdit_Callback(hObject, eventdata, handles)
% hObject    handle to GeneratorEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if ~isempty(handles.generator)
    module = handles.generator.modules('INPUT');
    module.edit();
else
    disp('NB: there is not INPUT Module available! Please LOAD an input file or CREATE one!');
end

guidata(hObject, handles);



function MaxBEdit_Callback(hObject, eventdata, handles)
% hObject    handle to MaxBEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of MaxBEdit as text
%        str2double(get(hObject,'String')) returns contents of MaxBEdit as a double


% --- Executes during object creation, after setting all properties.
function MaxBEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MaxBEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ParaScan_Pushbutton.
function ParaScan_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to ParaScan_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in ParaScanModule_Popupmenu.
function ParaScanModule_Popupmenu_Callback(hObject, eventdata, handles)
% hObject    handle to ParaScanModule_Popupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ParaScanModule_Popupmenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ParaScanModule_Popupmenu
str = get(hObject, 'String');
val = get(hObject, 'Value');
name = upper(str{val});

tags = {};
modules = handles.current_modules;
for i = 1:length(modules)
    if strcmp(modules(i).name, name)
        tags = [tags modules(i).tag];
    end
end

set(handles.ParaScanElement_Popupmenu, 'String', tags);
set(handles.ParaScanElement_Popupmenu, 'Value', 1);

set(handles.ParaScan_Popupmenu, 'String', Module().get_list(name));
set(handles.ParaScan_Popupmenu, 'Value', 1);


guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function ParaScanModule_Popupmenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ParaScanModule_Popupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ParaScanElement_Popupmenu.
function ParaScanElement_Popupmenu_Callback(hObject, eventdata, handles)
% hObject    handle to ParaScanElement_Popupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ParaScanElement_Popupmenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ParaScanElement_Popupmenu


% --- Executes during object creation, after setting all properties.
function ParaScanElement_Popupmenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ParaScanElement_Popupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ParaScan_Popupmenu.
function ParaScan_Popupmenu_Callback(hObject, eventdata, handles)
% hObject    handle to ParaScan_Popupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ParaScan_Popupmenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ParaScan_Popupmenu


% --- Executes during object creation, after setting all properties.
function ParaScan_Popupmenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ParaScan_Popupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function FromEdit_Callback(hObject, eventdata, handles)
% hObject    handle to FromEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of FromEdit as text
%        str2double(get(hObject,'String')) returns contents of FromEdit as a double


% --- Executes during object creation, after setting all properties.
function FromEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to FromEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit25_Callback(hObject, eventdata, handles)
% hObject    handle to edit25 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit25 as text
%        str2double(get(hObject,'String')) returns contents of edit25 as a double


% --- Executes during object creation, after setting all properties.
function edit25_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit25 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit26_Callback(hObject, eventdata, handles)
% hObject    handle to edit26 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit26 as text
%        str2double(get(hObject,'String')) returns contents of edit26 as a double


% --- Executes during object creation, after setting all properties.
function edit26_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit26 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in AddParaScan_Pushbutton.
function AddParaScan_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to AddParaScan_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in DeleteParaScan_Pushbutton.
function DeleteParaScan_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to DeleteParaScan_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit27_Callback(hObject, eventdata, handles)
% hObject    handle to edit27 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit27 as text
%        str2double(get(hObject,'String')) returns contents of edit27 as a double


% --- Executes during object creation, after setting all properties.
function edit27_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit27 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in MaxB2Imain_Pushbutton.
function MaxB2Imain_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to MaxB2Imain_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
var = get(handles.MaxBEdit, 'String');
MaxB = str2num(var);
if ~isempty(MaxB)
    %MaxB = -(0.0000372+0.000588*Imain);
    Imain = (-MaxB-0.0000372)/0.000588;
    set(handles.ImainEdit, 'String', num2str(Imain));
    
else
    disp('Please input the solenoid field first.');
end
guidata(hObject, handles);


function edit28_Callback(hObject, eventdata, handles)
% hObject    handle to edit28 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit28 as text
%        str2double(get(hObject,'String')) returns contents of edit28 as a double


% --- Executes during object creation, after setting all properties.
function edit28_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit28 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Refresh_Pushbutton.
function Refresh_Pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to Refresh_Pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

modules = handles.current_modules;
set(handles.ParaScanModule_Popupmenu, 'String', unique({modules.name}));

guidata(hObject, handles);

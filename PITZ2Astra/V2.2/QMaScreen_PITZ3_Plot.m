clear all;
clc;
close all;

Q_pos(1) = 4.7900 ;        Q_name{1} = 'HIGH1.Q1';
Q_pos(2) = 5.0050 ;        Q_name{2} = 'HIGH1.Q2';
Q_pos(3) = 5.6025 ;        Q_name{3} = 'HIGH1.Q3';
Q_pos(4) = 5.8525 ;        Q_name{4} = 'HIGH1.Q4';
Q_pos(5) = 6.6475 ;        Q_name{5} = 'HIGH1.Q5';
Q_pos(6) = 6.8925 ;        Q_name{6} = 'HIGH1.Q6';
Q_pos(7) = 8.1800 ;        Q_name{7} = 'HIGH1.Q7';
Q_pos(8) = 8.6550 ;        Q_name{8} = 'HIGH1.Q8';
Q_pos(9) = 10.208 ;        Q_name{9} = 'HIGH1.Q9';
Q_pos(10) = 10.388 ;       Q_name{10} = 'HIGH1.Q10';
Q_pos(11) = 12.088 ;       Q_name{11} = 'PST.QM1';
Q_pos(12) = 12.468 ;       Q_name{12} = 'PST.QM2';
Q_pos(13) = 12.848 ;       Q_name{13} = 'PST.QM3';
Q_pos(14) = 13.228 ;       Q_name{14} = 'PST.QT1';
Q_pos(15) = 13.608 ;       Q_name{15} = 'PST.QT2';
Q_pos(16) = 13.988 ;       Q_name{16} = 'PST.QT3';
Q_pos(17) = 14.368 ;       Q_name{17} = 'PST.QT4';
Q_pos(18) = 14.748 ;       Q_name{18} = 'PST.QT5';
Q_pos(19) = 15.128 ;       Q_name{19} = 'PST.QT6';
Q_pos(20) = 16.635 ;       Q_name{20} = 'HIGH2.Q1';
Q_pos(21) = 16.735 ;       Q_name{21} = 'HIGH2.Q2';

S_pos(1) = 0.803 ;        S_name{1} = 'LOW.S1';
S_pos(2) = 1.379 ;        S_name{2} = 'LOW.S2';
S_pos(3) = 1.708 ;        S_name{3} = 'LOW.S3';
S_pos(4) = 5.277 ;        S_name{4} = 'HIGH1.S1';
S_pos(5) = 7.125 ;        S_name{5} = 'HIGH1.S3';
S_pos(6) = 8.410 ;        S_name{6} = 'HIGH1.S4';
S_pos(7) = 8.920 ;        S_name{7} = 'HIGH1.S5';
S_pos(8) = 12.278 ;       S_name{8} = 'PST.S1';
S_pos(9) = 13.038 ;       S_name{9} = 'PST.S2';
S_pos(10) = 13.798 ;      S_name{10} = 'PST.S3';
S_pos(11) = 14.558 ;      S_name{11} = 'PST.S4';
S_pos(12) = 15.318 ;      S_name{12} = 'PST.S5';
S_pos(13) = 16.303 ;      S_name{13} = 'HIGH2.S1';
S_pos(14) = 18.262 ;      S_name{14} = 'HIGH2.S2';

%%
figure
set(gcf, 'units','inch','position', [1 1 15 3],'color','w');
hold on
for i =1:length(Q_pos)
    stem(Q_pos(i),1,'-r','Marker', 'none')
    text(Q_pos(i),1.1, Q_name{i},'color','r', 'FontSize',8, 'horizontal','left', 'vertical','middle','rotation',90)
end
for i =1:length(S_pos)
    stem(S_pos(i),1.5,'-b','Marker', 'none')
    text(S_pos(i), 1.6, S_name{i},'color','b', 'FontSize',8, 'horizontal','left', 'vertical','middle','rotation',90)
end
hold off
 axis([0 19 0 2]);
box on;
grid on;
xlabel('Z [m]' );
saveas(gcf,'PITZ3_SaQ.jpg')

% Modified on 04.05.2019: added get_position to get the position of the
% element if it has the property of `middleposition`

classdef Beamline < handle
    % "Elements" is a list of "Element", including everything defined in
    % the Excel file
    properties
        elements; % a list of `Element`
        keywords = {'Gun', 'Boo', 'TDS', 'Scr', 'St', 'Quad', 'ICT', 'BPM', 'Dipole', 'UND'};
    end
    
    methods
        %% Constructor 
        %  Read from an Excel file the elements in the beamline
        function obj = Beamline(filename, sheetname)
            % Parameters
            %   filename: PITZ coordinate table file name
            %   sheetname: sheet name in that file
            % Examples
            %   Beamline; % This will ask the user to select both arguments
            %   Beamline('PITZ.xlsx', 'PITZ3.0-Koordinatenliste')
            
            %%%% Determine the filename %%%%
            if nargin > 0 && ~isempty(filename)
                filename = filename;
            else
                %filename = 'PITZbeamlines.xlsx';
                %filename = 'PITZ3-Koordinaten_15-10-15beamlines_THz.xlsx';
                [file,path] = uigetfile({'*.xlsx'; '*.xls'; '*.*'}, 'Select the PITZ beamline', '.');
                
                if ~isequal(file, 0)
                    disp(['User selected: ', fullfile(path,file)]);
                    filename = fullfile(path,file);
                else
                    disp('No input file selected!');
                    return;
                end
            end

            % Check if the file exists. If not, return NULL
            if ~exist(filename, 'file')
                fprintf('NB: file %s does not exists!\n', filename);
                return;
            end
 
            %%%% Determine the sheet name %%%%
            if nargin > 1 && ~isempty(sheetname)
                sheetname = sheetname;
            else
                %sheetname = 'PITZ3.0-Koordinatenliste';
                [~, sheets] = xlsfinfo(filename);
                if length(sheets)>0
                    %
                    [s, v] = listdlg('PromptString','Choose a sheet name:',...
                        'SelectionMode','single',...
                        'ListString', sheets);
                    if ~isempty(v)
                        sheetname = sheets{s};
                    end
                end
            end
            
            % The contents are read into three formats: 'num' for numerical, 'txt' for text and 'raw' as what it
            % originally is.
            if isempty(sheetname)
                [num, txt, raw] = xlsread(filename);
            else
                [num, txt, raw] = xlsread(filename, sheetname);
            end
            % Convert raw from cell to array
            raw = string(raw);

            % Get the dimension of the data
            [nrows, ncols] = size(num);
            % fprintf('size: %i %i\n', nrows, ncols);

            j = 0;
            % elements = containers.Map();
            CC = 1;
            for i = 6:nrows
                kv = {};
                if ~ismissing(raw(i,2))
                    j = j+1;
                    name = raw{i,2};
                    kv = [kv {'name', name}];
                elseif ~ismissing(raw(i, 3)) && strcmp(raw(i, 3), 'Gun')
                    kv = [kv {'name', 'Gun'}];
                elseif ~ismissing(raw(i, 3)) && strcmp(raw(i, 3), 'CDS-Booster')
                    kv = [kv {'name', 'CDS-Booster'}];
                else
                    continue;
                end
                    
                if ~ismissing(raw(i, 3))
                    element = raw{i,3};
                    kv = [kv {'element', element}];
                end

                if ~ismissing(raw(i,4))
                    startposition = raw{i,4};
                    kv = [kv {'startposition', str2num(startposition)/1000.0}];
                end
                if ~ismissing(raw(i,5))
                    endposition = raw{i,5};
                    kv = [kv {'endposition', str2num(endposition)/1000.0}];
                end
                if ~ismissing(raw(i,6))
                    middleposition = raw{i,6};
                    kv = [kv {'middleposition', str2num(middleposition)/1000.0}];
                elseif contains(upper(raw(i,2)), '.SCR') && ~ismissing(raw(i+1,6))
                    middleposition = raw{i+1,6};
                    kv = [kv {'middleposition', str2num(middleposition)/1000.0}];
                elseif contains(upper(raw(i,2)), '.ICT') && ~ismissing(raw(i,4)) && ~ismissing(raw(i,5))
                    kv = [kv {'middleposition', (str2num(startposition)+str2num(endposition))/2.0/1000.0}];
                end
                if ~ismissing(raw(i,7))
                    len = raw{i,7};
                    kv = [kv {'length', str2num(len)/1000.0}];
                end
                %disp(kv); %disp('hello');
                names = regexp(kv{2}, '\s*[,;]\s*', 'split');
                if max(size(names))>1
                    len = max(size(kv));
                    for j=1:max(size(names))
                        kvj = ['name', names{j}, {kv{3:len}}];
                        element = Element(kvj{:});
                        %elements(upper(names{j})) = element;
                        
                        elements(CC) = element;
                        CC = CC+1;
                        
                        %elements = [elements {element}];
                        %disp(kvj);
                    end
                else
                    element = Element(kv{:});                  
                    %elements(upper(kv{2})) = element;
                    
                    elements(CC) = element;
                    CC = CC+1;
                        
                    %elements = [elements {element}];
                    %disp(kv);
                end
               
            end
            obj.elements = elements;
        end
        
        %% If the object is null or not
        function r = isNull(obj)
            if isempty(obj.elements)
                r = true;
            else
                r = false;
            end
        end
        
        %% Get the element specified by the keyword. The keyword could represent
        %  a type of element, such as 'Scr', 'quad' or be the full name of one element,
        %  e.g., 'High1.Scr1' or 'HIgH1.ScR1'
        function r = get(obj, keyword)
            % Parameters
            %   keyword: a string representing a group of the elements,
            %   such as 'Quad', 'Scr', 'Boo', 'Gun'
            % Returns
            %   r: a list of elements having the keyword
            % Examples
            %   get('Scr'); % This will return a list of all screens
            %   get('High1.Scr1'); this will give you the element of
            %   high1.scr1
            
            keyword = upper(keyword);
            % r = containers.Map();
            %keys = obj.elements.keys;
            current = obj.elements;
            
            CC = 1; %r = [];
            switch keyword
                case 'GUN'
                    for i = 1:length(current)
                       element = current(i);
                       key = element.name;
                       %element = obj.current(key);
                       s1 = regexp(key, 'GUN', 'once');
                       s2 = regexp(key, 'GUN.', 'once');
                       if ~isempty(s1) && isempty(s2)
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
                case {'BOO', 'BOOSTER', 'CDS-BOOSTER'}
                    for i = 1:length(current)
                       element = current(i);
                       key = element.name;
                       s1 = regexp(key, 'BOOSTER', 'once');
                       if ~isempty(s1)
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
                case {'TDS', 'RFD.TDS'}
                    for i = 1:length(current)
                       element = current(i);
                       key = element.name;
                       s1 = regexp(key, 'RFD.TDS', 'once');
                       if ~isempty(s1)
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
                case {'UND', 'U1'}
                    for i = 1:length(current)
                       element = current(i);
                       key = element.name;
                       s1 = regexp(key, 'UNDULATOR1', 'once');
                       if ~isempty(s1)
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
                case {'DIP', 'DIPOLE', 'DIPOL'}
                    for i = 1:length(current)
                       element = current(i);
                       key = element.element;
                       if ~isempty(key)
                           s1 = regexp(key, 'Dipol', 'once');
                           if ~isempty(s1)
                               r(CC) = element;
                               CC = CC+1;
                           end
                       end
                    end
                case 'SCR'
                    for i = 1:length(current)
                       element = current(i);
                       key = element.name;
                       s1 = regexp(key, '\.SCR', 'once');
                       s2 = regexp(key, 'DISP', 'once');
                       if ~isempty(s1) && isempty(s2)
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
                case {'QUAD', 'QUADRUPOLE'}
                    for i = 1:length(current)
                       element = current(i);
                       key = element.name;
                       s1 = regexp(key, '\.Q', 'once');
                       s2 = regexp(key, 'DISP', 'once');
                       if ~isempty(s1) && isempty(s2)
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
                case {'ST', 'STEERER'}
                    for i = 1:length(current)
                       element = current(i);
                       key = element.name;
                       s1 = regexp(key, '\.ST', 'once');
                       s2 = regexp(key, '\.STREAK', 'once');
                       if ~isempty(s1) && isempty(s2)
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
                case 'ICT'
                    for i = 1:length(current)
                       element = current(i);
                       key = element.name;
                       s1 = regexp(key, '\.ICT', 'once');
                       s2 = regexp(key, 'DISP', 'once');
                       if ~isempty(s1) && isempty(s2)
                           r(CC) = element;
                           CC = CC+1;
                       end
                   end
                case 'FC'
                   for i = 1:length(current)
                       element = current(i);
                       key = element.name;
                       s1 = regexp(key, '\.FC', 'once');
                       s2 = regexp(key, 'DISP', 'once');
                       if ~isempty(s1) && isempty(s2)
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
                case 'BPM'
                    for i = 1:length(current)
                       element = current(i);
                       key = element.name;
                       s1 = regexp(key, '\.BPM', 'once');
                       s2 = regexp(key, 'DISP', 'once');
                       if ~isempty(s1) && isempty(s2)
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
                otherwise
                    for i = 1:length(current)
                       element = current(i);
                       key = element.name;
                       s1 = strcmp(key, keyword);
                       if s1
                           r(CC) = element;
                           CC = CC+1;
                       end
                    end
            end
            if CC == 1
                r = [];
            end
        end
        
        %% Get the position of the element specified by the keyword. The keyword should 
        %  be the full name of an element, e.g., 'High1.Scr1' or 'HIgH1.ScR1'
        function r = get_position(obj, name)
            % Parameters
            %   name: name of the element
            % Returns
            %   position: position of the elment
            % Examples
            %   get_position('High1.Scr1') % will return 5.277
            
            element = obj.get(name);
            if ~isempty(element.middleposition)
                r = element.middleposition;
            else
                r = -1000;
            end
        end
        
        %% Get the names and positions of a group of elements, specified by
        %  the keyword, 
        function [names, positions] = get_pair(obj, keyword, centered)
            % Parameters
            %   keyword: a string representing a group of the elements,
            %   such as 'Quad', 'Scr'
            %   centered: true by default. if true, return the middle
            %   position of the elements; if false, return the starting and
            %   ending positions of the elements
            % Returns
            %   names: a list of names of the elements having the keyword
            %   positions: midddle positions of the elements or starting
            %   and ending positions of the elements
            % Examples
            %   get_pair('Scr') % 
            
            if nargin>2
                centered = centered;
            else
                centered = true;
            end
            eles = obj.get(keyword);
            %keys = eles.keys;
            pp = []; kk = {};
            for i=1:length(eles)
                key = eles(i).name;
                pos = eles(i).middleposition;
                ff = 0;
                if ~isempty(pos) && centered
                    pp = [pp; [pos]];
                    kk = [kk; {key}];
                    ff = 1;
                end
                startpos = eles(i).startposition;
                endpos = eles(i).endposition;
                if ~isempty(startpos) && ~isempty(endpos) && ~centered
                    pp = [pp; [startpos endpos]];
                    kk = [kk {key}];
                    ff = 2;
                end
            end
            [positions, index] = sortrows(pp);
            names = kk(index);
        end
        
        %% Print the name and position of the elements
        function r = print(obj, keyword)
            % Parameters
            %   keyword: a string representing a group of the elements,
            %   such as 'Quad', 'Scr'
            % Returns
            %   print " element | name | unit " element by element
            % Examples
            %   print('Scr')
            
            if nargin>1 
                eles = obj.get(keyword);
            else
                % NB: didn't work though
                eles = obj.elements;
            end
            %keys = eles.keys;
            pp = []; kk = {};
            for i=1:length(eles)
                key = eles(i).name;
                pos = eles(i).middleposition;
                if ~isempty(pos) && ~isempty(key)
                    pp = [pp [pos]];
                    kk = [kk {key}];
                end
            end
            [pp, index] = sort(pp);
            kk = kk(index);
            
            for i=1:length(kk)
                key = kk{i};
                pos = pp(i);
                if ~isempty(pos)
                    %fprintf('%s: %f\n', key, pos);
                    fprintf('%s | %f | m\n', key, pos);
                end
            end
        end
        
        %% Get the boundary of elements, specified by the keyword.
        %  The bounds are defined by the startposition and endposition.
        function [names, bounds] = get_bounds(obj, keyword)
            % Parameters
            %   keyword: a string representing a group of the elements,
            %   such as 'Quad', 'Scr'
            % Returns
            %   names: a list of names of the elements having the keyword
            %   bounds: start and end positions of the elements or starting
            %   and ending positions of the elements which really have the
            %   bounds
            % Examples
            %   get_bounds('Scr')
            
            eles = obj.get(keyword);
            %keys = eles.keys;
            pp = []; kk = {};
            for i=1:length(eles)
                key = eles(i).name;
                pos1 = eles(i).startposition;
                pos2 = eles(i).endposition;
                if ~isempty(pos1) && ~isempty(pos2)
                    pp = [pp; [pos1 pos2]];
                    kk = [kk {key}];
                end
            end
            [~, index] = sort(pp(:,1));
            b1 = pp(:,1);
            b2 = pp(:,2);
            bounds = [b1(index); b2(index)];
            names = kk(index);
            
        end
           
        %% Make a plot with the selected elements
        function r = plot(obj, varargin)
            % Make a plot of the elements in the list including:
            % 'Quad', 'Scr', 'St', 'ICT', 'FC', 'BPM'
            % Examples
            %    plot('Quad', 'Scr')
            zend = 20;
            zmid = zend/2;
            ax_to_fig = zend/(zend+2.0);
            ratio = 1.5;

            
            r = figure('name', 'PITZ beamline', 'numbertitle', 'off', ...
                'units', 'centimeter', 'position', [2, 2, zend+2, 13]*ratio, 'color', 'w');
            %r = fig;
            ax1 = subplot(2, 1, 1);
            ax2 = subplot(2, 1, 2);
            set(ax1, 'units', 'centimeter', 'position', [1 7 zend 5]*ratio, 'YTick', []);
            set(ax2, 'units', 'centimeter', 'position', [1 1 zend 5]*ratio, 'YTick', []);

            subplot(ax1)
            axis([0 zmid 0 5*0.75]);
            xticks([0:1:zmid]);
            ax1.YAxis.Visible = 'off';

            subplot(ax2)
            axis([zmid zend 0 5*0.75]);
            xticks([zmid:1:zend]);
            ax2.YAxis.Visible = 'off';
            xlabel('z (m)');
            
            element_list = upper({'Quad', 'Scr', 'St', 'ICT', 'FC', 'BPM'});
            color_list = {'r', 'b', 'g', 'm', 'c', 'k'};
            get_color = containers.Map(element_list, color_list);
            
            varargin = upper(varargin);
            for k = 1:length(varargin)
                %varargin(k) = upper(varargin(k));
                [E_name, E_pos] = obj.get_pair(varargin{k});
                for i =1:length(E_pos)
                    if (E_pos(i)<=zmid)
                        subplot(ax1);
                    else
                        subplot(ax2);
                    end
                    hold on;
                    ls = '-'; y0 = 0; y1 = 0;
                    if strcmp(upper(varargin{k}), 'FC')
                        ls = ':';
                        y1 = 1;
                    end
                    if strcmp(upper(varargin{k}), 'QUAD') || strcmp(upper(varargin{k}), 'ST')
                        y0 = 1; y1 = 1;
                    end
                    col = get_color(varargin{k});
                    stem(E_pos(i), y0+1.5, 'linestyle', ls, 'color', col, 'Marker', 'none');
                    text(E_pos(i), y1+1.6, E_name{i}, 'color', col, 'FontSize', 8, ...
                        'horizontal', 'left', 'vertical', 'middle', 'rotation', 90);
                end
            end
        end
                
        %% Sort a list of names according to the positions of the corresponding
        %  element
        function [name1, name2] = sort(obj, names)
            % Examples
            %   sort({'gun', 'tds', 'boo', 'low.scr1'})
            pos1 = []; name1 = {}; name2 = {};
            for i = 1:length(names)
                name = names{i};
                element = obj.get(name);
                if ~isempty(element)
                    if ~isempty(element.middleposition)
                        pos1 = [pos1 element.middleposition];
                    else
                        pos1 = [pos1 element.startposition];
                        %name2 = [name2 {name}];
                        %continue;
                    end
                    name1 = [name1 {name}];
                else
                    name2 = [name2 {name}];
                end
            end
            [pos1, index] = sort(pos1);
            name1 = name1(index);
        end
                
        %% Find the elements located around z = z0+/-error
        function r = find_element(obj, z0, error)
            if nargin<3
                error = 0.005;
            end
            found = {};
            eles = obj.elements;
            for i = 1:length(eles)
                element = eles(i);
                if abs(element.middleposition-z0)<error
                    found = [found {element.name}];
                end
            end
            r = found;
        end
    end
end

classdef Module < handle
% Define a `Module` class, which accepts key-value syntax as arguments. 
% This class is the basis for Generator and/or Astra.
% Example:
% To build a NEWRUN module for Astra, call
% -----------------------------------------------------------------------
% newrun = Module('Newrun', 'Run', 1, 'Head', 'PITZ beamline simulation',
%                 'Distribution', 'beam.ini', 'Auto_phase', true, ...
%                 'Track_All', true, 'check_ref_part', false, ...
%                 'Lprompt', false, 'Max_step', 20000);
% -----------------------------------------------------------------------
    properties
    % name  : a string, the name of the module, which is in the Astra
    %         namelist, for example, 'Input', 'Newrun', 'Charge', ...
    % kv    : a map, consisting of key-value pairs, which defines the
    %         input parameters for Astra modules, e.g., ('LEfield', TRUE)
    % output: a string, the output to be written in a file as Astra or
    %         Generator inputs
        name = 'SomeModule';
        kv = containers.Map();
        output;
    end
    methods
        function obj = Module(name, varargin)
        % Class constructor, which accepts the `name` of the module and 
        % key-value pairs for the mudole. The use of `varargin` allows to 
        % give as many parameters (in format of key-value pairs) as the 
        % user wants.
            if nargin > 0
                obj.name = upper(name);
                obj.kv = containers.Map();
                
                % Get the key-value pairs from `varargin`
                nargs = length(varargin);
                % Convert keys into upper cases
                keys = upper(varargin(1:2:nargs)); 
                values = varargin(2:2:nargs);
                % Set the key-value pairs to `kv`
                for i = 1:length(keys)
                    obj.kv(keys{i}) = values{i};
                end
                % Generating the output string
                obj.build();
            end
        end
        
        
        function r = set(obj, name, varargin)
        % Same as Constructor, but with a different function name
            if nargin > 0
                obj.name = upper(name);
                %obj.kv = containers.Map();
                
                % Get the key-value pairs from `varargin`
                nargs = length(varargin);
                keys = upper(varargin(1:2:nargs));
                values = varargin(2:2:nargs);
                % Set the key-value pairs to `kv`
                for i = 1:length(keys)
                    obj.kv(keys{i}) = values{i};
                end
                % Regenerating the output string
                obj.build();
            else
                disp('NB: There is no input argument!\n');
            end
        end
        
        
        function r = reset(obj, key, value, append)
        % Reset the keyValue pair, e.g., 
        % `reset('Run', 2)` would change 'Run' to 2
        % If `append` is given and is true, then the value will be appended to
        % the original one, e.g.,
        % `reset('File_Efield()', 5, true) will add an additional field data
        % file to File_Efield()
            key = upper(key);
            if nargin<4
                append = false;
            end
            if isKey(obj.kv, key) && obj.isList(key) && append
                obj.kv(upper(key)) = [obj.kv(key) value];
            else
               obj.kv(upper(key)) = value;
            end
            % Regenerating the output string
            obj.build();
        end
        
        function r = delete(obj, key)
        % Delete the keyValue pair, e.g., 
            key = upper(key);
            if isKey(obj.kv, key)
                remove(obj.kv, key);
            end
            % Regenerating the output string
            obj.build();
        end
        
        function r = build0(obj)
        % Formatting the output of the modules
            r = sprintf('&%s\n', obj.name);
            for k = obj.kv.keys
                key = k{1};
                val = obj.kv(key);
                
                if ~iscell(val) 
                % if not a cell list, e.g., LEfield
                    if ischar(val) || isstring(val)          
                    % if the value is char or string
                        str = strrep(val, '''', '');
                        r = sprintf('%s %s="%s"\n', r, key, str);
                    end
                    if islogical(val)      
                    % if the value is logical
                        r = sprintf('%s %s=%s\n', r, key, upper(string(val)));
                    end
                    if isnumeric(val)       
                    % if the value is a number
                        if abs(round(val)-val)<1e-9 % integer
                            r = sprintf('%s %s=%d\n', r, key, val);
                        else                        % float
                            r = sprintf('%s %s=%.6f\n', r, key, val);
                        end
                    end
                else
                % if `val` is a cell list, e.g., File_Efield()
                    for j = 1:length(val)
                        if ischar(val{j}) || isstring(val{j})  
                        % if the value is char or string
                            str = strrep(val{j}, '''', '');
                            r = sprintf('%s %s(%d)="%s"\n', r, key, j, str);
                        end
                        if islogical(val)    
                        % if the value is logical
                            r = sprintf('%s %s(%d)=%s\n', r, key, j, upper(string(val)));
                        end
                        if isnumeric(val{j}) 
                        % if the value is a number
                            if abs(round(val{j})-val{j})<1e-6 % integer
                                r = sprintf('%s %s(%d)=%d\n', r, key, j, val{j});
                            else                              % float
                                r = sprintf('%s %s(%d)=%.6f\n', r, key, j, val{j});
                            end
                        end
                    end
                end
            end
            r = sprintf('%s&\n\n', r);
            obj.output = r;
        end
        
        function r = isList(obj, key)
            startIndex = regexp(key, '\(\)');
            if isempty(startIndex)
                r=false
            else
                r=true
            end
        end
        
        
        function r = build(obj)
        % Formatting the output of the modules
            r = sprintf('&%s\n', obj.name);
            for k = obj.kv.keys
                key = k{1};
                val = obj.kv(key);
                
                startIndex = regexp(key, '\(\)');
                
                if isempty(startIndex) %~iscell(val) 
                % if not a cell list, e.g., LEfield
                    if ischar(val) || isstring(val)          
                    % if the value is char or string
                        str = strrep(val, '''', '');
                        r = sprintf('%s %s="%s"\n', r, key, str);
                    end
                    if islogical(val)      
                    % if the value is logical
                        r = sprintf('%s %s=%s\n', r, key, upper(string(val)));
                    end
                    if isnumeric(val)       
                    % if the value is a number
                        if abs(round(val)-val)<1e-9 % integer
                            r = sprintf('%s %s=%d\n', r, key, val);
                        else                        % float
                            r = sprintf('%s %s=%.6f\n', r, key, val);
                        end
                    end
                else
                % if `val` is a cell list, e.g., File_Efield()
                    key = key(1:startIndex-1);
                    for j = 1:length(val)
                        if ischar(val{j}) || isstring(val{j})  
                        % if the value is char or string
                            str = strrep(val{j}, '''', '');
                            r = sprintf('%s %s(%d)="%s"\n', r, key, j, str);
                        end
                        if islogical(val)    
                        % if the value is logical
                            r = sprintf('%s %s(%d)=%s\n', r, key, j, upper(string(val)));
                        end
                        if isnumeric(val{j}) 
                        % if the value is a number
                            if abs(round(val{j})-val{j})<1e-6 % integer
                                r = sprintf('%s %s(%d)=%d\n', r, key, j, val{j});
                            else                              % float
                                r = sprintf('%s %s(%d)=%.6f\n', r, key, j, val{j});
                            end
                        end
                    end
                end
            end
            r = sprintf('%s&\n\n', r);
            obj.output = r;
        end
        
        
        function r = write(obj, filename)
        % Write the output into a file
            fid = fopen(filename,'wt');
            fprintf(fid, obj.output);
            fclose(fid);
        end
    end
end


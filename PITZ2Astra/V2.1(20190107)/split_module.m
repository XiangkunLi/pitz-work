function r = split_module1(module)
% Split modules of the same kind into Astra modules
% Parameters
%   module: an Astra Module
% Returns
%   split_modules: a list of Module with the same Module.name
% Example:
% c = Module('cavity', 'LEfile', true, 'File_Efield()', {'gun.txt', 'booster.txt'}, 'C_pos()', {0, 2.675}, 'phi()', {0, 0})
% c = split_module(c);

name = module.name;
keys = module.kv.keys;

for key = keys
    key = key{1};
    if module.isList(key)
        vals = module.kv(key);
        for i = 1:length(vals)     
            split_modules(i) = Module(name);
        end  
    end
end

for key = keys
    key = key{1};
    if module.isList(key)
        vals = module.kv(key);
        for i = 1:length(vals)
            split_modules(i).reset(key, vals(i))
        end
    else
        val = module.kv(key);
        for i = 1:length(split_modules)
            split_modules(i).reset(key, val);
        end
    end
end

r = split_modules;
end
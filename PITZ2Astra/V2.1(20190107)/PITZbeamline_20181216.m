%% Draw the scaled layout of the PITZ Beamline
function [ output_args ] = PITZbeamline( input_args )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

zend = 20;
zmid = zend/2;
ax_to_fig = zend/(zend+2.0);
ratio = 1.35;

fig = figure('name', 'PITZ beamline', 'numbertitle', 'off', ...
    'units', 'centimeter', 'position', [2, 2, zend+2, 13]*ratio);

ax1 = subplot(2, 1, 1);
ax2 = subplot(2, 1, 2);
set(ax1, 'units', 'centimeter', 'position', [1 7 zend 5.]*ratio, 'YTick', []);
set(ax2, 'units', 'centimeter', 'position', [1 1 zend 5.]*ratio, 'YTick', []);

subplot(ax1)
axis([0 zmid 0 5*0.75]);
xticks([0:1:zmid]);
ax1.YAxis.Visible = 'off';
ax1.XMinorTick = 'on';

subplot(ax2)
axis([zmid zend 0 5*0.75]);
xticks([zmid:1:zend]);
ax2.YAxis.Visible = 'off';
ax2.XMinorTick = 'on';
xlabel('z (m)');

handles.pbeamline = 'PITZ3-Koordinaten_15-10-15beamlines_THz.xlsx';
handles.eles = Elements(handles.pbeamline, 'PITZ3.0-Koordinatenliste (THz)');
element_list = {'Quadrupole', 'Screen', 'Steerer', 'ICT', 'Faraday Cup', 'BPM'};
color_list = {'r', 'b', 'g', 'm', 'c', 'k'};
check_width = 20/7.;
for i = 1:length(element_list)
    handles.check_list(i) = uicontrol('style', 'checkbox', 'units', 'centimeters',...
        'position', [1+(i-1)*check_width, 12.15, check_width, 0.6/ratio]*ratio, ...
        'string', element_list{i}, 'fontsize', 10, 'foregroundcol', color_list{i},...
        'tag', sprintf('check_list_%s', element_list{i}), 'value', 0);
end
set(handles.check_list(1), 'callback', @(src, event)check_list_Callback(src, event, handles));
set(handles.check_list(2), 'callback', @(src, event)check_list_Callback(src, event, handles));
set(handles.check_list(3), 'callback', @(src, event)check_list_Callback(src, event, handles));
set(handles.check_list(4), 'callback', @(src, event)check_list_Callback(src, event, handles));
set(handles.check_list(5), 'callback', @(src, event)check_list_Callback(src, event, handles));
set(handles.check_list(6), 'callback', @(src, event)check_list_Callback(src, event, handles));

handles.element_list = element_list;


xbox = uicontrol('Style', 'text', 'Tag', 'xbox', 'units', 'centimeters',...
    'position', [6.5*check_width, 12.1, check_width, 0.6/ratio]*ratio, ...
    'fontsize', 10);
xboxlabel = uicontrol('Style', 'text', 'units', 'centimeters',...
    'String', 'Position at: ', 'fontsize', 10, ...
    'Position',get(xbox,'position')+[-0.5*check_width 0 -0.5*check_width 0]);

callback = ['pt = get(gca,''CurrentPoint'');' ...
    'xbox = findobj(''Tag'',''xbox'');' ...
    'zpos = pt(1,1);' ...
    'if pt(1,2)>4;' ...
    'zpos = zpos-10;' ...
    'end;' ...
    'set(xbox,''String'',[num2str(zpos) '' m'']);'];
set(gcf,'WindowButtonMotionFcn',callback);

%    


% try
%     while true
%         [x, y] = getpts(gcf);
%         text1 = uicontrol('style', 'text', 'units', 'centimeters', ...
%             'position', [7*check_width, 12.15, check_width, 0.6/ratio]*ratio, ...
%             'string', [], 'fontsiez', 10);
%     end
% end


function check_list_Callback(src, event, handles)

% if get(handles.check_list(1),'Value')==1
%     disp('Value=1');
% elseif get(handles.check_list(1),'Value')==0
%     disp('Value=0');
% end
vals = get(handles.check_list, 'Value');
checked = [vals{:}]
%checked = find([vals{:}]);
if isempty(checked)
   checked = 'none';
end

cla;

zend = 20;
zmid = zend/2;
ax_to_fig = zend/(zend+2.0);
ratio = 1.35;

fig = gcf;

ax1 = subplot(2, 1, 1);
ax2 = subplot(2, 1, 2);
set(ax1, 'units', 'centimeter', 'position', [1 7 zend 5.]*ratio, 'YTick', []);
set(ax2, 'units', 'centimeter', 'position', [1 1 zend 5.]*ratio, 'YTick', []);

subplot(ax1)
axis([0 zmid 0 5*0.75]);
xticks([0:1:zmid]);
ax1.YAxis.Visible = 'off';
ax1.XMinorTick = 'on';
    
subplot(ax2)
axis([zmid zend 0 5*0.75]);
xticks([zmid:1:zend]);
ax2.YAxis.Visible = 'off';
ax2.XMinorTick = 'on';
xlabel('z (m)');

element_list = upper({'Quad', 'Scr', 'St', 'ICT', 'FC', 'BPM'});
color_list = {'r', 'b', 'g', 'm', 'c', 'k'};
get_color = containers.Map(element_list, color_list);

for i = length(checked):-1:1
    if checked(i) == 0
        element_list(i) = [];
    end
end

eles = Elements(handles.pbeamline, 'PITZ3.0-Koordinatenliste (THz)');
for k = 1:length(element_list)
    [E_name, E_pos] = eles.get_data(element_list{k});
    for i =1:length(E_pos)
        if (E_pos(i)<=zmid)
            subplot(ax1);
        else
            subplot(ax2);
        end
        hold on;
        ls = '-'; y0 = 0; y1 = 0;
        if strcmp(element_list{k}, 'FC')
            ls = ':';
            y1 = 1;
        end
        if strcmp(element_list{k}, 'QUAD') || strcmp(element_list{k}, 'ST')
            y0 = 1; y1 = 1;
        end
        col = get_color(element_list{k});
        stem(E_pos(i), y0+1.5, 'linestyle', ls, 'color', col, 'Marker', 'none');
        text(E_pos(i), y1+1.6, E_name{i}, 'color', col, 'FontSize', 8, ...
            'horizontal', 'left', 'vertical', 'middle', 'rotation', 90);
    end
end

% try
%     while true
%         [x, y] = getpts(gcf);
%         text1 = uicontrol('style', 'text', 'units', 'centimeters', ...
%             'position', [7*check_width, 12.15, check_width, 0.6/ratio]*ratio, ...
%             'string', [], 'fontsiez', 10);
%     end
% end
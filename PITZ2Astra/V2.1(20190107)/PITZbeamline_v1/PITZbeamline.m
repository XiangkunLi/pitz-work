%% Draw the scaled layout of the PITZ Beamline
function [ output_args ] = PITZbeamline( input_args )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

%% Load the excel file
beamline = 'PITZ3-Koordinaten_15-10-15beamlines_THz.xlsx';
sheetname = 'PITZ3.0-Koordinatenliste (THz)';
handles.eles = Elements(beamline, sheetname);

%% Prepare the figure to plot
zend = 32; % Length of the beam line
zmid = zend/2; % Half length of the beam line
ax_to_fig = zend/(zend+2.0);
ratio = 1.35;

fig = figure('name', 'PITZ beamline', 'numbertitle', 'off', ...
    'units', 'centimeter', 'position', [2, 2, 20+2, 13]*ratio);

ax1 = subplot(2, 1, 1);
ax2 = subplot(2, 1, 2);
set(ax1, 'units', 'centimeter', 'position', [1 7 20 5.]*ratio, 'YTick', []);
set(ax2, 'units', 'centimeter', 'position', [1 1 20 5.]*ratio, 'YTick', []);

subplot(ax1)
axis([0 zmid 0 5*0.75]);
xticks([0:1:zmid]);
ax1.YAxis.Visible = 'off';
ax1.XMinorTick = 'on';

subplot(ax2)
axis([zmid zend 0 5*0.75]);
xticks([zmid:1:zend]);
ax2.YAxis.Visible = 'off';
ax2.XMinorTick = 'on';
xlabel('z (m)');
%% Plot the PITZ gun, booster, TDS, wall, undulator
subplot(ax1);
[name, ends] = handles.eles.get_data('GUN', false);
rectangle('Position', [0. 0.1 ends(2) 1.], ...
    'EdgeColor', 'y', 'FaceColor', 'y');
text(0.1, 0.45, name, 'color', 'k', 'FontSize', 9, ...
    'horizontal', 'left', 'vertical', 'middle', 'rotation', 90);

[name, ends] = handles.eles.get_data('BOOSTER', false);
rectangle('Position', [ends(1) 0.1 ends(2)-ends(1) 1.], ...
    'EdgeColor', 'y', 'FaceColor', 'y');
text(0.5*(ends(2)+ends(1)), 0.6, name, 'color', 'k', 'FontSize', 9, ...
    'horizontal', 'center', 'vertical', 'middle', 'rotation', 0);

[name, ends] = handles.eles.get_data('TDS', false);
rectangle('Position', [ends(1) 0.1 ends(2)-ends(1) 1.], ...
    'EdgeColor', 'y', 'FaceColor', 'y');
text(0.5*(ends(2)+ends(1)), 0.6, 'TDS', 'color', 'k', 'FontSize', 9, ...
    'horizontal', 'center', 'vertical', 'middle', 'rotation', 0);

subplot(ax2);
wall = handles.eles.get('wall');
ends = [wall.startposition wall.endposition];
rectangle('Position', [ends(1) 0.1 ends(2)-ends(1) 1.], ...
    'EdgeColor', [0.65 0.65 0.65], 'FaceColor', [0.65 0.65 0.65]);
text(0.5*(ends(2)+ends(1)), 0.6, 'Wall', 'color', 'k', 'FontSize', 9, ...
    'horizontal', 'center', 'vertical', 'middle', 'rotation', 0);

und = handles.eles.get('U1');
ends = [und.startposition und.endposition];
w1 = (ends(2)-ends(1))/10.;
h1 = w1*1.6;
for k1 = 1:5
    rectangle('Position', [ends(1)+(2*k1-2)*w1 0.1 w1 h1], ...
    'EdgeColor', [0.65 0.65 0.65], 'FaceColor', [0.65 0.65 0.65]);
    rectangle('Position', [ends(1)+(2*k1-1)*w1 0.1 w1 h1], ...
    'EdgeColor', [1 0 1], 'FaceColor', [1 0 1]);
    rectangle('Position', [ends(1)+(2*k1-1)*w1 0.1+h1 w1 h1], ...
    'EdgeColor', [0.65 0.65 0.65], 'FaceColor', [0.65 0.65 0.65]);
    rectangle('Position', [ends(1)+(2*k1-2)*w1 0.1+h1 w1 h1], ...
    'EdgeColor', [1 0 1], 'FaceColor', [1 0 1]);
end
text(0.5*(ends(2)+ends(1)), 1.6, 'LCLS-I', 'color', 'k', 'FontSize', 9, ...
    'horizontal', 'center', 'vertical', 'middle', 'rotation', 0);

%% Make a list of checkboxes to add more elements to the plot      
element_list = {'Quadrupole', 'Screen', 'Steerer', 'ICT', 'Faraday Cup',...
    'BPM', 'Dipole', 'Collimator'};
color_list = {'r', 'b', 'g', 'm', 'c', 'k', [0.5843 0.8157 0.9882], [0.93 0.69 0.13]};
width_list = {0.063, 0.01, 0.040, 0.01, 0.01, 0.01, 0.01, 0.01};

check_width = 20/9.;
for i = 1:length(element_list)
    handles.check_list(i) = uicontrol('style', 'checkbox', 'units', 'centimeters',...
        'position', [1+(i-1)*check_width, 12.15, check_width, 0.6/ratio]*ratio, ...
        'string', element_list{i}, 'fontsize', 10, 'foregroundcol', color_list{i},...
        'tag', sprintf('check_list_%s', element_list{i}), 'value', 0);
end
set(handles.check_list(1), 'callback', @(src, event)check_list_Callback(src, event, handles));
set(handles.check_list(2), 'callback', @(src, event)check_list_Callback(src, event, handles));
set(handles.check_list(3), 'callback', @(src, event)check_list_Callback(src, event, handles));
set(handles.check_list(4), 'callback', @(src, event)check_list_Callback(src, event, handles));
set(handles.check_list(5), 'callback', @(src, event)check_list_Callback(src, event, handles));
set(handles.check_list(6), 'callback', @(src, event)check_list_Callback(src, event, handles));
set(handles.check_list(7), 'callback', @(src, event)check_list_Callback(src, event, handles));
set(handles.check_list(8), 'callback', @(src, event)check_list_Callback(src, event, handles));

handles.element_list = element_list;

%% Dynamically diplay the current coordinate of the mouse
xbox = uicontrol('Style', 'text', 'Tag', 'xbox', 'units', 'centimeters',...
    'position', [9*check_width, 12.1, check_width, 0.6/ratio]*ratio, ...
    'fontsize', 10);
xboxlabel = uicontrol('Style', 'text', 'units', 'centimeters',...
    'String', 'Position at: ', 'fontsize', 10, ...
    'Position',get(xbox,'position')+[-0.5*check_width 0 -0.5*check_width 0]);

callback = ['pt = get(gca,''CurrentPoint'');' ...
    'xbox = findobj(''Tag'',''xbox'');' ...
    'zpos = pt(1,1);' ...
    'if pt(1,2)>4;' ...
    'zpos = zpos-16;' ...
    'end;' ...
    'set(xbox,''String'',[num2str(zpos) '' m'']);'];
set(gcf,'WindowButtonMotionFcn',callback);



function check_list_Callback(src, event, handles)
% Checkbox callback function

vals = get(handles.check_list, 'Value');
checked = [vals{:}]
%checked = find([vals{:}]);
if isempty(checked)
   checked = 'none';
end

%% Clear the previous plot and replot everything that has been selected by using the checkbox
cla;

zend = 32;
zmid = zend/2;
ax_to_fig = zend/(zend+2.0);
ratio = 1.35;

fig = gcf;

ax1 = subplot(2, 1, 1);
ax2 = subplot(2, 1, 2);
set(ax1, 'units', 'centimeter', 'position', [1 7 20 5.]*ratio, 'YTick', []);
set(ax2, 'units', 'centimeter', 'position', [1 1 20 5.]*ratio, 'YTick', []);

subplot(ax1)
axis([0 zmid 0 5*0.75]);
xticks([0:1:zmid]);
ax1.YAxis.Visible = 'off';
ax1.XMinorTick = 'on';

subplot(ax2)
axis([zmid zend 0 5*0.75]);
xticks([zmid:1:zend]);
ax2.YAxis.Visible = 'off';
ax2.XMinorTick = 'on';
xlabel('z (m)');

element_list = upper({'Quad', 'Scr', 'St', 'ICT', 'FC', 'BPM', 'Dipole', 'Collimator'});
color_list = {'r', 'b', 'g', 'm', 'c', 'k', [0.5843 0.8157 0.9882], [0.93 0.69 0.13]};
get_color = containers.Map(element_list, color_list);

for i = length(checked):-1:1
    if checked(i) == 0
        element_list(i) = [];
    end
end

eles = handles.eles;

subplot(ax1);
[name, ends] = handles.eles.get_data('GUN', false);
rectangle('Position', [0. 0.1 ends(2) 1.], ...
    'EdgeColor', 'y', 'FaceColor', 'y');
text(0.1, 0.45, name, 'color', 'k', 'FontSize', 9, ...
    'horizontal', 'left', 'vertical', 'middle', 'rotation', 90);

[name, ends] = handles.eles.get_data('BOOSTER', false);
rectangle('Position', [ends(1) 0.1 ends(2)-ends(1) 1.], ...
    'EdgeColor', 'y', 'FaceColor', 'y');
text(0.5*(ends(2)+ends(1)), 0.6, name, 'color', 'k', 'FontSize', 9, ...
    'horizontal', 'center', 'vertical', 'middle', 'rotation', 0);

[name, ends] = handles.eles.get_data('TDS', false);
rectangle('Position', [ends(1) 0.1 ends(2)-ends(1) 1.], ...
    'EdgeColor', 'y', 'FaceColor', 'y');
text(0.5*(ends(2)+ends(1)), 0.6, 'TDS', 'color', 'k', 'FontSize', 9, ...
    'horizontal', 'center', 'vertical', 'middle', 'rotation', 0);

[name, ends] = handles.eles.get_data('TDS', false);
rectangle('Position', [ends(1) 0.1 ends(2)-ends(1) 1.], ...
    'EdgeColor', 'y', 'FaceColor', 'y');
text(0.5*(ends(2)+ends(1)), 0.6, 'TDS', 'color', 'k', 'FontSize', 9, ...
    'horizontal', 'center', 'vertical', 'middle', 'rotation', 0);

subplot(ax2);
rectangle('Position', [19 0.05 13 3.45], 'linestyle', '--', ...
    'EdgeColor', [0.5 0.5 0.5], 'FaceColor', 'none')

wall = handles.eles.get('wall');
ends = [wall.startposition wall.endposition]
rectangle('Position', [ends(1) 0.1 ends(2)-ends(1) 1.], ...
    'EdgeColor', [0.65 0.65 0.65], 'FaceColor', [0.65 0.65 0.65]);
text(0.5*(ends(2)+ends(1)), 0.6, 'Wall', 'color', 'k', 'FontSize', 9, ...
    'horizontal', 'center', 'vertical', 'middle', 'rotation', 0);

und = handles.eles.get('U1');
ends = [und.startposition und.endposition];
w1 = (ends(2)-ends(1))/10.;
h1 = w1*1.6;
for k1 = 1:5
    rectangle('Position', [ends(1)+(2*k1-2)*w1 0.1 w1 h1], ...
    'EdgeColor', [0.65 0.65 0.65], 'FaceColor', [0.65 0.65 0.65]);
    rectangle('Position', [ends(1)+(2*k1-1)*w1 0.1 w1 h1], ...
    'EdgeColor', [1 0 1], 'FaceColor', [1 0 1]);
    rectangle('Position', [ends(1)+(2*k1-1)*w1 0.1+h1 w1 h1], ...
    'EdgeColor', [0.65 0.65 0.65], 'FaceColor', [0.65 0.65 0.65]);
    rectangle('Position', [ends(1)+(2*k1-2)*w1 0.1+h1 w1 h1], ...
    'EdgeColor', [1 0 1], 'FaceColor', [1 0 1]);
end
text(0.5*(ends(2)+ends(1)), 1.6, 'LCLS-I', 'color', 'k', 'FontSize', 9, ...
    'horizontal', 'center', 'vertical', 'middle', 'rotation', 0);

%% Plot those selected by the checkbox
for k = 1:length(element_list)
    switch element_list{k}
        case upper({'Quad', 'Scr', 'St', 'ICT', 'FC', 'BPM'})
            [E_name, E_pos] = eles.get_data(element_list{k});
            for i =1:length(E_pos)
                if (E_pos(i)<=zmid)
                    subplot(ax1);
                else
                    subplot(ax2);
                end
                hold on;
                ls = '-'; y0 = 0; y1 = 0; lw = 0.01;
                if strcmp(element_list{k}, 'FC')
                    ls = ':';
                    y1 = 1;
                end
                if strcmp(element_list{k}, 'QUAD')
                    y0 = 1; y1 = 1; lw = 0.063;
                end
                if strcmp(element_list{k}, 'ST')
                    y0 = 1; y1 = 1; lw = 0.050;
                end
                if strcmp(element_list{k}, 'ST')
                    lw = 0.050;
                end
                col = get_color(element_list{k});

                %stem(E_pos(i), y0+1.5, 'linestyle', ls, 'color', col, 'Marker', 'none', 'linewidth', lw);
                rectangle('Position', [E_pos(i)-lw/2., 0, lw, y0+1.5], 'linestyle', ls,...
                    'EdgeColor', col, 'FaceColor', col);
                text(E_pos(i), y1+1.6, E_name{i}, 'color', col, 'FontSize', 8, ...
                    'horizontal', 'left', 'vertical', 'middle', 'rotation', 90);
                
            end
        case upper({'dipole'})
            [E_name, E_pos] = eles.get_data(element_list{k}, false);
            for i =1:length(E_pos)
                if (E_pos(i,1)<=zmid)
                    subplot(ax1);
                else
                    subplot(ax2);
                end
                hold on;
                col = get_color(element_list{k});
                %disp('hi');
                rectangle('Position', [E_pos(i,1) 0.1 E_pos(i,2)-E_pos(i,1) 1.], ...
                    'EdgeColor', col, 'FaceColor', col);
                text(0.5*(E_pos(i,1)+E_pos(i,2)), 1.75, E_name(i), 'color', 'k', 'FontSize', 9, ...
                    'horizontal', 'center', 'vertical', 'middle', 'rotation', 90);
            end
        case upper({'Collimator'})
            %[E_name, E_pos] = eles.get_data(element_list{k}, false);
            coll = handles.eles.get('Collimator');
            E_pos = [coll.startposition coll.endposition];

            for i =1:1
                if (E_pos(1)<=zmid)
                    subplot(ax1);
                else
                    subplot(ax2);
                end
                hold on;
                ls = '-'; y0 = 0; y1 = 0;
                col = get_color(element_list{k}); %[0.93 0.69 0.13];
                disp('hi');
                rectangle('Position', [E_pos(1) 0.1 E_pos(2)-E_pos(1) 0.075], ...
                    'EdgeColor', col, 'FaceColor', col);
                rectangle('Position', [E_pos(1) 0.25 E_pos(2)-E_pos(1) 0.075], ...
                    'EdgeColor', col, 'FaceColor', col);
                text(0.5*(E_pos(1)+E_pos(2)), 1, 'COLLIMATOR', 'color', 'k', 'FontSize', 9, ...
                    'horizontal', 'center', 'vertical', 'middle', 'rotation', 90);
            end
    end
end

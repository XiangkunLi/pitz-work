classdef Astra < handle
% Define the class for generating Astra/Generator inputs
% Note that an Astra input file includes many modules while a Generator
% input includes only one module
    properties
    % The modules are stored also as a map, for the convenience of
    % updating the modules by its name. 
    % Here, key-value pairs are (Module.name, Module), respectively
        modules = containers.Map();
        % Only the modules that is loacted between `zmin` and `zmax` are
        % considered for output
        zmin = 0;   % Starting point of the beam line
        zmax = 1e3; % Ending point of the beam line
    end
    
    methods
        function obj = Astra(modules)
        % Class construcator
            obj.modules = containers.Map();
            if nargin > 0
                if ~iscell(modules) % If not a cell list
                    obj.modules(modules.name) = modules;
                else
                    for module = modules
                        mod = module{1};
                        obj.modules(mod.name) = mod;
                    end
                end
            end
        end
        function r = add_module(obj, module)
        % Add one module to the list, could also be used to update an
        % existing module
            obj.modules(module.name) = module;
        end
        function r = add_modules(obj, modules)
        % Add multiple modules to the list. If one or more modules
        % already exist, then update them
            for module = modules
                mod = module{1};
                obj.modules(mod.name) = mod;
            end
        end
        function r = set_range(obj, zmin, zmax)
            if nargin > 0
                obj.zmin = zmin
            end
            if nargin > 1
                obj.zmax = zmax
            end
        end
        function r = write(obj, filename)
        % Write the output into a file
            if nargin == 0
                filename = 'ast.in';
            end
            % Prepare for output
            r = '';
            for key = obj.modules.keys
                mod = obj.modules(key{1});
                r = sprintf('%s%s', r, mod.output);
            end
            fid = fopen(filename,'wt');
            r = strrep(r, '\', '\\')
            fprintf(fid, r);
            fclose(fid);
        end
    end
end


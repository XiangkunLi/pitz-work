from universal import *
import subprocess
import pandas as pd

fmt = '%14.6E%14.6E%14.6E%14.6E%14.6E%14.6E%14.6E%14.6E%4d%4d'
astra_format = fmt
ast_format = astra_format
ast_fmt = astra_format

def sampling(fname, every = 17, start = 0, end = -1):
    path, name, ext = fileparts(fname)
    fnew = os.path.join(path, name+'@every_%d' % every+ext)
    data = pd_loadtxt(fname)
    
    np.savetxt(fnew, data[start:end:every],
               fmt = '%14.6E%14.6E%14.6E%14.6E%14.6E%14.6E%14.6E%14.6E%4d%4d')
    
def _exist_(st, fname):
    '''
    Check if a string `st` exists in the file `fname`
    '''
    exist = False
    if os.path.exists(fname):
        log = open(fname)
        for i, line in enumerate(log):
            if st in line:
                exist = True
    return exist

def run_sco(grads, optics, particle):

    x = grads
    for i in np.arange(len(x)):
        direc += str.format('Q%d-%.2fT_m-' %  (i+1, x[i]))
        
    cmd = 'mkdir -p %s' % direc
    os.system(cmd)
    os.chdir(direc)

    shutil.copyfile('..\optimizer.dat', 'optimizer.dat')
    shutil.copyfile('..'+os.sep+optics, optics)
    #shutil.copyfile('ast.0900.003', 'ast.0900.001')
    
    output = optics+'''
..'''+os.sep+particle
    
    filename = 'inputs.txt'
    ff = open(filename, 'w')
    ff.write(output)
    ff.close()
    
    cmd = 'sco < inputs.txt'
    os.system(cmd)
    
    return

class Module:
    '''
    Modules in Astra: newrun, output, scan, modules, error, charge, aperture,
                      wake, cavity, solenoid, quadrupole, dipole, laser
    Example:
      To add a `newrun` module, call
          newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation',\
                           Distribution = 'beam.ini', Auto_Phase = True, Track_All = True,\
                           check_ref_part = False, Lprompt = False, Max_step=200000)
      The inputs for newrun could be changed by 
          newrun.reset(Run = 2)
    '''
    starter = '&'
    ending = '&end'
    def __init__(self, name = 'Module', **kwargs):
        self.name = name.upper()
        self.kv = {}
        for key, value in kwargs.items():
            self.kv.update({key.upper():value})
        self.build()
    def set(self, *args, **kwargs):
        if len(args)>0:
            self.name = args[0].upper()
        # self.kv.update(kwargs)
        for key, value in kwargs.items():
            self.kv.update({key.upper():value})
        self.build()
        return
    def reset(self, key, value):
        key = key.upper()
        self.kv.update({key.upper():value})
        self.build()
        return
    def delete(self, key):
        key = key.upper()
        if key in self.kv.keys:
            self.kv.pop(key)
    def build(self):
        output = self.starter+self.name.lower()+' \n'
        for k, v in self.kv.items():
            k = k.lower()
            if isinstance(v, (int, np.int8, np.int16, np.int32, np.int64)) and not isinstance(v, bool):
                output += str.format(' %s=%d\n' % (k, v))
            elif isinstance(v, (float, np.float32, np.float64)):
                output += str.format(' %s=%.6E\n' % (k, v))
            elif isinstance(v, str):
                output += str.format(' %s=\'%s\'\n' % (k, v))
            elif isinstance(v, bool):
                output += str.format(' %s=%s\n' % (k, str(v)))
            elif isinstance(v, (list, np.ndarray)):
                dim = len(np.asarray(v).shape)
                if dim == 1:
                    for i, vi in enumerate(v):
                        if isinstance(vi, (int, np.int8, np.int16, np.int32, np.int64)) and not isinstance(vi, bool):
                            output += str.format(' %s(%d)=%d\n' % (k, i+1, vi))
                        elif isinstance(vi, (float, np.float32, np.float64)):
                            output += str.format(' %s(%d)=%.6E\n' % (k, i+1, vi))
                        elif isinstance(vi, str):
                            output += str.format(' %s(%d)=\'%s\'\n' % (k, i+1, vi))
                        elif isinstance(vi, bool):
                            output += str.format(' %s(%d)=%s\n' % (k, i+1, str(vi)))
                elif dim == 2:
                    for i, vi in enumerate(v):
                        if k.upper() in ['D_GAP', 'MODULE', 'AP_GR', 'Q_MULT_A', 'Q_MULT_B']:
                            for j, vij in enumerate(vi):
                                print(j, vij)
                                try:
                                    output += str.format(' %s(%d,%d)=%g\n' % (k, j+1, i+1, vij))
                                except:
                                    output += str.format(' %s(%d,%d)=\'%s\'\n' % (k, j+1, i+1, vij))
                        elif k.upper() in ['D1', 'D2', 'D3', 'D4']:
                            output += str.format(' %s(%d)=(%g,%g)\n' % (k, i+1, vi[0], vi[1]))
                        else:
                            print('Unknown key!')
                        
                            
        output += self.ending+'\n\n'
        self.output = output

class Generator1(Module):
    '''
    Renamed to `Generator1` to distinguish from `Platypus/Generator` 
    '''
    def __init__(self, **kwargs):
        Module.__init__(self, name = 'Input', **kwargs)
        self.set(Lprompt = False)
    def write(self, filename = 'gen.in'):
        ff = open(filename, 'w')
        ff.write(self.output)
        ff.close()
        return
    def run(self, fname = None, force = True):
        '''
        If force is True, it will always start Astra simulation, otherwise, it will check if the simualtion has been already done before
        '''
        if fname is None:
            fname = 'gen.in'
        
        name = fname.split('.in')[0]
        logfile = name+'.log'        
        
        #finished = _exist_('phase-space distribution saved', logfile)
        finished = False; # Always run it
        
        if not finished:
            cmd = 'generator '+fname
            with open(logfile, 'w') as fout:
                r = subprocess.call(cmd, stdout = fout)
        
        return r

class Astra:
    def __init__(self):
        self.modules = {}
    def add_module(self, module):
        self.modules.update({module.name:module})
    def add_modules(self, modules):
        for module in modules:
            self.add_module(module)
    def write(self, filename = 'ast.in'):
        output = ''
        for name, module in self.modules.items():
            output = module.output+output
        ff = open(filename, 'w')
        ff.write(output)
        ff.close()
        return
    def run(self, fname = None, force = True):
        '''
        If force is True, it will always start Astra simulation, otherwise, it will check if the simualtion has been already done before
        '''
        if fname is None:
            fname = 'ast.in'
        
        name = fname.split('.in')[0]
        logfile = name+'.log'        
        finished = _exist_('finished simulation', logfile)
        print('Is finished?', finished)
        
        r = -1
        if (not finished) or force:
            cmd = 'astra '+fname
            #os.systme(cmd+' 2>&1 | tee '+logfile)
            with open(logfile, 'w') as fout:
                r = subprocess.call(cmd, stdout = fout)     
        return r
    
    def run0(self, fname = None, force = True):
        '''
        If force is True, it will always start Astra simulation, otherwise, it will check if the simualtion has been already done before
        '''
        if fname is None:
            fname = 'ast.in'
        
        name = fname.split('.in')[0]
        logfile = name+'.log'        
        finished = _exist_('finished simulation', logfile)
        print('Is finished?', finished)
        
        r = -1
        if (not finished):
            cmd = 'astra '+fname
            os.system(cmd+' 2>&1 | tee '+logfile)
            #with open(logfile, 'w') as fout:
            #    r = subprocess.call(cmd, stdout = fout)     
        return r
    
    def qsub(self, job_name, ast_name = None, gen_name = None, direc = '.'):
        '''
        Write the batch file to the `filename`, which could be submitted to a server by "qsub filename"
        '''
        #if ast_name is None:
        #    ast_name = 'ast.in'
        #if gen_name is None:
        #    gen_name = 'gen.in'
        
        con = '''\
#!/bin/zsh
#
#$ -cwd
#$ -o '''+job_name+'''.o
#$ -e '''+job_name+'''.e
#$ -V
#$ -l h_cpu=12:00:00
#$ -l h_rss=2G
#$ -P pitz
##$ -pe multicore 32
##$ -R y

'''
        chdir = '''cd '''+direc+'''
'''
        if gen_name is not None:  
            run_generator = '''generator '''+gen_name+'''.in 2>&1 | tee '''+gen_name+'''.log
'''
        else:
            run_generator = '''\n'''
        if ast_name is not None:
            run_astra = '''astra '''+ast_name+'''.in 2>&1 | tee '''+ast_name+'''.log
'''
        else:
            run_astra = '''\n'''
        
        con = con+chdir+run_generator+run_astra
        
        ff = open(job_name+'.sh', 'w')
        ff.write(con)
        ff.close()
        return

def nemixrms(x, xp, w = None):
    '''
    Parameters
      x, xp: phase space coordinates. xp could be either divergence or normalized momentum
      w: weighting factor or charge or the particles
    Returns
      emittance: phase space emittance
    '''
    if w is None:
        w = np.ones(len(x))
    x2 = weighted_mean(x*x, w)
    xp2 = weighted_mean(xp*xp, w)
    xxp = weighted_mean(x*xp, w)
    emit_x = np.sqrt(x2*xp2-xxp*xxp)
    return emit_x
nemiyrms = nemixrms

class BeamDiagnostics:
    '''
    Postprocessing class for Astra and/or Warp simulations
    '''
    def __init__(self, fname = None, dist = None, start_index = 0, plot = False, **kwargs):
        '''
        Parameters
          fname: full name of the data file
          dist: 2D arrays that stores the distributions
          start_index: 0 by default, the starting number to refer to the calculated beam parameters
          plot: False by default, if plot current profile
          **kwargs:
            key_value = None
            tracking = 'Astra'
            momentum = 'eV/c'
            em3d = None
        '''
        
#         self.keyIndex = {'avg_z':0, 'nemit_x':1, 'nemit_y':2, 'std_x':3, 'std_y':4, 'std_z':5,\
#                          'Ekin':6, 'std_Ekin':7, 'nemit_z':8, 'Q_b':9, 'avg_x':10, 'avg_y':11,\
#                          'alpha_x':12, 'beta_x':13, 'gamma_x':14, 'emit_x':15, 'alpha_y':16,\
#                          'beta_y':17, 'gamma_y':18, 'emit_y':19, 'cor_zEk':20, 'loss_cathode':21,\
#                          'loss_aperture':22, 'FWHM':23}
        keys = ['avg_z', 'nemit_x', 'nemit_y', 'std_x', 'std_y', 'std_z', 'Ekin', 'std_Ekin', 'nemit_z', 'Q_b',
                'avg_x', 'avg_y', 'alpha_x', 'beta_x', 'gamma_x', 'emit_x', 'alpha_y', 'beta_y', 'gamma_y', 'emit_y',
                'cor_Ekin', 'loss_cathode', 'loss_aperture', 'FWHM', 'NoP', 'I1', 'I0', 'cov_xxp', 'cov_yyp', 'std_xp', 'std_yp']
        indices = np.arange(len(keys))
        units = ['m', 'um', 'um', 'mm', 'mm', 'mm', 'MeV', 'keV', 'keV mm', 'C',
                 'm', 'm', ' ', 'm', 'm^-1', 'm', ' ', 'm', 'm^-1', 'm', 'keV', ' ',
                 ' ', 'mm', ' ', 'A', 'A', 'mmmrad', 'mmmrad', 'mrad', 'mrad']
        keyIndex = {}
        keyUnit = {}
        for i in np.arange(len(keys)):
            keyIndex[keys[i]] = indices[i] + start_index
            keyUnit[keys[i]] = units[i]
        self.keyIndex = keyIndex
        self.keyUnit = keyUnit
        
        kwargs.update(plot = plot)
        if fname is not None:
            try:
                dist = pd.read_csv(fname, delimiter = '\s+', header = None).values
            except Exception as err:
                print(err)
                dist = np.loadtxt(fname)
                
            if 'tracking' in kwargs.keys():
                if kwargs['tracking'] in ['astra', 'Astra', 'ASTRA']:
                    dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
                else:
                    pass
            else:
                dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
            
            self.dist = dist
            self.diagnostics(**kwargs)
        elif dist is not None:
            self.dist = dist
            self.diagnostics(**kwargs)
        else:
            self.x = np.zeros(len(self.keyIndex))

    def __getitem__(self, key):
        return self.get(key)
    
    def __getattr__(self, key):
        return self.get(key)
    
    @property
    def x(self):
        return self.__x

    @x.setter
    def x(self, x):
        self.__x = x
        
    def get(self, key):
        '''
        Return the corresponding parameter if key is one of `self.kv.keys()`, or return the index of the key 
        if requesting by "idx_"+key and key is one of `self.kv.keys()`
        '''
        if key in self.keyIndex.keys():
            index = self.keyIndex[key]
            return self.x[index]
        elif key in ['idx_'+ i for i in self.keyIndex.keys()]:
            index = self.keyIndex[key[4:]]
            return index
        else:
            return -999
    
    def get_index(self, key):
        index = self.keyIndex[key]
        return index
        
    def diagnostics(self, key_value = None, tracking = 'Astra', momentum = 'eV/c', energy = True,\
                    momentum_z = False, em3d = None, plot = False, fig_ext = '.eps'):
        '''
        Parameters
          x y z Px Py Pz w: 6D phase space of the particles
        Outputs
          [avg_z nemit_x nemit_y std_x std_y std_z Ekin std_Ekin nemit_z Q_b\
          avg_x avg_y alpha_x beta_x gamma_x emit_x alpha_y beta_y gamma_y emit_y\
          cor_zEk loss_cathode loss_aperture FWHM NoP I1 I0]: array
        '''
        
        tracking = tracking.upper()
        if tracking in ['ASTRA', 'A']:
            ix, iy, iz, iux, iuy, iuz, iw = 0, 1, 2, 3, 4, 5, 7
            weight_to_charge = 1e-9
        elif tracking in ['WARP', 'W']:
            ix, iy, iz, iux, iuy, iuz, iw = 0, 1, 2, 3, 4, 5, 6
            weight_to_charge = g_qe
            default_momentum = 'Dimentionless'
            momentum = 'Dimentionless'
        else:
            ix, iy, iz, iux, iuy, iuz, iw = 0, 1, 2, 3, 4, 5, 6
            weight_to_charge = 1.0
        
        if key_value != None:
            keys = key_value.keys()
            if 'x' in keys:      
                ix = key_value['x']
            if 'y' in keys:
                iy = key_value['y']
            if 'z' in keys:
                iz = key_value['z']
            if 'ux' in keys:
                iux = key_value['ux']
            if 'uy' in keys:
                iuy = key_value['uy']
            if 'uz' in keys:
                iuz = key_value['uz']
            if 'iw' in keys:
                iw = key_value['iw']
        
        dist = np.copy(self.dist)
        NoP = len(dist[:,1])
        
        if tracking in ['ASTRA']:
            select = (dist[:,9]==-89); n_lost_cathode = np.sum(select)
            select = (dist[:,9]==-15); n_lost_aperture = np.sum(select)

            select = (dist[:,9]>0)
            if np.sum(select) == 0:
                select = (dist[:,9]==-1); 
                if np.sum(select) == 0:
                    self.x = np.zeros(len(self.keyIndex))
                    return
                else:
                    dist[:,iz] = dist[:,6]*1e-9*g_c

            dist = dist[select]
        else:
            n_lost_cathode = -1
            n_lost_aperture = -1
        
        
        x, y, z = dist[:,ix], dist[:,iy] ,dist[:,iz] # positions in meter
        
        momentum = momentum.upper()
        if momentum == 'EV/C':
            bgx, bgy, bgz = dist[:,iux]/g_mec2/1e6, dist[:,iuy]/g_mec2/1e6, dist[:,iuz]/g_mec2/1e6 # dimentionless momentum
        elif momentum == 'DIMENTIONLESS':
            bgx, bgy, bgz = dist[:,iux], dist[:,iuy], dist[:,iuz]
        elif momentum == 'M/S':
            bgx, bgy, bgz = dist[:,iux]/g_c, dist[:,iuy]/g_c, dist[:,iuz]/g_c
            gamma = 1.0/np.sqrt(1.0-bgx**2-bgy**2-bgz**2)
            bgx, bgy, bgz = bgx*gamma, bgy*gamma, bgz*gamma
        
        
        w = dist[:,iw]*weight_to_charge # C
        #w = np.ones(len(w))
        
        if energy:
            Ek = np.sqrt(1+bgx**2+bgy**2+bgz**2)*g_mec2-g_mec2 # kinetic energy in MeV
        else:
            Ek = np.sqrt(bgx**2+bgy**2+bgz**2)*g_mec2 # P*c in MeV/c
        
        if momentum_z:
            Ek = np.sqrt(bgz**2)*g_mec2
        
        Qtot = np.sum(w) # C
        
        x2, y2, z2 = np.apply_along_axis(np.cov, arr = dist[:,0:3], axis = 0, aweights = np.abs(w), bias = True)
        std_x, std_y, std_z = np.sqrt([x2, y2, z2])
        
        xc, yc, zmean = np.apply_along_axis(weighted_mean, arr = dist[:,0:3], axis = 0, weights = w)
        
        xp = bgx/bgz; yp = bgy/bgz
        xp2 = weighted_cov(xp, xp, w); yp2 = weighted_cov(yp, yp, w)
        xxp = weighted_cov(x, xp, w);  yyp = weighted_cov(y, yp, w)
        
        emit_x = np.sqrt(x2*xp2-xxp*xxp); emit_y = np.sqrt(y2*yp2-yyp*yyp)
        alpha_x = -xxp/emit_x; beta_x = x2/emit_x; gamma_x = xp2/emit_x
        alpha_y = -yyp/emit_y; beta_y = y2/emit_y; gamma_y = yp2/emit_y
        
        fwhm = get_FWHM(z)
        [I1, I0] = zdist(z, weights = w, plot = plot, fig_ext = fig_ext)
        
        select = (z-zmean>-1.*std_z)*(z-zmean<1.*std_z); Ek1 = Ek[select]; z1 = z[select]; w1 = w[select]
        Ek1mean = weighted_mean(Ek1, w1); z1mean = weighted_mean(z1, w1); std_z1 = weighted_std(z1, w1);
        cor_zEkin = weighted_mean(Ek1*z1, w1)-z1mean*Ek1mean; # MeV/c*m
        cor_Ekin = cor_zEkin/std_z1**2; # MeV/c/m
        
        Ek2 = Ek-Ek1mean-cor_Ekin*(z-z1mean) # higher order energy spread
        std_Ek2 = weighted_std(Ek2, w)*1e3
        #import pdb; pdb.set_trace()
        
        if plot:
            fig, [ax1, ax2] = plt.subplots(ncols = 2, figsize = (6, 3))
            ax1.plot((z[::97]-zmean)*1e3, Ek[::97], '.')
            ztmp = np.linspace(-5, 5)*1e-3
            Ektmp = Ek1mean+cor_Ekin*ztmp
            ax1.plot(ztmp*1e3, Ektmp, '-')

            ax2.plot((z[::97]-zmean)*1e3, Ek2[::97], '.')
            fig.tight_layout()
            fig.savefig('Ek-z.png')
        
        
        if em3d is not None:
            Bz = em3d.get_field(x, y, z)[:,5]
            bgx += 0.5*Bz*y*g_c/g_mec2/1e6
            bgy -= 0.5*Bz*x*g_c/g_mec2/1e6
            
        bgx2 = weighted_cov(bgx, bgx, w); bgy2 = weighted_cov(bgy, bgy, w)
        xbgx = weighted_cov(x, bgx, w);   ybgy = weighted_cov(y, bgy, w)
        
        nemit_x = np.sqrt(x2*bgx2-xbgx**2); nemit_y = np.sqrt(y2*bgy2-ybgy**2)
        
        x = np.array([zmean, nemit_x*1e6, nemit_y*1e6, std_x*1e3, std_y*1e3, std_z*1e3, 
                      weighted_mean(Ek, w), weighted_std(Ek, w)*1e3, nemixrms(z, Ek, w)*1e6, 
                      Qtot, xc, yc,
                      alpha_x, beta_x, gamma_x, emit_x, alpha_y, beta_y, gamma_y, emit_y,
                      cor_Ekin, n_lost_cathode, n_lost_aperture, fwhm*1e3, NoP, I1, I0, xxp*1e6, yyp*1e6,
                      np.sqrt(xp2)*1e3, np.sqrt(yp2)*1e3, std_Ek2])
        
        self.x = x
    
    def demo(self, basename = None):
        if basename is not None:
            filename = 'diag@'+basename+'.txt'
        else:
            filename = 'diag@'+'.txt'
        
        a = sorted(self.keyIndex.items(), key=lambda x: x[1])
        ss = ''
        for i in np.arange(len(a)):
            ss += str('%17s: %15.6E %s\n' % (a[i][0], self.x[i], self.keyUnit.get(a[i][0])))
        print(ss)

        ff = open(filename, 'w')
        ff.write(ss)
        ff.close()
        return 

def astra_demo(fname):
    pass

def warp_post(fname = None, dist = None, **kwargs):
    '''
    Parameters
      x y z bgx bgy bgz w: 6D phase space of the particles
    Outputs
      see class `BeamDiagnostics`
    Examples
    
      data = pd_loadtxt('ast.xxxx.001')
      data[1:,2] += data[0,2]
      data[1:,5] += data[0,5]
      astra_post(dist = data)
      
      or 
      
      astra_post('ast.xxxx.001')
    '''
    
    kwargs.update(tracking = 'Warp')
    kwargs.update(momentum = 'Dimentionless')
    
    diag = BeamDiagnostics(fname = fname, dist = dist, **kwargs)
    return diag.x

def astra_post(fname = None, dist = None, **kwargs):
    '''
    Parameters
      x y z Px Py Pz t w: 6D phase space of the particles
    Outputs
      see class `BeamDiagnostics`
    Examples
    
      data = pd_loadtxt('ast.xxxx.001')
      warp_post(dist = data)
      
      or 
      
      warp_post('ast.xxxx.001')
    '''
    
    kwargs.update(tracking = 'Astra')
    kwargs.update(momentum = 'eV/c')
    
    diag = BeamDiagnostics(fname = fname, dist = dist, **kwargs)
    return diag.x

def astra2slice_smooth(fname, fout = None, nslice = 100, nc = 9, momentum_z = False):
    '''
    The output file follows the format required for Genesis 1.3 simulation.
    From left the right, the column are:
      ZPOS GAMMA0 DELGAM EMITX EMITY BETAX BETAY XBEAM YBEAM PXBEAM PYBEAM ALPHAX ALPHAY CURPEAK ELOSS
    Parameters
      fname: filename of Astra output
      fout: filename of the output
      nslice: number of slices
      nc: number of slices to discard on the sides
    '''
    
    header = '? VERSION = 1.0\n'+\
    '? COLUMNS ZPOS GAMMA0 DELGAM EMITX EMITY BETAX BETAY XBEAM YBEAM PXBEAM PYBEAM ALPHAX ALPHAY CURPEAK ELOSS'
    
    data = np.loadtxt(fname)
    data[1:,2] += data[0,2]
    data[1:,5] += data[0,5]
    
    nop = len(data[:,0])
    #select = (data[:,0]>=-3e-3)*(data[:,0]<=3e-3)*(data[:,1]>=-1e-3)*(data[:,1]<=1e-3)
    select = (data[:,9]>0); data = data[select]
    nop = len(data[:,0])
    
    nc = nc
    counts, edges = np.histogram(data[:,2], bins = nslice)
    edges = (edges[:-1]+edges[1:])/2.0
    #print(counts, edges, nc)
    
    zmin, zmax = edges[0], edges[-1]
    #print(zmin, zmax)
    
    for i in np.arange(len(counts[0:nslice//2])):
        if counts[i] < nc:
            zmin = edges[i+1]; print(zmin)
    for i in np.arange(len(counts[nslice//2:])):
        if counts[nslice-i-1] < nc:
            zmax = edges[nslice-i-2]
    #print(zmin, zmax)
    
    
    dz = (zmax-zmin)/nslice
    
    r = []; zpos = 0; counts = 0
    for i in np.arange(nslice):
        
        smooth = 3 # smooth window size smooth*2+1
        zmin_i, zmax_i = zmin+i*dz-smooth*dz, zmin+i*dz+smooth*dz+dz
        
        select = (data[:,2]>=zmin_i)*(data[:,2]<=zmax_i)
        beam_i = data[select]; #print(len(beam_i))
        
        diag = BeamDiagnostics(dist = beam_i, momentum_z = momentum_z);
        Ipeak = -diag.Q_b/dz*g_c/(smooth*2+1)
        
        if i == 0:
            tmp = [zpos, kinetic2gamma(diag.Ekin), diag.std_Ekin*1e-3/g_mec2, \
                   diag.nemit_x*1e-6, diag.nemit_y*1e-6, diag.beta_x, diag.beta_y, \
                   0, 0, 0, 0, diag.alpha_x, diag.alpha_y, 0, 0]
            r.append(tmp); #counts += 1; print(counts)
            zpos += dz
            
            
        tmp = [zpos, kinetic2gamma(diag.Ekin), diag.std_Ekin*1e-3/g_mec2, \
             diag.nemit_x*1e-6, diag.nemit_y*1e-6, diag.beta_x, diag.beta_y, \
             0, 0, 0, 0, diag.alpha_x, diag.alpha_y, Ipeak, 0]
        r.append(tmp); #counts += 1; print(counts)
        
        zpos += dz
        
        if i == nslice-1:
            #zpos -= dz/2.0
            tmp = [zpos, kinetic2gamma(diag.Ekin), diag.std_Ekin*1e-3/g_mec2, \
                   diag.nemit_x*1e-6, diag.nemit_y*1e-6, diag.beta_x, diag.beta_y, \
                   0, 0, 0, 0, diag.alpha_x, diag.alpha_y, 0, 0]
            r.append(tmp); #counts += 1; print(counts)
            
            
    if fout is None:
        fout = 'slice@'+fname+'.dat'
    print('The distribution is saved to '+fout)
    np.savetxt(fout, np.array(r), fmt = '%-15.6e', \
               header = header, comments='')
    return np.array(r)

def astra2slice(fname, fout = None, nslice = 100, nc = 9, momentum_z = False):
    '''
    The output file follows the format required for Genesis 1.3 simulation.
    From left the right, the column are:
      ZPOS GAMMA0 DELGAM EMITX EMITY BETAX BETAY XBEAM YBEAM PXBEAM PYBEAM ALPHAX ALPHAY CURPEAK ELOSS
    Parameters
      fname: filename of Astra output
      fout: filename of the output
      nslice: number of slices
      nc: number of slices to discard on the sides
    '''
    
    header = '? VERSION = 1.0\n'+\
    '? COLUMNS ZPOS GAMMA0 DELGAM EMITX EMITY BETAX BETAY XBEAM YBEAM PXBEAM PYBEAM ALPHAX ALPHAY CURPEAK ELOSS'
    
    data = np.loadtxt(fname)
    data[1:,2] += data[0,2]
    data[1:,5] += data[0,5]
    
    nop = len(data[:,0])
    #select = (data[:,0]>=-3e-3)*(data[:,0]<=3e-3)*(data[:,1]>=-1e-3)*(data[:,1]<=1e-3)
    select = (data[:,9]>0); data = data[select]
    nop = len(data[:,0])
    
    nc = nc
    counts, edges = np.histogram(data[:,2], bins = nslice)
    edges = (edges[:-1]+edges[1:])/2.0
    #print(counts, edges, nc)
    
    zmin, zmax = edges[0], edges[-1]
    #print(zmin, zmax)
    
    for i in np.arange(len(counts[0:nslice//2])):
        if counts[i] < nc:
            zmin = edges[i+1]; print(zmin)
    for i in np.arange(len(counts[nslice//2:])):
        if counts[nslice-i-1] < nc:
            zmax = edges[nslice-i-2]
    #print(zmin, zmax)
    
    
    dz = (zmax-zmin)/nslice
    
    r = []; zpos = 0; counts = 0
    for i in np.arange(nslice):
        zmin_i, zmax_i = zmin+i*dz, zmin+i*dz+dz
        select = (data[:,2]>=zmin_i)*(data[:,2]<=zmax_i)
        beam_i = data[select]; #print(len(beam_i))
        
        diag = BeamDiagnostics(dist = beam_i, momentum_z = momentum_z);
        Ipeak = -diag.Q_b/dz*g_c
        
        if i == 0:
            tmp = [zpos, kinetic2gamma(diag.Ekin), diag.std_Ekin*1e-3/g_mec2, \
                   diag.nemit_x*1e-6, diag.nemit_y*1e-6, diag.beta_x, diag.beta_y, \
                   0, 0, 0, 0, diag.alpha_x, diag.alpha_y, 0, 0]
            r.append(tmp); #counts += 1; print(counts)
            zpos += dz/2.0
            
            
        tmp = [zpos, kinetic2gamma(diag.Ekin), diag.std_Ekin*1e-3/g_mec2, \
             diag.nemit_x*1e-6, diag.nemit_y*1e-6, diag.beta_x, diag.beta_y, \
             0, 0, 0, 0, diag.alpha_x, diag.alpha_y, Ipeak, 0]
        r.append(tmp); #counts += 1; print(counts)
        
        zpos += dz
        
        if i == nslice-1:
            zpos -= dz/2.0
            tmp = [zpos, kinetic2gamma(diag.Ekin), diag.std_Ekin*1e-3/g_mec2, \
                   diag.nemit_x*1e-6, diag.nemit_y*1e-6, diag.beta_x, diag.beta_y, \
                   0, 0, 0, 0, diag.alpha_x, diag.alpha_y, 0, 0]
            r.append(tmp); #counts += 1; print(counts)
            
            
    if fout is None:
        fout = 'slice@'+fname+'.dat'
    print('The distribution is saved to '+fout)
    np.savetxt(fout, np.array(r), fmt = '%-15.6e', \
               header = header, comments='')
    return np.array(r)

def astra2warp(fname, fout = None, Q_coef = -1.0, High_res = True):
    '''
    The output file follows the format required for Warp simulation.
    From left the right, the column are:
      X Y Z UX UY UZ W, where Ux Uy Uz are dimentionless momentum, W is macro particle charge
    Parameters
      fname: filename of Astra output
      fout: filename of the output
      Q_coef: an coefficient to scale the bunch charge, default is -1.0
    '''
    data = np.loadtxt(fname)
    data[1:,2] += data[0,2]
    data[1:,5] += data[0,5]
    
    select = (data[:,9]>0); data = data[select]
    data[:,3] = data[:,3]/g_mec2/1e6
    data[:,4] = data[:,4]/g_mec2/1e6
    data[:,5] = data[:,5]/g_mec2/1e6
    
    data[:,6] = data[:,7]*1e-9/g_qe*Q_coef # number of electrons for each macro particle
    
    if fout is None:
        fout = fname+'.warp'
    print('The distribution is saved to '+fout)
    if High_res:
        fmt = '%20.12E'
    else:
        fmt = '%14.6E'
    np.savetxt(fout, data[:,0:7], fmt = fmt)
    return data[:,0:7]

def warp2astra(fname, fout = None, Run = 1, Q_coef = -1.0, ratio = 100):
    '''
    The output file follows the format required for Astra simulation.
    Parameters
      fname: filename of Warp output, which includes columns of X Y Z UX UY UZ W, where Ux Uy Uz are dimentionless momentum, W is macro particle charge
      fout: filename of the output
      Q_coef: an coefficient to scale the bunch charge, default is -1.0
      ratio: a factor used to scale the position of electron bunch to be used in Astra file name
    '''
    
    data = np.loadtxt(fname)
    data[:,3:6] *= g_mec2*1e6
    
    z0 = weighted_mean(data[:,2], data[:,-1])
    pz0 = weighted_mean(data[:,5], data[:,-1])
    
    data[:,2] -= z0
    data[:,5] -= pz0; #print pz0; return
    data[:,6] *= (-g_qe*1e9) # convert to nC
    
    nop = len(data)
    d1 = np.zeros((nop+1, 10))
    d1[0, 2] = z0
    d1[0, 5] = pz0
    
    
    d1[1:,0:6] = data[:,0:6]
    d1[1:,7] = data[:,6]
    d1[:, -2] = 1
    d1[:, -1] = 5
    
    
    if fout is None:
        print('The current bunch center is at ', z0, ' meters')
        fid = z0*ratio
        while round(fid)<1:
            fid *= 10
        fid = round(fid)
        fout = 'ast.%04d.%03d' % (fid, Run)
    print('The distribution is saved to '+fout)
    
    np.savetxt(fout, d1, fmt = '%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%4d%4d')
    return d1

###
# workdir = r'C:\Users\lixiangk\Desktop\Backup\sim\2018\LCLS-I-matching2\_n200k-sig_x-1.47mm-sig_y-0.32mm-alp_x-8.08-alp_y-4.73'
# os.chdir(workdir)
# 
# astra2warp('ast.0005.005', 'beam_und@50um.warp')
# warp2astra('beam-48000.dat', 1)
###
"""
def read_pitz(filename = 'PITZbeamlines.xlsx'):
    
    QuadPos, ScrPos = [], []
    if os.path.exists(filename):
        print('Hi\n')
        a = pd.read_excel(filename, usecols = "A:G,I",\
                      index_col = None, skiprows = 3, na_values=['NA'])
    else:
        return [QuadPos, ScrPos]
    
    [section, name, element, start, end, middle, length, modul_length] = a.columns

    for i in a.index:
        ni = a.loc[i, name]
        if ni == ni and 'DISP' not in `ni` and 'Quadrupol' in `a.loc[i, element]`:
                QuadPos.append(a.loc[i, middle]/1e3) # mm to meter

    for i in a.index:
        ni = a.loc[i, name]
        if ni == ni:
            if 'DISP' not in ni and ('.Scr' in ni or '.SCR' in ni):
                if a.loc[i, middle] == a.loc[i, middle]:
                    ScrPos.append(a.loc[i, middle]/1e3) # mm to meter
                else:
                    ScrPos.append(a.loc[i+1, middle]/1e3) # mm to meter

    return [QuadPos, ScrPos]
"""

### Functions to access the default statistics from Astra simulation
def get_nemixrms(fname = 'ast.Xemit.001'):
    xemit = np.loadtxt(fname)
    return xemit[-1,5]
def get_nemixrms_at(z, fname = 'ast.Xemit.001'):
    xemit = np.loadtxt(fname)
    fxemit = interp1d(xemit[:,0], xemit[:,5])
    return fxemit(z)

def get_nemiyrms(fname = 'ast.Yemit.001'):
    yemit = np.loadtxt(fname)
    return yemit[-1,5]
def get_nemiyrms_at(z, fname = 'ast.Yemit.001'):
    yemit = np.loadtxt(fname)
    fyemit = interp1d(yemit[:,0], yemit[:,5])
    return fyemit(z)

def get_ref_momentum(fname = 'ast.ref.001'):
    ref = np.loadtxt(fname)
    return ref[-1,2]

### Functions to calculate statistics using particle distributions from Astra simulation 
def cal_momentum(fname = 'beam.ini'):
    beam = np.loadtxt(fname)
    beam[1:,5] += beam[0,5]
    return np.mean(np.sqrt(beam[:,3]**2+beam[:,4]**2+beam[:,5]**2))
def cal_kinetic(fname = 'beam.ini'):
    momentum = cal_momentum(fname)
    return momentum2kinetic(momentum)

def cal_nemixrms(fname = 'beam.ini'):
    beam = np.loadtxt(fname)
    return nemixrms(beam[:,0], beam[:,3])/g_mec2/1e6*1e6
def cal_nemiyrms(fname = 'beam.ini'):
    beam = np.loadtxt(fname)
    return nemixrms(beam[:,1], beam[:,4])/g_mec2/1e6*1e6


#print cal_nemixrms(), cal_nemiyrms()

### Generate 1D and 2D statistics from raw samples
def hist_centered(x, bins = None, range = None, weights = None, **kwargs):
    '''
    Returns
      counts, centers: 1d arrays including each bin's count and center, respectively
    '''
    if bins is None:
        bins = 100

    counts, edges = np.histogram(x, bins = bins, range = range, weights = weights, **kwargs)
    centers = edges[1:]-0.5*(edges[1]-edges[0])
    return counts, centers

def hist2d_scattered(x, y, bins = [100, 100], range = None, weights = None, **kwargs):
    '''
    Returns
      xs, ys, cs: 1d arrays, (xs, ys) are the coordinates and cs the number of particles at (xs, ys)
    '''
    counts, xedges, yedges = np.histogram2d(x, y, bins = bins, range = range, weights = weights, **kwargs)
    xs = np.array([xedges[i]+0.5*(xedges[1]-xedges[0]) for i in np.arange(len(xedges)-1) for j in np.arange(len(yedges)-1)])
    ys = np.array([yedges[j]+0.5*(yedges[1]-yedges[0]) for i in np.arange(len(xedges)-1) for j in np.arange(len(yedges)-1)])
    cs = np.array([counts[i,j] for i in np.arange(len(xedges)-1) for j in np.arange(len(yedges)-1)])
    select = (cs>0)
    return xs[select], ys[select], cs[select]

def hist2d_contourf(x, y, weights = None, bins = [100, 100], range = None):
    '''
    Returns
      xs, ys, cs: 1d arrays, (xs, ys) are the coordinates and cs the number of particles at (xs, ys)
    '''
    counts, xedges, yedges = np.histogram2d(x, y, bins = bins, range = range, weights = weights)
    xs = np.array([xedges[i]+0.5*(xedges[1]-xedges[0]) for i in np.arange(len(xedges)-1)])
    ys = np.array([yedges[j]+0.5*(yedges[1]-yedges[0]) for j in np.arange(len(yedges)-1)])
    ax, ay = np.meshgrid(xs, ys, indexing = 'ij')
    return ax, ay, counts


### plot 1D distribution
def zdist(x, bins = 100, range = None, weights = None, smooth = False, fig_ext = '.eps', plot = True, **fig_kw):
    '''
    Parameters
      x: longitudinal coordinates in mm
    '''
    
    counts, centers = hist_centered(x, bins = bins, range = range, weights = weights)
    dz = centers[1]-centers[0]
    
    currents = np.abs(counts/dz*g_c); Ipeak0 = np.max(currents)
    if Ipeak0 > 1000:
        ratio = 1e-3
        ylabel = r'$I$ (kA)'
    else:
        ratio = 1
        ylabel = r'$I$ (A)'
        
    if plot:
        fig, ax = plt.subplots(**fig_kw)
        ax.plot(centers/g_c*1e12, currents*ratio, '-')
    
    if smooth:
        currents = smooth_easy(currents, 5); Ipeak1 = np.max(currents)
    else:
        Ipeak1 = Ipeak0
        
    if plot:
        if smooth:
            ax.plot(centers/g_c*1e12, currents*ratio, '--')
    
        ax.grid()
        #ax.set_xlabel(r'$\xi$ (mm)')
        ax.set_xlabel(r'$t_0$ (ps)')
        ax.set_ylabel(ylabel)
        #ax.set_xlim(-15, 15)
        #ax.set_ylim(0, 200)
        fig.savefig('dQ_dt-t'+fig_ext)
    return [Ipeak1, Ipeak0]

def xdist(x, bins = 50, range = None, weights = None, fig_ext = '.eps', **fig_kw):
    '''
    Parameters
      x: horizontal coordinates in mm
    '''
    fig, ax = plt.subplots(**fig_kw)
    h = ax.hist(x, bins = bins, range = range, weights = weights, histtype = 'step')
    #ax.grid()
    ax.set_xlabel(r'$x$ (mm)')
    if range == None:
        ax.set_ylabel(r'd$Q/$d$x$ (arb. unit)')
    else:
        ax.set_ylabel(r'd$Q/$d$x$ (pC/mm)')
    #ax.set_xlim(-15, 15)
    #ax.set_ylim(0, 200)
    fig.savefig('dQ_dx-x'+fig_ext)
    return

def ydist(y, bins = 50, range = None, weights = None, fig_ext = '.eps', **fig_kw):
    '''
    Parameters
      x: vertical coordinates in mm
    '''
    fig, ax = plt.subplots(**fig_kw)
    h = ax.hist(x, bins = bins, range = range, weights = weights, histtype = 'step')
    #ax.grid()
    ax.set_xlabel(r'$y$ (mm)')
    if range == None:
        ax.set_ylabel(r'd$Q/$d$y$ (arb. unit)')
    else:
        ax.set_ylabel(r'd$Q/$d$y$ (pC/mm)')
    #ax.set_xlim(-15, 15)
    #ax.set_ylim(0, 200)
    fig.savefig('dQ_dy-y'+fig_ext)
    return

def pdist(z, bins = 50, range = None, weights = None, fig_ext = '.eps', **fig_kw):
    '''
    Parameters
      p: momentum in MeV/c
    '''
    fig, ax = plt.subplots(**fig_kw)
    h = ax.hist(z, bins = bins, range = range, weights = weights, histtype = 'step')
    #ax.grid()
    ax.set_xlabel(r'$P$ (MeV/c)')
    if range == None:
        ax.set_ylabel(r'd$Q/$d$P$ (arb. unit)')
    else:
        ax.set_ylabel(r'd$Q/$d$P$ (pC/mm)')
    #ax.set_xlim(-15, 15)
    #ax.set_ylim(0, 200)
    fig.savefig('dQ_dP-P'+fig_ext)
    return

def tdist(t, bins = 50, range = None, weights = None, fig_ext = '.eps', **fig_kw):
    '''
    Parameters
      t: temperal coordinates in ps
    '''
    fig, ax = plt.subplots(**fig_kw)
    h = ax.hist(t, bins = bins, range = range, weights = weights, histtype = 'step')
    #ax.grid()
    ax.set_xlabel(r'$t$ (ps)')
    if range == None:
        ax.set_ylabel(r'd$Q/$d$t$ (arb. unit)')
    else:
        ax.set_ylabel(r'd$Q/$d$t$ (pC/ps)')
    #ax.set_xlim(-15, 15)
    #ax.set_ylim(0, 200)
    fig.savefig('dQ_dt-t'+fig_ext)
    return


### plot slice parameters
def plot_slice(fname, fig_ext = '.eps'):
    
    d1 = astra2slice(fname, 'slice@'+fname+'.dat', nc = 1)
    
    zz = (d1[:,0]-np.mean(d1[:,0]))*1e3
    II = d1[:,-2]; print('Peak current is ', II.max(), ' A')
    ee = d1[:,3]*1e6

    fig, ax = plt.subplots()
    ax.plot(zz, II, 'r-')
    ax.plot([-1], [-1], 'b--')
    ax.set_xlabel(r'$\xi$ (mm)')
    ax.set_ylabel(r'Current (A)', color = 'r')
    #ax.set_xlim(-5, 5)
    #ax.set_ylim(0, 200)
    ax.tick_params(axis='y', colors='r')
    ax.grid()
    ax.legend(['current', 'emittance'])

    ax1 = ax.twinx()
    ax1.plot(zz, ee, 'b--')
    ax1.set_ylabel(u_emi_x, color = 'b')
    #ax1.set_ylim(0., 10)
    ax1.tick_params(axis='y', colors='b')

    fig.savefig('slice@'+fname+fig_ext)
    

### plot 2D phase space distributions
def plot2d(x, y, weights = None, xlabel = r'$x$ (mm)', ylabel = r'$y$ (mm)', figname = 'xy2d',\
           fig_ext = '.eps', vmin = 0.0001, vmax = 1., bins = None, extent = None, returned = False, **kwargs):
    '''
    Parameters
      bins: None or [xbins, ybins], set the sampling frequencies for x and y
      extent: None or [xmin, xmax, ymin, ymax], set the lower limit and upper limit of the x-axis and y-axis
    '''
    if bins == None:
        xbins = ybins = 100
    else:
        xbins, ybins = bins
    if extent == None:
        xs = np.sort(x[::5])
        xavg = np.mean(xs); dx = xs[-int(0.005*len(xs))]-xs[int(0.005*len(xs))]
        xavg = np.mean(xs); dx = np.max(xs)-np.min(xs)
        xmin, xmax = xavg-dx*0.75, xavg+dx*0.75; print(xavg, dx, xmin, xmax)
        
        ys = np.sort(y[::5])
        yavg = np.mean(ys); dy = ys[-int(0.005*len(ys))]-ys[int(0.005*len(ys))]
        yavg = np.mean(ys); dy = np.max(ys)-np.min(ys)
        ymin, ymax = yavg-dy*0.75, yavg+dy*0.75
    else:
        xmin, xmax, ymin, ymax = extent
    print('extent: ', xmin, xmax, ymin, ymax)
    
    fig = plt.figure(figsize=(4, 3.375))
    ax1 = fig.add_axes([0.18, 0.15, 0.675, 0.15]); ax1.patch.set_alpha(0)
    ax3 = fig.add_axes([0.18, 0.15, 0.15, 0.80]); ax3.patch.set_alpha(0)
    ax2 = fig.add_axes([0.18, 0.15, 0.75, 0.80]); ax2.patch.set_alpha(0)

    cnts, cens = hist_centered(x, bins=xbins, range=(xmin,xmax), weights=weights)
    ax1.plot(cens, smooth_easy(cnts, 4), 'b-')

    # vmin is the lower limit of the colorbar, and is free to change
    # xs, ys, cs = hist2d_scattered(x, y, bins=[xbins, ybins], range=[[xmin, xmax],[ymin, ymax]], weights=weights)
    # cax = ax2.scatter(xs, ys, s=2, c=cs/np.max(cs), edgecolor='', vmin=vmin, norm=mpl.colors.LogNorm())
   
    ax, ay, signal = hist2d_contourf(x, y, bins=[xbins, ybins],
                                     range=[[xmin, xmax],[ymin, ymax]],
                                     weights=weights)
    signal = np.abs(signal)
    signal = signal/np.max(signal)
    v = np.linspace(vmin, vmax, 99)
    #from matplotlib import ticker
    cax = ax2.contourf(ax, ay, signal, v)
    
    ## Estimate the 2D histograms
    #H, xedges, yedges = np.histogram2d(x, y, bins = (xbins, ybins), range = [[xmin, xmax],[ymin, ymax]], weights = weights)
    #H = np.abs(H)
    #H /= np.max(H)
    ## H needs to be rotated and flipped
    #H = np.rot90(H)
    #H = np.flipud(H)
    ## Mask zeros
    #Hmasked = np.ma.masked_where(H==0, H) # Mask pixels with a value of zero
    ## Plot
    #cax = ax2.pcolormesh(xedges, yedges, Hmasked)
    
    #cbar = fig.colorbar(cax)
    cbar = fig.colorbar(cax, fraction=0.09, pad=0.01, format = '%.1f')
    cbar.set_ticks([vmin, 1.0])
    cbar.ax.tick_params(labelsize=11, pad=0)

    cnts, cens = hist_centered(y, bins=ybins, range=(ymin,ymax), weights=weights)
    ax3.plot(smooth_easy(cnts, 4), cens, 'b-')

    ax1.axis('off')
    ax1.set_xlim(xmin, xmax)

    ax2.set_xlim(xmin, xmax)
    ax2.set_ylim(ymin, ymax)
    ax2.set_xlabel(xlabel)
    ax2.set_ylabel(ylabel, labelpad=-1)
    ax2.minorticks_on()
    ax2.grid()

    ax3.axis('off')
    ax3.set_ylim(ymin, ymax)

    fig.savefig(figname+fig_ext)
    
    if returned:
        return [ax, ay, signal]
    return

# In[41]:

def plot2dx2(x, y, x1, y1, weights = None, xlabel = r'$x$ (mm)', ylabel = r'$y$ (mm)', figname = 'xy2d',\
           fig_ext = '.eps', vmin = 0.01, vmax = 1.0, bins = None, extent = None, **kwargs):
    '''
    Parameters
      bins: None or [xbins, ybins], set the sampling frequencies for x and y
      extent: None or [xmin, xmax, ymin, ymax], set the lower limit and upper limit of the x-axis and y-axis
    '''
    if bins == None:
        xbins = ybins = 100
    else:
        xbins, ybins = bins
    if extent == None:
        xavg = np.mean(x); dx = np.max(x)-np.min(x)
        xmin, xmax = xavg-dx*0.75, xavg+dx*0.75
        yavg = np.mean(y); dy = np.max(y)-np.min(y)
        ymin, ymax = yavg-dy*0.75, yavg+dy*0.75
    else:
        xmin, xmax, ymin, ymax = extent
    
    fig = plt.figure(figsize=(4, 3.375))
    ax1 = fig.add_axes([0.18, 0.15, 0.675, 0.15]); ax1.patch.set_alpha(0)
    ax3 = fig.add_axes([0.18, 0.15, 0.15, 0.80]); ax3.patch.set_alpha(0)
    ax2 = fig.add_axes([0.18, 0.15, 0.75, 0.80]); ax2.patch.set_alpha(0)

    cnts, cens = hist_centered(x, bins=xbins, range=(xmin,xmax), weights=weights)
    #ax1.plot(cens, smooth_easy(cnts, 8), 'b-')

    #xs, ys, cs = hist2d_scattered(x, y, bins=[xbins, ybins], range=[[xmin, xmax],[ymin, ymax]], weights=weights)
    ## vmin is the lower limit of the colorbar, and is free to change
    #cax = ax2.scatter(xs, ys, s=2, c=cs/np.max(cs), edgecolor='', vmin=vmin, norm=mpl.colors.LogNorm())
    
    ax, ay, signal = hist2d_contourf(x, y, bins=[xbins, ybins], range=[[xmin, xmax],[ymin, ymax]], weights=weights)
    signal = np.abs(signal)
    signal = signal/np.max(signal)
    v = np.linspace(vmin, vmax, 99)
    cax = ax2.contourf(ax, ay, signal, v)
    
    #xs1, ys1, cs1 = hist2d_scattered(x1, y1, bins=[xbins, ybins], range=[[xmin, xmax],[ymin, ymax]], weights=weights)
    ## vmin is the lower limit of the colorbar, and is free to change
    #cax = ax2.scatter(xs1, ys1, s=2, c=cs1/np.max(cs1), edgecolor='', vmin=vmin, norm=mpl.colors.LogNorm())
    
    ax, ay, signal = hist2d_contourf(x1, y1, bins=[xbins, ybins], range=[[xmin, xmax],[ymin, ymax]], weights=weights)
    signal = np.abs(signal)
    signal = signal/np.max(signal)
    v = np.linspace(vmin, vmax, 99)
    cax = ax2.contourf(ax, ay, signal, v)
    
    cbar = fig.colorbar(cax, fraction=0.09, pad=0.01)
    cbar.set_ticks([vmin, 1.0])
    cbar.ax.tick_params(labelsize=11, pad=0)

    cnts, cens = hist_centered(y, bins=ybins, range=(ymin,ymax), weights=weights)
    #ax3.plot(smooth_easy(cnts, 4), cens, 'b-')

    ax1.axis('off')
    ax1.set_xlim(xmin, xmax)

    ax2.set_xlim(xmin, xmax)
    ax2.set_ylim(ymin, ymax)
    ax2.set_xlabel(xlabel)
    ax2.set_ylabel(ylabel, labelpad=0)
    ax2.minorticks_on()
    ax2.grid()

    ax3.axis('off')
    ax3.set_ylim(ymin, ymax)

    fig.savefig(figname+fig_ext)
    return

def plot2d_xpx_ypy(fname = 'ast.0500.001', fig_ext = '.eps', **kwargs):
    '''
    Parameters:
      **kwargs: bins = (xbins, ybins), extent = [xmin, xmax, ymin, ymax]
    Returns:
      A saved figure
    '''

    beam = np.loadtxt(fname)
    beam[1:,2] += beam[0, 2]
    beam[1:,5] += beam[0, 5]
    
    select = (beam[:,9] == 5)|(beam[:,9] == -1)|(beam[:,9] == -3)
    beam = beam[select]
    
    x, y, w = beam[:,0]*1e3, beam[:,3]/1e3, -beam[:,7]*1e3
    x1, y1 = beam[:,1]*1e3, beam[:,4]/1e3
    plot2dx2(x = x, y = y, x1 = x1, y1 = y1, weights = w, xlabel = r'$x$ or $y$ (mm)',\
             ylabel = r'$p_x$ or $p_y$ (keV/$c$)', figname = 'xpx+ypy', fig_ext = fig_ext, **kwargs)
    
    return

def plot2d_all(fname = 'ast.0500.001', dist = None, fig_ext = '.eps', **kwargs):
    '''
    Parameters:
      **kwargs: bins = (xbins, ybins), extent = [xmin, xmax, ymin, ymax]
    Returns:
      A saved figure
    '''
    if dist is None:
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0, 2]
        dist[1:,5] += dist[0, 5]
    
    select = (dist[:,9] > 0)|(dist[:,9] == -1)|(dist[:,9] == -3)
    dist = dist[select]
    
    #tdist(t = beam[:,6]*1e3, bins = 60, weights = -beam[:,7]*1e3*2, range = (-15, 15))
    
    x, y, w = dist[:,0]*1e3, dist[:,1]*1e3, -dist[:,7]*1e3
    plot2d(x = x, y = y, weights = w, fig_ext = fig_ext, **kwargs)

    x, y, w = dist[:,0]*1e3, dist[:,3]/1e6, -dist[:,7]*1e3
    plot2d(x = x, y = y, weights = w, ylabel = r'$p_x$ (MeV/$c$)', figname = 'xpx', fig_ext = fig_ext, **kwargs)

    x, y, w = dist[:,2]*1e3, dist[:,5]/1e6, -dist[:,7]*1e3
    plot2d(x = x, y = y, weights = w, xlabel = r'$z$ (mm)', ylabel = r'$p_z$ (MeV/$c$)', figname = 'zpz', fig_ext = fig_ext, **kwargs)
    
    return

def plot2d_xy(fname = 'ast.0500.001', dist = None, fig_ext = '.eps', **kwargs):
    '''
    Parameters:
      **kwargs: bins = (xbins, ybins), extent = [xmin, xmax, ymin, ymax]
    Returns:
      A saved figure
    '''

    if dist is None:
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0, 2]
        dist[1:,5] += dist[0, 5]
    
    select = (dist[:,9] > 0)|(dist[:,9] == -1)|(dist[:,9] == -3)
    dist = dist[select]
    
    x, y, w = dist[:,0]*1e3, dist[:,1]*1e3, -dist[:,7]*1e3
    plot2d(x = x, y = y, weights = w, fig_ext = fig_ext, **kwargs)
   
    return

def plot2d_xpx(fname = 'ast.0500.001', dist = None, fig_ext = '.eps', **kwargs):
    '''
    Parameters:
      **kwargs: bins = (xbins, ybins), extent = [xmin, xmax, ymin, ymax]
    Returns:
      A saved figure
    '''

    if dist is None:
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0, 2]
        dist[1:,5] += dist[0, 5]
    
    select = (dist[:,9] > 0)|(dist[:,9] == -1)|(dist[:,9] == -3)
    dist = dist[select]
    
    x, y, w = dist[:,0]*1e3, dist[:,3]/1e3, -dist[:,7]*1e3
    plot2d(x = x, y = y, weights = w, ylabel = r'$p_x$ (keV/$c$)', figname = 'xpx', fig_ext = fig_ext, **kwargs)
    
    return

def plot2d_xxp(fname = 'ast.0500.001', dist = None, fig_ext = '.eps', **kwargs):
    '''
    Parameters:
      **kwargs: bins = (xbins, ybins), extent = [xmin, xmax, ymin, ymax]
    Returns:
      A saved figure
    '''

    if dist is None:
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0, 2]
        dist[1:,5] += dist[0, 5]
    
    select = (dist[:,9] > 0)|(dist[:,9] == -1)|(dist[:,9] == -3)
    dist = dist[select]
    
    x, y, w = dist[:,0]*1e3, dist[:,3]/dist[:,5]*1e3, -dist[:,7]*1e3
    plot2d(x = x, y = y, weights = w, ylabel = r'$x^{\prime}$ (mrad)', figname = 'xxp', fig_ext = fig_ext, **kwargs)
    
    return

def plot2d_ypy(fname = 'ast.0500.001', dist = None, fig_ext = '.eps', **kwargs):
    '''
    Parameters:
      **kwargs: bins = (xbins, ybins), extent = [xmin, xmax, ymin, ymax]
    Returns:
      A saved figure
    '''

    if dist is None:
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0, 2]
        dist[1:,5] += dist[0, 5]
    
    select = (dist[:,9] > 0)|(dist[:,9] == -1)|(dist[:,9] == -3)
    dist = dist[select]
    
    x, y, w = dist[:,1]*1e3, dist[:,4]/1e3, -dist[:,7]*1e3
    plot2d(x = x, y = y, weights = w, xlabel = r'$y$ (mm)', ylabel = r'$p_y$ (keV/$c$)', figname = 'ypy', fig_ext = fig_ext, **kwargs)
    
    return

def plot2d_yyp(fname = 'ast.0500.001', dist = None, fig_ext = '.eps', **kwargs):
    '''
    Parameters:
      **kwargs: bins = (xbins, ybins), extent = [xmin, xmax, ymin, ymax]
    Returns:
      A saved figure
    '''

    if dist is None:
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0, 2]
        dist[1:,5] += dist[0, 5]
    
    select = (dist[:,9] > 0)|(dist[:,9] == -1)|(dist[:,9] == -3)
    dist = dist[select]
    
    x, y, w = dist[:,1]*1e3, dist[:,4]/dist[:,5]*1e3, -dist[:,7]*1e3
    plot2d(x = x, y = y, weights = w, xlabel = r'$y$ (mm)', ylabel = r'$y^{\prime}$ (mrad)', figname = 'yyp', fig_ext = fig_ext, **kwargs)
    
    return

def plot2d_zpz(fname = 'ast.0500.001', dist = None, fig_ext = '.eps', **kwargs):
    '''
    Parameters:
      **kwargs: bins = (xbins, ybins), extent = [xmin, xmax, ymin, ymax]
    Returns:
      A saved figure
    '''

    if dist is None:
        dist = np.loadtxt(fname)
        #dist[1:,2] += dist[0, 2]
        dist[0, 2] = 0
        dist[1:,5] += dist[0, 5]
    
    select = (dist[:,9] > 0)
    if np.sum(select)>0:
        pass
    else:
        select = (dist[:,9] == -1)|(dist[:,9] == -3)
        if np.sum(select) > 0:
            dist[:,2] = dist[:,6]*1e-9*g_c
        else:
            return
    
    dist = dist[select]
    
    x, y, w = dist[:,2]*1e3, dist[:,5]/1e6, -dist[:,7]*1e3
    plot2d(x = x, y = y, weights = w, xlabel = r'$\xi$ (mm)', ylabel = r'$p_z$ (MeV/$c$)', figname = 'zpz', fig_ext = fig_ext, **kwargs)
    
    return

### plot evolutions of beam parameters along z
def plot_avg_xy(prefix = 'ast', suffix = '001', fig_ext = '.eps', extent = None, **fig_kw):
    xemit = np.loadtxt(prefix+'.Xemit.'+suffix)
    yemit = np.loadtxt(prefix+'.Yemit.'+suffix)
    
    fig, ax = plt.subplots(**fig_kw)
    ax.plot(xemit[:,0], xemit[:,2], '-')
    ax.plot(yemit[:,0], yemit[:,2], '-')
    ax.grid()
    ax.set_xlabel(r'$z$ (m)')
    ax.set_ylabel(r'$x, y$ (mm)')
    if extent is not None:
        xmin, xmax, ymin, ymax = extent
        ax.set_xlim(xmin, xmax)
        ax.set_ylim(ymin, ymax)
    ax.legend(['$x$', '$y$'])
    fig.savefig('avg_xy-z'+fig_ext)
    return

def plot_rms_xy(prefix = 'ast', suffix = '001', fig_ext = '.eps', extent = None, **fig_kw):
    xemit = np.loadtxt(prefix+'.Xemit.'+suffix)
    yemit = np.loadtxt(prefix+'.Yemit.'+suffix)
    
    fig, ax = plt.subplots(**fig_kw)
    ax.plot(xemit[:,0], xemit[:,3], '-')
    ax.plot(yemit[:,0], yemit[:,3], '-')
    ax.grid()
    ax.set_xlabel(r'$z$ (m)')
    ax.set_ylabel(r'$\sigma_{x/y}$ (mm)')
    if extent is not None:
        xmin, xmax, ymin, ymax = extent
        ax.set_xlim(xmin, xmax)
        ax.set_ylim(ymin, ymax)
    ax.legend(['$x$', '$y$'])
    fig.savefig('rms_xy-z'+fig_ext)
    return

def plot_rms_xyz(prefix = 'ast', suffix = '001', fig_ext = '.eps', extent = None, **fig_kw):
    xemit = np.loadtxt(prefix+'.Xemit.'+suffix)
    yemit = np.loadtxt(prefix+'.Yemit.'+suffix)
    zemit = np.loadtxt(prefix+'.Zemit.'+suffix)
    
    fig, ax = plt.subplots(**fig_kw)
    ax.plot(xemit[:,0], xemit[:,3], '-')
    ax.plot(yemit[:,0], yemit[:,3], '-')
    ax1 = ax.twinx()
    ax1.plot(zemit[:,0], zemit[:,3], 'g-')
    ax1.set_ylim()
    ax1.set_ylabel(u_rms_z)
    ax.grid()
    ax.set_xlabel(r'$z$ (m)')
    ax.set_ylabel(r'$\sigma_{x,y}$ (mm)')
    if extent is not None:
        xmin, xmax, ymin, ymax = extent
        ax.set_xlim(xmin, xmax)
        ax.set_ylim(ymin, ymax)
    ax.legend(['$x$', '$y$', '$z$'])
    fig.savefig('rms_xyz-z'+fig_ext)
    return

def plot_emi_xy(prefix = 'ast', suffix = '001', fig_ext = '.eps', extent = None, **fig_kw):
    xemit = np.loadtxt(prefix+'.Xemit.'+suffix)
    yemit = np.loadtxt(prefix+'.Yemit.'+suffix)
    
    fig, ax = plt.subplots(**fig_kw)
    ax.plot(xemit[:,0], xemit[:,5], '-')
    ax.plot(yemit[:,0], yemit[:,5], '-')
    ax.grid()
    ax.set_xlabel(r'$z$ (m)')
    ax.set_ylabel(r'$\varepsilon_{n, x/y}$ (mm)')
    if extent is not None:
        xmin, xmax, ymin, ymax = extent
        ax.set_xlim(xmin, xmax)
        ax.set_ylim(ymin, ymax)
    ax.legend(['$x$', '$y$'], loc = 'lower right')
    fig.savefig('emi_xy-z'+fig_ext)
    return

def plot_kin(prefix = 'ast', suffix = '001', fig_ext = '.eps', extent = None, **fig_kw):
    '''
    Plot Ek and Delta_Ek 
    '''
    zemit = np.loadtxt(prefix+'.Zemit.'+suffix)
    
    fig, ax = plt.subplots(**fig_kw)
    ax.plot(zemit[:,0], zemit[:,2], 'r-', label = r'$E_k$')
    ax2 = ax.twinx()
    ax2.plot(zemit[:,0], zemit[:,4]/1e3/zemit[:,2]*100, 'b-', label = r'$\sigma_E/E$')
    ax.grid()
    ax.set_xlabel(r'$z$ (m)')
    ax.set_ylabel(u_kinetic)
    ax2.set_ylabel(u_kinetic_rel)
    if extent is not None:
        xmin, xmax, ymin, ymax = extent
        ax.set_xlim(xmin, xmax)
        ax.set_ylim(ymin, ymax)
    fig.savefig('kin-z'+fig_ext)
    return

def plot_kin2(prefix = 'ast', suffix = '001', fig_ext = '.eps', extent = None, **fig_kw):
    '''
    Plot Ek and Cor_zEk 
    '''
    zemit = np.loadtxt(prefix+'.Zemit.'+suffix)
    
    fig, ax = plt.subplots(**fig_kw)
    ax.plot(zemit[:,0], zemit[:,6], 'r-', label = r'$\langle z\cdot E_{\rm k}\rangle$')
    ax2 = ax.twinx()
    ax2.plot(zemit[:,0], zemit[:,4]/1e3/zemit[:,2]*100, 'b-', label = r'$\sigma_E/E$')
    ax.grid()
    ax.set_xlabel(r'$z$ (m)')
    ax.set_ylabel(r'$\langle z\cdot E_{\rm k}\rangle$')
    ax2.set_ylabel(u_kinetic_rel)
    if extent is not None:
        xmin, xmax, ymin, ymax = extent
        ax.set_xlim(xmin, xmax)
        ax.set_ylim(ymin, ymax)
    fig.savefig('kin-z'+fig_ext)
    return

def plot_zEk(prefix = 'ast', suffix = '001', fig_ext = '.eps', extent = None, **fig_kw):
    '''
    Plot only Cor_zEk
    '''
    fig, ax = plt.subplots(**fig_kw)
    
    zemit = np.loadtxt(prefix+'.Zemit.'+suffix)
    
    ax.plot(zemit[:,0], zemit[:,6], 'r-', label = r'$\langle z\cdot E_{\rm k}\rangle$')
    ax.grid()
    ax.set_xlabel(r'$z$ (m)')
    ax.set_ylabel(r'$\langle z\cdot E_{\rm k}\rangle$ (keV)', color = 'r')
    
    ax.tick_params(axis='y', colors='r', which = 'both')
    # ax.spines['left'].set_color('blue')
    
    ax2 = ax.twinx()
    ax2.plot(zemit[:,0], zemit[:,4]/1e3/zemit[:,2]*100, 'b-', label = r'$\sigma_E/E$')
    ax2.set_ylabel(u_kinetic_rel, color = 'b')
    
    ax2.tick_params(axis='y', colors='b', which = 'both')
    ax2.spines['left'].set_color('red')
    ax2.spines['right'].set_color('blue')
    
    fig.savefig('zEkin-z'+fig_ext)
    return

def plot_all_z(prefix = 'ast', suffix = '001', fig_ext = '.eps', extent = None, **fig_kw):

    fig, [ax1, ax2, ax3] = plt.subplots(nrows = 3, **fig_kw)
    
    xemit = np.loadtxt(prefix+'.Xemit.'+suffix)
    yemit = np.loadtxt(prefix+'.Yemit.'+suffix)
    zemit = np.loadtxt(prefix+'.Zemit.'+suffix)
    
    ax1.plot(xemit[:,0], xemit[:,3], '-')
    ax1.plot(yemit[:,0], yemit[:,3], '-')
    ax1.grid()
    ax1.set_xlabel(r'$z$ (m)')
    ax1.set_ylabel(r'$\sigma_{x,y}$ (mm)')
    ax1.legend(['$x$', '$y$'])
    ax1.set_ylim(0, 10)
    
    ax12 = ax1.twinx()
    ax12.plot(zemit[:,0], zemit[:,3], '-')
    ax12.set_ylabel(r'$\sigma_{z}$ (mm)')
    ax12.set_ylim(0, 10)
    
    ax2.plot(xemit[:,0], xemit[:,5], '-')
    ax2.plot(yemit[:,0], yemit[:,5], '-')
    ax2.grid()
    ax2.set_xlabel(r'$z$ (m)')
    ax2.set_ylabel(r'$\varepsilon_{n, x/y}$ (mm)')
    ax2.legend(['$x$', '$y$'])
    
    #ax3.plot(zemit[:,0], zemit[:,6], 'r-', label = r'$\langle z\cdot E_{\rm k}\rangle$')
    ax3.plot(zemit[:,0], zemit[:,2], 'r-', label = r'$E_{\rm k}$')
    ax32 = ax3.twinx()
    ax32.plot(zemit[:,0], zemit[:,4]/1e3/zemit[:,2]*100, 'b-', label = r'$\sigma_E/E$')
    ax3.grid()
    ax3.set_xlabel(r'$z$ (m)')
    #ax3.set_ylabel(r'$\langle z\cdot E_{\rm k}\rangle$ (keV)')
    ax3.set_ylabel(u_kinetic)
    ax32.set_ylabel(u_kinetic_rel)
    
    fig.savefig('all-along-z'+fig_ext)
    return

#workdir = r'C:\Users\lixiangk\Desktop\test'
#os.chdir(workdir)

#fig_ext = '@5m.png'

#plot_phase_space('ast.0500.001')


#plot_rms_xy('pithz100um')
#plot_emi_xy('pithz100um')
#plot_kin('pithz100um')

### Others
def read_sco(z0 = 5.277, fbeamline = None, foptimized = None):
    '''
    Read the quadrupole positions and gradients from a SCO output
    Parameters
      z0: the starting point of the quadrupole used in Astra
      foptimized: the name of the file that stores the optimized gradients
      fbeamline: the name of the file that stores the input beamline for SCO
    Returns
      [Q_pos, Q_grad]: positions and gradients of the quadrupoles
    '''
    
    astra_to_sco = 1.590444459638447
    
    Lret = False
    if foptimized is not None and isinstance(foptimized, str) and os.path.exists(foptimized):
        Lopt = True
        tmp = np.loadtxt(foptimized)
        grad_opt = tmp[-1,:-1] # read the optimized gradients into grads
    elif isinstance(foptimized, (list, np.ndarray)):
        grad_opt = foptimized;
        Lopt = True;
    else:
        Lopt = False
    
    # read the beamline into info, which has three columns for length, strength and
    # a boolean indicating if the element is used for optimization
    if fbeamline is not None and os.path.exists(fbeamline):
        info = np.loadtxt(fbeamline, usecols = (1, 2, 5), skiprows = 0)
        # read the names of the elements, 'Q' for quadrupole and 'O' for drift
        isQuad = np.loadtxt(fbeamline, usecols = 0, skiprows = 0, dtype=np.object)
    else:
        Lret = True
    
    if Lret:
        Q_pos = []
        Q_grad = []
        return
    
    Q_pos = []
    Q_grad = []
    
    j = 0
    for i, l in enumerate(isQuad):
        if l == 'Q':
            z0 += info[i, 0]/2.
            if info[i, 2] == 1 and Lopt:
                # use the optimized gradient
                Q_grad.append(grad_opt[j]*astra_to_sco) 
                j += 1
            else: 
                # use the initial gradient for thosed not optimized
                Q_grad.append(info[i, 1]*astra_to_sco)
            Q_pos.append(z0) # save the center position of the quadrupole
            z0 += info[i, 0]/2.
        else:
            z0 += info[i, 0]

    return [Q_pos, Q_grad]
def gen_astra(gen_name = None, ast_name = None, **kwargs):
    '''
    Parameters
      gen_name: filename of Generator input
      ast_name: filename of Astra input
      kwargs: key-value pairs to add more parameters or to update the values of existing parameters
    Returns
      write Generator and Astra input files
    '''
    # define the parameter lists for each Astra module
    newrun_list = {'Head', 'RUN', 'LOOP', 'NLoop', 'Distribution', 'ion_mass', 'N_red', 'Xoff', 'Yoff',\
                   'xp', 'yp', 'Zoff', 'Toff', 'Xrms', 'Yrms', 'Zrms', 'Trms', 'Tau', 'cor_py', 'cor_py',\
                   'Qbunch', 'SRT_Q_Schottky', 'Q_Schottky', 'debunch', 'Track_All', 'Track_On_Axis',\
                   'Auto_Phase', 'Phase_Scan', 'check_ref_part', 'L_rm_back', 'Z_min', 'Z_Cathode',\
                   'H_max', 'H_min', 'Max_step', 'Lmonitor', 'Lprompt'}
    output_list = {'ZSTART', 'ZSTOP', 'Zemit', 'Zphase', 'Screen', 'Scr_xrot', 'Scr_yrot', 'Step_width',\
                   'Step_max', 'Lproject_emit', 'Local_emit', 'Lmagnetized', 'Lsub_rot', 'Lsub_Larmor',\
                   'Rot_ang', 'Lsub_cor', 'RefS', 'EmitS', 'C_EmitS', 'C99_EmitS', 'Tr_EmitS', 'Sub_EmitS',\
                   'Cross_start', 'Cross_end', 'PhaseS', 'T_PhaseS', 'High_res', 'Binary', 'TrackS',\
                   'TcheckS', 'SigmaS', 'CathodeS', 'LandFS', 'LarmorS'}
    charge_list = {'LOOP', 'LSPCH', 'LSPCH3D', 'L2D_3D', 'Lmirror', 'L_Curved_Cathode', 'Cathode_Contour',\
                   'R_zero', 'Nrad', 'Cell_var', 'Nlong_in', 'N_min', 'min_grid', 'Merge_1', 'Merge_2',\
                   'Merge_3', 'Merge_4', 'Merge_5', 'Merge_6', 'Merge_7', 'Merge_8', 'Merge_9', 'Merge_10',\
                   'z_trans', 'min_grid_trans', 'Nxf', 'Nx0', 'Nyf', 'Ny0', 'Nzf', 'Nz0', 'Smooth_x',\
                   'Smooth_y', 'Smooth_z', 'Max_scale', 'Max_count', 'Exp_control'}
    aperture_list = {'LOOP', 'LApert', 'File_Aperture', 'Ap_Z1', 'Ap_Z2', 'Ap_R', 'Ap_GR', 'A_pos',\
                     'A_xoff', 'A_yoff', 'A_xrot', 'A_yrot', 'A_zrot', 'SE_d0', 'SE_Epm', 'SE_fs', \
                     'SE_Tau', 'SE_Esc', 'SE_ff1', 'SE_ff2', 'Max_Secondary', 'LClean_Stack'}
    cavity_list = {'LOOP', 'LEfield', 'File_Efield', 'C_noscale', 'C_smooth', 'Com_grid', 'C_higher_order',\
                   'Nue', 'K_wave', 'MaxE', 'Ex_stat', 'Ey_stat', 'Bx_stat', 'By_stat', 'Bz_stat',\
                   'Flatness', 'Phi', 'C_pos', 'C_numb', 'T_dependence', 'T_null', 'C_Tau', 'E_stored',\
                   'C_xoff', 'C_yoff', 'C_xrot', 'C_yrot', 'C_zrot', 'C_zkickmin', 'C_zkickmax', 'C_Xkick',\
                   'C_Ykick', 'File_A0', 'P_Z1', 'P_R1', 'P_Z2', 'P_R2', 'P_n', 'E_a0', 'E_Z0', 'E_sig',\
                   'E_sigz', 'E_Zr', 'E_Eps', 'E_lam', 'zeta'}
    solenoid_list = {'LOOP', 'LBfield', 'File_Bfield', 'S_noscale', 'S_smooth', 'S_higher_order', 'MaxB',\
                     'S_pos', 'S_xoff', 'S_yoff', 'S_xrot', 'S_yrot'}
    quadrupole_list = {'LOOP', 'Lquad', 'Q_type', 'Q_grad', 'Q_K', 'Q_noscale', 'Q_length', 'Q_smooth',\
                       'Q_bore', 'Q_dist', 'Q_mult_a', 'Q_mult_b', 'Q_pos', 'Q_xoff', 'Q_yoff', 'Q_zoff',\
                       'Q_xrot', 'Q_yrot', 'Q_zrot'}
    input_list = {'Fname', 'Add', 'N_add', 'Ipart', 'Species', 'ion_mass', 'Probe', 'Passive',\
                  'Noise_reduc', 'Cathode', 'R_Cathode', 'High_res', 'Binary', 'Q_total', 'Type', 'Rad',\
                  'Tau', 'Ref_zpos', 'Ref_clock', 'Ref_Ekin', 'Dist_z', 'sig_z', 'C_sig_z', 'Lz', 'rz',\
                  'sig_clock', 'C_sig_clcok', 'Lt', 'rt', 'Dist_pz', 'sig_Ekin', 'C_sig_Ekin', 'LE', 'rE',\
                  'emit_z', 'cor_Ekin', 'E_photon', 'phi_eff', 'Dist_x', 'sig_x', 'C_sig_x', 'Lx', 'rx',\
                  'x_off', 'Disp_x', 'Dist_px', 'Nemit_x', 'sig_px', 'C_sig_px', 'Lpx', 'rpx', 'cor_px',\
                  'Dist_y', 'sig_y', 'C_sig_y', 'Ly', 'ry', 'y_off', 'Disp_y', 'Dist_py', 'Nemit_y',\
                  'sig_py', 'C_sig_py', 'Lpy', 'rpy', 'cor_py'}
    # change all the variables into upper case
    newrun_list = [x.upper() for x in newrun_list]
    output_list = [x.upper() for x in output_list]
    charge_list = [x.upper() for x in charge_list]
    cavity_list = [x.upper() for x in cavity_list]
    solenoid_list = [x.upper() for x in solenoid_list]
    quadrupole_list = [x.upper() for x in quadrupole_list]
    aperture_list = [x.upper() for x in aperture_list]
    input_list = [x.upper() for x in input_list]
    
    if gen_name == None:
        gen_name = './gen.in'
    if ast_name == None:
        ast_name = './ast.in'
    
    kv = {} # change the keys into upper case
    for key, value in kwargs.items():
        kv.update({key.upper():value})
    
    # Get the path to field maps and PITZ beamlines
    if 'FIELD_MAPS' in kv.keys():
        field_maps = kv.pop('FIELD_MAPS')
    else:
        field_maps = '.'
    
    QuadPos, ScrPos = [], []
    # if a PITZ beamline Excel is given then use it as input for positions of quads and screens
    # if 'PITZ_BEAMLINE' in kv.keys():
    #     filename = kv.pop('PITZ_BEAMLINE')
    # else:
    #     filename = 'PITZbeamlines.xlsx'
    # [QuadPos, ScrPos] = read_pitz(field_maps+os.sep+filename)
    
    QuadGrad = np.zeros(len(QuadPos))
    
    # If a SCO optimized result is given, then use it for updating the
    # quadrupoles' position and gradients
    if 'SCO_Z0' in kv.keys():
        z0 = kv.pop('SCO_Z0')
        if 'SCO_BEAMLINE' in kv.keys():
            fbeamline = kv.pop('SCO_BEAMLINE')
            if 'SCO_OPTIMIZED' in kv.keys():
                foptimized = kv.pop('SCO_OPTIMIZED')
                [QuadPos, QuadGrad] = read_sco(z0, fbeamline, foptimized)
            else:
                [QuadPos, QuadGrad] = read_sco(z0, fbeamline)
        else:
            [QuadPos, QuadGrad] = read_sco(z0)
    
    QuadType = [field_maps+os.sep+'Q3.data' for i in QuadPos]
    input0 = Module('Input', FNAME = 'beam.ini', Lprompt = False, Species = 'electrons',\
                    IPart = 50000, Q_total = -4.,\
                    Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',\
                    Dist_z = 'p', Lt = 21.5e-3, rt = 2e-3, Cathode = True,\
                    Dist_x = 'r', sig_x = 1.0, Dist_px = 'g', Nemit_x = 0,\
                    Dist_y = 'r', sig_y = 1.0, Dist_py = 'g', Nemit_y = 0)

    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation',\
                    Distribution = 'beam.ini', Auto_Phase = True, Track_All = True,\
                    check_ref_part = False, Lprompt = False, Max_step=200000)
    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 40, Nlong_in = 50,\
                    N_min = 10, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True,\
                    File_Efield = [field_maps+os.sep+'gun42cavity.txt',field_maps+os.sep+ 'CDS14_15mm.txt'],\
                    MaxE = [60, 12], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [0, 0])   
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'],\
                    MaxB = [0.2], S_pos = [0.])
    quadru = Module('Quadrupole', Lquad = True, Q_grad = QuadGrad, Q_pos = QuadPos, Q_type = QuadType)
    output = Module('Output', Zstart = 0, Zstop = 18.0, Zemit = 100, Zphase = 1, RefS = True,\
                    EmitS = True, PhaseS = True, TrackS = False, LandFS = True, Screen = ScrPos)
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app1.txt'])
    
    for key, value in kv.items():
        if key in input_list:
            input0.reset(key, value)
        elif key in newrun_list:
            newrun.reset(key, value)
        elif key in charge_list:
            charge.reset(key, value)
        elif key in cavity_list:
            cavity.reset(key, value)
        elif key in solenoid_list:
            soleno.reset(key, value)
        elif key in quadrupole_list:
            quadru.reset(key, value)
        elif key in output_list:
            output.reset(key, value)
        elif key in aperture_list:
            apertu.reset(key, value)
        else:
            print('Note: key '+key+' is not in the lists!')
    
    generator = Astra()
    generator.add_module(input0)
    
    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, quadru, apertu, output])
    
    generator.write(gen_name)
    astra.write(ast_name)
    # astra.qsub()
    
    return [generator, astra]

# read_pitz()
# read_sco(5.277, 'beamline.txt', 'Otvet.dat')

# gen_astra('gen.in', 'ast.in', PITZ_BEAMLINE = 'PITZbeamlines.xlsx',\
#           SCO_beamline = 'beamline.txt', SCO_z0 = 5.277, SCO_optimized = 'Otvet.dat',\
#           IPART = 5000, ZSTOP = 5.3, Zemit = 100, LSPCH = False, LSPCH3D = False)



# SpaceChargeOptimizer.exe
def readfromsco(z0 = 5.277, otvet_file = 'Otvet.dat', beamline_file = 'beamlineX.txt'):
    '''
    Read the quadrupole positions and gradients from a SCO output
    Parameters
      z0: the starting point of the quadrupole used in Astra
      otvet_file: the name of the file that stores the optimized gradients
      beamline_file: the name of the file that stores the input beamline for SCO
    Returns
      [Q_pos, Q_grad]: positions and gradients of the quadrupoles
    '''
    tmp = np.loadtxt(otvet_file)
    grads = tmp[-1,:-1] # read the optimized gradients into grads

    # read the beamline into info, which has three columns for length, strength and
    # a boolean indicating if the element is used for optimization
    info = np.loadtxt(beamline_file, usecols = (1, 2, 5), skiprows = 0)
    # read the names of the elements, 'Q' for quadrupole and 'D' for drift
    isQuad = np.loadtxt(beamline_file, usecols = 0, skiprows = 0, dtype=np.object)

    Q_pos = []
    Q_grad = []
    
    j, pos = 0, z0

    for i, l in enumerate(isQuad):   
        grad0 = info[i, 1] # starting points for the quads
        if l == 'Q':
            pos += info[i, 0]/2.
            Q_pos.append(pos) # save the center position of the quadrupole
            pos += info[i, 0]/2.
            if info[i, 2] == 1:
                Q_grad.append(grads[j]*1.590444459638447) # save the optimized gradient
                j += 1
            else: 
                # save the initial gradient for thosed not optimized
                Q_grad.append(grad0*1.590444459638447) 
        else:
            pos += info[i, 0]

    return [Q_pos, Q_grad]

def sco_update_grads(foptimized = 'Otvet.dat', fbeamline = 'beamlineX.txt', new_beamline = None, disabled = False, index = -1):
    '''
    Update the SCO beamline from a previous optimization result.
    Parameters
      foptimized: the name of the file that stores the optimized gradients
      fbeamline: the name of the file that stores the input beamline for SCO
      new_beamline: the name of the updated file that stores the input beamline for SCO
    Returns
      Write the beamline into a new file
    '''
    
    if isinstance(foptimized, str) and os.path.exists(foptimized):
        tmp = np.loadtxt(foptimized)
        Q_grads = tmp[-1,:-1] # read the optimized gradients into grads
    elif isinstance(foptimized, (list, np.ndarray)):
        Q_grads = foptimized
    
    #tmp = np.loadtxt(foptimized)
    #Q_grads = tmp[index,:-1] # read the optimized gradients into grads
    
    # read the beamline into info, which has three columns: 1->length, 2->strength and
    # 3->lower bound, 4->upper boudn and 5->boolean indicating if the element is used for optimization
    info = np.loadtxt(fbeamline, usecols = (1, 2, 3, 4, 5), skiprows = 0)
    # read the names of the elements, 'Q' for quadrupole and 'D' for drift
    flag = np.loadtxt(fbeamline, usecols = 0, skiprows = 0, dtype=np.object)

    j = 0
    output = ''
    for i, l in enumerate(flag):   
        if l == 'Q' and info[i,4] == 1:
            info[i,1] = Q_grads[j]
            j += 1
        if disabled:
            info[i,4] = 0
        tmp = tuple(l)+tuple(info[i])
        output += str.format('%-s%12.6f%12.6f%12.6f%12.6f%5.0f\n' % tmp)
    
    if new_beamline == None:
        new_beamline = fbeamline
    f = open(new_beamline, 'w')
    f.write(output)
    f.close()
    return

def sco_update_bounds(upper, lower = None, old_beamline_file = 'beamlineX.txt', new_beamline_file = None):
    '''
    Update the SCO beamline from a previous optimization result.
    Parameters
      otvet_file: the name of the file that stores the optimized gradients
      old_beamline_file: the name of the file that stores the input beamline for SCO
      new_beamline_file: the name of the updated file that stores the input beamline for SCO
    Returns
      Write the beamline into a new file
    '''
    
    # read the beamline into info, which has three columns: 1->length, 2->strength and
    # 3->lower bound, 4->upper boudn and 5->boolean indicating if the element is used for optimization
    info = np.loadtxt(old_beamline_file, usecols = (1, 2, 3, 4, 5), skiprows = 0)
    # read the names of the elements, 'Q' for quadrupole and 'D' for drift
    flag = np.loadtxt(old_beamline_file, usecols = 0, skiprows = 0, dtype=np.object)

    if lower == None:
        lower = -upper
    j = 0
    output = ''
    for i, l in enumerate(flag):   
        if l == 'Q': # and info[i,4] == 1:
            info[i,2] = lower
            info[i,3] = upper
            j += 1
        tmp = tuple(l)+tuple(info[i])
        output += str.format('%-s%12.6f%12.6f%12.6f%12.6f%5.0f\n' % tmp)
    
    if new_beamline_file == None:
        new_beamline_file = old_beamline_file
    f = open(new_beamline_file, 'w')
    f.write(output)
    f.close()
    return

def plot_sco(fname = 'BeamDynamics.dat', x = 'avg_z', y = ['std_x', 'std_y'], xlabel = u_z, ylabel = u_rms_xy,\
             legends = ['x', 'y'], figname = 'sco-rms_xy-z', z0 = 0, fig_ext = '.eps', extent = None, **kwargs):
    '''
    Parameters
      fname: the file name that stores the beam parameters
      x,y: specify which columns are to be plotted, could be string or list of strings:
           kv={'avg_z':0, 'nemit_x':1, 'nemit_y':2, 'std_x':3, 'std_y':4, 'std_z':5,\
               'beta_x':6, 'beta_y':7, 'alpha_x':8, 'alpha_y':9, 'r56':10, 'avg_p': 13}
      xlabel, ylabel: specify the labels of the plot
      legends: a list of strings, used only when y is also a list
      figname: specify the figure name to be saved
      fig_ext: specify the extension of the figure name
      **kwargs: to be passed to plt.subplots()
    '''
    kv={'avg_z':0, 'nemit_x':1, 'nemit_y':2, 'std_x':3, 'std_y':4, 'std_z':5, 'beta_x':6, 'beta_y':7,\
    'alpha_x':8, 'alpha_y':9, 'r56':10, 'avg_p': 13}

    data = np.loadtxt(fname)
    
    xd = data[:,kv[x]]
    fig, ax = plt.subplots(**kwargs)
    if isinstance(y, list):
        for i, yi in enumerate(y):
            yd = data[:,kv[yi]]
            ax.plot(xd+z0, yd, '-', label = legends[i])
    else:
        yd = data[:,kv[y]]
        ax.plot(xd+z0, yd, '-')
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    if extent is not None:
        xmin, xmax, ymin, ymax = extent
        ax.set_xlim(xmin, xmax)
        ax.set_ylim(ymin, ymax)
    ax.grid()
    ax.legend()
    
    fig.savefig(figname+fig_ext)
    plt.close()
    return

def plot_sco_xyz(fname = 'BeamDynamics.dat', x = 'avg_z', y = ['std_x', 'std_y', 'std_z'], xlabel = u_z, ylabel = u_rms_xy,\
             legends = ['x', 'y', 'z'], figname = 'sco-rms_xyz-z', z0 = 0, fig_ext = '.eps', extent = None, **kwargs):
    '''
    Parameters
      fname: the file name that stores the beam parameters
      x,y: specify which columns are to be plotted, could be string or list of strings:
           kv={'avg_z':0, 'nemit_x':1, 'nemit_y':2, 'std_x':3, 'std_y':4, 'std_z':5,\
               'beta_x':6, 'beta_y':7, 'alpha_x':8, 'alpha_y':9, 'r56':10, 'avg_p': 13}
      xlabel, ylabel: specify the labels of the plot
      legends: a list of strings, used only when y is also a list
      figname: specify the figure name to be saved
      fig_ext: specify the extension of the figure name
      **kwargs: to be passed to plt.subplots()
    '''
    kv={'avg_z':0, 'nemit_x':1, 'nemit_y':2, 'std_x':3, 'std_y':4, 'std_z':5, 'beta_x':6, 'beta_y':7,\
    'alpha_x':8, 'alpha_y':9, 'r56':10, 'avg_p': 13}

    data = np.loadtxt(fname)
    
    xd = data[:,kv[x]]
    fig, ax = plt.subplots(**kwargs)
    if isinstance(y, list):
        for i, yi in enumerate(y):
            yd = data[:,kv[yi]]
            ax.plot(xd+z0, yd, '-', label = legends[i])
    else:
        yd = data[:,kv[y]]
        ax.plot(xd+z0, yd, '-')
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    if extent is not None:
        xmin, xmax, ymin, ymax = extent
        ax.set_xlim(xmin, xmax)
        ax.set_ylim(ymin, ymax)
    ax.grid()
    ax.legend()
    
    fig.savefig(figname+fig_ext)
    plt.close()
    return


# coding: utf-8

# In[2]:

# %matplotlib inline
import numpy as np
import scipy as sp
import matplotlib as mpl
import matplotlib.pyplot as plt

from scipy import optimize
from scipy.integrate import ode
from scipy.interpolate import interp1d
from scipy.optimize import curve_fit
import os
import re

import sys 
sys.path.insert(0,'E:\\Lixiangkun\\Sources\\python')

from consts import * 
from transform2D import * 


# ## GPT modules

# In[3]:

class ECS: 
    def __init__(self,name='WCS',transform='',position=0,matrix=[]): 
        self.name=name
        self.transform=transform
        self.position=position
        self.matrix=matrix
    def __str__(self,matrix=[]):
        if self.transform=='I':
            return '\"{self.name}\",\"{self.transform}\"'.format(self=self)
        elif self.transform=='z':
            return '\"{self.name}\",\"{self.transform}\",{self.position}'.format(self=self) 
        elif self.transform=='':
            return '\"{0}\",{1[0]},{1[1]},{1[2]},{1[3]},{1[4]},{1[5]},{1[6]},{1[7]},{1[8]}'.format(self.name,self.matrix) 
        else:
            return 'Error!'
#print str(ECS('WCS','z',0))
#print str(ECS('WCS',matrix=[0,0,0,1,0,0,0,1,0]))


# In[6]:

def astra2gpt(infile,outfile):
    '''infile:x y z Px Py Pz
    outfile:x y z bx by bz G'''
    data=np.loadtxt(infile)
    data[1:,2]=data[1:,2]+data[0,2]
    data[1:,5]=data[1:,5]+data[0,5]
    xx=data[:,0]
    yy=data[:,1]
    zz=data[:,2]
    gg=np.sqrt(1+(data[:,3]**2+data[:,4]**2+data[:,5]**2)/g_mec2**2/1e12)
    bx=data[:,3]/g_mec2/1e6
    by=data[:,4]/g_mec2/1e6
    bz=data[:,5]/g_mec2/1e6
    bxc=np.mean(bx)
    bx=bx-bxc
    
    m=np.ones(len(xx))*g_me
    q=np.ones(len(xx))*g_qe
    nmacro=np.ones(len(xx))*data[0,7]/g_qe
    
    data1=np.array([xx,yy,zz,bx,by,bz,m,q,nmacro]).T
    np.savetxt(outfile,data1,fmt='%11.6e',               header='{:>11}{:>12}{:>12}{:>12}{:>12}{:>12}{:>12}{:>12}{:>12}'.               format('x','y','z','GBx','GBy','GBz','m','q','nmacro'),comments='')
def gpt2undulator(infile,outifle,B0=1.0,lam_u=38e-3):
    data=np.loadtxt(infile)
    xc,yc,zc=np.mean(data[:,0]),np.mean(data[:,1]),np.mean(data[:,2])
    gamma=np.mean(data[:,6])
    beta=np.sqrt(1-1./gamma**2)
    xx=data[:,0]-xc
    yy=data[:,1]-yc
    zz=data[:,2]-zc
    Bx=data[:,3]
    By=data[:,4]
    Bz=data[:,5]
    GG=data[:,6]
    ku=2*np.pi/lam_u
    beta_x=g_qe*B0/gamma/g_me/ku
    theta_x=-np.arctan(beta_x/beta)
    nn=len(xx)
    for i in np.arange(nn):
        zz1,xx1=transform2D(zz[i],xx[i],theta_x)
        zz[i],xx[i]=zz1,xx1
        Bz1,Bx1=transform2D(Bz[i],Bx[i],theta_x)
        Bz[i],Bx[i]=Bz1,Bx1
    data1=np.array([xx,yy,zz,bx,by,bz]).T
    np.savetxt(outfile,data1,fmt='%11.6e',               header='{:>11}{:>12}{:>12}{:>12}{:>12}{:>12}'.               format('x','y','z','Bx','By','Bz'),comments='')
    
def gpt_get(infile,col):
    data=np.loadtxt(infile,skiprows=1)
    return data[-1,col]

class GPT:
    def __init__(self):
        self.Ek=0.03 # eV
        self.output=''
        self.beam='beam'
    def set_m(self,m=g_me):
        self.output=self.output+        str.format('m=%e;\n' % m)
    def set_q(self,q=g_qe):
        self.output=self.output+        str.format('q=%e;\n' % q)
    def set_time(self,time):
        self.output=self.output+        str.format('time=%e;\n' % time)
    def setparticles(self,beam,N,m,q,Qtot):
        self.output=self.output+        str.format('setparticles(\"%s\",%d,%e,%e,%e);\n' % (beam,N,m,q,Qtot))
    def spacecharge3D(self):
        self.output=self.output+        str.format('spacecharge3D();\n')
    def spacecharge3Dmesh(self):
        self.output=self.output+        str.format('spacecharge3Dmesh();\n')
    def setfile(self,beam,filename):
        self.output=self.output+        str.format('setfile(\"%s\",\"%s\");\n' % (beam,filename))
    def sectormagnet(self,fromCCS,toCCS,R,Bfield):
        self.output=self.output+        str.format('sectormagnet(\"%s\",\"%s\",%e,%e);\n' % (fromCCS,toCCS,R,Bfield))
    def solenoid(self,ECS,R,I):
        self.output=self.output+        str.format('solenoid(%s,%e,%e);\n' % (ECS,R,I))    
    def quadrupole(self,ECS,L,G):
        self.output=self.output+        str.format('quadrupole(%s,%e,%e);\n' % (ECS,L,G))
    def unduplan(self,ECS,Nu,lamu,Bu,trim1=0.25,trim2=0.43):
        self.output=self.output+        str.format('unduplan(%s,%d,%e,%e,%e,%e);\n' % (ECS,Nu,lamu,Bu,trim1,trim2))
    def map3D_B(self,ECS,mapfile,Bfac):
        self.output=self.output+        str.format('map3D_B(%s,\"%s\",\"x\",\"y\",\"z\",\"Bx\",\"By\",\"Bz\",%e);\n' % (ECS,mapfile,Bfac))
    
    def set_dtmin(self,dtmin):
        self.output=self.output+        str.format('dtmin=%e;\n' % dtmin)
    def screen(self,ECS,at):
        self.output=self.output+        str.format('screen(%s,%e);\n' % (ECS,at))
    def tout(self,fr,to=0,step=0):
        if to==0:
            self.output=self.output+str.format('tout(%e);\n' % fr)
            return
        self.output=self.output+        str.format('tout(%e,%e,%e);\n' % (fr,to,step))
    
    def ccs(self,ECS,CCSname):
        self.output=self.output+        str.format('ccs(%s,\"%s\");\n' % (ECS,CCSname))
    def write(self,fname='gpt.in'):
        ff=open(fname,'w')
        ff.write(self.output)
        ff.close()
gauss_fit=lambda x,a,b,c,d:a*np.exp(-(x-c)**2/2.0/b**2)+d

def nemixrms(x,xp):
    '''input: x-xp phase space
       output: emittance'''
    return np.sqrt(np.var(x)*np.var(xp)-np.mean((x-np.mean(x))*(xp-np.mean(xp)))**2)
def gpt_post(data,tail=1,head=0,by=2):
    '''input data: x y z Bx By Bz G
       output: avgz nemit_x nemit_y std_x std_y std_z Ekin std_Ekin nemit_z'''
    if tail-head<1:
        data=data[data[:,by].argsort()]
        n=len(data)
        n1,n2=(int)(n*(1-tail)),(int)(n*(1-head))
        data=data[n1:n2,:]
    #plt.plot(data[:,2],data[:,6]*0.511-0.511,'.')
    #plt.show()
    x,y,z=data[:,0],data[:,1],data[:,2]
    bgx,bgy,Ek=data[:,3]*data[:,6],data[:,4]*data[:,6],data[:,6]*0.511-0.511
    
    return np.array([np.mean(z),nemixrms(x,bgx)*1e6,nemixrms(y,bgy)*1e6,np.std(x)*1e3,                     np.std(y)*1e3,np.std(z)*1e3,np.mean(Ek),np.std(Ek)*1e3,nemixrms(z,Ek)*1e6])
def gpt_split(datafile,prefix=""):
    '''input: file to be splitted, each subfilename begins with a prefix
       output: no
    '''
    data=open(datafile, 'r')
    while True:
        while True:
            line=data.readline()
            if not line:
                break
            match=re.search(r'^time',line)
            if match:
                time=re.search(r'[\d.+-]+e[\d.+-]+',line)
                if time:
                    t=(float)(time.group())*1e9
                    fname=str.format('%stime_%.2lfns.txt' % (prefix,t))
                    break
            #else:
            #    continue
            match=re.search(r'^position',line)
            if match:
                pos=re.search(r'[\d.+-]+e[\d.+-]+',line)
                if pos:
                    t=(float)(pos.group())*1e3
                    fname=str.format('%sposition_%.2lfmm.txt' % (prefix,t))
                    break
        if not line:
            break
        #if t==17:
        subdata=open(fname,'w')
        #else:
        #    break
        while True:
            line=data.readline()
            blank=re.search(r'                                ',line)
            if blank:
                break
            else:
                subdata.write(line)
        subdata.close()
        #break
    data.close()
def gpt_split_position(datafile,prefix=""):
    '''input: file to be splitted, each subfilename begins with a prefix
       output: no
    '''
    data=open(datafile, 'r')
    while True:
        while True:
            line=data.readline()
            if not line:
                break
            match=re.search(r'^position',line)
            if match:
                pos=re.search(r'[\d.+-]+e[\d.+-]+',line)
                if pos:
                    t=(float)(pos.group())*1e3
                    fname=str.format('%sposition_%.2lfmm.txt' % (prefix,t))
                    break
        if not line:
            break
        subdata=open(fname,'w')
        while True:
            line=data.readline()
            blank=re.search(r'                                ',line)
            if blank:
                break
            else:
                subdata.write(line)
        subdata.close()
        #break
    data.close()


# In[5]:

def gpt_batch(infile='gpt',outfile='test'):
    '''infile: gpt input filename, without suffix .in
    outfile: gpt output filename, also without suffix .gdf'''
    output=''
    output=output+     str.format('gpt -o %s.gdf %s.in\n' % (outfile,infile))+     str.format('gdfa -o time_%s.gdf %s.gdf time avgz avgx avgy nemixrms nemiyrms stdx stdy stdza avgG\n'               % (outfile,outfile))+     str.format('gdf2a -w 12 -o time_%s.txt time_%s.gdf\n' % (outfile,outfile))+     str.format('rem gdf2a -w 12 -o %s.txt %s.gdf x y z Bx By Bz G t\n\n' % (outfile,outfile))

    f = file('test.bat', 'w') # open for 'w'riting
    f.write(output) # write text to file
    f.close() # close the file
#gpt_batch()


# In[272]:

#workdir='E:\\Lixiangkun\\Work\\ERL-THz\\Chicane'
def traj_fitting(filename,col1=0,col2=1,row1=0,row2=-1,skiprows=1):
    data=np.loadtxt(filename,skiprows=skiprows)
    z=data[:,col1]
    x=data[:,col2]
    n1=row1
    n2=row2

    f1 = lambda x,a,b:a+b*x
    popt,pcov=curve_fit(f1,z[n1:n2],x[n1:n2])
    a,b=popt
    alpha=np.arctan(b)*180./np.pi

    fig,axes=plt.subplots(figsize=(10,4))
    axes.plot(z[::2],x[::2],'r-')
    axes.plot(z[n1:n2],f1(z[n1:n2],a,b),'b')
    #axes.axis([3.5,7.5,-3.8,0.2])
    axes.axis([1.5,4.5,-0.6,0.4])
    #axes.plot(z,f1(z,a,b),'b')
    axes.grid()
    axes.set_xlabel('z (m)')
    axes.set_ylabel('x (m)')
    fig.savefig('traj_fitting.eps')
    
    return alpha
#traj_fitting('time_test.txt',col1=1,col2=2,row1=65,row2=80)


# -*- coding: utf-8 -*-
"""
Created on Wed Mar 24 21:27:31 2021

@author: lixiangk
"""

from universal import *

workdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim\2020\Shift\20210303A'
os.chdir(workdir)

data = np.loadtxt('1.22A.txt')

fig, ax = plt.subplots(figsize = (6, 3.6))

ax.plot(data[:,0], data[:,1], '-<')
ax.plot(data[:,0], data[:,2], '-D')

ax.grid()

ax.set_xlabel(r'$z$ (m)')
ax.set_ylabel(r'RMS size (mm)')

ax.legend(['$x$', '$y$'])

fig.savefig('1.22A.png')
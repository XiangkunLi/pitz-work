# -*- coding: utf-8 -*-
"""
Created on Tue Nov 17 11:19:38 2020

@author: lixiangk
"""

from universal import *
from astra_modules import *
import matplotlib.tri as tri

workdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim\2020\LCLS-I-matching-100um'
#workdir = r'D:\2018\LCLS-I-matching'
os.chdir(workdir)

def cov2alpha(cov, nemit = 4e-6, P0 = 17.05):
    return -cov/nemit*gamma2bg(M2G(P0))
def alpha2cov(alpha, nemit = 4e-6, P0 = 17.05):
    return -alpha*nemit/gamma2bg(M2G(P0))

#%%
data = np.loadtxt('seeds60um_x-y-emit@4nC.dat')

res = []
for i, v in enumerate(data[:]):
    Ipart = 250000
    
    xrms, yrms, alphax, alphay, emitx, emity = v[0:6]
    v1 = v.copy()
    v1[2:4] = cov2alpha(-v1[2:4]*1e-6, v[4:6]*1e-6, P0 = 22)
    direc = str.format('./n%.0fk-sig_x-%.2fmm-sig_y-%.2fmm-alp_x-%.2f-alp_y-%.2f-nemit_x-%.2fum_nemit_y-%.2fum' %
                       (Ipart/1000., *v1))
    
    #os.rmdir(direc); continue
    #try:
    #    os.chdir(direc); #print(direc)
    #except Exception as err:
    #    continue
    # os.chdir(direc)
    # for fname in ['beam_und.ini']: #['ast.0360.001', 'ast.ref.001']:
    #     try:
    #         os.remove(fname)
    #     except Exception as err:
    #         pass 
    # os.chdir('..'); continue
    
    # try:
    #     diag = BeamDiagnostics(fname = direc+os.sep+'ast.0360.001')
    
    #     xrms1, yrms1, alphax1, alphay1 = diag.std_x, diag.std_y, diag.alpha_x, diag.alpha_y
    #     nemit_x1, nemit_y1 = diag.nemit_x, diag.nemit_y
    # except Exception as err:
    if 1:
        #print(direc)
        data = np.loadtxt(direc+os.sep+'ast.Xemit.001')
        gb = np.sqrt(1+M2G(17.05)**2)
        nemit_x1 = data[-1,5]
        xrms1 = data[-1,3]
        alphax1 = -data[-1,6]/(nemit_x1/gb)
        xrms1_rms = np.mean(data[:,3])
        
        data = np.loadtxt(direc+os.sep+'ast.Yemit.001')
        nemit_y1 = data[-1,5]
        yrms1 = data[-1,3]
        alphay1 = -data[-1,6]/(nemit_x1/gb)
        yrms1_rms = np.mean(data[:,3]) #
        
    res.append(list(v)+[xrms1, yrms1, alphax1, alphay1, nemit_x1, nemit_y1]+
               [xrms1_rms, yrms1_rms])

#%%
res1 = np.copy(np.array(res))
with open('results_60um_x-y-emit@4nC.dat', 'w') as f_handle:
    np.savetxt(f_handle, res1, fmt = '%14.6E')
#%%
res1 = np.loadtxt('results_60um_x-y-emit@4nC.dat')
#res1 = np.loadtxt('results_60um_ynew.dat')
res1 = res1[:]

goal = []
for i, v in enumerate(res1):
    xrms, yrms, alphax, alphay, _, _, xrms1, yrms1, alphax1, alphay1 = v[:10]
    tempx = np.abs(xrms-xrms1)
    tempy = np.abs(yrms-yrms1)
    #goal.append([tempx, tempy, tempx+tempy])
    goal.append([tempx, v[-1], v[-2]/11+v[-1]/5])
    
goal = np.array(goal)

#%% x and xp scan
axis = 'y'
if axis is 'x':
    ix, iy, ig = 0, 2, 0
    xlabel = r'$\sigma_x$ (mm)'
    ylabel = r'$\langle xx^{\prime}\rangle$ ($\mu$m)'
if axis is 'y':
    ix, iy, ig = 1, 3, 1
    xlabel = r'$\sigma_y$ (mm)'
    ylabel = r'$\langle yy^{\prime}\rangle$ ($\mu$m)'
  
x = res1[:,ix]
y = res1[:,iy]
z = goal[:,ig]

max_goal = 0.5

xmin, xmax = np.min(x), np.max(x); print(xmin, xmax)
ymin, ymax = np.min(y), np.max(y); print(ymin, ymax)
#xmin, xmax = 1, 2
#ymin, ymax = 0.1, 1.5

ngridx, ngridy = len(np.unique(x))*1, len(np.unique(y))*1

# Create grid values first.
xi = np.linspace(xmin, xmax, ngridx)
yi = np.linspace(ymin, ymax, ngridy)

# Perform linear interpolation of the data (x,y)
# on a grid defined by (xi,yi)
triang1 = tri.Triangulation(x, y)
interp1 = tri.LinearTriInterpolator(triang1, z)

Xi, Yi = np.meshgrid(xi, yi)
zi = interp1(Xi, Yi)
vi = np.linspace(0, max_goal, 11)

fig, ax = plt.subplots()

ax.contour(xi, yi, zi, levels = vi,
           linewidths = 0.5, colors = 'k')
cntr1 = ax.contourf(xi, yi, zi, levels = vi)#, cmap = "RdBu_r")

cbar = fig.colorbar(cntr1, ax = ax, ticks = np.linspace(0, 0.5, 6))
cbar.ax.set_ylabel(r'$F$', labelpad = 2)

#ax.plot(x, y, 'k.', ms = 3)
ax.grid()
ax.set(xlim=(xmin, xmax), ylim=(ymin, ymax))
ax.set_xlabel(xlabel)
ax.set_ylabel(ylabel)

#fig.savefig('matching-goal-60um_xnew@2.5nC.eps')

#%% xemit and yemit scan
axis = 'x'
if axis is 'x':
    ix, iy, ig = 4, 5, 0
if axis is 'y':
    ix, iy, ig = 4, 5, 1
    
xlabel = u_emi_x
ylabel = u_emi_y


x = res1[:,ix]
y = res1[:,iy]
z = goal[:,ig]
max_goal = 0.2

xmin, xmax = np.min(x), np.max(x); print(xmin, xmax)
ymin, ymax = np.min(y), np.max(y); print(ymin, ymax)
#xmin, xmax = 1, 2
#ymin, ymax = 0.1, 1.5

ngridx, ngridy = len(np.unique(x))*1, len(np.unique(y))*1

# Create grid values first.
xi = np.linspace(xmin, xmax, ngridx)
yi = np.linspace(ymin, ymax, ngridy)

# Perform linear interpolation of the data (x,y)
# on a grid defined by (xi,yi)
triang1 = tri.Triangulation(x, y)
interp1 = tri.LinearTriInterpolator(triang1, z)

Xi, Yi = np.meshgrid(xi, yi)
zi = interp1(Xi, Yi)
vi = np.linspace(0, max_goal, 101)

fig, ax = plt.subplots()

#ax.contour(xi, yi, zi, levels = vi,
#           linewidths = 0.5, colors = 'k')
cntr1 = ax.contourf(xi, yi, zi, levels = vi)#, cmap = "RdBu_r")

cbar = fig.colorbar(cntr1, ax = ax, ticks = np.linspace(0, 0.5, 6))
cbar.ax.set_ylabel(r'$F_'+axis+'$', labelpad = 2)

#ax.plot(x, y, 'k.', ms = 3)
ax.grid()
ax.set(xlim=(xmin, xmax), ylim=(ymin, ymax))
ax.set_xlabel(xlabel)
ax.set_ylabel(ylabel)

fig.savefig('matching-goal-60um-'+axis+'-vs-emit@4nC.eps')

#%%
#shape = (21, 15)
shape = (11, 21)

X = np.reshape(x, shape)
Y = np.reshape(y, shape)
Z = np.reshape(z2, shape)

from matplotlib import cm
from mpl_toolkits import mplot3d

ax = plt.axes(projection='3d')
cax = ax.plot_surface(X, Y, Z, cmap = cm.coolwarm)
cbar = fig.colorbar(cax, ax = ax)
cbar.ax.set_ylabel(r'$F$', labelpad = 2)

ax.set_zlim(0, 0.5)

#%%
fig, ax = plt.subplots()

ax.contour(X, Y, Z, levels = 500, linewidths = 0.5, colors = 'k')
cntr1 = ax.contourf(X, Y, Z, levels = 500)#, cmap = "RdBu_r")
ax.grid()
#%% x scan, 100 um
fig, ax = plt.subplots()
for xxp in np.linspace(0.3, 1.8, 6):
    #xxp = alpha2cov(-alp)*1e6
    alp = cov2alpha(-xxp*1e-6, P0 = 17.05)
    direc = 'backup'+os.sep+'n250k-sig_x-1.55mm-sig_y-0.30mm-alp_x-%.2f-alp_y-3.25-nemit_x-4.00um_nemit_y-4.00um' % alp
    
    data = np.loadtxt(direc+os.sep+'ast.Yemit.001')
    
    ax.plot(data[:,0], data[:,3], '-', label = r'$\langle xx^{\prime}\rangle$ = %.1f um' % xxp)
ax.grid()
ax.legend()
ax.set_ylim(0, 0.5)
ax.set_xlabel(r'$z$ (m)')
ax.set_ylabel(r'$\sigma_y$ (mm)')
fig.savefig('sig_y-1.55mm-alp_x-scan-100um.png')
#%% y scan, 100 um
fig, ax = plt.subplots()
for cov in np.linspace(0.1, 1.6, 6):
    alp = cov2alpha(-cov*1e-6, P0 = 17.05)
    direc = 'backup'+os.sep+'n250k-sig_x-1.55mm-sig_y-0.30mm-alp_x-10.93-alp_y-%.2f-nemit_x-4.00um_nemit_y-4.00um' % alp
    
    data = np.loadtxt(direc+os.sep+'ast.Yemit.001')
    
    ax.plot(data[:,0], data[:,3], '-', label = r'$\langle yy^{\prime}\rangle$ = %.1f um' % cov)
ax.grid()
ax.legend()
ax.set_ylim(0, 1)
ax.set_xlabel(r'$z$ (m)')
ax.set_ylabel(r'$\sigma_x$ (mm)')
fig.savefig('sig_y-0.30mm-alp_y-scan-100um.png')
#%% x 60 um
fig, ax = plt.subplots()
for xxp in np.linspace(0.2, 1.0, 5):
    #xxp = alpha2cov(-alp_x)*1e6
    alp = cov2alpha(-xxp*1e-6, P0 = 22)
    direc = 'backup60um'+os.sep+'n250k-sig_x-1.10mm-sig_y-0.30mm-alp_x-%.2f-alp_y-3.00-nemit_x-4.00um_nemit_y-4.00um' % alp
    
    data = np.loadtxt(direc+os.sep+'ast.Xemit.001')
    
    ax.plot(data[:,0], data[:,3], '-', label = r'$\langle xx^{\prime}\rangle$ = %.2f um' % xxp)
ax.grid()
ax.legend()
ax.set_ylim(0, 2.5)
ax.set_xlabel(r'$z$ (m)')
ax.set_ylabel(r'$\sigma_x$ (mm)')
fig.savefig('sig_x-1.10mm-alp_x-scan-60um.png')

#%% y scan, 60 um
fig, ax = plt.subplots()
for cov in np.linspace(0.1, 1.1, 6):
    alp = cov2alpha(-cov*1e-6, P0 = 22)
    direc = 'backup60um'+os.sep+'n250k-sig_x-1.10mm-sig_y-0.25mm-alp_x-7.53-alp_y-%.2f-nemit_x-4.00um_nemit_y-4.00um' % alp
    
    data = np.loadtxt(direc+os.sep+'ast.Yemit.001')
    
    ax.plot(data[:,0], data[:,3], '-', label = r'$\langle yy^{\prime}\rangle$ = %.1f um' % cov)
ax.grid()
ax.legend()
ax.set_ylim(0, 1)
ax.set_xlabel(r'$z$ (m)')
ax.set_ylabel(r'$\sigma_y$ (mm)')
fig.savefig('sig_y-0.25mm-alp_y-scan-60um.png')

#%% xemit scan 60 um
fig, ax = plt.subplots()
for v in np.linspace(4, 8, 5):
    #xxp = alpha2cov(-alp_x)*1e6
    alp_x = cov2alpha(-0.50*1e-6, v*1e-6, P0 = 22)
    alp_y = cov2alpha(-0.30*1e-6, 4*1e-6, P0 = 22)
    direc = '.'+os.sep+'n250k-sig_x-1.10mm-sig_y-0.25mm-alp_x-%.2f-alp_y-%.2f-nemit_x-%.2fum_nemit_y-4.00um' % (alp_x, alp_y, v)
    
    data = np.loadtxt(direc+os.sep+'ast.Xemit.001')
    
    ax.plot(data[:,0], data[:,3], '-', label = r'$\varepsilon_{x,n}$ = %.2f um' % v)
ax.grid()
ax.legend()
ax.set_ylim(0, 1.5)
ax.set_xlabel(r'$z$ (m)')
ax.set_ylabel(r'$\sigma_x$ (mm)')
fig.savefig('sig_x-vs-z-emit-scan-60um@4nC.png')

#%% x scan, 100 um, SASE
fig, ax = plt.subplots()
for xxp in np.linspace(0.8, 1.4, 4):
    
    direc = 'xxp-yyp-scan-SASE'+os.sep+'%.2fum_0.40um' % xxp
    print(direc)
    
    data = np.loadtxt(direc+os.sep+'ast.Xemit.001')
    
    ax.plot(data[:,0], data[:,3], '-', label = r'$\langle xx^{\prime}\rangle$ = %.1f um' % xxp)
ax.grid()
ax.legend()
ax.set_ylim(0, 0.5)
ax.set_xlabel(r'$z$ (m)')
ax.set_ylabel(r'$\sigma_y$ (mm)')
#fig.savefig('sig_y-1.55mm-alp_x-scan-100um.png')
#%% y scan, 100 um, SASE
fig, ax = plt.subplots()
for cov in np.linspace(0.1, 1.6, 6):
    alp = cov2alpha(-cov*1e-6, P0 = 17.05)
    direc = 'xxp-yyp-scan-SASE'+os.sep+'n250k-sig_x-1.55mm-sig_y-0.30mm-alp_x-10.93-alp_y-%.2f-nemit_x-4.00um_nemit_y-4.00um' % alp
    
    data = np.loadtxt(direc+os.sep+'ast.Yemit.001')
    
    ax.plot(data[:,0], data[:,3], '-', label = r'$\langle yy^{\prime}\rangle$ = %.1f um' % cov)
ax.grid()
ax.legend()
ax.set_ylim(0, 1)
ax.set_xlabel(r'$z$ (m)')
ax.set_ylabel(r'$\sigma_x$ (mm)')
fig.savefig('sig_y-0.30mm-alp_y-scan-100um.png')
#%%
plottype = 'contourf'
signal = zi

fig, ax = plt.subplots(figsize = (4, 3))
if plottype is 'contourf':
    h, w = signal.shape
    #X = xi
    #Y = yi
    #aX, aY = np.meshgrid(X, Y)
    
    # Noise level for 2D plot
    noise = 0.001
    v = np.linspace(noise, 1, 99)*np.max(Z)

    cax = ax.contourf(X, Y, Z, v, linestyles = None, zorder=-9)
    ax.set_rasterization_zorder(-1)
    ax.set_aspect('auto')
    
elif plottype is 'imshow':
    h, w = signal.shape
    X = np.arange(0, w)*info.scale-xc
    Y = np.arange(0, h)*info.scale-yc
    
    tmp = np.copy(signal)
    tmp[tmp==0] = np.nan
    cax = ax.imshow(tmp[:,::-1], extent = (X[0], X[-1], Y[0], Y[-1]),
                    aspect = 'equal')
    del tmp
    
#%%
data = np.loadtxt('pithz.13.out', skiprows = 1171)

fig, ax = plt.subplots()
ax.plot(data[:,0])

#%%
for root, dirs, files in os.walk("."):  
    for direc in dirs:
        try:
            #os.remove(direc+os.sep+direc+'.ini')
            for it in range(11, 21):
                f = 'pithz.%d.out' % it
                os.remove(direc+os.sep+f)
        except Exception as err:
            #os.system('rm -r '+direc)
            pass
        
#%%
simdir = r'D:\PITZ'
workdir = os.path.join(simdir, '2019', 'EMSY2500pC2')
workdir = os.path.join(simdir, '2018', 'EMSY41')
os.chdir(workdir)

res1 = np.loadtxt('results.all@20m.dat'); #print len(res1)
#select = res1[:,10]<10; res1 = res1[select]; print len(res1)
D = res1[:,2]
Ipeak = res1[:,-2] 
#Ipeak = 4000e-12/(res1[:,-2]*3.3e-3/g_c)

#s0 = (res1[:,2]>3.7)*(res1[:,2]<3.8); print res1[s0,0:11]

fig, ax = plt.subplots()
ax.plot(D, Ipeak, '.')
#ax.plot(res2[:,8], res2[:,9], '.')
ax.grid()

f = lambda x, a, b, c: a+b*x+c*x*x
popt, pcov = curve_fit(f, D, Ipeak)
xx = np.linspace(3, 5, 100)
ax.plot(xx, f(xx, *popt), '-')

ax.set_xlim(3., 4.5)
ax.set_ylim(120, 200)

ax.set_xlabel(r'BSA (mm)')
ax.set_ylabel(r'$I_{\rm peak}$ (A)')

ax.legend(['Samples', 'Polynomial fit'], loc = 'lower right')
ax.text(0.925, 0.95, '($a$)', horizontalalignment='center', verticalalignment='center', transform=ax.transAxes)
#fig.savefig('I1@20m-vs-BSA@2500pC(a).eps')

#%%
workdir = os.path.join(simdir, '2019', 'LCLS-I-matching-100um',
                       '_n200k-sig_x-1.55mm-sig_y-0.30mm-alp_x-10.93-alp_y-3.25')
#workdir = get_path()
os.chdir(workdir)

fig, [ax, ax2] = plt.subplots(nrows = 2, figsize = (5, 4), sharex=True)

d1 = np.loadtxt('ast.Xemit.001'); select = (d1[:,0]<=3.6); d1 = d1[select]
d3 = np.loadtxt('ast.Xemit.002'); select = (d3[:,0]<=3.6); d3 = d3[select]

ax.plot(d1[:,0]-1.8, d1[:,3], '-')
ax.plot(d3[:,0]-1.8, d3[:,3], '-')

ax.grid()
ax.set_ylabel(r'$\sigma_x$ (mm)')

ax.set_ylim(0, 2)
ax.legend(['Space Charge ON', 'Space Charge OFF'], loc = 'upper center',
          ncol = 1, handlelength = 1.5)

d1 = np.loadtxt('ast.Yemit.001'); select = (d1[:,0]<=3.6); d1 = d1[select]
d3 = np.loadtxt('ast.Yemit.002'); select = (d3[:,0]<=3.6); d3 = d3[select]

ax2.plot(d1[:,0]-1.8, d1[:,3], '-')
ax2.plot(d3[:,0]-1.8, d3[:,3], '-')

ax2.grid()
ax2.set_xlabel(r'$z$ (m)')
ax2.set_ylabel(r'$\sigma_y$ (mm)')

ax2.set_ylim(0, 0.5)
ax2.legend(['Space Charge ON', 'Space Charge OFF'],
           loc = 'upper center')

fig.tight_layout()
fig.savefig('beam-transport-in-undulator-sc-on-and-off.png')


#%%
workdir = r'C:\Users\lixiangk\Desktop\sco-triplet-tuning2'
os.chdir(workdir)

gamma0 = M2G(22)

fig, ax = plt.subplots(ncols = 2, figsize = (8, 4))

data = np.loadtxt('scan.dat')
cov_x = alpha2cov(data[:,9], data[:,2]*1e-6, 22)*1e6 # um

ax[0].plot(data[:,0], -cov_x, 'r')
ax0 = ax[0].twinx()
ax0.plot(data[:,0], data[:,4], 'b')

data = np.loadtxt('scan_to_PSTSCR1.dat')
cov_x = alpha2cov(data[:,9], data[:,2]*1e-6, 22)*1e6 # um

ax[1].plot(data[:,0], cov_x, 'r')
ax1 = ax[1].twinx()
ax1.plot(data[:,0], data[:,4], 'b')

for axis in ax:
    axis.grid()
    axis.set_xlabel(r'Grad of third quad (T/m)')

fig.tight_layout()

#%%
workdir = r'C:\Users\lixiangk\Desktop\sco-triplet-tuning2'
os.chdir(workdir)

gamma0 = M2G(22)

fig, ax = plt.subplots()

data = np.loadtxt('scan.dat')
cov_x = alpha2cov(data[:,9], data[:,2]*1e-6, 22)*1e6 # um
rms_x = data[:,4]

ax.plot(rms_x, -cov_x)

data = np.loadtxt('scan_to_PSTSCR1.dat')
cov_x = alpha2cov(data[:,9], data[:,2]*1e-6, 22)*1e6 # um
rms_x = data[:,4]

ax.plot(rms_x, cov_x)

ax.grid()
ax.set_xlabel(u_rms_x)
ax.set_ylabel(r'$\langle xx^{\prime}\rangle$ ($\mu$m)', labelpad=-4)

ax.legend(['to be matched bf triplet', 'incoming beam'])
fig.savefig('matching-bf-triplet.eps')

#%%
grads = np.array([0.875, -1.26303079, 0.61470678, 1.6297666, -3.6364, 2.0350000])

#%%
workdir = r'C:\Users\lixiangk\Desktop\sco-triplet-tuning2'
os.chdir(workdir)

gamma0 = M2G(22)

fig, ax = plt.subplots()

data = np.loadtxt('scan_PSTScr3_to_EMSY1.dat')
cov_x = alpha2cov(data[:,9], data[:,2]*1e-6, 22)*1e6 # um
rms_x = data[:,4]
cov_x1, rms_x1, I1 = -cov_x, rms_x, data[:,0]

ax.plot(rms_x, -cov_x)

data = np.loadtxt('scan_back_to_EMSY1.dat')
cov_x = alpha2cov(data[:,9], data[:,2]*1e-6, 22)*1e6 # um
rms_x = data[:,4]
cov_x1, rms_x1, I1 = -cov_x, rms_x, data[:,0]

ax.plot(rms_x, -cov_x)

data = np.loadtxt('ParaScanSol22MeV.dat')
start, end = 7, 0
cov_x = data[start:,-4]
rms_x = data[start:,10]
cov_x2, rms_x2, I2 = cov_x, rms_x, data[start:, 6]

ax.plot(rms_x, cov_x)

ax.grid()
ax.set_xlabel(u_rms_x)
ax.set_ylabel(r'$\langle xx^{\prime}\rangle$ ($\mu$m)', labelpad=-4)

ax.legend(['to be matched bf triplet', 'incoming beam'])
#fig.savefig('matching-bf-triplet.eps')

#%%
f1 = interp1d(rms_x1, cov_x1, 'quadratic', fill_value = 'extrapolate')
f2 = interp1d(rms_x2, cov_x2, 'quadratic', fill_value = 'extrapolate')

fig, ax = plt.subplots()

tmp = np.linspace(0.5, 1.5, 100)

ax.plot(rms_x1, cov_x1, 'r*')
ax.plot(tmp, f1(tmp), 'r-')

ax.plot(rms_x2, cov_x2, 'b*')
ax.plot(tmp, f2(tmp), 'b-')

ax.grid()

fig, ax = plt.subplots()
ax.plot(tmp, f2(tmp)-f1(tmp), 'b-')

func = lambda x: f2(x)-f1(x)
r = sp.optimize.fsolve(func, [0.6])

f1I = interp1d(rms_x1, I1, 'quadratic', fill_value = 'extrapolate')
f2I = interp1d(rms_x2, I2, 'quadratic', fill_value = 'extrapolate')
print(r)
print(f1I(r), f2I(r))

#%%
# for 389.616 A, High1.Q7 is 0.6343 T/m
grads = np.array([0.475114, -0.730151, 0.364678, 0.960821, -2.199501, 1.226072])

# or
# for 387.582 A, High1.Q7 is 0.6584 T/m
grads = np.array([0.153546, -0.293827, 0.147129, 0.960821, -2.199501, 1.226072])

#%% 9 ps simulation, backward from PST.Scr3 to PST.Scr1 vs forward from EMSY1 to PST.Scr1
workdir = r'C:\Users\lixiangk\Desktop\sco-triplet-tuning3'
os.chdir(workdir)

gamma0 = M2G(22)

fig, ax = plt.subplots()

data = np.loadtxt('scan-PST.Scr3-to-PST.Scr1-new.dat')
cov_x = alpha2cov(data[:,9+2], data[:,2+2]*1e-6, 22)*1e6 # um
rms_x = data[:,4+2]
cov_x1, rms_x1, I1 = -cov_x, rms_x, data[:,0+2]
print(np.vstack((rms_x1, cov_x1, I1)).T)

ax.plot(rms_x, -cov_x)


data = np.loadtxt('scan-EMSY1-to-PST.Scr1@388A.dat')
cov_x = alpha2cov(data[:,9+2], data[:,2+2]*1e-6, 22)*1e6 # um
rms_x = data[:,4+2]
cov_x2, rms_x2, I2, I6, I4 = cov_x, rms_x, data[:,0+0], data[:,0+1], data[:,0+2]
print(np.vstack((rms_x1, cov_x1, I1)).T)

ax.plot(rms_x, cov_x)

ax.grid()
ax.set_xlabel(u_rms_x)
ax.set_ylabel(r'$\langle xx^{\prime}\rangle$ ($\mu$m)', labelpad=-4)

ax.legend(['PST.Scr3 to PST.Scr1', 'EMSY1 to PST.Scr1'])
fig.savefig('match-at-PST.SCr1@388A.png')

#%% matched beam at PST.Scr3 in the above case
res = np.array([8.52099,	 8.39537,	7.94226,	1.13137,	0.309617,	2.90376,	6.64575,	0.526117,
                2.46582, 1.79031,	0.00448236,	0,	0,	22.2799,	0.999942])

xcov = alpha2cov(res[8], res[1]*1e-6, P0 = res[13])
ycov = alpha2cov(res[9], res[2]*1e-6, P0 = res[13])
print(xcov, ycov)
#%%
f1 = interp1d(rms_x1, cov_x1, 'quadratic', fill_value = 'extrapolate')
f2 = interp1d(rms_x2, cov_x2, 'quadratic', fill_value = 'extrapolate')

fig, ax = plt.subplots()

tmp = np.linspace(rms_x1.min(), rms_x1.max(), 100)

ax.plot(rms_x1, cov_x1, 'r*')
ax.plot(tmp, f1(tmp), 'r-')

ax.plot(rms_x2, cov_x2, 'b*')
ax.plot(tmp, f2(tmp), 'b-')

ax.grid()


fig, ax = plt.subplots()
ax.plot(tmp, f2(tmp)-f1(tmp), 'b-')

func = lambda x: f2(x)-f1(x)
r = sp.optimize.fsolve(func, [1.4])

f1I = interp1d(rms_x1, I1, 'quadratic', fill_value = 'extrapolate')
f2I = interp1d(rms_x2, I2, 'quadratic', fill_value = 'extrapolate')
print(r)
print(f1I(r), f2I(r))
I70 = f1I(r)

#%% 9 ps simulation, backward from PST.Scr3 to EMSY1 vs forward from cathode to EMSY1
workdir = r'C:\Users\lixiangk\Desktop\sco-triplet-tuning3'
os.chdir(workdir)

gamma0 = M2G(22)

fig, ax = plt.subplots()

data = np.loadtxt('scan-PST.Scr3-to-EMSY1@2A.dat')
cov_x = alpha2cov(data[:,9+2], data[:,2+2]*1e-6, 22)*1e6 # um
rms_x = data[:,4+2]
cov_x1, rms_x1, I1 = -cov_x, rms_x, data[:,0+2]
print(np.vstack((rms_x1, cov_x1, I1)).T)

ax.plot(rms_x, -cov_x)


data = np.loadtxt('scan-PST.Scr3-to-EMSY1@1.9A.dat')
cov_x = alpha2cov(data[:,9+2], data[:,2+2]*1e-6, 22)*1e6 # um
rms_x = data[:,4+2]
cov_x1, rms_x1, I1, I6, I4 = -cov_x, rms_x, data[:,0+2], data[:,0+1], data[:,0+0]
print(np.vstack((rms_x1, cov_x1, I1)).T)

ax.plot(rms_x, -cov_x)

data = np.loadtxt('scan-PST.Scr3-to-EMSY1@1.8A.dat')
cov_x = alpha2cov(data[:,9+2], data[:,2+2]*1e-6, 22)*1e6 # um
rms_x = data[:,4+2]
cov_x1, rms_x1, I1, I6, I4 = -cov_x, rms_x, data[:,0+2], data[:,0+1], data[:,0+0]
print(np.vstack((rms_x1, cov_x1, I1)).T)

ax.plot(rms_x, -cov_x)

data = np.loadtxt('ParaScanSol22MeV_9ps_cov_fit.dat')
select = data[:,0]<4500; data = data[select]
start, end = 6, -2
cov_x = data[start:end,-4-12]
rms_x = data[start:end,10]
cov_x2, rms_x2, I2 = cov_x, rms_x, data[start:end, 6]

ax.plot(rms_x, cov_x)

data = np.loadtxt('SolScan1.dat')
start, end = 0, -2
cov_x = data[start:end,4]
rms_x = data[start:end,1]
cov_x2, rms_x2, I2 = cov_x, rms_x, data[start:end, 0]

ax.errorbar(rms_x, cov_x, yerr = data[start:,-2])

cov_y = data[start:,5]
rms_y = data[start:,2]
#cov_y2, rms_y2, I2 = cov_y, rms_y, data[start:, 0]

ax.errorbar(rms_y, cov_y, yerr = data[start:,-1])


ax.grid()
ax.set_xlabel(u_rms_x)
ax.set_ylabel(r'$\langle xx^{\prime}\rangle$ ($\mu$m)', labelpad=-4)

#ax.legend(['backward from PST', 'forward from the gun'])
#fig.savefig('match-at-EMSY1-exp++.png')

#%%
f1 = interp1d(rms_x1, cov_x1, 'quadratic', fill_value = 'extrapolate')
f2 = interp1d(rms_x2, cov_x2, 'quadratic', fill_value = 'extrapolate')

fig, ax = plt.subplots()

tmp = np.linspace(rms_x2.min(), rms_x2.max(), 100)

ax.plot(rms_x1, cov_x1, 'r*')
ax.plot(tmp, f1(tmp), 'r-')

ax.plot(rms_x2, cov_x2, 'b*')
ax.plot(tmp, f2(tmp), 'b-')

ax.grid()


fig, ax = plt.subplots()
ax.plot(tmp, f2(tmp)-f1(tmp), 'b-')

func = lambda x: f2(x)-f1(x)
r = sp.optimize.fsolve(func, [1.4])

f1I = interp1d(rms_x1, I1, 'quadratic', fill_value = 'extrapolate')
f2I = interp1d(rms_x2, I2, 'quadratic', fill_value = 'extrapolate')
print(r)
print(f1I(r), f2I(r))
I70 = f1I(r)
#%%
f1Q4 = interp1d(I1, I4, 'quadratic', fill_value = 'extrapolate')
I40 = f1Q4(I70)
f1Q6 = interp1d(I1, I6, 'quadratic', fill_value = 'extrapolate')
I60 = f1Q6(I70)
print([I40, I60, I70])

#%%
grads = np.array([0.80790936, -1.2134239, 0.6224262, 1.304083,	-3.271195,	1.8])
print(grads/0.65)

# dist = pd_loadtxt('ast.2745.001')
# dist[:,3] *= -1
# dist[:,4] *= -1
# np.savetxt('forward.2745.001', dist, fmt = '%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%4d%4d')

# after tuning the beam size on PST.Scr1
screens = ['HIGH1.SCR4', 'PST.SCR1', 'PST.SCR2', 'PST.SCR3', 'PST.SCR4']
zpos = [scrns[scr] for scr in screens]
env = np.array([[1.66, 1.37, 1.29, 1.30, 1.15], [1.75, 1.37, 1.27, 0.27, 1.34]]).T

fig, ax = plt.subplots(figsize = (5, 3))
ax.plot(zpos, env[:,0], '*')
ax.plot(zpos, env[:,1], '<')

data = np.loadtxt('BeamDynamics_backward.dat')
ax.plot(data[:,0]+8.41, data[::-1,3], 'r-')
ax.plot(data[:,0]+8.41, data[::-1,4], 'b-')

data = np.loadtxt('BeamDynamics_backward1.dat')
ax.plot(data[:,0]+12.277, data[::1,3], 'r--')
ax.plot(data[:,0]+12.277, data[::1,4], 'b--')


data = np.loadtxt('BeamDynamics.dat')
ax.plot(data[:,0]+13.798, data[:,3], 'r-')
ax.plot(data[:,0]+13.798, data[:,4], 'b-')

ax.grid()
ax.set_ylim(0, 2.5)
ax.set_xlim(8, 18)

ax.set_xlabel(r'$z$ (m)')
ax.set_ylabel(r'RMS size (mm)')

ax.legend(['measured $x$', 'mesured $y$', 'simulated $x$', 'simulated $y$'])
#fig.savefig('tmp.png')

#%%
workdir = r'C:\Users\lixiangk\Desktop\sco-triplet-tuning3\scan-PST.Scr3-to-PST.Scr1\Q1-1.80T_m-Q2--3.27T_m-Q3-1.31T_m'
os.chdir(workdir)

data = pd_loadtxt('ast.0152.002')
data[1:,3:6] *= -1

np.savetxt('ast.0152.012', data, fmt = ast_fmt)

#%%
def I2Gx(I):
    if I>0:
        r = -0.686*I+0.081
    else:
        r = -0.687*I-0.073
    return r

def I2Gy(I):
    if I>0:
        r = 0.7128798*I-0.09945292
    else:
        r = 0.7072355*I+0.10050549
    return r

def I2G(I):
    return 0.5*(I2Gx(I)-I2Gy(I))

#%%#%%
def Gx2I(G):
    if G<0:
        r = -1.4578783*G+0.11869394
    else:
        r = -1.45460901*G-0.10700756
    return r

def Gy2I(G):
    if G>0:
        r = 1.40239547*G+0.1401052
    else:
        r = 1.41152547*G-0.14604085
    return r

def G2I(G):
    return 0.5*(Gx2I(G)+Gy2I(-G))

#%%

data = np.loadtxt('scan-PST.Scr3-to-PST.Scr1.dat')
cov_x = alpha2cov(data[:,9+2], data[:,2+2]*1e-6, 22)*1e6 # um
rms_x = data[:,4+2]


#%% 9 ps, forward to PST.Scr1
workdir = r'C:\Users\lixiangk\Desktop\sco-triplet-tuning3'
os.chdir(workdir)

gamma0 = M2G(22)

fig, ax = plt.subplots()

data = np.loadtxt('scan-PST.Scr3-to-PST.Scr1.dat')
cov_x = alpha2cov(data[:,9], data[:,2]*1e-6, 22)*1e6 # um
rms_x = data[:,4]
cov_x1, rms_x1, I1 = -cov_x, rms_x, data[:,0]

ax.plot(rms_x, -cov_x)

data = np.loadtxt('scan-EMSY1-to-PST.Scr1@390A.dat')
cov_x = alpha2cov(data[:,9], data[:,2]*1e-6, 22)*1e6 # um
rms_x = data[:,4]
cov_x2, rms_x2, I2 = cov_x, rms_x, data[:,0]

ax.plot(rms_x, cov_x)

ax.grid()
ax.set_xlabel(u_rms_x)
ax.set_ylabel(r'$\langle xx^{\prime}\rangle$ ($\mu$m)', labelpad=-4)

ax.legend(['to be matched bf triplet', 'incoming beam'])
#fig.savefig('matching-bf-triplet.eps')

#%%
f1 = interp1d(rms_x1, cov_x1, 'quadratic', fill_value = 'extrapolate')
f2 = interp1d(rms_x2, cov_x2, 'quadratic', fill_value = 'extrapolate')

fig, ax = plt.subplots()

tmp = np.linspace(0.75, 1.95, 100)

ax.plot(rms_x1, cov_x1, 'r*')
ax.plot(tmp, f1(tmp), 'r-')

ax.plot(rms_x2, cov_x2, 'b*')
ax.plot(tmp, f2(tmp), 'b-')

ax.grid()

fig, ax = plt.subplots()
ax.plot(tmp, f2(tmp)-f1(tmp), 'b-')

func = lambda x: f2(x)-f1(x)
r = sp.optimize.fsolve(func, [1.4])

f1I = interp1d(rms_x1, I1, 'quadratic', fill_value = 'extrapolate')
f2I = interp1d(rms_x2, I2, 'quadratic', fill_value = 'extrapolate')
print(r)
print(f1I(r), f2I(r))


#%% Polynomial fit to the beam size along z for the covariance
workdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim\2020\MBI'
os.chdir(workdir)

data = np.loadtxt('ParaScanSol22MeV_9ps_cov_fit.dat')
select = data[:,0] == 4250
data = data[select]

start = 7
diag = BeamDiagnostics()

idx_cov_xxp = diag.idx_cov_xxp+start
idx_cov_yyp = diag.idx_cov_yyp+start

idx_std_xp = diag.idx_std_xp+start
idx_std_yp = diag.idx_std_yp+start

Imain = data[:,start_idx-1]


start_fit = -12

std_xp2, cov_xxp, std_x2 = data[:,start_fit:start_fit+3].T[:]
std_xp = np.sqrt(std_xp2)
cov_xxp *= 0.5
std_x = np.sqrt(std_x2)

err_xp2, err_xxp, err_x2 = data[:,start_fit+3:start_fit+6].T[:]
err_xp = err_xp2/2/std_xp
err_xxp *= 0.5
err_x = err_x2/2/std_x

fig, ax = plt.subplots()
ax.errorbar(Imain, data[:,idx_cov_xxp])
ax.errorbar(Imain, cov_xxp, yerr = err_xxp)

ax.grid()
ax.set_xlabel(r'Imain (A)')
ax.set_ylabel(r'$\langle xx^{\prime} \rangle$ (um)')
ax.legend(['Astra', 'Poly fit'])
fig.savefig('cov_xxp-vs-Imain-4250pC.png')


fig, ax = plt.subplots()
ax.errorbar(Imain, data[:,idx_std_xp])
ax.errorbar(Imain, std_xp, yerr = err_xp)
#ax.errorbar(Imain, std_xp, yerr = np.sqrt(data[:,start_fit+3])/2/np.sqrt(data[:,start_fit]))

ax.grid()
ax.set_xlabel(r'Imain (A)')
ax.set_ylabel(r'$\sigma_{x^{\prime}}$ (mrad)')
ax.legend(['Astra', 'Poly fit'])
fig.savefig('std_xp-vs-Imain-4250pC.png')


eps_x = np.sqrt(std_x**2*std_xp**2-cov_xxp**2)
mmt = data[:,start_idx+6]
gg = M2G(mmt)

err_eps_x = (std_x**2*std_xp**4*err_x**2+std_x**4*std_xp**2*err_xp**2+cov_xxp**2*err_xxp**2)/eps_x

fig, ax = plt.subplots()
ax.errorbar(data[:,start_idx-1], data[:,start_idx+1])
ax.errorbar(data[:,start_idx-1], eps_x*gg, yerr = err_eps_x*gg)

ax.grid()
ax.set_xlabel(r'Imain (A)')
ax.set_ylabel(u_emi_x)
ax.legend(['Astra', 'Poly fit'])
fig.savefig('emit_xn-vs-Imain-4250pC.png')

#%% Polynomial fit to the beam size along z for the covariance
workdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim\2020\MBI'
os.chdir(workdir)

data = np.loadtxt('ParaScanSol22MeV_9ps_cov_fit4.dat')
select = data[:,0] == 4250
data = data[select]

start = 7
diag = BeamDiagnostics()

idx_cov_xxp = diag.idx_cov_xxp+start
idx_cov_yyp = diag.idx_cov_yyp+start

idx_std_xp = diag.idx_std_xp+start
idx_std_yp = diag.idx_std_yp+start

Imain = data[:,start-1]


start_fit = -20

std_x2, cov_xxp, std_xp2 = data[:,start_fit:start_fit+3].T[:]
std_xp = np.sqrt(std_xp2)
cov_xxp *= 0.5
std_x = np.sqrt(std_x2)

err_x2, err_xxp, err_xp2 = data[:,start_fit+5:start_fit+8].T[:]
err_xp = err_xp2/2/std_xp
err_xxp *= 0.5
err_x = err_x2/2/std_x

fig, ax = plt.subplots()
ax.errorbar(Imain, data[:,idx_cov_xxp])
ax.errorbar(Imain, cov_xxp, yerr = err_xxp)

ax.grid()
ax.set_xlabel(r'Imain (A)')
ax.set_ylabel(r'$\langle xx^{\prime} \rangle$ (um)')
ax.legend(['Astra', 'Poly fit'])
#fig.savefig('cov_xxp-vs-Imain-4250pC.png')


fig, ax = plt.subplots()
ax.errorbar(Imain, data[:,idx_std_xp])
ax.errorbar(Imain, std_xp, yerr = err_xp)
#ax.errorbar(Imain, std_xp, yerr = np.sqrt(data[:,start_fit+3])/2/np.sqrt(data[:,start_fit]))

ax.grid()
ax.set_xlabel(r'Imain (A)')
ax.set_ylabel(r'$\sigma_{x^{\prime}}$ (mrad)')
ax.legend(['Astra', 'Poly fit'])
#fig.savefig('std_xp-vs-Imain-4250pC.png')


eps_x = np.sqrt(std_x**2*std_xp**2-cov_xxp**2)
mmt = data[:,start+6]
gg = M2G(mmt)

err_eps_x = (std_x**2*std_xp**4*err_x**2+std_x**4*std_xp**2*err_xp**2+cov_xxp**2*err_xxp**2)/eps_x

fig, ax = plt.subplots()
ax.errorbar(Imain, data[:,start+1])
ax.errorbar(Imain, eps_x*gg, yerr = err_eps_x*gg)

ax.grid()
ax.set_xlabel(r'Imain (A)')
ax.set_ylabel(u_emi_x)
ax.legend(['Astra', 'Poly fit'])
#fig.savefig('emit_xn-vs-Imain-4250pC.png')

#%% Example of poly fit
workdir = r'Q-4250.00pC-D-4.20mm-E1-60.00MV_m-phi1-0.00deg-E2-18.59MV_m-phi2--20.00deg-I-386.00A'
#os.chdir(workdir)

fname = 'ast.Xemit.003'
data = np.loadtxt(fname)
fx = interp1d(data[:,0], data[:,3])
fname = 'ast.Yemit.003'
data = np.loadtxt(fname)
fy = interp1d(data[:,0], data[:,3])

z = np.array([5.28, 6.25, 7.125, 8.14])
xrms = fx(z)
yrms = fy(z)
z -= 5.28

poly2 = lambda x, a, b, c: a*x**2+b*x+c
popt, pcov = curve_fit(poly2, z, xrms**2)
res = res+list(popt)+list(np.sqrt(np.diag(pcov)))

# poly2 = lambda x, a, b, c: a*x**2+b*x+c
# popt, pcov = curve_fit(poly2, z, yrms**2)
# res = res+list(popt)+list(np.sqrt(np.diag(pcov)))

tmp = np.linspace(0, z[-1])
fig, ax = plt.subplots(figsize = (4, 3))
ax.plot(z+5.28, xrms**2, '<')
ax.plot(tmp+5.28, poly2(tmp, *popt), '-')

ax.grid()
ax.set_xlabel(u_z)
ax.set_ylim(0.5, 2.5)
ax.set_yticks([0.5, 1.0, 1.5, 2.0, 2.5])
ax.set_ylabel(r'$\sigma_x^2$ (mm$^2$)', labelpad = -3)

fig.savefig('rms_x2-vs-z-4nC-386A.png')


#%% xcov vs xrms under various laser pulse lengths
workdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim\2020\MBI'
os.chdir(workdir)

start = 7
diag = BeamDiagnostics()

idx_cov_xxp = diag.idx_cov_xxp+start
idx_std_x = diag.idx_std_x+start


fig, ax = plt.subplots()

data = np.loadtxt('ParaScanSol22MeV_6ps_cov_fit.dat')
select = data[:,0] == 4250
data = data[select]

ax.plot(data[:,idx_std_x], data[:,idx_cov_xxp])

data = np.loadtxt('ParaScanSol22MeV_9ps_cov_fit.dat')
select = data[:,0] == 4250
data = data[select]

ax.plot(data[:,idx_std_x], data[:,idx_cov_xxp])

data = np.loadtxt('ParaScanSol22MeV_12ps_cov_fit.dat')
select = data[:,0] == 4000
data = data[select]

ax.plot(data[:,idx_std_x], data[:,idx_cov_xxp])

ax.grid()
ax.set_xlabel(u_rms_x)
ax.set_ylabel(r'$\langle xx^{\prime}\rangle$ ($\mu$m)', labelpad=-4)

ax.legend(['6 ps', '9 ps', '12 ps'])
fig.savefig('cov_xxp-vs-std_x-vs-laser-pulse-length.png')

#%% nemit_x at EMSY1 vs Imain under various laser pulse lengths
start = 7
diag = BeamDiagnostics()

idx_nemit_x = diag.idx_nemit_x+start

fig, ax = plt.subplots()

data = np.loadtxt('ParaScanSol22MeV_6ps_cov_fit.dat')
select = data[:,0] == 4250
data = data[select]

ax.plot(data[:,start-1], data[:,idx_nemit_x])

data = np.loadtxt('ParaScanSol22MeV_9ps_cov_fit.dat')
select = data[:,0] == 4250
data = data[select]

ax.plot(data[:,start-1], data[:,idx_nemit_x])

data = np.loadtxt('ParaScanSol22MeV_12ps_cov_fit.dat')
select = data[:,0] == 4000
data = data[select]

ax.plot(data[:,start-1], data[:,idx_nemit_x])

ax.grid()
ax.set_xlabel(u_rms_x)
ax.set_ylabel(r'$\langle xx^{\prime}\rangle$ ($\mu$m)', labelpad=-4)

ax.legend(['6 ps', '9 ps', '12 ps'])
fig.savefig('nemit_x-vs-Imain-vs-laser-pulemitse-length.png')

#%% Backward PST.Scr3 to PST.Scr1 to Hgih1.Scr4
from universal import *
workdir = r'scan-PST.Scr3-to-PST.Scr1-new'
os.chdir(workdir)

res = np.loadtxt('../scan-PST.Scr3-to-PST.Scr1-new.dat')

def make_folder(y):
    direc = ''
    for i, xi in enumerate(y):
        direc += str.format('Q%d-%.2fT_m-' %  (i+1, xi))
    direc = direc[:-1]
    return direc

temp = []
for i, v in enumerate(res[:-1]):
    #print(v[0:3][::-1])
    direc = make_folder(v[0:3][::-1])
    print(direc)

    data = np.loadtxt(direc+os.sep+'BeamDynamics.dat')
    
    fx = interp1d(data[:,0], data[:,3])
    fy = interp1d(data[:,0], data[:,4])
    
    temp.append([v[0], v[1], v[2], fx(5.388), fy(5.388), fx(1.520), fy(1.520)])

temp = np.array(temp); temp1 = temp
np.savetxt('RMS-size-with-focusing.dat', temp, fmt = '%12.6f')

fig, ax = plt.subplots(figsize = (5, 4))
ax.plot(temp[:,3], temp[:,5], '-o')
ax.grid()
ax.set_xlabel(r'RMS size at High1.Scr4 (mm)')
ax.set_ylabel(r'RMS size at PST.Scr1 (mm)')

fig.savefig('RMS-size-with-focusing.png')

os.chdir('..')

# fig, ax = plt.subplots()
# ax.plot(temp[:,2], temp[:,0], '-<')
# ax.plot(temp[:,1], temp[:,0], '-<')

#% Forward EMSY1 to High1.Scr4 to PST.Scr1
workdir = r'scan-EMSY1-to-PST.Scr1@388A'
os.chdir(workdir)

res = np.loadtxt('../scan-EMSY1-to-PST.Scr1@388A.dat')

temp = []
for i, v in enumerate(res):
    #print(v[0:3][::-1])
    direc = make_folder(v[0:3])
    print(direc)

    data = np.loadtxt(direc+os.sep+'BeamDynamics.dat')
    
    fx = interp1d(data[:,0], data[:,3])
    fy = interp1d(data[:,0], data[:,4])
    
    temp.append([v[0], v[1], v[2], fx(3.133), fy(3.133), fx(7.001), fy(7.001)])

temp = np.array(temp); temp2 = temp
np.savetxt('RMS-size-with-focusing.dat', temp, fmt = '%12.6f')

#fig, ax = plt.subplots(figsize = (5, 4))
ax.plot(temp[:,3], temp[:,5], '-o')
#ax.grid()
ax.set_xlabel(r'RMS size at High1.Scr4 (mm)')
ax.set_ylabel(r'RMS size at PST.Scr1 (mm)')

ax.set_xlim(0.5, 2.5)
ax.set_ylim(1, 2)
ax.legend(['Backward tracking', 'Forward tracking'], loc = 'upper left')
fig.savefig('RMS-size-with-focusing.png')

os.chdir('..')


# fig, ax = plt.subplots()
# ax.plot(temp[:,0], -temp[:,1], '-<')
# ax.plot(temp[:,0], temp[:,2], '-<')

from intersect import intersection

x, y = intersection(temp1[:,3], temp1[:,5], temp2[:,3], temp2[:,5])

T1 = np.zeros(3)
for i in [0, 1, 2]:
    fQ = interp1d(temp2[:,3], temp2[:,i])
    T1[i] = fQ(x)[0]

T2 = np.zeros(3)
for i in [0, 1, 2]:
    fQ = interp1d(temp1[:,3], temp1[:,i])
    T2[i] = fQ(x)[0]

#%%
grads = list(T1)+list(T2)

quadNames = ['HIGH1.Q4', 'HIGH1.Q6', 'HIGH1.Q7', 'PST.QM2', 'PST.QM3', 'PST.QT1']

positions = [quads[qname] for qname in quadNames]

run_sco(grads, positions, 'ast.0527.001', Zstart = 5.28, Zstop = 15.318)

#7.00075	7.52808	7.82002	1.36613	1.363460	2.91084	10.8063	10.36220	-0.838608	-0.885617	0.00368266	0	0	22.2799	0.999942
#8.51775	8.43683	7.98586	1.15340 0.321689	    2.90377	6.8732	0.564842	2.471570	1.860790	0.00448066	0	0	22.2799	0.999942

Xrms, Yrms = 1.15340, 0.321689	
Xcov = alpha2cov(2.471570, 8.43683e-6, K2M(22.2799))
Ycov = alpha2cov(1.860790, 7.98586e-6, K2M(22.2799))
print(Xrms, Xcov, Yrms, Ycov)
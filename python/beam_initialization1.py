import numpy as np
from scipy.constants import c
from warp import PicklableFunction


def printer(text, *args):
    print(text)
    with open('warp_1x0.5_2.log', 'a') as f_handle:
        f_handle.write(text+'\n')

clight = c
class BeamInjector( object ):

    def __init__(self, elec, w3d, top, dim, filename, xmean=0., ymean=0., zmean=0.,
                 js=None, lmomentum=0, gamma_boost=1, **kw):
        """
        Initialize an injector for the plasma.

        Parameters
        ----------
        elec: a Species object, as defined in Warp
           The particles that represent the electrons

        w3d, top: Forthon objects

        dim : str
           The dimension of the simulation (either "2d", "circ" or "3d")

        """
        # Register the species to be injected
        self.elec = elec
        self.dim = dim
        self.w3d = w3d
        self.top = top

        self.add_available_dist_boosted(filename, xmean, ymean, zmean,
                                        js = js, lmomentum = lmomentum, gamma_boost = gamma_boost, **kw)

        # For the continuous injection:
        # self.injection_direction = injection_direction

        # Inject particles after next step
        tmin = self.top.time+self.top.dt
        tmax = tmin+self.top.dt
        self.load_beam( tmin, tmax )

    def add_available_dist_boosted(self, filename, xmean=0., ymean=0., zmean=0.,
                                   js=None, lmomentum=0, gamma_boost=1, **kw):
        """
        Add particles with a Gaussian distribution.
        - xmean,ymean,zmean: center of the cylinder, defaults to 0.
        - zdist='random': type of random distribution along z, possible values are
                       'random' and 'regular'
        - nz=1000: number of data points to use along z
        - fourfold=False: whether to use four fold symmetry.
        - js: particle species number, don't set it unless you mean it
        - lmomentum=false: Set to false when velocities are input as velocities, true
                        when input as massless momentum (as WARP stores them).
                        Only used when top.lrelativ is true.
        - gamma_boost: boost factor
        Note that the lreturndata option doesn't work.
        """

        inverse_gamma_boost = 1./gamma_boost
        beta_boost = np.sqrt(1.-inverse_gamma_boost**2)
        def drift_backward(x, xp, L):
            return [x+xp*L, xp]
        def lab2boosted(z):
            return [gamma_boost*z, -gamma_boost*beta_boost*z/clight]
        
        #kw['lmomentum'] = lmomentum
        self.lmomentum = lmomentum

        data = np.loadtxt(filename)
        nop = len(data)

        xa, ya, za, uxa, uya, uza, wa = data.T[:]
        za = za-np.mean(za)+zmean

        ga = np.sqrt(1.+(uxa*uxa+uya*uya+uza*uza)/clight**2)
        
        vxa = uxa/ga*clight # beta*c in lab frame at t = 0
        vya = uya/ga*clight
        vza = uza/ga*clight
            
        # transfrom from lab frame to boosted frame here
        # injection time in boosted frame (t = 0 in lab frame)
        tb = -gamma_boost*beta_boost*za/clight 

        xb = xa             # x at t = tb in boosted frame (t = 0 in lab frame)
        yb = ya             # y at t = tb in boosted frame (t = 0 in lab frame)
        zb = za*gamma_boost # z at t = tb in boosted frame (t = 0 in lab frame)

        ccc = (1.-beta_boost*vza/clight)

        vxb = vxa/gamma_boost/ccc
        vyb = vya/gamma_boost/ccc
        vzb = (vza-beta_boost*clight)/ccc
        gb = 1./np.sqrt(1.-(vxb**2+vyb**2+vzb**2)/clight**2)
        
        if lmomentum:
            gib = 1./gb
        else:
            gib = 1.
        # end of transform

        self.xb = xb
        self.yb = yb
        self.zb = zb
        self.tb = tb
        self.vxb = vxb
        self.vyb = vyb
        self.vzb = vzb
        self.gib = gib
        self.js = js
            
    def load_beam( self, tmin, tmax ):
        """
        Load beam between tmin and tmax.

        Parameters
        ----------
        tmin, tmax : floats (seconds)
           Injection time between which the beam is to be loaded, in the
           local domain
        """

        select = (self.tb>=tmin)*(self.tb<tmax)
        nop = np.sum(select)
        
        if nop > 0:
            
            printer('%6d electrons loaded at: %.3f ps' % (nop, self.top.time*1e12))

            xb = self.xb[select]
            yb = self.yb[select]
            zb = self.zb[select]
            tb = self.tb[select]
            vxb = self.vxb[select]
            vyb = self.vyb[select]
            vzb = self.vzb[select]
            gib = self.gib[select]
            js = self.js


            dt = tb-self.top.time
            xb = xb-vxb*dt
            yb = yb-vyb*dt
            zb = zb-vzb*dt

            vxb = vxb/gib
            vyb = vyb/gib
            vzb = vzb/gib
            
            # Load the electrons (on top of each other)
            if self.elec is not None:
                self.elec.addparticles(xb, yb, zb, vxb, vyb, vzb, gib, js, lmomentum = self.lmomentum)

    def continuous_injection(self):
        """
        Routine which is called by warp at each timestep
        """

        tmin = self.top.time+self.top.dt
        tmax = tmin+self.top.dt
        
        self.load_beam(tmin, tmax)

def unalign_angles( thetap ) :
    """
    Shifts the angles by a random amount

    Parameter
    ---------
    thetap: 3darray
       An array of shape Nr, Nz, Ntheta, where Nr, Nz, Ntheta are the
       number of regularly-spaced macroparticles along each direction.
       The angles evenly sample [0,2 \pi], when changing the last index

    Returns
    -------
    A 1darray of length Nr*Nz*Ntheta, where a random angle has been added to
    each set of Ntheta macroparticles that are at a given position in r and z
    """
    # Generate random angles between 0 and 2 pi
    # (but only for the two first axis, to the last)
    theta_shift = 2*np.pi*np.random.rand( thetap.shape[0], thetap.shape[1] )

    # For each r and z position, add the same random shift for all Ntheta values
    theta = thetap + theta_shift[:,:,np.newaxis]

    # Flatten
    return( theta.flatten() )

# -*- coding: utf-8 -*-
"""
Created on Fri Mar 19 21:28:01 2021 for Flash RT Chengdu

@author: lixiangk
"""
from my_object import *

#%% Rotate or translate particle distributions
data = pd_loadtxt('ast.1037.017')
#data[1:,2] += data[0,2]
data[1:,5] += data[0,5]

# Preserve the reference particles
if data[0,-1]<0:
    data[0,-1] = 5
select = data[:,-1]>0
dist = data[select]

zmean = dist[0,2]
xmean = np.mean(dist[:,0])
dist[:,0] -= xmean

def RotateMatrix(theta, axis = 'x'):
    cs = np.cos(theta)
    ss = np.sin(theta)
    
    if axis in ['x', 'X']:
        M = np.array([[1, 0, 0],
                      [0, cs, -ss],
                      [0, ss, cs]])
    if axis in ['y', 'Y']:
        M = np.array([[cs, 0, ss],
                      [0, 1, 0],
                      [-ss, 0, cs]])
    if axis in ['z', 'Z']:
        M = np.array([[cs, -ss, 0],
                      [ss, cs, 0],
                      [0, 0, 1]])
        
    return M

theta, offset = -3.726896*np.pi/180, -xmean

theta, offset = 0*np.pi/180, -dxoff

theta, offset = 90*np.pi/180, -xmean

theta, offset = 0*np.pi/180, -0.

M = RotateMatrix(theta, 'y')

dist[0,2] = 0
temp = np.copy(dist)

for i, v in enumerate(dist[:], start = 0):
    v1 = v[0:3]
    u1 = M @ np.asarray(v1)
    
    v2 = v[3:6]
    u2 = M @ np.asarray(v2)
    
    temp[i,0:3] = u1[:]
    temp[i,3:6] = u2[:]

temp[0,2] = zmean
temp[:,0] -= offset

temp[1:,5] -= temp[0, 5]

np.savetxt('ast.1037.023', temp[::1], fmt = ast_fmt)

#%%
fig, ax = plt.subplots(figsize = (5, 5))

end = -1
#ax.plot(data[1:end:10,2], data[1:end:10,0], '.')
ax.plot(temp[1:end:10,2], temp[1:end:10,5], '.')

#ax.set_xlim(-0.02, 0.02)
#ax.set_ylim(-0.02, 0.02)
ax.grid()

#%% Check trajectory slopes
from transport.TransferMatrix import *

#FitSlopeOfRef('trk.ref.001', xmin = 1, index_x = 2, index_y = 0, scale_x = 1)
#FitSlopeOfRef('ast.ref.004', xmin = 6.2)
#FitSlopeOfRef('ast.Xemit.010', xmin = 7.5, index_x = 0, index_y = 2, scale_x = 1e-3)

FitSlopeOfRef('stat.txt', xmin = 0.1, index_x = 3, index_y = 1, scale_x = 1)

#%% Check dipole edge definition used in Astra
def PrepareDipoleCoordinates(rho, theta, poleWidth = 0, X0 = [0, 0],
                             theta0 = 0, beta1 = 0, beta2 = 0):
    '''
    Return four pairs of coordinates for the four corners of the pole face, in the form of 
    (x, z) in unit of meter.
    |--
    |                 
    A1             |
    |            B1  
    A          |
    |        B
    A2     | 
    |   B2
    | |
    |
    '''
    if poleWidth == 0:
        poleWidth = rho
    width = poleWidth/2
    sg = np.sign(theta)
    
    X0 = np.reshape(X0, (2, 1))
    Xa = np.reshape([0, 0], (2, 1))
    Xc = np.reshape([0, rho*sg], (2, 1))
    
    Xa1 = np.reshape([0,  width], (2, 1))+Xa
    Xa2 = np.reshape([0, -width], (2, 1))+Xa
    
    Xb = RotatedBy(Xa-Xc, theta)+Xc
    Xb1 = RotatedBy(Xa1-Xc, theta)+Xc
    Xb2 = RotatedBy(Xa2-Xc, theta)+Xc
    
    Xa1 = RotatedBy(Xa1-Xa, beta1)+Xa
    Xa2 = RotatedBy(Xa2-Xa, beta1)+Xa
    
    Xb1 = RotatedBy(Xb1-Xb, beta2)+Xb
    Xb2 = RotatedBy(Xb2-Xb, beta2)+Xb
    
    #
    V = np.concatenate((Xa1, Xa2, Xb1, Xb2), axis=1) # 2x4
    M = RotationMatrix(theta0) # 2x2
    #V = (M@V).T # (2x4).T -> 4x2
    V = np.matmul(M, V).T
    
    V[:,0] += X0[1]
    V[:,1] += X0[0]
    
    return [vi[::-1] for vi in V]

rho, theta = 0.2, np.pi/4
beta = 0

fig, ax = plt.subplots()

XX = PrepareDipoleCoordinates(rho, theta-beta, poleWidth = 0.36, theta0 = beta, X0 = [0, 0]) # (x, z)
XX = np.asarray(XX).reshape((4, 2)); print(XX)
ax.plot(XX[:,1], XX[:,0], '-*')

# XX = PrepareDipoleCoordinates(rho, -theta, poleWidth = 0.36, theta0 = theta, X0 = [0, 0])
# XX = np.asarray(XX).reshape((4, 2)); print(XX)
# ax.plot(XX[:,1], XX[:,0], '-*')

beta = 6.504661e-2
dz = rho*np.sin(beta)
dx = dz*np.tan(beta)
dxoff = rho*np.cos(beta)+dx-rho

XX = PrepareDipoleCoordinates(rho, theta-beta, poleWidth = 0.36, theta0 = beta, X0 = [(dx-dxoff), dz]) # (x, z)
XX = np.asarray(XX).reshape((4, 2)); print(XX)

ax.plot(XX[:,1], XX[:,0], '-*')

ax.grid()
ax.set_aspect('equal')

#%% Check quads positions used in Astra
os.chdir('../temp')
FitSlopeOfRef('ast.ref.016', xmin = 8.5, xmax = 8.8)
FitSlopeOfRef('ast.ref.016', xmin = 9.8)

aa = np.array([[8.589409E+00,  5.091461E-01],
               [8.682818E+00,  6.025550E-01],
               [8.776226E+00,  6.959638E-01],
               [9.699385E+00,  1.201900E+00],
               [9.966885E+00,  1.201900E+00],
               [1.023439E+01,  1.201900E+00]])

fig, ax = plt.subplots()

data = np.loadtxt('ast.ref.016')
f1 = interp1d(data[:,0], data[:,5]/1e3)
f1(aa[:,0])-aa[:,1]
ax.plot(data[:,0], data[:,5]/1e3, '-')
ax.plot(aa[:,0], aa[:,1], 'D')
ax.grid()

#%%
x = [-1.498503E+01, 1.276233E+01]; y=[2.6000E-01, 2.5000E-01]
ft = interp1d(x, y)
ft(0)

#%% Transport particles using transfer matirx
dist = pd_loadtxt('ast.1037.017')
#dist[:,2] = 0

dist[0,2] = 0
dist[1:,5] += dist[0,5]
dist[:,0] -= 1.2

# vx, vy, vz = momentum2velocity(dist[:,3], dist[:,4], dist[:,5], unit = g_mec2*1e6)
# dist[:,0] = dist[:,0]-vx*dist[:,2]/vz
# dist[:,1] = dist[:,1]-vx*dist[:,2]/vz

# z0 = np.copy(dist[:,2])
# dist[:,2] = 0

#sigma0, P0 = SigmaMatrix6D(dist = dist)
#P0 *= 1e3; print('P0 = ', P0, ' MeV/c')

gg = kinetic2gamma(M2K(15.403013399970584))

result = []
#%%
rho, theta = 0.2, 1.0*np.pi/2
Dip = Wedge(rho, -theta, gg)

l0, l1 = 0.2, 0.5
D0 = Drift(l0)
D1 = Drift(l1)

TR = np.ones(6)
D0, Dip, D1 = [TR*np.array(M.evalf(5), dtype = 'double')
       for M in [D0, Dip, D1]]


beam = np.zeros((len(dist),6))
beam[:,0] = dist[:,0]
beam[:,2] = dist[:,1]
beam[:,4] = dist[:,2]

beam[:,1] = dist[:,3]/dist[:,5]
beam[:,3] = dist[:,4]/dist[:,5]

Pc = np.mean(dist[:,5])
beam[:,5] = (dist[:,5]-Pc)/Pc

# Transport the particles with transfer matrix
newBeam = D1 @ Dip @ D0 @ beam.T

newBeam = newBeam.T
newBeam[:,5] *= Pc
newBeam[:,5] += Pc

newBeam[:,1] = newBeam[:,1]*newBeam[:,5]
newBeam[:,3] = newBeam[:,3]*newBeam[:,5]

newDist = np.copy(dist)
newDist[:,0] = newBeam[:,0]
newDist[:,1] = newBeam[:,2]
newDist[:,2] = newBeam[:,4]
newDist[:,3] = newBeam[:,1]
newDist[:,4] = newBeam[:,3]
newDist[:,5] = newBeam[:,5]

diag = BeamDiagnostics(dist = newDist); diag.demo()
result.append([l0+rho*theta+l1]+list(diag.x))
#%%
result = np.array(result)

fig, ax = plt.subplots()
ax.plot(result[:,0], result[:,2])
ax.plot(result[:,0], result[:,3])

fig, ax = plt.subplots()
ax.plot(result[:,0], result[:,4])
ax.plot(result[:,0], result[:,5])
ax.plot(result[:,0], result[:,6])

#%% Astra output to GPT input
def astra2gpt(infile, outfile):
    '''
    Convert Astra time snapshot distribution into gpt input beam distributions
    Parameter
      fname: the Astra output in ascii format or initial distribution in spatial domain longitudinally.
    Return
      write the beam distribution for GPT input in the ascii format
    '''
    beam = np.loadtxt(infile)
    beam[1:,2] += beam[0,2]
    beam[1:,5] += beam[0,5]
    
    beam[:,3:6] = beam[:,3:6]/g_mec2/1e6 # to beta*gamma: GBx GBy GBz
    
    ##
    beam[:,3] -= np.mean(beam[:,3])
    beam[:,4] -= np.mean(beam[:,4])
    ##
    
    #beam[:,6] = beam[:,6]*1e-9 # ns to s
    #beam[:,7] = beam[:,7]*1e-9/g_qe # number of macro particles: nmacro
    #beam[:,8] = g_qe # q
    
    beam[:,6] = beam[:,7]*1e-9/g_qe # number of macro particles: nmacro
    beam[:,7] = -g_qe # q
    
    np.savetxt(outfile, beam[:,0:8], fmt = '%20.12E',\
               header = 'x  y  z  GBx  GBy  GBz nmacro q', comments='')
    return

astra2gpt('ast.1037.023', 'gpt.1037.023')

#%% 
data = np.loadtxt('temp.txt', skiprows = 206485)

#%% Make plots
fig, ax = plt.subplots(figsize = (6, 3))

data = np.loadtxt('ast.Yemit.003')
select = data[:,0]<5; data = data[select]
ax.plot(data[:,0], data[:,3], 'r-')

data = np.loadtxt('ast.Xemit.003')
select = data[:,0]<5; data = data[select]
ax.plot(data[:,0], data[:,3], 'b-')

data = np.loadtxt('ast.Zemit.003')
select = data[:,0]<5; data = data[select]
ax.plot(data[:,0], data[:,3], 'g-')


data = np.loadtxt('ast.Yemit.011')
select = data[:,0]<8; data = data[select]
ax.plot(data[:,0], data[:,3], 'r-')

data = np.loadtxt('ast.Xemit.011')
select = data[:,0]<8; data = data[select]
ax.plot(data[:,0], data[:,3], 'b-')

data = np.loadtxt('ast.Zemit.011')
select = data[:,0]<8; data = data[select]
ax.plot(data[:,0], data[:,3], 'g-')


data = np.loadtxt('ast.Yemit.017')
select = data[:,0]<10.37; data = data[select]
ax.plot(data[:,0], data[:,3], 'r-')

data = np.loadtxt('ast.Xemit.017')
select = data[:,0]<10.37; data = data[select]
ax.plot(data[:,0], data[:,3], 'b-')

data = np.loadtxt('ast.Zemit.017')
select = data[:,0]<10.37; data = data[select]
ax.plot(data[:,0], data[:,3], 'g-')

data = np.loadtxt('stat1.txt')

ax.plot(data[:,0]*g_c+10.37, data[:,6]*1e3, 'r-')

ax.plot(data[:,0]*g_c+10.37, data[:,5]*1e3, 'b-')

ax.plot(data[:,0]*g_c+10.37, data[:,7]*1e3, 'g-')

ax.grid()

ax.legend(['$x$', '$y$', '$z$'])
ax.set_xlabel(r'$z$ (m)')
ax.set_ylabel(r'RMS (mm)')

fig.savefig('RMS-z.png')

#%%
fig, ax = plt.subplots(figsize = (6, 3))

# data = np.loadtxt('ast.Xemit.003')
# select = data[:,0]<5; data = data[select]
# ax.plot(data[:,0], data[:,2], 'r-')


data = np.loadtxt('ast.Xemit.010')
select = data[:,0]<8; data1 = data[select]
ax.plot(data1[:,0], data1[:,2], 'b-')


data = np.loadtxt('ast.Xemit.017')
select = data[:,0]<10.37; data2 = data[select]
ax.plot(data2[:,0], 100+data2[:,2], 'b-')


data3 = np.loadtxt('stat1.txt')

ax.plot(data3[:40,3]+10.37, 1300-data3[:40,1]*1e3, 'b-')


ax.grid()

#ax.legend(['$x$'])
ax.set_xlabel(r'$z$ (m)')
ax.set_ylabel(r'$y$ (mm)')

fig.savefig('Xc-z.png')

#%% Get the angles of beam trajectory
z = np.concatenate((data1[:,0], data2[:,0], data3[:40,3]+10.37))
x = np.concatenate((data1[:,2], 100+data2[:,2], 1300-data3[:40,1]*1e3))

fx = interp1d(z, x, bounds_error = False, fill_value = 'extrapolate')
zz = np.linspace(5, 10.77, 500)
xx = fx(zz)
dx_dz = np.gradient(xx/1e3, zz[1]-zz[0])

fig, ax = plt.subplots(figsize = (6, 3))
ax.plot(zz, np.arctan(dx_dz)*180/np.pi)

ftheta = interp1d(zz, np.arctan(dx_dz), bounds_error = False, fill_value = 'extrapolate')

# -*- coding: utf-8 -*-
"""
Created on Fri Feb 28 17:20:23 2020

@author: lixiangk
"""

from FEL.FEL import *


#%% Example
Nu, lam_u = 5, 10e-2

Ek = 20 # MeV
gamma = 1+Ek/g_mec2

Qe = 68e-12
sig_t = 250e-15/2.355 # fs
sig_z = sig_t*g_c

lamb = np.linspace(100e-6, 500e-6, 41)

rr=[]
for i, ll in enumerate(lamb):
    
    ww = 2*np.pi*g_c/ll
    
    ff2 = form_factor(ll, sig_t)**2
    
    KK = resonant_undulator_parameter(ll, lam_u, gamma)
    und = Undulator(Nu = Nu, lam_u = lam_u, K = KK)
    #print(und.B)
    
    SR = Superradiance(Qe, sig_t = sig_t, lam_s = ll, und = und)
    
    nj = SR.radiation_from_bunch(ww)*1e9
    rr.append([ll, nj, ff2])
    
rr = np.array(rr)

fig, ax = plt.subplots(nrows = 1, ncols = 2, figsize = (12,4))
#ax[0].plot(rr[:,0]*1e6, rr[:,1], '-', label = 'energy')
ax[0].plot(g_c/rr[:,0]/1e12, rr[:,1], 'r-', label = r'FWHM = 250 fs')
ax[1].plot(g_c/rr[:,0]/1e12, rr[:,2], 'b-', label = 'form factor')

ax[0].legend(fontsize = 11)
ax[1].legend(fontsize = 11)
#ax[0].set_yscale("log")
ax[0].set_xlabel(r'radiation frequency (THz)')
ax[0].set_ylabel(r'radiation energy (nJ)')

ax[1].set_xlabel(r'radiation frequency (THz)')
ax[1].set_ylabel(r'form factor^2')

#%% Solve bunch rms length to achieve 1 uJ radiation energy
def fun(x, *args):
    SR = Superradiance(Qtot = args[0], sig_t = x, und = LCLS1(), lam_s = args[1])
    return [SR.radiation_from_bunch(SR.omega_s), SR.ff2]

def obj(x, *args):
    return np.abs(1.0-fun(x, *args)[0]/args[-1])**2

from scipy.optimize import minimize

lam_s = 100e-6
Qtot = np.linspace(10, 100, 10)
nj = [1, 10, 100, 500, 1000]

res = []
for q in Qtot:    
    for e in nj:
        args = (q*1e-12, lam_s, e*1e-9)
        result = minimize(obj, 10e-15, args = args, method = 'Nelder-Mead'); #print(result)
        result = minimize(obj, result.x[0], args = args, method = 'Nelder-Mead'); #print(result)
        if result.success:
            f = fun(result.x, *args); #print(f)
            res.append([lam_s*1e6, q, result.x[0], f[0], f[1]])
#print(np.reshape(res, (len(res), 5)))
#%%
fig, [ax, ax2] = plt.subplots(ncols = 2, figsize = (8, 4))

res = np.array(res)
ax.plot(res[:,1], res[:,2]*g_c*1e6)
ax2.plot(res[:,1], res[:,4])
    
ax.set_xlabel(r'charge (pC)')
ax.set_ylabel(r'$\sigma_z$ ($\mu$m)')

ax2.set_xlabel(r'charge (pC)')
ax2.set_ylabel(r'form factor')
ax2.set_yscale('log')

fig.tight_layout()
#%%
res = np.array(res)
np.savetxt('sig_t-vs-q-lam_s-100um.dat', res, fmt = '%14.6E')
#%%

data = np.loadtxt('sig_t-vs-q-lam_s-100um.dat')

var = [1, 10, 100, 500, 1000]

fig, [ax, ax2] = plt.subplots(ncols = 2, figsize = (8, 4))

for v in var:
    select = np.abs(1-np.around(data[:,3]*1e9) / v)<0.05
    res = data[select]; print(res)

    ax.plot(res[:,1], res[:,2]*g_c*1e6)
    ax2.plot(res[:,1], res[:,4])
    
ax.set_xlabel(r'charge (pC)')
ax.set_ylabel(r'$\sigma_z$ ($\mu$m)')

ax2.set_xlabel(r'charge (pC)')
ax2.set_ylabel(r'form factor^2')
ax2.set_yscale('log')

lgs = ['1 nJ', '10 nJ', '100 nJ', '500 nJ', '1 uJ']
ax.legend(lgs)
ax2.legend(lgs)

fig.tight_layout()

fig.savefig('sig_t-vs-q-lam_s-100um.png')
#%%
und = LCLS1()

lam_s = 100e-6
gamma = resonant_energy(und.K, und.lam_u, lam_s)

Qe = np.linspace(10, 100, 200)*1e-12
sig_z = np.linspace(0.05e-3, 0.2e-3, 200)
sig_t = sig_z/g_c

E = np.zeros((len(Qe), len(sig_t)))
for i, q in enumerate(Qe):
    for j, s in enumerate(sig_t):
        w1 = 2*np.pi*g_c/lam_s
        ww = w1
        SR = Superradiance(q, sig_t = s, und = und, gamma = gamma, lam_s = lam_s)
        E[i,j] = SR.radiation_from_bunch(ww, w1)*1e9

#%%
extent = [sig_t.min()*1e15, sig_t.max()*1e15, Qe.min()*1e12, Qe.max()*1e12]
print(E.min(), E.max())

from matplotlib.colors import LogNorm

fig, ax = plt.subplots()
cax = ax.imshow(E[::-1,:], extent = extent, aspect = 'auto')#,
#                norm=LogNorm(vmin=1, vmax=100))

cbar = fig.colorbar(cax, fraction = 0.046, pad = 0.04)
cbar.ax.tick_params(labelsize = 11, pad = 2)
#cbar.ax.set_yscale("log")

#%%
SR = Superradiance(10e-12, Ipeak = 100, und = LCLS1(), lam_s = 100e-6)
SR.radiation_from_bunch(ww, w1)*1e9
#%%
from universal import *
path = r'C:\Users\lixiangk\Desktop\2020'
os.chdir(path)

fname = r'saveopt_fin_min_fin_sigz_analyze.txt'
data = np.loadtxt(fname)

und = LCLS1()

lam_s = 150e-6
gamma = resonant_energy(und.K, und.lam_u, lam_s)

w1 = 2*np.pi*g_c/lam_s
ww = w1

res = []
for i in np.arange(len(data)):
    Qtot = data[i,0]*1e-12
    sig_t = data[i,1]/g_c
    
    SR = Superradiance(Qtot, sig_t = sig_t, und = und, gamma = gamma, lam_s = lam_s)
    res.append([Qtot*1e12, sig_t*1e15, SR.I, SR.radiation_from_bunch(ww)*1e9, SR.ff2])
res = np.array(res)

# Make plot
fig, [ax1, ax2, ax3] = plt.subplots(ncols = 3, figsize = (12, 4))
ax1.plot(res[:,0], res[:,1]*1e-15*g_c*1e6, '-*')
ax2.plot(res[:,0], res[:,3], '-<')
ax3.plot(res[:,0], res[:,4], '-D')

ax1.set_xlabel(r'charge (pC)')
ax1.set_ylabel(r'$\sigma_z$ ($\mu$m)')

ax2.set_xlabel(r'charge (pC)')
ax2.set_ylabel(r'energy (nJ)')

ax3.set_xlabel(r'charge (pC)')
ax3.set_ylabel(r'form factor^2')
ax3.set_yscale('log')

fig.tight_layout()
fig.savefig('radiation-vs-charge-from-best-bunch-length.png')

#%% Calculate form factor vs rms bunch length
fig, ax = plt.subplots()

ss = np.linspace(10, 90, 990)*1e-6
ff2 = form_factor(100e-6, ss/g_c)**2
ax.plot(ss*1e6, ff2, '-')

ss = np.linspace(10, 90, 990)*1e-6
ff2 = form_factor(200e-6, ss/g_c)**2
ax.plot(ss*1e6, ff2, '-')

ss = np.linspace(10, 90, 990)*1e-6
ff2 = form_factor(300e-6, ss/g_c)**2
ax.plot(ss*1e6, ff2, '-')

ax.set_xlabel(r'$\sigma_z$ ($\mu$m)')
ax.set_ylabel(r'form factor^2')
ax.set_yscale('log')

ax.set_xlim(0, 100)
ax.grid()

ax.legend(['100 um', '200 um', '300 um'])

fig.tight_layout()
fig.savefig('ff2-vs-bunch-length2.png')

#%% Calculate the FEL parameter, gain length and cooperation length
Ipeak = [100, 200, 300, 400, 500]
Qtot = [1e-9, 2e-9, 3e-9, 4e-9]

res = []
for I in Ipeak:
    for Q in Qtot:
        
        ss = SASE(Q, I, dist = 'Gaussian', und = LCLS1(), lam_s = 100e-6, sig_x = 1e-3)

        res.append([I, Q, ss.rho, ss.Lg, ss.Lc])
res = np.array(res)

# Make plot
fig, [ax1, ax2] = plt.subplots(ncols = 2, figsize = (8, 4))

Qtot = [1e-9, 2e-9, 3e-9, 4e-9]

for Q in Qtot:
    select = np.abs(1-res[:,1]/Q)<0.1
    
    d1 = res[select]
    
    ax1.plot(d1[:,0], d1[:,4]*1e6)
    ax2.plot(d1[:,0], d1[:,3]*1e3)

ax1.set_xlabel(r'$I_{\rm peak}$ (A)')
ax1.set_ylabel(r'Cooperation length ($\mu$m)')

ax2.set_xlabel(r'$I_{\rm peak}$ (A)')
ax2.set_ylabel(r'Gain length (mm)')

fig.tight_layout()

#%%
und = LCLS1()

lam_s = np.linspace(50, 500, 451)*1e-6
gamma = np.array([resonant_energy(und.K, und.lam_u, ll) for ll in lam_s])
energy = (gamma-1)*g_mec2
momentum = kinetic2momentum(energy)

fig, ax = plt.subplots()
ax.plot(lam_s*1e6, momentum, '-')
ax.set_xlabel(r'Radiation wavelength ($\mu$m)')
ax.set_ylabel(r'Momentum (MeV/c)')
ax.grid()

ax.set_xlim(0, 550)
#ax.set_xticks([50, 100, 150, 200, 250, 300, 350, 400, 450, 500])
fig.savefig('resonance.eps')

#%% TELEB
path = get_path()
os.chdir(path)

Nu, lam_u, B = 8, 300e-3, 0.4
TELBE = Undulator(Nu = Nu, B = B, lam_u = lam_u)

Ek = 24
P0 = kinetic2momentum(Ek)
gamma = 1+Ek/g_mec2


Qe = 77e-12
sig_t = 150e-15 # fs
sig_z = sig_t*g_c

lamb = np.linspace(100e-6, 1000e-6, 100)

rr=[]
for i, ll in enumerate(lamb):
    
    ww = 2*np.pi*g_c/ll
    
    ff2 = form_factor(ll, sig_t)**2
    
    KK = resonant_undulator_parameter(ll, lam_u, gamma); 
    #print(KK, undulator_field(KK, lam_u))
    
    und = Undulator(Nu = Nu, lam_u = lam_u, K = KK)
    #print(und.B)
    
    SR = Superradiance(Qe, sig_t = sig_t, lam_s = ll, und = und)
    
    nj = SR.radiation_from_bunch(ww)*1e9
    rr.append([ll, nj, ff2])
    
rr = np.array(rr)

fig, ax = plt.subplots(nrows = 1, ncols = 2, figsize = (8,4))
#ax[0].plot(rr[:,0]*1e6, rr[:,1], '-', label = 'energy')
ax[0].plot(g_c/rr[:,0]/1e12, rr[:,1]/1e3, 'r-', label = r'RMS = 150 fs')
ax[1].plot(g_c/rr[:,0]/1e12, rr[:,2], 'b-', label = 'form factor')


ax[0].set_yscale("log")
ax[0].set_xlabel(r'radiation frequency (THz)')
ax[0].set_ylabel(r'radiation energy ($\mu$J)')
ax[0].grid()
ax[0].legend(fontsize = 11)

ax[1].set_xlabel(r'radiation frequency (THz)')
ax[1].set_ylabel(r'form factor^2')
ax[1].legend(fontsize = 11)
ax[1].grid()

fig.savefig('TELBE-superradiance-estimate.eps')
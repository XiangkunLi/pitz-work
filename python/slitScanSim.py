# -*- coding: utf-8 -*-
"""
Created on Wed Jun 10 09:41:52 2020

@author: lixiangk
"""
from universal import *
from image_process.ImageProcess import *
from image_process.PhaseSpace import *

simdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim'

workdir = os.path.join(simdir,'2020', 'THz', '20200214M')
os.chdir(workdir)

dx = 50e-6
data = pd_loadtxt(r'booGrad-scan/Q-3200.00pC-I-390A-0.95/ast12.0528.012')
data[1:,2] += data[0,2]+dx; data[1:,5]+=data[0,5]
select = (data[:,-1]>0); d1 = data[select]

#data = pd_loadtxt(r'Q-3200.00pC-I-380A--20deg/ast13.0528.013')
data = np.loadtxt(r'booPhase-scan/Q-3200.00pC-I-380A--20deg/ast13.0528.013')
data[1:,2] += data[0,2]; data[1:,5]+=data[0,5]
select = (data[:,-1]>0); d2 = data[select]

data = pd_loadtxt(r'booPhase-scan/Q-3200.00pC-I-390A--15deg/ast13.0528.013')
data[1:,2] += data[0,2]-dx; data[1:,5]+=data[0,5]
select = (data[:,-1]>0); d3 = data[select]

#%%
sscan = SlitScan()

L = sscan.drift
slitWidth = sscan.slitWidth
gg = momentum2gamma(sscan.momentum)
step = sscan.step
#step = 50e-6

xmax = 5e-3
xpmax = 1.5e-3

nrows = 150
nsteps = int(xmax*2/step)

PS = np.zeros((nrows, nsteps))

for i in np.arange(nsteps):
    xslit = -xmax+step/2.0+step*i
    
    select = (d1[:,0]>xslit-slitWidth/2.0)*(d1[:,0]<xslit+slitWidth/2.0)
    bl1 = d1[select]
    
    select = (d2[:,0]>xslit-slitWidth/2.0)*(d2[:,0]<xslit+slitWidth/2.0)
    bl2 = d2[select]
    
    select = (d3[:,0]>xslit-slitWidth/2.0)*(d3[:,0]<xslit+slitWidth/2.0)
    bl3 = d3[select]
    
    dist = np.concatenate((bl1, bl2, bl3))
    dist = bl2
    
    if len(dist)>0:
        xp = dist[:,3]/dist[:,5]; print(np.std(xp)*1e3, ' mrad')
        yp = dist[:,4]/dist[:,5]
        
        xx = dist[:,0]+L*xp; print(np.std((xx-xslit)/L)*1e3, ' mrad')
        yy = dist[:,1]+L*yp
        
        hist, _ = np.histogram((xx-xslit)/L, bins = nrows,
                               range = (-xpmax, xpmax))
        print(hist.shape, PS.shape)
        PS[:,i] = hist[:]

#PS -= 0.001*np.max(PS); PS[PS<0] = 0
Xscale = step; Yscale = 2*xpmax/nrows
[xc, xpc, xrms, xprms] = calcRMS(PS[::-1], Xscale, Yscale)
[eps_x, beta_x, gamma_x, alpha_x] = calcTwiss(PS[::-1], Xscale, Yscale)

plotImage(PS, scale = [Xscale, Yscale], extent = [-xmax, xmax, -xpmax, xpmax])
print(eps_x*gg)
#%%
fig, ax = plt.subplots(nrows = 2)
ax[0].plot((xx-x0)/L*1e3, yy, '.')
ax[0].grid()
ax[0].set_xlim(-0.5, 0.5)

ax[1].hist((xx-x0)/L*1e3, bins = 100, histtype = r'step', density = True,
           range = (-0.5, 0.5))
ax[1].grid()

#%%
fig, ax = plt.subplots(nrows = 2)
ax[0].plot(dist[:,2]-5.28, dist[:,5], '.')
ax[0].grid()

ax[1].hist(dist[:,5], bins = 100, histtype = r'step', density = True)
ax[1].grid()

#%%


import sys
import timeit
import os,shutil

from transport.TransferMatrix import *

#%% Transport of beam in HEDA2 using transfer matrix
#####
lq = 0.0675
Rquad = 2.15e-1 # cm

rho, theta = 0.6, np.pi/3.0
beta1, beta2, gap = 0*np.pi/6.0, 0*np.pi/6.0, 0.07

fname = 'ast.1700.009'

dist = pd_loadtxt(fname)
dist[0,2] = 0
#dist[1:,2] += dist[0,2]
dist[1:,5] += dist[0,5]

####
#select = (dist[:,0]>-50e-6)*(dist[:,0]<50e-6)
#select = ((dist[:,5]-dist[0,5])>-30e3)*((dist[:,5]-dist[0,5])<30e3)
#dist = dist[select]; print(len(dist))
####

sigma0, P0 = SigmaMatrix6D(dist = dist)
P0 *= 1e3; print('P0 = ', P0, ' MeV/c')

gg = kinetic2gamma(M2K(P0))

l0, l1 = 0.14, 0.6994

D0 = Drift(l0)
#Dip1 = Sector(rho, theta, beta1, beta2, gg, gap)
Dip1 = Wedge(rho, theta, gg)
#Dip1 = Drift(rho*theta)
D1 = Drift(l1/5)
D2 = Drift(l1/5)
D3 = Drift(l1/5)
D4 = Drift(l1/5)
D5 = Drift(l1/5)

TR = np.ones(6)
D0, Dip1, D1, D2, D3, D4, D5 = [TR*np.array(M.evalf(5), dtype = 'double')
                                for M in [D0, Dip1, D1, D2, D3, D4, D5]]
Disp3 = D5@D4@D3@D2@D1@Dip1

# dimension for the transport
dim = 4
sigma, transfer = Transport6D(sigma0, D0, Dip1, D1, D2, D3, D4, D5, dim = dim)

# positions of the ends of the elements, for plotting
ss = np.array([0, l0, rho*theta, l1/5, l1/5, l1/5, l1/5, l1/5])
for i in np.arange(1, len(ss)):
    ss[i] += ss[i-1]
    
def Demo(sigma, transfer = None, fig_ext = '.eps'):
    res = []
    for s in sigma:
        xemit = np.sqrt(np.linalg.det(s[0:2,0:2]))*gg
        yemit = np.sqrt(np.linalg.det(s[2:4,2:4]))*gg
        res.append([s[i,i]**0.5 for i in np.arange(dim)]+[xemit, yemit])
    res = np.array(res); print(res)
    
    #reset_margin(bottom=0.1, top=0.9)
    fig, [ax1, ax2, ax3]= plt.subplots(nrows = 3, figsize = (4.5, 5),
                                       sharex = True)
    res[:,0] *= 10
    res[:,dim] *= 10
    for i in [0, 2]:
        ax1.plot(ss, res[:,i])
    
    for i in [dim, dim+1]:
        ax2.plot(ss, res[:,i])   
    
    ax3.plot(ss, res[:,0]**2/res[:,dim]*gg*10)
    ax3.plot(ss, res[:,2]**2/res[:,dim+1]*gg)
    
    if transfer is not None and dim == 6:
        R16 = [ma[0,5] for ma in transfer[1:]]
        R26 = [ma[1,5] for ma in transfer[1:]]
        R56 = [ma[4,5] for ma in transfer[1:]]
        
        ax32 = ax3.twinx()
        ax32.plot(ss[1:], R16, 'c')
        ax32.plot(ss[1:], R26, 'm')
        ax32.plot(ss[1:], R56, 'k')
        ax32.set_ylabel(r'$D$', labelpad=-2)
        ax32.legend([r'$R_{16}$', r'$R_{26}$', r'$R_{56}$'],
                    loc = 'upper right')
        ax32.set_ylim(-1, 1)
        #ax32.set_yticks([-0.6, -0.3, 0, 0.3, 0.6])
    
    ax3.set_xlabel(r'$s$ (m)')
    ax1.set_ylabel(r'RMS (mm)', labelpad=3)
    ax2.set_ylabel(r'Emit. ($\mu$m)', labelpad=3)
    ax3.set_ylabel(r'$\beta_{x, y}$ (m)', labelpad=8)
    #ax1.set_ylim(0, 0.299)
    #ax2.set_ylim(0, 1.99)
    #ax3.set_ylim(0, 2.99)
    ax1.grid()
    ax2.grid()
    ax3.grid()
    
    #ax2.set_yscale('log')
    ax1.legend([r'$x\times10$', r'$y$', r'$z$'], loc = 'lower left')
    ax2.legend([r'$x\times10$', r'$y$'], loc = 'upper left')
    ax3.legend([r'$x\times10$', r'$y$'], loc = 'upper left')        
    
    plt.tight_layout(h_pad = 0)
    fig.savefig('Transport@'+fname+fig_ext)
    
    return
Demo(sigma, transfer, '.png')

#%%
dist = pd_loadtxt('ast.1000.002')
dist[0,2] = 0
dist[1:,5] += dist[0,5]

# Get reference z and P distributions
zRef = dist[:,2]
PRef = dist[:,5]

dist = pd_loadtxt('ast.1700.002')
dist[:,2] = 0
dist[1:,5] += dist[0,5]

#PRef = dist[:,5]
#%%
beam = np.zeros((len(dist),6))
beam[:,0] = dist[:,0]
beam[:,2] = dist[:,1]
beam[:,4] = dist[:,2]

beam[:,1] = dist[:,3]/dist[:,5]
beam[:,3] = dist[:,4]/dist[:,5]

Pc = np.mean(dist[:,5])
beam[:,5] = (dist[:,5]-Pc)/Pc

# Transport the particles with transfer matrix
Disp3 = D5 @ D4 @ D3 @ D2 @ D1 @ Dip1
newBeam = Disp3 @ D0 @ beam.T

newBeam = newBeam.T
newBeam[:,5] *= Pc
newBeam[:,5] += Pc

# Fit the momentum to the horizontal position
func = lambda x, a, b: a+b*x
popt, pcov = curve_fit(func, newBeam[::97,0]*1e3, (PRef[::97]-Pc)/1e3)

fig, [ax1, ax2] = plt.subplots(ncols = 2, figsize = (6, 3))
ax1.plot(newBeam[::97,0]*1e3, (PRef[::97]-Pc)/1e3, '.')
xx = np.linspace(-8, 6)
ax1.plot(xx, func(xx, *popt), '-')


counts, edges = np.histogram((PRef-Pc)/1e3, bins = 50,
                             range = (-200, 150), density = False)
centers = edges[1:]-(edges[1]-edges[0])*0.5
ax2.plot(centers, counts/100, '-')


counts, edges = np.histogram((beam[:,5]*Pc)/1e3, bins = 50,
                             range = (-200, 150), density = False)
centers = edges[1:]-(edges[1]-edges[0])*0.5
ax2.plot(centers, counts/100, '-')


counts, edges = np.histogram((newBeam[:,0]/0.9057*Pc)/1e3, bins = 50,
                             range = (-200, 150), density = False)
centers = edges[1:]-(edges[1]-edges[0])*0.5
ax2.plot(centers, counts/100, '-')


#.hist(PRef/1e6, bins = 50, histtype = r'step')
#ax2.hist((1+newBeam[:,0]/0.9057)*Pc/1e6, bins = 50, histtype = r'step')

ax1.grid()
ax2.grid()

ax1.set_xlabel(r'$x$ (mm)')
ax1.set_ylabel(r'$\Delta P$ (keV/c)')
ax1.legend(['Beam at Disp3.Scr1', 'linear fit'], loc = 'upper left',
           fontsize = 10)

ax2.set_xlabel(r'$\Delta P$ (keV/c)')
ax2.set_ylabel(r'Counts')
ax2.legend(['$\Delta P$ before TDS', '$\Delta P$ before Disp3',
            'From $x$ at Disp3.Scr1'], loc = 'upper left', fontsize = 10)
ax2.set_ylim(0, 150)

fig.tight_layout()
fig.savefig('Momentum-reconstruction-HEDA2-002.png')

#%% sampling of distributions
tmp = np.loadtxt('ast.5277.001')

np.savetxt('ast.5277.101', tmp[::17], fmt = '%14.6E%14.6E%14.6E%14.6E%14.6E%14.6E%14.6E%14.6E%4d%4d')

#%% Reconstruct longitudinal profiles
data = pd_loadtxt('ast.1000.002')
data[0,2] = 0
data[1:,5] += data[0,5]

# Get reference z and P distributions
zRef = data[:,2]
PRef = data[:,5]

plt.figure()
plt.hist2d(zRef, PRef, bins = (100, 100))

fig, ax = plt.subplots()
#ax.hist(zRef*1e3, bins = 50, histtype = r'step', range = (-4, 4))
counts, edges = np.histogram(zRef*1e3, bins = 50,
                             range = (-4, 4))
centers = edges[1:]-(edges[1]-edges[0])*0.5
ax.plot(centers, counts, '-')

positions = [12.28, 17, 18.52]
ratios = [-0.27, -0.315, 0.44]
for i, pos in enumerate(positions):
    fname = 'ast.%4.0f.019' % (pos*100)
    data = pd_loadtxt(fname)

    data[0,2] = 0
    data[1:,5] += data[0,5]

    #% Convert from Astra format to the one suitable for transfer matrix
    beam = np.zeros((len(data),6))
    beam[:,0] = data[:,0]
    beam[:,2] = data[:,1]
    beam[:,4] = data[:,2]
    
    beam[:,1] = data[:,3]/data[:,5]
    beam[:,3] = data[:,4]/data[:,5]
    
    Pc = np.mean(data[:,5])
    beam[:,5] = (data[:,5]-Pc)/Pc

    # Transport the particles with 6D transfer matrix
    newBeam = beam
    newBeam[:,5] *= Pc
    newBeam[:,5] += Pc
    
    func = lambda x, a, b: a+b*x
    popt, pcov = curve_fit(func, newBeam[:,2]*1e3, zRef*1e3)

    
    counts, edges = np.histogram(func(newBeam[:,2]*1e3, *popt), bins = 50,
                                 range = (-4, 4))
    centers = edges[1:]-(edges[1]-edges[0])*0.5
    ax.plot(centers, counts, '-')
    
ax.grid()
ax.set_xlim(-3, 4)
ax.set_xlabel(r'$z$ (mm)')
ax.set_ylabel(r'Counts')
ax.legend(['$z$ before TDS', '$y$ at PST.Scr1',
           '$y$ before Disp3', '$y$ after Disp3'],
          loc = 'lower center')

fig.savefig('Reconstructed-profile-019.png')

#%% Fit the longitudinal coordinate to the vertical position
func = lambda x, a, b: a+b*x
popt, pcov = curve_fit(func, newBeam[:,2]*1e3, zRef*1e3)

fig, [ax1, ax2] = plt.subplots(ncols = 2, figsize = (6, 3))
ax1.plot(newBeam[:,2]*1e3, zRef*1e3, '.')
yy = np.linspace(-20, 20)
ax1.plot(yy, func(yy, *popt), '-')

#ax1.plot(data[:,2]*1e3, data[:,1]*1e3, '.')

ax2.hist(zRef*1e3, bins = 50, histtype = r'step', range = (-4, 4))
#ax2.hist(beam[:,4]*1e3, bins = 50, histtype = r'step')
#ax2.hist(func(newBeam[:,2]*1e3, *popt), bins = 50, histtype = r'step')

### For PST.Scr1 reconstruction: 008
#ax2.hist(-newBeam[:,2]*1e3*0.27, bins = 50, histtype = r'step', range = (-4, 4))
###

### For reconstruction at 17 m: 008
#ax2.hist(-newBeam[:,2]*1e3*0.315, bins = 50, histtype = r'step', range = (-4, 4))
###

### For Disp3.Scr1 reference position reconstruction: 008
ax2.hist(newBeam[:,2]*1e3*0.44, bins = 50, histtype = r'step', range = (-4, 4))
###

ax1.grid()
ax2.grid()

ax1.set_xlabel(r'$y$ (mm)')
ax1.set_ylabel(r'$z$ (mm)')
ax1.legend(['Beam', 'linear fit'])

ax2.set_xlabel(r'$z$ (mm)')
ax2.set_ylabel(r'Counts')
ax2.legend(['Simulated', 'Reconstructed'])

fig.tight_layout()
#fig.savefig('Momentum-reconstruction-HEDA2-2.png')


#%% slice emittance vs TDS voltage: 250 pC
grads = np.array([[0, 0.02, 0.03, 0.04, 0.05, 0.06]]).T
suffix = np.array([12, 17, 14, 20, 21, 15])
colors = ['r', 'b', 'g']
fig, ax = plt.subplots()

ax.plot([], [], 'o')
ax.plot([], [], 'o')
ax.plot([], [], 'o')

for i, islice in enumerate([2, 7, 12]):
    res = []
    for dd in suffix:
        fname = 'slice400um@ast.1228.%03d' % dd
        temp = np.loadtxt(fname, skiprows = 1)
        
        res.append(temp[islice])
        
    res = np.concatenate((grads, res), axis = 1)
    
    #res[1:,3] = np.sqrt(res[1:,3]**2-res[0,3]**2)
    ax.plot((res[:,0]*40/3)**2, (res[:,2])**2, 'o', color = colors[i])
    func = lambda x, a, b:a+b*x
    popt, pcov = curve_fit(func, (res[1:,0]*40/3)**2, (res[1:,2])**2)
    xtemp = np.linspace(0, 0.65)
    ax.plot(xtemp, func(xtemp, *popt), '--', color = colors[i])

ax.grid()
ax.set_xlabel(r'Voltage squared (MV$^2$)')
ax.set_ylabel(r'Slice M-spread squared (keV$^2$)')
#ax.set_ylim(0, 25)

ax.legend(['slice A', 'C', 'B'])
#fig.savefig('slice-momentum-spread-vs-TDS-voltage-012-zoomed.png')

#%% slice emittance vs TDS voltage: 10 pC
grads = np.array([[0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06]]).T

colors = ['r', 'b', 'g']
fig, ax = plt.subplots()

ax.plot([], [], 'bo')
ax.plot([], [], 'b--')
ax.plot([], [], 'o')

for i, islice in enumerate([7]):
    res = []
    for gr in grads:
        direc = 'Q7--1.50T_m-Q8-1.35T_m-%.2f-58.360000' % gr
        fname = direc+os.sep+'slice400um@ast.1228.%03d' % 5
        #direc = 'Q7-0.00T_m-Q8-0.00T_m-%.2f-60.440000' % gr
        #fname = direc+os.sep+'slice220um@ast.1228.%03d' % 4
        temp = np.loadtxt(fname, skiprows = 1)
        
        res.append(temp[islice])
        
    res = np.concatenate((grads, res), axis = 1)
    
    #res[1:,3] = np.sqrt(res[1:,3]**2-res[0,3]**2)
    ax.plot((res[:,0]*40/3)**2, (res[:,2])**2, 'bo', color = colors[1])
    func = lambda x, a, b:a+b*x
    popt, pcov = curve_fit(func, (res[1:,0]*40/3)**2, (res[1:,2])**2)
    xtemp = np.linspace(0, 0.65)
    ax.plot(xtemp, func(xtemp, *popt), 'b--', color = colors[1])

ax.grid()
ax.set_xlabel(r'Voltage squared (MV$^2$)')
ax.set_ylabel(r'Slice M-spread squared (keV$^2$)')
#ax.set_ylim(0, 25)

#ax.legend(['slice A', 'C', 'B'])
#ax.legend(['tail slice', 'center slice', 'head slice'])
#ax.set_xlim(-0.01, 0.03)
#ax.set_ylim(0, 30)
#ax.plot([-0.01, 0.03, 0.03, -0.01, -0.01], [0, 0, 30, 30, 0], 'k--')
ax.legend(['simulation', 'linear fit'])
fig.savefig('slice-momentum-spread-vs-TDS-voltage@50um6.png')

#%% slice emittance vs TDS voltage: 250 pC 2
grads = np.array([[0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8]]).T

colors = ['r', 'b', 'g']
fig, ax = plt.subplots()

ax.plot([], [], 'o')
ax.plot([], [], 'r-')
ax.plot([], [], 'o')

for i, islice in enumerate([7]):
    res = []
    for gr in grads:
        direc = 'Q7--1.50T_m-Q8-1.35T_m-%.2f-61.420000' % gr
        fname = direc+os.sep+'slice320um@ast.1228.%03d' % 2
        #direc = 'Q7-0.00T_m-Q8-0.00T_m-%.2f-60.440000' % gr
        #fname = direc+os.sep+'slice220um@ast.1228.%03d' % 4
        temp = np.loadtxt(fname, skiprows = 1)
        
        res.append(temp[islice])
        
    res = np.concatenate((grads, res), axis = 1)
    
    #res[1:,3] = np.sqrt(res[1:,3]**2-res[0,3]**2)
    ax.plot((res[:,0])**2, (res[:,2])**2, 'o', color = colors[i])

    func = lambda x, a, b:a+b*x
    popt, pcov = curve_fit(func, (res[1:,0])**2, (res[1:,2])**2)
    xtemp = np.linspace(0, 0.65)
    ax.plot(xtemp, func(xtemp, *popt), '--', color = colors[i])

ax.grid()
ax.set_xlabel(r'Voltage squared (MV$^2$)')
ax.set_ylabel(r'Slice M-spread squared (keV$^2$)')
ax.set_xlim(-0.01, 0.7)
ax.set_ylim(0, 120)

#ax.legend(['slice A', 'C', 'B'])
#ax.legend(['tail slice', 'center slice', 'head slice'])
#ax.set_xlim(-0.01, 0.03)
#ax.set_ylim(0, 30)
#ax.plot([-0.01, 0.03, 0.03, -0.01, -0.01], [0, 0, 30, 30, 0], 'k--')
ax.legend(['simulation', 'linear fit'])
fig.savefig('slice-momentum-spread-vs-TDS-voltage@50um.png')

#%%
fig, ax = plt.subplots(nrows = 1, figsize = (6, 4))

os.chdir(r'Q7--1.50T_m-Q8-1.35T_m-0.00-61.420000')

Run = 9
d1 = np.loadtxt('ast.Xemit.%03d' % Run)
d2 = np.loadtxt('ast.Yemit.%03d' % Run)

ax.plot(d1[:,0], d1[:,3], 'r-') # **2/(d1[:,5]/37.77)
ax.plot(d2[:,0], d2[:,3], 'b-') # **2/(d2[:,5]/37.77)
ax.plot([], [], 'r--')
ax.plot([], [], 'b--')

for i in [0.2, 0.4, 0.6]:
    
    os.chdir('../Q7--1.50T_m-Q8-1.35T_m-%.2f-61.420000' % i)
    
    d1 = np.loadtxt('ast.Xemit.%03d' % Run)
    d2 = np.loadtxt('ast.Yemit.%03d' % Run)

    ax.plot(d1[:,0], d1[:,3], 'r--')
    ax.plot(d2[:,0], d2[:,3], 'b--')

os.chdir(r'..')

ax.grid()
ax.set_xlabel(r'$z$ (m)')
ax.set_ylabel(r'RMS size (mm)')
ax.set_xlim(4, 20)
#ax.set_ylim(0, 2)
ax.legend([r'$x$ w/o TDS', r'$y$ w/o TDS'], loc = 'upper left', ncol = 1)

#fig.savefig('beam-transport-vs-TDS-voltage@50um.png')

#%%
Matrix2Array = lambda M, digits = 5: np.array(M.evalf(digits), dtype = 'double')
M2A = Matrix2Array

fname = 'ast.1228.002'
sigma0, P0 = SigmaMatrix6D(fname = fname)
P0 *= 1e3; print('P0 = ', P0, ' MeV/c')
gamma0 = kinetic2gamma(M2K(P0))

lq = 0.0675

### 002
l0, l1, l2, l3 = 1.674250, 1.072500, 1.439500, 0.33125
g1, g2, g3 = -0.660193, 0.806569*1.00, -0.688444*1.0
###

### 003
l0, l1, l2, l3 = 1.674250, 1.072500, 1.439500, 0.33125
g1, g2, g3 = -0.691631, 0.844292, -0.824541*1.0
###

# ### 004
# l0, l1, l2, l3 = 1.674250, 1.072500, 1.439500, 0.33125
# g1, g2, g3 = -0.628755, 0.768287*0.8, -0.598849*0.5
# ###

# ### 006
# l0, l1, l2, l3 = 0.534250, 1.072500, 1.072500, 3.358250 # 1.97825
# g1, g2, g3 = -0.597317, 0.815938, -0.667131*0.9
# ###

# ## opt1
# l0, l1, l2, l3 = 0.534250, 1.072500, 1.072500, 1.83825
# g1, g2, g3 = -0.565880,	0.781269,	-0.619552
# ###

### opt2
l0, l1, l2, l3 = 1.674250, 1.072500, 1.439500, 0.33125
g1, g2, g3 = -0.597317, 0.729495,-0.522658
###


k1, k2, k3 = G2K(g1,P0), G2K(g2, P0), G2K(g3, P0)

# PST.Scr1 to 17 m
D0 = Drift(l0)
Q1 = QuadD(k1, lq)
D1 = Drift(l1)
Q2 = QuadF(k2, lq)
D2 = Drift(l2)
Q3 = QuadD(k3, lq)
D3 = Drift(l3)
PST_list = [D0, Q1, D1, Q2, D2, Q3, D3]

#D3 = Drift(0)
#PST_list = [D3]

# Disp3
l01, l11 = 0.14, 0.6994

D01 = Drift(l01)
Dip1 = Wedge(rho, theta, gg)
Dip1 = [Wedge(rho, theta/6, gg) for _ in range(6)]
#Dip1 = Drift(rho*theta)
#Dip1 = [Drift(rho*theta/6) for _ in range(6)]

Disp3_list = [D01] + Dip1 + [Drift(l11/5) for _ in range(5)]

TR = np.ones(6)
PST_list = [TR*np.array(M.evalf(5), dtype = 'double') for M in PST_list]
Disp3_list = [TR*np.array(M.evalf(5), dtype = 'double') for M in Disp3_list]

#TRall = D3@Q3@D2@Q2@D1@Q1@D0

fig, [ax1, ax2, ax3, ax4]= plt.subplots(nrows = 4, figsize = (4.5, 5),
                                   sharex = True)
for i, v in enumerate([0]):
    os.chdir('../Q7--1.50T_m-Q8-1.35T_m-%.2f-61.420000' % v)
    
    fname = 'ast.1700.009'
    sigma0, P0 = SigmaMatrix6D(fname = fname)
    P0 *= 1e3; #print('P0 = ', P0, ' MeV/c')
    gamma0 = kinetic2gamma(M2K(P0))

    # dimension for the transport
    dim = 4
    sigma, transfer = Transport6D(sigma0, *PST_list, *Disp3_list, dim = dim)
    
    # positions of the ends of the elements, for plotting
    ss = np.array([0, l0, lq, l1, lq, l2, lq, l3]+[l01]+
                  [rho*theta/6 for _ in range(6)]+
                  [l11/5 for _ in range(5)])
    #ss = np.array([0, 0] +[l01] +[rho*theta/6 for _ in range(6)]+
    #              [l11/5 for _ in range(5)])
    
    for j in np.arange(1, len(ss)):
        ss[j] += ss[j-1]
    
    res = []
    for s in sigma:
        
        nemitx = np.sqrt(np.linalg.det(s[0:2,0:2]))*gamma0
        nemity = np.sqrt(np.linalg.det(s[2:4,2:4]))*gamma0
        betaMx = s[0:2, 0:2]/nemitx*gamma0
        betaMy = s[2:4, 2:4]/nemity*gamma0
         
        res.append([s[i,i]**0.5 for i in np.arange(dim)]+[nemitx, nemity]+
                   [betaMx[0,0], betaMy[0,0], -betaMx[0,1], -betaMy[0,1]]+
                   [betaMx[1,1], betaMy[1,1]])
    res = np.array(res); print(res[-1])
    
    #reset_margin(bottom=0.1, top=0.9)
    
    res[:,0] *= 10
    res[:,dim] *= 10
    for j in [0, 2]:
        ax1.plot(ss, res[:,j], color = color_cycle[i])
    
    for j in [dim, dim+1]:
        ax2.plot(ss, res[:,j], color = color_cycle[i])   
    
    ax3.plot(ss, res[:,6], color = color_cycle[i])
    ax3.plot(ss, res[:,7], color = color_cycle[i])
    
    ax4.plot(ss, res[:,8], color = color_cycle[i])
    #ax4.plot(ss, res[:,9], color = color_cycle[i])

#%%

#%% Summary of slice emittance vs TDS voltage with different setup
colors = ['r', 'b', 'g']
fig, ax = plt.subplots()

ax.plot([], [], 'o')
ax.plot([], [], 'o')
ax.plot([], [], 'o')

res = res250
ax.plot((res[:,0]*40/3), (res[:,2]), 'o', color = colors[0])
func = lambda x, a, b:a+b*x
popt, pcov = curve_fit(func, (res[1:,0]*40/3)**2, (res[1:,2])**2)
xtemp = np.linspace(0, 0.65)
#ax.plot(xtemp, func(xtemp, *popt), '--', color = colors[0])

res = res2
ax.plot((res[:,0]*40/3), (res[:,2]), 'o', color = colors[1])
func = lambda x, a, b:a+b*x
popt, pcov = curve_fit(func, (res[1:,0]*40/3)**2, (res[1:,2])**2)
xtemp = np.linspace(0, 0.65)
#ax.plot(xtemp, func(xtemp, *popt), '--', color = colors[1])

res = res10
ax.plot((res[:,0]*40/3), (res[:,2]), 'o', color = colors[2])
func = lambda x, a, b:a+b*x
popt, pcov = curve_fit(func, (res[1:,0]*40/3)**2, (res[1:,2])**2)
xtemp = np.linspace(0, 0.65)
#ax.plot(xtemp, func(xtemp, *popt), '--', color = colors[1])

ax.grid()
ax.set_xlabel(r'Voltage (MV)')
ax.set_ylabel(r'Slice M-spread (keV)')
#ax.set_ylim(0, 25)

ax.legend(['250 pC', 'Cut by EMSY1 slit', '10 pC'])
fig.savefig('slice-momentum-spread-vs-TDS-voltage@250pC_comp3.png')

#%% Slice emittance profiles with different TDS voltages
fnames = ['slice@ast.1852.019', 'slice@ast.1852.020', 'slice@ast.1852.021']

fig, ax = plt.subplots()

for fname in fnames:
    
    temp = np.loadtxt(fname, skiprows = 2)
    ax.plot(temp[:,0]*1e3, np.sqrt(temp[:,3]/temp[:,1]*temp[:,5])*1e3, '-*')

ax.grid()
ax.set_xlabel(r'$\xi$ (mm)')
ax.set_ylabel(r'$\sigma_x^s (mm)$')
#ax.set_ylim(0, 30)
ax.legend(['0.40 MV', '0.53 MV', '0.67 MV'])

fig.savefig('Slice-Xrms-vs-TDS-voltage.png')

#%% Slice emittance profiles with different positions
fnames = ['slice400um@ast.1000.012', 'slice400um@ast.1228.014', 'slice400um@ast.1700.019']

fig, ax = plt.subplots()

for i, fname in enumerate(fnames):
    
    temp = np.loadtxt(fname, skiprows = 0)
    ax.plot(temp[:,0]*1e3, temp[:,1], '-*')

for i, fname in enumerate(fnames):
    
    temp = np.loadtxt(fname, skiprows = 0)
    ax.plot(temp[:,0]*1e3, temp[:,3], '--', color = color_cycle[i])

ax.grid()
ax.set_xlabel(r'$\xi$ (mm)')
ax.set_ylabel(r'$\sigma_P^s (keV)$')
ax.set_ylim(0, 30)
ax.legend(['Before TDS', 'After TDS', 'Before Disp3.D1'])

fig.savefig('Slice-momentum-spread-019.png')

#%%
fnames = ['slice400um@ast.1000.002', 'slice400um@ast.1228.002', 'slice400um@ast.1700.019']

fig, ax = plt.subplots()

for i, fname in enumerate(fnames):
    
    temp = np.loadtxt(fname, skiprows = 0)
    ax.plot(temp[:,0]*1e3, temp[:,1], '-*')

for i, fname in enumerate(fnames):
    
    temp = np.loadtxt(fname, skiprows = 0)
    ax.plot(temp[:,0]*1e3, temp[:,3], '--', color = color_cycle[i])

ax.grid()
ax.set_xlabel(r'$\xi$ (mm)')
ax.set_ylabel(r'$\sigma_P^s (keV)$')
ax.set_ylim(0, 30)
ax.legend(['Before TDS', 'After TDS', 'Before Disp3.D1'])

fig.savefig('Slice-momentum-spread-019.png')

#%%
# Estimate the 2D histogram
data = pd_loadtxt('ast.1000.002')

nbins = 200
H, xedges, yedges = np.histogram2d(data[1:,2]*1e3, data[1:,5]/1e3,
                                   bins = nbins)
 
# H needs to be rotated and flipped
H = np.rot90(H)
H = np.flipud(H)
 
# Mask zeros
Hmasked = np.ma.masked_where(H==0,H) # Mask pixels with a value of zero
 
# Plot 2D histogram using pcolor
fig2 = plt.figure()
plt.pcolormesh(xedges, yedges, Hmasked)
plt.xlabel(r'$\xi$ (mm)')
plt.ylabel(r'$\Delta P$ (keV)')
cbar = plt.colorbar()
cbar.ax.set_ylabel('Counts')

#%% plot LPS
fname = 'ast.1700.019'
data = pd_loadtxt(fname)

x = data[1:,2]*1e3
y = data[1:,5]/1e3
w = -data[1:,7]*1e-9/g_qe
plot2d(x = x, y = y, bins = (200, 200),
       xlabel = r'$\xi$ (mm)', ylabel = r'$\Delta P_z$ (keV/$c$)',
       figname = 'zpz@'+fname, fig_ext = '.png', extent = [-3, 4, -200, 160])

#%%
data = pd_loadtxt('ast.1000.002')
data[0,2] = 0
data[1:,5] += data[0,5]

# Get reference z and P distributions
zRef = data[:,2]
PRef = data[:,5]


fname = 'ast.1700.019'
data = pd_loadtxt(fname)

data[0,2] = 0
data[1:,5] += data[0,5]

####
select = (data[:,0]>-25e-6)*(data[:,0]<25e-6)
data = data[select]; print(len(data))
zRef = zRef[select]
PRef = PRef[select]
####

#% Convert from Astra format to the format suitable for transfer matrix
beam = np.zeros((len(data),6))
beam[:,0] = data[:,0]
beam[:,2] = data[:,1]
beam[:,4] = data[:,2]

beam[:,1] = data[:,3]/data[:,5]
beam[:,3] = data[:,4]/data[:,5]

Pc = np.mean(data[:,5])
beam[:,5] = (data[:,5]-Pc)/Pc

# Transport the particles with 6D transfer matrix
Disp3 = D5 @ D4 @ D3 @ D2 @ D1 @ Dip1
newBeam = Disp3 @ D0 @ beam.T

newBeam = newBeam.T
newBeam[:,5] *= Pc
newBeam[:,5] += Pc

func = lambda x, a, b: a+b*x
popt, pcov = curve_fit(func, newBeam[:,2]*1e3, zRef*1e3)

x = func(newBeam[:,2]*1e3, *popt)
y = (newBeam[:,0]/0.9057*Pc)/1e3

plot2d(x = x, y = y, bins = (200, 200),
       xlabel = r'$\xi$ (mm)', ylabel = r'$\Delta P_z$ (keV/$c$)',
       figname = 'zpz3@'+fname, fig_ext = '.png', extent = [-3, 4, -200, 160])


#%% TDS phase scan
data = np.loadtxt('ParaScanTDSPhase.dat', skiprows = 1)

fig, ax = plt.subplots()
ax.plot(data[:,2], data[:,4])

func = lambda x, a, b: a+b*x
popt, pcov = curve_fit(func, data[:,2], data[:,3])

print('Zero-crossing phase: ', func(0, *popt))

#%% Calculate slice momentum spread
#direc = './Q7-0.00T_m-Q8-0.00T_m-%.2f-60.440000' % 0.02
#direc = '../Q7--1.50T_m-Q8-1.35T_m-%.2f-58.360000' % 0.02

#direc = '../Q7--1.50T_m-Q8-1.35T_m-%.2f-61.420000' % 0.8
#os.chdir(direc)

fname = 'ast.5277.001'
fname = 'ast.5277.001@50um'
data = pd_loadtxt(fname)
data[0, 2] = 0
data[0, 5] = 0

# Get reference z and P distributions
zRef = data[:,2]
PRef = data[:,5]

half_width = 4.0e-3/10/2

res = []
for slice in np.linspace(-7, 7, 15):
#for slice in np.linspace(-1760, 1760, 17)*1e-6:
    
    slice *= half_width*2
    
    select = (data[:,2]>slice-half_width)*(data[:,2]<slice+half_width)
    sigma_Pz_s = np.std(data[:,5][select])/1e3
    Qb = np.sum(select)*data[0,7]
    
    res.append([slice, sigma_Pz_s, Qb, -Qb*1e-9/half_width/2*g_c])
res = np.array(res)
np.savetxt('slice400um@'+fname, res, fmt = '%14.6E')

#fig, ax = plt.subplots(figsize = (4, 3))
ax.plot(res[:,0]*1e3, res[:,1])

ax.set_xlabel(r'$\xi$ (mm)')
ax.set_ylabel(r'$\sigma_P^s (keV)$')
ax.set_ylim(0, 10)
#ax.grid()
ax.legend(['w/o slit', 'w/slit'])

fig.savefig('Slice-momentum-spread@'+fname+'.png')

#%% 250 pC with EMSY1 slit
fname = 'ast.0528.001'
data = pd_loadtxt(fname)

# Get reference z and P distributions
zRef = data[:,2]
PRef = data[:,5]

select = (data[:,0]>-25e-6)*(data[:,0]<25e-6)
data = data[select]; print(len(data))

zRef = zRef[select]
PRef = PRef[select]

#astra_format = '%14.6E%14.6E%14.6E%14.6E%14.6E%14.6E%14.6E%14.6E%4d%4d'
#np.savetxt(fname+'@50um', data, fmt = astra_format)

x = zRef[1:]*1e3
y = PRef[1:]/1e3

select = (x>-2)*(x<2)
popt, _ = curve_fit(linear, x, y)

plot2d(x = x, y = y-linear(x, *popt), bins = (100, 100),
       xlabel = r'$\xi$ (mm)', ylabel = r'$\Delta P_z$ (keV/$c$)',
       figname = 'zpz_2nd_higher@'+fname, fig_ext = '.png',
       extent = [-3.5, 3.5, -60, 30])
#%%

#plt.hist2d(x, y-linear(x, *popt), bins = (200, 200),
#                  range = [[-3.5, 3.5], [-60, 30]], cmin = 0.001)

xmin, xmax = -3.5, 3.5
ymin, ymax = -60, 30

nbins = 200
H, xedges, yedges = np.histogram2d(x, y-linear(x, *popt), bins = nbins,
                                   range = [[xmin, xmax], [ymin, ymax]],
                                   density = True)

# H needs to be rotated and flipped
H = np.rot90(H)
H = np.flipud(H)

# Mask zeros
Hmasked = np.ma.masked_where(H==0,H) # Mask pixels with a value of zero
 
# Plot 2D histogram using pcolor
reset_margin(left = 0.15, right = 0.85)
fig, ax = plt.subplots(figsize = (4, 3.375))
cax = ax.pcolormesh(xedges, yedges, Hmasked)
ax.set_xlabel(r'$\xi$ (mm)')
ax.set_ylabel(r'$\Delta P_z$ (keV/$c$)', labelpad = -4)
ax.set_xlim(xmin, xmax)
ax.set_ylim(ymin, ymax)
#ax.set_aspect('equal')
cbar = plt.colorbar(cax, fraction = 0.046)
cbar.ax.set_ylabel('Counts', labelpad=-1)
fig.savefig('zpz50um-2nd-higher@'+fname+'.png')
    
#%%
fname = 'ast.1000.001'
data = pd_loadtxt(fname)

# Get reference z and P distributions
zRef = data[:,2]
PRef = data[:,5]

x = zRef[1:]*1e3
y = PRef[1:]/1e3

fname = 'ast.5277.001'
data = pd_loadtxt(fname)

# Get reference z and P distributions
zRef = data[:,2]
PRef = data[:,5]

x1 = zRef[1:]*1e3
y1 = PRef[1:]/1e3

plot2dx2(x, y, x1, y1, bins = (200, 200),
       xlabel = r'$\xi$ (mm)', ylabel = r'$\Delta P_z$ (keV/$c$)',
       figname = 'zpz9@'+fname, fig_ext = '.png', extent = [-3, 3, -150, 100])

#%% Cut beam properties vs slit width
fname = 'ast.0528.001'
data = pd_loadtxt(fname)

# Get reference z and P distributions
zRef = data[:,2]
PRef = data[:,5]

res = []
for slit in np.linspace(20, 200, 19):
    
    select = (data[1:,0]>-1e-6*slit/2)*(data[1:,0]<1e-6*slit/2)
    sigma_Pz = np.std(data[1:,5][select])/1e3
    Qb = np.sum(select)*data[0,7]
    
    select *= (data[1:,2]>-200e-6)*(data[1:,2]<200e-6)
    sigma_Pz_s = np.std(data[1:,5][select])/1e3
    
    d1 = data[1:][select]
    gg = momentum2gamma(data[0,5]/1e6)
    nemit_x_s = nemixrms(d1[:,0], d1[:,3]/(d1[:,5]+data[0,5]))*gg
    
    res.append([slit, sigma_Pz, sigma_Pz_s, nemit_x_s, Qb])
res = np.array(res)

np.savetxt('Cut-beam-vs-slit-width.dat', res, fmt = '%14.6E')

fig, ax = plt.subplots()
ax.plot(res[:,0], res[:,2])

ax.grid()
ax.set_xlabel(r'Slit width ($\mu$m)')
ax.set_ylabel(r'Centroid slice M-spread (keV/c)')
ax.legend(['Momentum spread'], loc = 'upper left')

ax2 = ax.twinx()
ax2.plot(res[:,0], -res[:,4]*1e3, 'b-D')
ax2.set_ylabel(r'Charge left (pC)', labelpad=-1)
ax2.set_ylim(0, 99)
ax2.legend(['Charge'], loc = 'lower right')

fig.savefig('slice-momentum-vs-slit-width.png')

fig, ax = plt.subplots()
ax.plot(res[:,0], res[:,3]*1e9)

ax.grid()
ax.set_xlabel(r'Slit width ($\mu$m)')
ax.set_ylabel(r'Centroid slice emittance (nm)')
ax.legend(['Norm. emittance'], loc = 'upper left')

ax2 = ax.twinx()
ax2.plot(res[:,0], -res[:,4]*1e3, 'b-D')
ax2.set_ylabel(r'Charge left (pC)', labelpad=-1)
ax2.set_ylim(0, 99)
ax2.legend(['Charge'], loc = 'lower right')

fig.savefig('slice-emittance-vs-slit-width.png')

#%%
fname = 'ast.0528.001'
data = pd_loadtxt(fname)

# Get reference z and P distributions
zRef = data[:,2]
PRef = data[:,5]

sliceWidths = np.linspace(5, 500, 100)

res = []
for width in sliceWidths:
    
    select = (data[1:,2]>-1e-6*width/2)*(data[1:,2]<1e-6*width/2)
    sigma_Pz = np.std(data[1:,5][select])/1e3
    Qb = np.sum(select)*data[0,7]
    Nb = np.sum(select)
    
    #select *= (data[1:,2]>-200e-6)*(data[1:,2]<200e-6)
    sigma_Pz_s = np.std(data[1:,5][select])/1e3
    
    d1 = data[1:][select]
    gg = momentum2gamma(data[0,5]/1e6)
    nemit_x_s = nemixrms(d1[:,0], d1[:,3]/(d1[:,5]+data[0,5]))*gg
    
    res.append([width, sigma_Pz_s, nemit_x_s, Qb, Nb])
res = np.array(res)
np.savetxt('slice-parameters-vs-slice-length@'+fname+'.dat',
           res, fmt = '%14.6E')
#%%
fig, ax = plt.subplots()
ax2 = ax.twinx()

# fname = 'ast.1000.001'
# res = np.loadtxt('slice-parameters-vs-slice-length@'+fname+'.dat')
# ax.plot(res[:,0], res[:,1], '-.')

# ax2.plot(res[:,0], -res[:,3]*1e3, '-.')

fname = 'ast.5277.001'
res = np.loadtxt('slice-parameters-vs-slice-length@'+fname+'.dat')
ax.plot(res[:,0], res[:,1], '-')

ax2.plot(res[:,0], -res[:,3]*1e3, 'b-')

# fname = 'ast.1228.012'
# res = np.loadtxt('slice-parameters-vs-slice-length@'+fname+'.dat')
# ax.plot(res[:,0], res[:,1], '--')

# ax2.plot(res[:,0], -res[:,3]*1e3, '--')

ax.grid()
ax.set_xlabel(r'Slice length ($\mu$m)')
ax.set_ylabel(r'Centroid slice M-spread (keV/c)')
ax.set_ylim(0, 6)
ax.legend(['M-spread at EMSY1', 'PST.Scr1'], loc = 'upper left')

ax2.set_ylabel(r'Charge (pC)', labelpad=1)
ax2.set_ylim(0, 99)
ax2.legend(['Charge at EMSY1', 'Charge at PST.Scr1'], loc = 'lower right')

fig.savefig('slice-momentum-vs-slice-length@'+fname+'.png')
#%%
fig, ax = plt.subplots()

data = np.loadtxt('slice@ast.5277.001', skiprows = 2)
ax.plot(data[:,0]*1e3, data[:,2]*0.511e3)

data = np.loadtxt('slice@ast.5277.001@50um', skiprows = 2)
ax.plot(data[:,0]*1e3, data[:,2]*0.511e3)

#%%
dirs = ['Q1-0.30T_m-Q2--0.55T_m-Q3-0.30T_m',
        'Q1-0.38T_m-Q2--0.69T_m-Q3-0.40T_m',
        'Q1-0.45T_m-Q2--0.82T_m-Q3-0.53T_m',
        'Q1-0.53T_m-Q2--0.95T_m-Q3-0.70T_m',
        'Q1-0.60T_m-Q2--1.09T_m-Q3-0.95T_m']
fname = 'ast.2361.007'

for direc in dirs[0:1]:
    os.chdir(direc)
    
    data = pd_loadtxt(fname)
    
    x = data[:,0]*1e3
    y = data[:,1]*1e3
    # Estimate the 2D histogram
    nbins = 100
    H, xedges, yedges = np.histogram2d(x,y,bins=nbins)
     
    # H needs to be rotated and flipped
    H = np.rot90(H)
    H = np.flipud(H)
     
    # Mask zeros
    Hmasked = np.ma.masked_where(H==0,H) # Mask pixels with a value of zero
     
    # Plot 2D histogram using pcolor
    fig, ax = plt.subplots()
    cax = ax.pcolormesh(xedges, yedges, Hmasked)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_xlim(-8, 8)
    ax.set_ylim(-8, 8)
    ax.set_aspect('equal')
    cbar = plt.colorbar(cax)
    cbar.ax.set_ylabel('Counts')
    
    os.chdir('..')
    
#%%
dirs = ['Q1-0.30T_m-Q2--0.55T_m-Q3-0.30T_m',
        'Q1-0.38T_m-Q2--0.69T_m-Q3-0.40T_m',
        'Q1-0.45T_m-Q2--0.82T_m-Q3-0.53T_m',
        'Q1-0.53T_m-Q2--0.95T_m-Q3-0.70T_m',
        'Q1-0.60T_m-Q2--1.09T_m-Q3-0.95T_m']
fname = 'ast.2361.007'
fname2 = 'ast.2550.007'

for direc in dirs[::2]:
    os.chdir(direc)
    
    data = pd_loadtxt(fname)
    z0 = data[0,2]
    data[0,2] = 0
    data[1:,5] += data[0,5]
    
    
    xp0 = data[:,3]/data[:,5]*1e3
    yp0 = data[:,4]/data[:,5]*1e3
    
    x0 = data[:,0]*1e3-data[:,2]*xp0
    y0 = data[:,1]*1e3-data[:,2]*yp0
    
    L = 1.0 # mm
    #select = 
    
    r0 = np.sqrt(x0**2+y0**2)
    
    rcut = np.sqrt(3)*np.std(x0); print(rcut)
    

    xmax = rcut*2
    xpmax = 2
    
    select = (r0<rcut); print(np.sum(select)/len(data))
    select2 = (r0>rcut)
    
    data[0,2] = z0
    data[1:,5] -= data[0,5]
    
    np.savetxt('collimated@'+fname, data[select],
               fmt = '%14.6E%14.6E%14.6E%14.6E%14.6E%14.6E%14.6E%14.6E%4d%4d')
    os.chdir('..')
    continue

    x = x0
    y = y0
       
    # Estimate the 2D histogram
    nbins = 100
    H, xedges, yedges = np.histogram2d(x,y,bins=nbins,
                                       range = [[-xmax, xmax], [-xmax, xmax]])
     
    # H needs to be rotated and flipped
    H = np.rot90(H)
    H = np.flipud(H)
     
    # Mask zeros
    Hmasked = np.ma.masked_where(H==0,H) # Mask pixels with a value of zero
    
    reset_margin(left = 0.15, right = 0.85)
    # Plot 2D histogram using pcolor
    fig, [ax1, ax2] = plt.subplots(ncols = 2, figsize = (6, 2.4), sharey = True)
    ax = ax1
    cax = ax.pcolormesh(xedges, yedges, Hmasked)
    ax.set_xlabel(r'$x$ (mm)')
    ax.set_ylabel(r'$y$ (mm)')
    ax.set_xlim(-xmax, xmax)
    ax.set_ylim(-xmax, xmax)
    ax.set_aspect('equal')
    #cbar = plt.colorbar(cax, ax = ax, fraction = 0.046)
    #cbar.ax.set_ylabel('Counts', labelpad=-1)
    #fig.savefig('xy@'+fname+'.png')
    
    x = x0[select]
    y = y0[select]
    xp = xp0[select]
       
    # Estimate the 2D histogram
    nbins = 100
    H, xedges, yedges = np.histogram2d(x,y,bins=nbins,
                                       range = [[-xmax, xmax], [-xmax, xmax]])
     
    # H needs to be rotated and flipped
    H = np.rot90(H)
    H = np.flipud(H)
     
    # Mask zeros
    Hmasked = np.ma.masked_where(H==0,H) # Mask pixels with a value of zero
    
    # Plot 2D histogram using pcolor
    #fig, ax = plt.subplots(figsize = (3, 2.4))
    ax = ax2
    cax = ax.pcolormesh(xedges, yedges, Hmasked)
    ax.set_xlabel(r'$x$ (mm)')
    #ax.set_ylabel(r'$y$ (mm)')
    ax.set_xlim(-xmax, xmax)
    ax.set_ylim(-xmax, xmax)
    ax.set_aspect('equal')
    cbar = plt.colorbar(cax, ax = [ax1, ax2], fraction = 0.046)
    cbar.ax.set_ylabel('Counts', labelpad=-1)
    fig.savefig('xy_collimated2@'+fname+'.png')
    
    #%
    x = x0
    xp = xp0
    
    # Estimate the 2D histogram
    nbins = 100
    H, xedges, yedges = np.histogram2d(x, xp, bins=nbins,
                                       range = [[-xmax, xmax], [-xpmax, xpmax]])
     
    # H needs to be rotated and flipped
    H = np.rot90(H)
    H = np.flipud(H)
    
    # Mask zeros
    Hmasked = np.ma.masked_where(H==0,H) # Mask pixels with a value of zero
    
    # Plot 2D histogram using pcolor
    fig, [ax1, ax2] = plt.subplots(ncols = 2, figsize = (6, 2.4), sharey=True)
    ax = ax1
    cax = ax.pcolormesh(xedges, yedges, Hmasked)
    ax.set_xlabel(r'$x$ (mm)')
    ax.set_ylabel(r'$x^{\prime}$ (mrad)')
    ax.set_xlim(-xmax, xmax)
    ax.set_ylim(-xpmax, xpmax)
    ax.set_aspect(xmax/xpmax)
    #cbar = plt.colorbar(cax, ax = ax, fraction = 0.046)
    #cbar.ax.set_ylabel('Counts', labelpad=-1)
    #fig.savefig('xxp@'+fname+'.png')
    
    
    x = x0[select]
    xp = xp0[select]
    
    # Estimate the 2D histogram
    nbins = 100
    H, xedges, yedges = np.histogram2d(x, xp, bins=nbins,
                                       range = [[-xmax, xmax], [-xpmax, xpmax]])
     
    # H needs to be rotated and flipped
    H = np.rot90(H)
    H = np.flipud(H)
    
    # Mask zeros
    Hmasked = np.ma.masked_where(H==0,H) # Mask pixels with a value of zero
    
    # Plot 2D histogram using pcolor
    #fig, ax = plt.subplots(figsize = (3, 2.4))
    ax = ax2
    cax = ax.pcolormesh(xedges, yedges, Hmasked)
    ax.set_xlabel(r'$x$ (mm)')
    #ax.set_ylabel(r'$x^{\prime}$ (mrad)')
    ax.set_xlim(-xmax, xmax)
    ax.set_ylim(-xpmax, xpmax)
    ax.set_aspect(xmax/xpmax)
    cbar = plt.colorbar(cax, ax = [ax1, ax2], fraction = 0.046)
    cbar.ax.set_ylabel('Counts', labelpad=-1)
    fig.savefig('xxp_collimated2@'+fname+'.png')
    
    #%
    data = pd_loadtxt(fname2)
    data[1:,5] += data[0,5]
    
    x1 = data[:,0]*1e3
    y1 = data[:,1]*1e3
    xp1 = data[:,3]/data[:,5]*1e3
    #r0 = np.sqrt(x0**2+y0**2)
    #select = (r0<4)
    
    x = x1[select]
    y = y1[select]
    # Estimate the 2D histogram
    nbins = 100
    H, xedges, yedges = np.histogram2d(x,y,bins=nbins,
                                       range = [[-xmax, xmax], [-xmax, xmax]])
     
    # H needs to be rotated and flipped
    H = np.rot90(H)
    H = np.flipud(H)
     
    # Mask zeros
    Hmasked = np.ma.masked_where(H==0,H) # Mask pixels with a value of zero
     
    # Plot 2D histogram using pcolor
    fig, ax1 = plt.subplots(ncols = 1, figsize = (3, 2.4), sharey=False)
    ax = ax1
    cax = ax.pcolormesh(xedges, yedges, Hmasked)
    ax.set_xlabel(r'$x$ (mm)')
    ax.set_ylabel(r'$y$ (mm)')
    ax.set_xlim(-xmax, xmax)
    ax.set_ylim(-xmax, xmax)
    ax.set_aspect('equal')
    cbar = plt.colorbar(cax, ax = ax, fraction = 0.046)
    cbar.ax.set_ylabel('Counts', labelpad=-1)
    fig.savefig('xy_collimated@'+fname2+'.png')
    
    x = x1[select]
    xp = xp1[select]
    
    # Estimate the 2D histogram
    nbins = 100
    H, xedges, yedges = np.histogram2d(x, xp, bins=nbins,
                                       range = [[-xmax, xmax], [-xpmax, xpmax]])
     
    # H needs to be rotated and flipped
    H = np.rot90(H)
    H = np.flipud(H)
    
    # Mask zeros
    Hmasked = np.ma.masked_where(H==0,H) # Mask pixels with a value of zero
    
    # Plot 2D histogram using pcolor
    reset_margin(left = 0.11, right = 0.81)
    fig, ax2 = plt.subplots(figsize = (3., 2.4))
    ax = ax2
    cax = ax.pcolormesh(xedges, yedges, Hmasked)
    ax.set_xlabel(r'$x$ (mm)')
    ax.set_ylabel(r'$x^{\prime}$ (mrad)')
    ax.set_xlim(-xmax, xmax)
    ax.set_ylim(-xpmax, xpmax)
    ax.set_aspect(xmax/xpmax)
    cbar = plt.colorbar(cax, ax = ax, fraction = 0.046)
    cbar.ax.set_ylabel('Counts', labelpad=-1)
    #fig.tight_layout()
    fig.savefig('xxp_collimated@'+fname2+'.png')
    
    x = x1[select2]
    y = y1[select2]
    # Estimate the 2D histogram
    nbins = 100
    H, xedges, yedges = np.histogram2d(x,y,bins=nbins,
                                       range = [[-xmax, xmax], [-xmax, xmax]])
     
    # H needs to be rotated and flipped
    H = np.rot90(H)
    H = np.flipud(H)
     
    # Mask zeros
    Hmasked = np.ma.masked_where(H==0,H) # Mask pixels with a value of zero
     
    # Plot 2D histogram using pcolor
    reset_margin(left = 0.15, right = 0.85)
    fig, ax = plt.subplots(figsize = (3., 2.4))
    cax = ax.pcolormesh(xedges, yedges, Hmasked)
    ax.set_xlabel(r'$x$ (mm)')
    ax.set_ylabel(r'$y$ (mm)')
    ax.set_xlim(-xmax, xmax)
    ax.set_ylim(-xmax, xmax)
    ax.set_aspect('equal')
    cbar = plt.colorbar(cax, fraction = 0.046)
    cbar.ax.set_ylabel('Counts', labelpad=-1)
    fig.savefig('xy_collimated2@'+fname2+'.png')
    
    
    os.chdir('..')
    
#%%
dirs = ['Q1-0.30T_m-Q2--0.55T_m-Q3-0.30T_m',
        'Q1-0.38T_m-Q2--0.69T_m-Q3-0.40T_m',
        'Q1-0.45T_m-Q2--0.82T_m-Q3-0.53T_m',
        'Q1-0.53T_m-Q2--0.95T_m-Q3-0.70T_m',
        'Q1-0.60T_m-Q2--1.09T_m-Q3-0.95T_m',
        'Q1-0.68T_m-Q2--1.22T_m-Q3-1.32T_m']
fname = 'ast.Xemit.007'
fname2 = 'ast.Yemit.007'

fig, ax = plt.subplots(figsize = (6 ,3))
for i, direc in enumerate(dirs[:]):
    os.chdir(direc)
    
    data = np.loadtxt(fname)
    
    ax.plot(data[:,0], data[:,3], '-', color = color_cycle[i])
    
    os.chdir('..')
    
ax.grid()
ax.set_xlim(18, 26)
ax.set_ylim(0, 3)
ax.set_xlabel(r'$z$ (m)')
ax.set_ylabel(r'$\sigma_x$ (mm')
fig.savefig('rms_x-vs-z-focusing.png')

#%%
fname2 = 'ast.2800.013'
data = pd_loadtxt(fname2)
data[1:,5] += data[0,5]

xc = np.mean(data[:,0])
x1 = data[:,0]*1e3-xc*1e3
y1 = data[:,1]*1e3
xp1 = data[:,3]/data[:,5]*1e3
#r0 = np.sqrt(x0**2+y0**2)
#select = (r0<4)

xmax = 10

x = x1
y = y1
# Estimate the 2D histogram
nbins = 100
H, xedges, yedges = np.histogram2d(x,y,bins=nbins,
                                   range = [[-xmax, xmax], [-xmax, xmax]])
 
# H needs to be rotated and flipped
H = np.rot90(H)
H = np.flipud(H)
 
# Mask zeros
Hmasked = np.ma.masked_where(H==0,H) # Mask pixels with a value of zero
 
# Plot 2D histogram using pcolor
reset_margin(left = 0.15, right = 0.85)
fig, ax1 = plt.subplots(ncols = 1, figsize = (3, 2.4), sharey=False)
ax = ax1
cax = ax.pcolormesh(xedges, yedges, Hmasked)
ax.set_xlabel(r'$x$ (mm)')
ax.set_ylabel(r'$y$ (mm)')
ax.set_xlim(-xmax, xmax)
ax.set_ylim(-xmax, xmax)
ax.set_aspect('equal')
cbar = plt.colorbar(cax, ax = ax, fraction = 0.046)
cbar.ax.set_ylabel('Counts', labelpad=-1)
fig.savefig('xy_collimated@'+fname2+'.png')


select = (y1>-50e-3)*(y1<50e-3)

x = x1[select]


counts, edges = np.histogram(x, 50)

edges = edges[1:]-(edges[1]-edges[0])*0.5

fig, ax = plt.subplots(figsize = (3, 3))
ax.plot(edges, counts, '-')
import numpy as np
import os

import sys
print(sys.argv)
if len(sys.argv)>1:
    os.chdir(sys.argv[1])

for root, dirs, files in os.walk("."):  
    for direc in dirs:
        try:
            for f in ['beam.ini', 'ast.0098.001', 'ast.0099.001', 'ast.0100.001']:
                os.remove(direc+os.sep+f)
        except Exception as err:
            #os.system('rm -r '+direc)
            pass
            # print 'File not exists.'
sys.exit()

start_line = 0
data = np.loadtxt('results.sorted.dat', skiprows = start_line)

for i, line in enumerate(data):
    #print line
    direc = str.format('n%.0fk-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' % tuple(line[0:-1]))
    try:
        if line[-1]>0:
            #cmd = 'rm -R '+direc
            cmd = 'rm '+direc+'/beam.ini'
            os.system(cmd)
            cmd = 'rm '+direc+'/ast.0530.001'
            os.system(cmd)
            cmd = 'rm '+direc+'/ast.ref.001'
            os.system(cmd)
            
    except IOError:
        print(fname, ' not found')

# -*- coding: utf-8 -*-
"""
Created on Fri May 22 15:17:22 2020

@author: lixiangk
"""

from astra_modules import *

def plot_config():
    from cycler import cycler
    from matplotlib.ticker import AutoMinorLocator

    fsize = 14 # a quarter of the paper width: 20 pt; half of the paper width: 12
    font = {'size' : fsize, 'family' : 'serif'}
    color_cycle = ['r', 'b', 'g', 'c', 'm', 'y', 'k']
    linestyle_cycle = ['-', '--', '-.', ':', (0, (5, 2, 5, 2)), (0, (10, 2, 5, 2, 2, 2)), (0, (12, 2, 2, 2))]
    marker_cycle = ['o', 'd', 'v', '^', '<', '>', '*']
    
    mpl.rc('font', **font)
    mpl.rc('xtick', labelsize = 10, direction = 'in', top   = True)
    mpl.rc('ytick', labelsize = 10, direction = 'in', right = True)
    mpl.rc('xtick.major', size = 5, width = 1)
    mpl.rc('ytick.major', size = 5, width = 1)
    mpl.rc('xtick.minor', size = 3, width = 0.7, visible = True)
    mpl.rc('ytick.minor', size = 3, width = 0.7, visible = True)
    
    mpl.rc('lines', linewidth=2, markersize=6, color='r')
    # mpl.rc('lines', linestyle = 'solid')
    mpl.rc('axes', labelpad = 0, prop_cycle=(cycler('color', color_cycle) + cycler('linestyle', linestyle_cycle) + cycler('marker', marker_cycle)))
    mpl.rc('legend', fontsize = 12, labelspacing = 0.05, handletextpad=0.4, frameon=False, handlelength=2.1)
    
    mpl.rc('figure', dpi = 100, figsize = (4, 4))
    mpl.rc('figure.subplot', bottom = 0.15, top = 0.9, left = 0.15, right = 0.9)
    
    mpl.rc('image', cmap = 'jet')
    
    return
plot_config()

from tkinter import *
from tkinter import ttk
from tkinter.messagebox import *
from tkinter.filedialog import askopenfilename, askopenfilenames


# Select all the text in the current widget
def select_all(event):
    w = event.widget
    if isinstance(w, Text):
        w.tag_add(SEL, "1.0", END)
        w.mark_set(INSERT, "1.0")
        w.see(INSERT)
    elif isinstance(w, Entry):
        w.select_range(0, END)
    return 'break'

def browse_callback(ent):
    filename = askopenfilename()
    ent.insert(0, filename)
    global diag
    diag = BeamDiagnostics(fname = filename, plot = True, energy = True)
    diag.demo(filename)

root = Tk(screenName = 'PostPro')
root.bind_class("Entry", "<Control-a>", func = select_all)
root.bind_class("Text", "<Control-a>", func = select_all)
root.geometry("500x400")

tab_parent = ttk.Notebook(root)

tab1 = ttk.Frame(tab_parent)
tab2 = ttk.Frame(tab_parent)
tab3 = ttk.Frame(tab_parent)
tab4 = ttk.Frame(tab_parent)

tab_parent.add(tab1, text = "PhaseSpace")
tab_parent.add(tab2, text = "Evolution")
tab_parent.add(tab3, text = "Slice")
tab_parent.add(tab4, text = "SpaceCharge")

tab_parent.pack(expand = 1, fill = "both")

# Variables
diag = None;
extent = None;

# Choose the file
row0 = Frame(tab1)
lab0 = Label(row0, text = "Distribution", width = 10, anchor = 'w')
ent0 = Entry(row0, width = 50)
but0 = Button(row0, text = 'Browse', width = 10, anchor = 'w')

row0.pack(side = TOP, fill = X, padx = 20, pady = 5)
lab0.pack(side = LEFT)
ent0.pack(side = RIGHT, expand = YES, fill = X, padx = 5)
but0.pack(side = RIGHT, fill = X, padx = 0)
but0.config(command = (lambda e0 = ent0: browse_callback(e0)))

# xmin
row1 = Frame(tab1)
lab1 = Label(row1, text = "xmin", width = 10, anchor = 'w')
xmin = Entry(row1, width = 10)

row1.pack(side = TOP, fill = X, padx = 20, pady = 5)
lab1.pack(side = LEFT)
xmin.pack(side = LEFT, expand = NO, fill = X)

# xmax
row2 = Frame(tab1)
lab2 = Label(row2, text = "xmax", width = 10, anchor = 'w')
xmax = Entry(row2, width = 10)
row2.pack(side = TOP, fill = X, padx = 20, pady = 5)
lab2.pack(side = LEFT)
xmax.pack(side = LEFT, expand = NO, fill = X)

# ymin
row3 = Frame(tab1)
lab3 = Label(row3, text = "ymin", width = 10, anchor = 'w')
ymin = Entry(row3, width = 10)
row3.pack(side = TOP, fill = X, padx = 20, pady = 5)
lab3.pack(side = LEFT)
ymin.pack(side = LEFT, expand = NO, fill = X)

# ymax
row4 = Frame(tab1)
lab4 = Label(row4, text = "ymax", width = 10, anchor = 'w')
ymax = Entry(row4, width = 10)
row4.pack(side = TOP, fill = X, padx = 20, pady = 5)
lab4.pack(side = LEFT)
ymax.pack(side = LEFT, expand = NO, fill = X)

# plot
def plot_callback(e1, e2, e3, e4, x, y):
    global diag
    
    try:
        extent = [float(e1.get()), float(e2.get()), float(e3.get()), float(e4.get())]
    except Exception as err:
        extent = None
        print(err)
    
    fig_ext = '.eps'
    if x is 'x' and y is 'y':
        plot2d_xy( dist = diag.dist, fig_ext = fig_ext, vmin = 0.01, bins = (200, 200), extent = extent)
    if x is 'x' and y is 'px':
        plot2d_xpx( dist = diag.dist, fig_ext = fig_ext, vmin = 0.01, bins = (200, 200), extent = extent)
    if x is 'y' and y is 'py':
        plot2d_ypy( dist = diag.dist, fig_ext = fig_ext, vmin = 0.01, bins = (200, 200), extent = extent)
    if x is 'z' and y is 'pz':
        plot2d_zpz( dist = diag.dist, fig_ext = fig_ext, vmin = 0.01, bins = (200, 200), extent = extent)
    
        
b1 = Button(root, text='Plot xy', width = 10,\
            command = (lambda e1 = xmin, e2 = xmax, e3 = ymin, e4 = ymax: plot_callback(e1, e2, e3, e4, 'x', 'y')))
b1.pack(side = LEFT, padx = 20, pady = 10)

b2 = Button(root, text='Plot xpx', width = 10,\
            command = (lambda e1 = xmin, e2 = xmax, e3 = ymin, e4 = ymax: plot_callback(e1, e2, e3, e4, 'x', 'px')))
b2.pack(side = LEFT, padx = 20, pady = 10)

b3 = Button(root, text='Plot ypy', width = 10,\
            command = (lambda e1 = xmin, e2 = xmax, e3 = ymin, e4 = ymax: plot_callback(e1, e2, e3, e4, 'y', 'py')))
b3.pack(side = LEFT, padx = 20, pady = 10)

b4 = Button(root, text='Plot zpz', width = 10,\
            command = (lambda e1 = xmin, e2 = xmax, e3 = ymin, e4 = ymax: plot_callback(e1, e2, e3, e4, 'z', 'pz')))
b4.pack(side = LEFT, padx = 20, pady = 10)

root.mainloop()

#%%

### Some pretext
row0 = Frame(root)
lab0 = Label(row0, text = 'Fill the following items to add a new ENTRY!')

row0.pack(side = TOP, fill = X, padx = 5, pady = 5)
lab0.pack(side = TOP)

### Title of your entry
row1 = Frame(root)
lab1 = Label(row1, text = "Title", width = 10, anchor = 'w')
ent1 = Entry(row1, width = 50)

row1.pack(side = TOP, fill = X, padx = 20, pady = 5)
lab1.pack(side = LEFT)
ent1.pack(side = RIGHT, expand = YES, fill = X)


root.bind('<Return>', (lambda event, x=0: x))

root.mainloop()
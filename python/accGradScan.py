'''
The purpose of this script is to find out the gradients of the gun and the booster, given the momentum gain after the gun and its phase and the momentum after the booster and its phase.

Parameters: 
  1. momentum at gun exit
  2. gun phase
  3. momentum at booster exit
  4. booster phase

Output:
  1. gun gradient
  2. booster gradient
'''

import sys
sys.path.append('/afs/ifh.de/group/pitz/data/lixiangk/work/sync/python')

from my_object import *

import matplotlib as mpl
import matplotlib.pyplot as plt
plt.switch_backend('agg')
             
def plot_config():
    from cycler import cycler
    from matplotlib.ticker import AutoMinorLocator

    fsize = 14 # a quarter of the paper width: 20 pt; half of the paper width: 12
    font = {'size' : fsize, 'family' : 'serif'}
    color_cycle = ['r', 'b', 'g', 'c', 'm', 'y', 'k']
    linestyle_cycle = ['-', '--', '-.', ':', (0, (5, 2, 5, 2)), (0, (10, 2, 5, 2, 2, 2)), (0, (12, 2, 2, 2))]
    marker_cycle = ['o', 'd', 'v', '^', '<', '>', '*']
    
    mpl.rc('font', **font)
    mpl.rc('xtick', labelsize = 10, direction = 'in', top   = True)
    mpl.rc('ytick', labelsize = 10, direction = 'in', right = True)
    mpl.rc('xtick.major', size = 5, width = 1)
    mpl.rc('ytick.major', size = 5, width = 1)
    mpl.rc('xtick.minor', size = 3, width = 0.7, visible = True)
    mpl.rc('ytick.minor', size = 3, width = 0.7, visible = True)
    
    mpl.rc('lines', linewidth=2, markersize=6, color='r')
    # mpl.rc('lines', linestyle = 'solid')
    mpl.rc('axes', labelpad = 0, prop_cycle=(cycler('color', color_cycle) + cycler('linestyle', linestyle_cycle) + cycler('marker', marker_cycle)))
    mpl.rc('legend', fontsize = 12, labelspacing = 0.05, handletextpad=0.4, frameon=False, handlelength=2.1)
    
    mpl.rc('figure', dpi = 100, figsize = (4, 4))
    mpl.rc('figure.subplot', bottom = 0.15, top = 0.9, left = 0.15, right = 0.9)
    
    mpl.rc('image', cmap = 'jet')
    
    return
plot_config()

def obj_gunGradScan(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    Ipart = 1000
    sigma_x = sigma_y = 1.0/4.
    phi_gun, phi_booster = x[1], 0
    Imain = 0
    
    MaxE_gun = x[0]
    MaxE_booster = 0
    MaxB = I2B(Imain)

    direc = str.format('n%.0fk-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Ipart/1000., sigma_x*4, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    
    direc = 'gunGradScan'
    os.system('mkdir -p '+direc)    

    field_maps = rootdir+os.sep+'sync'+os.sep+'field-maps'
    # cwd = os.getcwd()
    # field_maps = '../'+os.path.relpath(field_maps, cwd)+'/'

    gen_astra(direc+os.sep+'gen.in', direc+os.sep+'ast.in', field_maps = field_maps,\
              Ipart = Ipart, sig_x = sigma_x, sig_y = sigma_y, LSPCH = True,\
              MaxE = [MaxE_gun, MaxE_booster], Phi = [phi_gun, phi_booster],\
              MaxB = [MaxB], Lquad = False, zstop = 0.5, zemit = 200,\
              Screen = [0.5], Track_all = False)
    
    os.chdir(direc)
    os.system('generator gen.in > gen.log')
    os.system('astra ast.in > ast.log') 
    
    try:
        obj = get_ref_momentum()   
        
    except:
        # if no output beam file, then set the objective to 10000
        obj = 1e4
        print('Error: not accelerated well!')

    os.chdir('..'); #os.system('rm -r '+direc)
	
    res = [Ipart, sigma_x*4, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, obj]
    with open('gunGradScan.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_boosterGradScan(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    Ipart = 1000
    sigma_x = sigma_y = 1.0/4.
    phi_gun, phi_booster = x[1], x[3]
    Imain = 0
    
    MaxE_gun = x[0]
    MaxE_booster = x[2]
    MaxB = I2B(Imain)

    direc = str.format('n%.0fk-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Ipart/1000., sigma_x*4, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    
    direc = 'boosterGradScan'
    os.system('mkdir -p '+direc)    

    field_maps = rootdir+os.sep+'sync'+os.sep+'field-maps'
    # cwd = os.getcwd()
    # field_maps = '../'+os.path.relpath(field_maps, cwd)+'/'

    gen_astra(direc+os.sep+'gen.in', direc+os.sep+'ast.in', field_maps = field_maps,\
              Ipart = Ipart, sig_x = sigma_x, sig_y = sigma_y, LSPCH = True,\
              MaxE = [MaxE_gun, MaxE_booster], Phi = [phi_gun, phi_booster],\
              MaxB = [MaxB], Lquad = False, zstop = 5, zemit = 200,\
              Screen = [0.5], Track_all = False)
    
    os.chdir(direc)
    os.system('generator gen.in > gen.log')
    os.system('astra ast.in > ast.log') 
    
    try:
        obj = get_ref_momentum()   
        
    except:
        # if no output beam file, then set the objective to 10000
        obj = 1e4
        print('Error: not accelerated well!')

    os.chdir('..'); #os.system('rm -r '+direc)

    res = [Ipart, sigma_x*4, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, obj]
    with open('boosterGradScan.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

# Define the phases and objective momentum
MG_gun = 6.6 # MeV/c
phi_gun = 0
MG_boo = 17.05 # MeV/c
phi_boo = -20

argv = sys.argv
print(argv)

if len(argv) > 1:
    MG_gun = float(argv[1])
if len(argv) > 2:
    phi_gun = float(argv[2])
if len(argv) > 3:
    MG_boo = float(argv[3])
if len(argv) > 4:
    phi_boo = float(argv[4])


#remove_files(['gunGradScan.dat', 'boosterGradScan.dat'])
    
# Gun gradient scan
var0 = np.linspace(25, 65, 81); print(var0)
var1 = np.array([phi_gun])
combi = np.array([[v0, v1] for v0 in var0 for v1 in var1])

for i in combi:
    res = obj_gunGradScan(i)

gunGradScan = np.loadtxt('gunGradScan.dat')
MMMG_Egun = interp1d(gunGradScan[:,2], gunGradScan[:,7])
Egun_MMMG = interp1d(gunGradScan[:,7], gunGradScan[:,2])

obj_Egun = Egun_MMMG(MG_gun)

Egun = var0
MMMG = MMMG_Egun(Egun)

fig, ax = plt.subplots()
ax.plot(gunGradScan[:,2], gunGradScan[:,7], '*')
ax.plot(Egun, MMMG, '-')
ax.grid()
ax.set_xlabel(r'$E_{\rm gun}$ (MV/m)')
ax.set_ylabel(r'MMMG (MeV/c)')
fig.savefig('MMMG-vs-Egun.eps')

print('Egun = %.2f MV/m' % obj_Egun)

# Booster gradient scan
var0 = np.array([obj_Egun])
var1 = np.array([phi_gun])
var2 = np.linspace(10, 25, 31)
var3 = np.array([phi_boo])
combi = np.array([[v0, v1, v2, v3] for v0 in var0 for v1 in var1 for v2 in var2 for v3 in var3])

for i in combi:
    res = obj_boosterGradScan(i)

data = np.loadtxt('boosterGradScan.dat')
MMMG_Eboo = interp1d(data[:,4], data[:,7])
Eboo_MMMG = interp1d(data[:,7], data[:,4])
obj_Eboo = Eboo_MMMG(MG_boo)

Eboo = np.linspace(10, 25, 101)
MMMG = MMMG_Eboo(Eboo)

fig, ax = plt.subplots()
ax.plot(data[:,4], data[:,7], '*')
ax.plot(Eboo, MMMG, '-')
ax.grid()
ax.set_xlabel(r'$E_{\rm booster}$ (MV/m)')
ax.set_ylabel(r'MMMG (MeV/c)')
fig.savefig('MMMG-vs-booster.eps')

print('Eboo = %.2f MV/m' % obj_Eboo)

obj = obj_boosterGradScan([obj_Egun, 0, obj_Eboo, 0])
print('Mean momentum at MMMG: ', obj)
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 19 16:27:23 2021

@author: lixiangk
"""

import sys

#from platypus import *
#from platypus.mpipool import MPIPool
#from algorithms.NSGAII_resume import *

import logging
import timeit
import os,shutil

from transport.TransferMatrix import *
from my_object import *

def Demo(sigma, transfer = None, ss = None, fig_ext = '.eps'):
    if ss is None:
        ss = np.arange(len(sigma))
        
    res = []
    for s in sigma:
        xemit = np.sqrt(np.linalg.det(s[0:2,0:2]))*gg
        yemit = np.sqrt(np.linalg.det(s[2:4,2:4]))*gg
        res.append([s[i,i]**0.5 for i in np.arange(dim)]+[xemit, yemit])
    res = np.array(res); print(res)
    
    #reset_margin(bottom=0.1, top=0.9)
    fig, [ax1, ax2, ax3]= plt.subplots(nrows = 3, figsize = (4.5, 5),
                                       sharex = True)
    res[:,0] *= 1
    res[:,dim] *= 1
    #import pdb; pdb.set_trace()
    for i in [0, 2]:
        ax1.plot(ss, res[:,i])
    
    for i in [dim, dim+1]:
        ax2.plot(ss, res[:,i])   
    
    ax3.plot(ss, res[:,0]**2/res[:,dim]*gg)
    ax3.plot(ss, res[:,2]**2/res[:,dim+1]*gg)
    
    if transfer is not None and dim == 6:
        R16 = [ma[0,5] for ma in transfer[1:]]
        R26 = [ma[1,5] for ma in transfer[1:]]
        R56 = [ma[4,5] for ma in transfer[1:]]
        
        ax32 = ax3.twinx()
        ax32.plot(ss[1:], R16, 'c')
        ax32.plot(ss[1:], R26, 'm')
        ax32.plot(ss[1:], R56, 'k')
        ax32.set_ylabel(r'$D$', labelpad=-2)
        ax32.legend([r'$R_{16}$', r'$R_{26}$', r'$R_{56}$'],
                    loc = 'upper right')
        ax32.set_ylim(-1, 1)
        #ax32.set_yticks([-0.6, -0.3, 0, 0.3, 0.6])
    
    ax3.set_xlabel(r'$s$ (m)')
    ax1.set_ylabel(r'RMS (mm)', labelpad=3)
    ax2.set_ylabel(r'Emit. ($\mu$m)', labelpad=3)
    ax3.set_ylabel(r'$\beta_{x, y}$ (m)', labelpad=8)
    #ax1.set_ylim(0, 0.299)
    #ax2.set_ylim(0, 1.99)
    #ax3.set_ylim(0, 2.99)
    ax1.grid()
    ax2.grid()
    ax3.grid()
    
    #ax2.set_yscale('log')
    ax1.legend([r'$x$', r'$y$', r'$z$'], loc = 'lower left')
    ax2.legend([r'$x$', r'$y$'], loc = 'upper left')
    ax3.legend([r'$x$', r'$y$'], loc = 'upper left')        
    
    plt.tight_layout(h_pad = 0)
    fig.savefig('Transport@'+fig_ext)
    
    return

#%%
lq = 0.0675
Rquad = 2.15e-1 # cm

beamfile = r'//afs/ifh.de/group/pitz/data/lixiangk/sim/2020/BioRad/Case2/T1/ast.0528.001'
sigma0, P0 = SigmaMatrix6D(beamfile)
P0 *= 1e3; print('P0 = ', P0, ' MeV/c')

gg = kinetic2gamma(M2K(P0))

pos0, pos4, pos5 = 5.277, 10, 12
pos1, pos2, pos3 = 5.852500E+00, 6.892500E+00, 8.180000E+00

l0 = pos1-pos0-lq/2
l1 = pos2-pos1-lq
l2 = pos3-pos2-lq
l3 = pos4-pos3-lq/2

l4 = pos5-pos4

# positions of the ends of the elements, for plotting
ss = np.array([0, l0, lq, l1, lq, l2, lq, l3, l4])
for j in np.arange(1, len(ss)):
    ss[j] += ss[j-1]
    
g1 = 0.5
g2 = g1*1.00
g3 = g1*0.50

init_step = g1*0.1 # control step size
alpha = 0.05 # control scaling of the fitted parameter
err_sum = 0.05 # the sum of the absolute err

#% scan g2 to get a round beam at the first screen station then g3 at the second
### station and then repeat
plt.close('all')
for i in np.arange(10):
    g_list = []
    std_x_list = []
    std_y_list = []
    
    plt.figure()
    flag = 0
    nTrials, maxTrials = 0, 10
    while nTrials<maxTrials:
        nTrials += 1
        
        # define optics
        k1, k2, k3 = G2K(g1,P0), G2K(g2, P0), G2K(g3, P0)
        
        D0, D1, D2, D3, D4 = NMatrix([l0, l1, l2, l3, l4])
        Q1, Q2, Q3 = NMatrix([k1, -k2, k3], lq, func = Quad)
        
        M_list = [D0, Q1, D1, Q2, D2, Q3, D3, D4]
        M_list = [np.array(M.evalf(5), dtype = 'double')
                                    for M in M_list]
        
        # dimension for the transport
        dim = 4
        sigma, transfer = Transport6D(sigma0, *M_list, dim = dim)
        
        std_x = np.sqrt(sigma[-2][0,0])
        std_y = np.sqrt(sigma[-2][2,2])
        if nTrials == 1:
            if std_x>std_y:
                sg = -1
            else:
                sg = 1
                
        g_list.append([g2])
        std_x_list.append([std_x])
        std_y_list.append([std_y])
        
        # plot intermediate results
        plt.plot(np.asarray(g_list).flatten(), np.array(std_x_list), '-o')
        plt.plot(np.asarray(g_list).flatten(), np.array(std_y_list), '-D')
        plt.show()
        plt.pause(0.1)
        plt.cla()
        
        if np.abs(std_x-std_y)<0.01 or flag:
            break
        
        if len(g_list)>=2:
            func = lambda x, a, b:a+b*x
            xx = np.array(std_x_list)-np.array(std_y_list)
            yy = np.array(g_list)
            
            xx, yy = xx.flatten(), yy.flatten()
            
            sort = yy.argsort()[:]
            xx = xx[sort]
            yy = yy[sort]
            
            #popt, pcov = curve_fit(func, xx.flatten()[-3:], yy.flatten()[-3:])
            popt, pcov = curve_fit(func, xx, yy)
            #print(pcov)
            #g2 = g2+(func(0, *popt)-g2)*1.05
            g2 = func(0, *popt)*(1+sg*alpha)
            flag = 1
        else:
            if std_x>std_y:
                g2 -= init_step
            else:
                g2 += init_step
    alpha *= 0.5
    init_step *= 0.5
    
    plt.plot(np.asarray(g_list).flatten(), np.array(std_x_list), '-o')
    plt.plot(np.asarray(g_list).flatten(), np.array(std_y_list), '-D')
    #plt.close()
    
    #Demo(sigma, transfer, ss, fig_ext = '.png')
    
    # now find g3 to make a round beam at the second screen station
    g_list = []
    std_x_list = []
    std_y_list = []
    
    plt.figure()
    flag = 0
    nTrials, maxTrials = 0, 10
    while nTrials<maxTrials:
        nTrials += 1
        
        k1, k2, k3 = G2K(g1,P0), G2K(g2, P0), G2K(g3, P0)
        
        D0, D1, D2, D3, D4 = NMatrix([l0, l1, l2, l3, l4])
        Q1, Q2, Q3 = NMatrix([k1, -k2, k3], lq, func = Quad)
        
        M_list = [D0, Q1, D1, Q2, D2, Q3, D3, D4]
        M_list = [np.array(M.evalf(5), dtype = 'double')
                                    for M in M_list]
        
        # dimension for the transport
        dim = 4
        sigma, transfer = Transport6D(sigma0, *M_list, dim = dim)
        
        std_x = np.sqrt(sigma[-1][0,0])
        std_y = np.sqrt(sigma[-1][2,2])
        
        g_list.append([g3])
        std_x_list.append([std_x])
        std_y_list.append([std_y])
        
        plt.plot(np.asarray(g_list).flatten(), np.array(std_x_list), '-o')
        plt.plot(np.asarray(g_list).flatten(), np.array(std_y_list), '-D')
        plt.pause(0.1)
        plt.cla()
        
        if np.abs(std_x-std_y)<0.01 or flag:
            break
        
        if len(g_list)>=2:
            func = lambda x, a, b:a+b*x
            xx = np.array(std_x_list)-np.array(std_y_list)
            yy = np.array(g_list)
            
            xx, yy = xx.flatten(), yy.flatten()
            
            sort = yy.argsort()[:]
            xx = xx[sort]
            yy = yy[sort]
            
            popt, pcov = curve_fit(func, xx, yy)
            #print(pcov)
            #g3 = g3+(func(0, *popt)-g3)*1.0
            
            g3 = func(0, *popt)
            flag  = 1
        else:
            g3 += init_step
        
    plt.plot(np.asarray(g_list).flatten(), np.array(std_x_list), '-o')
    plt.plot(np.asarray(g_list).flatten(), np.array(std_y_list), '-D')
    #plt.close()
    
    std_x = np.sqrt(np.array(sigma)[-2:-1,0,0])
    std_y = np.sqrt(np.array(sigma)[-2:-1,2,2])
    
    if np.sum(np.abs(std_x-std_y))<err_sum:
        break
print('Solution found after %d rounds' % (i+1))
Demo(sigma, transfer, ss, fig_ext = '.png')
#%% scan Q2 with a moderate Q3, almost same procedure as the Matlab script does
lq = 0.0675
Rquad = 2.15e-1 # cm

beamfile = r'//afs/ifh.de/group/pitz/data/lixiangk/sim/2020/BioRad/Case2/T1/ast.0528.001'
sigma0, P0 = SigmaMatrix6D(beamfile)
P0 *= 1e3; print('P0 = ', P0, ' MeV/c')

gg = kinetic2gamma(M2K(P0))

# define positions of quads and screens
pos0 = 5.277
pos1, pos2, pos3 = 5.852500E+00, 6.892500E+00, 8.180000E+00
pos4, pos5 = 10, 14

l0 = pos1-pos0-lq/2
l1 = pos2-pos1-lq
l2 = pos3-pos2-lq
l3 = pos4-pos3-lq/2
l4 = pos5-pos4

# positions of the ends of the elements, used for plot
ss = np.array([0, l0, lq, l1, lq, l2, lq, l3, l4])
for j in np.arange(1, len(ss)):
    ss[j] += ss[j-1]

# define inital gradients
g1 = 0.5
g2 = g1*0.8
g3 = g1*0.5

g_step = g1*0.1

# linear function for fitting
func = lambda x, a, b:a+b*x

#% find g2 to make a round beam at the first screen station
g_list = []
std_x_list = []
std_y_list = []

plt.figure()
nTrials, maxTrials = 0, 10
while nTrials<maxTrials:
    nTrials += 1
    
    if len(g_list)>=2:
        xx = np.array(std_x_list)-np.array(std_y_list)
        yy = np.array(g_list)
        popt, pcov = curve_fit(func, xx.flatten()[-3:], yy.flatten()[-3:])
        #print(pcov)
        g2 = g2+(func(0, *popt)-g2)*0.95
    else:
        g2 += g_step
    
    k1, k2, k3 = G2K(g1,P0), G2K(g2, P0), G2K(g3, P0)
    
    D0, D1, D2, D3, D4 = NMatrix([l0, l1, l2, l3, l4])
    Q1, Q2, Q3 = NMatrix([k1, -k2, k3], lq, func = Quad)
    
    M_list = [D0, Q1, D1, Q2, D2, Q3, D3, D4]
    M_list = [np.array(M.evalf(5), dtype = 'double')
                                for M in M_list]
    
    # dimension for the transport
    dim = 4
    sigma, transfer = Transport6D(sigma0, *M_list, dim = dim)
    
    std_x = np.sqrt(sigma[-2][0,0])
    std_y = np.sqrt(sigma[-2][2,2])

    g_list.append([g2])
    std_x_list.append([std_x])
    std_y_list.append([std_y])
    
    plt.plot(np.asarray(g_list).flatten(), np.array(std_x_list), '-o')
    plt.plot(np.asarray(g_list).flatten(), np.array(std_y_list), '-D')
    plt.pause(0.1)
    plt.cla()
    
    if np.abs(std_x-std_y)<0.01:
        break
plt.plot(np.asarray(g_list).flatten(), np.array(std_x_list), '-o')
plt.plot(np.asarray(g_list).flatten(), np.array(std_y_list), '-D')

Demo(sigma, transfer, ss, fig_ext = '.png')

#% find g2 to make a round beam at the third quad
g_list = []
std_x_list = []
std_y_list = []

g20 = g2
z0, zz = pos3, [pos4, pos5]

plt.figure()
# first scan g2 around the setting found in previous step
for r in np.linspace(0.8, 1.0, 2):
    g2 = g20*r
    
    k1, k2, k3 = G2K(g1,P0), G2K(g2, P0), G2K(g3, P0)
    
    D0, D1, D2, D3, D4 = NMatrix([l0, l1, l2, l3, l4])
    Q1, Q2, Q3 = NMatrix([k1, -k2, k3], lq, func = Quad)
    
    M_list = [D0, Q1, D1, Q2, D2, Q3, D3, D4]
    M_list = [np.array(M.evalf(5), dtype = 'double')
                                for M in M_list]
    
    # dimension for the transport
    dim = 4
    sigma, transfer = Transport6D(sigma0, *M_list, dim = dim)
    
    std_x = np.sqrt(np.array(sigma)[-2:,0,0])
    std_y = np.sqrt(np.array(sigma)[-2:,2,2])
    
    popt, pcov = curve_fit(func, zz, std_x)
    std_x0 = func(z0, *popt)
    popt, pcov = curve_fit(func, zz, std_y)
    std_y0 = func(z0, *popt)
    
    g_list.append([g2])
    std_x_list.append([std_x0])
    std_y_list.append([std_y0])
    
plt.plot(np.asarray(g_list).flatten(), np.array(std_x_list), '-o')
plt.plot(np.asarray(g_list).flatten(), np.array(std_y_list), '-D')

Demo(sigma, transfer, ss, fig_ext = '.png')

# then interpolate to get g2
xx = np.array(std_x_list)-np.array(std_y_list)
yy = np.array(g_list)
popt, pcov = curve_fit(func, xx.flatten()[-3:], yy.flatten()[-3:])

g2 = func(0, *popt)

#% find g3 to make a round beam at the first screen station
g_list = []
std_x_list = []
std_y_list = []

plt.figure()

nTrials, maxTrials = 0, 10
while nTrials<maxTrials:
    nTrials += 1
    
    if len(g_list)>=2:
        func = lambda x, a, b:a+b*x
        xx = np.array(std_x_list)-np.array(std_y_list)
        yy = np.array(g_list)
        popt, pcov = curve_fit(func, xx.flatten()[-3:], yy.flatten()[-3:])
        print(pcov)
        g3 = g3+(func(0, *popt)-g3)*0.9
    else:
        g3 += 0.05
    
    k1, k2, k3 = G2K(g1,P0), G2K(g2, P0), G2K(g3, P0)
    
    D0, D1, D2, D3, D4 = NMatrix([l0, l1, l2, l3, l4])
    Q1, Q2, Q3 = NMatrix([k1, -k2, k3], lq, func = Quad)
    
    M_list = [D0, Q1, D1, Q2, D2, Q3, D3, D4]
    M_list = [np.array(M.evalf(5), dtype = 'double')
                                for M in M_list]
    
    # dimension for the transport
    dim = 4
    sigma, transfer = Transport6D(sigma0, *M_list, dim = dim)
    
    std_x = np.sqrt(sigma[-2][0,0])
    std_y = np.sqrt(sigma[-2][2,2])

    g_list.append([g3])
    std_x_list.append([std_x])
    std_y_list.append([std_y])
    
    plt.plot(np.asarray(g_list).flatten(), np.array(std_x_list), '-o')
    plt.plot(np.asarray(g_list).flatten(), np.array(std_y_list), '-D')
    plt.pause(0.1)
    plt.cla()
    
    if np.abs(std_x-std_y)<0.01:
        break
          
plt.plot(np.asarray(g_list).flatten(), np.array(std_x_list), '-o')
plt.plot(np.asarray(g_list).flatten(), np.array(std_y_list), '-D')

Demo(sigma, transfer, ss, fig_ext = '.png')
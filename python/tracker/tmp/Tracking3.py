# coding: utf-8
# get_ipython().magic(u'matplotlib inline')
# %matplotlib notebook

from universal import *

from scipy.optimize import minimize
from scipy.misc import derivative
from scipy.interpolate import interp2d
from scipy import signal
import pickle as pickle

from .SpaceCharge3DFFT import *
from .Beam import *

def unwrap_step_i(arg, **kwarg):
    return Tracking.step_i(*arg, **kwarg)

class Tracking():
    
    def __init__(self, beam, em3d, t1, dt = 1e-12, t0 = None, diag_interval = 10, dump_interval = 100,\
                 Lspch = False, Run = 1):
        self.beam = beam
        self.em3d = em3d
        #self.sc3d = sc3d
        
        self.clock = None
        self.time = t0
        self.dt = dt
        self.tend = t1
        self.stepped = 0
        self.diag_interval = diag_interval
        self.dump_interval = dump_interval
        self.dump_factor = 1
        
        self.Lspch = Lspch
        self.Run = Run
        
        g_mec2_eV = g_mec2*1e6
        self.cc0 = g_c/g_mec2_eV
        self.cc1 = g_mec2_eV/g_c
        self.cc2 = 1./g_me*g_qe
        
        return
    
    def deriv(self, t, y):
        '''
        Calculate derivatives of many particles altogeter
        Parameters
          t: time/s
          y[0]: x/m
          y[1]: y/m
          y[2]: z/m
          y[3]: P_x*c
          y[4]: P_y*c
          y[5]: P_z*c
        Returns
          derivatives of y with respect to t
        '''
        #import pdb; pdb.set_trace()
        #print inspect.stack()[0][3], len(y)
        y = y.reshape(len(y)/6, 6)
        Pxc, Pyc, Pzc = y[:,3], y[:,4], y[:,5]
        #if len(f_args)>0:
        #    size = size()
        #    clock = f_args[:,0]*1e-9
        #    charge = f_args[:,1]
        #    index = f_args[:,2]
        #    status = f_args[:,3]
        if self.clock is not None:
            clock = self.clock
        else:
            clock = np.zeros(len(y))
        
        #Ex, Ey, Ez, Bx, By, Bz = np.zeros((len(y), 6)).T[:]
        Ex, Ey, Ez, Bx, By, Bz = self.em3d.get_field(y[:,0], y[:,1], y[:,2], t+clock, self.time).T[:]
        #Ex, Ey, Ez, Bx, By, Bz = self.em3d.get_field(y[0,0], y[0,1], y[0,2], t)[0]
        
        
        g_mec2_eV = g_mec2*1e6
        gamma = np.sqrt(g_mec2_eV**2+Pxc**2+Pyc**2+Pzc**2)/g_mec2_eV
        cc0 = self.cc0/gamma
        cc1 = self.cc1*gamma
        cc2 = self.cc2/gamma
        
        
        #F2D = self.em3d.get_field(y[:,0], y[:,1], y[:,2], t+clock)
        #print F2D.T
        #if self.sc3d is not None:
        #    vz = Pzc*g_qe/g_me/gamma/g_c
        #    #print vz
        #    print self.sc3d(y[:,0], y[:,1], y[:,2]-(t-self.time)*vz)
        #    F2D = np.add(F2D, self.sc3d(y[:,0], y[:,1], y[:,2], t-self.time))
        #Ex, Ey, Ez, Bx, By, Bz = F2D.T[:]
        #import pdb; pdb.set_trace()
        
        dydt = np.zeros(y.shape)
        dydt[:,0] = Pxc*cc0
        dydt[:,1] = Pyc*cc0
        dydt[:,2] = Pzc*cc0
        dydt[:,3] = (Pyc*Bz-Pzc*By+cc1*Ex)*cc2
        dydt[:,4] = (Pzc*Bx-Pxc*Bz+cc1*Ey)*cc2
        dydt[:,5] = (Pxc*By-Pyc*Bx+cc1*Ez)*cc2

        return dydt.ravel()
    
    def stepOneParticle(self, y0, t0, dt):
    
        #y0 = np.atleast_2d(y0)
        r = ode(self.deriv).set_integrator('dopri5')
        r.set_initial_value(y0, t0).set_f_params()
        
        r.integrate(r.t+dt)
        #print(r.y)
        
        return r.y
    
    def stepOneParticleVec(self, y0, t0, dt):
    
        #y0 = [xx, yy, zz, px, py, pz]
        #y0 = np.atleast_2d(y0)
        r = ode(self.deriv).set_integrator('dopri5')
        r.set_initial_value(y0, t0).set_f_params()
        
        r.integrate(r.t+dt)
        #print(r.y)
        return r.y
    
    def step3(self, nstep = 1, num_cores = 1, NMAX = 20000, **kwagrs):
        '''
        Integrate all particles together
        Returns
          (x, y, z, Px*c, Py*c, Pz*c)
        '''
        
        while nstep>0:
            self.stepped += 1
            
            #if self.Lspch is True:
            #    sc3d = self.beam.get_sc3d(**kwagrs)
            #    self.em3d.update_external_field('spacecharge', sc3d)
            
            dist = self.beam.dist
            select = (dist[:,9]>0)
            dist = dist[select]
            if num_cores>1:
                # multi-core run
                # num = np.arange(self.NP)
                # results = Parallel(n_jobs=num_cores)(delayed(unwrap_step_i)(i) for i in zip([self]*len(num), num))
                # self.update(results)
                pass
            else:
                # run with one core
                nop = len(dist)
                if nop>NMAX:
                    nbeamlet = int(1.0*(nop-1)/NMAX)+1
                    #print inspect.stack()[0][3], 'beamlets: ', nbeamlet
                    #r = ode(self.deriv).set_integrator('dopri5')
                    for i in np.arange(nbeamlet):
                        #print inspect.stack()[0][3], 'beamlet: #', i
                        i0 = i*NMAX
                        i1 = i*NMAX+NMAX
                        if i1>nop:
                            i1 = nop
                        
                        y0 = dist[i0:i1,0:6]
                        t0 = np.ones(len(y0))*self.time
                        dt = np.ones(len(y0))*self.dt
                        
                        #y = map(self.stepOneParticle, y0, t0, dt)
                        #dist[i0:i1,0:6] = y
                        
                        vfunc = np.vectorize(self.stepOneParticleVec,\
                                         signature='(m),(),()->(n)')
                        y = vfunc(y0, t0, dt)
                        dist[i0:i1,0:6] = y
                else:
                    #r = ode(self.deriv).set_integrator('dopri5')
                    y0 = dist[:,0:6]
                    t0 = np.ones(len(y0))*self.time
                    dt = np.ones(len(y0))*self.dt

                    y = map(self.stepOneParticle, y0, t0, dt)
                    #dist[:,0:6] = y
                    
                    #vfunc = np.vectorize(self.stepOneParticleVec,\
                    #                     signature='(m),(),()->(n)')
                    #y = vfunc(y0, t0, dt); 
                    print(np.shape(y))
                    dist[:,0:6] = y
            
            select2 = (dist[:,5]<0)|(dist[:,2]<0)
            dist[select2,-1] = -89
            self.beam.dist[select] = dist
            
            
            self.time += self.dt
            nstep = nstep-1
            
            self.zmean = self.beam.zmean()
            
            if self.stepped % self.diag_interval == 0:
                self.beam.diagnose('trk.diag.%03d' % self.Run, em3d = self.em3d, time = self.time)
            
            #if self.stepped % self.dump_interval == 0:
            #    while int(self.zmean*self.dump_factor)<10:
            #        self.dump_factor *= 10
            #    self.beam.dump('trk.%04d.%03d' % (self.zmean*self.dump_factor, self.Run))
            
            #if self.stepped % self.dump_interval == 0:
            #    self.beam.dump('trk.%06d.%03d' % (self.stepped, self.Run))
            
            if self.stepped % 100 == 0:
                print(str.format('Tracking: iter = %6d, time = %12.6E' % (self.stepped, self.time)))
        return
    
    def step_i(self, i):
        y0 = self.get(i)
        t0 = self.time

        r = ode(self.deriv).set_integrator('dopri5')
        r.set_initial_value(y0, t0).set_f_params()
        r.integrate(r.t+self.dt)

        # self.set(i, r.y)
        return r.y 
    
    def step(self, nstep = 1, num_cores = 1, NMAX = 20000, **kwagrs):
        '''
        Integrate all particles together
        Returns
          (x, y, z, Px*c, Py*c, Pz*c)
        '''
        
        while nstep>0:
            self.stepped += 1
            
            #if self.Lspch is True:
            #    sc3d = self.beam.get_sc3d(**kwagrs)
            #    self.em3d.update_external_field('spacecharge', sc3d)
            
            dist = self.beam.dist
            select = (dist[:,9]>0)
            dist = dist[select]
            if num_cores>1:
                # multi-core run
                # num = np.arange(self.NP)
                # results = Parallel(n_jobs=num_cores)(delayed(unwrap_step_i)(i) for i in zip([self]*len(num), num))
                # self.update(results)
                pass
            else:
                # run with one core
                nop = len(dist)
                if nop>NMAX:
                    nbeamlet = int(1.0*(nop-1)/NMAX)+1
                    #print inspect.stack()[0][3], 'beamlets: ', nbeamlet
                    r = ode(self.deriv).set_integrator('dopri5')
                    for i in np.arange(nbeamlet):
                        #print inspect.stack()[0][3], 'beamlet: #', i
                        i0 = i*NMAX
                        i1 = i*NMAX+NMAX
                        if i1>nop:
                            i1 = nop
                        y0 = dist[i0:i1,0:6].ravel()
                        t0 = self.time
                        
                        r.set_initial_value(y0, t0).set_f_params()
                        r.integrate(r.t+self.dt)
                        
                        if not r.successful():
                            print(inspect.stack()[0][3], 'Integration failed.')
                        
                        dist[i0:i1,0:6] = r.y.reshape((i1-i0, 6))
                else:
                    r = ode(self.deriv).set_integrator('dopri5')
                    y0 = dist[:,0:6].ravel()
                    t0 = self.time

                    r.set_initial_value(y0, t0).set_f_params()
                    r.integrate(r.t+self.dt)
                    
                    if not r.successful():
                        print(inspect.stack()[0][3], 'Integration failed.')
                            
                    dist[:,0:6] = r.y.reshape((nop, 6))
            
            select2 = (dist[:,5]<0)|(dist[:,2]<0)
            dist[select2,-1] = -89
            self.beam.dist[select] = dist
            
            
            self.time += self.dt
            nstep = nstep-1
            
            self.zmean = self.beam.zmean()
            
            if self.stepped % self.diag_interval == 0:
                self.beam.diagnose('trk.diag.%03d' % self.Run, em3d = self.em3d, time = self.time)
            
            #if self.stepped % self.dump_interval == 0:
            #    while int(self.zmean*self.dump_factor)<10:
            #        self.dump_factor *= 10
            #    self.beam.dump('trk.%04d.%03d' % (self.zmean*self.dump_factor, self.Run))
            
            #if self.stepped % self.dump_interval == 0:
            #    self.beam.dump('trk.%06d.%03d' % (self.stepped, self.Run))
            
            if self.stepped % 100 == 0:
                print(str.format('Tracking: iter = %6d, time = %12.6E' % (self.stepped, self.time)))
        return
    
    def dump(self):
        while int(self.zmean*self.dump_factor)<1:
            self.dump_factor *= 10
        self.beam.dump('trk.%04d.%03d' % (self.zmean*self.dump_factor, self.Run), self.time)
        
    def is_emit(self):
        select = (self.beam.dist[:,9]==-1)|(self.beam.dist[:,9]==-3)
        if np.sum(select)>0:
            return True
        else:
            return False
        
    def cathode(self, namelist, fnamebase = 'trk.cathode'):
        r = [self.beam.zmean(), self.time]
        #if self.sc3d is not None:
        #    Ex, Ey, Ez, Bx, By, Bz = self.sc3d(0, 0, 0).T[:]
        #    r.append(Ez[0])
        #else:
        #    r.append(0)
        for name in namelist:
            Ex, Ey, Ez, Bx, By, Bz = self.em3d.get_field(0, 0, 0, self.time, namelist = [name]).T[:]
            r.append(Ez[0])
        r.append(self.beam.charge()) # Qtot
        
        with open(fnamebase+('.%03d' % self.Run), 'a') as f_handle:
            np.savetxt(f_handle, np.atleast_2d(r), fmt='%14.6E')
         
    def emit(self, nstep = 1, num_cores=1, NMAX = 10000, NMIN = 30, **kwargs):
        '''
        Integrate all particles together
        Returns
          (x, y, z, Px*c, Py*c, Pz*c)
        '''
        
        while nstep>0:
            self.stepped += 1
            
            #if self.Lspch is True and self.stepped>5:
            #    sc3d = self.beam.get_sc3d(**kwargs)
            #    self.em3d.update_external_field('spacecharge', sc3d)
            
            # Get the particles not emitted yet
            # dist = self.beam.dist
            select = (self.beam.dist[:,9]==-1)|(self.beam.dist[:,9]==-3)
            # Get also the particles emitted already
            select2 = (self.beam.dist[:,9]>0)
            dt_max = self.dt
            #import pdb; pdb.set_trace()
            
            # Select those to be emitted
            if np.sum(select)<=NMIN:
                sort = (self.beam.dist[select,6]).argsort()[::-1]; # print sort
                t1 = self.beam.dist[select][sort][0,6] # first particle to emit this time
                tc = self.beam.dist[select][sort][-1,6] # first particle to emit next time
                dt_max = (t1-tc)*1.05e-9
            else:
                sort = (self.beam.dist[select,6]).argsort()[::-1]; # print sort
                t1 = self.beam.dist[select][sort][0,6]
                tc = self.beam.dist[select][sort][NMIN,6]
                select *= (self.beam.dist[:,6]>tc)
                dt_max = (t1-tc)*1e-9
            dist_emit = self.beam.dist[select]
            
            if 0: 
                # Particles emission one by one
                for i in np.arange(len(dist_emit)):
                    y0 = dist_emit[i,0:6]
                    
                    if self.time is None:
                        self.time = -t1*1e-9
                        print('Initial time is set to ', self.time*1e12, ' ps')
                    
                    dt = dt_max-(t1-dist_emit[i,6])*1e-9
                    t0 = self.time+dt_max-dt
                    
                    Ex, Ey, Ez, Bx, By, Bz = self.em3d.get_field(y0[0], y0[1], y0[2], t0).T[:]
                    if Ez[0]<=0:
                        dist_emit[i,9] = -89
                        continue
                    
                    r = ode(self.deriv).set_integrator('dopri5')
                    
                    r.set_initial_value(y0, t0).set_f_params()
                    r.integrate(r.t+dt)
                    
                    if not r.successful():
                        print(inspect.stack()[0][3], 'Integration failed.')
                    
                    dist_emit[i,0:6] = r.y
                    dist_emit[i,6] = t0/1e-9 # ns, emitting time
                    
                    if dist_emit[i,9] == -1:
                        dist_emit[i,9] = 5;
                    elif dist_emit[i,9] == -3:
                        dist_emit[i,9] = 3
                    else:
                        dist_emit[i,9] *= -1
            else:
                if self.time is None:
                    self.time = -t1*1e-9
                    print('Initial time is set to ', self.time*1e12, ' ps')
                        
                y0 = dist_emit[:,0:6]
                dt = dt_max-(t1-dist_emit[:,6])*1e-9
                t0 = self.time+dt_max-dt
                #print(t0)
                Ex, Ey, Ez, Bx, By, Bz = self.em3d.get_field(y0[:,0], y0[:,1], y0[:,2], t0).T[:]
                
                not_emit = (Ez<=0)
                dist_emit[not_emit,9] = -89
                
                
                to_emit = (Ez>0); print(to_emit.shape)
                
                y0 = dist_emit[to_emit,0:6]
                dt = dt_max-(t1-dist_emit[to_emit,6])*1e-9
                t0 = self.time+dt_max-dt
                
                #import pdb; pdb.set_trace()
                y = map(self.stepOneParticle, y0, t0, dt)
                #dist[:,0:6] = y
                
                #vfunc = np.vectorize(self.stepOneParticleVec,\
                #                     signature='(m),(),()->(n)')
                #y = vfunc(y0, t0, dt)
                #print(y)
                dist_emit[to_emit,0:6] = np.reshape(y, (len(y), 6))
                
                dist_emit[to_emit,6] = t0/1e-9 # ns, emitting time
                
                dist_emit[to_emit,9] = np.where(dist_emit[to_emit,9] == -1, 5,\
                         -dist_emit[to_emit,9])
            
            
            # Replace the distributions with those
            self.beam.dist[select] = dist_emit
            
            # Now it's turn to advance one step for those emitted already
            dist = self.beam.dist[select2]
            
            if num_cores>1:
                # multi-core run
                pass
            else:
                # run with one core
                nop = len(dist); # print inspect.stack()[0][3], nop
                if nop == 0:
                    pass
                elif nop>NMAX:
                    nbeamlet = int(1.0*(nop-1)/NMAX)+1
                    #print inspect.stack()[0][3], 'beamlets: ', nbeamlet
                    r = ode(self.deriv).set_integrator('dopri5')
                    for i in np.arange(nbeamlet):
                        #print inspect.stack()[0][3], 'beamlet: #', i
                        i0 = i*NMAX
                        i1 = i*NMAX+NMAX
                        if i1>nop:
                            i1 = nop
                        y0 = dist[i0:i1,0:6].ravel()
                        t0 = self.time
                        r.set_initial_value(y0, t0).set_f_params()
                        r.integrate(r.t+dt_max)
                        
                        if not r.successful():
                            print(inspect.stack()[0][3], 'Integration failed.')
                        
                        dist[i0:i1,0:6] = r.y.reshape((i1-i0, 6))
                else:
                    r = ode(self.deriv).set_integrator('dopri5')
                    y0 = dist[:,0:6].ravel()
                    t0 = self.time
                    r.set_initial_value(y0, t0).set_f_params()
                    r.integrate(r.t+dt_max)
                    
                    if not r.successful():
                        print(inspect.stack()[0][3], 'Integration failed.')
                    
                    dist[:,0:6] = r.y.reshape((nop, 6))
                 
#                    for i in np.arange(nbeamlet):
#                        i0 = i*NMAX
#                        i1 = i*NMAX+NMAX
#                        if i1>nop:
#                            i1 = nop
#                        
#                        y0 = dist[i0:i1,0:6]
#                        t0 = np.ones(len(y0))*self.time
#                        dt = np.ones(len(y0))*dt_max
#                        
#                        #y = map(self.stepOneParticle, y0, t0, dt)
#                        #dist[i0:i1,0:6] = y
#                        
#                        vfunc = np.vectorize(self.stepOneParticleVec,\
#                                         signature='(m),(),()->(n)')
#                        y = vfunc(y0, t0, dt)
#                        dist[i0:i1,0:6] = y
#                else:
#                    #r = ode(self.deriv).set_integrator('dopri5')
#                    y0 = dist[:,0:6]
#                    t0 = np.ones(len(y0))*self.time
#                    dt = np.ones(len(y0))*dt_max
#
#                    #y = map(self.stepOneParticle, y0, t0, dt)
#                    #dist[:,0:6] = y
#                    
#                    vfunc = np.vectorize(self.stepOneParticleVec,\
#                                         signature='(m),(),()->(n)')
#                    y = vfunc(y0[:,0], y0[:,1], y0[:,2], y0[:,3], y0[:,4], y0[:,5], t0, dt)
#                    dist[:,0:6] = y
            
            select3 = (dist[:,5]<0)|(dist[:,2]<0)
            dist[select3,-1] = -89
            self.beam.dist[select2] = dist
            
            self.time += dt_max
            nstep = nstep-1
            
            #self.beam.diagnose()
            self.zmean = self.beam.zmean()
            if self.stepped % self.diag_interval == 0:
                self.beam.diagnose('trk.diag.%03d' % self.Run, em3d = self.em3d, time = self.time)
            if self.stepped % self.dump_interval == 0:
                self.beam.dump('trk.%06d.%03d' % (self.stepped, self.Run))
            
            if self.stepped % 100 == 0:
                print(str.format('Emitting: iter = %6d, time = %12.6E' % (self.stepped, self.time)))
        return
    
    def ref(self, y0, t1, dt, t0 = 0):
        '''
        Parameters
          y0: (x, y, z, P_x*c, P_y*c, P_z*c)
          t1: end time for tracking
          dt: time step for tracking
          t0: start time for tracking
        Returns
          2D arraylike: (x, y, z, P_x*c, P_y*c, P_z*c, t)
        '''

        r = ode(self.deriv).set_integrator('dopri5')
        r.set_initial_value(y0, t0).set_f_params()

        sol = [list(y0)+[t0]+list(self.em3d.get_field(y0[0], y0[1], y0[2], t0)[0])]
        while r.successful() and r.t<t1:
            r.integrate(r.t+dt)
            
            a = list(r.y)+[r.t]+list(self.em3d.get_field(r.y[0], r.y[1], r.y[2], r.t)[0])
            
            with open('trk.ref'+('.%03d' % self.Run), 'a') as f_handle:
                np.savetxt(f_handle, np.atleast_2d(a), fmt='%14.6E')
            
            #sol.append(a)
        
        #sol = np.array(sol)
        #np.savetxt('trk.ref.001', sol, fmt = '%12.6E')
        return sol  
    
    def auto_phase(self, freq = 1.3e9, t1 = 1000e-12, dt = 10e-12):
        '''
        Returns
          2D arraylike: (x, y, z, P_x*c, P_y*c, P_z*3, t)
        '''
        y0 = self.beam.dist[0]
        dist = np.array([y0 for i in np.arange(901)])
        #print inspect.stack()[0][3], dist.shape
        
        phase = np.linspace(0, 90, 901)
        self.clock = phase/360./freq
        #print self.clock*1e12
        
        t0 = 0
        y0 = dist[:,0:6].ravel(); #print inspect.stack()[0][3], len(y0)
        
        #import pdb; pdb.set_trace()
        r = ode(self.deriv).set_integrator('dopri5')
        r.set_initial_value(y0, t0).set_f_params()
        
        r.integrate(r.t+dt)
        #import pdb; pdb.set_trace()
        #print inspect.stack()[0][3], r.successful()
        if not r.successful():
            print(inspect.stack()[0][3], 'Integration failed.')
        
        while r.successful() and r.t < t1:
            #print inspect.stack()[0][3], r.t
            r.integrate(r.t+dt)
        #print inspect.stack()[0][3], r.t
        
        dist = np.reshape(r.y, (len(r.y)/6, 6))
        np.savetxt('auto.dat', dist, fmt = '%12.6E')
        
        i, m = index_max(dist[:,5])
        p0 = phase[i]
        #select = (phase>p0-10)*(phase<p0+10)
        
        #fun = lambda x, a, b, c, d, e: a+b*x+c*x**2+d*x**3+e*x**4
        #popt, pcov = sp.optimize.curve_fit(fun, phase[select], 1./dist[select,5])
        #res = sp.optimize.minimize(fun, p0, tuple(popt))
        
        fig, ax = plt.subplots(figsize = (6, 4))
        ax.plot(phase[:], dist[:,5]/1e6, '-')
        #ax.plot(phase[select], 1./fun(phase[select], *popt)/1e6, '-')
        ax.grid()
        ax.set_xlabel(r'phase (degree)')
        ax.set_ylabel(u_kinetic)
        fig.savefig('phase_scan.eps')
        
        #return [res.x[0], momentum2kinetic(1./fun(res.x[0], *popt)/1e6)]
        return [p0, momentum2kinetic(m/1e6)]

def emitting():
    dist = np.loadtxt('beam1.ini')
    NMIN = 20

    select = (dist[:,9]==-1)|(dist[:,9]==-3); print(np.sum(select))
    #import pdb; pdb.set_trace()

    if np.sum(select)<=NMIN:
        dist_emit = dist[select]
        sort = (dist[select,6]).argsort()[::-1]; # print sort
        t0 = dist[select][sort][0,6]
        tc = dist[select][sort][-1,6];  print(tc, (t0-tc)*1e3)
    else:
        sort = (dist[select,6]).argsort()[::-1]; # print sort
        t0 = dist[select][sort][0,6]
        tc = dist[select][sort][NMIN,6]; print(tc, (t0-tc)*1e3)
        select *= (dist[:,6]>tc)
        dist_emit = dist[select]

    dist_emit[:,9] = 5
    dist[select] = dist_emit

    np.savetxt('beam2.ini', dist, fmt = '%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%4.0f%4.0f')

    fig, ax = plt.subplots(figsize = (6, 4))
    ax.hist(dist[:,6], bins = 20, range = (-0.003, 0.003))
    select = (dist[:,9]==5)
    ax.hist(dist[select,6], bins = 20, range = (-0.003, 0.003), histtype = 'step')
    return
#emitting()

'''
workdir = r"\\afs\ifh.de\group\pitz\data\lixiangk\work\sync\python\space-charge"
os.chdir(workdir)

field_maps = r'\\afs\ifh.de\group\pitz\data\lixiangk\work\sync\field-maps'
gun3d = build_cavity(field_maps+os.sep+'gun42Cavity.txt', 0, 39.44e6, 1.3e9, 37)
sol3d = build_solenoid(field_maps+os.sep+'gunsolenoidsPITZ.txt', 0, -0.158797)

em3d = EMsolver()
em3d.add_external_field('gun', gun3d)
em3d.add_external_field('solenoid', sol3d)

#beam = Beam('beam.ini')
#sc3d = beam.get_sc3d()

#em3d.add_external_field('spacecharge', sc3d)


# In[563]:


import timeit

beam = Beam('beam1.ini'); # beam.dist=beam.dist[0:100000]
track = Tracking(beam, em3d, 1000e-12, 1e-12, out_inteval = 250)

n_steps = 2000
n_stepped = 0
stime = 0
while n_stepped<n_steps:
    time1 = timeit.default_timer()
    
    if track.is_emit():
        flag = 'Emitting'
        nstep = 1
        track.emit(nstep, NMIN = 100)
    else:
        flag = 'Tracking'
        nstep = 10
        track.step(nstep)
    
    time2 = timeit.default_timer()
    stime = stime+(time2-time1)

    n_stepped += nstep

    #print flag, ': 1 step takes: ', stime/n_stepped

print 'Total time:', stime
'''

# In[559]:

'''
data = np.loadtxt('trk.000050.001')
data[1:,2] += data[0,2]; data[1:,5] += data[0,5]
select = (data[:,9]>0)
print np.std(data[select,5]/1e3), np.mean(data[select,5]/1e3)
r = plt.hist(data[select,5]/1e3, bins = 30, histtype = 'step')
'''

# In[473]:

'''
beam = Beam('beam1.ini')
#beam.dist=beam.dist[0:100000]
track = Tracking(beam, em3d, 1000e-12, 1e-12)
#print track.beam.dist
import timeit
time1 = timeit.default_timer()
ref = track.step(1, NMAX = 3000)
#x = track.beam.dist[:,0]
#y = track.beam.dist[:,1]
#z = track.beam.dist[:,2]
#F2d = track.em3d.get_field(x, y, z, 0)
time2 = timeit.default_timer()
print time2-time1


beam = Beam('beam.ini')
track = Tracking(beam, em3d, 1000e-12, 1e-12)

import timeit
time1 = timeit.default_timer()
phi = track.auto_phase()
time2 = timeit.default_timer()
print time2-time1
print phi
'''

# In[238]:

'''
track = Tracking(beam, em3d, 1000e-12, 1e-12)
y0 = track.beam.dist[0,0:6]

import timeit
time1 = timeit.default_timer()
ref = track.ref(y0, 1000e-12, 10e-12)
time2 = timeit.default_timer()
print time2-time1
print track.time

fig, [ax1, ax2] = plt.subplots(figsize = (6, 8), nrows = 2)
Ek = momentum2kinetic(ref[:,5]/1e6); # print Ek[-1]
ax1.plot(ref[:,2], Ek, '-')
ax1.grid()

ax2.plot(ref[:,2], ref[:,9]/1e6, '-')
ax2.grid()
'''
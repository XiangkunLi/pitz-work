# -*- coding: utf-8 -*-
"""
Created on Wed Feb 19 17:20:16 2020

@author: lixiangk
"""

a = np.array([[1, 2], [3, 4]])

def func(a1d):
    a1d[2] = 10
    return a1d.reshape(2, 2)

def func2(a):
    a = a.reshape((1, 4))
    return a

func2(a)

#%%
import matplotlib as mpl
import matplotlib.pyplot as plt

from scipy.integrate import ode

def deriv(t, y, arg1):
    
    dydt = np.zeros(4)
    dydt[0] = y[2]
    dydt[1] = y[3]
    dydt[2] = -y[3]*arg1
    dydt[3] = y[2]*arg1
    
    return dydt

r = ode(deriv).set_integrator('dopri5')

y0 = [0, 0, 1, 1]
t0 = 0

r.set_initial_value(y0, t0).set_f_params(1)
                    
t1 = 10
dt = 0.1
res = []
while r.successful() and r.t < t1:
    r.integrate(r.t+dt)
    res.append(r.y)

res = np.array(res)
plt.plot(res[:,0], res[:,1], '-o')
plt.grid()

#%%
import matplotlib as mpl
import matplotlib.pyplot as plt

from RK45 import *

def deriv(t, y):

    y = np.reshape(y, (len(y)//4, 4))
    dydt = np.zeros(y.shape)

    dydt[:,0] =  y[:,2]
    dydt[:,1] =  y[:,3]
    dydt[:,2] = -y[:,3]
    dydt[:,3] =  y[:,2]
    
    return dydt.ravel()

y0 = np.array([0, 0, 1, 1], dtype = float)
t0 = 0
t1 = 5

solver = RK45(deriv, t0, y0, t1, max_step = 0.1, first_step = 0.01)

dt = 0.1
res = []
while solver.t<t1:
    solver.step()
    #print(solver.h_abs)
    res.append(solver.y)

res = np.array(res)
plt.plot(res[:,0], res[:,1], '-o')
plt.grid()
plt.show()
#%%
def step(y0, t0, dt):
    
    y0, t0, dt = y0, t0, dt
    
    r = ode(deriv).set_integrator('dopri5')
    r.set_initial_value(y0, t0).set_f_params(1)
    
    r.integrate(r.t+dt)
    
    return r.y

y0 = [0, 0, 1, 1]
t0 = 0

t1 = 10
dt = 0.1

res = []
while t0<t1:
    y0 = step(y0, t0, dt)
    
    res.append(y0)
    
    t0 += dt
    print(t0)

res = np.array(res)
plt.plot(res[:,0], res[:,1])
#%%
def step(y0, y1, y2, y3, t0, dt):
    
    r = ode(deriv).set_integrator('dopri5')
    r.set_initial_value([y0, y1, y2, y3], t0).set_f_params(1)
    
    r.integrate(r.t+dt)
    
    return r.y

y0 = [0, 0, 1, 1]
t0 = 0

t1 = 10
dt = 0.1

res = []
while t0<t1:
    y0 = step(*y0, t0, dt)
    
    res.append(y0)
    
    t0 += dt
    print(t0)

res = np.array(res)
plt.plot(res[:,0], res[:,1])
#%%
step_vec = np.vectorize(step)

y0 = [0, 0]
y1 = [0, 1]
y2 = [1, 1]
y3 = [1, 1]
t0 = [0, 0]
dt= [0.1, 0.1]

step_vec(y0, y1, y2, y3, t0, dt)

#%%
x0 = [[0, 0],[0, 1], [1, 1], [1, 1], [0, 0],[dt, dt]]
res = []
while t0<t1:
    
    y0 = step(x0, 1)
    
    res.append(y0)
    
    t0 += dt
    print(t0)

res = np.array(res)
plt.plot(res[:,0], res[:,1])

#%%
import numpy as np
def myfunc(a, b):
    "Return a-b if a>b, otherwise return a+b"
    r = [a + b, a*b]
    print(r)
    return r
#print(myfunc(1, 2))
vfunc = np.vectorize(myfunc, signature='(),()->(n)')

aa = [1, 2, 3, 4]
bb = [2, 3, 5, 4]
out = vfunc(aa, bb)

def test1():
    a = np.random.rand(1000)
    b = np.random.rand(1000)
    
    vfunc(a, b)
    
def test2():
    a = np.random.rand(1000)
    b = np.random.rand(1000)
    
    for i in np.arange(len(a)):
        myfunc(a[i], b[i])
        
def test3():
    a = np.random.rand(1000)
    b = np.random.rand(1000)
    
    map(myfunc, a, b)
    
def test4():
    a = np.random.rand(1000)
    b = 0.5
    
    map(myfunc, a, b)
#%%
def f(x, y):
    #return (x+y) * np.array([1,1,1,1,1], dtype=np.float32)
    return np.array([np.sum(x)*y, np.sum(x)/y])
g = np.vectorize(f, signature='(m),()->(n)')

g(np.ones((7,4)), np.arange(1, 8))

#%%
B = np.random.rand(1, 3)
V = np.random.rand(1, 3)
F = np.cross(B, V)

def oneByOne(V, B):
    Fx = V[:,1]*B[:,2]-V[:,2]*B[:,1]
    Fy = V[:,2]*B[:,0]-V[:,0]*B[:,2]
    Fz = V[:,0]*B[:,1]-V[:,1]*B[:,0]
    
#%%
from scipy.integrate import solve_ivp, RK45
def exponential_decay(t, y): return -0.5 * y
sol = solve_ivp(exponential_decay, [0, 1], [2, 4, 8])
print(sol.t)
print(sol.y)

solver = RK45(exponential_decay, 0, [2, 4, 8], 10, vectorized=False)
solver.step()
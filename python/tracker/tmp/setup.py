# -*- coding: utf-8 -*-
"""
Created on Mon Feb 24 15:09:41 2020

@author: lixiangk
"""

from distutils.core import setup
from Cython.Build import cythonize

for f in ["SpaceCharge3DFFT.pyx", "Beam.pyx", "EMSolver.pyx", "Tracking2.pyx"]:
    setup(
        ext_modules = cythonize(f)
    )
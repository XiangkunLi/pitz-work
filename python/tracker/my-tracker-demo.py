# -*- coding: utf-8 -*-
"""
Created on Thu Jun  4 23:40:50 2020

@author: lixiangk
"""

import timeit
from universal import *

Nu = 114
lam_u = 3e-2
Lu = Nu*lam_u


#####
sample = 64
lam_u, Nu, trim1, trim2 = 30e-3, 114, 0.25, 0.75
ku = 2*np.pi/lam_u
Lu = lam_u*Nu

F0 = np.zeros(sample//2)
F1 = np.ones(sample//2)*trim1
F2 = np.ones(sample//2)*trim2
Fs = np.ones((Nu-3)*sample+1)

zz = np.linspace(-Lu/2.0, Lu/2.0, Nu*sample+1)
Fs = np.concatenate((F0, F1, F2, Fs, F2[::-1], F1[::-1], F0[::-1]))

fFs = interp1d(zz, Fs, bounds_error = False, fill_value = 0)
#####

def EM3D(x, y, z, t=0, *args):
            
    if np.isscalar(x):
        x = [x]
    if np.isscalar(y):
        y = [y]
    if np.isscalar(z):
        z = [z]
    x = np.asarray(x).flatten()
    y = np.asarray(y).flatten()
    z = np.asarray(z).flatten()
    
    nn = np.max([x.size, y.size, z.size])
    
    if x.size == 1:
        x = np.ones(nn)*x[0]
    if y.size == 1:
        y = np.ones(nn)*y[0]
    if z.size == 1:
        z = np.ones(nn)*z[0]
    
    if not np.isscalar(t):
        t = np.asarray(t)
        
    z1 = z #-z0
    
    #####
    
    #B1y = 1.25*fFs(z)*np.cosh(ku*y)*np.sin(ku*z)*(1+0.75*x)
    #B1z = 1.25*fFs(z)*np.sinh(ku*y)*np.cos(ku*z)*(1+0.75*x)
    
    #####
    
    #B1y = 1.25*(1+0.75*x)*np.sin(2*np.pi*z/lam_u)
             
    B1y = np.array([0.0002335 for _ in z1])
    Lu = 3.4
    B1y = np.where((z1>-Lu/2.)*(z1< Lu/2.), B1y, 0)
    #print(B1y)
    F2d = np.zeros((nn, 6))
    F2d[:,4] = B1y # 0.00224
    
    return F2d

#%% Load the LCLS-I undulator field map saved in Astra format
from tracker.FieldMap3D import *

basename = field_maps+os.sep+'3DDESY26-20x10mm'
u3d = StaticM3D(basename, z0 = 0e-3, gamma_boost = 1)

#%% Reference particle tracking with correction coils
from astra_modules import *

from tracker.SpaceCharge3DFFT import *
from tracker.Beam import *
from tracker.EMSolver import *
from tracker.Tracking2 import *

em3d = EMsolver()
em3d.add_external_field('und', u3d.EM3D)

scale = 1
em3d.add_external_field('coil', EM3D, scale = scale)

track = Tracking(None, em3d, 1000e-12, 10e-12, dump_interval = 250, Qtot = -1)

track.Run = 999

res = track.ref([0, 0, -1.8, 0*7.10844e-3*17e6, 0, 17e6], 13300e-12, 3e-12)

reset_margin(bottom = 0.1, top = 0.95, left = 0.1, right = 0.95)

fig, ax = plt.subplots(nrows = 2, figsize = (7.2, 5.4))

# data = pd_loadtxt('trk.ref.001')
# ax[0].plot(data[:,2], data[:,0]*1e3, '-')
# ax[1].plot(data[:,2], data[:,11], '-')

data = pd_loadtxt('trk.ref.%03d' % track.Run)
ax[0].plot(data[:,2], data[:,0]*1e3, '-')
ax[1].plot(data[:,2], data[:,11], 'g-')

for i in [0, 1]:
    #ax[i].set_xlim(-1.8, 1.8)
    ax[i].set_xlabel(r'$z$ (m)')
    ax[i].grid()
#ax[0].set_ylim(-30, 10)
ax[1].set_ylim(-1.5, 1.5)
ax[0].set_ylabel(r'$x$ (mm)')
ax[1].set_ylabel(r'$B_y$ (mm)')
fig.tight_layout()
#fig.savefig('ref-trajectory-with-coil++.png')

reset_margin()
#%% Iteratively find the amplitude of correction coils for a given momentum
temp = []
scale = 1.05

#fig, ax = plt.subplots(nrows = 2, figsize = (7.2, 5.4))
for P in np.arange(17, 18):
    temp9 = []
    for i in [0, 1, 2]:
        em3d = EMsolver()
        em3d.add_external_field('und', u3d.EM3D)
        
        #scale = scale
        em3d.add_external_field('coil', EM3D, scale = scale)
        
        track = Tracking(None, em3d, 1000e-12, 10e-12, dump_interval = 250, Qtot = -1)
        
        track.Run = P+100
        #res = track.ref([0, 0, -2.0, 0e-3*17.05e6, 0, P*1e6], 13300e-12, 3e-12)
        res = track.ref([0, 0, -1.8, 0e-3*17.05e6, 0, P*1e6], 13300e-12, 3e-12)
        
        temp9.append([scale, res[-1,0]])
        
        data = np.array(temp9)
        print(i, data)
        if len(data)>1:
            f1 = interp1d(data[:,1], data[:,0], bounds_error = False, fill_value = 'extrapolate')
            #f1 = curve_fit(lambda x, a, b:a+b*x, data[:,1], data[:,0])
            scale = f1([0])[0]
        else:
            scale *= 0.9
    temp.append([P, res[-1, 0], scale])
    
    reset_margin(bottom = 0.1, top = 0.95, left = 0.1, right = 0.95)

    fig, ax = plt.subplots(nrows = 2, figsize = (7.2, 5.4))
    
    data = pd_loadtxt('trk.ref.%03d' % track.Run)
    
    ax[0].plot(data[:,2], data[:,0]*1e3, '-')
    ax[1].plot(data[:,2], data[:,11], 'g-')
    
    # data = pd_loadtxt('trk.ref.012')
    
    # ax[0].plot(data[:,2], data[:,0]*1e3, '-')
    #ax[1].plot(data[:,2], data[:,11], '-')
    
    for i in [0, 1]:
        #ax[i].set_xlim(-1.8, 1.8)
        ax[i].set_xlabel(r'$z$ (m)')
        ax[i].grid()
    #ax[0].set_ylim(-30, 10)
    ax[1].set_ylim(-1.5, 1.5)
    ax[0].set_ylabel(r'$x$ (mm)')
    ax[1].set_ylabel(r'$B_y$ (mm)')
    fig.tight_layout()
    fig.savefig('ref-trajectory-with-coil@%.0fMeV_c.png' % P)
    
    reset_margin()
#%% Very first verion for correction coils
from astra_modules import *

from tracker.SpaceCharge3DFFT import *
from tracker.Beam import *
from tracker.EMSolver import *
from tracker.Tracking2 import *

em3d = EMsolver()
em3d.add_external_field('und', und2.EM3D)

scale = 1.06621
em3d.add_external_field('coil', coil.EM3D, scale = scale)

track = Tracking(None, em3d, 1000e-12, 10e-12, dump_interval = 250, Qtot = -1)
track.Run = 4 # Only the U field with transverse gradient
#track.Run = 9 # U field + coil 3D -230.8 uT, OK! But with positron above
track.Run = 10 # U field + coil 3D 232.5 uT, electron
#track.Run = 11 # U field only, electron
#track.Run = 12 # U field only, electron with initial kick of 7e-3

track.Run = 17
res = track.ref([0, 0, -2., 0e-3*17.05e6, 0, 17e6], 13300e-12, 3e-12)
#res = track.ref([3e-3, -1e-3, -1.8, -7e-3*22e6, 0, 22e6], 12000e-12, 20e-12)

# temp9.append([scale, res[-1,0]])

# data = np.array(temp9)
# if len(data)>1:
#     f1 = interp1d(data[:,1], data[:,0])
#     print(f1(0))
#%
reset_margin(bottom = 0.1, top = 0.95, left = 0.1, right = 0.95)

fig, ax = plt.subplots(nrows = 2, figsize = (7.2, 5.4))

data = pd_loadtxt('trk.ref.%03d' % track.Run)

ax[0].plot(data[:,2], data[:,0]*1e3, '-')
ax[1].plot(data[:,2], data[:,11], 'g-')

# data = pd_loadtxt('trk.ref.012')

# ax[0].plot(data[:,2], data[:,0]*1e3, '-')
#ax[1].plot(data[:,2], data[:,11], '-')

for i in [0, 1]:
    #ax[i].set_xlim(-1.8, 1.8)
    ax[i].set_xlabel(r'$z$ (m)')
    ax[i].grid()
#ax[0].set_ylim(-30, 10)
ax[1].set_ylim(-1.5, 1.5)
ax[0].set_ylabel(r'$x$ (mm)')
ax[1].set_ylabel(r'$B_y$ (mm)')
fig.tight_layout()
#fig.savefig('ref-trajectory-with-coil++.png')

reset_margin()

#%% Plot
data = np.loadtxt('xc-vs-P.dat')
gamma = M2G(data[:,0])

reset_margin(left = 0.125, bottom = 0.1, top = 0.9, right = 0.875)
fig, ax = plt.subplots(figsize = (4, 3.75))
ax.plot(data[:,0], data[:,1]*1e3)

#data = np.loadtxt('scale-vs-P.dat')
ax2 = ax.twinx()
ax2.plot(data[:,0], data[:,2], 'b-D')

ax.grid()
ax.set_ylim(-30, -12)
ax.set_yticks(np.arange(-30, -12, 3))
ax.set_xlabel(r'$P$ (MeV/c)')
ax.set_ylabel(r'$x_{\rm off}$ (mm)', color = 'r', labelpad = -2)

ax2.set_ylim(5, 8)
ax2.set_ylabel(r'$I$ (A)', color = 'b')

ax.tick_params(axis = 'y', which = 'both', color = 'r', labelcolor = 'r')
ax2.tick_params(which = 'both', color = 'b', labelcolor = 'b')

fig.savefig('xoff-vs-P.png')

#%% Plot 2021
data = np.loadtxt('scale-vs-P2021.dat')
gamma = M2G(data[:,0])

reset_margin(left = 0.125, bottom = 0.1, top = 0.9, right = 0.875)
fig, ax = plt.subplots(figsize = (4, 3.75))
ax.plot(data[:,0], data[:,2]*20*0.36748686631968, '-D')

ax.grid()

#ax.set_ylim(5, 8)
ax.set_xlabel(r'$P$ (MeV/c)')
ax.set_ylabel(r'$I$ (A)')

fig.savefig('Icor-vs-P.png')

#%% Plot 2021

reset_margin(bottom = 0.1, top = 0.95, left = 0.1, right = 0.95)
fig, ax = plt.subplots(nrows = 1, figsize = (7.2, 3))

data = np.loadtxt('trk.ref.997')
ax.plot(data[:,2], data[:,0]*1e3, '-')

data = np.loadtxt('trk.ref.998')
ax.plot(data[:,2], data[:,0]*1e3, '-')

data = np.loadtxt('trk.ref.996')
ax.plot(data[:,2], data[:,0]*1e3, '-')

ax.set_ylabel(r'$x$ (mm)')
ax.legend(['No correction', 'W/ a kick', 'W/ uniform fields'])

ax.grid()
ax.set_xlim(-1.8, 1.8)
ax.set_ylim(-25, 10)

fig.savefig('ref-trajectory-with-and-without-correction.png')
reset_margin()
#%%
data = pd_loadtxt('trk.ref.022')
ax[0].plot(data[:,2], data[:,0]*1e3, '-')

ax[0].set_ylabel(r'$x$ (mm)')
#ax[0].legend([r'$x$', r'$y$'])

ax[1].set_xlabel(r'$z$ (m)')
ax[1].set_ylabel(r'$B_y$ (mm)')
#ax[1].legend([r'$B_x$', r'$B_y$'])

ax[0].grid()
ax[1].grid()

fig.savefig('ref-trajectory-with-coil.png')

#%%
field_maps = r'\\afs\ifh.de\group\pitz\data\lixiangk\work\sync\field-maps'

gun3d = build_cavity(field_maps+os.sep+'gun42cavity.txt', 0, 60e6, 1.3e9, 224.94-180)
boo3d = build_cavity(field_maps+os.sep+'CDS14_15mm.txt', 2.675, 11.40e6, 1.3e9, 28.95-20)
sol3d = build_solenoid(field_maps+os.sep+'gunsolenoidsPITZ.txt', 0, 1)

em3d = EMsolver()
#em3d.add_external_field('gun', gun3d)
#em3d.add_external_field('booster', boo3d)
em3d.add_external_field('solenoid', sol3d)

a = np.random.rand(100000, 6)

#%%
from tracker.FieldMap3D import *

gun3d = RF3D(field_maps+os.sep+'3D_gun42cavity', amp0 = 60e6, z0 = 0, freq = 1.3e9, phi0 = 224.94-180)
boo3d = RF3D(field_maps+os.sep+'3D_CDS14_15mm', 11.40e6, 2.675, 1.3e9, 28.95-20)
sol3d = Magnet3D(field_maps+os.sep+'3D_gunsolenoidsPITZ', 0.2, 0)

em3d = EMsolver()
#em3d.add_external_field('gun', gun3d.EM3D)
#em3d.add_external_field('booster', boo3d.EM3D)
em3d.add_external_field('solenoid', sol3d.EM3D)

    
#%%
def und3DBoosted():
    
def deriv(t, y, B0y = 1.27, ku = 1, gamma_boost = 1):
    '''
    Calculate derivatives of many particles altogeter
    Parameters
      t: time/s
      y[0]: x/m
      y[1]: z/m
      y[2]: vx
      y[3]: vz
    Returns
      derivatives of y with respect to t
    '''
    
    beta_boost = gamma2beta(gamma_boost); #print(beta_boost)
    z_lab = gamma_boost*(y[1]+beta_boost*g_c*t)
    z1 = z_lab-0
    
    ku_z= ku*z1
    if ku_z<=np.pi:
        ss = 0.25
    elif ku_z<=2*np.pi:
        ss = 0.75
    else:
        ss = 1.00
        
    if ku_z>=(20+2)*np.pi and ku_z<(20+3)*np.pi:
        ss = 0.75
    elif ku_z>=(20+3)*np.pi and ku_z<=(20+4)*np.pi:
        ss = 0.25
    elif ku_z>(20+4)*np.pi:
        ss = 0
    #ss = 1
    
    #E1x = -gamma_boost*beta_boost*g_c*B0y*ss
    #B1y =  gamma_boost*B0y*ss
    
    B1y =  B0y*np.sin(ku_z)*ss
    
    beta = np.sqrt(y[2]**2+y[3]**2)/g_c
    gamma = beta2gamma(beta); #print(t*1e12, gamma, beta)
    
    cc0 =  g_qe/(gamma*g_me)*(gamma_boost*B1y)
    cc1 =  beta_boost*g_c
    
    dydt = np.zeros(y.shape)
    dydt[0] =  y[2]*g_c
    dydt[1] =  y[3]*g_c
    dydt[2] =  cc0*(y[3]+cc1)
    dydt[3] = -cc0*y[2]
    
    return dydt
#%%
t0, t1, dt = 0, 95e-12, 0.01e-12

gamma_boost = 12.25

lbf = LorentzBoost(gamma_boost)
ux, uy, uz = 0, 0, [17e6/g_mec2/1e6]
vx, vy, vz = momentum2velocity(ux, uy, uz)
vxp, vyp, vzp = lbf.velocity(vx, vy, vz)

y0 = [0, 0, vxp[0], vzp[0]]

betap = np.sqrt(vxp**2+vyp**2+vzp**2)/g_c
gammap = beta2gamma(betap)

B0y = 1.27
ku = 2*np.pi/3e-2
 
t_eval = np.linspace(t0, t1, int((t1-t0)/dt)+1)
solver = solve_ivp(deriv, [t0, t1], y0, t_eval = t_eval, max_step = dt,
                   args = (B0y, ku, gamma_boost))

sol = np.vstack((solver.y, solver.t)).T

fig, ax = plt.subplots(nrows = 3)
ax[0].plot(sol[:,1]*1e6, sol[:,0]*1e6, '-*')
ax[0].plot(sol[:,1]*1e6, sol[:,2]*7e-7, '-*')
ax[0].grid()

ax[1].plot(sol[:,1]*1e6, 1+sol[:,2]/g_c, '-')
ax[1].plot(sol[:,1]*1e6, sol[:,3]/g_c, '-')
ax[1].plot(sol[:,1]*1e6, np.sqrt(sol[:,2]**2+sol[:,3]**2)/g_c, '-')
ax[1].grid()

ax[2].plot(sol[:,-1]*1e12, sol[:,1]*1e3, '-')
ax[2].grid()


#%% 
B0y = 1.27
ku = 2*np.pi/3e-2
gamma_boost = 1 #12.284 # 12.284 for 1.27 Tesla
beta_boost = gamma2beta(gamma_boost); #print(beta_boost)

def EM3D(x, y, z, t=0, *args):
            
    if np.isscalar(x):
        x = [x]
    if np.isscalar(y):
        y = [y]
    if np.isscalar(z):
        z = [z]
    x = np.asarray(x).flatten()
    y = np.asarray(y).flatten()
    z = np.asarray(z).flatten()
    
    nn = np.max([x.size, y.size, z.size])
    
    if x.size == 1:
        x = np.ones(nn)*x[0]
    if y.size == 1:
        y = np.ones(nn)*y[0]
    if z.size == 1:
        z = np.ones(nn)*z[0]
    
    if not np.isscalar(t):
        t = np.asarray(t)
    
    #gamma_boost = 12.25
    beta_boost = gamma2beta(gamma_boost); #print(beta_boost)
    
    z_lab = gamma_boost*(z+beta_boost*g_c*t)
    z1 = z_lab-(3.6-3.39)/2
    
    ku_z= ku*z1
    ss = np.ones(z1.shape)
    
    ss[ku_z<=0] = 0
    
    ss[(ku_z>0)*(ku_z<=np.pi)] = 0.25
    ss[(ku_z>np.pi)*(ku_z<=2*np.pi)] = 0.75
    
    Nu = 113
    ss[(ku_z>=(Nu*2+2)*np.pi)*(ku_z<(Nu*2+3)*np.pi)] = 0.75
    ss[(ku_z>=(Nu*2+3)*np.pi)*(ku_z<(Nu*2+4)*np.pi)] = 0.25
    
    ss[(ku_z>=(Nu*2+4)*np.pi)] = 0
    
    #B0y = 1.27
    B0y = 1.27*np.sin(ku_z)*ss
    #B0y = 0
    
    E1x = -gamma_boost*beta_boost*g_c*B0y
    #E1y =  gamma_boost*beta_boost*g_c*B0x
    
    #B1x =  gamma_boost*B0x
    B1y =  gamma_boost*B0y
    #B1z =  B0z

    F2d = np.zeros((nn, 6))

    F2d[:, 0] = E1x
    #F2d[:, 1] = E1y
    
    #F2d[:, 3] = B1x
    F2d[:, 4] = B1y
    #F2d[:, 5] = B1z

    return F2d

from astra_modules import *

from tracker.SpaceCharge3DFFT import *
from tracker.Beam import *
from tracker.EMSolver import *
from tracker.Tracking2 import *

em3d = EMsolver()
em3d.add_external_field('und', EM3D)

track = Tracking(None, em3d, 160000e-12, 10e-12, dump_interval = 250,
                 Qtot = -1)
track.Run = 900

lbf = LorentzBoost(gamma_boost)
ux, uy, uz = 0.11825, 0, [17e6/g_mec2/1e6]
ux, uy, uz = 0, 0, [17e6/g_mec2/1e6]
vx, vy, vz = momentum2velocity(ux, uy, uz)
vxp, vyp, vzp = lbf.velocity(vx, vy, vz)
betap = np.sqrt(vxp**2+vyp**2+vzp**2)/g_c
gammap = beta2gamma(betap)

Pxp = vxp/g_c*gammap*g_mec2*1e6
Pzp = betap*gammap*g_mec2*1e6

res = track.ref([0, 0, 0, 0, 0, Pzp], 12000e-12, 0.115e-11)

#%%
sol = pd_loadtxt('trk.ref.900')
gg = M2G(np.sqrt(sol[:,3]**2+sol[:,4]**2+sol[:,5]**2)/1e6)
vx = sol[:,3]/1e6/g_mec2/gg*g_c
vy = sol[:,4]/1e6/g_mec2/gg*g_c
vz = sol[:,5]/1e6/g_mec2/gg*g_c
bb = np.sqrt(1-(vx**2+vy**2+vz**2)/g_c**2)
gg = beta2gamma(bb)

reset_margin(bottom = 0.1, top = 0.95, left = 0.1, right = 0.95)

fig, ax = plt.subplots(nrows = 3, sharex = False)
ax[0].plot(sol[:,2]*1e3, sol[:,0]*1e3, '-')
#ax[0].plot(sol[:,2]*1e6, sol[:,3], '-*')
ax[0].grid()

ax[1].plot(sol[:,2]*1e3, 1+vx/g_c, '-')
ax[1].plot(sol[:,2]*1e3, vz/g_c, '-')
ax[1].plot(sol[:,2]*1e3, np.sqrt(vx**2+vz**2)/g_c, '-')
ax[1].grid()

ax[2].plot(sol[:,6]*1e12, sol[:,2]*1e3, '-')
#ax[2].plot(sol[:,2]*1e3, gg, '-')
ax[2].grid()


lam_u = 3e-2
gammab = M2G(17)
K = undulator_parameter(B0y, lam_u)
lam_s = resonant_wavelength(K, lam_u, gammab)

lam_u_boost = lam_u/gamma_boost
lam_s_boost = lam_s*(1+beta_boost)*gamma_boost

ku_boost = 2*np.pi/lam_u_boost
ks_boost = 2*np.pi/lam_s_boost

phi = (ks_boost+ku_boost)*sol[:,2] -(ks_boost-beta_boost*ku_boost)*g_c*sol[:,6]
phi2 = (ks_boost-ku_boost)*sol[:,2] -(ks_boost+beta_boost*ku_boost)*g_c*sol[:,6]
#phi = (ks_boost+0)*sol[:,2]-(ks_boost)*g_c*sol[:,6]

ax2 = ax[2].twinx()
ax2.plot(sol[:,6]*1e12, phi, 'b-')
#ax2.plot(sol[:,6]*1e12, np.cos(phi2), 'g-')

#ax[0].set_xlim(0, 1)
#ax[1].set_xlim(0, 1)

#%%
ax[0].plot(data[:,2], data[:,0]*1e3, '-')
ax[1].plot(data[:,2], data[:,11], 'g-')

# data = pd_loadtxt('trk.ref.012')

# ax[0].plot(data[:,2], data[:,0]*1e3, '-')
#ax[1].plot(data[:,2], data[:,11], '-')

for i in [0, 1]:
    #ax[i].set_xlim(-1.8, 1.8)
    ax[i].set_xlabel(r'$z$ (m)')
    ax[i].grid()
#ax[0].set_ylim(-30, 10)
ax[1].set_ylim(-1.5, 1.5)
ax[0].set_ylabel(r'$x$ (mm)')
ax[1].set_ylabel(r'$B_y$ (mm)')
fig.tight_layout()
#fig.savefig('ref-trajectory-with-coil++.png')

#%%
def ponderomotivePhase(z, beta, k, ku = 0, phi0 = 0):
    t = z/beta/g_c
    return (k+ku)*z-k*g_c*t+phi0

gamma = M2G(17)
beta = gamma2beta(gamma)

lam_u = 3e-2
ku = 2*np.pi/lam_u

B = 1.27
K = undulator_parameter(B, lam_u)

beta_avg = 1-(1+K**2/2)/2/gamma**2

lam_s = lam_u*(1+K**2/2)/2/gamma**2
k = 2*np.pi/lam_s

z = np.linspace(0, 10*lam_u, 11)
phi = ponderomotivePhase(z, beta_avg, k, ku)

fig, ax = plt.subplots()
ax.plot(z/lam_u, phi/np.pi/2, '-*')
ax.grid()


#%%
from astra_modules import *

from tracker.SpaceCharge3DFFT import *
from tracker.Beam import *
from tracker.FieldMap3D import *
from tracker.EMSolver import *
from tracker.Tracking2 import *

gamma_boost = 12.25 #14 #.384
lbf = LorentzBoost(gamma_boost)
beta_boost = lbf.beta

basename = field_maps+os.sep+'3DDESY26-20x10mm-uniform'
und3d = StaticM3D(basename, z0 = 1.8-5e-3, gamma_boost = gamma_boost)

em3d = EMsolver()
em3d.add_external_field('und', und3d.EM3D)

track = Tracking(None, em3d, 10e-12, 1e-12, dump_interval = 250,
                 Qtot = -1)
track.Run = 2 # 12.25
track.Run = 3 # 12.514
track.Run = 4 # 12.514, zc = -10e-3

track.Run = 4 # 12.25, zc = -5e-3

ux, uy, uz = 0.11825, 0, [17e6/g_mec2/1e6]
ux, uy, uz = 0, 0, [17e6/g_mec2/1e6]

vx, vy, vz = momentum2velocity(ux, uy, uz)
vxp, vyp, vzp = lbf.velocity(vx, vy, vz)
betap = np.sqrt(vxp**2+vyp**2+vzp**2)/g_c
gammap = beta2gamma(betap)

Pxp = vxp/g_c*gammap*g_mec2*1e6
Pzp = betap*gammap*g_mec2*1e6

res = track.ref([0, 0, -5e-3, 0, 0, Pzp], 1200e-12, 0.2e-12)

#%%
plt.figure(); 

res = res0 # 0.2 ps
x, y, z, t = lbf.inverse_spacetime(res[:,0], res[:,1], res[:,2], res[:,6])
plt.plot(z, x*1e3, '-'); plt.xlim(0, 4.5); plt.grid()

res = res1 # 0.5 ps
x, y, z, t = lbf.inverse_spacetime(res[:,0], res[:,1], res[:,2], res[:,6])
plt.plot(z, x*1e3, '-'); plt.xlim(0, 4); plt.grid()

res = res2 # 1.0 ps
x, y, z, t = lbf.inverse_spacetime(res[:,0], res[:,1], res[:,2], res[:,6])
plt.plot(z, x*1e3, '-'); plt.xlim(0, 4); plt.grid()

#%%
sol = pd_loadtxt('trk.ref.%03d' % track.Run)
#sol = sol[0:1000]
gg = M2G(np.sqrt(sol[:,3]**2+sol[:,4]**2+sol[:,5]**2)/1e6)
vx = sol[:,3]/1e6/g_mec2/gg*g_c
vy = sol[:,4]/1e6/g_mec2/gg*g_c
vz = sol[:,5]/1e6/g_mec2/gg*g_c
bb = np.sqrt(1-(vx**2+vy**2+vz**2)/g_c**2)
gg = beta2gamma(bb)

reset_margin(bottom = 0.1, top = 0.95, left = 0.1, right = 0.9)

fig, ax = plt.subplots(nrows = 3, sharex = False, figsize = (9, 6))
ax[0].plot(sol[:,2]*1e3, sol[:,0]*1e3, '--')
#ax[0].plot(sol[:,2]*1e6, sol[:,3], '-*')
ax[0].grid()
ax[0].set_xlabel(r'$z$ (mm)')
ax[0].set_ylabel(r'$x$ (mm)')

ax[1].plot(sol[:,2]*1e3, 1+vx/g_c, '-')
ax[1].plot(sol[:,2]*1e3, vz/g_c, '-')
ax[1].plot(sol[:,2]*1e3, np.sqrt(vx**2+vz**2)/g_c, '-')
ax[1].grid()
ax[1].set_xlabel(r'$z$ (mm)')
ax[1].set_ylabel(r'1+$\beta_x$')
ax[1].set_ylim(-1, 2)
ax[1].legend([r'1+$\beta_x$', r'$\beta_z$', r'$\beta$'], ncol = 3,
             loc = 'upper left')

for axis in ax[0:2]:
    axis.set_xlim(4.1, 4.6)
    
ax[2].plot(sol[:,6]*1e12, sol[:,2]*1e3, '-')
#ax[2].plot(sol[:,2]*1e3, sol[:,6]*1e12, '-')
ax[2].plot([], [], '-')
#ax[2].plot(sol[:,2]*1e3, gg, '-')
ax[2].grid()
ax[2].set_xlabel(r'time (ps)')
ax[2].set_ylabel(r'$z$ (mm)')
ax[2].legend([r'$z$', r'$\phi$'])
ax[2].set_ylim(0, 20)

zz = np.linspace(-2, 2, 10000)
By = und3d.EM3D(0, 0, zz)[:,4]
B0y = np.max(By)/gamma_boost
#B0y = 1.2792

lam_u = 3e-2
gammab = M2G(17)

K = undulator_parameter(B0y, lam_u)
lam_s = resonant_wavelength(K, lam_u, gammab)
#lam_s = 100e-6

lam_u_boost = lam_u/gamma_boost
lam_s_boost = lam_s*(1+beta_boost)*gamma_boost

ku_boost = 2*np.pi/lam_u_boost
ks_boost = 2*np.pi/lam_s_boost

phi = (ks_boost+ku_boost)*sol[:,2]-(ks_boost-beta_boost*ku_boost)*g_c*sol[:,6]
phi2= (ks_boost-ku_boost)*sol[:,2]-(ks_boost+beta_boost*ku_boost)*g_c*sol[:,6]
#phi = (ks_boost+0)*sol[:,2]-(ks_boost)*g_c*sol[:,6]

ax2 = ax[2].twinx()
ax2.plot(sol[:,6]*1e12, phi/2/np.pi, 'b-')
#ax2.plot(sol[:,6]*1e12, np.cos(phi2), 'g-')

ax2.set_ylabel(r'$\phi/2\pi$')
ax2.set_ylim(0, 10)

fig.tight_layout()
fig.savefig('ref-in-undulator@%03d.png' % track.Run)


#%% Tracking samples in the boosted frame

from astra_modules import *

from tracker.SpaceCharge3DFFT import *
from tracker.Beam import *
from tracker.FieldMap3D import *
from tracker.EMSolver import *
from tracker.Tracking2 import *

gamma_boost = 12.514
lbf = LorentzBoost(gamma_boost)
beta_boost = lbf.beta

basename = field_maps+os.sep+'3DDESY26-20x10mm-uniform'
und3d = StaticM3D(basename, z0 = 1.8-5e-3, gamma_boost = gamma_boost)

em3d = EMsolver()
em3d.add_external_field('und', und3d.EM3D)

#%%
beamdist = str.format('../beam_und_250k_ham_sampled2.warp')
dist = np.loadtxt(beamdist)
x, y, z, ux, uy, uz = dist[:,0:6].T[:]

z += -5e-3

xp, yp, zp, tp = lbf.spacetime(x, y, z, 0)

vx, vy, vz = momentum2velocity(ux, uy, uz)
vxp, vyp, vzp = lbf.velocity(vx, vy, vz)

xp -= tp*vxp
yp -= tp*vyp
zp -= tp*vzp

uxp, uyp, uzp = velocity2momentum(vxp, vyp, vzp)

nps = len(xp)
dist_boost = np.vstack((xp, yp, zp, uxp*g_mec2*1e6, uyp*g_mec2*1e6, uzp*g_mec2*1e6,
                  np.zeros(nps), np.ones(nps)*1e-3, np.arange(nps), np.ones(nps)*5))
dist_boost = dist_boost.T

#fig, ax = plt.subplots()
#ax.plot(zp, xp, '.')
#ax.hist(uzp)

beam = Beam(dist = dist_boost, Qtot = 1e-3, debug = 0)
nop = len(beam.dist)

track = Tracking(beam, em3d, 1500e-12, 0.05e-12, dump_interval = 5000,
                 Qtot = -1)
track.Run = 103 # 12.25

screens = np.linspace(0, 3.6, 25)
boostdiag = BoostedDiagnostics(screens, lbf, track.Run)
#%%
n_steps = 30000
n_stepped = 0
stime = 0
zmean = 0

import gc
import psutil
import timeit

proc = psutil.Process(os.getpid())

while n_stepped<n_steps and zmean < 3.6:
    time1 = timeit.default_timer()
    #import pdb; pdb.set_trace()
    if track.is_emit():
        flag = 'Emitting'
        nstep = 1
        
        nz = 64
        kwargs = dict(nz = nz, Lemit = True, Lmirror = True)
        if n_stepped % dump_interval == 0:
            kwargs = dict(nz = nz, Lemit = True, Lmirror = True, fig_ext = '.%04d.%03d.eps' % (n_stepped, Run))
            
        if track.Lspch is True and track.stepped>1:
            sc3d = track.beam.get_sc3d(**kwargs)
            track.em3d.update_external_field('spacecharge', sc3d)
            #track.sc3d = sc3d
            track.cathode(['spacecharge', 'gun'], 'trk.cathode')
        
        track.emit(nstep, NMIN = Nmin, **kwargs)
    else:
        flag = 'Tracking'
        nstep = 1
        
        nz = 32
        kwargs = dict(nz = nz, Lemit = False, Lmirror = False)
        #if n_stepped % 500 == 0:
        #    kwargs = dict(nz = nz, Lemit = False, Lmirror = True, fig_ext = '.%04d.%03d.eps' % (n_stepped, Run))
        
        if track.Lspch is True and track.stepped>1:
            #if sc3d in locals():
            #    del sc3d
            sc3d = track.beam.get_sc3d(**kwargs)
            track.em3d.update_external_field('spacecharge', sc3d)
            track.cathode(['spacecharge', 'gun'], 'trk.cathode')
            
        #track.dump_interval = 500
        
        track.step3(nstep, NMAX = 50000, **kwargs)

        boostdiag.check(track.beam, track.time-track.dt, track.time)
        
    plt.close('all')
    
    gc.collect()
    mem0 = proc.memory_info().rss
    
    zmean = track.zmean
    for i, scr in enumerate(screens):
        if np.abs(zmean>=scr):
            track.dump()
            screens = np.delete(screens, i)
    
    time2 = timeit.default_timer()
    stime = stime+(time2-time1)

    n_stepped += nstep

    if n_stepped % 100 == 0:
        print('Current memory: ', mem0/1e9)
        print('Current -> ', flag, ': on average 1 step takes: ', stime/n_stepped, ' after ', n_stepped, ' steps.')

track.dump()
print('Total time:', stime)

#%% Interpolation of particles

n_dump = 60
dump_steps = np.arange(200, 12001, 200)

nrows, ncols = dist_boost.shape
dist_all = np.zeros((n_dump, nrows, ncols))

for i, step in enumerate(dump_steps):
    print(i)
    fname = 'trk.%06d.%03d' % (step, track.Run)
    
    # ### replace some text in the data file
    # with open (fname, "r") as myfile:
    #     temp=myfile.readlines()
    
    # for j, tt in enumerate(temp):
    #     temp[j] = tt.replace('4.9237E+02', '4.9237E+02 ')
    
    # with open (fname+'_', "w") as myfile:
    #     for j, tt in enumerate(temp):
    #         myfile.write(tt)
    # ####   
    
    temp = np.loadtxt(fname+'_')
    temp[1:,2] += temp[0,2]
    temp[1:,5] += temp[0,5]
    temp[:,6] = step*0.1e-12
    
    xp, yp, zp, pxp, pyp, pzp, tp = temp[:,0:7].T[:]
    x, y, z, t = lbf.inverse_spacetime(xp, yp, zp, tp)
    
    vxp, vyp, vzp = momentum2velocity(pxp, pyp, pzp, g_mec2*1e6)
    vx, vy, vz = lbf.inverse_velocity(vxp, vyp, vzp)
    px, py, pz = velocity2momentum(vx, vy, vz, g_mec2*1e6)
    
    dist_all[i, :, :7] = np.vstack((x, y, z, px, py, pz, t)).T[:]
    
#%%
for i in range(37):
    i *= 10
    cmd = 'cat warp.%04d.001.* > warp.%04d.001' % (i, i)
    os.system(cmd)
    print(cmd)
#%%
fname = 'beam_und_250k_ham.warp'
fname = get_file()
temp = pd_loadtxt(fname)
x, y, z, ux, uy, uz, t = temp[:,0:7].T[:]
gamma = np.sqrt(1+ux**2+uy**2+uz**2)
z = -(t-np.mean(t))*uz/gamma*g_c

# lbf = LorentzBoost(12.25)
# x, y, z, t = lbf.inverse_spacetime(temp[:,0], temp[:,1], temp[:,2], 0)
# ux, uy, uz = lbf.inverse_momentum(temp[:,3], temp[:,4], temp[:,5])
# gamma = np.sqrt(1+ux**2+uy**2+uz**2)
# z = z-t*uz/gamma*g_c
# x = x-t*ux/gamma*g_c
# y = y-t*uy/gamma*g_c

# plt.figure()
# #plt.hist(z0, 100, histtype = r'step')
# plt.plot(z0, x0, '.')

fig, ax = plt.subplots(ncols = 2, nrows = 2, figsize = (6, 6))
ax = ax.flatten()
#plt.hist(temp[:,0], 100, histtype = r'step', density = True)
ax[0].plot(x, y, '.')

ax[1].plot(z, x*1e3, '.')
ax[1].plot(z, y*1e3, '.')

ax[2].plot(x, ux, '.')
ax[2].plot(y, uy, '.')

ax[3].plot(z, uz, '.')

for axis in ax:
    axis.grid()
    
#%%
def print_all(f_h5):
    try:
        for key in f_h5.keys():
            print('- %s' % key)
            for subkey in f_h5.get(key).keys():
                #print(' - ')
                print_all(f_h5.get(key))
    except Exception:
        pass
#%%
from astra_modules import *
from tracker.FieldMap3D import *

gamma_boost = 12.514
lbf = LorentzBoost(gamma_boost)
beta_boost = lbf.beta

basename = field_maps+os.sep+'3DDESY26-20x10mm-uniform'
und3d = StaticM3D(basename, z0 = 1.8-5e-3, gamma_boost = gamma_boost)
#%%
from universal import *
import h5py

for it in range(1, 16, 100):
    it *= 20
    f = h5py.File('data%08d.h5' % it, 'r')

    x = f['data/%d/particles/beam/position/x' % it][:]
    y = f['data/%d/particles/beam/position/y' % it][:]
    z = f['data/%d/particles/beam/position/z' % it][:]
    
    ux = f['data/%d/particles/beam/momentum/x' % it][:]/g_me/g_c
    uy = f['data/%d/particles/beam/momentum/y' % it][:]/g_me/g_c
    uz = f['data/%d/particles/beam/momentum/z' % it][:]/g_me/g_c
    gamma = np.sqrt(1+ux**2+uy**2+uz**2)
    
    Ex = f['data/%d/particles/beam/E/x' % it][:]
    Ey = f['data/%d/particles/beam/E/y' % it][:]
    Ez = f['data/%d/particles/beam/E/z' % it][:]
    
    By = f['data/%d/particles/beam/B/y' % it][:]
    
    #Jx = f['data/%d/particles/beam/J/x' % it][:]
    #Jy = f['data/%d/particles/beam/J/y' % it][:]
    #Jz = f['data/%d/particles/beam/J/z' % it][:]
    
    #Rho = f['data/%d/fields/rho' % it][...]
    Ex = f['data/%d/fields/E/x' % it][...]
    
    start = f['data/%d/fields/E' % it].attrs.get('gridGlobalOffset')
    step = f['data/%d/fields/E' % it].attrs.get('gridSpacing')
    ngrid = Ex.shape
    end = start + step*ngrid
    zgrid = np.linspace(start[-1], end[-1], ngrid[-1])
    
    time = f['data/%d' % it].attrs['time']
    
    ExU, EyU, EzU, BxU, ByU, BzU = und3d.EM3D(x, y, z, time).T[:]
    
    thetitle = 'it = %d' % it
    
    fig, ax = plt.subplots(nrows = 2, ncols = 2, figsize = (10, 10))
    ax = ax.flatten()
    
    #ax[0].plot(z*1e3, x*1e3, '.'); #ax[0].set_ylim(-6, 6)
    ax[0].hist(z*1e3, 400, histtype = r'step')
    ax[0].plot(zgrid*1e3, -np.sum(np.sum(Rho, axis = 0), axis = 0)*1400/30*4, '-*')
    
    ax[0].set_title(thetitle)   
    #ax[1].hist(x*1e3, 100, histtype = r'step')
    
    ax[1].plot(x*1e3, ux/gamma, '.')
    ax[1].plot(y*1e3, uy/gamma, '.')
    #ax[1].plot(z*1e3, By/12.25, '.') 
    
    ax[2].plot(z*1e3, Ex-ExU, '.'); #ax[3].set_ylim(-500, 500)
    
    ax[3].plot(x*1e3, Ex-ExU, '.'); #ax[2].set_ylim(-500, 500)  
    ax[3].plot(x*1e3, (By-ByU)*g_c, '.'); #ax[2].set_ylim(-500, 500) 
    
    for i in range(4):
        ax[i].grid()
    
    fig.savefig('%d-overview.png' % it)
    
    f.close()
        
#%%
for it in np.arange(10, 361, 10):
    fname = 'warp.%04d.001' % it
    
    file = open(fname, 'r')
    line = file.readline()
    
    with open(fname+'_', 'a') as f_handle:
        while True:
            if not line:
                break
            if len(line.split())==7:
                f_handle.write(line)
            line = file.readline()
    
    file.close()
    
#%%
for it in np.arange(0, 361, 20):
    fname = 'warp.%04d.001' % it
    
    try:
        dist = pd_loadtxt(fname, error_bad_lines = False)
        #select = dist[:,5]>30
        
        diag = BeamDiagnostics(dist = dist, tracking = 'warp')
        std_z = np.std(dist[:,6])*g_c*1e3
        diag.x[5] = std_z
        
        with open('beam.dat', 'a') as f_handle:
            np.savetxt(f_handle, np.atleast_2d(diag.x), fmt = '%14.6E')
    except Exception as err:
        print(err)
        
#%% refine the grid size
Run = 100
beam_zc = -5e-3
reset_margin(bottom = 0.05, top = 0.95)
fig, [ax1, ax2, ax3] = plt.subplots(nrows = 3, figsize = (6, 9), sharex = True)
data = np.loadtxt('../ast.Xemit.%03d' % Run)
ax1.plot(data[:,0]+beam_zc, data[:,3], '-')

data = np.loadtxt('../ast.Yemit.%03d' % Run)
ax2.plot(data[:,0]+beam_zc, data[:,3], '-')

data = np.loadtxt('../ast.Zemit.%03d' % Run)
ax3.plot(data[:,0]+beam_zc, data[:,3], '-')

# data = np.loadtxt('beam.dat')
# ax1.plot(data[:,0], data[:,3])
# ax2.plot(data[:,0], data[:,4])
# ax3.plot(data[:,0], data[:,5])

# data = np.loadtxt('../2x2x2_13/beam.dat')
# ax1.plot(data[:,0], data[:,3])
# ax2.plot(data[:,0], data[:,4])
# ax3.plot(data[:,0], data[:,5])

data = np.loadtxt('../2x2x2_13/beam.dat'); start = 2
ax1.plot(data[:,0+start], data[:,3+start])
ax2.plot(data[:,0+start], data[:,4+start])
ax3.plot(data[:,0+start], data[:,5+start])

data = np.loadtxt('../4x2x2_1/beam.dat'); start = 2
ax1.plot(data[:,0+start], data[:,3+start])
ax2.plot(data[:,0+start], data[:,4+start])
ax3.plot(data[:,0+start], data[:,5+start])

data = np.loadtxt('../8x2x2_1/beam.dat'); start = 2
ax1.plot(data[:,0+start], data[:,3+start])
ax2.plot(data[:,0+start], data[:,4+start])
ax3.plot(data[:,0+start], data[:,5+start])

data = np.loadtxt('../16x2x2_1_1/beam.dat'); start = 2
ax1.plot(data[:,0+start], data[:,3+start])
ax2.plot(data[:,0+start], data[:,4+start])
ax3.plot(data[:,0+start], data[:,5+start])

data = np.loadtxt('./beam.dat'); start = 2
ax1.plot(data[:,0+start], data[:,3+start])
ax2.plot(data[:,0+start], data[:,4+start])
ax3.plot(data[:,0+start], data[:,5+start])

ax3.set_xlabel(u_z)
ax1.set_ylabel(u_rms_x)
ax2.set_ylabel(u_rms_y)
ax3.set_ylabel(u_rms_z)

ax1.grid()
ax2.grid()
ax3.grid()
ax2.set_ylim(0, 0.4)
ax3.set_ylim(1.75, 2.25)

ax1.legend(['Astra']+['$n_z/\lambda$ = %.0f' % c for c in [2, 4, 8, 16, 24]],
           ncol = 2)
fig.savefig('rms-z.png')
reset_margin()
#%% refine the time step
Run = 100
beam_zc = -5e-3
reset_margin(bottom = 0.05, top = 0.95)
fig, [ax1, ax2, ax3] = plt.subplots(nrows = 3, figsize = (6, 9), sharex = True)
data = np.loadtxt('../ast.Xemit.%03d' % Run)
ax1.plot(data[:,0]+beam_zc, data[:,3], '-')

data = np.loadtxt('../ast.Yemit.%03d' % Run)
ax2.plot(data[:,0]+beam_zc, data[:,3], '-')

data = np.loadtxt('../ast.Zemit.%03d' % Run)
ax3.plot(data[:,0]+beam_zc, data[:,3], '-')

# data = np.loadtxt('beam.dat')
# ax1.plot(data[:,0], data[:,3])
# ax2.plot(data[:,0], data[:,4])
# ax3.plot(data[:,0], data[:,5])

# data = np.loadtxt('../2x2x2_13/beam.dat')
# ax1.plot(data[:,0], data[:,3])
# ax2.plot(data[:,0], data[:,4])
# ax3.plot(data[:,0], data[:,5])

data = np.loadtxt('../24x2x2_1/beam.dat'); start = 2
ax1.plot(data[:,0+start], data[:,3+start])
ax2.plot(data[:,0+start], data[:,4+start])
ax3.plot(data[:,0+start], data[:,5+start])

data = np.loadtxt('../24x2x2_5/beam.dat'); start = 2
ax1.plot(data[:,0+start], data[:,3+start])
ax2.plot(data[:,0+start], data[:,4+start])
ax3.plot(data[:,0+start], data[:,5+start])

data = np.loadtxt('../24x2x2_4/beam.dat'); start = 2
ax1.plot(data[:,0+start], data[:,3+start])
ax2.plot(data[:,0+start], data[:,4+start])
ax3.plot(data[:,0+start], data[:,5+start])

data = np.loadtxt('../24x2x2_2/beam.dat'); start = 2
ax1.plot(data[:,0+start], data[:,3+start])
ax2.plot(data[:,0+start], data[:,4+start])
ax3.plot(data[:,0+start], data[:,5+start])

data = np.loadtxt('./beam.dat'); start = 2
ax1.plot(data[:,0+start], data[:,3+start])
ax2.plot(data[:,0+start], data[:,4+start])
ax3.plot(data[:,0+start], data[:,5+start])

ax3.set_xlabel(u_z)
ax1.set_ylabel(u_rms_x)
ax2.set_ylabel(u_rms_y)
ax3.set_ylabel(u_rms_z)

ax1.grid()
ax2.grid()
ax3.grid()
ax2.set_ylim(0, 0.4)
ax3.set_ylim(1.75, 2.25)

ax1.legend(['Astra']+['dtcoef = %.2f' % c for c in 
                      [0.73, 0.70, 0.65, 0.60, 0.55]],
           ncol = 2)
#fig.savefig('rms-z.png')
reset_margin()

#%% include the waveguide
Run = 100
beam_zc = -5e-3
reset_margin(bottom = 0.05, top = 0.95)
fig, [ax1, ax2, ax3] = plt.subplots(nrows = 3, figsize = (6, 9), sharex = True)
data = np.loadtxt('../ast.Xemit.%03d' % Run)
ax1.plot(data[:,0]+beam_zc, data[:,3], '-')

data = np.loadtxt('../ast.Yemit.%03d' % Run)
ax2.plot(data[:,0]+beam_zc, data[:,3], '-')

data = np.loadtxt('../ast.Zemit.%03d' % Run)
ax3.plot(data[:,0]+beam_zc, data[:,3], '-')

# data = np.loadtxt('beam.dat')
# ax1.plot(data[:,0], data[:,3])
# ax2.plot(data[:,0], data[:,4])
# ax3.plot(data[:,0], data[:,5])

# data = np.loadtxt('../2x2x2_13/beam.dat')
# ax1.plot(data[:,0], data[:,3])
# ax2.plot(data[:,0], data[:,4])
# ax3.plot(data[:,0], data[:,5])

data = np.loadtxt('../24x2x2_2/beam.dat'); start = 2
ax1.plot(data[:,0+start], data[:,3+start])
ax2.plot(data[:,0+start], data[:,4+start])
ax3.plot(data[:,0+start], data[:,5+start])

data = np.loadtxt('../24x2x2_6/beam.dat'); start = 2
ax1.plot(data[:,0+start], data[:,3+start])
ax2.plot(data[:,0+start], data[:,4+start])
ax3.plot(data[:,0+start], data[:,5+start])


data = np.loadtxt('../24x2x2_7/beam.dat'); start = 2
ax1.plot(data[:,0+start], data[:,3+start])
ax2.plot(data[:,0+start], data[:,4+start])
ax3.plot(data[:,0+start], data[:,5+start])

data = np.loadtxt('../24x2x2_9/beam.dat'); start = 2
ax1.plot(data[:,0+start], data[:,3+start])
ax2.plot(data[:,0+start], data[:,4+start])
ax3.plot(data[:,0+start], data[:,5+start])

ax3.set_xlabel(u_z)
ax1.set_ylabel(u_rms_x)
ax2.set_ylabel(u_rms_y)
ax3.set_ylabel(u_rms_z)

ax1.grid()
ax2.grid()
ax3.grid()
ax2.set_ylim(0, 0.4)
ax3.set_ylim(1.75, 2.25)

ax1.legend(['Astra']+['no WG', 'WG setup 1', 'WG setup 2', 'WG setup 3'],
           ncol = 2)
fig.savefig('rms-z.png')
reset_margin()
#%%

fig, [ax1, ax2, ax3] = plt.subplots(nrows = 3, figsize = (6, 6), sharex = True)
data = np.loadtxt('./ast.Xemit.%03d' % 99)
ax1.plot(data[:,0], data[:,3], '-')

data = np.loadtxt('1x2x2_61/beam.dat')
ax1.plot(data[::10,0], data[::10,3], 'D')

data = np.loadtxt('./ast.Xemit.%03d' % 103)
ax2.plot(data[:,0], data[:,3], '-')

data = np.loadtxt('2x2x2_12/beam.dat')
ax2.plot(data[:,0], data[:,3], 'D')

data = np.loadtxt('./ast.Xemit.%03d' % 100)
ax3.plot(data[:,0], data[:,3], '-')

data = np.loadtxt('2x2x2_13/beam.dat')
ax3.plot(data[:,0], data[:,3], 'D')

ax3.set_xlabel(u_z)

for axis in [ax1, ax2, ax3]:
    axis.grid()
    axis.set_ylim(0, 3)
    axis.set_ylabel(u_rms_x)
    axis.legend(['Astra', 'Warp'], loc = 'upper left')
ax1.set_title('Case 1: w/ undulator, w/o charge ')
ax2.set_title('Case 2: w/o undulator, w/ charge')
ax3.set_title('Case 3: w/ both undulator and charge')
fig.tight_layout()
fig.savefig('rms_x-z-astra-and-warp.png')
#%%
ref = []
for it in np.arange(0, 361, 1):
    fname = 'warp.%04d.001' % it
    
    dist = np.loadtxt(fname)
    select = dist[:,1] == 0
    
    ref.append(dist[select][0])

ref = np.array(ref)

fig, ax = plt.subplots()
ax.plot(ref[:,2], ref[:,0]*1e3, '-D')

ref0 = np.loadtxt('../shotnoise_test/trk.ref.001')
ax.plot(ref0[:,2]-5e-6, ref0[:,0]*1e3, '-')
ax.grid()

#%%
data = pd_loadtxt('beam_und_250k_ham.warp')
lbf = LorentzBoost(12.25)
xp, yp, zp, tp = lbf.spacetime(data[:,0], data[:,1], data[:,2]-5e-3, 0)
uxp, uyp, uzp = lbf.momentum(data[:,3], data[:,4], data[:,5], unit = 1)

gammap = np.sqrt(1+uxp**2+uyp**2+uzp**2)
zp0 = zp - tp*uzp/gammap*g_c

dist_boosted = np.vstack((xp, yp, zp0, uxp, uyp, uzp, data[:,6])).T
np.savetxt('beam_und_250k_ham_boosted.warp', dist_boosted, fmt = '%16.9E')
plt.figure()
plt.plot(dist_boosted[:,2], dist_boosted[:,0], '.')

# plt.figure()
# plt.hist(xp, 100, histtype = r'step')
# plt.hist(xp+zp/(uzp/gammap*g_c)*uxp/gammap*g_c, 100, histtype = r'step')

#%%
fig, ax = plt.subplots(nrows = 2)

data = np.loadtxt('2x2x2_1/lab_diags/beam.dat')
ax[0].plot(data[:,2], data[:,12], '-*')
#ax[1].plot(data[:,0], data[:,4], '-*')

#data = np.loadtxt('beam3.dat')
#ax[0].plot(data[:,0], data[:,3], '-*')
#ax[1].plot(data[:,0], data[:,4], '-*')


#%% FLASH RT kicker

def EM3D(x, y, z, t=0, *args):
            
    if np.isscalar(x):
        x = [x]
    if np.isscalar(y):
        y = [y]
    if np.isscalar(z):
        z = [z]
    x = np.asarray(x).flatten()
    y = np.asarray(y).flatten()
    z = np.asarray(z).flatten()
    
    nn = np.max([x.size, y.size, z.size])
    
    if x.size == 1:
        x = np.ones(nn)*x[0]
    if y.size == 1:
        y = np.ones(nn)*y[0]
    if z.size == 1:
        z = np.ones(nn)*z[0]
    
    if not np.isscalar(t):
        t = np.asarray(t)
        
    z1 = z #-z0
    
    L0 = 1
    Ex = np.array([1e6 for _ in z1])
    Ex= np.where((z1>=0)*(z1<=L0), Ex, 0)
    
    F2d = np.zeros((nn, 6))
    F2d[:,0] = Ex 
    
    return F2d

from astra_modules import *

from tracker.SpaceCharge3DFFT import *
from tracker.Beam import *
from tracker.EMSolver import *
from tracker.Tracking2 import *

em3d = EMsolver()
em3d.add_external_field('kicker', EM3D)

track = Tracking(None, em3d, 1000e-12, 10e-12, dump_interval = 250, Qtot = -1)

track.Run = 1

res = track.ref([0, 0, 0, 0, 0, 15e6], 6800e-12, 5e-12)

reset_margin(bottom = 0.1, top = 0.95, left = 0.1, right = 0.95)

fig, ax = plt.subplots(nrows = 2, figsize = (7.2, 5.4))

# data = pd_loadtxt('trk.ref.001')
# ax[0].plot(data[:,2], data[:,0]*1e3, '-')
# ax[1].plot(data[:,2], data[:,11], '-')

data = pd_loadtxt('trk.ref.%03d' % track.Run)
ax[0].plot(data[:,2], data[:,5]*1e0, '-')
ax[1].plot(data[:,2], data[:,7]/1e6, 'g-')

for i in [0, 1]:
    #ax[i].set_xlim(-1.8, 1.8)
    ax[i].set_xlabel(r'$z$ (m)')
    ax[i].grid()
#ax[0].set_ylim(-30, 10)
ax[1].set_ylim(-1.5, 1.5)
ax[0].set_ylabel(r'$x$ (mm)')
ax[1].set_ylabel(r'$E_x$ (MV/m)')
fig.tight_layout()
#fig.savefig('ref-trajectory-with-coil++.png')

reset_margin()

#%%
from transport.TransferMatrix import *

#FitSlopeOfRef('trk.ref.001', xmin = 1, index_x = 2, index_y = 0, scale_x = 1)
FitSlopeOfRef('ast.ref.004', xmin = 6.2)


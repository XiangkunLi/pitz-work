# -*- coding: utf-8 -*-
"""
Created on Thu Mar 25 09:06:57 2021

@author: lixiangk
"""

from universal import *
from scipy.integrate import solve_ivp, RK45

def __convert2array__(x, *args):
    '''
    Convert input arguments to 1D arrays

    Parameters
    ----------
    x : scalar or 1D vector
    *args : same as x

    Returns
    -------
    
        n by (1+len(args)) arrays, with n the maximum dimension of the inputs

    '''
    ndims = -np.ones(1+len(args))
    
    if not np.isscalar(x):    
        ndims[0] = np.asarray(x).size
    
    if len(args)>0:
        for i, v in enumerate(args):
            if not np.isscalar(v):
                ndims[1+i] = np.asarray(v).size
    ndims = np.array(ndims, dtype = np.int)
    
    if np.sum(ndims)+len(ndims) == 0:
        ndims = -ndims
    
    r = np.zeros((np.max(ndims), 1+len(args)))
    
    if ndims[0] == -1:
        r[:,0] = x
    else:
        r[:ndims[0],0] = x
    
    for i, d in enumerate(ndims[1:]):
        if d == -1:
            r[:,1+i] = args[i]
        else:
            r[:d,1+i] = args[i]
    
    return r.T[:]

def velocity2momentum(vx, vy, vz, unit = 1):
    '''
    Parameters
    ----------
    vx : float or 1D
        Horizontal velocity, m/s.
    vy : float or 1D
        Vertical velocity, m/s.
    vz : float or 1D
        Longitudinal velocity, m/s.
    unit : float, optional
        Unit of momentum, to be scaled to the unitless momentum. 
        The default is 1, which means returning unitless momentum.

    Returns
    -------
    ux : 1D
        Horimontal momentum.
    uy : 1D
        Vertical momentum.
    uz : 1D
        Longitudinal momentum.

    '''
    
    vx, vy, vz = __convert2array__(vx, vy, vz)
    
    beta = np.sqrt(vx*vx+vy*vy+vz*vz)/g_c
    gamma = 1.0/np.sqrt(1.0-beta*beta)
    
    ux = vx/g_c*gamma*unit # ux, uy, uz are scaled to unit, e.g., g_mec2*1e6
    uy = vy/g_c*gamma*unit
    uz = vz/g_c*gamma*unit
    
    return ux, uy, uz

def momentum2velocity(ux, uy, uz, unit = 1):
    
    ux, uy, uz = __convert2array__(ux, uy, uz)
    
    ux, uy, uz = ux/unit, uy/unit, uz/unit # ux, uy, uz are unitless
    
    gamma = np.sqrt(1+ux*ux+uy*uy+uz*uz)
    
    vx = ux*g_c/gamma
    vy = uy*g_c/gamma
    vz = uz*g_c/gamma
    
    return vx, vy, vz

class LorentzBoost():
    
    def __init__(self, gamma = 1):
        
        self.gamma = gamma
        self.beta  = gamma2beta(gamma)
        
    def spacetime(self, x, y, z, t = 0, gamma = None):
        
        if gamma is not None:
            gamma = gamma
        else:
            gamma = self.gamma
        
        beta  = gamma2beta(gamma)
        
        x, y, z, t = __convert2array__(x, y, z, t)
        
        xp = x
        yp = y
        zp = gamma*(z-beta*g_c*t)
        tp = gamma*(t-beta*z/g_c)
        
        return xp, yp, zp, tp
    
    def inverse_spacetime(self, xp, yp, zp, tp = 0, gamma = None):
        
        if gamma is not None:
            gamma = gamma
        else:
            gamma = self.gamma
        
        beta  = gamma2beta(gamma)
        
        xp, yp, zp, tp = __convert2array__(xp, yp, zp, tp)
        
        x = xp
        y = yp
        z = gamma*(zp+beta*g_c*tp)
        t = gamma*(tp+beta*zp/g_c)
        
        return x, y, z, t
    
    def momentum(self, ux, uy, uz, unit = 1):
        
        vx, vy, vz = momentum2velocity(ux, uy, uz, unit)
        vxp, vyp, vzp = self.velocity(vx, vy, vz)
        uxp, uyp, uzp = velocity2momentum(vxp, vyp, vzp, unit)
        
        return uxp, uyp, uzp
    
    def inverse_momentum(self, uxp, uyp, uzp, unit = 1):
        
        vxp, vyp, vzp = momentum2velocity(uxp, uyp, uzp, unit)
        vx, vy, vz = self.inverse_velocity(vxp, vyp, vzp)
        ux, uy, uz = velocity2momentum(vx, vy, vz, unit)
        
        return ux, uy, uz
        
    def velocity(self, vx, vy, vz, gamma = None):
    
        if gamma is not None:
            gamma = gamma
        else:
            gamma = self.gamma
        
        beta  = gamma2beta(gamma)
        
        vx, vy, vz = __convert2array__(vx, vy, vz)
        
        cc = 1.0/(1.0-beta*vz/g_c)
        
        vxp = cc*vx/gamma
        vyp = cc*vy/gamma
        vzp = cc*(vz-beta*g_c)
        
        return vxp, vyp, vzp
    
    def inverse_velocity(self, vxp, vyp, vzp, gamma = None):
        
        if gamma is not None:
            gamma = gamma
        else:
            gamma = self.gamma
        
        beta  = gamma2beta(gamma)
        
        vxp, vyp, vzp = __convert2array__(vxp, vyp, vzp)
        
        cc = 1.0/(1.0+beta*vzp/g_c)
        
        vx = cc*vxp/gamma
        vy = cc*vyp/gamma
        vz = cc*(vzp+beta*g_c)
        
        return vx, vy, vz
    
    def copropagating_length(self, L, u, v):
        '''
        Parameters
          L: length measured in a static reference frame of F
          u: velecity of the copropagating object in the static reference frame of F
          v: velocity of the moving frame of F' in the direction of z in the frame of F
        Returns
          L': length measured in the moving frame of F'
        '''
        beta = v/g_c
        gamma = beta2gamma(beta)
        return L/gamma/(1.-u/g_c*beta)
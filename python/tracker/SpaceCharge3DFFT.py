from universal import *

from scipy.interpolate import RegularGridInterpolator, Rbf
from numpy.fft import fftn, ifftn
import inspect

print('Importing SpaceCharge3DFFT ...')

# Static potential and electric field of a uniformly charged ball
def static_potential(r, R, Q = 100e-12):
    cc = Q/4./np.pi/g_eps0
    if r<=R:
        phi = cc*((R**2-r**2)/2./R**3+1./R)
    else:
        phi = cc/r
    return phi

def electric_field(r, R, Q = 100e-12):
    cc = Q/4./np.pi/g_eps0
    if r<=R:
        Er = cc*r/R**3
    else:
        Er = cc/r**2
    return Er

'''
R = 1e-3
r = np.linspace(0, 15*R, 501)
phi = np.array([static_potential(x, R) for x in r])
Er = np.array([electric_field(x, R) for x in r])

fig, [ax1, ax2] = plt.subplots(figsize = (12, 4), ncols = 2)
ax1.plot(r/R, phi, '-')
ax1.grid()
ax2.plot(r/R, Er, '-')
ax2.grid()

np.savetxt('R1mm.dat', np.array([r, phi, Er]).T, fmt = '%12.6E')
'''

# ## Class-level scripts
def GF3D(xyz):
    '''
    Green function (GF) in 3D cartesian CS
    '''
    (x, y, z) = xyz
    r = np.sqrt(x*x+y*y+z*z)
    return np.where(r>0, 1./np.sqrt(x*x+y*y+z*z), 1e100)

def IGF3D(xyz):
    '''
    Indefinite integral of or integrated green function in 3D cartesian CS, according to Eq.(2) on 129901-2. 
    Here, `indefinite` refers to the integrating limits
    ''' 
    (x, y, z) = xyz
    r = np.sqrt(x*x+y*y+z*z)
    
    #EPS = 1e-12
    #if abs(r) < EPS:
    #    return 0
    #if abs(x) < EPS:
    #    return y*z*np.log(r)
    #if abs(y) < EPS:
    #    return z*x*np.log(r)
    #if abs(z) < EPS:
    #    return x*y*np.log(r)
    EPS = 1e-9
    x2 = x*x; y2 = y*y; z2 = z*z
    xy = x*y; yz = y*z; zx = z*x
    s = yz*np.log(x+r)+zx*np.log(y+r)+xy*np.log(z+r)
    a = -0.5*x2*np.arctan(yz/(x+EPS)/(r+EPS))
    b = -0.5*y2*np.arctan(zx/(y+EPS)/(r+EPS))
    c = -0.5*z2*np.arctan(xy/(z+EPS)/(r+EPS))
    return s+a+b+c

def shifted_IGF3D(x, y, z, xc = 0, yc = 0, zc = 0):
    return IGF3D(x+xc, y+yc, z+zc)

def cyclic_IGF3D_mapping(ijk, IGF_MAP):
    # n = np.array([-1, 1, 1, -1, 1, -1, -1, 1])
    # v = np.array([IGF_MAP[i, j, k], IGF_MAP[i, j, k+1],\
    #             IGF_MAP[i, j+1, k], IGF_MAP[i, j+1, k+1],\
    #             IGF_MAP[i+1, j, k], IGF_MAP[i+1, j, k+1],\
    #             IGF_MAP[i+1, j+1, k], IGF_MAP[i+1, j+1, k+1]])
    # print i, j, k
    (i, j, k) = ijk
    rr = -IGF_MAP[i, j, k] +IGF_MAP[i, j, k+1]  +IGF_MAP[i, j+1, k]  -IGF_MAP[i, j+1, k+1]+         IGF_MAP[i+1, j, k]-IGF_MAP[i+1, j, k+1]-IGF_MAP[i+1, j+1, k]+IGF_MAP[i+1, j+1, k+1]
    return rr

class SpaceCharge3DFFT():
    
    def __init__(self, nx = 32, ny = 32, nz = 32, N_cutoff = 5, debug = 0,\
                 Lspch = True, Lmirror = False, Lbin = False, Lemit = False, Nbin = 1):
        
        self.nx = nx; self.ny = ny; self.nz = nz
        self.N_cutoff = N_cutoff       
        self.debug = debug
        
        self.Lspch = Lspch; self.Lmirror = Lmirror; self.Lemit = False
        self.Lbin = Lbin; self.Nbin = Nbin
        
        return
    
    def __del__(self):
        print('SpaceCharge3DFFT instance is dead!')
    
    def get_rho_3D0(self, sigma_x = 1e-3, sigma_y = 1e-3, sigma_z = 1e-3, gamma0 = 1, Qtot = 100e-12):
        '''
        Set charge density to 3D gaussian distributions
        '''
        self.Qtot = Qtot
        self.gamma0 = gamma0
        
        sigma_z_static = sigma_z*gamma0
         
        Nx, Ny, Nz = self.nx, self.ny, self.nz
        N_cutoff = self.N_cutoff
        
        self.gamma0 = gamma0
        
        def rho_3D(xyz_static):
            (x_static, y_static, z_static) = xyz_static
            xc, yc, zc = 0, 0, 0
            f_x = 1./np.sqrt(2.*np.pi)/sigma_x*np.exp(-(x_static-xc)**2/2./sigma_x**2)
            f_y = 1./np.sqrt(2.*np.pi)/sigma_y*np.exp(-(y_static-yc)**2/2./sigma_y**2)
            f_z = 1./np.sqrt(2.*np.pi)/sigma_z*np.exp(-(z_static-zc)**2/2./sigma_z_static**2)
            
            return f_x*f_y*f_z
        
        xgrid = np.linspace(-N_cutoff*sigma_x, N_cutoff*sigma_x, Nx+1)
        ygrid = np.linspace(-N_cutoff*sigma_y, N_cutoff*sigma_y, Ny+1)
        zgrid_static = np.linspace(-N_cutoff*sigma_z_static, N_cutoff*sigma_z_static, Nz+1)
        
        XX = np.meshgrid(xgrid, ygrid, zgrid_static, indexing = 'ij')        
        rho = rho_3D(XX)
        
        self.dx = N_cutoff*2*sigma_x/Nx
        self.dy = N_cutoff*2*sigma_y/Ny
        self.dz = N_cutoff*2*sigma_z/Nz
        self.dz_static = self.dz*gamma0
        self.sigma_x = sigma_x
        self.sigma_y = sigma_y
        self.sigma_z = sigma_z
        self.sigma_z_static = self.sigma_z*gamma0
        
        self.xgrid = xgrid; self.ygrid = ygrid; self.zgrid_static = zgrid_static
        self.zmean = 0; self.zref = 0
        self.rho = rho
        return
    
    def get_rho_3D1(self, R = 1e-3, gamma0 = 1, Qtot = 100e-12):
        '''
        Set charge density to 3D uniform distributions confined in a sphere
        '''
        self.Qtot = Qtot
        self.gamma0 = gamma0
        
        sigma_x = R; sigma_y = R; sigma_z = R
        sigma_z_static = sigma_z*gamma0
        
        Nx, Ny, Nz = self.nx, self.ny, self.nz
        N_cutoff = self.N_cutoff  
        
        def rho_3D(xyz_static):
            (x_static, y_static, z_static) = xyz_static
            radius = R #*np.sqrt(15)/5.0;
            xc, yc, zc = 0, 0, 0
            r = np.sqrt((x_static-xc)**2+(y_static-yc)**2+(z_static/gamma0-zc)**2)
            rho = np.where(r>radius, 0, 3./(4*np.pi*radius**3))
            return rho
        
        xgrid = np.linspace(-N_cutoff*sigma_x, N_cutoff*sigma_x, Nx+1)
        ygrid = np.linspace(-N_cutoff*sigma_y, N_cutoff*sigma_y, Ny+1)
        zgrid_static = np.linspace(-N_cutoff*sigma_z_static, N_cutoff*sigma_z_static, Nz+1)
        
        XX = np.meshgrid(xgrid, ygrid, zgrid_static, indexing = 'ij')        
        rho = rho_3D(XX)
        
        self.dx = N_cutoff*2*sigma_x/Nx
        self.dy = N_cutoff*2*sigma_y/Ny
        self.dz = N_cutoff*2*sigma_z/Nz
        self.dz_static = self.dz*gamma0
        self.sigma_x = sigma_x
        self.sigma_y = sigma_y
        self.sigma_z = sigma_z
        self.sigma_z_static = self.sigma_z*gamma0
        
        self.xgrid = xgrid; self.ygrid = ygrid; self.zgrid_static = zgrid_static
        self.zgrid = zgrid_static/gamma0
        
        self.zmean = 0; self.zref = 1.5*R
        self.rho = rho*Qtot
        return
    
    def get_rho_3D(self, dist, nx = None, ny = None, nz = None, N_cutoff = None,\
                   Lmirror = None, Lemit = None, format = 'Astra', fig_ext = '.eps'):
        '''
        Parameters
          dist: particle distributions in the same format as Astra, but absolute `z` and `Pz*c`
        '''
        if nx is not None:
            self.nx = nx
        if ny is not None:
            self.ny = ny
        if nz is not None:
            self.nz = nz
        Nx = self.nx; Ny = self.ny; Nz = self.nz
        if N_cutoff is not None:
            self.N_cutoff = N_cutoff
        N_cutoff = self.N_cutoff
        
        if Lmirror is not None:
            self.Lmirror = Lmirror
        if Lemit is not None:
            self.Lemit = Lemit
        
        if format == 'Astra' or format == 'ASTRA' or format == 'astra':
            ix, iy, iz, iux, iuy, iuz, iw = 0, 1, 2, 3, 4, 5, 7
                       
            # Select active partiles
            select = (dist[:,9]>0); dist = dist[select]
            x = dist[:,ix]
            y = dist[:,iy]
            z = dist[:,iz]
            w = dist[:,iw]*1e-9; self.Qtot = np.sum(w)
            
            zc =  weighted_mean(z, w)
            #self.zmean = (np.max(z)+np.min(z))*0.5
            #z -= self.zmean
            
            bgx, bgy, bgz = dist[:,iux]/g_mec2/1e6, dist[:,iuy]/g_mec2/1e6, dist[:,iuz]/g_mec2/1e6
            gamma = np.sqrt(1+bgx**2+bgy**2+bgz**2)
            
            gamma0 = weighted_mean(gamma, w)
            #z_static = z*gamma0
        
        self.gamma0 = gamma0;              #print inspect.stack()[0][3], 'gamma = ', self.gamma0
        self.sigma_x = weighted_std(x, w); #print inspect.stack()[0][3], 'sigma_x = ', self.sigma_x
        self.sigma_y = weighted_std(y, w); #print inspect.stack()[0][3], 'sigma_y = ', self.sigma_y
        self.sigma_z = weighted_std(z, w); #print inspect.stack()[0][3], 'sigma_z_static = ', self.sigma_z_static
        self.sigma_z_static = self.sigma_z*gamma0;       #print inspect.stack()[0][3], 'sigma_z = ', self.sigma_z
        
        xmin = np.max([np.min(x), -N_cutoff*self.sigma_x])
        xmax = np.min([np.max(x),  N_cutoff*self.sigma_x])
        ymin = np.max([np.min(y), -N_cutoff*self.sigma_y])
        ymax = np.min([np.max(y),  N_cutoff*self.sigma_y])
        N_cutoff_z = 5
        zmin = np.max([np.min(z), zc-N_cutoff_z*self.sigma_z])
        zmax = np.min([np.max(z), zc+N_cutoff_z*self.sigma_z])
        
        xmax = np.max([xmax, -xmin])
        ymax = np.max([ymax, -ymin])
        xmin, ymin = -xmax, -ymax
        #zmin = np.min(z)
        #zmax = np.max(z)
        nx0, ny0, nz0 = 1, 1, 1 # Guarding grids on both sides
        if self.Lemit:
            zmin = 0
            zmax = np.max(z)
            nz0 = 2.5
        #print inspect.stack()[0][3], zmin, zmax, self.zmean
        
        #import pdb; pdb.set_trace()
        self.zmean = (zmin+zmax)*0.5 # zmean is geometric center
        z -= self.zmean
        z_static = z*gamma0
        
        zmax -= self.zmean
        zmin -= self.zmean
        
        hx = (xmax-xmin)/(Nx-2*nx0); hy = (ymax-ymin)/(Ny-2*ny0)
        hz = (zmax-zmin)/(Nz-2*nz0)
        
        zmin_static, zmax_static = zmin*gamma0, zmax*gamma0
        hz_static = hz*gamma0
        
        self.dx = hx; self.dy = hy; self.dz = hz; #print inspect.stack()[0][3], 'mesh sizes: ', hx, hy, hz
        self.dz_static = hz_static
        
        if self.debug and 0:
            print((inspect.stack()[0][3], 'Qtot = ', self.Qtot))
            print((inspect.stack()[0][3], 'gamma = ', self.gamma0))
            print((inspect.stack()[0][3], 'sigma_x = ', self.sigma_x))
            print((inspect.stack()[0][3], 'sigma_y = ', self.sigma_y))
            print((inspect.stack()[0][3], 'sigma_z_static = ', self.sigma_z_static))
            print((inspect.stack()[0][3], 'sigma_z = ', self.sigma_z))
            print((inspect.stack()[0][3], 'mesh sizes in rest frame: ', hx, hy, hz_static))
        
        xgrid = np.linspace(xmin-nx0*hx, xmax+nx0*hx, Nx+1)
        ygrid = np.linspace(ymin-ny0*hy, ymax+ny0*hy, Ny+1)
        zgrid = np.linspace(zmin-nz0*hz, zmax+nz0*hz, Nz+1); # print zgrid+self.zmean
        #import pdb; pdb.set_trace()
        zgrid_static = zgrid*gamma0
        
        ranges = [(xgrid[0]-0.5*hx, xgrid[-1]+0.5*hx), (ygrid[0]-0.5*hy, ygrid[-1]+0.5*hy),\
                  (zgrid_static[0]-0.5*hz_static, zgrid_static[-1]+0.5*hz_static)]
        rho, edges = np.histogramdd((x, y, z_static), bins = (Nx+1, Ny+1, Nz+1), range = ranges, weights = w)
        del edges
        
        if 1 or self.debug:
            #print xgrid/self.sigma_x, ygrid/self.sigma_y, zgrid/self.sigma_z_static
            #print rho
            #print ranges
            fig, [ax1, ax2, ax3] = plt.subplots(ncols = 3, figsize = (12, 4))
            rho_x = np.sum(np.sum(rho, axis = 1), axis = 1)
            ax1.plot(xgrid*1e3, rho_x, '-*')
            ax1.grid()
            ax1.set_xlabel(r'$x$ (mm)')
            ax1.set_ylabel(r'$\rho$ (arb. unit)')
            
            [aX,aY] = np.meshgrid(xgrid, ygrid)
            data = np.abs(rho[:,:,Nz//2-1])
            v = np.linspace(0.05, 1., 96)
            ax2.contourf(aX*1e3, aY*1e3, data/np.max(data), v, linestyles = None)
            ax2.grid()
            ax2.set_xlabel(r'$x$ (mm)')
            ax2.set_ylabel(r'$y$ (mm)')
            
            rho_z = np.sum(np.sum(rho, axis = 0), axis = 0)
            ax3.plot(zgrid*1e3, rho_z, '-*')
            ax3.grid()
            ax3.set_xlabel(r'$z$ (mm)')
            ax3.set_ylabel(r'$\rho$ (arb. unit)')
            
            fig.savefig('charge-density'+fig_ext)
        
        #rho = sgolay2d(rho, window_size=11, order=4)   
        #self.rho_RZ = interp2d(rr, zz, rho.T, kind = 'cubic')
        self.xgrid = xgrid
        self.ygrid = ygrid
        self.zgrid = zgrid
        self.zgrid_static = zgrid_static
        self.rho = rho/(hx*hy*hz_static)
        
        return
    
    def build(self, Xshift = (0, 0, 0), **kwargs):
        if 'fig_ext' in list(kwargs.keys()):
            fig_ext = kwargs['fig_ext'];
        else:
            fig_ext = '.eps'
        
        Nx = self.nx; Ny = self.ny; Nz = self.nz; 
        sigma_x = self.sigma_x; sigma_y = self.sigma_y; sigma_z_static = self.sigma_z_static
        #hx = self.dx; hy = self.dy; hz = self.dz_static
        
        if self.Lspch:
            # Create arrays for GFs and densities
            GFC = np.zeros((2*Nx, 2*Ny, 2*Nz))
            RHOC = np.zeros((2*Nx, 2*Ny, 2*Nz))
            
            xgrid = self.xgrid
            ygrid = self.ygrid
            zgrid_static = self.zgrid_static
            RHOC[0:Nx+1, 0:Ny+1, 0:Nz+1] = self.rho
            
            hx = xgrid[1]-xgrid[0]; hy = ygrid[1]-ygrid[0]; hz_static = zgrid_static[1]-zgrid_static[0]
            if self.debug:
                print((inspect.stack()[0][3], Nx, Ny, Nz))
                print((inspect.stack()[0][3], hx, hy, hz_static))
            
            ## using vector to calculate GFs
            ## Calculate GFs by using the mapping, faster
            xf, yf, zf = Xshift; 
            zf0 = -hz_static/self.gamma0/2.
            zf = zf+zf0
            
            xx = xf-Nx*hx+np.arange(2*Nx+1)*hx-0.5*hx
            yy = yf-Ny*hy+np.arange(2*Ny+1)*hy-0.5*hy
            zz_static = zf*self.gamma0-Nz*hz_static+np.arange(2*Nz+1)*hz_static-0.5*hz_static
            XX = np.meshgrid(xx, yy, zz_static, indexing = 'ij')
            IGF_MAP = IGF3D(XX)

            ii = np.mod(np.arange(2*Nx)+Nx, 2*Nx)
            jj = np.mod(np.arange(2*Ny)+Ny, 2*Ny)
            kk = np.mod(np.arange(2*Nz)+Nz, 2*Nz)
            II = np.meshgrid(ii, jj, kk, indexing = 'ij')
            GFC = cyclic_IGF3D_mapping(II, IGF_MAP)

            ## FFT convolution to get the static potenital: PHI
            GFC_FFT = fftn(GFC)
            RHOC_FFT = fftn(RHOC)
            PHIC_FFT = GFC_FFT*RHOC_FFT

            PHIC = ifftn(PHIC_FFT)
            self.PHI = PHIC[:Nx+1,:Ny+1,:Nz+1]
            
        if self.Lspch and self.Lmirror:
            #zf = (self.zref+self.zmean)*2; print zf
            zf0 = -hz_static/self.gamma0/2.
            zf = (0+self.zmean)*2; #print zf
            zf = (0+self.zmean)*2+zf0; #print zf
            
            GFC_mirror = np.zeros((2*Nx, 2*Ny, 2*Nz))
            RHOC_mirror = np.zeros((2*Nx, 2*Ny, 2*Nz))
            
            RHOC_mirror[0:Nx+1, 0:Ny+1, 0:Nz+1] = self.rho[:,:,::-1]
            
            if self.debug:
                print((inspect.stack()[0][3], Nx, Ny, Nz))
                print((inspect.stack()[0][3], hx, hy, hz_static))
            
            ## using vector to calculate GFs
            ## Calculate GFs by using the mapping, faster
            xx = xf-Nx*hx+np.arange(2*Nx+1)*hx-0.5*hx
            yy = yf-Ny*hy+np.arange(2*Ny+1)*hy-0.5*hy
            zz_static = zf*self.gamma0-Nz*hz_static+np.arange(2*Nz+1)*hz_static-0.5*hz_static
            XX = np.meshgrid(xx, yy, zz_static, indexing = 'ij')
            IGF_mirror_MAP = IGF3D(XX)
            
            ii = np.mod(np.arange(2*Nx)+Nx, 2*Nx)
            jj = np.mod(np.arange(2*Ny)+Ny, 2*Ny)
            kk = np.mod(np.arange(2*Nz)+Nz, 2*Nz)
            II = np.meshgrid(ii, jj, kk, indexing = 'ij')
            GFC_mirror = cyclic_IGF3D_mapping(II, IGF_mirror_MAP)

            ## FFT convolution to get the static potenital: PHI
            GFC_mirror_FFT = fftn(GFC_mirror)
            RHOC_mirror_FFT = fftn(RHOC_mirror)
            PHIC_mirror_FFT = GFC_mirror_FFT*RHOC_mirror_FFT

            PHIC_mirror = ifftn(PHIC_mirror_FFT)
            self.PHI_mirror = PHIC_mirror[:Nx+1,:Ny+1,:Nz+1]
        
        if self.Lspch and self.Lmirror:
            
            Ex_static, Ey_static, Ez_static = np.gradient(self.PHI.real, hx, hy, hz_static) 
            Ex_static_m, Ey_static_m, Ez_static_m = np.gradient(self.PHI_mirror.real, hx, hy, hz_static) 
            cc = -1./4/np.pi/g_eps0
        
            print((inspect.stack()[0][3], 'Qtot = ', self.Qtot))
            fig, [ax1, ax2] = plt.subplots(figsize = (12, 4), ncols = 2)
            #ax1.plot(xgrid/sigma_x, Ex_static[:, Ny/2-1, Nz/4-1]*cc/1e3, '-*')
            #ax1.plot(xgrid/sigma_x, -Ex_static_m[:, Ny/2-1, Nz/4-1]*cc/1e3, '-o')
            
            ax1.plot((self.zgrid+self.zmean+zf0)*1e3, Ex_static[Nx//2-1, Ny//2-1, :]*cc/1e3, '-*')
            ax1.plot((self.zgrid+self.zmean+zf0)*1e3, -Ex_static_m[Nx//2-1, Ny//2-1, :]*cc/1e3, '-o')
            ax1.plot((self.zgrid+self.zmean+zf0)*1e3, Ex_static[Nx//2-1, Ny//2-1, :]*cc/1e3-Ex_static_m[Nx//2-1, Ny//2-1, :]*cc/1e3, '--')
            #ax1.set_xlim(-0.05, 0.05)
            #ax1.set_ylim(-200, 200)
            
            ax1.set_xlabel(r'$z$')
            ax1.set_ylabel(r'$E_x(z)$ (kV/m)')
            ax1.grid()
            
            ax2.plot((self.zgrid+self.zmean+zf0)*1e3, Ez_static[Nx//2-1, Ny//2-1, :]*cc/1e3, '-*')
            ax2.plot((self.zgrid+self.zmean+zf0)*1e3, -Ez_static_m[Nx//2-1, Ny//2-1, :]*cc/1e3, '-o')
            ax2.plot((self.zgrid+self.zmean+zf0)*1e3, Ez_static[Nx//2-1, Ny//2-1, :]*cc/1e3-Ez_static_m[Nx//2-1, Ny//2-1, :]*cc/1e3, '--')
            #print Ez_static[Nx/2-1, Ny/2-1, :]*cc/1e3
            #print -Ez_static_m[Nx/2-1, Ny/2-1, :]*cc/1e3
            ax2.set_xlabel(r'$z$ (mm)')
            ax2.set_ylabel(r'$E_z(z)$ (kV/m)')
            #ax2.set_xlim(-0.05, 0.05)
            ax2.grid()
            plt.tight_layout()
            fig.savefig('fields-on-axis'+fig_ext)
            
            fig, [ax1, ax2, ax3] = plt.subplots(figsize = (12, 4), ncols = 3)
            ax1.plot(GFC[Nx//2-1, Ny//2-1, :], '-*')
            ax1.plot(GFC_mirror[Nx//2-1, Ny//2-1, :], '-o')
            ax1.grid()
            ax1.set_xlabel(r'grid number')
            ax1.set_title(r'green function')
            
            to_astra_unit = self.dx*self.dy*self.dz_static/self.dz*1e9
            ax2.plot((self.zgrid+self.zmean+zf0)*1e3, np.sum(np.sum(RHOC[:,:,:Nz+1], axis = 0), axis = 0)*to_astra_unit, '-*')
            ax2.plot((self.zgrid+self.zmean+zf0)*1e3, np.sum(np.sum(RHOC_mirror[:,:,:Nz+1], axis = 0), axis = 0)*to_astra_unit, '-o')
            #ax2.set_xlim(-0.05, 0.05)
            ax2.grid()
            ax2.set_xlabel(r'$z$ (mm)')
            ax2.set_title(r'charge density (nC/m)')
            
            ax3.plot(self.PHI[Nx/2-1, Ny/2-1, :], '-*')
            ax3.plot(self.PHI_mirror[Nx/2-1, Ny/2-1, :], '-o')
            #ax3.set_xlim(-1, 5)
            #ax3.set_ylim(-4.0e-7, -3.5e-7)
            ax3.grid()
            ax3.set_xlabel(r'grid number')
            ax3.set_title(r'static potential')
            plt.tight_layout()
            fig.savefig('green-function'+fig_ext)
            
            #plt.close('all')
            
        if self.Lspch and self.Lmirror:
            self.PHI -= self.PHI_mirror
        
        ######
        Ex_static, Ey_static, Ez_static = np.gradient(self.PHI.real, hx, hy, hz_static)     
        cc = -1./4/np.pi/g_eps0
        ######
        if self.debug and 0:
            print((inspect.stack()[0][3], 'Qtot = ', self.Qtot))
            fig, [ax1, ax2] = plt.subplots(figsize = (12, 4), ncols = 2)
            ax1.plot(xgrid/sigma_x, Ex_static[:, Ny//2-1, Nz//4-1]*cc/1e3, '-*')
            ax1.set_xlabel(r'$x/\sigma_x$')
            ax1.set_ylabel(r'$E_x(x)$ (kV/m)')
            ax1.grid()
            
            ax2.plot(zgrid_static/sigma_z_static, Ez_static[Nx//2-1, Ny//2-1, :]*cc/1e3, '-*')
            ax2.set_xlabel(r'$z/\sigma_z$')
            ax2.set_ylabel(r'$E_z(z)$ (kV/m)')
            ax2.grid()
            
            plt.tight_layout()
            fig.savefig('fields-on-axis.eps')
            
            fig, [ax1, ax2, ax3] = plt.subplots(figsize = (12, 4), ncols = 3)
            ax1.plot(GFC[Nx//2-1, Ny//2-1, :], '-*')
            ax1.grid()
            ax1.set_xlabel(r'grid number')
            ax1.set_title(r'green function')
            
            ax2.plot(RHOC[Nx//2-1, Ny//2-1, :], '-*')
            ax2.grid()
            ax2.set_xlabel(r'grid number')
            ax2.set_title(r'charge density')
            
            ax3.plot(self.PHI[Nx//2-1, Ny//2-1, :]*cc*(-1), '-*')
            ax3.grid()
            ax3.set_xlabel(r'grid number')
            ax3.set_title(r'static potential')
            
            plt.tight_layout()
            fig.savefig('green-function.eps')
        
        del GFC, RHOC, GFC_FFT, RHOC_FFT, PHIC_FFT, IGF_MAP
        if self.Lmirror:
            del GFC_mirror, RHOC_mirror, GFC_mirror_FFT, RHOC_mirror_FFT, PHIC_mirror_FFT, IGF_mirror_MAP
        del XX, II
        
        fEx3D_static = RegularGridInterpolator((xgrid, ygrid, zgrid_static+zf0), Ex_static, bounds_error = False, fill_value = None)
        fEy3D_static = RegularGridInterpolator((xgrid, ygrid, zgrid_static+zf0), Ey_static, bounds_error = False, fill_value = None)
        fEz3D_static = RegularGridInterpolator((xgrid, ygrid, zgrid_static+zf0), Ez_static, bounds_error = False, fill_value = None)
        
        cc = -1./4/np.pi/g_eps0 # 
        gamma0 = self.gamma0; beta0 = gamma2beta(gamma0); zmean = self.zmean
        def EM3D(x, y, z, t = 0, *args):
            if np.isscalar(x):
                x = [x]
            if np.isscalar(y):
                y = [y]
            if np.isscalar(z):
                z = [z]
                
            if len(args)>0:
                dt  = t-args[0]
            else:
                dt = 0
            
            x = np.asarray(x)
            y = np.asarray(y)
            z = np.asarray(z)
            z_static = (z-zmean-beta0*g_c*dt)*gamma0 # here t is the time difference during one RK step
            
            n = np.max([len(x), len(y), len(z)])
            
            Ex_static = fEx3D_static((x, y, z_static))*cc
            Ey_static = fEy3D_static((x, y, z_static))*cc
            Ez_static = fEz3D_static((x, y, z_static))*cc
            
            # return normalied E and B in lab frame
            F2d = np.zeros((n, 6))
            F2d[:,0] = gamma0*Ex_static
            F2d[:,1] = gamma0*Ey_static
            F2d[:,2] = Ez_static
            F2d[:,3] =-beta0*gamma0*Ey_static/g_c # beta0*gamma0*Ey_static/g_c
            F2d[:,4] = beta0*gamma0*Ex_static/g_c # -beta0*gamma0*Ex_static/g_c
            
            return F2d
        
        self.EM3D = EM3D
        return
    
    def get_rho_3D_binned(self, dist, nx = None, ny = None, nz = None, N_cutoff = None,\
                          nbins = 4, Lmirror = None, Lemit = None, format = 'Astra', fig_ext = '.eps'):
        '''
        Parameters
          dist: particle distributions in the same format as Astra, but absolute `z` and `Pz*c`
        '''
        if nx is not None:
            self.nx = nx
        if ny is not None:
            self.ny = ny
        if nz is not None:
            self.nz = nz
        Nx = self.nx; Ny = self.ny; Nz = self.nz
        if N_cutoff is not None:
            self.N_cutoff = N_cutoff
        N_cutoff = self.N_cutoff
        
        if Lmirror is not None:
            self.Lmirror = Lmirror
        if Lemit is not None:
            self.Lemit = Lemit
        
        if format == 'Astra' or format == 'ASTRA' or format == 'astra':
            ix, iy, iz, iux, iuy, iuz, iw = 0, 1, 2, 3, 4, 5, 7
                       
            # Select active partiles
            select = (dist[:,9]>0); dist = dist[select]
            x = dist[:,ix]
            y = dist[:,iy]
            z = dist[:,iz]
            w = dist[:,iw]*1e-9; self.Qtot = np.sum(w)
            
            zc =  weighted_mean(z, w)
            #self.zmean = (np.max(z)+np.min(z))*0.5
            #z -= self.zmean
            
            bgx, bgy, bgz = dist[:,iux]/g_mec2/1e6, dist[:,iuy]/g_mec2/1e6, dist[:,iuz]/g_mec2/1e6
            gamma = np.sqrt(1+bgx**2+bgy**2+bgz**2)
            
            gamma0 = weighted_mean(gamma, w)
            #z_static = z*gamma0
        
        self.gamma0 = gamma0;              #print inspect.stack()[0][3], 'gamma = ', self.gamma0
        self.sigma_x = weighted_std(x, w); #print inspect.stack()[0][3], 'sigma_x = ', self.sigma_x
        self.sigma_y = weighted_std(y, w); #print inspect.stack()[0][3], 'sigma_y = ', self.sigma_y
        self.sigma_z = weighted_std(z, w); #print inspect.stack()[0][3], 'sigma_z_static = ', self.sigma_z_static
        self.sigma_z_static = self.sigma_z*gamma0;       #print inspect.stack()[0][3], 'sigma_z = ', self.sigma_z
        
        xmin = np.max([np.min(x), -N_cutoff*self.sigma_x])
        xmax = np.min([np.max(x),  N_cutoff*self.sigma_x])
        ymin = np.max([np.min(y), -N_cutoff*self.sigma_y])
        ymax = np.min([np.max(y),  N_cutoff*self.sigma_y])
        N_cutoff_z = 5
        zmin = np.max([np.min(z), zc-N_cutoff_z*self.sigma_z])
        zmax = np.min([np.max(z), zc+N_cutoff_z*self.sigma_z])
        
        xmax = np.max([xmax, -xmin])
        ymax = np.max([ymax, -ymin])
        xmin, ymin = -xmax, -ymax
        #zmin = np.min(z)
        #zmax = np.max(z)
        nx0, ny0, nz0 = 1, 1, 1 # Guarding grids on one side
        if self.Lemit:
            zmin = 0
            zmax = np.max(z)
            nz0 = 2.5
        #print inspect.stack()[0][3], zmin, zmax, self.zmean
        
        #import pdb; pdb.set_trace()
        self.zmean = (zmin+zmax)*0.5 # zmean is geometric center
        z -= self.zmean
        z_static = z*gamma0
        
        zmax -= self.zmean
        zmin -= self.zmean
        
        hx = (xmax-xmin)/(Nx-2*nx0); hy = (ymax-ymin)/(Ny-2*ny0)
        hz = (zmax-zmin)/(Nz-2*nz0)
        
        zmin_static, zmax_static = zmin*gamma0, zmax*gamma0
        hz_static = hz*gamma0
        
        self.dx = hx; self.dy = hy; self.dz = hz; #print inspect.stack()[0][3], 'mesh sizes: ', hx, hy, hz
        self.dz_static = hz_static
        
        if self.debug and 0:
            print((inspect.stack()[0][3], 'Qtot = ', self.Qtot))
            print((inspect.stack()[0][3], 'gamma = ', self.gamma0))
            print((inspect.stack()[0][3], 'sigma_x = ', self.sigma_x))
            print((inspect.stack()[0][3], 'sigma_y = ', self.sigma_y))
            print((inspect.stack()[0][3], 'sigma_z_static = ', self.sigma_z_static))
            print((inspect.stack()[0][3], 'sigma_z = ', self.sigma_z))
            print((inspect.stack()[0][3], 'mesh sizes in rest frame: ', hx, hy, hz_static))
        
        '''
        xgrid = np.linspace(xmin-nx0*hx, xmax+nx0*hx, Nx+1)
        ygrid = np.linspace(ymin-ny0*hy, ymax+ny0*hy, Ny+1)
        zgrid = np.linspace(zmin-nz0*hz, zmax+nz0*hz, Nz+1); # print zgrid+self.zmean
        #import pdb; pdb.set_trace()
        zgrid_static = zgrid*gamma0
        '''
        
        ### Select sub-group of particles
        bg = np.sqrt(gamma**2-1)
        bg0 = weighted_mean(bg, w); std_bg0 = weighted_std(bg, w)
        #print bg0, std_bg0
        
        s1 = (bg<bg0-std_bg0)
        s2 = (bg>=bg0-std_bg0)*(bg<bg0)
        s3 = (bg>=bg0)*(bg<bg0+std_bg0)
        s4 = (bg>=bg0+std_bg0)
        ss = [s1, s2, s3, s4]
        for i in np.arange(len(ss)):
            if len(ss[i]) == 0:
                del ss[i]
        
        Qtot = []
        gamma0 = []
        rho = []
        zgrid_static = []
        
        xgrid = np.linspace(xmin-nx0*hx, xmax+nx0*hx, Nx+1)
        ygrid = np.linspace(ymin-ny0*hy, ymax+ny0*hy, Ny+1)
        zgrid = np.linspace(zmin-nz0*hz, zmax+nz0*hz, Nz+1)
        
        fig, ax = plt.subplots()
        ax.grid()
        for si in ss:
            #print [np.sum(i) for i in ss]
            xi = x[si]
            yi = y[si]
            zi = z[si]
            wi = w[si]
            gi = gamma[si]
            
            ax.plot(zi*1e3, bg[si], '.')
            #import pdb; pdb.set_trace()
            g0i = weighted_mean(gi, wi)
            q0i = np.sum(wi)
            
            hz_static = hz*g0i   
            zmin_static, zmax_static = zmin*g0i, zmax*g0i      
            zgridi_static = np.linspace(zmin_static-nz0*hz_static, zmax_static+nz0*hz_static, Nz+1)
            
            ranges = [(xgrid[0]-0.5*hx, xgrid[-1]+0.5*hx), (ygrid[0]-0.5*hy, ygrid[-1]+0.5*hy),\
                      (zgridi_static[0]-0.5*hz_static, zgridi_static[-1]+0.5*hz_static)]
            #print ranges
            rhoi, edges = np.histogramdd((xi, yi, zi*g0i), bins = (Nx+1, Ny+1, Nz+1), range = ranges, weights = wi)
            
            gamma0.append(g0i)
            Qtot.append(q0i)
            zgrid_static.append(zgridi_static)
            rho.append(rhoi/(hx*hy*hz_static))
            
            del zgridi_static
            del rhoi
        ax.set_xlabel(r'$z$ (mm)')
        ax.set_ylabel(r'$\beta\gamma$')
        ax.legend([r'slice #1', r'slice #2', r'slice #3', r'slice #4'])
        fig.savefig('binned-by-bg'+fig_ext)
        
        self.dx = hx; self.dy = hy; self.dz = hz 
        #print inspect.stack()[0][3], 'mesh sizes: ', hx, hy, hz
        
        if self.debug:
            fig, ax1 = plt.subplots(ncols = 1, figsize = (4, 4))
            add = np.zeros(len(zgrid))
            for i in np.arange(4):
                #data = np.abs(rho[i][Nx/2-1, Ny/2-1, :]*gamma0[i])
                rhoi_z = np.abs(np.sum(np.sum(rho[i], axis = 0), axis = 0)*gamma0[i])
                add += rhoi_z
                ax1.plot(zgrid*1e3, rhoi_z, label = 'slice #%d' % (i+1))
            ax1.plot(zgrid*1e3, add, '--', label = 'all')
            ax1.grid()
            ax1.legend()
            ax1.set_xlabel(r'$z$ (mm)')
            ax1.set_ylabel(r'$\rho$ (arb. unit)')
            fig.savefig('charge-density-z.eps')
            
            fig, ax2 = plt.subplots(ncols = 1, figsize = (4, 4))
            add = np.zeros(len(xgrid))
            for i in np.arange(4):
                rhoi_x = np.abs(np.sum(np.sum(rho[i], axis = 1), axis = 1)*gamma0[i])
                add += rhoi_x
                ax2.plot(xgrid*1e3, rhoi_x, label = 'slice #%d' % (i+1))
            ax2.plot(xgrid*1e3, add, '--', label = 'add')
            ax2.grid()
            ax2.legend()
            ax2.set_xlabel(r'$x$ (mm)')
            ax2.set_ylabel(r'$\rho$ (arb. unit)')
            
            fig.savefig('charge-density-x'+fig_ext)
            
        
        # Save 
        self.Qtot = Qtot
        self.gamma0 = gamma0
        
        self.xgrid = xgrid
        self.ygrid = ygrid
        self.zgrid = zgrid
        
        self.rho = rho
        
        return  
    
    def build_binned(self, Xshift = (0, 0, 0), nbins = 4, **kwargs):
        
        if 'fig_ext' in list(kwargs.keys()):
            fig_ext = kwargs['fig_ext'];
        else:
            fig_ext = '.eps'
        
        Nx = self.nx; Ny = self.ny; Nz = self.nz; 
        sigma_x = self.sigma_x; sigma_y = self.sigma_y; sigma_z = self.sigma_z
        #hx = self.dx; hy = self.dy; hz = self.dz_static
        
        xgrid = self.xgrid
        ygrid = self.ygrid
        zgrid = self.zgrid
        hx = xgrid[1]-xgrid[0]; hy = ygrid[1]-ygrid[0]; hz = zgrid[1]-zgrid[0]
        
        if self.Lspch:
            # Create arrays for GFs and densities
            nbins = len(self.Qtot)
            self.PHI = []
            for i in np.arange(nbins):
                GFC = np.zeros((2*Nx, 2*Ny, 2*Nz))
                RHOC = np.zeros((2*Nx, 2*Ny, 2*Nz))
                
                RHOC[0:Nx+1, 0:Ny+1, 0:Nz+1] = self.rho[i]
                
                zgrid_static = self.zgrid*self.gamma0[i]
                hz_static = zgrid_static[1]-zgrid_static[0]
                
                if self.debug:
                    print(inspect.stack()[0][3], Nx, Ny, Nz)
                    print(inspect.stack()[0][3], hx, hy, hz_static)
                
                ## using vector to calculate GFs
                ## Calculate GFs by using the mapping, faster
                xf, yf, zf = Xshift
                zf0 = -hz/2.
                zf = zf+zf0
                
                xx = xf-Nx*hx+np.arange(2*Nx+1)*hx-0.5*hx
                yy = yf-Ny*hy+np.arange(2*Ny+1)*hy-0.5*hy
                zz_static = zf*self.gamma0[i]-Nz*hz_static+np.arange(2*Nz+1)*hz_static-0.5*hz_static
                XX = np.meshgrid(xx, yy, zz_static, indexing = 'ij')
                IGF_MAP = IGF3D(XX)
                
                ii = np.mod(np.arange(2*Nx)+Nx, 2*Nx)
                jj = np.mod(np.arange(2*Ny)+Ny, 2*Ny)
                kk = np.mod(np.arange(2*Nz)+Nz, 2*Nz)
                II = np.meshgrid(ii, jj, kk, indexing = 'ij')
                GFC = cyclic_IGF3D_mapping(II, IGF_MAP)
                
                ## FFT convolution to get the static potenital: PHI
                GFC_FFT = fftn(GFC)
                RHOC_FFT = fftn(RHOC)
                PHIC_FFT = GFC_FFT*RHOC_FFT
                
                PHIC = ifftn(PHIC_FFT)
                
                self.PHI.append(PHIC[:Nx+1,:Ny+1,:Nz+1])
        
        if self.Lspch and self.Lmirror:
            #zf = (self.zref+self.zmean)*2; print zf
            zf0 = -hz/2.
            zf = (0+self.zmean)*2; #print zf
            zf = (0+self.zmean)*2+zf0; #print zf
            
            nbins = len(self.Qtot)
            self.PHI_mirror = []
            for i in np.arange(nbins):
                
                GFC_mirror = np.zeros((2*Nx, 2*Ny, 2*Nz))
                RHOC_mirror = np.zeros((2*Nx, 2*Ny, 2*Nz))
                
                RHOC_mirror[0:Nx+1, 0:Ny+1, 0:Nz+1] = self.rho[i][:,:,::-1]
                #RHOC_mirror = RHOC[:,:,::-1]
                
                zgrid_static = self.zgrid*self.gamma0[i]
                hz_static = zgrid_static[1]-zgrid_static[0]
                
                if self.debug:
                    print(inspect.stack()[0][3], Nx, Ny, Nz)
                    print(inspect.stack()[0][3], hx, hy, hz_static)
                
                ## using vector to calculate GFs
                ## Calculate GFs by using the mapping, faster
                g0i = self.gamma0[i]
                xx = xf-Nx*hx+np.arange(2*Nx+1)*hx-0.5*hx
                yy = yf-Ny*hy+np.arange(2*Ny+1)*hy-0.5*hy
                zz_static = zf*g0i-Nz*hz_static+np.arange(2*Nz+1)*hz_static-0.5*hz_static
                XX = np.meshgrid(xx, yy, zz_static, indexing = 'ij')
                IGF_mirror_MAP = IGF3D(XX)
                
                ii = np.mod(np.arange(2*Nx)+Nx, 2*Nx)
                jj = np.mod(np.arange(2*Ny)+Ny, 2*Ny)
                kk = np.mod(np.arange(2*Nz)+Nz, 2*Nz)
                II = np.meshgrid(ii, jj, kk, indexing = 'ij')
                GFC_mirror = cyclic_IGF3D_mapping(II, IGF_mirror_MAP)
                
                ## FFT convolution to get the static potenital: PHI
                GFC_mirror_FFT = fftn(GFC_mirror)
                RHOC_mirror_FFT = fftn(RHOC_mirror)
                PHIC_mirror_FFT = GFC_mirror_FFT*RHOC_mirror_FFT
                
                PHIC_mirror = ifftn(PHIC_mirror_FFT)
                self.PHI_mirror.append(PHIC_mirror[:Nx+1,:Ny+1,:Nz+1])
                
                self.PHI[i] -= self.PHI_mirror[i]
        
        # Field interpolation
        cc = -1./4/np.pi/g_eps0
        fEx3D_static = []
        fEy3D_static = []
        fEz3D_static = []
        for i in np.arange(nbins):
            Ex_static, Ey_static, Ez_static = np.gradient(self.PHI[i].real, hx, hy, hz*self.gamma0[i])     
        
            fEx3Di_static = RegularGridInterpolator((xgrid, ygrid, zgrid*self.gamma0[i]), Ex_static,                                                    bounds_error = False, fill_value = None)
            fEy3Di_static = RegularGridInterpolator((xgrid, ygrid, zgrid*self.gamma0[i]), Ey_static,                                                    bounds_error = False, fill_value = None)
            fEz3Di_static = RegularGridInterpolator((xgrid, ygrid, zgrid*self.gamma0[i]), Ez_static,                                                    bounds_error = False, fill_value = None)
            
            fEx3D_static.append(fEx3Di_static)
            fEy3D_static.append(fEy3Di_static)
            fEz3D_static.append(fEz3Di_static)
        
        
        if self.debug and 0:
            print(inspect.stack()[0][3], 'Qtot = ', self.Qtot)
            fig, [ax1, ax2] = plt.subplots(figsize = (12, 4), ncols = 2)
            ax1.plot(xgrid/sigma_x, Ex_static[:, Ny//2-1, Nz//4-1]*cc/1e3, '-*')
            ax1.set_xlabel(r'$x/\sigma_x$')
            ax1.set_ylabel(r'$E_x(x)$ (kV/m)')
            ax1.grid()
            ax2.plot(zgrid_static/sigma_z_static, Ez_static[Nx//2-1, Ny//2-1, :]*cc/1e3, '-*')
            ax2.set_xlabel(r'$z/\sigma_z$')
            ax2.set_ylabel(r'$E_z(z)$ (kV/m)')
            ax2.grid()
            plt.tight_layout()
            fig.savefig('fields-on-axis.eps')
            
            fig, [ax1, ax2, ax3] = plt.subplots(figsize = (12, 4), ncols = 3)
            ax1.plot(GFC[:, Ny//2-1, Nz//2-1], '-*')
            ax1.grid()
            ax1.set_xlabel(r'grid number')
            ax1.set_title(r'green function')
            ax2.plot(RHOC[:, Ny//2-1, Nz//2-1], '-*')
            ax2.grid()
            ax2.set_xlabel(r'grid number')
            ax2.set_title(r'charge density')
            ax3.plot(self.PHI[:, Ny//2-1, Nz//2-1], '-*')
            ax3.grid()
            ax3.set_xlabel(r'grid number')
            ax3.set_title(r'static potential')
            plt.tight_layout()
            fig.savefig('green-function.eps')
        
        del GFC, RHOC, GFC_FFT, RHOC_FFT, PHIC_FFT, IGF_MAP
        if self.Lmirror:
            del GFC_mirror, RHOC_mirror, GFC_mirror_FFT, RHOC_mirror_FFT, PHIC_mirror_FFT, IGF_mirror_MAP
        del XX, II
        
        gamma0 = self.gamma0; zmean = self.zmean
        def EM3D(x, y, z, t = 0, *args):
            
            if np.isscalar(x):
                x = [x]
            if np.isscalar(y):
                y = [y]
            if np.isscalar(z):
                z = [z]
            
            if len(args)>0:
                dt  = t-args[0]
            else:
                dt = 0
                
            x = np.asarray(x)
            y = np.asarray(y)
            z = np.asarray(z)
            #z_static = (z-zmean)*gamma0
            
            n = np.max([len(x), len(y), len(z)])
            
            F2d = np.zeros((n, 6))
            for i in np.arange(nbins):
                g0i = gamma0[i]; b0i = gamma2beta(g0i)
                
                #z_static = (z-zmean-beta0*g_c*dt)*gamma0
                z_static = (z-zmean)*g0i
                Ex_static = fEx3D_static[i]((x, y, z_static))*cc
                Ey_static = fEy3D_static[i]((x, y, z_static))*cc
                Ez_static = fEz3D_static[i]((x, y, z_static))*cc
                
                # Normalied E and B in lab frame
                
                F2d[:,0] += g0i*Ex_static
                F2d[:,1] += g0i*Ey_static
                F2d[:,2] += Ez_static
                F2d[:,3] -= b0i*g0i*Ey_static/g_c
                F2d[:,4] += b0i*g0i*Ex_static/g_c           
                #F2D = np.add(F2D, [g0i*Ex_static, g0i*Ey_static, Ez_static, b0i*g0i*Ey_static, -b0i*g0i*Ex_static, 0])
            
            return F2d
        
        self.EM3D = EM3D
        return

"""
class SpaceCharge3DFFT():
    def __init__(self, nx = 32, ny = 32, nz = 32, N_cutoff = 5, debug = 0,\
                 Lspch = True, Lmirror = False, Lbin = False, Lemit = False, Nbin = 1):
        
        self.nx = nx; self.ny = ny; self.nz = nz
        self.N_cutoff = N_cutoff       
        self.debug = debug
        
        self.Lspch = Lspch; self.Lmirror = Lmirror; self.Lemit = False
        self.Lbin = Lbin; self.Nbin = Nbin
        
        return
    def get_rho_3D0(self, sigma_x = 1e-3, sigma_y = 1e-3, sigma_z = 1e-3, gamma0 = 1, Qtot = 100e-12):
        '''
        Set charge density to 3D gaussian distributions
        '''
        self.Qtot = Qtot
        self.gamma0 = gamma0
        
        sigma_z_static = sigma_z*gamma0
         
        Nx, Ny, Nz = self.nx, self.ny, self.nz
        N_cutoff = self.N_cutoff
        
        self.gamma0 = gamma0
        
        def rho_3D((x_static, y_static, z_static)):
            xc, yc, zc = 0, 0, 0
            f_x = 1./np.sqrt(2.*np.pi)/sigma_x*np.exp(-(x_static-xc)**2/2./sigma_x**2)
            f_y = 1./np.sqrt(2.*np.pi)/sigma_y*np.exp(-(y_static-yc)**2/2./sigma_y**2)
            f_z = 1./np.sqrt(2.*np.pi)/sigma_z*np.exp(-(z_static-zc)**2/2./sigma_z_static**2)
            
            return f_x*f_y*f_z
        
        xgrid = np.linspace(-N_cutoff*sigma_x, N_cutoff*sigma_x, Nx+1)
        ygrid = np.linspace(-N_cutoff*sigma_y, N_cutoff*sigma_y, Ny+1)
        zgrid_static = np.linspace(-N_cutoff*sigma_z_static, N_cutoff*sigma_z_static, Nz+1)
        
        XX = np.meshgrid(xgrid, ygrid, zgrid_static, indexing = 'ij')        
        rho = rho_3D(XX)
        
        self.dx = N_cutoff*2*sigma_x/Nx
        self.dy = N_cutoff*2*sigma_y/Ny
        self.dz = N_cutoff*2*sigma_z/Nz
        self.dz_static = self.dz*gamma0
        self.sigma_x = sigma_x
        self.sigma_y = sigma_y
        self.sigma_z = sigma_z
        self.sigma_z_static = self.sigma_z*gamma0
        
        self.xgrid = xgrid; self.ygrid = ygrid; self.zgrid_static = zgrid_static
        self.zmean = 0; self.zref = 0
        self.rho = rho
        return
    def get_rho_3D1(self, R = 1e-3, gamma0 = 1, Qtot = 100e-12):
        '''
        Set charge density to 3D uniform distributions confined in a sphere
        '''
        self.Qtot = Qtot
        self.gamma0 = gamma0
        
        sigma_x = R; sigma_y = R; sigma_z = R
        sigma_z_static = sigma_z*gamma0
        
        Nx, Ny, Nz = self.nx, self.ny, self.nz
        N_cutoff = self.N_cutoff  
        
        def rho_3D((x_static, y_static, z_static)):
            radius = R #*np.sqrt(15)/5.0;
            xc, yc, zc = 0, 0, 0
            r = np.sqrt((x_static-xc)**2+(y_static-yc)**2+(z_static/gamma0-zc)**2)
            rho = np.where(r>radius, 0, 3./(4*np.pi*radius**3))
            return rho
        
        xgrid = np.linspace(-N_cutoff*sigma_x, N_cutoff*sigma_x, Nx+1)
        ygrid = np.linspace(-N_cutoff*sigma_y, N_cutoff*sigma_y, Ny+1)
        zgrid_static = np.linspace(-N_cutoff*sigma_z_static, N_cutoff*sigma_z_static, Nz+1)
        
        XX = np.meshgrid(xgrid, ygrid, zgrid_static, indexing = 'ij')        
        rho = rho_3D(XX)
        
        self.dx = N_cutoff*2*sigma_x/Nx
        self.dy = N_cutoff*2*sigma_y/Ny
        self.dz = N_cutoff*2*sigma_z/Nz
        self.dz_static = self.dz*gamma0
        self.sigma_x = sigma_x
        self.sigma_y = sigma_y
        self.sigma_z = sigma_z
        self.sigma_z_static = self.sigma_z*gamma0
        
        self.xgrid = xgrid; self.ygrid = ygrid; self.zgrid_static = zgrid_static
        self.zgrid = zgrid_static/gamma0
        
        self.zmean = 0; self.zref = 1.5*R
        self.rho = rho*Qtot
        return
    
    def get_rho_3D(self, dist, nx = None, ny = None, nz = None, N_cutoff = None,\
                   Lmirror = None, Lemit = None, format = 'Astra'):
        '''
        Parameters
          dist: particle distributions in the same format as Astra, but absolute `z` and `Pz*c`
        '''
        if nx is not None:
            self.nx = nx
        if ny is not None:
            self.ny = ny
        if nz is not None:
            self.nz = nz
        Nx = self.nx; Ny = self.ny; Nz = self.nz
        if N_cutoff is not None:
            self.N_cutoff = N_cutoff
        N_cutoff = self.N_cutoff
        
        if Lmirror is not None:
            self.Lmirror = Lmirror
        if Lemit is not None:
            self.Lemit = Lemit
        
        if format == 'Astra' or format == 'ASTRA' or format == 'astra':
            ix, iy, iz, iux, iuy, iuz, iw = 0, 1, 2, 3, 4, 5, 7
            
            #self.zref = 0 #dist[0,iz]
            #dist[0,iz] = 0
            
            # Select active partiles
            select = (dist[:,9]>0); dist = dist[select]
            x = dist[:,ix]
            y = dist[:,iy]
            z = dist[:,iz]
            w = dist[:,iw]*1e-9; self.Qtot = np.sum(w)
            
            self.zmean =  weighted_mean(z, w)
            self.zmean = (np.max(z)+np.min(z))*0.5
            z -= self.zmean
                        
            bgx, bgy, bgz = dist[:,iux]/g_mec2/1e6, dist[:,iuy]/g_mec2/1e6, dist[:,iuz]/g_mec2/1e6
            
            gamma = np.sqrt(1+bgx**2+bgy**2+bgz**2)
            gamma0 = weighted_mean(gamma, w)
            z_static = z*gamma0

        self.gamma0 = gamma0;              #print inspect.stack()[0][3], 'gamma = ', self.gamma0
        self.sigma_x = weighted_std(x, w); #print inspect.stack()[0][3], 'sigma_x = ', self.sigma_x
        self.sigma_y = weighted_std(y, w); #print inspect.stack()[0][3], 'sigma_y = ', self.sigma_y
        self.sigma_z_static = weighted_std(z_static, w); #print inspect.stack()[0][3], 'sigma_z_static = ', self.sigma_z_static
        self.sigma_z = self.sigma_z_static/gamma0;       #print inspect.stack()[0][3], 'sigma_z = ', self.sigma_z
        
        xmin = np.max([np.min(x), -N_cutoff*self.sigma_x])
        xmax = np.min([np.max(x),  N_cutoff*self.sigma_x])
        ymin = np.max([np.min(y), -N_cutoff*self.sigma_y])
        ymax = np.min([np.max(y),  N_cutoff*self.sigma_y])
        zmin_static = np.max([np.min(z_static), -N_cutoff*self.sigma_z_static])
        zmax_static = np.min([np.max(z_static),  N_cutoff*self.sigma_z_static])
        zmin, zmax = zmin_static/gamma0, zmax_static/gamma0
        
        zmin, zmax = np.min(z), np.max(z)
        nx0 = 1 # Guarding grids on one side
        ny0 = 1
        nz0 = 1
        if self.Lemit:
            zmin = -self.zmean
            nz0 = 2
        
        hx = (xmax-xmin)/(Nx-2*nx0); hy = (ymax-ymin)/(Ny-2*ny0)
        hz = (zmax-zmin)/(Nz-2*nz0)
        
        zmin_static, zmax_static = zmin*gamma0, zmax*gamma0
        hz_static = hz*gamma0
        
        self.dx = hx; self.dy = hy; self.dz = hz; #print inspect.stack()[0][3], 'mesh sizes: ', hx, hy, hz
        self.dz_static = hz_static
        
        if self.debug and 0:
            print inspect.stack()[0][3], 'Qtot = ', self.Qtot
            print inspect.stack()[0][3], 'gamma = ', self.gamma0
            print inspect.stack()[0][3], 'sigma_x = ', self.sigma_x
            print inspect.stack()[0][3], 'sigma_y = ', self.sigma_y
            print inspect.stack()[0][3], 'sigma_z_static = ', self.sigma_z_static
            print inspect.stack()[0][3], 'sigma_z = ', self.sigma_z
            print inspect.stack()[0][3], 'mesh sizes in rest frame: ', hx, hy, hz_static
        
        xgrid = np.linspace(xmin-nx0*hx, xmax+nx0*hx, Nx+1)
        ygrid = np.linspace(ymin-ny0*hy, ymax+ny0*hy, Ny+1)
        zgrid = np.linspace(zmin-nz0*hz, zmax+nz0*hz, Nz+1)
        
        zgrid_static = zgrid*gamma0
        
        ranges = [(xgrid[0]-0.5*hx, xgrid[-1]+0.5*hx), (ygrid[0]-0.5*hy, ygrid[-1]+0.5*hy),\
                  (zgrid_static[0]-0.5*hz_static, zgrid_static[-1]+0.5*hz_static)]
        rho, edges = np.histogramdd((x, y, z_static), bins = (Nx+1, Ny+1, Nz+1), range = ranges, weights = w)
        
        if self.debug:
            #print xgrid/self.sigma_x, ygrid/self.sigma_y, zgrid/self.sigma_z_static
            #print rho
            #print ranges
            fig, [ax1, ax2] = plt.subplots(ncols = 2, figsize = (8, 4))
            data = np.abs(rho[:,Ny/2-1, Nz/2-1])
            ax1.plot(xgrid*1e3, data/np.max(data), '-*')
            ax1.grid()
            ax1.set_xlabel(r'$x$ (mm)')
            ax1.set_ylabel(r'$\rho$ (arb. unit)')
            
            [aX,aY] = np.meshgrid(xgrid, ygrid)
            data = np.abs(rho[:,:,Nz/2-1])
            v = np.linspace(0.05, 1., 96)
            ax2.contourf(aX*1e3, aY*1e3, data/np.max(data), v, linestyles = None)
            ax2.grid()
            ax2.set_xlabel(r'$x$ (mm)')
            ax2.set_ylabel(r'$y$ (mm)')
            
            fig.savefig('charge-density.eps')
        
        #rho = sgolay2d(rho, window_size=11, order=4)   
        #self.rho_RZ = interp2d(rr, zz, rho.T, kind = 'cubic')
        self.xgrid = xgrid
        self.ygrid = ygrid
        self.zgrid = zgrid
        self.zgrid_static = zgrid_static
        self.rho = rho/(hx*hy*hz_static)
        return  
    def build(self, Xshift = (0, 0, 0)):
        
        Nx = self.nx; Ny = self.ny; Nz = self.nz; 
        sigma_x = self.sigma_x; sigma_y = self.sigma_y; sigma_z_static = self.sigma_z_static
        #hx = self.dx; hy = self.dy; hz = self.dz_static
        
        #if N_cutoff is None:
        #    N_cutoff = self.N_cutoff
        N_cutoff = self.N_cutoff
        
        if self.Lspch:
            # Create arrays for GFs and densities
            GFC = np.zeros((2*Nx, 2*Ny, 2*Nz))
            RHOC = np.zeros((2*Nx, 2*Ny, 2*Nz))

            xgrid = self.xgrid
            ygrid = self.ygrid
            zgrid_static = self.zgrid_static
            RHOC[0:Nx+1, 0:Ny+1, 0:Nz+1] = self.rho

            hx = xgrid[1]-xgrid[0]; hy = ygrid[1]-ygrid[0]; hz_static = zgrid_static[1]-zgrid_static[0]
            if self.debug:
                print inspect.stack()[0][3], Nx, Ny, Nz
                print inspect.stack()[0][3], hx, hy, hz_static

            ## using vector to calculate GFs
            ## Calculate GFs by using the mapping, faster
            xf, yf, zf = Xshift
            xx = xf-Nx*hx+np.arange(2*Nx+1)*hx-0.5*hx
            yy = yf-Ny*hy+np.arange(2*Ny+1)*hy-0.5*hy
            zz_static = zf*self.gamma0-Nz*hz_static+np.arange(2*Nz+1)*hz_static-0.5*hz_static
            XX = np.meshgrid(xx, yy, zz_static, indexing = 'ij')
            IGF_MAP = IGF3D(XX)

            ii = np.mod(np.arange(2*Nx)+Nx, 2*Nx)
            jj = np.mod(np.arange(2*Ny)+Ny, 2*Ny)
            kk = np.mod(np.arange(2*Nz)+Nz, 2*Nz)
            II = np.meshgrid(ii, jj, kk, indexing = 'ij')
            GFC = cyclic_IGF3D_mapping(II, IGF_MAP)

            ## FFT convolution to get the static potenital: PHI
            GFC_FFT = fftn(GFC)
            RHOC_FFT = fftn(RHOC)
            PHIC_FFT = GFC_FFT*RHOC_FFT

            PHIC = ifftn(PHIC_FFT)
            self.PHI = PHIC[:Nx+1,:Ny+1,:Nz+1]
            
        if self.Lspch and self.Lmirror:
            #zf = (self.zref+self.zmean)*2; print zf
            zf = (0+self.zmean)*2; #print zf
            
            GFC_mirror = np.zeros((2*Nx, 2*Ny, 2*Nz))
            RHOC_mirror = np.zeros((2*Nx, 2*Ny, 2*Nz))
            
            RHOC_mirror[0:Nx+1, 0:Ny+1, 0:Nz+1] = self.rho[:,:,::-1]
            #RHOC_mirror = RHOC[:,:,::-1]
            #zgrid = -self.zgrid[::-1]
            
            if self.debug:
                print inspect.stack()[0][3], Nx, Ny, Nz
                print inspect.stack()[0][3], hx, hy, hz_static
            
            ## using vector to calculate GFs
            ## Calculate GFs by using the mapping, faster
            xx = xf-Nx*hx+np.arange(2*Nx+1)*hx-0.5*hx
            yy = yf-Ny*hy+np.arange(2*Ny+1)*hy-0.5*hy
            zz_static = zf*self.gamma0-Nz*hz_static+np.arange(2*Nz+1)*hz_static-0.5*hz_static
            XX = np.meshgrid(xx, yy, zz_static, indexing = 'ij')
            IGF_mirror_MAP = IGF3D(XX)
            
            ii = np.mod(np.arange(2*Nx)+Nx, 2*Nx)
            jj = np.mod(np.arange(2*Ny)+Ny, 2*Ny)
            kk = np.mod(np.arange(2*Nz)+Nz, 2*Nz)
            II = np.meshgrid(ii, jj, kk, indexing = 'ij')
            GFC_mirror = cyclic_IGF3D_mapping(II, IGF_mirror_MAP)

            ## FFT convolution to get the static potenital: PHI
            GFC_mirror_FFT = fftn(GFC_mirror)
            RHOC_mirror_FFT = fftn(RHOC_mirror)
            PHIC_mirror_FFT = GFC_mirror_FFT*RHOC_mirror_FFT

            PHIC_mirror = ifftn(PHIC_mirror_FFT)
            self.PHI_mirror = PHIC_mirror[:Nx+1,:Ny+1,:Nz+1]
        
        if self.debug and self.Lmirror:
            
            Ex_static, Ey_static, Ez_static = np.gradient(self.PHI, hx, hy, hz_static) 
            Ex_static_m, Ey_static_m, Ez_static_m = np.gradient(self.PHI_mirror, hx, hy, hz_static) 
            cc = -1./4/np.pi/g_eps0
        
            print inspect.stack()[0][3], 'Qtot = ', self.Qtot
            fig, [ax1, ax2] = plt.subplots(figsize = (12, 4), ncols = 2)
            ax1.plot(xgrid/sigma_x, Ex_static[:, Ny/2-1, Nz/4-1]*cc/1e3, '-*')
            ax1.plot(xgrid/sigma_x, -Ex_static_m[:, Ny/2-1, Nz/4-1]*cc/1e3, '-o')
            ax1.set_xlabel(r'$x/\sigma_x$')
            ax1.set_ylabel(r'$E_x(x)$ (kV/m)')
            ax1.grid()
            
            ax2.plot(zgrid_static/sigma_z_static, Ez_static[Nx/2-1, Ny/2-1, :]*cc/1e3, '-*')
            ax2.plot(zgrid_static/sigma_z_static, -Ez_static_m[Nx/2-1, Ny/2-1, :]*cc/1e3, '-o')
            #print Ez_static[Nx/2-1, Ny/2-1, :]*cc/1e3
            #print -Ez_static_m[Nx/2-1, Ny/2-1, :]*cc/1e3
            ax2.set_xlabel(r'$z/\sigma_z$')
            ax2.set_ylabel(r'$E_z(z)$ (kV/m)')
            ax2.grid()
            plt.tight_layout()
            fig.savefig('fields-on-axis.eps')
            
            fig, [ax1, ax2, ax3] = plt.subplots(figsize = (12, 4), ncols = 3)
            ax1.plot(GFC[Nx/2-1, Ny/2-1, :], '-*')
            ax1.plot(GFC_mirror[Nx/2-1, Ny/2-1, :], '-o')
            ax1.grid()
            ax1.set_xlabel(r'grid number')
            ax1.set_title(r'green function')
            ax2.plot(RHOC[Nx/2-1, Ny/2-1, :], '-*')
            ax2.plot(RHOC_mirror[Nx/2-1, Ny/2-1, :], '-*')
            ax2.grid()
            ax2.set_xlabel(r'grid number')
            ax2.set_title(r'charge density')
            ax3.plot(self.PHI[Nx/2-1, Ny/2-1, :], '-*')
            ax3.plot(self.PHI_mirror[Nx/2-1, Ny/2-1, :], '-*')
            ax3.grid()
            ax3.set_xlabel(r'grid number')
            ax3.set_title(r'static potential')
            plt.tight_layout()
            fig.savefig('green-function.eps')
        
        if self.Lspch and self.Lmirror:
            self.PHI -= self.PHI_mirror
        
        Ex_static, Ey_static, Ez_static = np.gradient(self.PHI, hx, hy, hz_static)     
        cc = -1./4/np.pi/g_eps0
        
        if self.debug:
            print inspect.stack()[0][3], 'Qtot = ', self.Qtot
            fig, [ax1, ax2] = plt.subplots(figsize = (12, 4), ncols = 2)
            ax1.plot(xgrid/sigma_x, Ex_static[:, Ny/2-1, Nz/4-1]*cc/1e3, '-*')
            ax1.set_xlabel(r'$x/\sigma_x$')
            ax1.set_ylabel(r'$E_x(x)$ (kV/m)')
            ax1.grid()
            ax2.plot(zgrid_static/sigma_z_static, Ez_static[Nx/2-1, Ny/2-1, :]*cc/1e3, '-*')
            ax2.set_xlabel(r'$z/\sigma_z$')
            ax2.set_ylabel(r'$E_z(z)$ (kV/m)')
            ax2.grid()
            
            plt.tight_layout()
            fig.savefig('fields-on-axis.eps')
            
            fig, [ax1, ax2, ax3] = plt.subplots(figsize = (12, 4), ncols = 3)
            ax1.plot(GFC[Nx/2-1, Ny/2-1, :], '-*')
            ax1.grid()
            ax1.set_xlabel(r'grid number')
            ax1.set_title(r'green function')
            ax2.plot(RHOC[Nx/2-1, Ny/2-1, :], '-*')
            ax2.grid()
            ax2.set_xlabel(r'grid number')
            ax2.set_title(r'charge density')
            ax3.plot(self.PHI[Nx/2-1, Ny/2-1, :]*cc*(-1), '-*')
            ax3.grid()
            ax3.set_xlabel(r'grid number')
            ax3.set_title(r'static potential')
            
            plt.tight_layout()
            fig.savefig('green-function.eps')
        
        fEx3D_static = RegularGridInterpolator((xgrid, ygrid, zgrid_static), Ex_static, bounds_error = False, fill_value = None)
        fEy3D_static = RegularGridInterpolator((xgrid, ygrid, zgrid_static), Ey_static, bounds_error = False, fill_value = None)
        fEz3D_static = RegularGridInterpolator((xgrid, ygrid, zgrid_static), Ez_static, bounds_error = False, fill_value = None)
        
        gamma0 = self.gamma0; beta = gamma2beta(gamma0); zmean = self.zmean
        def EM3D(x, y, z, t = 0):
            #if np.isscalar(x):
            #    x = [x]
            #if np.isscalar(y):
            #    y = [y]
            #if np.isscalar(z):
            #    z = [z]
            x = np.atleast_1d(x)
            y = np.atleast_1d(y)
            z = np.atleast_1d(z)
            n = np.max(len(x), len(y), len(z))
            #if len(x)<n:
            #    x = [x[0] for i in np.arange(n)]
            #if len(y)<n:
            #    y = [y[0] for i in np.arange(n)]
            #if len(z)<n:
            #    z = [z[0] for i in np.arange(n)]
            
            #x = np.asarray(x)
            #y = np.asarray(y)
            #z = np.asarray(z)
            z_static = (z-zmean)*gamma0
            if not np.isscalar(t):
                t = np.asarray(t)
            
            Ex_static = fEx3D_static((x, y, z_static))*cc
            Ey_static = fEy3D_static((x, y, z_static))*cc
            Ez_static = fEz3D_static((x, y, z_static))*cc
            
            # return normalied E and B in lab frame
            F2d = np.zeros((n, 6))
            F2d[:,0] = gamma0*Ex_static
            F2d[:,1] = gamma0*Ey_static
            F2d[:,2] = Ez_static
            F2d[:,3] = beta*gamma0*Ey_static/g_c
            F2d[:,4] =-beta*gamma0*Ex_static/g_c
            
            return F2d
        
        if self.debug and 0:
            X, Y, Z = np.meshgrid(xx, yy, zz, indexing = 'ij')

            fEx3D = Rbf(X, Y, Z, Ex, function = 'linear') # or X.ravel()
            fEy3D = Rbf(X, Y, Z, Ey, function = 'linear')
            fEz3D = Rbf(X, Y, Z, Ez, function = 'linear')

            gamma = self.gamma; beta = gamma2beta(gamma)
            def EM3D(x, y, z, t = 0):
                z = z*gamma
                Ex = fEx3D(x, y, z)*cc
                Ey = fEy3D(x, y, z)*cc
                Ez = fEz3D(x, y, z)*cc
                # return normalied E and B
                return [gamma*Ex, gamma*Ey, Ez, beta*gamma*Ey, -beta*gamma*Ex, 0]
        
        self.EM3D = EM3D
        return
    
    def get_rho_3D_binned(self, dist, nx = None, ny = None, nz = None, N_cutoff = None,\
                          nbins = 4, format = 'Astra', Lemit = True):
        '''
        Parameters
          dist: particle distributions in the same format as Astra, but absolute `z` and `Pz*c`
        '''
        if nx is not None:
            self.nx = nx
        if ny is not None:
            self.ny = ny
        if nz is not None:
            self.nz = nz
        Nx = self.nx; Ny = self.ny; Nz = self.nz
        if N_cutoff is not None:
            self.N_cutoff = N_cutoff
        N_cutoff = self.N_cutoff
        
        if format == 'Astra' or format == 'ASTRA' or format == 'astra':
            ix, iy, iz, iux, iuy, iuz, iw = 0, 1, 2, 3, 4, 5, 7
            
            self.zref = dist[0,iz]
            
            # Select active partiles
            select = (dist[:,9]>0); dist = dist[select]
            x = dist[:,ix]
            y = dist[:,iy]
            z = dist[:,iz]
            w = dist[:,iw]*1e-9
            
            self.zmean =  weighted_mean(z, w); print self.zmean
            self.zmean = (np.max(z)+np.min(z))*0.5; print self.zmean
            z -= self.zmean
            
            bgx, bgy, bgz = dist[:,iux]/g_mec2/1e6, dist[:,iuy]/g_mec2/1e6, dist[:,iuz]/g_mec2/1e6
            
            gamma = np.sqrt(1+bgx**2+bgy**2+bgz**2)
            gamma0 = weighted_mean(gamma, w); std_gamma0 = weighted_std(gamma, w)
        
        self.gamma0 = gamma0;              #print inspect.stack()[0][3], 'gamma = ', self.gamma0
        self.sigma_x = weighted_std(x, w); #print inspect.stack()[0][3], 'sigma_x = ', self.sigma_x
        self.sigma_y = weighted_std(y, w); #print inspect.stack()[0][3], 'sigma_y = ', self.sigma_y
        self.sigma_z = weighted_std(z, w); #print inspect.stack()[0][3], 'sigma_z_static = ', self.sigma_z_static
        
        xmin = np.max([np.min(x), -N_cutoff*self.sigma_x])
        xmax = np.min([np.max(x),  N_cutoff*self.sigma_x])
        ymin = np.max([np.min(y), -N_cutoff*self.sigma_y])
        ymax = np.min([np.max(y),  N_cutoff*self.sigma_y])
        zmin = np.max([np.min(z), -N_cutoff*self.sigma_z])
        zmax = np.min([np.max(z),  N_cutoff*self.sigma_z])
        
        zmin, zmax = np.min(z), np.max(z)
        
        if Lemit:
            zmin = -self.zmean
        
        nx0 = 1# Guarding grids on one side
        ny0 = 1
        nz0 = 1
        
        if Lemit:
            nz0 = 2
        
        hx = (xmax-xmin)/(Nx-2*nx0); hy = (ymax-ymin)/(Ny-2*nx0)
        hz = (zmax-zmin)/(Nz-2*nz0)
        
        # Select sub-group of particles
        bg = np.sqrt(gamma**2-1)
        bg0 = weighted_mean(bg, w); std_bg0 = weighted_std(bg, w)
        #print bg0, std_bg0
        
        s1 = (bg<bg0-std_bg0)
        s2 = (bg>=bg0-std_bg0)*(bg<bg0)
        s3 = (bg>=bg0)*(bg<bg0+std_bg0)
        s4 = (bg>=bg0+std_bg0)
        ss = [s1, s2, s3, s4]
        
        Qtot = []
        gamma0 = []
        rho = []
        zgrid_static = []
        
        xgrid = np.linspace(xmin-nx0*hx, xmax+nx0*hx, Nx+1)
        ygrid = np.linspace(ymin-ny0*hy, ymax+ny0*hy, Ny+1)
        zgrid = np.linspace(zmin-nz0*hz, zmax+nz0*hz, Nz+1)
        
        
        fig, ax = plt.subplots()
        ax.grid()
        for si in ss:
            #print [np.sum(i) for i in ss]
            xi = x[si]
            yi = y[si]
            zi = z[si]
            wi = w[si]
            gi = gamma[si]
            
            ax.plot(zi*1e3, bg[si], '.')
            #import pdb; pdb.set_trace()
            g0i = weighted_mean(gi, wi)
            q0i = np.sum(wi)
            
            hz_static = hz*g0i   
            zmin_static, zmax_static = zmin*g0i, zmax*g0i      
            zgridi_static = np.linspace(zmin_static-nz0*hz_static, zmax_static+nz0*hz_static, Nz+1)
            
            ranges = [(xgrid[0]-0.5*hx, xgrid[-1]+0.5*hx),                      (ygrid[0]-0.5*hy, ygrid[-1]+0.5*hy),                      (zgridi_static[0]-0.5*hz_static, zgridi_static[-1]+0.5*hz_static)]
            #print ranges
            rhoi, edges = np.histogramdd((xi, yi, zi*g0i), bins = (Nx+1, Ny+1, Nz+1),                                        range = ranges, weights = wi)
            
            gamma0.append(g0i)
            Qtot.append(q0i)
            zgrid_static.append(zgridi_static)
            rho.append(rhoi/(hx*hy*hz_static))
            
            del zgridi_static
            del rhoi
        
        self.dx = hx; self.dy = hy; self.dz = hz 
        #print inspect.stack()[0][3], 'mesh sizes: ', hx, hy, hz
        
        if self.debug:
            fig, [ax1, ax2] = plt.subplots(ncols = 2, figsize = (12, 4))
            add = np.zeros(len(zgrid))
            for i in np.arange(4):
                #data = np.abs(rho[i][Nx/2-1, Ny/2-1, :]*gamma0[i])
                rhoi_z = np.abs(np.sum(np.sum(rho[i], axis = 0), axis = 0)*gamma0[i])
                add += rhoi_z
                ax1.plot(zgrid*1e3, rhoi_z, label = '%d' % i)
            ax1.plot(zgrid*1e3, add, '--', label = 'all')
            ax1.grid()
            ax1.legend()
            ax1.set_xlabel(r'$z$ (mm)')
            ax1.set_ylabel(r'$\rho$ (arb. unit)')
            
            add = np.zeros(len(xgrid))
            for i in np.arange(4):
                rhoi_x = np.abs(np.sum(np.sum(rho[i], axis = 1), axis = 1)*gamma0[i])
                add += rhoi_x
                ax2.plot(xgrid*1e3, rhoi_x, label = '%d' % i)
            ax2.plot(xgrid*1e3, add, '--', label = 'add')
            ax2.grid()
            ax2.legend()
            ax2.set_xlabel(r'$x$ (mm)')
            ax2.set_ylabel(r'$\rho$ (arb. unit)')
            
            fig.savefig('charge-density.eps')
        
        # Save 
        self.Qtot = Qtot
        self.gamma0 = gamma0
        
        self.xgrid = xgrid
        self.ygrid = ygrid
        self.zgrid = zgrid
        
        self.rho = rho
        
        return  
    def build_binned(self, Xshift = (0, 0, 0), N_cutoff = None, nbins = 4):
        
        Nx = self.nx; Ny = self.ny; Nz = self.nz; 
        sigma_x = self.sigma_x; sigma_y = self.sigma_y; sigma_z = self.sigma_z
        #hx = self.dx; hy = self.dy; hz = self.dz_static
        
        if N_cutoff is None:
            N_cutoff = self.N_cutoff
        
        xgrid = self.xgrid
        ygrid = self.ygrid
        zgrid = self.zgrid
        hx = xgrid[1]-xgrid[0]; hy = ygrid[1]-ygrid[0]; hz = zgrid[1]-zgrid[0]
        
        if self.Lspch:
            # Create arrays for GFs and densities
            nbins = len(self.Qtot)
            self.PHI = []
            for i in np.arange(nbins):
                GFC = np.zeros((2*Nx, 2*Ny, 2*Nz))
                RHOC = np.zeros((2*Nx, 2*Ny, 2*Nz))
                
                RHOC[0:Nx+1, 0:Ny+1, 0:Nz+1] = self.rho[i]
                
                zgrid_static = self.zgrid*self.gamma0[i]
                hz_static = zgrid_static[1]-zgrid_static[0]
                
                if self.debug:
                    print inspect.stack()[0][3], Nx, Ny, Nz
                    print inspect.stack()[0][3], hx, hy, hz_static
                
                ## using vector to calculate GFs
                ## Calculate GFs by using the mapping, faster
                xf, yf, zf = Xshift
                xx = xf-Nx*hx+np.arange(2*Nx+1)*hx-0.5*hx
                yy = yf-Ny*hy+np.arange(2*Ny+1)*hy-0.5*hy
                zz_static = zf*self.gamma0[i]-Nz*hz_static+np.arange(2*Nz+1)*hz_static-0.5*hz_static
                XX = np.meshgrid(xx, yy, zz_static, indexing = 'ij')
                IGF_MAP = IGF3D(XX)
                
                ii = np.mod(np.arange(2*Nx)+Nx, 2*Nx)
                jj = np.mod(np.arange(2*Ny)+Ny, 2*Ny)
                kk = np.mod(np.arange(2*Nz)+Nz, 2*Nz)
                II = np.meshgrid(ii, jj, kk, indexing = 'ij')
                GFC = cyclic_IGF3D_mapping(II, IGF_MAP)
                
                ## FFT convolution to get the static potenital: PHI
                GFC_FFT = fftn(GFC)
                RHOC_FFT = fftn(RHOC)
                PHIC_FFT = GFC_FFT*RHOC_FFT
                
                PHIC = ifftn(PHIC_FFT)
                
                self.PHI.append(PHIC[:Nx+1,:Ny+1,:Nz+1])
        
        if self.Lspch and self.Lmirror:
            #zf = (self.zref+self.zmean)*2; print zf
            zf = (0+self.zmean)*2; #print zf
            
            nbins = len(self.Qtot)
            self.PHI_mirror = []
            for i in np.arange(nbins):
                
                GFC = np.zeros((2*Nx, 2*Ny, 2*Nz))
                RHOC_mirror = np.zeros((2*Nx, 2*Ny, 2*Nz))
                
                zgrid_static = self.zgrid*self.gamma0[i]
                hz_static = zgrid_static[1]-zgrid_static[0]
                
                RHOC_mirror[0:Nx+1, 0:Ny+1, 0:Nz+1] = self.rho[i][:,:,::-1]
                #RHOC_mirror = RHOC[:,:,::-1]
                
                if self.debug:
                    print inspect.stack()[0][3], Nx, Ny, Nz
                    print inspect.stack()[0][3], hx, hy, hz_static
                
                ## using vector to calculate GFs
                ## Calculate GFs by using the mapping, faster
                g0i = self.gamma0[i]
                xx = xf-Nx*hx+np.arange(2*Nx+1)*hx-0.5*hx
                yy = yf-Ny*hy+np.arange(2*Ny+1)*hy-0.5*hy
                zz_static = zf*g0i-Nz*hz_static+np.arange(2*Nz+1)*hz_static-0.5*hz_static
                XX = np.meshgrid(xx, yy, zz_static, indexing = 'ij')
                IGF_MAP = IGF3D(XX)
                
                ii = np.mod(np.arange(2*Nx)+Nx, 2*Nx)
                jj = np.mod(np.arange(2*Ny)+Ny, 2*Ny)
                kk = np.mod(np.arange(2*Nz)+Nz, 2*Nz)
                II = np.meshgrid(ii, jj, kk, indexing = 'ij')
                GFC = cyclic_IGF3D_mapping(II, IGF_MAP)
                
                ## FFT convolution to get the static potenital: PHI
                GFC_FFT = fftn(GFC)
                RHOC_FFT = fftn(RHOC_mirror)
                PHIC_FFT = GFC_FFT*RHOC_FFT
                
                PHIC_mirror = ifftn(PHIC_FFT)
                self.PHI_mirror.append(PHIC_mirror[:Nx+1,:Ny+1,:Nz+1])
                
                self.PHI[i] -= self.PHI_mirror[i]
        
        # Field interpolation
        cc = -1./4/np.pi/g_eps0
        fEx3D_static = []
        fEy3D_static = []
        fEz3D_static = []
        for i in np.arange(nbins):
            Ex_static, Ey_static, Ez_static = np.gradient(self.PHI[i], hx, hy, hz*self.gamma0[i])     
        
            fEx3Di_static = RegularGridInterpolator((xgrid, ygrid, zgrid*self.gamma0[i]), Ex_static,                                                    bounds_error = False, fill_value = None)
            fEy3Di_static = RegularGridInterpolator((xgrid, ygrid, zgrid*self.gamma0[i]), Ey_static,                                                    bounds_error = False, fill_value = None)
            fEz3Di_static = RegularGridInterpolator((xgrid, ygrid, zgrid*self.gamma0[i]), Ez_static,                                                    bounds_error = False, fill_value = None)
            
            fEx3D_static.append(fEx3Di_static)
            fEy3D_static.append(fEy3Di_static)
            fEz3D_static.append(fEz3Di_static)
        
        if self.debug and 0:
            print inspect.stack()[0][3], 'Qtot = ', self.Qtot
            fig, [ax1, ax2] = plt.subplots(figsize = (12, 4), ncols = 2)
            ax1.plot(xgrid/sigma_x, Ex_static[:, Ny/2-1, Nz/4-1]*cc/1e3, '-*')
            ax1.set_xlabel(r'$x/\sigma_x$')
            ax1.set_ylabel(r'$E_x(x)$ (kV/m)')
            ax1.grid()
            ax2.plot(zgrid_static/sigma_z_static, Ez_static[Nx/2-1, Ny/2-1, :]*cc/1e3, '-*')
            ax2.set_xlabel(r'$z/\sigma_z$')
            ax2.set_ylabel(r'$E_z(z)$ (kV/m)')
            ax2.grid()
            plt.tight_layout()
            fig.savefig('fields-on-axis.eps')
            
            fig, [ax1, ax2, ax3] = plt.subplots(figsize = (12, 4), ncols = 3)
            ax1.plot(GFC[:, Ny/2-1, Nz/2-1], '-*')
            ax1.grid()
            ax1.set_xlabel(r'grid number')
            ax1.set_title(r'green function')
            ax2.plot(RHOC[:, Ny/2-1, Nz/2-1], '-*')
            ax2.grid()
            ax2.set_xlabel(r'grid number')
            ax2.set_title(r'charge density')
            ax3.plot(self.PHI[:, Ny/2-1, Nz/2-1], '-*')
            ax3.grid()
            ax3.set_xlabel(r'grid number')
            ax3.set_title(r'static potential')
            plt.tight_layout()
            fig.savefig('green-function.eps')
        
        gamma0 = self.gamma0; zmean = self.zmean
        def EM3D(x, y, z, t = 0):
            if np.isscalar(x):
                x = [x]
                y = [y]
                z = [z]
            x = np.asarray(x)
            y = np.asarray(y)
            z = np.asarray(z)
            if not np.isscalar(t):
                t = np.asarray(t)
            
            F2d = np.zeros((len(x), 6))
            for i in np.arange(nbins):
                g0i = gamma0[i]; b0i = gamma2beta(g0i)
                z_static = (z-zmean)*g0i
                Ex_static = fEx3D_static[i]((x, y, z_static))*cc
                Ey_static = fEy3D_static[i]((x, y, z_static))*cc
                Ez_static = fEz3D_static[i]((x, y, z_static))*cc
                
                # Normalied E and B in lab frame
                
                F2d[:,0] += g0i*Ex_static
                F2d[:,1] += g0i*Ey_static
                F2d[:,2] += Ez_static
                F2d[:,3] += b0i*g0i*Ey_static/g_c
                F2d[:,4] -= b0i*g0i*Ex_static/g_c           
                #F2D = np.add(F2D, [g0i*Ex_static, g0i*Ey_static, Ez_static, b0i*g0i*Ey_static, -b0i*g0i*Ex_static, 0])
            
            return F2D
        
        if self.debug and 0:
            X, Y, Z = np.meshgrid(xx, yy, zz, indexing = 'ij')

            fEx3D = Rbf(X, Y, Z, Ex, function = 'linear') # or X.ravel()
            fEy3D = Rbf(X, Y, Z, Ey, function = 'linear')
            fEz3D = Rbf(X, Y, Z, Ez, function = 'linear')

            gamma = self.gamma; beta = gamma2beta(gamma)
            def EM3D(x, y, z, t = 0):
                z = z*gamma
                Ex = fEx3D(x, y, z)*cc
                Ey = fEy3D(x, y, z)*cc
                Ez = fEz3D(x, y, z)*cc
                # return normalied E and B
                return [gamma*Ex, gamma*Ey, Ez, beta*gamma*Ey, -beta*gamma*Ex, 0]

        self.EM3D = EM3D
        return
"""

"""
# Backup on July 31 2019
class SpaceCharge3DFFT():
    def __init__(self, nx = 32, ny = 32, nz = 32, N_cutoff = 5, debug = 0,\
                 Lspch = True, Lmirror = False, Lbin = False, Lemit = False, Nbin = 1):
        
        self.nx = nx; self.ny = ny; self.nz = nz
        self.N_cutoff = N_cutoff       
        self.debug = debug
        
        self.Lspch = Lspch; self.Lmirror = Lmirror; self.Lemit = False
        self.Lbin = Lbin; self.Nbin = Nbin
        
        return
    def __del__(self):
        print 'SpaceCharge3DFFT instance is dead!'
    
    def get_rho_3D0(self, sigma_x = 1e-3, sigma_y = 1e-3, sigma_z = 1e-3, gamma0 = 1, Qtot = 100e-12):
        '''
        Set charge density to 3D gaussian distributions
        '''
        self.Qtot = Qtot
        self.gamma0 = gamma0
        
        sigma_z_static = sigma_z*gamma0
         
        Nx, Ny, Nz = self.nx, self.ny, self.nz
        N_cutoff = self.N_cutoff
        
        self.gamma0 = gamma0
        
        def rho_3D((x_static, y_static, z_static)):
            xc, yc, zc = 0, 0, 0
            f_x = 1./np.sqrt(2.*np.pi)/sigma_x*np.exp(-(x_static-xc)**2/2./sigma_x**2)
            f_y = 1./np.sqrt(2.*np.pi)/sigma_y*np.exp(-(y_static-yc)**2/2./sigma_y**2)
            f_z = 1./np.sqrt(2.*np.pi)/sigma_z*np.exp(-(z_static-zc)**2/2./sigma_z_static**2)
            
            return f_x*f_y*f_z
        
        xgrid = np.linspace(-N_cutoff*sigma_x, N_cutoff*sigma_x, Nx+1)
        ygrid = np.linspace(-N_cutoff*sigma_y, N_cutoff*sigma_y, Ny+1)
        zgrid_static = np.linspace(-N_cutoff*sigma_z_static, N_cutoff*sigma_z_static, Nz+1)
        
        XX = np.meshgrid(xgrid, ygrid, zgrid_static, indexing = 'ij')        
        rho = rho_3D(XX)
        
        self.dx = N_cutoff*2*sigma_x/Nx
        self.dy = N_cutoff*2*sigma_y/Ny
        self.dz = N_cutoff*2*sigma_z/Nz
        self.dz_static = self.dz*gamma0
        self.sigma_x = sigma_x
        self.sigma_y = sigma_y
        self.sigma_z = sigma_z
        self.sigma_z_static = self.sigma_z*gamma0
        
        self.xgrid = xgrid; self.ygrid = ygrid; self.zgrid_static = zgrid_static
        self.zmean = 0; self.zref = 0
        self.rho = rho
        return
    def get_rho_3D1(self, R = 1e-3, gamma0 = 1, Qtot = 100e-12):
        '''
        Set charge density to 3D uniform distributions confined in a sphere
        '''
        self.Qtot = Qtot
        self.gamma0 = gamma0
        
        sigma_x = R; sigma_y = R; sigma_z = R
        sigma_z_static = sigma_z*gamma0
        
        Nx, Ny, Nz = self.nx, self.ny, self.nz
        N_cutoff = self.N_cutoff  
        
        def rho_3D((x_static, y_static, z_static)):
            radius = R #*np.sqrt(15)/5.0;
            xc, yc, zc = 0, 0, 0
            r = np.sqrt((x_static-xc)**2+(y_static-yc)**2+(z_static/gamma0-zc)**2)
            rho = np.where(r>radius, 0, 3./(4*np.pi*radius**3))
            return rho
        
        xgrid = np.linspace(-N_cutoff*sigma_x, N_cutoff*sigma_x, Nx+1)
        ygrid = np.linspace(-N_cutoff*sigma_y, N_cutoff*sigma_y, Ny+1)
        zgrid_static = np.linspace(-N_cutoff*sigma_z_static, N_cutoff*sigma_z_static, Nz+1)
        
        XX = np.meshgrid(xgrid, ygrid, zgrid_static, indexing = 'ij')        
        rho = rho_3D(XX)
        
        self.dx = N_cutoff*2*sigma_x/Nx
        self.dy = N_cutoff*2*sigma_y/Ny
        self.dz = N_cutoff*2*sigma_z/Nz
        self.dz_static = self.dz*gamma0
        self.sigma_x = sigma_x
        self.sigma_y = sigma_y
        self.sigma_z = sigma_z
        self.sigma_z_static = self.sigma_z*gamma0
        
        self.xgrid = xgrid; self.ygrid = ygrid; self.zgrid_static = zgrid_static
        self.zgrid = zgrid_static/gamma0
        
        self.zmean = 0; self.zref = 1.5*R
        self.rho = rho*Qtot
        return
    
    def get_rho_3D(self, dist, nx = None, ny = None, nz = None, N_cutoff = None,\
                   Lmirror = None, Lemit = None, format = 'Astra', fig_ext = '.eps'):
        '''
        Parameters
          dist: particle distributions in the same format as Astra, but absolute `z` and `Pz*c`
        '''
        if nx is not None:
            self.nx = nx
        if ny is not None:
            self.ny = ny
        if nz is not None:
            self.nz = nz
        Nx = self.nx; Ny = self.ny; Nz = self.nz
        if N_cutoff is not None:
            self.N_cutoff = N_cutoff
        N_cutoff = self.N_cutoff
        
        if Lmirror is not None:
            self.Lmirror = Lmirror
        if Lemit is not None:
            self.Lemit = Lemit
        
        if format == 'Astra' or format == 'ASTRA' or format == 'astra':
            ix, iy, iz, iux, iuy, iuz, iw = 0, 1, 2, 3, 4, 5, 7
                       
            # Select active partiles
            select = (dist[:,9]>0); dist = dist[select]
            x = dist[:,ix]
            y = dist[:,iy]
            z = dist[:,iz]
            w = dist[:,iw]*1e-9; self.Qtot = np.sum(w)
            
            self.zmean =  weighted_mean(z, w)
            #self.zmean = (np.max(z)+np.min(z))*0.5
            #z -= self.zmean
            
            bgx, bgy, bgz = dist[:,iux]/g_mec2/1e6, dist[:,iuy]/g_mec2/1e6, dist[:,iuz]/g_mec2/1e6
            gamma = np.sqrt(1+bgx**2+bgy**2+bgz**2)
            
            gamma0 = weighted_mean(gamma, w)
            #z_static = z*gamma0
        
        self.gamma0 = gamma0;              #print inspect.stack()[0][3], 'gamma = ', self.gamma0
        self.sigma_x = weighted_std(x, w); #print inspect.stack()[0][3], 'sigma_x = ', self.sigma_x
        self.sigma_y = weighted_std(y, w); #print inspect.stack()[0][3], 'sigma_y = ', self.sigma_y
        self.sigma_z = weighted_std(z, w); #print inspect.stack()[0][3], 'sigma_z_static = ', self.sigma_z_static
        self.sigma_z_static = self.sigma_z*gamma0;       #print inspect.stack()[0][3], 'sigma_z = ', self.sigma_z
        
        xmin = np.max([np.min(x), -N_cutoff*self.sigma_x])
        xmax = np.min([np.max(x),  N_cutoff*self.sigma_x])
        ymin = np.max([np.min(y), -N_cutoff*self.sigma_y])
        ymax = np.min([np.max(y),  N_cutoff*self.sigma_y])
        N_cutoff_z = 5
        zmin = np.max([np.min(z), self.zmean-N_cutoff_z*self.sigma_z])
        zmax = np.min([np.max(z), self.zmean+N_cutoff_z*self.sigma_z])
        
        #zmin = np.min(z)
        #zmax = np.max(z)
        nx0 = 1 # Guarding grids on one side
        ny0 = 1
        nz0 = 1
        if self.Lemit:
            zmin = 0
            zmax = np.max(z)
            #zmin = -self.zmean
            nz0 = 2.5
        #print inspect.stack()[0][3], zmin, zmax, self.zmean
        
        #import pdb; pdb.set_trace()
        self.zmean = (zmin+zmax)*0.5
        z -= self.zmean
        z_static = z*gamma0
        
        zmax -= self.zmean
        zmin -= self.zmean
        
        hx = (xmax-xmin)/(Nx-2*nx0); hy = (ymax-ymin)/(Ny-2*ny0)
        hz = (zmax-zmin)/(Nz-2*nz0)
        
        zmin_static, zmax_static = zmin*gamma0, zmax*gamma0
        hz_static = hz*gamma0
        
        self.dx = hx; self.dy = hy; self.dz = hz; #print inspect.stack()[0][3], 'mesh sizes: ', hx, hy, hz
        self.dz_static = hz_static
        
        if self.debug and 0:
            print inspect.stack()[0][3], 'Qtot = ', self.Qtot
            print inspect.stack()[0][3], 'gamma = ', self.gamma0
            print inspect.stack()[0][3], 'sigma_x = ', self.sigma_x
            print inspect.stack()[0][3], 'sigma_y = ', self.sigma_y
            print inspect.stack()[0][3], 'sigma_z_static = ', self.sigma_z_static
            print inspect.stack()[0][3], 'sigma_z = ', self.sigma_z
            print inspect.stack()[0][3], 'mesh sizes in rest frame: ', hx, hy, hz_static
        
        xgrid = np.linspace(xmin-nx0*hx, xmax+nx0*hx, Nx+1)
        ygrid = np.linspace(ymin-ny0*hy, ymax+ny0*hy, Ny+1)
        zgrid = np.linspace(zmin-nz0*hz, zmax+nz0*hz, Nz+1)
        #import pdb; pdb.set_trace()
        zgrid_static = zgrid*gamma0
        
        ranges = [(xgrid[0]-0.5*hx, xgrid[-1]+0.5*hx), (ygrid[0]-0.5*hy, ygrid[-1]+0.5*hy),\
                  (zgrid_static[0]-0.5*hz_static, zgrid_static[-1]+0.5*hz_static)]
        rho, edges = np.histogramdd((x, y, z_static), bins = (Nx+1, Ny+1, Nz+1), range = ranges, weights = w)
        del edges
        
        if self.debug:
            #print xgrid/self.sigma_x, ygrid/self.sigma_y, zgrid/self.sigma_z_static
            #print rho
            #print ranges
            fig, [ax1, ax2, ax3] = plt.subplots(ncols = 3, figsize = (12, 4))
            rho_x = np.sum(np.sum(rho, axis = 1), axis = 1)
            ax1.plot(xgrid*1e3, rho_x, '-*')
            ax1.grid()
            ax1.set_xlabel(r'$x$ (mm)')
            ax1.set_ylabel(r'$\rho$ (arb. unit)')
            
            [aX,aY] = np.meshgrid(xgrid, ygrid)
            data = np.abs(rho[:,:,Nz/2-1])
            v = np.linspace(0.05, 1., 96)
            ax2.contourf(aX*1e3, aY*1e3, data/np.max(data), v, linestyles = None)
            ax2.grid()
            ax2.set_xlabel(r'$x$ (mm)')
            ax2.set_ylabel(r'$y$ (mm)')
            
            rho_z = np.sum(np.sum(rho, axis = 0), axis = 0)
            ax3.plot(zgrid*1e3, rho_z, '-*')
            ax3.grid()
            ax3.set_xlabel(r'$z$ (mm)')
            ax3.set_ylabel(r'$\rho$ (arb. unit)')
            
            fig.savefig('charge-density'+fig_ext)
        
        #rho = sgolay2d(rho, window_size=11, order=4)   
        #self.rho_RZ = interp2d(rr, zz, rho.T, kind = 'cubic')
        self.xgrid = xgrid
        self.ygrid = ygrid
        self.zgrid = zgrid
        self.zgrid_static = zgrid_static
        self.rho = rho/(hx*hy*hz_static)
        return
    def build(self, Xshift = (0, 0, 0), **kwargs):
        if 'fig_ext' in kwargs.keys():
            fig_ext = kwargs['fig_ext'];
        else:
            fig_ext = '.eps'
        
        Nx = self.nx; Ny = self.ny; Nz = self.nz; 
        sigma_x = self.sigma_x; sigma_y = self.sigma_y; sigma_z_static = self.sigma_z_static
        #hx = self.dx; hy = self.dy; hz = self.dz_static
        
        #if N_cutoff is None:
        #    N_cutoff = self.N_cutoff
        #N_cutoff = self.N_cutoff
        
        if self.Lspch:
            # Create arrays for GFs and densities
            GFC = np.zeros((2*Nx, 2*Ny, 2*Nz))
            RHOC = np.zeros((2*Nx, 2*Ny, 2*Nz))
            
            xgrid = self.xgrid
            ygrid = self.ygrid
            zgrid_static = self.zgrid_static
            RHOC[0:Nx+1, 0:Ny+1, 0:Nz+1] = self.rho
            
            hx = xgrid[1]-xgrid[0]; hy = ygrid[1]-ygrid[0]; hz_static = zgrid_static[1]-zgrid_static[0]
            if self.debug:
                print inspect.stack()[0][3], Nx, Ny, Nz
                print inspect.stack()[0][3], hx, hy, hz_static

            ## using vector to calculate GFs
            ## Calculate GFs by using the mapping, faster
            xf, yf, zf = Xshift
            xx = xf-Nx*hx+np.arange(2*Nx+1)*hx-0.5*hx
            yy = yf-Ny*hy+np.arange(2*Ny+1)*hy-0.5*hy
            zz_static = zf*self.gamma0-Nz*hz_static+np.arange(2*Nz+1)*hz_static-0.5*hz_static
            XX = np.meshgrid(xx, yy, zz_static, indexing = 'ij')
            IGF_MAP = IGF3D(XX)

            ii = np.mod(np.arange(2*Nx)+Nx, 2*Nx)
            jj = np.mod(np.arange(2*Ny)+Ny, 2*Ny)
            kk = np.mod(np.arange(2*Nz)+Nz, 2*Nz)
            II = np.meshgrid(ii, jj, kk, indexing = 'ij')
            GFC = cyclic_IGF3D_mapping(II, IGF_MAP)

            ## FFT convolution to get the static potenital: PHI
            GFC_FFT = fftn(GFC)
            RHOC_FFT = fftn(RHOC)
            PHIC_FFT = GFC_FFT*RHOC_FFT

            PHIC = ifftn(PHIC_FFT)
            self.PHI = PHIC[:Nx+1,:Ny+1,:Nz+1]
            
        if self.Lspch and self.Lmirror:
            #zf = (self.zref+self.zmean)*2; print zf
            zf = (0+self.zmean)*2; #print zf
            
            GFC_mirror = np.zeros((2*Nx, 2*Ny, 2*Nz))
            RHOC_mirror = np.zeros((2*Nx, 2*Ny, 2*Nz))
            
            RHOC_mirror[0:Nx+1, 0:Ny+1, 0:Nz+1] = self.rho[:,:,::-1]
            
            if self.debug:
                print inspect.stack()[0][3], Nx, Ny, Nz
                print inspect.stack()[0][3], hx, hy, hz_static
            
            ## using vector to calculate GFs
            ## Calculate GFs by using the mapping, faster
            xx = xf-Nx*hx+np.arange(2*Nx+1)*hx-0.5*hx
            yy = yf-Ny*hy+np.arange(2*Ny+1)*hy-0.5*hy
            zz_static = zf*self.gamma0-Nz*hz_static+np.arange(2*Nz+1)*hz_static-0.5*hz_static
            XX = np.meshgrid(xx, yy, zz_static, indexing = 'ij')
            IGF_mirror_MAP = IGF3D(XX)
            
            ii = np.mod(np.arange(2*Nx)+Nx, 2*Nx)
            jj = np.mod(np.arange(2*Ny)+Ny, 2*Ny)
            kk = np.mod(np.arange(2*Nz)+Nz, 2*Nz)
            II = np.meshgrid(ii, jj, kk, indexing = 'ij')
            GFC_mirror = cyclic_IGF3D_mapping(II, IGF_mirror_MAP)

            ## FFT convolution to get the static potenital: PHI
            GFC_mirror_FFT = fftn(GFC_mirror)
            RHOC_mirror_FFT = fftn(RHOC_mirror)
            PHIC_mirror_FFT = GFC_mirror_FFT*RHOC_mirror_FFT

            PHIC_mirror = ifftn(PHIC_mirror_FFT)
            self.PHI_mirror = PHIC_mirror[:Nx+1,:Ny+1,:Nz+1]
        
        if self.Lemit and self.Lmirror:
            
            Ex_static, Ey_static, Ez_static = np.gradient(self.PHI.real, hx, hy, hz_static) 
            Ex_static_m, Ey_static_m, Ez_static_m = np.gradient(self.PHI_mirror.real, hx, hy, hz_static) 
            cc = -1./4/np.pi/g_eps0
        
            print inspect.stack()[0][3], 'Qtot = ', self.Qtot
            fig, [ax1, ax2] = plt.subplots(figsize = (12, 4), ncols = 2)
            ax1.plot(xgrid/sigma_x, Ex_static[:, Ny/2-1, Nz/4-1]*cc/1e3, '-*')
            ax1.plot(xgrid/sigma_x, -Ex_static_m[:, Ny/2-1, Nz/4-1]*cc/1e3, '-o')
            ax1.set_xlabel(r'$x/\sigma_x$')
            ax1.set_ylabel(r'$E_x(x)$ (kV/m)')
            ax1.grid()
            
            ax2.plot((self.zgrid+self.zmean)*1e3, Ez_static[Nx/2-1, Ny/2-1, :]*cc/1e3, '-*')
            ax2.plot((self.zgrid+self.zmean)*1e3, -Ez_static_m[Nx/2-1, Ny/2-1, :]*cc/1e3, '-o')
            ax2.plot((self.zgrid+self.zmean)*1e3, Ez_static[Nx/2-1, Ny/2-1, :]*cc/1e3-Ez_static_m[Nx/2-1, Ny/2-1, :]*cc/1e3, '--')
            #print Ez_static[Nx/2-1, Ny/2-1, :]*cc/1e3
            #print -Ez_static_m[Nx/2-1, Ny/2-1, :]*cc/1e3
            ax2.set_xlabel(r'$z$ (mm)')
            ax2.set_ylabel(r'$E_z(z)$ (kV/m)')
            ax2.set_xlim(-0.02, 0.03)
            ax2.grid()
            plt.tight_layout()
            fig.savefig('fields-on-axis'+fig_ext)
            
            fig, [ax1, ax2, ax3] = plt.subplots(figsize = (12, 4), ncols = 3)
            ax1.plot(GFC[Nx/2-1, Ny/2-1, :], '-*')
            ax1.plot(GFC_mirror[Nx/2-1, Ny/2-1, :], '-o')
            ax1.grid()
            ax1.set_xlabel(r'grid number')
            ax1.set_title(r'green function')
            
            to_astra_unit = self.dx*self.dy*self.dz_static/self.dz*1e9
            ax2.plot((self.zgrid+self.zmean)*1e3, np.sum(np.sum(RHOC[:,:,:Nz+1], axis = 0), axis = 0)*to_astra_unit, '-*')
            ax2.plot((self.zgrid+self.zmean)*1e3, np.sum(np.sum(RHOC_mirror[:,:,:Nz+1], axis = 0), axis = 0)*to_astra_unit, '-o')
            ax2.set_xlim(-0.02, 0.03)
            ax2.grid()
            ax2.set_xlabel(r'$z$ (mm)')
            ax2.set_title(r'charge density (nC/m)')
            
            ax3.plot(self.PHI[Nx/2-1, Ny/2-1, :], '-*')
            ax3.plot(self.PHI_mirror[Nx/2-1, Ny/2-1, :], '-o')
            ax3.grid()
            ax3.set_xlabel(r'grid number')
            ax3.set_title(r'static potential')
            plt.tight_layout()
            fig.savefig('green-function'+fig_ext)
            
            #plt.close('all')
            
        if self.Lspch and self.Lmirror:
            self.PHI -= self.PHI_mirror
        
        del GFC, RHOC, GFC_FFT, RHOC_FFT, PHIC_FFT, IGF_MAP
        if self.Lmirror:
            del GFC_mirror, RHOC_mirror, GFC_mirror_FFT, RHOC_mirror_FFT, PHIC_mirror_FFT, IGF_mirror_MAP
        del XX, II
        
        Ex_static, Ey_static, Ez_static = np.gradient(self.PHI.real, hx, hy, hz_static)     
        cc = -1./4/np.pi/g_eps0
        
        if self.debug and 0:
            print inspect.stack()[0][3], 'Qtot = ', self.Qtot
            fig, [ax1, ax2] = plt.subplots(figsize = (12, 4), ncols = 2)
            ax1.plot(xgrid/sigma_x, Ex_static[:, Ny/2-1, Nz/4-1]*cc/1e3, '-*')
            ax1.set_xlabel(r'$x/\sigma_x$')
            ax1.set_ylabel(r'$E_x(x)$ (kV/m)')
            ax1.grid()
            ax2.plot(zgrid_static/sigma_z_static, Ez_static[Nx/2-1, Ny/2-1, :]*cc/1e3, '-*')
            ax2.set_xlabel(r'$z/\sigma_z$')
            ax2.set_ylabel(r'$E_z(z)$ (kV/m)')
            ax2.grid()
            
            plt.tight_layout()
            fig.savefig('fields-on-axis.eps')
            
            fig, [ax1, ax2, ax3] = plt.subplots(figsize = (12, 4), ncols = 3)
            ax1.plot(GFC[Nx/2-1, Ny/2-1, :], '-*')
            ax1.grid()
            ax1.set_xlabel(r'grid number')
            ax1.set_title(r'green function')
            ax2.plot(RHOC[Nx/2-1, Ny/2-1, :], '-*')
            ax2.grid()
            ax2.set_xlabel(r'grid number')
            ax2.set_title(r'charge density')
            ax3.plot(self.PHI[Nx/2-1, Ny/2-1, :]*cc*(-1), '-*')
            ax3.grid()
            ax3.set_xlabel(r'grid number')
            ax3.set_title(r'static potential')
            
            plt.tight_layout()
            fig.savefig('green-function.eps')
        
        fEx3D_static = RegularGridInterpolator((xgrid, ygrid, zgrid_static), Ex_static, bounds_error = False, fill_value = None)
        fEy3D_static = RegularGridInterpolator((xgrid, ygrid, zgrid_static), Ey_static, bounds_error = False, fill_value = None)
        fEz3D_static = RegularGridInterpolator((xgrid, ygrid, zgrid_static), Ez_static, bounds_error = False, fill_value = None)
        
        cc = 1./4/np.pi/g_eps0 # 
        gamma0 = self.gamma0; beta = gamma2beta(gamma0); zmean = self.zmean
        def EM3D(x, y, z, t = 0):
            if np.isscalar(x):
                x = [x]
            if np.isscalar(y):
                y = [y]
            if np.isscalar(z):
                z = [z]
            
            x = np.asarray(x)
            y = np.asarray(y)
            z = np.asarray(z)
            z_static = (z-zmean)*gamma0
            
            n = np.max([len(x), len(y), len(z)])
            
            Ex_static = fEx3D_static((x, y, z_static))*cc
            Ey_static = fEy3D_static((x, y, z_static))*cc
            Ez_static = fEz3D_static((x, y, z_static))*cc
            
            # return normalied E and B in lab frame
            F2d = np.zeros((n, 6))
            F2d[:,0] = gamma0*Ex_static
            F2d[:,1] = gamma0*Ey_static
            F2d[:,2] = Ez_static
            F2d[:,3] = beta*gamma0*Ey_static/g_c
            F2d[:,4] =-beta*gamma0*Ex_static/g_c
            
            return F2d
        
        self.EM3D = EM3D
        return
    
    def get_rho_3D_binned(self, dist, nx = None, ny = None, nz = None, N_cutoff = None,\
                          nbins = 4, Lmirror = None, Lemit = None, format = 'Astra'):
        '''
        Parameters
          dist: particle distributions in the same format as Astra, but absolute `z` and `Pz*c`
        '''
        if nx is not None:
            self.nx = nx
        if ny is not None:
            self.ny = ny
        if nz is not None:
            self.nz = nz
        Nx = self.nx; Ny = self.ny; Nz = self.nz
        if N_cutoff is not None:
            self.N_cutoff = N_cutoff
        N_cutoff = self.N_cutoff
        
        if Lmirror is not None:
            self.Lmirror = Lmirror
        if Lemit is not None:
            self.Lemit = Lemit
        
        if format == 'Astra' or format == 'ASTRA' or format == 'astra':
            ix, iy, iz, iux, iuy, iuz, iw = 0, 1, 2, 3, 4, 5, 7
            
            self.zref = dist[0,iz]
            
            # Select active partiles
            select = (dist[:,9]>0); dist = dist[select]
            x = dist[:,ix]
            y = dist[:,iy]
            z = dist[:,iz]
            w = dist[:,iw]*1e-9
            
            self.zmean =  weighted_mean(z, w); print self.zmean
            self.zmean = (np.max(z)+np.min(z))*0.5; print self.zmean
            z -= self.zmean
            
            bgx, bgy, bgz = dist[:,iux]/g_mec2/1e6, dist[:,iuy]/g_mec2/1e6, dist[:,iuz]/g_mec2/1e6
            
            gamma = np.sqrt(1+bgx**2+bgy**2+bgz**2)
            gamma0 = weighted_mean(gamma, w); std_gamma0 = weighted_std(gamma, w)
        
        self.gamma0 = gamma0;              #print inspect.stack()[0][3], 'gamma = ', self.gamma0
        self.sigma_x = weighted_std(x, w); #print inspect.stack()[0][3], 'sigma_x = ', self.sigma_x
        self.sigma_y = weighted_std(y, w); #print inspect.stack()[0][3], 'sigma_y = ', self.sigma_y
        self.sigma_z = weighted_std(z, w); #print inspect.stack()[0][3], 'sigma_z_static = ', self.sigma_z_static
        
        xmin = np.max([np.min(x), -N_cutoff*self.sigma_x])
        xmax = np.min([np.max(x),  N_cutoff*self.sigma_x])
        ymin = np.max([np.min(y), -N_cutoff*self.sigma_y])
        ymax = np.min([np.max(y),  N_cutoff*self.sigma_y])
        zmin = np.max([np.min(z), -N_cutoff*self.sigma_z])
        zmax = np.min([np.max(z),  N_cutoff*self.sigma_z])
        
        zmin = np.min(z)
        zmax = np.max(z)
        nx0 = 1 # Guarding grids on one side
        ny0 = 1
        nz0 = 1
        if self.Lemit:
            zmin = -self.zmean
            nz0 = 2
        #print inspect.stack()[0][3], zmin, zmax, self.zmean
        
        hx = (xmax-xmin)/(Nx-2*nx0); hy = (ymax-ymin)/(Ny-2*ny0)
        hz = (zmax-zmin)/(Nz-2*nz0)
        
        zmin_static, zmax_static = zmin*gamma0, zmax*gamma0
        hz_static = hz*gamma0
        
        self.dx = hx; self.dy = hy; self.dz = hz; #print inspect.stack()[0][3], 'mesh sizes: ', hx, hy, hz
        self.dz_static = hz_static
        
        # Select sub-group of particles
        bg = np.sqrt(gamma**2-1)
        bg0 = weighted_mean(bg, w); std_bg0 = weighted_std(bg, w)
        #print bg0, std_bg0
        
        s1 = (bg<bg0-std_bg0)
        s2 = (bg>=bg0-std_bg0)*(bg<bg0)
        s3 = (bg>=bg0)*(bg<bg0+std_bg0)
        s4 = (bg>=bg0+std_bg0)
        ss = [s1, s2, s3, s4]
        
        Qtot = []
        gamma0 = []
        rho = []
        zgrid_static = []
        
        xgrid = np.linspace(xmin-nx0*hx, xmax+nx0*hx, Nx+1)
        ygrid = np.linspace(ymin-ny0*hy, ymax+ny0*hy, Ny+1)
        zgrid = np.linspace(zmin-nz0*hz, zmax+nz0*hz, Nz+1)
        
        
        fig, ax = plt.subplots()
        ax.grid()
        for si in ss:
            #print [np.sum(i) for i in ss]
            xi = x[si]
            yi = y[si]
            zi = z[si]
            wi = w[si]
            gi = gamma[si]
            
            ax.plot(zi*1e3, bg[si], '.')
            #import pdb; pdb.set_trace()
            g0i = weighted_mean(gi, wi)
            q0i = np.sum(wi)
            
            hz_static = hz*g0i   
            zmin_static, zmax_static = zmin*g0i, zmax*g0i      
            zgridi_static = np.linspace(zmin_static-nz0*hz_static, zmax_static+nz0*hz_static, Nz+1)
            
            ranges = [(xgrid[0]-0.5*hx, xgrid[-1]+0.5*hx), (ygrid[0]-0.5*hy, ygrid[-1]+0.5*hy),\
                      (zgridi_static[0]-0.5*hz_static, zgridi_static[-1]+0.5*hz_static)]
            #print ranges
            rhoi, edges = np.histogramdd((xi, yi, zi*g0i), bins = (Nx+1, Ny+1, Nz+1), range = ranges, weights = wi)
            
            gamma0.append(g0i)
            Qtot.append(q0i)
            zgrid_static.append(zgridi_static)
            rho.append(rhoi/(hx*hy*hz_static))
            
            del zgridi_static
            del rhoi
        
        self.dx = hx; self.dy = hy; self.dz = hz 
        #print inspect.stack()[0][3], 'mesh sizes: ', hx, hy, hz
        
        if self.debug:
            fig, [ax1, ax2] = plt.subplots(ncols = 2, figsize = (12, 4))
            add = np.zeros(len(zgrid))
            for i in np.arange(4):
                #data = np.abs(rho[i][Nx/2-1, Ny/2-1, :]*gamma0[i])
                rhoi_z = np.abs(np.sum(np.sum(rho[i], axis = 0), axis = 0)*gamma0[i])
                add += rhoi_z
                ax1.plot(zgrid*1e3, rhoi_z, label = '%d' % i)
            ax1.plot(zgrid*1e3, add, '--', label = 'all')
            ax1.grid()
            ax1.legend()
            ax1.set_xlabel(r'$z$ (mm)')
            ax1.set_ylabel(r'$\rho$ (arb. unit)')
            
            add = np.zeros(len(xgrid))
            for i in np.arange(4):
                rhoi_x = np.abs(np.sum(np.sum(rho[i], axis = 1), axis = 1)*gamma0[i])
                add += rhoi_x
                ax2.plot(xgrid*1e3, rhoi_x, label = '%d' % i)
            ax2.plot(xgrid*1e3, add, '--', label = 'add')
            ax2.grid()
            ax2.legend()
            ax2.set_xlabel(r'$x$ (mm)')
            ax2.set_ylabel(r'$\rho$ (arb. unit)')
            
            fig.savefig('charge-density.eps')
        
        # Save 
        self.Qtot = Qtot
        self.gamma0 = gamma0
        
        self.xgrid = xgrid
        self.ygrid = ygrid
        self.zgrid = zgrid
        
        self.rho = rho
        
        return  
    def build_binned(self, Xshift = (0, 0, 0), N_cutoff = None, nbins = 4):
        
        Nx = self.nx; Ny = self.ny; Nz = self.nz; 
        sigma_x = self.sigma_x; sigma_y = self.sigma_y; sigma_z = self.sigma_z
        #hx = self.dx; hy = self.dy; hz = self.dz_static
        
        if N_cutoff is None:
            N_cutoff = self.N_cutoff
        
        xgrid = self.xgrid
        ygrid = self.ygrid
        zgrid = self.zgrid
        hx = xgrid[1]-xgrid[0]; hy = ygrid[1]-ygrid[0]; hz = zgrid[1]-zgrid[0]
        
        if self.Lspch:
            # Create arrays for GFs and densities
            nbins = len(self.Qtot)
            self.PHI = []
            for i in np.arange(nbins):
                GFC = np.zeros((2*Nx, 2*Ny, 2*Nz))
                RHOC = np.zeros((2*Nx, 2*Ny, 2*Nz))
                
                RHOC[0:Nx+1, 0:Ny+1, 0:Nz+1] = self.rho[i]
                
                zgrid_static = self.zgrid*self.gamma0[i]
                hz_static = zgrid_static[1]-zgrid_static[0]
                
                if self.debug:
                    print inspect.stack()[0][3], Nx, Ny, Nz
                    print inspect.stack()[0][3], hx, hy, hz_static
                
                ## using vector to calculate GFs
                ## Calculate GFs by using the mapping, faster
                xf, yf, zf = Xshift
                xx = xf-Nx*hx+np.arange(2*Nx+1)*hx-0.5*hx
                yy = yf-Ny*hy+np.arange(2*Ny+1)*hy-0.5*hy
                zz_static = zf*self.gamma0[i]-Nz*hz_static+np.arange(2*Nz+1)*hz_static-0.5*hz_static
                XX = np.meshgrid(xx, yy, zz_static, indexing = 'ij')
                IGF_MAP = IGF3D(XX)
                
                ii = np.mod(np.arange(2*Nx)+Nx, 2*Nx)
                jj = np.mod(np.arange(2*Ny)+Ny, 2*Ny)
                kk = np.mod(np.arange(2*Nz)+Nz, 2*Nz)
                II = np.meshgrid(ii, jj, kk, indexing = 'ij')
                GFC = cyclic_IGF3D_mapping(II, IGF_MAP)
                
                ## FFT convolution to get the static potenital: PHI
                GFC_FFT = fftn(GFC)
                RHOC_FFT = fftn(RHOC)
                PHIC_FFT = GFC_FFT*RHOC_FFT
                
                PHIC = ifftn(PHIC_FFT)
                
                self.PHI.append(PHIC[:Nx+1,:Ny+1,:Nz+1])
        
        if self.Lspch and self.Lmirror:
            #zf = (self.zref+self.zmean)*2; print zf
            zf = (0+self.zmean)*2; #print zf
            
            nbins = len(self.Qtot)
            self.PHI_mirror = []
            for i in np.arange(nbins):
                
                GFC = np.zeros((2*Nx, 2*Ny, 2*Nz))
                RHOC_mirror = np.zeros((2*Nx, 2*Ny, 2*Nz))
                
                zgrid_static = self.zgrid*self.gamma0[i]
                hz_static = zgrid_static[1]-zgrid_static[0]
                
                RHOC_mirror[0:Nx+1, 0:Ny+1, 0:Nz+1] = self.rho[i][:,:,::-1]
                #RHOC_mirror = RHOC[:,:,::-1]
                
                if self.debug:
                    print inspect.stack()[0][3], Nx, Ny, Nz
                    print inspect.stack()[0][3], hx, hy, hz_static
                
                ## using vector to calculate GFs
                ## Calculate GFs by using the mapping, faster
                g0i = self.gamma0[i]
                xx = xf-Nx*hx+np.arange(2*Nx+1)*hx-0.5*hx
                yy = yf-Ny*hy+np.arange(2*Ny+1)*hy-0.5*hy
                zz_static = zf*g0i-Nz*hz_static+np.arange(2*Nz+1)*hz_static-0.5*hz_static
                XX = np.meshgrid(xx, yy, zz_static, indexing = 'ij')
                IGF_MAP = IGF3D(XX)
                
                ii = np.mod(np.arange(2*Nx)+Nx, 2*Nx)
                jj = np.mod(np.arange(2*Ny)+Ny, 2*Ny)
                kk = np.mod(np.arange(2*Nz)+Nz, 2*Nz)
                II = np.meshgrid(ii, jj, kk, indexing = 'ij')
                GFC = cyclic_IGF3D_mapping(II, IGF_MAP)
                
                ## FFT convolution to get the static potenital: PHI
                GFC_FFT = fftn(GFC)
                RHOC_FFT = fftn(RHOC_mirror)
                PHIC_FFT = GFC_FFT*RHOC_FFT
                
                PHIC_mirror = ifftn(PHIC_FFT)
                self.PHI_mirror.append(PHIC_mirror[:Nx+1,:Ny+1,:Nz+1])
                
                self.PHI[i] -= self.PHI_mirror[i]
        
        # Field interpolation
        cc = -1./4/np.pi/g_eps0
        fEx3D_static = []
        fEy3D_static = []
        fEz3D_static = []
        for i in np.arange(nbins):
            Ex_static, Ey_static, Ez_static = np.gradient(self.PHI[i], hx, hy, hz*self.gamma0[i])     
        
            fEx3Di_static = RegularGridInterpolator((xgrid, ygrid, zgrid*self.gamma0[i]), Ex_static,                                                    bounds_error = False, fill_value = None)
            fEy3Di_static = RegularGridInterpolator((xgrid, ygrid, zgrid*self.gamma0[i]), Ey_static,                                                    bounds_error = False, fill_value = None)
            fEz3Di_static = RegularGridInterpolator((xgrid, ygrid, zgrid*self.gamma0[i]), Ez_static,                                                    bounds_error = False, fill_value = None)
            
            fEx3D_static.append(fEx3Di_static)
            fEy3D_static.append(fEy3Di_static)
            fEz3D_static.append(fEz3Di_static)
        
        if self.debug and 0:
            print inspect.stack()[0][3], 'Qtot = ', self.Qtot
            fig, [ax1, ax2] = plt.subplots(figsize = (12, 4), ncols = 2)
            ax1.plot(xgrid/sigma_x, Ex_static[:, Ny/2-1, Nz/4-1]*cc/1e3, '-*')
            ax1.set_xlabel(r'$x/\sigma_x$')
            ax1.set_ylabel(r'$E_x(x)$ (kV/m)')
            ax1.grid()
            ax2.plot(zgrid_static/sigma_z_static, Ez_static[Nx/2-1, Ny/2-1, :]*cc/1e3, '-*')
            ax2.set_xlabel(r'$z/\sigma_z$')
            ax2.set_ylabel(r'$E_z(z)$ (kV/m)')
            ax2.grid()
            plt.tight_layout()
            fig.savefig('fields-on-axis.eps')
            
            fig, [ax1, ax2, ax3] = plt.subplots(figsize = (12, 4), ncols = 3)
            ax1.plot(GFC[:, Ny/2-1, Nz/2-1], '-*')
            ax1.grid()
            ax1.set_xlabel(r'grid number')
            ax1.set_title(r'green function')
            ax2.plot(RHOC[:, Ny/2-1, Nz/2-1], '-*')
            ax2.grid()
            ax2.set_xlabel(r'grid number')
            ax2.set_title(r'charge density')
            ax3.plot(self.PHI[:, Ny/2-1, Nz/2-1], '-*')
            ax3.grid()
            ax3.set_xlabel(r'grid number')
            ax3.set_title(r'static potential')
            plt.tight_layout()
            fig.savefig('green-function.eps')
        
        gamma0 = self.gamma0; zmean = self.zmean
        def EM3D(x, y, z, t = 0):
            
            if np.isscalar(x):
                x = [x]
            if np.isscalar(y):
                y = [y]
            if np.isscalar(z):
                z = [z]
            
            x = np.asarray(x)
            y = np.asarray(y)
            z = np.asarray(z)
            z_static = (z-zmean)*gamma0
            
            n = np.max([len(x), len(y), len(z)])
            
            F2d = np.zeros((len(x), 6))
            for i in np.arange(nbins):
                g0i = gamma0[i]; b0i = gamma2beta(g0i)
                z_static = (z-zmean)*g0i
                Ex_static = fEx3D_static[i]((x, y, z_static))*cc
                Ey_static = fEy3D_static[i]((x, y, z_static))*cc
                Ez_static = fEz3D_static[i]((x, y, z_static))*cc
                
                # Normalied E and B in lab frame
                
                F2d[:,0] += g0i*Ex_static
                F2d[:,1] += g0i*Ey_static
                F2d[:,2] += Ez_static
                F2d[:,3] += b0i*g0i*Ey_static/g_c
                F2d[:,4] -= b0i*g0i*Ex_static/g_c           
                #F2D = np.add(F2D, [g0i*Ex_static, g0i*Ey_static, Ez_static, b0i*g0i*Ey_static, -b0i*g0i*Ex_static, 0])
            
            return F2D
        
        self.EM3D = EM3D
        return
"""

"""
# Before modifying binned energy
class SpaceCharge3DFFT():
    def __init__(self, nx = 32, ny = 32, nz = 32, N_cutoff = 5, debug = 0,\
                 Lspch = True, Lmirror = False, Lbin = False, Lemit = False, Nbin = 1):
        
        self.nx = nx; self.ny = ny; self.nz = nz
        self.N_cutoff = N_cutoff       
        self.debug = debug
        
        self.Lspch = Lspch; self.Lmirror = Lmirror; self.Lemit = False
        self.Lbin = Lbin; self.Nbin = Nbin
        
        return
    def __del__(self):
        print 'SpaceCharge3DFFT instance is dead!'
    
    def get_rho_3D0(self, sigma_x = 1e-3, sigma_y = 1e-3, sigma_z = 1e-3, gamma0 = 1, Qtot = 100e-12):
        '''
        Set charge density to 3D gaussian distributions
        '''
        self.Qtot = Qtot
        self.gamma0 = gamma0
        
        sigma_z_static = sigma_z*gamma0
         
        Nx, Ny, Nz = self.nx, self.ny, self.nz
        N_cutoff = self.N_cutoff
        
        self.gamma0 = gamma0
        
        def rho_3D((x_static, y_static, z_static)):
            xc, yc, zc = 0, 0, 0
            f_x = 1./np.sqrt(2.*np.pi)/sigma_x*np.exp(-(x_static-xc)**2/2./sigma_x**2)
            f_y = 1./np.sqrt(2.*np.pi)/sigma_y*np.exp(-(y_static-yc)**2/2./sigma_y**2)
            f_z = 1./np.sqrt(2.*np.pi)/sigma_z*np.exp(-(z_static-zc)**2/2./sigma_z_static**2)
            
            return f_x*f_y*f_z
        
        xgrid = np.linspace(-N_cutoff*sigma_x, N_cutoff*sigma_x, Nx+1)
        ygrid = np.linspace(-N_cutoff*sigma_y, N_cutoff*sigma_y, Ny+1)
        zgrid_static = np.linspace(-N_cutoff*sigma_z_static, N_cutoff*sigma_z_static, Nz+1)
        
        XX = np.meshgrid(xgrid, ygrid, zgrid_static, indexing = 'ij')        
        rho = rho_3D(XX)
        
        self.dx = N_cutoff*2*sigma_x/Nx
        self.dy = N_cutoff*2*sigma_y/Ny
        self.dz = N_cutoff*2*sigma_z/Nz
        self.dz_static = self.dz*gamma0
        self.sigma_x = sigma_x
        self.sigma_y = sigma_y
        self.sigma_z = sigma_z
        self.sigma_z_static = self.sigma_z*gamma0
        
        self.xgrid = xgrid; self.ygrid = ygrid; self.zgrid_static = zgrid_static
        self.zmean = 0; self.zref = 0
        self.rho = rho
        return
    def get_rho_3D1(self, R = 1e-3, gamma0 = 1, Qtot = 100e-12):
        '''
        Set charge density to 3D uniform distributions confined in a sphere
        '''
        self.Qtot = Qtot
        self.gamma0 = gamma0
        
        sigma_x = R; sigma_y = R; sigma_z = R
        sigma_z_static = sigma_z*gamma0
        
        Nx, Ny, Nz = self.nx, self.ny, self.nz
        N_cutoff = self.N_cutoff  
        
        def rho_3D((x_static, y_static, z_static)):
            radius = R #*np.sqrt(15)/5.0;
            xc, yc, zc = 0, 0, 0
            r = np.sqrt((x_static-xc)**2+(y_static-yc)**2+(z_static/gamma0-zc)**2)
            rho = np.where(r>radius, 0, 3./(4*np.pi*radius**3))
            return rho
        
        xgrid = np.linspace(-N_cutoff*sigma_x, N_cutoff*sigma_x, Nx+1)
        ygrid = np.linspace(-N_cutoff*sigma_y, N_cutoff*sigma_y, Ny+1)
        zgrid_static = np.linspace(-N_cutoff*sigma_z_static, N_cutoff*sigma_z_static, Nz+1)
        
        XX = np.meshgrid(xgrid, ygrid, zgrid_static, indexing = 'ij')        
        rho = rho_3D(XX)
        
        self.dx = N_cutoff*2*sigma_x/Nx
        self.dy = N_cutoff*2*sigma_y/Ny
        self.dz = N_cutoff*2*sigma_z/Nz
        self.dz_static = self.dz*gamma0
        self.sigma_x = sigma_x
        self.sigma_y = sigma_y
        self.sigma_z = sigma_z
        self.sigma_z_static = self.sigma_z*gamma0
        
        self.xgrid = xgrid; self.ygrid = ygrid; self.zgrid_static = zgrid_static
        self.zgrid = zgrid_static/gamma0
        
        self.zmean = 0; self.zref = 1.5*R
        self.rho = rho*Qtot
        return
    
    def get_rho_3D(self, dist, nx = None, ny = None, nz = None, N_cutoff = None,\
                   Lmirror = None, Lemit = None, format = 'Astra', fig_ext = '.eps'):
        '''
        Parameters
          dist: particle distributions in the same format as Astra, but absolute `z` and `Pz*c`
        '''
        if nx is not None:
            self.nx = nx
        if ny is not None:
            self.ny = ny
        if nz is not None:
            self.nz = nz
        Nx = self.nx; Ny = self.ny; Nz = self.nz
        if N_cutoff is not None:
            self.N_cutoff = N_cutoff
        N_cutoff = self.N_cutoff
        
        if Lmirror is not None:
            self.Lmirror = Lmirror
        if Lemit is not None:
            self.Lemit = Lemit
        
        if format == 'Astra' or format == 'ASTRA' or format == 'astra':
            ix, iy, iz, iux, iuy, iuz, iw = 0, 1, 2, 3, 4, 5, 7
                       
            # Select active partiles
            select = (dist[:,9]>0); dist = dist[select]
            x = dist[:,ix]
            y = dist[:,iy]
            z = dist[:,iz]
            w = dist[:,iw]*1e-9; self.Qtot = np.sum(w)
            
            zc =  weighted_mean(z, w)
            #self.zmean = (np.max(z)+np.min(z))*0.5
            #z -= self.zmean
            
            bgx, bgy, bgz = dist[:,iux]/g_mec2/1e6, dist[:,iuy]/g_mec2/1e6, dist[:,iuz]/g_mec2/1e6
            gamma = np.sqrt(1+bgx**2+bgy**2+bgz**2)
            
            gamma0 = weighted_mean(gamma, w)
            #z_static = z*gamma0
        
        self.gamma0 = gamma0;              #print inspect.stack()[0][3], 'gamma = ', self.gamma0
        self.sigma_x = weighted_std(x, w); #print inspect.stack()[0][3], 'sigma_x = ', self.sigma_x
        self.sigma_y = weighted_std(y, w); #print inspect.stack()[0][3], 'sigma_y = ', self.sigma_y
        self.sigma_z = weighted_std(z, w); #print inspect.stack()[0][3], 'sigma_z_static = ', self.sigma_z_static
        self.sigma_z_static = self.sigma_z*gamma0;       #print inspect.stack()[0][3], 'sigma_z = ', self.sigma_z
        
        xmin = np.max([np.min(x), -N_cutoff*self.sigma_x])
        xmax = np.min([np.max(x),  N_cutoff*self.sigma_x])
        ymin = np.max([np.min(y), -N_cutoff*self.sigma_y])
        ymax = np.min([np.max(y),  N_cutoff*self.sigma_y])
        N_cutoff_z = 5
        zmin = np.max([np.min(z), zc-N_cutoff_z*self.sigma_z])
        zmax = np.min([np.max(z), zc+N_cutoff_z*self.sigma_z])
        
        xmax = np.max([xmax, -xmin])
        ymax = np.max([ymax, -ymin])
        xmin, ymin = -xmax, -ymax
        #zmin = np.min(z)
        #zmax = np.max(z)
        nx0, ny0, nz0 = 1, 1, 1 # Guarding grids on one side
        if self.Lemit:
            zmin = 0
            zmax = np.max(z)
            nz0 = 2.5
        #print inspect.stack()[0][3], zmin, zmax, self.zmean
        
        #import pdb; pdb.set_trace()
        self.zmean = (zmin+zmax)*0.5 # zmean is geometric center
        z -= self.zmean
        z_static = z*gamma0
        
        zmax -= self.zmean
        zmin -= self.zmean
        
        hx = (xmax-xmin)/(Nx-2*nx0); hy = (ymax-ymin)/(Ny-2*ny0)
        hz = (zmax-zmin)/(Nz-2*nz0)
        
        zmin_static, zmax_static = zmin*gamma0, zmax*gamma0
        hz_static = hz*gamma0
        
        self.dx = hx; self.dy = hy; self.dz = hz; #print inspect.stack()[0][3], 'mesh sizes: ', hx, hy, hz
        self.dz_static = hz_static
        
        if self.debug and 0:
            print inspect.stack()[0][3], 'Qtot = ', self.Qtot
            print inspect.stack()[0][3], 'gamma = ', self.gamma0
            print inspect.stack()[0][3], 'sigma_x = ', self.sigma_x
            print inspect.stack()[0][3], 'sigma_y = ', self.sigma_y
            print inspect.stack()[0][3], 'sigma_z_static = ', self.sigma_z_static
            print inspect.stack()[0][3], 'sigma_z = ', self.sigma_z
            print inspect.stack()[0][3], 'mesh sizes in rest frame: ', hx, hy, hz_static
        
        xgrid = np.linspace(xmin-nx0*hx, xmax+nx0*hx, Nx+1)
        ygrid = np.linspace(ymin-ny0*hy, ymax+ny0*hy, Ny+1)
        zgrid = np.linspace(zmin-nz0*hz, zmax+nz0*hz, Nz+1); # print zgrid+self.zmean
        #import pdb; pdb.set_trace()
        zgrid_static = zgrid*gamma0
        
        ranges = [(xgrid[0]-0.5*hx, xgrid[-1]+0.5*hx), (ygrid[0]-0.5*hy, ygrid[-1]+0.5*hy),\
                  (zgrid_static[0]-0.5*hz_static, zgrid_static[-1]+0.5*hz_static)]
        rho, edges = np.histogramdd((x, y, z_static), bins = (Nx+1, Ny+1, Nz+1), range = ranges, weights = w)
        del edges
        
        if self.debug:
            #print xgrid/self.sigma_x, ygrid/self.sigma_y, zgrid/self.sigma_z_static
            #print rho
            #print ranges
            fig, [ax1, ax2, ax3] = plt.subplots(ncols = 3, figsize = (12, 4))
            rho_x = np.sum(np.sum(rho, axis = 1), axis = 1)
            ax1.plot(xgrid*1e3, rho_x, '-*')
            ax1.grid()
            ax1.set_xlabel(r'$x$ (mm)')
            ax1.set_ylabel(r'$\rho$ (arb. unit)')
            
            [aX,aY] = np.meshgrid(xgrid, ygrid)
            data = np.abs(rho[:,:,Nz/2-1])
            v = np.linspace(0.05, 1., 96)
            ax2.contourf(aX*1e3, aY*1e3, data/np.max(data), v, linestyles = None)
            ax2.grid()
            ax2.set_xlabel(r'$x$ (mm)')
            ax2.set_ylabel(r'$y$ (mm)')
            
            rho_z = np.sum(np.sum(rho, axis = 0), axis = 0)
            ax3.plot(zgrid*1e3, rho_z, '-*')
            ax3.grid()
            ax3.set_xlabel(r'$z$ (mm)')
            ax3.set_ylabel(r'$\rho$ (arb. unit)')
            
            fig.savefig('charge-density'+fig_ext)
        
        #rho = sgolay2d(rho, window_size=11, order=4)   
        #self.rho_RZ = interp2d(rr, zz, rho.T, kind = 'cubic')
        self.xgrid = xgrid
        self.ygrid = ygrid
        self.zgrid = zgrid
        self.zgrid_static = zgrid_static
        self.rho = rho/(hx*hy*hz_static)
        
        return
    def build(self, Xshift = (0, 0, 0), **kwargs):
        if 'fig_ext' in kwargs.keys():
            fig_ext = kwargs['fig_ext'];
        else:
            fig_ext = '.eps'
        
        Nx = self.nx; Ny = self.ny; Nz = self.nz; 
        sigma_x = self.sigma_x; sigma_y = self.sigma_y; sigma_z_static = self.sigma_z_static
        #hx = self.dx; hy = self.dy; hz = self.dz_static
        
        if self.Lspch:
            # Create arrays for GFs and densities
            GFC = np.zeros((2*Nx, 2*Ny, 2*Nz))
            RHOC = np.zeros((2*Nx, 2*Ny, 2*Nz))
            
            xgrid = self.xgrid
            ygrid = self.ygrid
            zgrid_static = self.zgrid_static
            RHOC[0:Nx+1, 0:Ny+1, 0:Nz+1] = self.rho
            
            hx = xgrid[1]-xgrid[0]; hy = ygrid[1]-ygrid[0]; hz_static = zgrid_static[1]-zgrid_static[0]
            if self.debug:
                print inspect.stack()[0][3], Nx, Ny, Nz
                print inspect.stack()[0][3], hx, hy, hz_static
            
            ## using vector to calculate GFs
            ## Calculate GFs by using the mapping, faster
            xf, yf, zf = Xshift; 
            zf0 = -hz_static/self.gamma0/2.
            zf = zf+zf0
            
            xx = xf-Nx*hx+np.arange(2*Nx+1)*hx-0.5*hx
            yy = yf-Ny*hy+np.arange(2*Ny+1)*hy-0.5*hy
            zz_static = zf*self.gamma0-Nz*hz_static+np.arange(2*Nz+1)*hz_static-0.5*hz_static
            XX = np.meshgrid(xx, yy, zz_static, indexing = 'ij')
            IGF_MAP = IGF3D(XX)

            ii = np.mod(np.arange(2*Nx)+Nx, 2*Nx)
            jj = np.mod(np.arange(2*Ny)+Ny, 2*Ny)
            kk = np.mod(np.arange(2*Nz)+Nz, 2*Nz)
            II = np.meshgrid(ii, jj, kk, indexing = 'ij')
            GFC = cyclic_IGF3D_mapping(II, IGF_MAP)

            ## FFT convolution to get the static potenital: PHI
            GFC_FFT = fftn(GFC)
            RHOC_FFT = fftn(RHOC)
            PHIC_FFT = GFC_FFT*RHOC_FFT

            PHIC = ifftn(PHIC_FFT)
            self.PHI = PHIC[:Nx+1,:Ny+1,:Nz+1]
            
        if self.Lspch and self.Lmirror:
            #zf = (self.zref+self.zmean)*2; print zf
            zf0 = -hz_static/self.gamma0/2.
            zf = (0+self.zmean)*2; #print zf
            zf = (0+self.zmean)*2+zf0; #print zf
            
            GFC_mirror = np.zeros((2*Nx, 2*Ny, 2*Nz))
            RHOC_mirror = np.zeros((2*Nx, 2*Ny, 2*Nz))
            
            RHOC_mirror[0:Nx+1, 0:Ny+1, 0:Nz+1] = self.rho[:,:,::-1]
            
            if self.debug:
                print inspect.stack()[0][3], Nx, Ny, Nz
                print inspect.stack()[0][3], hx, hy, hz_static
            
            ## using vector to calculate GFs
            ## Calculate GFs by using the mapping, faster
            xx = xf-Nx*hx+np.arange(2*Nx+1)*hx-0.5*hx
            yy = yf-Ny*hy+np.arange(2*Ny+1)*hy-0.5*hy
            zz_static = zf*self.gamma0-Nz*hz_static+np.arange(2*Nz+1)*hz_static-0.5*hz_static
            XX = np.meshgrid(xx, yy, zz_static, indexing = 'ij')
            IGF_mirror_MAP = IGF3D(XX)
            
            ii = np.mod(np.arange(2*Nx)+Nx, 2*Nx)
            jj = np.mod(np.arange(2*Ny)+Ny, 2*Ny)
            kk = np.mod(np.arange(2*Nz)+Nz, 2*Nz)
            II = np.meshgrid(ii, jj, kk, indexing = 'ij')
            GFC_mirror = cyclic_IGF3D_mapping(II, IGF_mirror_MAP)

            ## FFT convolution to get the static potenital: PHI
            GFC_mirror_FFT = fftn(GFC_mirror)
            RHOC_mirror_FFT = fftn(RHOC_mirror)
            PHIC_mirror_FFT = GFC_mirror_FFT*RHOC_mirror_FFT

            PHIC_mirror = ifftn(PHIC_mirror_FFT)
            self.PHI_mirror = PHIC_mirror[:Nx+1,:Ny+1,:Nz+1]
        
        if self.Lmirror:
            
            Ex_static, Ey_static, Ez_static = np.gradient(self.PHI.real, hx, hy, hz_static) 
            Ex_static_m, Ey_static_m, Ez_static_m = np.gradient(self.PHI_mirror.real, hx, hy, hz_static) 
            cc = -1./4/np.pi/g_eps0
        
            print inspect.stack()[0][3], 'Qtot = ', self.Qtot
            fig, [ax1, ax2] = plt.subplots(figsize = (12, 4), ncols = 2)
            #ax1.plot(xgrid/sigma_x, Ex_static[:, Ny/2-1, Nz/4-1]*cc/1e3, '-*')
            #ax1.plot(xgrid/sigma_x, -Ex_static_m[:, Ny/2-1, Nz/4-1]*cc/1e3, '-o')
            
            ax1.plot((self.zgrid+self.zmean+zf0)*1e3, Ex_static[Nx/2-1, Ny/2-1, :]*cc/1e3, '-*')
            ax1.plot((self.zgrid+self.zmean+zf0)*1e3, -Ex_static_m[Nx/2-1, Ny/2-1, :]*cc/1e3, '-o')
            ax1.plot((self.zgrid+self.zmean+zf0)*1e3, Ex_static[Nx/2-1, Ny/2-1, :]*cc/1e3-Ex_static_m[Nx/2-1, Ny/2-1, :]*cc/1e3, '--')
            #ax1.set_xlim(-0.05, 0.05)
            #ax1.set_ylim(-200, 200)
            
            ax1.set_xlabel(r'$z$')
            ax1.set_ylabel(r'$E_x(z)$ (kV/m)')
            ax1.grid()
            
            ax2.plot((self.zgrid+self.zmean+zf0)*1e3, Ez_static[Nx/2-1, Ny/2-1, :]*cc/1e3, '-*')
            ax2.plot((self.zgrid+self.zmean+zf0)*1e3, -Ez_static_m[Nx/2-1, Ny/2-1, :]*cc/1e3, '-o')
            ax2.plot((self.zgrid+self.zmean+zf0)*1e3, Ez_static[Nx/2-1, Ny/2-1, :]*cc/1e3-Ez_static_m[Nx/2-1, Ny/2-1, :]*cc/1e3, '--')
            #print Ez_static[Nx/2-1, Ny/2-1, :]*cc/1e3
            #print -Ez_static_m[Nx/2-1, Ny/2-1, :]*cc/1e3
            ax2.set_xlabel(r'$z$ (mm)')
            ax2.set_ylabel(r'$E_z(z)$ (kV/m)')
            #ax2.set_xlim(-0.05, 0.05)
            ax2.grid()
            plt.tight_layout()
            fig.savefig('fields-on-axis'+fig_ext)
            
            fig, [ax1, ax2, ax3] = plt.subplots(figsize = (12, 4), ncols = 3)
            ax1.plot(GFC[Nx/2-1, Ny/2-1, :], '-*')
            ax1.plot(GFC_mirror[Nx/2-1, Ny/2-1, :], '-o')
            ax1.grid()
            ax1.set_xlabel(r'grid number')
            ax1.set_title(r'green function')
            
            to_astra_unit = self.dx*self.dy*self.dz_static/self.dz*1e9
            ax2.plot((self.zgrid+self.zmean+zf0)*1e3, np.sum(np.sum(RHOC[:,:,:Nz+1], axis = 0), axis = 0)*to_astra_unit, '-*')
            ax2.plot((self.zgrid+self.zmean+zf0)*1e3, np.sum(np.sum(RHOC_mirror[:,:,:Nz+1], axis = 0), axis = 0)*to_astra_unit, '-o')
            #ax2.set_xlim(-0.05, 0.05)
            ax2.grid()
            ax2.set_xlabel(r'$z$ (mm)')
            ax2.set_title(r'charge density (nC/m)')
            
            ax3.plot(self.PHI[Nx/2-1, Ny/2-1, :], '-*')
            ax3.plot(self.PHI_mirror[Nx/2-1, Ny/2-1, :], '-o')
            #ax3.set_xlim(-1, 5)
            #ax3.set_ylim(-4.0e-7, -3.5e-7)
            ax3.grid()
            ax3.set_xlabel(r'grid number')
            ax3.set_title(r'static potential')
            plt.tight_layout()
            fig.savefig('green-function'+fig_ext)
            
            #plt.close('all')
            
        if self.Lspch and self.Lmirror:
            self.PHI -= self.PHI_mirror
        
        del GFC, RHOC, GFC_FFT, RHOC_FFT, PHIC_FFT, IGF_MAP
        if self.Lmirror:
            del GFC_mirror, RHOC_mirror, GFC_mirror_FFT, RHOC_mirror_FFT, PHIC_mirror_FFT, IGF_mirror_MAP
        del XX, II
        
        ######
        Ex_static, Ey_static, Ez_static = np.gradient(self.PHI.real, hx, hy, hz_static)     
        cc = -1./4/np.pi/g_eps0
        ######
        if self.debug and 0:
            print inspect.stack()[0][3], 'Qtot = ', self.Qtot
            fig, [ax1, ax2] = plt.subplots(figsize = (12, 4), ncols = 2)
            ax1.plot(xgrid/sigma_x, Ex_static[:, Ny/2-1, Nz/4-1]*cc/1e3, '-*')
            ax1.set_xlabel(r'$x/\sigma_x$')
            ax1.set_ylabel(r'$E_x(x)$ (kV/m)')
            ax1.grid()
            
            ax2.plot(zgrid_static/sigma_z_static, Ez_static[Nx/2-1, Ny/2-1, :]*cc/1e3, '-*')
            ax2.set_xlabel(r'$z/\sigma_z$')
            ax2.set_ylabel(r'$E_z(z)$ (kV/m)')
            ax2.grid()
            
            plt.tight_layout()
            fig.savefig('fields-on-axis.eps')
            
            fig, [ax1, ax2, ax3] = plt.subplots(figsize = (12, 4), ncols = 3)
            ax1.plot(GFC[Nx/2-1, Ny/2-1, :], '-*')
            ax1.grid()
            ax1.set_xlabel(r'grid number')
            ax1.set_title(r'green function')
            
            ax2.plot(RHOC[Nx/2-1, Ny/2-1, :], '-*')
            ax2.grid()
            ax2.set_xlabel(r'grid number')
            ax2.set_title(r'charge density')
            
            ax3.plot(self.PHI[Nx/2-1, Ny/2-1, :]*cc*(-1), '-*')
            ax3.grid()
            ax3.set_xlabel(r'grid number')
            ax3.set_title(r'static potential')
            
            plt.tight_layout()
            fig.savefig('green-function.eps')
        
        fEx3D_static = RegularGridInterpolator((xgrid, ygrid, zgrid_static+zf0), Ex_static, bounds_error = False, fill_value = None)
        fEy3D_static = RegularGridInterpolator((xgrid, ygrid, zgrid_static+zf0), Ey_static, bounds_error = False, fill_value = None)
        fEz3D_static = RegularGridInterpolator((xgrid, ygrid, zgrid_static+zf0), Ez_static, bounds_error = False, fill_value = None)
        
        cc = -1./4/np.pi/g_eps0 # 
        gamma0 = self.gamma0; beta0 = gamma2beta(gamma0); zmean = self.zmean
        def EM3D(x, y, z, t = 0, *args):
            if np.isscalar(x):
                x = [x]
            if np.isscalar(y):
                y = [y]
            if np.isscalar(z):
                z = [z]
                
            if len(args)>0:
                dt  = t-args[0]
            else:
                dt = 0
            
            x = np.asarray(x)
            y = np.asarray(y)
            z = np.asarray(z)
            z_static = (z-zmean-beta0*g_c*dt)*gamma0 # here t is the time difference during one RK step
            
            n = np.max([len(x), len(y), len(z)])
            
            Ex_static = fEx3D_static((x, y, z_static))*cc
            Ey_static = fEy3D_static((x, y, z_static))*cc
            Ez_static = fEz3D_static((x, y, z_static))*cc
            
            # return normalied E and B in lab frame
            F2d = np.zeros((n, 6))
            F2d[:,0] = gamma0*Ex_static
            F2d[:,1] = gamma0*Ey_static
            F2d[:,2] = Ez_static
            F2d[:,3] =-beta0*gamma0*Ey_static/g_c # beta0*gamma0*Ey_static/g_c
            F2d[:,4] = beta0*gamma0*Ex_static/g_c # -beta0*gamma0*Ex_static/g_c
            
            return F2d
        
        self.EM3D = EM3D
        return
    
    def get_rho_3D_binned(self, dist, nx = None, ny = None, nz = None, N_cutoff = None,\
                          nbins = 4, Lmirror = None, Lemit = None, format = 'Astra', fig_ext = '.eps'):
        '''
        Parameters
          dist: particle distributions in the same format as Astra, but absolute `z` and `Pz*c`
        '''
        if nx is not None:
            self.nx = nx
        if ny is not None:
            self.ny = ny
        if nz is not None:
            self.nz = nz
        Nx = self.nx; Ny = self.ny; Nz = self.nz
        if N_cutoff is not None:
            self.N_cutoff = N_cutoff
        N_cutoff = self.N_cutoff
        
        if Lmirror is not None:
            self.Lmirror = Lmirror
        if Lemit is not None:
            self.Lemit = Lemit
        
        if format == 'Astra' or format == 'ASTRA' or format == 'astra':
            ix, iy, iz, iux, iuy, iuz, iw = 0, 1, 2, 3, 4, 5, 7
                       
            # Select active partiles
            select = (dist[:,9]>0); dist = dist[select]
            x = dist[:,ix]
            y = dist[:,iy]
            z = dist[:,iz]
            w = dist[:,iw]*1e-9; self.Qtot = np.sum(w)
            
            zc =  weighted_mean(z, w)
            #self.zmean = (np.max(z)+np.min(z))*0.5
            #z -= self.zmean
            
            bgx, bgy, bgz = dist[:,iux]/g_mec2/1e6, dist[:,iuy]/g_mec2/1e6, dist[:,iuz]/g_mec2/1e6
            gamma = np.sqrt(1+bgx**2+bgy**2+bgz**2)
            
            gamma0 = weighted_mean(gamma, w)
            #z_static = z*gamma0
        
        self.gamma0 = gamma0;              #print inspect.stack()[0][3], 'gamma = ', self.gamma0
        self.sigma_x = weighted_std(x, w); #print inspect.stack()[0][3], 'sigma_x = ', self.sigma_x
        self.sigma_y = weighted_std(y, w); #print inspect.stack()[0][3], 'sigma_y = ', self.sigma_y
        self.sigma_z = weighted_std(z, w); #print inspect.stack()[0][3], 'sigma_z_static = ', self.sigma_z_static
        self.sigma_z_static = self.sigma_z*gamma0;       #print inspect.stack()[0][3], 'sigma_z = ', self.sigma_z
        
        xmin = np.max([np.min(x), -N_cutoff*self.sigma_x])
        xmax = np.min([np.max(x),  N_cutoff*self.sigma_x])
        ymin = np.max([np.min(y), -N_cutoff*self.sigma_y])
        ymax = np.min([np.max(y),  N_cutoff*self.sigma_y])
        N_cutoff_z = 5
        zmin = np.max([np.min(z), zc-N_cutoff_z*self.sigma_z])
        zmax = np.min([np.max(z), zc+N_cutoff_z*self.sigma_z])
        
        xmax = np.max([xmax, -xmin])
        ymax = np.max([ymax, -ymin])
        xmin, ymin = -xmax, -ymax
        #zmin = np.min(z)
        #zmax = np.max(z)
        nx0, ny0, nz0 = 1, 1, 1 # Guarding grids on one side
        if self.Lemit:
            zmin = 0
            zmax = np.max(z)
            nz0 = 2.5
        #print inspect.stack()[0][3], zmin, zmax, self.zmean
        
        #import pdb; pdb.set_trace()
        self.zmean = (zmin+zmax)*0.5 # zmean is geometric center
        z -= self.zmean
        z_static = z*gamma0
        
        zmax -= self.zmean
        zmin -= self.zmean
        
        hx = (xmax-xmin)/(Nx-2*nx0); hy = (ymax-ymin)/(Ny-2*ny0)
        hz = (zmax-zmin)/(Nz-2*nz0)
        
        zmin_static, zmax_static = zmin*gamma0, zmax*gamma0
        hz_static = hz*gamma0
        
        self.dx = hx; self.dy = hy; self.dz = hz; #print inspect.stack()[0][3], 'mesh sizes: ', hx, hy, hz
        self.dz_static = hz_static
        
        if self.debug and 0:
            print inspect.stack()[0][3], 'Qtot = ', self.Qtot
            print inspect.stack()[0][3], 'gamma = ', self.gamma0
            print inspect.stack()[0][3], 'sigma_x = ', self.sigma_x
            print inspect.stack()[0][3], 'sigma_y = ', self.sigma_y
            print inspect.stack()[0][3], 'sigma_z_static = ', self.sigma_z_static
            print inspect.stack()[0][3], 'sigma_z = ', self.sigma_z
            print inspect.stack()[0][3], 'mesh sizes in rest frame: ', hx, hy, hz_static
        
        '''
        xgrid = np.linspace(xmin-nx0*hx, xmax+nx0*hx, Nx+1)
        ygrid = np.linspace(ymin-ny0*hy, ymax+ny0*hy, Ny+1)
        zgrid = np.linspace(zmin-nz0*hz, zmax+nz0*hz, Nz+1); # print zgrid+self.zmean
        #import pdb; pdb.set_trace()
        zgrid_static = zgrid*gamma0
        '''
        
        ### Select sub-group of particles
        bg = np.sqrt(gamma**2-1)
        bg0 = weighted_mean(bg, w); std_bg0 = weighted_std(bg, w)
        #print bg0, std_bg0
        
        s1 = (bg<bg0-std_bg0)
        s2 = (bg>=bg0-std_bg0)*(bg<bg0)
        s3 = (bg>=bg0)*(bg<bg0+std_bg0)
        s4 = (bg>=bg0+std_bg0)
        ss = [s1, s2, s3, s4]
        
        Qtot = []
        gamma0 = []
        rho = []
        zgrid_static = []
        
        xgrid = np.linspace(xmin-nx0*hx, xmax+nx0*hx, Nx+1)
        ygrid = np.linspace(ymin-ny0*hy, ymax+ny0*hy, Ny+1)
        zgrid = np.linspace(zmin-nz0*hz, zmax+nz0*hz, Nz+1)
        
        fig, ax = plt.subplots()
        ax.grid()
        for si in ss:
            #print [np.sum(i) for i in ss]
            xi = x[si]
            yi = y[si]
            zi = z[si]
            wi = w[si]
            gi = gamma[si]
            
            ax.plot(zi*1e3, bg[si], '.')
            #import pdb; pdb.set_trace()
            g0i = weighted_mean(gi, wi)
            q0i = np.sum(wi)
            
            hz_static = hz*g0i   
            zmin_static, zmax_static = zmin*g0i, zmax*g0i      
            zgridi_static = np.linspace(zmin_static-nz0*hz_static, zmax_static+nz0*hz_static, Nz+1)
            
            ranges = [(xgrid[0]-0.5*hx, xgrid[-1]+0.5*hx), (ygrid[0]-0.5*hy, ygrid[-1]+0.5*hy),\
                      (zgridi_static[0]-0.5*hz_static, zgridi_static[-1]+0.5*hz_static)]
            #print ranges
            rhoi, edges = np.histogramdd((xi, yi, zi*g0i), bins = (Nx+1, Ny+1, Nz+1), range = ranges, weights = wi)
            
            gamma0.append(g0i)
            Qtot.append(q0i)
            zgrid_static.append(zgridi_static)
            rho.append(rhoi/(hx*hy*hz_static))
            
            del zgridi_static
            del rhoi
        
        self.dx = hx; self.dy = hy; self.dz = hz 
        #print inspect.stack()[0][3], 'mesh sizes: ', hx, hy, hz
        
        if self.debug:
            fig, [ax1, ax2] = plt.subplots(ncols = 2, figsize = (12, 4))
            add = np.zeros(len(zgrid))
            for i in np.arange(4):
                #data = np.abs(rho[i][Nx/2-1, Ny/2-1, :]*gamma0[i])
                rhoi_z = np.abs(np.sum(np.sum(rho[i], axis = 0), axis = 0)*gamma0[i])
                add += rhoi_z
                ax1.plot(zgrid*1e3, rhoi_z, label = '%d' % i)
            ax1.plot(zgrid*1e3, add, '--', label = 'all')
            ax1.grid()
            ax1.legend()
            ax1.set_xlabel(r'$z$ (mm)')
            ax1.set_ylabel(r'$\rho$ (arb. unit)')
            
            add = np.zeros(len(xgrid))
            for i in np.arange(4):
                rhoi_x = np.abs(np.sum(np.sum(rho[i], axis = 1), axis = 1)*gamma0[i])
                add += rhoi_x
                ax2.plot(xgrid*1e3, rhoi_x, label = '%d' % i)
            ax2.plot(xgrid*1e3, add, '--', label = 'add')
            ax2.grid()
            ax2.legend()
            ax2.set_xlabel(r'$x$ (mm)')
            ax2.set_ylabel(r'$\rho$ (arb. unit)')
            
            fig.savefig('charge-density.eps')
        
        # Save 
        self.Qtot = Qtot
        self.gamma0 = gamma0
        
        self.xgrid = xgrid
        self.ygrid = ygrid
        self.zgrid = zgrid
        
        self.rho = rho
        
        return  
    def build_binned(self, Xshift = (0, 0, 0), nbins = 4, **kwargs):
        
        if 'fig_ext' in kwargs.keys():
            fig_ext = kwargs['fig_ext'];
        else:
            fig_ext = '.eps'
        
        Nx = self.nx; Ny = self.ny; Nz = self.nz; 
        sigma_x = self.sigma_x; sigma_y = self.sigma_y; sigma_z = self.sigma_z
        #hx = self.dx; hy = self.dy; hz = self.dz_static
        
        xgrid = self.xgrid
        ygrid = self.ygrid
        zgrid = self.zgrid
        hx = xgrid[1]-xgrid[0]; hy = ygrid[1]-ygrid[0]; hz = zgrid[1]-zgrid[0]
        
        if self.Lspch:
            # Create arrays for GFs and densities
            nbins = len(self.Qtot)
            self.PHI = []
            for i in np.arange(nbins):
                GFC = np.zeros((2*Nx, 2*Ny, 2*Nz))
                RHOC = np.zeros((2*Nx, 2*Ny, 2*Nz))
                
                RHOC[0:Nx+1, 0:Ny+1, 0:Nz+1] = self.rho[i]
                
                zgrid_static = self.zgrid*self.gamma0[i]
                hz_static = zgrid_static[1]-zgrid_static[0]
                
                if self.debug:
                    print inspect.stack()[0][3], Nx, Ny, Nz
                    print inspect.stack()[0][3], hx, hy, hz_static
                
                ## using vector to calculate GFs
                ## Calculate GFs by using the mapping, faster
                xf, yf, zf = Xshift
                zf0 = -hz/2.
                zf = zf+zf0
                
                xx = xf-Nx*hx+np.arange(2*Nx+1)*hx-0.5*hx
                yy = yf-Ny*hy+np.arange(2*Ny+1)*hy-0.5*hy
                zz_static = zf*self.gamma0[i]-Nz*hz_static+np.arange(2*Nz+1)*hz_static-0.5*hz_static
                XX = np.meshgrid(xx, yy, zz_static, indexing = 'ij')
                IGF_MAP = IGF3D(XX)
                
                ii = np.mod(np.arange(2*Nx)+Nx, 2*Nx)
                jj = np.mod(np.arange(2*Ny)+Ny, 2*Ny)
                kk = np.mod(np.arange(2*Nz)+Nz, 2*Nz)
                II = np.meshgrid(ii, jj, kk, indexing = 'ij')
                GFC = cyclic_IGF3D_mapping(II, IGF_MAP)
                
                ## FFT convolution to get the static potenital: PHI
                GFC_FFT = fftn(GFC)
                RHOC_FFT = fftn(RHOC)
                PHIC_FFT = GFC_FFT*RHOC_FFT
                
                PHIC = ifftn(PHIC_FFT)
                
                self.PHI.append(PHIC[:Nx+1,:Ny+1,:Nz+1])
        
        if self.Lspch and self.Lmirror:
            #zf = (self.zref+self.zmean)*2; print zf
            zf0 = -hz/2.
            zf = (0+self.zmean)*2; #print zf
            zf = (0+self.zmean)*2+zf0; #print zf
            
            nbins = len(self.Qtot)
            self.PHI_mirror = []
            for i in np.arange(nbins):
                
                GFC_mirror = np.zeros((2*Nx, 2*Ny, 2*Nz))
                RHOC_mirror = np.zeros((2*Nx, 2*Ny, 2*Nz))
                
                RHOC_mirror[0:Nx+1, 0:Ny+1, 0:Nz+1] = self.rho[i][:,:,::-1]
                #RHOC_mirror = RHOC[:,:,::-1]
                
                zgrid_static = self.zgrid*self.gamma0[i]
                hz_static = zgrid_static[1]-zgrid_static[0]
                
                if self.debug:
                    print inspect.stack()[0][3], Nx, Ny, Nz
                    print inspect.stack()[0][3], hx, hy, hz_static
                
                ## using vector to calculate GFs
                ## Calculate GFs by using the mapping, faster
                g0i = self.gamma0[i]
                xx = xf-Nx*hx+np.arange(2*Nx+1)*hx-0.5*hx
                yy = yf-Ny*hy+np.arange(2*Ny+1)*hy-0.5*hy
                zz_static = zf*g0i-Nz*hz_static+np.arange(2*Nz+1)*hz_static-0.5*hz_static
                XX = np.meshgrid(xx, yy, zz_static, indexing = 'ij')
                IGF_mirror_MAP = IGF3D(XX)
                
                ii = np.mod(np.arange(2*Nx)+Nx, 2*Nx)
                jj = np.mod(np.arange(2*Ny)+Ny, 2*Ny)
                kk = np.mod(np.arange(2*Nz)+Nz, 2*Nz)
                II = np.meshgrid(ii, jj, kk, indexing = 'ij')
                GFC_mirror = cyclic_IGF3D_mapping(II, IGF_mirror_MAP)
                
                ## FFT convolution to get the static potenital: PHI
                GFC_mirror_FFT = fftn(GFC_mirror)
                RHOC_mirror_FFT = fftn(RHOC_mirror)
                PHIC_mirror_FFT = GFC_mirror_FFT*RHOC_mirror_FFT
                
                PHIC_mirror = ifftn(PHIC_mirror_FFT)
                self.PHI_mirror.append(PHIC_mirror[:Nx+1,:Ny+1,:Nz+1])
                
                self.PHI[i] -= self.PHI_mirror[i]
        
        # Field interpolation
        cc = -1./4/np.pi/g_eps0
        fEx3D_static = []
        fEy3D_static = []
        fEz3D_static = []
        for i in np.arange(nbins):
            Ex_static, Ey_static, Ez_static = np.gradient(self.PHI[i], hx, hy, hz*self.gamma0[i])     
        
            fEx3Di_static = RegularGridInterpolator((xgrid, ygrid, zgrid*self.gamma0[i]), Ex_static,                                                    bounds_error = False, fill_value = None)
            fEy3Di_static = RegularGridInterpolator((xgrid, ygrid, zgrid*self.gamma0[i]), Ey_static,                                                    bounds_error = False, fill_value = None)
            fEz3Di_static = RegularGridInterpolator((xgrid, ygrid, zgrid*self.gamma0[i]), Ez_static,                                                    bounds_error = False, fill_value = None)
            
            fEx3D_static.append(fEx3Di_static)
            fEy3D_static.append(fEy3Di_static)
            fEz3D_static.append(fEz3Di_static)
        
        if self.debug and 0:
            print inspect.stack()[0][3], 'Qtot = ', self.Qtot
            fig, [ax1, ax2] = plt.subplots(figsize = (12, 4), ncols = 2)
            ax1.plot(xgrid/sigma_x, Ex_static[:, Ny/2-1, Nz/4-1]*cc/1e3, '-*')
            ax1.set_xlabel(r'$x/\sigma_x$')
            ax1.set_ylabel(r'$E_x(x)$ (kV/m)')
            ax1.grid()
            ax2.plot(zgrid_static/sigma_z_static, Ez_static[Nx/2-1, Ny/2-1, :]*cc/1e3, '-*')
            ax2.set_xlabel(r'$z/\sigma_z$')
            ax2.set_ylabel(r'$E_z(z)$ (kV/m)')
            ax2.grid()
            plt.tight_layout()
            fig.savefig('fields-on-axis.eps')
            
            fig, [ax1, ax2, ax3] = plt.subplots(figsize = (12, 4), ncols = 3)
            ax1.plot(GFC[:, Ny/2-1, Nz/2-1], '-*')
            ax1.grid()
            ax1.set_xlabel(r'grid number')
            ax1.set_title(r'green function')
            ax2.plot(RHOC[:, Ny/2-1, Nz/2-1], '-*')
            ax2.grid()
            ax2.set_xlabel(r'grid number')
            ax2.set_title(r'charge density')
            ax3.plot(self.PHI[:, Ny/2-1, Nz/2-1], '-*')
            ax3.grid()
            ax3.set_xlabel(r'grid number')
            ax3.set_title(r'static potential')
            plt.tight_layout()
            fig.savefig('green-function.eps')
        
        gamma0 = self.gamma0; zmean = self.zmean
        def EM3D(x, y, z, t = 0):
            
            if np.isscalar(x):
                x = [x]
            if np.isscalar(y):
                y = [y]
            if np.isscalar(z):
                z = [z]
            
            x = np.asarray(x)
            y = np.asarray(y)
            z = np.asarray(z)
            z_static = (z-zmean)*gamma0
            
            n = np.max([len(x), len(y), len(z)])
            
            F2d = np.zeros((len(x), 6))
            for i in np.arange(nbins):
                g0i = gamma0[i]; b0i = gamma2beta(g0i)
                z_static = (z-zmean)*g0i
                Ex_static = fEx3D_static[i]((x, y, z_static))*cc
                Ey_static = fEy3D_static[i]((x, y, z_static))*cc
                Ez_static = fEz3D_static[i]((x, y, z_static))*cc
                
                # Normalied E and B in lab frame
                
                F2d[:,0] += g0i*Ex_static
                F2d[:,1] += g0i*Ey_static
                F2d[:,2] += Ez_static
                F2d[:,3] += b0i*g0i*Ey_static/g_c
                F2d[:,4] -= b0i*g0i*Ex_static/g_c           
                #F2D = np.add(F2D, [g0i*Ex_static, g0i*Ey_static, Ez_static, b0i*g0i*Ey_static, -b0i*g0i*Ex_static, 0])
            
            return F2D
        
        self.EM3D = EM3D
        return
    
    def get_rho_3D_binned0(self, dist, nx = None, ny = None, nz = None, N_cutoff = None,\
                          nbins = 4, Lmirror = None, Lemit = None, format = 'Astra'):
        '''
        Parameters
          dist: particle distributions in the same format as Astra, but absolute `z` and `Pz*c`
        '''
        if nx is not None:
            self.nx = nx
        if ny is not None:
            self.ny = ny
        if nz is not None:
            self.nz = nz
        Nx = self.nx; Ny = self.ny; Nz = self.nz
        if N_cutoff is not None:
            self.N_cutoff = N_cutoff
        N_cutoff = self.N_cutoff
        
        if Lmirror is not None:
            self.Lmirror = Lmirror
        if Lemit is not None:
            self.Lemit = Lemit
        
        if format == 'Astra' or format == 'ASTRA' or format == 'astra':
            ix, iy, iz, iux, iuy, iuz, iw = 0, 1, 2, 3, 4, 5, 7
            
            self.zref = dist[0,iz]
            
            # Select active partiles
            select = (dist[:,9]>0); dist = dist[select]
            x = dist[:,ix]
            y = dist[:,iy]
            z = dist[:,iz]
            w = dist[:,iw]*1e-9
            
            self.zmean =  weighted_mean(z, w); print self.zmean
            self.zmean = (np.max(z)+np.min(z))*0.5; print self.zmean
            z -= self.zmean
            
            bgx, bgy, bgz = dist[:,iux]/g_mec2/1e6, dist[:,iuy]/g_mec2/1e6, dist[:,iuz]/g_mec2/1e6
            
            gamma = np.sqrt(1+bgx**2+bgy**2+bgz**2)
            gamma0 = weighted_mean(gamma, w); std_gamma0 = weighted_std(gamma, w)
        
        self.gamma0 = gamma0;              #print inspect.stack()[0][3], 'gamma = ', self.gamma0
        self.sigma_x = weighted_std(x, w); #print inspect.stack()[0][3], 'sigma_x = ', self.sigma_x
        self.sigma_y = weighted_std(y, w); #print inspect.stack()[0][3], 'sigma_y = ', self.sigma_y
        self.sigma_z = weighted_std(z, w); #print inspect.stack()[0][3], 'sigma_z_static = ', self.sigma_z_static
        
        xmin = np.max([np.min(x), -N_cutoff*self.sigma_x])
        xmax = np.min([np.max(x),  N_cutoff*self.sigma_x])
        ymin = np.max([np.min(y), -N_cutoff*self.sigma_y])
        ymax = np.min([np.max(y),  N_cutoff*self.sigma_y])
        zmin = np.max([np.min(z), -N_cutoff*self.sigma_z])
        zmax = np.min([np.max(z),  N_cutoff*self.sigma_z])
        
        zmin = np.min(z)
        zmax = np.max(z)
        nx0 = 1 # Guarding grids on one side
        ny0 = 1
        nz0 = 1
        if self.Lemit:
            zmin = -self.zmean
            nz0 = 2
        #print inspect.stack()[0][3], zmin, zmax, self.zmean
        
        hx = (xmax-xmin)/(Nx-2*nx0); hy = (ymax-ymin)/(Ny-2*ny0)
        hz = (zmax-zmin)/(Nz-2*nz0)
        
        zmin_static, zmax_static = zmin*gamma0, zmax*gamma0
        hz_static = hz*gamma0
        
        self.dx = hx; self.dy = hy; self.dz = hz; #print inspect.stack()[0][3], 'mesh sizes: ', hx, hy, hz
        self.dz_static = hz_static
        
        # Select sub-group of particles
        bg = np.sqrt(gamma**2-1)
        bg0 = weighted_mean(bg, w); std_bg0 = weighted_std(bg, w)
        #print bg0, std_bg0
        
        s1 = (bg<bg0-std_bg0)
        s2 = (bg>=bg0-std_bg0)*(bg<bg0)
        s3 = (bg>=bg0)*(bg<bg0+std_bg0)
        s4 = (bg>=bg0+std_bg0)
        ss = [s1, s2, s3, s4]
        
        Qtot = []
        gamma0 = []
        rho = []
        zgrid_static = []
        
        xgrid = np.linspace(xmin-nx0*hx, xmax+nx0*hx, Nx+1)
        ygrid = np.linspace(ymin-ny0*hy, ymax+ny0*hy, Ny+1)
        zgrid = np.linspace(zmin-nz0*hz, zmax+nz0*hz, Nz+1)
        
        
        fig, ax = plt.subplots()
        ax.grid()
        for si in ss:
            #print [np.sum(i) for i in ss]
            xi = x[si]
            yi = y[si]
            zi = z[si]
            wi = w[si]
            gi = gamma[si]
            
            ax.plot(zi*1e3, bg[si], '.')
            #import pdb; pdb.set_trace()
            g0i = weighted_mean(gi, wi)
            q0i = np.sum(wi)
            
            hz_static = hz*g0i   
            zmin_static, zmax_static = zmin*g0i, zmax*g0i      
            zgridi_static = np.linspace(zmin_static-nz0*hz_static, zmax_static+nz0*hz_static, Nz+1)
            
            ranges = [(xgrid[0]-0.5*hx, xgrid[-1]+0.5*hx), (ygrid[0]-0.5*hy, ygrid[-1]+0.5*hy),\
                      (zgridi_static[0]-0.5*hz_static, zgridi_static[-1]+0.5*hz_static)]
            #print ranges
            rhoi, edges = np.histogramdd((xi, yi, zi*g0i), bins = (Nx+1, Ny+1, Nz+1), range = ranges, weights = wi)
            
            gamma0.append(g0i)
            Qtot.append(q0i)
            zgrid_static.append(zgridi_static)
            rho.append(rhoi/(hx*hy*hz_static))
            
            del zgridi_static
            del rhoi
        
        self.dx = hx; self.dy = hy; self.dz = hz 
        #print inspect.stack()[0][3], 'mesh sizes: ', hx, hy, hz
        
        if self.debug:
            fig, [ax1, ax2] = plt.subplots(ncols = 2, figsize = (12, 4))
            add = np.zeros(len(zgrid))
            for i in np.arange(4):
                #data = np.abs(rho[i][Nx/2-1, Ny/2-1, :]*gamma0[i])
                rhoi_z = np.abs(np.sum(np.sum(rho[i], axis = 0), axis = 0)*gamma0[i])
                add += rhoi_z
                ax1.plot(zgrid*1e3, rhoi_z, label = '%d' % i)
            ax1.plot(zgrid*1e3, add, '--', label = 'all')
            ax1.grid()
            ax1.legend()
            ax1.set_xlabel(r'$z$ (mm)')
            ax1.set_ylabel(r'$\rho$ (arb. unit)')
            
            add = np.zeros(len(xgrid))
            for i in np.arange(4):
                rhoi_x = np.abs(np.sum(np.sum(rho[i], axis = 1), axis = 1)*gamma0[i])
                add += rhoi_x
                ax2.plot(xgrid*1e3, rhoi_x, label = '%d' % i)
            ax2.plot(xgrid*1e3, add, '--', label = 'add')
            ax2.grid()
            ax2.legend()
            ax2.set_xlabel(r'$x$ (mm)')
            ax2.set_ylabel(r'$\rho$ (arb. unit)')
            
            fig.savefig('charge-density.eps')
        
        # Save 
        self.Qtot = Qtot
        self.gamma0 = gamma0
        
        self.xgrid = xgrid
        self.ygrid = ygrid
        self.zgrid = zgrid
        
        self.rho = rho
        
        return  
    def build_binned0(self, Xshift = (0, 0, 0), N_cutoff = None, nbins = 4):
        
        Nx = self.nx; Ny = self.ny; Nz = self.nz; 
        sigma_x = self.sigma_x; sigma_y = self.sigma_y; sigma_z = self.sigma_z
        #hx = self.dx; hy = self.dy; hz = self.dz_static
        
        if N_cutoff is None:
            N_cutoff = self.N_cutoff
        
        xgrid = self.xgrid
        ygrid = self.ygrid
        zgrid = self.zgrid
        hx = xgrid[1]-xgrid[0]; hy = ygrid[1]-ygrid[0]; hz = zgrid[1]-zgrid[0]
        
        if self.Lspch:
            # Create arrays for GFs and densities
            nbins = len(self.Qtot)
            self.PHI = []
            for i in np.arange(nbins):
                GFC = np.zeros((2*Nx, 2*Ny, 2*Nz))
                RHOC = np.zeros((2*Nx, 2*Ny, 2*Nz))
                
                RHOC[0:Nx+1, 0:Ny+1, 0:Nz+1] = self.rho[i]
                
                zgrid_static = self.zgrid*self.gamma0[i]
                hz_static = zgrid_static[1]-zgrid_static[0]
                
                if self.debug:
                    print inspect.stack()[0][3], Nx, Ny, Nz
                    print inspect.stack()[0][3], hx, hy, hz_static
                
                ## using vector to calculate GFs
                ## Calculate GFs by using the mapping, faster
                xf, yf, zf = Xshift
                xx = xf-Nx*hx+np.arange(2*Nx+1)*hx-0.5*hx
                yy = yf-Ny*hy+np.arange(2*Ny+1)*hy-0.5*hy
                zz_static = zf*self.gamma0[i]-Nz*hz_static+np.arange(2*Nz+1)*hz_static-0.5*hz_static
                XX = np.meshgrid(xx, yy, zz_static, indexing = 'ij')
                IGF_MAP = IGF3D(XX)
                
                ii = np.mod(np.arange(2*Nx)+Nx, 2*Nx)
                jj = np.mod(np.arange(2*Ny)+Ny, 2*Ny)
                kk = np.mod(np.arange(2*Nz)+Nz, 2*Nz)
                II = np.meshgrid(ii, jj, kk, indexing = 'ij')
                GFC = cyclic_IGF3D_mapping(II, IGF_MAP)
                
                ## FFT convolution to get the static potenital: PHI
                GFC_FFT = fftn(GFC)
                RHOC_FFT = fftn(RHOC)
                PHIC_FFT = GFC_FFT*RHOC_FFT
                
                PHIC = ifftn(PHIC_FFT)
                
                self.PHI.append(PHIC[:Nx+1,:Ny+1,:Nz+1])
        
        if self.Lspch and self.Lmirror:
            #zf = (self.zref+self.zmean)*2; print zf
            zf = (0+self.zmean)*2; #print zf
            
            nbins = len(self.Qtot)
            self.PHI_mirror = []
            for i in np.arange(nbins):
                
                GFC = np.zeros((2*Nx, 2*Ny, 2*Nz))
                RHOC_mirror = np.zeros((2*Nx, 2*Ny, 2*Nz))
                
                zgrid_static = self.zgrid*self.gamma0[i]
                hz_static = zgrid_static[1]-zgrid_static[0]
                
                RHOC_mirror[0:Nx+1, 0:Ny+1, 0:Nz+1] = self.rho[i][:,:,::-1]
                #RHOC_mirror = RHOC[:,:,::-1]
                
                if self.debug:
                    print inspect.stack()[0][3], Nx, Ny, Nz
                    print inspect.stack()[0][3], hx, hy, hz_static
                
                ## using vector to calculate GFs
                ## Calculate GFs by using the mapping, faster
                g0i = self.gamma0[i]
                xx = xf-Nx*hx+np.arange(2*Nx+1)*hx-0.5*hx
                yy = yf-Ny*hy+np.arange(2*Ny+1)*hy-0.5*hy
                zz_static = zf*g0i-Nz*hz_static+np.arange(2*Nz+1)*hz_static-0.5*hz_static
                XX = np.meshgrid(xx, yy, zz_static, indexing = 'ij')
                IGF_MAP = IGF3D(XX)
                
                ii = np.mod(np.arange(2*Nx)+Nx, 2*Nx)
                jj = np.mod(np.arange(2*Ny)+Ny, 2*Ny)
                kk = np.mod(np.arange(2*Nz)+Nz, 2*Nz)
                II = np.meshgrid(ii, jj, kk, indexing = 'ij')
                GFC = cyclic_IGF3D_mapping(II, IGF_MAP)
                
                ## FFT convolution to get the static potenital: PHI
                GFC_FFT = fftn(GFC)
                RHOC_FFT = fftn(RHOC_mirror)
                PHIC_FFT = GFC_FFT*RHOC_FFT
                
                PHIC_mirror = ifftn(PHIC_FFT)
                self.PHI_mirror.append(PHIC_mirror[:Nx+1,:Ny+1,:Nz+1])
                
                self.PHI[i] -= self.PHI_mirror[i]
        
        # Field interpolation
        cc = -1./4/np.pi/g_eps0
        fEx3D_static = []
        fEy3D_static = []
        fEz3D_static = []
        for i in np.arange(nbins):
            Ex_static, Ey_static, Ez_static = np.gradient(self.PHI[i], hx, hy, hz*self.gamma0[i])     
        
            fEx3Di_static = RegularGridInterpolator((xgrid, ygrid, zgrid*self.gamma0[i]), Ex_static,                                                    bounds_error = False, fill_value = None)
            fEy3Di_static = RegularGridInterpolator((xgrid, ygrid, zgrid*self.gamma0[i]), Ey_static,                                                    bounds_error = False, fill_value = None)
            fEz3Di_static = RegularGridInterpolator((xgrid, ygrid, zgrid*self.gamma0[i]), Ez_static,                                                    bounds_error = False, fill_value = None)
            
            fEx3D_static.append(fEx3Di_static)
            fEy3D_static.append(fEy3Di_static)
            fEz3D_static.append(fEz3Di_static)
        
        if self.debug and 0:
            print inspect.stack()[0][3], 'Qtot = ', self.Qtot
            fig, [ax1, ax2] = plt.subplots(figsize = (12, 4), ncols = 2)
            ax1.plot(xgrid/sigma_x, Ex_static[:, Ny/2-1, Nz/4-1]*cc/1e3, '-*')
            ax1.set_xlabel(r'$x/\sigma_x$')
            ax1.set_ylabel(r'$E_x(x)$ (kV/m)')
            ax1.grid()
            ax2.plot(zgrid_static/sigma_z_static, Ez_static[Nx/2-1, Ny/2-1, :]*cc/1e3, '-*')
            ax2.set_xlabel(r'$z/\sigma_z$')
            ax2.set_ylabel(r'$E_z(z)$ (kV/m)')
            ax2.grid()
            plt.tight_layout()
            fig.savefig('fields-on-axis.eps')
            
            fig, [ax1, ax2, ax3] = plt.subplots(figsize = (12, 4), ncols = 3)
            ax1.plot(GFC[:, Ny/2-1, Nz/2-1], '-*')
            ax1.grid()
            ax1.set_xlabel(r'grid number')
            ax1.set_title(r'green function')
            ax2.plot(RHOC[:, Ny/2-1, Nz/2-1], '-*')
            ax2.grid()
            ax2.set_xlabel(r'grid number')
            ax2.set_title(r'charge density')
            ax3.plot(self.PHI[:, Ny/2-1, Nz/2-1], '-*')
            ax3.grid()
            ax3.set_xlabel(r'grid number')
            ax3.set_title(r'static potential')
            plt.tight_layout()
            fig.savefig('green-function.eps')
        
        gamma0 = self.gamma0; zmean = self.zmean
        def EM3D(x, y, z, t = 0):
            
            if np.isscalar(x):
                x = [x]
            if np.isscalar(y):
                y = [y]
            if np.isscalar(z):
                z = [z]
            
            x = np.asarray(x)
            y = np.asarray(y)
            z = np.asarray(z)
            z_static = (z-zmean)*gamma0
            
            n = np.max([len(x), len(y), len(z)])
            
            F2d = np.zeros((len(x), 6))
            for i in np.arange(nbins):
                g0i = gamma0[i]; b0i = gamma2beta(g0i)
                z_static = (z-zmean)*g0i
                Ex_static = fEx3D_static[i]((x, y, z_static))*cc
                Ey_static = fEy3D_static[i]((x, y, z_static))*cc
                Ez_static = fEz3D_static[i]((x, y, z_static))*cc
                
                # Normalied E and B in lab frame
                
                F2d[:,0] += g0i*Ex_static
                F2d[:,1] += g0i*Ey_static
                F2d[:,2] += Ez_static
                F2d[:,3] += b0i*g0i*Ey_static/g_c
                F2d[:,4] -= b0i*g0i*Ex_static/g_c           
                #F2D = np.add(F2D, [g0i*Ex_static, g0i*Ey_static, Ez_static, b0i*g0i*Ey_static, -b0i*g0i*Ex_static, 0])
            
            return F2D
        
        self.EM3D = EM3D
        return
"""


"""
workdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\work\sync\python\space-charge'
os.chdir(workdir)

#dist = np.loadtxt('ast.0528.999')
dist = np.loadtxt('trk.002000.999')

dist[1:,2] += dist[0,2] 
dist[1:,5] += dist[0,5]

Qtot = 100e-12
sigma_x = 1e-3; sigma_y = 1e-3; sigma_z = 1e-3
Nx, Ny, Nz = 32, 32, 32
Nc = 5
gamma0 = 1. #+150./g_mec2; print gamma

xf, yf, zf = 0*sigma_x, 0, 0*sigma_x

import timeit
sc3d = SpaceCharge3DFFT(Nx, Ny, Nz, Nc, debug = 1, Lmirror = True)
time1 = timeit.default_timer()
#c3d.get_rho_3D1()
sc3d.get_rho_3D(dist = dist, Lemit = True)
sc3d.build(Xshift = (xf, yf, zf))
time2 = timeit.default_timer()
print 'time elapsed', time2-time1

sc3d4b = SpaceCharge3DFFT(Nx, Ny, Nz, Nc, debug = 1, Lmirror = True)
time1 = timeit.default_timer()
sc3d4b.get_rho_3D_binned(dist = dist, Lemit = True)
sc3d4b.build_binned(Xshift = (xf, yf, zf))
time2 = timeit.default_timer()
print 'time elapsed', time2-time1

def demo1(sc3d):
    sigma_x, sigma_y, sigma_z = sc3d.sigma_x, sc3d.sigma_y, sc3d.sigma_z
    Nx, Ny, Nz, Nc = sc3d.nx, sc3d.ny, sc3d.nz, sc3d.N_cutoff
    print sigma_x, sigma_y, sigma_z

    z0 = sc3d.zmean; print z0
    #z0 = 0;
    xx = np.linspace(-Nc*sigma_x, Nc*sigma_x, 2*Nx+1)+xf; #print xx
    yy = np.linspace(-Nc*sigma_y, Nc*sigma_y, 2*Ny+1); #print yy
    zz = np.linspace(-Nc*sigma_z, Nc*sigma_z, 2*Nz+1)+z0; #print zz
    #zz = sc3d.zgrid_static#/sc3d.gamma0[0]
    
    fig, ax = plt.subplots(figsize = (6, 4))
    ax.plot(xx/sigma_x,            np.array([sc3d.EM3D(x, 0, z0+0*sigma_z)[0] for x in xx])/1e3, '-*')
    ax.plot(xx/sigma_x,            np.array([sc3d.EM3D(x, 0, z0+1*sigma_z)[0] for x in xx])/1e3, '-o')
    ax.plot(xx/sigma_x,            np.array([sc3d.EM3D(x, 0, z0+2*sigma_z)[0] for x in xx])/1e3, '-^')
    ax.plot(xx/sigma_x,            np.array([sc3d.EM3D(x, 0, z0-1*sigma_z)[0] for x in xx])/1e3, '-o')
    ax.plot(xx/sigma_x,            np.array([sc3d.EM3D(x, 0, z0-2*sigma_z)[0] for x in xx])/1e3, '-^')
    ax.set_xlim(-Nc, Nc)
    #ax.set_ylim(-200, 200)
    #ax.set_yticks(np.linspace(-200, 200, 11))
    ax.grid()
    ax.set_xlabel(r'$x/\sigma_x$')
    ax.set_ylabel(r'$E_x(x)$ (kV/m)')
    ax.legend([r'$\sigma_z/z = 0$', r'$\sigma_z/z = 1$', r'$\sigma_z/z = 2$', r'$\sigma_z/z = -1$', r'$\sigma_z/z = -2$'])
    fig.savefig('Ex-x-z4.eps')

    fig, ax = plt.subplots(figsize = (6, 4))
    ax.plot(yy/sigma_y,            np.array([sc3d.EM3D(0, y, 0*sigma_z)[1] for y in yy])/1e3, '-*')
    ax.plot(yy/sigma_y,            np.array([sc3d.EM3D(0, y, 1*sigma_z)[1] for y in yy])/1e3, '-o')
    ax.plot(yy/sigma_y,            np.array([sc3d.EM3D(0, y, 2*sigma_z)[1] for y in yy])/1e3, '-^')
    ax.plot(yy/sigma_y,            np.array([sc3d.EM3D(0, y,-1*sigma_z)[1] for y in yy])/1e3, '-o')
    ax.plot(yy/sigma_y,            np.array([sc3d.EM3D(0, y,-2*sigma_z)[1] for y in yy])/1e3, '-^')
    ax.set_xlim(-Nc, Nc)
    #ax.set_ylim(-200, 200)
    ax.grid()
    ax.set_xlabel(r'$y/\sigma_y$')
    ax.set_ylabel(r'$E_y(y)$ (kV/m)')
    ax.legend([r'$\sigma_z/z = 0$', r'$\sigma_z/z = 1$', r'$\sigma_z/z = 2$', r'$\sigma_z/z = -1$', r'$\sigma_z/z = -2$'])
    fig.savefig('Ey-y-z4.eps')

    fig, ax = plt.subplots(figsize = (6, 4))
    ax.plot(zz*1e3, np.array([sc3d.EM3D(0, 0, z)[2] for z in zz])/1e3, '-')
    ax.plot(zz*1e3, np.array([sc3d.EM3D(sigma_x*0.375, 0, z)[2] for z in zz])/1e3, '-')
    ax.plot(zz*1e3, np.array([sc3d.EM3D(sigma_x*0.750, 0, z)[2] for z in zz])/1e3, '-')
    ax.plot(zz*1e3, np.array([sc3d.EM3D(sigma_x*1.125, 0, z)[2] for z in zz])/1e3, '-')
    ax.plot(zz*1e3, np.array([sc3d.EM3D(sigma_x*1.500, 0, z)[2] for z in zz])/1e3, '-')
    #ax.set_xlim(-Nc, Nc)
    #ax.set_ylim(-200, 200)
    ax.grid()
    ax.set_xlabel(r'$z$')
    ax.set_ylabel(r'$E_z(z)$ (kV/m)')
    ax.legend([r'$r/\sigma_x = 0$', r'$r/\sigma_x = 0.375$', r'$r/\sigma_x = 0.750$',               r'$r/\sigma_x = 1.125$', r'$r/\sigma_x = 1.500$'])
    fig.savefig('Ez-z-x4.eps')
    return
#demo1(sc3d)
def demo2():
    sigma_x, sigma_y, sigma_z = sc3d.sigma_x, sc3d.sigma_y, sc3d.sigma_z
    Nx, Ny, Nz, Nc = sc3d.nx, sc3d.ny, sc3d.nz, sc3d.N_cutoff
    print sigma_x, sigma_y, sigma_z
    
    z0 = sc3d.zmean; print z0
    
    xx = sc3d.xgrid
    yy = sc3d.ygrid
    zz = sc3d.zgrid+z0
    
    Nc = 3
    fig, ax = plt.subplots(figsize = (6, 4))
    ax.plot(xx/sigma_x,            np.array([sc3d.EM3D(x, 0, z0+0*sigma_z)[0] for x in xx])/1e3, 'ro')
    ax.plot(xx/sigma_x,            np.array([sc3d.EM3D(x, 0, z0+2*sigma_z)[0] for x in xx])/1e3, 'b^')
    
    z0 = sc3d4b.zmean
    xx = sc3d4b.xgrid
    ax.plot(xx/sigma_x,            np.array([sc3d4b.EM3D(x, 0, z0+0*sigma_z)[0] for x in xx])/1e3, 'g--')
    ax.plot(xx/sigma_x,            np.array([sc3d4b.EM3D(x, 0, z0+2*sigma_z)[0] for x in xx])/1e3, 'c--')
    
    
    ax.set_xlim(-Nc, Nc)
    #ax.set_ylim(-200, 200)
    #ax.set_yticks(np.linspace(-200, 200, 11))
    ax.grid()
    ax.set_xlabel(r'$x/\sigma_x$')
    ax.set_ylabel(r'$E_x(x)$ (kV/m)')
    ax.legend([r'$\sigma_z/z = 0$', r'$\sigma_z/z = 2$',               r'$\sigma_z/z = 0$, binned', r'$\sigma_z/z = 2$, binned'])
    fig.savefig('Ex-x-z41.eps')
    
    fig, ax = plt.subplots(figsize = (6, 4))
    ax.plot(zz*1e3, np.array([sc3d.EM3D(0, 0, z)[2] for z in zz])/1e3, 'ro')
    ax.plot(zz*1e3, np.array([sc3d.EM3D(sigma_x*1.5, 0, z)[2] for z in zz])/1e3, 'b^')
    
    zz = sc3d4b.zgrid+z0
    ax.plot(zz*1e3, np.array([sc3d4b.EM3D(0, 0, z)[2] for z in zz])/1e3, 'g--')
    ax.plot(zz*1e3, np.array([sc3d4b.EM3D(sigma_x*1.5, 0, z)[2] for z in zz])/1e3, 'c--')
    
    #ax.set_xlim(-Nc, Nc)
    #ax.set_ylim(-200, 200)
    ax.grid()
    ax.set_xlabel(r'$z$')
    ax.set_ylabel(r'$E_z(z)$ (kV/m)')
    ax.legend([r'$r/\sigma_x = 0$', r'$r/\sigma_x = 1.5$',               r'$r/\sigma_x = 0$, binned', r'$r/\sigma_x = 1.5$, binned'])
    fig.savefig('Ez-z-x41.eps')
    return
#demo2()
def demo3(sc3d):
    sigma_x, sigma_y, sigma_z = sc3d.sigma_x, sc3d.sigma_y, sc3d.sigma_z
    
    Nx, Ny, Nz, Nc = sc3d.nx, sc3d.ny, sc3d.nz, sc3d.N_cutoff
    print sigma_x, sigma_y, sigma_z
    
    zz = np.linspace(-Nc*sigma_z, Nc*sigma_z, Nz+1); #print xx
    Ez = np.array([sc3d.EM3D(0, 0, z)[2] for z in zz])
    
    data = np.loadtxt('R1mm.dat')
    
    fig, [ax1, ax2] = plt.subplots(figsize = (12, 4), ncols = 2)
    ax1.plot((zz+zf)/sigma_z, Ez/1e3, '-*')
    ax1.plot(data[:,0]/sigma_z, data[:,2]/1e3, '-')
    #ax1.set_ylim(-200, 0)
    ax1.grid()
    ax1.set_xlabel(r'$z/R$')
    ax1.set_ylabel(r'$E_z$ (kV/m)')
    
    cc = -1./4/np.pi/g_eps0
    
    ax2.plot((zz+zf)/sigma_z, sc3d.PHI[Nx/2-1, Ny/2-1, :]*1./4/np.pi/g_eps0, '-*')
    ax2.plot(data[:,0]/sigma_z, data[:,1], '-')
    ax2.grid()
    #ax2.set_ylim(0, 500)
    ax2.set_xlabel(r'$r/R$')
    ax2.set_ylabel(r'$\phi$ (V)')
    
    plt.tight_layout()
    #fig.savefig('field-on-axis-from-uniform-ball2.eps')
    return
#demo3(sc3d)
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Mar 25 14:49:13 2021

@author: lixiangk
"""

from universal import *

workdir = r'D:\PITZ\2020\LPS\250pC2'
os.chdir(workdir)

data = pd_loadtxt('ast.0528.001')

data[1:,5] += data[0,5]
data[:,5] = np.sqrt(data[:,3]**2+data[:,4]**2+data[:,5]**2)

#%%
data[0,2] = 0

slitWidth = 50

res = []
for slitWidth in np.linspace(50, 1000, 20):
    sw = slitWidth/1e6
    select = (data[:,0]>-sw/2)*(data[:,0]<sw/2)*(data[:,2]>-100e-6)*(data[:,2]<100e-6)
    
    std_Pz = np.std(data[select,5])
    res.append([slitWidth, std_Pz])
res = np.array(res)    


fig, ax = plt.subplots()
ax.plot(res[:,0], res[:,1]/1e3)
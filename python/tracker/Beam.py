# coding: utf-8
# get_ipython().magic(u'matplotlib inline')
# %matplotlib notebook

from universal import *

from scipy.optimize import minimize
from scipy.misc import derivative
from scipy.interpolate import interp2d
from scipy import signal
import pickle as pickle

import pandas as pd

from .SpaceCharge3DFFT import *

print('Importing Beam ...')

def nemixrms(x, xp, w = None):
    '''
    Parameters
      x, xp: phase space coordinates. xp could be either divergence or normalized momentum
      w: weighting factor or charge or the particles
    Returns
      emittance: phase space emittance
    '''
    if w is None:
        w = np.ones(len(x))
    x2 = weighted_mean(x*x, w)
    xp2 = weighted_mean(xp*xp, w)
    xxp = weighted_mean(x*xp, w)
    emit_x = np.sqrt(x2*xp2-xxp*xxp)
    return emit_x


class Beam():
    def __init__(self, fname = None, dist = None, Qtot = None, debug = 0):
        #self.diag = BeamDiagnostics(fname, dist)
        #BeamDiagnostics(fname, dist)
        self.sc3d = SpaceCharge3DFFT(debug = debug)
        
        #self.key_value = {'x':0, 'ux':1, 'y':2, 'uy':3, 'z':4, 'uz':5, 'w':6}
        self.key_value = {'x':0, 'y':1, 'z':2, 'Px':3, 'Py':4, 'Pz':5, 'w':7}
        
        if fname is not None:
            self.dist = np.loadtxt(fname)
            self.dist[1:,2] += self.dist[0,2]
            self.dist[1:,5] += self.dist[0,5]
        elif dist is not None:
            self.dist = dist
        else:
            self.dist = []
        if len(self.dist)>0:
            if Qtot is not None:
                qq = Qtot/np.sum(self.dist[:,7])
                self.dist[:,7] *= (qq*1e9)
        
        self.distold = self.dist.copy()    
        return
    
    def load(self, fname):
        #self.dist = np.loadtxt(fname)
        self.dist = pd.read_csv(fname, delimiter = '\s+', header = None).values
        self.dist[1:,2] += self.dist[0,2]
        self.dist[1:,5] += self.dist[0,5]
        self.distold = self.dist.copy()
        return
    
    def zmean(self):
        
        select = (self.dist[:,9]>0)
        z0 = weighted_mean(self.dist[select,2], self.dist[select,7])
        
        return z0
    
    def charge(self):
        
        select = (self.dist[:,9]>0)
        q0 = np.sum(self.dist[select,7])*1e-9
        
        return q0
    
    def dump(self, fname, time = None):
        dist = np.copy(self.dist)
        if dist[0,9] == -89:
            select = (dist[:,9]>0)
            dist[0,2] = weighted_mean(dist[select,2], dist[select,7])
            dist[0,5] = weighted_mean(dist[select,5], dist[select,7])
        dist[1:,2] -= dist[0,2]
        dist[1:,5] -= dist[0,5]
        
        if time is not None:
            dist[:,6] = time*1e9 # ns
            dist[1:,6] = dist[1:,2]/g_c*1e9
            
        np.savetxt(fname, dist, fmt = '%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%4.0f%4.0f')
        return
    
    def get_sc3d(self, **kwargs):
        self.sc3d.get_rho_3D(self.dist, **kwargs)
        self.sc3d.build(**kwargs)
        return self.sc3d.EM3D
    
    def diagnose(self, fname = None, key_value = None, em3d = None, time = None):
        #self.diag = BeamDiagnostics(dist = self.dist)
        self.diagnostics(key_value = key_value, em3d = em3d, time = time)
        if fname is not None:
            with open(fname, 'a') as f_handle:
                np.savetxt(f_handle, np.atleast_2d(self.x), fmt='%14.6E')
        return
    
    def diagnostics(self, key_value = None, tracking = 'Astra', momentum = 'eV/c', energy = True,\
                    em3d = None, time = 0, plot = False, fig_ext = '.eps'):
        '''
        Parameters
          x y z Px Py Pz w: 6D phase space of the particles
        Outputs
          [avg_z nemit_x nemit_y std_x std_y std_z Ekin std_Ekin nemit_z Q_b\
          avg_x avg_y alpha_x beta_x gamma_x emit_x alpha_y beta_y gamma_y emit_y\
          cor_zEk loss_cathode loss_aperture FWHM NoP I1 I0]: array
        '''
        
        tracking = tracking.upper()
        
        if tracking in ['ASTRA', 'A']:
            ix, iy, iz, iux, iuy, iuz, iw = 0, 1, 2, 3, 4, 5, 7
            weight_to_charge = 1e-9
        elif tracking in ['WARP', 'W']:
            ix, iy, iz, iux, iuy, iuz, iw = 0, 1, 2, 3, 4, 5, 6
            weight_to_charge = g_qe
            default_momentum = 'Dimentionless'
        else:
            ix, iy, iz, iux, iuy, iuz, iw = 0, 1, 2, 3, 4, 5, 6
            weight_to_charge = 1.0
        
        if key_value is not None:
            
            keys = key_value.keys()
            if 'x' in keys:      
                ix = key_value['x']
            if 'y' in keys:
                iy = key_value['y']
            if 'z' in keys:
                iz = key_value['z']
            if 'ux' in keys:
                iux = key_value['ux']
            if 'uy' in keys:
                iuy = key_value['uy']
            if 'uz' in keys:
                iuz = key_value['uz']
            if 'iw' in keys:
                iw = key_value['iw']
        
        dist = np.copy(self.dist)
        NoP = len(dist[:,1])
        
        if tracking in ['ASTRA']:
            select = (dist[:,9]==-89); n_lost_cathode = np.sum(select)
            select = (dist[:,9]==-15); n_lost_aperture = np.sum(select)

            select = (dist[:,9]>0)
            if np.sum(select) == 0:
                select = (dist[:,9]==-1); 
                if np.sum(select) == 0:
                    self.x = np.zeros(99)
                    return
                else:
                    dist[:,iz] = dist[:,6]*1e-9*g_c

            dist = dist[select]
        else:
            n_lost_cathode = -1
            n_lost_aperture = -1
        
        
        x, y, z = dist[:,ix], dist[:,iy] ,dist[:,iz] # positions in meter
        
        momentum = momentum.upper()
        
        if momentum == 'EV/C':
            bgx, bgy, bgz = dist[:,iux]/g_mec2/1e6, dist[:,iuy]/g_mec2/1e6, dist[:,iuz]/g_mec2/1e6 # dimentionless momentum
        elif momentum == 'DIMENTIONLESS':
            bgx, bgy, bgz = dist[:,iux], dist[:,iuy], dist[:,iuz]
        elif momentum == 'M/S':
            bgx, bgy, bgz = dist[:,iux]/g_c, dist[:,iuy]/g_c, dist[:,iuz]/g_c
            gamma = 1.0/np.sqrt(1.0-bgx**2-bgy**2-bgz**2)
            bgx, bgy, bgz = bgx*gamma, bgy*gamma, bgz*gamma
        
        
        w = dist[:,iw]*weight_to_charge # C
        
        if energy:
            Ek = np.sqrt(1+bgx**2+bgy**2+bgz**2)*g_mec2-g_mec2 # kinetic energy in MeV
        else:
            Ek = np.sqrt(bgx**2+bgy**2+bgz**2)*g_mec2 # P*c in MeV/c
            
        Qtot = np.sum(w) # C
        
        x2, y2, z2 = np.apply_along_axis(np.cov, arr = dist[:,0:3], axis = 0, aweights = np.abs(w), bias = True)
        std_x, std_y, std_z = np.sqrt([x2, y2, z2])
        
        xc, yc, zmean = np.apply_along_axis(weighted_mean, arr = dist[:,0:3], axis = 0, weights = w)
        
        xp = bgx/bgz; yp = bgy/bgz
        xp2 = weighted_cov(xp, xp, w); yp2 = weighted_cov(yp, yp, w)
        xxp = weighted_cov(x, xp, w);  yyp = weighted_cov(y, yp, w)
        
        emit_x = np.sqrt(x2*xp2-xxp*xxp); emit_y = np.sqrt(y2*yp2-yyp*yyp)
        alpha_x = -xxp/emit_x; beta_x = x2/emit_x; gamma_x = xp2/emit_x
        alpha_y = -yyp/emit_y; beta_y = y2/emit_y; gamma_y = yp2/emit_y
        
        fwhm = get_FWHM(z)
        #[I1, I0] = zdist(z, weights = w, plot = plot, fig_ext = fig_ext)
        
        select = (z-zmean>-5*std_z)*(z-zmean<5*std_z); Ek1 = Ek[select]; z1 = z[select]; w1 = w[select]
        cor_zEkz = weighted_mean(Ek1*z1, w1)-weighted_mean(Ek1, w1)*weighted_mean(z1, w1)
        cor_zEkz = cor_zEkz/weighted_std(z1, w1)*1e3
        
        
        if em3d is not None:
            Bz = em3d.get_field(0, 0, z, time)[:,5]; # print np.max(Bz)
            bgx -= 0.5*Bz*y*g_c/g_mec2/1e6  # += in astra
            bgy += 0.5*Bz*x*g_c/g_mec2/1e6  # -= in astra
        
        bgx2 = weighted_cov(bgx, bgx, w); bgy2 = weighted_cov(bgy, bgy, w)
        xbgx = weighted_cov(x, bgx, w);   ybgy = weighted_cov(y, bgy, w)
        
        nemit_x = np.sqrt(x2*bgx2-xbgx**2); nemit_y = np.sqrt(y2*bgy2-ybgy**2)
        
        x = np.array([zmean, nemit_x*1e6, nemit_y*1e6, std_x*1e3, std_y*1e3, std_z*1e3, 
                      weighted_mean(Ek, w), weighted_std(Ek, w)*1e3, nemixrms(z, Ek, w)*1e6, 
                      Qtot, xc, yc,
                      alpha_x, beta_x, gamma_x, emit_x, alpha_y, beta_y, gamma_y, emit_y,
                      cor_zEkz, n_lost_cathode, n_lost_aperture, fwhm*1e3, NoP])#, I1, I0])
        
        self.x = x
    
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 21 15:02:43 2020

@author: lixiangk
"""

from universal import *

#%%
sig_x = np.array([1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.55, 1.6, 1.7, 1.8, 1.9, 2.0])
alp_x = np.array([4.34, 5.35, 6.44, 7.62, 8.88, 10.23, 10.93,
                  11.67, 13.19, 14.81, 16.52, 18.32])
P0 = 17.05; gb = np.sqrt(1+M2G(P0)**2)
emit_x = 4.0/gb
beta_x = sig_x**2/emit_x
gam_x = (1+alp_x**2)/beta_x

fig, [ax1, ax2, ax3] = plt.subplots(nrows = 3, figsize = (6, 6))
ax1.plot(sig_x, beta_x)
ax2.plot(beta_x, alp_x)
ax3.plot(beta_x, gam_x)
for axis in [ax1, ax2, ax3]:
    axis.grid()
#%%
data = np.loadtxt('ast.Xemit.001')
gb = np.sqrt(1+M2G(diag.Ek)**2)
emit = data[:,5]/gb
beta = data[:,3]**2/emit
gamma = data[:,4]**2/emit
alpha = -data[:,6]/emit

fig, [ax1, ax2, ax3] = plt.subplots(nrows = 3, figsize = (6, 6), sharex = True)
ax1.plot(data[:,0], beta)
ax2.plot(data[:,0], gamma)
ax3.plot(data[:,0], alpha)

data = np.loadtxt('ast.Yemit.001')
gb = np.sqrt(1+M2G(diag.Ek)**2)
emit = data[:,5]/gb
beta = data[:,3]**2/emit
gamma = data[:,4]**2/emit
alpha = -data[:,6]/emit

ax1.plot(data[:,0], beta)
ax2.plot(data[:,0], gamma)
ax3.plot(data[:,0], alpha)

ax1.set_ylabel(r'$\beta$ (m)')
ax2.set_ylabel(r'$\gamma$ (m$^-1$)')
ax3.set_ylabel(r'$\alpha$')
ax1.set_ylim(0, 30)
for axis in [ax1, ax2, ax3]:
    axis.grid()
ax3.set_xlabel(r'$z$ (m)')
fig.tight_layout()
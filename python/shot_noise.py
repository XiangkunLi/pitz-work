# -*- coding: utf-8 -*-
"""
Created on Tue Nov  3 21:37:24 2020

@author: lixiangk
"""

from universal import *

workdir = os.path.join(simdir, '2020', 'Warp')
os.chdir(workdir)

data = pd_loadtxt('beam_und_250k_ham_sampled.warp')
x, y, z, ux, uy, uz, w = data.T[:]

t = 0
z -= 10e-3 # beam center in the lab frame
vx, vy, vz = momentum2velocity(ux, uy, uz)


### Boosted factor
gamma_boost = 12.25 # 12.25
lbf = LorentzBoost(gamma_boost)


### Transform into boosted frame
xp, yp, zp, tp = lbf.spacetime(x, y, z, t)
vxp, vyp, vzp = lbf.velocity(vx, vy, vz)

tp_0 = tp
zp -= tp*vzp
tp[:] = 0

### Transform back into lab frame again
xx, yy, zz, tt = lbf.inverse_spacetime(xp, yp, zp, tp)
vxx, vyy, vzz = lbf.inverse_velocity(vxp, vyp, vzp)

#%%
#######
lambda0 = 100e-6
lambdau = 3e-2
#gamma_boost = 12.25
ne_per_macro = 24966.036297843053

inv_gamma_boost = 1./gamma_boost
beta_boost = np.sqrt(1.-inv_gamma_boost**2)

# Wavelength of radiation and undulator period
lambda0_boost = lambda0*(1+beta_boost)*gamma_boost
lambdau_boost = lambdau/gamma_boost

k0_boost = 2*np.pi/lambda0_boost
ku_boost = 2*np.pi/lambdau_boost

seed = 1
np.random.seed(1234567*seed)
rand = np.random.rand
#######

#%
t0, t1 = 0, np.max(tp)
dt = 114.9e-15*1

zb4 = np.zeros(4*len(zp))
tb4 = np.zeros(4*len(zp))
vzb4 = np.zeros(4*len(zp))

time = t0
nall = 0
while time<=t1:
    
    select = (tp>=time)*(tp<time+dt)
    ns = np.sum(select); #print('%d particles selected!' % ns)
    
    xb = xp[select]
    yb = yp[select]
    zb = zp[select]
    tb = tp[select]
    vxb = vxp[select]
    vyb = vyp[select]
    vzb = vzp[select]
    
    Lb = (time-tb)*vzb
    #xb = xb+vxb/vzb*Lb
    #yb = yb+vyb/vzb*Lb
    zb = zb+Lb
    
    #vxb = vxb/gi
    #vyb = vyb/gi
    #vzb = vzb/gi
    
    wt = (k0_boost-ku_boost*beta_boost)*g_c*tb
    wt = 0
    #ku_boost = 0
    theta_boost = (k0_boost+ku_boost)*zb-wt
    
    base = np.floor(theta_boost/(2*np.pi))
    remainder = np.mod(theta_boost, 2*np.pi)
    
    rn = rand(len(zb), 2)
    phi_n = 2*np.pi*rn[:,0]
    an = 2*np.sqrt(-np.log(rn[:,1])/(4*ne_per_macro))
    
    for i in np.arange(4):
        
        theta_boost_i = base*2*np.pi+np.mod(2.0*np.pi*i/4+remainder, 2*np.pi)
            
        dtheta_boost_i = -an*np.sin(theta_boost_i+phi_n) 
        theta_boost_i += dtheta_boost_i
        
        #zb_i = theta_boost_i/(k0_boost+ku_boost)
        zb_i = (theta_boost_i+wt)/(k0_boost+ku_boost)
        
        if i == 0:
            Dz_i = zb-zb_i
            
        zb4[nall+i:nall+ns*4+i:4] = zb_i[:] #+Dz_i
        #zb4[nall+i:nall+ns*4+i:4] = zb_i[:] #+Lb
        tb4[nall+i:nall+ns*4+i:4] = tb
        vzb4[nall+i:nall+ns*4+i:4] = vzb
        
    time += dt
    nall += ns*4
    if nall>10000:
        pass
        #break
#%
theta = (k0_boost+ku_boost)*np.copy(zb4)-(k0_boost-ku_boost*lbf.beta)*g_c*0
bf0 = np.mean(np.exp(-theta[:]*1j))
bf2, bf2_mean = np.abs(bf0)**2, 1.0/(4e-9/g_qe)
print(1/bf2, 1/bf2_mean, bf2/bf2_mean)

#%%
for i in np.arange(5):
    print((zb4[4*i:4*i+4]-zb4[4*i])*1e6)
    
_, _, z04, t04 = lbf.inverse_spacetime(0, 0, zb4, tb4)
_, _, vz04 = lbf.inverse_velocity(0, 0, vzb4)

z04 -= t04*vz04
plt.figure()
_ = plt.hist(z04*1e3, 100, histtype = r'step')

for i in np.arange(5):
    print((z04[4*i:4*i+4]-z04[4*i])*1e6)
    
#%%
zb4_0 = zb4-tb4*vzb4
_ = plt.hist(zb4_0*1e3, 100, histtype = r'step')

#%%
workdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim\2020\Warp\16x2x2_37'
os.chdir(workdir)

beam = pd_loadtxt('particle_at_start_in_boosted_frame0.dat')

#%%
zp = beam[:,2]
vzp = beam[:,5]
tp = beam[:,6]
zp0 = zp-tp*vzp
zp2 = zp0+tp*np.mean(vzp)

plt.figure()
_ = plt.hist(zp*1e3, 100, histtype = r'step')
_ = plt.hist(zp0*1e3, 100, histtype = r'step')
_ = plt.hist(zp1*1e3, 100, histtype = r'step')
#%%
beam = pd_loadtxt('shotnoise_test/particle_at_start_in_boosted_frame0.dat')

zp = beam[:,2]
tp = beam[:,6]

gg = 1/np.sqrt(1-(beam[:,3]**2+beam[:,4]**2+beam[:,5]**2)/g_c**2)
vzp = beam[:,5]

tp0 = 0
zp0 = zp-tp*vzp

zp1 = zp0+tp*np.mean(vzp)

plt.figure()
_ = plt.hist(zp*1e3, 100, histtype = r'step')
_ = plt.hist(zp0*1e3, 100, histtype = r'step')
_ = plt.hist(zp1*1e3, 100, histtype = r'step')

#%
theta = (k0_boost+ku_boost)*np.copy(zp0)-(k0_boost-ku_boost*lbf.beta)*g_c*tp0
bf0 = np.mean(np.exp(-theta[:]*1j))
bf2, bf2_mean = np.abs(bf0)**2, 1.0/(4e-9/g_qe)
print(1/bf2, 1/bf2_mean, bf2/bf2_mean)

theta = (k0_boost+ku_boost)*np.copy(zp)-(k0_boost-ku_boost*lbf.beta)*g_c*tp
bf0 = np.mean(np.exp(-theta[:]*1j))
bf2, bf2_mean = np.abs(bf0)**2, 1.0/(4e-9/g_qe)
print(1/bf2, 1/bf2_mean, bf2/bf2_mean)


#%%
workdir = r'C:\Users\lixiangk\Desktop\check_astra_Pz'
os.chdir(workdir)

data = pd_loadtxt('beam_und_200k.ini')

plt.figure(1)
_ = plt.hist(data[:,2], 100, histtype = r'step')

plt.figure(2)
_ = plt.hist(data[1:,5], 100, histtype = r'step')

#%%
t, z, vz = 0, 1, 0.999*g_c

gamma = 12.25
beta = gamma2beta(gamma)

Lp = z*gamma*(1+vz/g_c*beta); print(Lp)

Lp = z/gamma/(1-vz/g_c*beta); print(Lp)

#%%

ne = 1000000 
nmacro = 100000

lam_s = 100e-6
lam_u = 3e-2

k0 = 2*np.pi/lam_s
ku = 2*np.pi/lam_u

rd = np.random
res = []
for seed in np.arange(1, 100):
    rd.seed(1234567*seed)
    
    modulated = rd.normal(0, 0.5, nmacro)
    uniform = rd.rand(nmacro)*4-2
    
    modulated *= 100e-6/4
    uniform *= 100e-6/4
    
    zz = modulated
    theta = (k0+ku)*np.copy(zz)
    bf0 = np.mean(np.exp(-theta[:]*1j)) # bunching
    bf2, bf2_mean = np.abs(bf0)**2, 1.0/ne

    zz = uniform
    theta = (k0+ku)*np.copy(zz)
    bf0 = np.mean(np.exp(-theta[:]*1j)) # bunching
    bf22, bf2_mean = np.abs(bf0)**2, 1.0/ne
    
    res.append([seed, bf2, bf22])

res2 = np.array(res)
#%%
plt.figure()
_ = plt.hist(modulated*1e6, 50, histtype = r'step')
_ = plt.hist(uniform*1e6, 50, histtype = r'step')
plt.xlabel(r'$z$ (um)')
plt.xlim(-50, 50)
plt.legend(['modulated', 'uniform'])
plt.savefig('modulated-dist.png')

#%%

fig, [ax1, ax2] = plt.subplots(nrows = 2, figsize = (6, 5), sharex = True)
ax1.plot(res1[:,0], res1[:,1], '*')
ax1.plot(res2[:,0], res2[:,1], '*')

ax2.plot(res1[:,0], res1[:,2], '*')
ax2.plot(res2[:,0], res2[:,2], '*')

ax1.legend(['nmacro/ne = 0.01', 'nmacro/ne = 0.1'])
ax2.legend(['nmacro/ne = 0.01', 'nmacro/ne = 0.1'])

ax1.set_ylabel('$|b(\omega)|^2$')
ax2.set_ylabel('$|b(\omega)|^2$')
ax2.set_xlabel('seed for random generator')

ax1.set_title('modulated beam')
ax2.set_title('uniform beam')

fig.savefig('bunching-factor.png')

#%%
print('inverse bunching from modulated: ', 1/bf2)
print('ratio to shot noise: ', bf2/bf2_mean)

zz = uniform
theta = (k0+ku)*np.copy(zz)
bf0 = np.mean(np.exp(-theta[:]*1j)) # bunching
bf2, bf2_mean = np.abs(bf0)**2, 1.0/ne
print('inverse bunching from modulated : ', 1/bf2, bf2/bf2_mean)
print('ratio to shot noise: ', bf2/bf2_mean)

#%%
workdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim\2020\Warp\16x2x2_test'
os.chdir(workdir)

a = np.loadtxt('../beam_und_250k_ham_sampled.warp')
b = np.loadtxt('xyzpr01.dat')

x, y, z, ux, uy, uz, w = a.T[:]

t = 0
z -= 5e-3 # beam center in the lab frame
vx, vy, vz = momentum2velocity(ux, uy, uz)

x1 = x-(z+10e-3)/vz*vx #*lbf.gamma

### Boosted factor
gamma_boost = 12.25 # 12.25
lbf = LorentzBoost(gamma_boost)


### Transform into boosted frame
xp, yp, zp, tp = lbf.spacetime(x, y, z, t)
vxp, vyp, vzp = lbf.velocity(vx, vy, vz)

gamma = 33.366
beta = gamma2beta(gamma)
beta = np.mean(vz)/g_c

vbeamframe = g_c*(beta-lbf.beta)/(1.-beta*lbf.beta)

### Shift to tp = 0
tp_0 = tp
zp -= tp*vbeamframe
xp -= tp*vxp
yp -= tp*vyp

#tp[:] = 0

xp1, yp1, zp1, tp1 = b.T[:]

plt.figure(1); plt.cla()
_ = plt.hist(zp*1e3, 50, histtype = r'step')
_ = plt.hist(zp1*1e3, 50, histtype = r'step')

plt.figure(2); plt.cla()
_ = plt.hist(x1*1e3, 50, histtype = r'step')
_ = plt.hist(xp*1e3, 50, histtype = r'step')
_ = plt.hist(xp1*1e3, 50, histtype = r'step')

plt.figure(3); plt.cla()
plt.plot(zp*1e3, (tp-tp1)*1e12, '.')
#plt.plot(zp1*1e3, tp1*1e12, '-')
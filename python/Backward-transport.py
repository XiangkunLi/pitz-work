# -*- coding: utf-8 -*-
"""
Created on Thu Jun  4 16:22:33 2020

@author: lixiangk
"""

from astra_modules import *
from my_object import *
from FELs import *
import pickle
#from tracker import *

color_cycle = ['r', 'b', 'g', 'c', 'm', 'y', 'k']
linestyle_cycle = ['-', '--', '-.', ':', (0, (5, 2, 5, 2)), (0, (10, 2, 5, 2, 2, 2)), (0, (12, 2, 2, 2))]
marker_cycle = ['o', 'd', 'v', '^', '<', '>', '*']

def plot_config():
    from cycler import cycler
    from matplotlib.ticker import AutoMinorLocator

    fsize = 14 # a quarter of the paper width: 20 pt; half of the paper width: 12
    font = {'size' : fsize, 'family' : 'serif'}
    color_cycle = ['r', 'b', 'g', 'c', 'm', 'y', 'k']
    linestyle_cycle = ['-', '--', '-.', ':', (0, (5, 2, 5, 2)), (0, (10, 2, 5, 2, 2, 2)), (0, (12, 2, 2, 2))]
    marker_cycle = ['o', 'd', 'v', '^', '<', '>', '*']
    
    mpl.rc('font', **font)
    mpl.rc('xtick', labelsize = 10, direction = 'in', top   = True)
    mpl.rc('ytick', labelsize = 10, direction = 'in', right = True)
    mpl.rc('xtick.major', size = 5, width = 1)
    mpl.rc('ytick.major', size = 5, width = 1)
    mpl.rc('xtick.minor', size = 3, width = 0.7, visible = True)
    mpl.rc('ytick.minor', size = 3, width = 0.7, visible = True)
    
    mpl.rc('lines', linewidth=2, markersize=6, color='r')
    # mpl.rc('lines', linestyle = 'solid')
    mpl.rc('axes', labelpad = 0,\
           prop_cycle=(cycler('color', color_cycle) + cycler('linestyle', linestyle_cycle) + cycler('marker', marker_cycle)))
    mpl.rc('legend', fontsize = 12, labelspacing = 0.05, handletextpad=0.4, frameon=False, handlelength=2.1)
    
    mpl.rc('figure', dpi = 100, figsize = (4, 4))
    mpl.rc('figure.subplot', bottom = 0.15, top = 0.9, left = 0.15, right = 0.9)
    
    mpl.rc('image', cmap = 'jet')
    
    return
plot_config()

rootdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\work'
simdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim'
os.chdir(simdir)

# Frequently used functions
I2B = lambda I: -(0.0000372+0.000588*I)
B2I = lambda B: (-B-0.0000372)/0.000588
momentum2kinetic(6.3)

def timestamp(option = True):
    import datetime

    today = datetime.datetime.now()

    if option:
        stamp = '__%02d.%02d.%02d %02d:%02d:%02d__' % (today.day, today.month, today.year,
                                                       today.hour, today.minute, today.second)
    else:
        timestamp = '__%02d.%02d.%02d__%02d_%02d_%02d__' % (today.day, today.month, today.year,
                                                            today.hour, today.minute, today.second)
    return stamp
#%%
def __convert2array__(x, *args):
    ndims = -np.ones(1+len(args))
    
    if not np.isscalar(x):    
        ndims[0] = np.asarray(x).size
    
    if len(args)>0:
        for i, v in enumerate(args):
            if not np.isscalar(v):
                ndims[1+i] = np.asarray(v).size
    ndims = np.array(ndims, dtype = np.int)
    
    r = np.zeros((np.max(ndims), 1+len(args)))
    
    if ndims[0] == -1:
        r[:,0] = x
    else:
        r[:ndims[0],0] = x
    
    for i, d in enumerate(ndims[1:]):
        if d == -1:
            r[:,1+i] = args[i]
        else:
            r[:d,1+i] = args[i]
    
    return r.T[:]

def velocity2momentum(vx, vy, vz, unit = 1):
    
    vx, vy, vz = __convert2array__(vx, vy, vz)
    
    beta = np.sqrt(vx*vx+vy*vy+vz*vz)/g_c
    gamma = 1.0/np.sqrt(1.0-beta*beta)
    
    ux = vx/g_c*gamma*unit    
    uy = vy/g_c*gamma*unit
    uz = vz/g_c*gamma*unit
    
    return ux, uy, uz

def momentum2velocity(ux, uy, uz, unit = 1):
    
    ux, uy, uz = __convert2array__(ux, uy, uz)
    
    ux, uy, uz = ux/unit, uy/unit, uz/unit
    
    gamma = np.sqrt(1+ux*ux+uy*uy+uz*uz)
    
    vx = ux*g_c/gamma
    vy = uy*g_c/gamma
    vz = uz*g_c/gamma
    
    return vx, vy, vz

class LorentzBoost():
    
    def __init__(self, gamma = 1):
        
        self.gamma = gamma
        self.beta  = gamma2beta(gamma)
        
    def spacetime(self, x, y, z, t = 0, gamma = None):
        
        if gamma is not None:
            gamma = gamma
        else:
            gamma = self.gamma
        
        beta  = gamma2beta(gamma)
        
        x, y, z, t = __convert2array__(x, y, z, t)
        
        xp = x
        yp = y
        zp = gamma*(z-beta*g_c*t)
        tp = gamma*(t-beta*z/g_c)
        
        return xp, yp, zp, tp
    
    def inverse_spacetime(self, xp, yp, zp, tp = 0, gamma = None):
        
        if gamma is not None:
            gamma = gamma
        else:
            gamma = self.gamma
        
        beta  = gamma2beta(gamma)
        
        xp, yp, zp, tp = __convert2array__(xp, yp, zp, tp)
        
        x = xp
        y = yp
        z = gamma*(zp+beta*g_c*tp)
        t = gamma*(tp+beta*zp/g_c)
        
        return x, y, z, t
    
    def velocity(self, vx, vy, vz, gamma = None):
    
        if gamma is not None:
            gamma = gamma
        else:
            gamma = self.gamma
        
        beta  = gamma2beta(gamma)
        
        vx, vy, vz = __convert2array__(vx, vy, vz)
        
        cc = 1.0/(1.0-beta*vz/g_c)
        
        vxp = cc*vx/gamma
        vyp = cc*vy/gamma
        vzp = cc*(vz-beta*g_c)
        
        return vxp, vyp, vzp
    
    def inverse_velocity(self, vxp, vyp, vzp, gamma = None):
        
        if gamma is not None:
            gamma = gamma
        else:
            gamma = self.gamma
        
        beta  = gamma2beta(gamma)
        
        vxp, vyp, vzp = __convert2array__(vxp, vyp, vzp)
        
        cc = 1.0/(1.0+beta*vzp/g_c)
        
        vx = cc*vxp/gamma
        vy = cc*vyp/gamma
        vz = cc*(vzp+beta*g_c)
        
        return vx, vy, vz
    
    def copropagating_length(self, L, u, v):
        '''
        Parameters
          L: length measured in a static reference frame of F
          u: velecity of the copropagating object in the static reference frame of F
          v: velocity of the moving frame of F' in the direction of z in the frame of F
        Returns
          L': length measured in the moving frame of F'
        '''
        beta = v/g_c
        gamma = beta2gamma(beta)
        return L/gamma/(1.-u/g_c*beta)

#%%
workdir = os.path.join(simdir, '2020', 'Warp')
os.chdir(workdir)

#data = pd_loadtxt('beam_und_250k_4_shot.warp')

with open('16x2x2_20/lab_diags/data/data-0.pkl', 'rb') as f:
    pkl = pickle.load(f)
data = pkl['beam']


x, y, z, ux, uy, uz, w = data.T[:]

t = 0
#z -= 10e-3 # beam center in the lab frame
vx, vy, vz = momentum2velocity(ux, uy, uz)

### Boosted factor
gamma_boost = 12.25
lbf = LorentzBoost(gamma_boost)

### Transform into boosted frame
xp, yp, zp, tp = lbf.spacetime(x, y, z, t)
vxp, vyp, vzp = lbf.velocity(vx, vy, vz)

### Transform back into lab frame again
xx, yy, zz, tt = lbf.inverse_spacetime(xp, yp, zp, tp)
vxx, vyy, vzz = lbf.inverse_velocity(vxp, vyp, vzp)

### Plot: beam in the boosted frame, x' or y' vs z
fig, ax = plt.subplots(figsize = (6, 4))
ax.plot(zp*1e3, xp*1e3, '.')
ax.plot(zp*1e3, yp*1e3, '.')
ax.grid()

ax.set_xlabel(r'$z^{\prime}$ (mm)')
ax.set_ylabel(r'$x^{\prime}, y^{\prime}$ (mm)')

### Plot: beam in the boosted frame, t' vs z'
fig, ax = plt.subplots(figsize = (6, 4))
ax.plot(zp*1e3, tp*1e12, '.')
ax.plot(zp*1e3, tp*1e12, '.')
ax.grid()

ax.set_xlabel(r'$z^{\prime}$ (mm)')
ax.set_ylabel(r'$t^{\prime}$ (ps)')

### Plot: beam velocities in lab and boosted frames
fig, [ax1, ax2, ax3] = plt.subplots(ncols = 3, figsize = (9, 3))
r = ax1.hist(vx/g_c,  bins = 100, histtype = r'step')
r = ax1.hist(vxp/g_c, bins = 100, histtype = r'step')
r = ax2.hist(vy/g_c,  bins = 100, histtype = r'step')
r = ax2.hist(vyp/g_c, bins = 100, histtype = r'step')
r = ax3.hist(vz/g_c,  bins = 100, histtype = r'step')
r = ax3.hist(vzp/g_c, bins = 100, histtype = r'step')
fig.tight_layout()

### Shift the beam to t' = 0
tp0 = tp - tp
xp0 = xp - vxp*tp
yp0 = yp - vyp*tp
zp0 = zp - vzp*tp

### Plot: beam dist at t' = 0
fig, ax = plt.subplots(figsize = (6, 4))
ax.plot(zp0*1e3, xp0*1e3, '.')
ax.plot(zp0*1e3, yp0*1e3, '.')
ax.grid()

ax.set_xlabel(r'$z^{\prime}$ (mm)')
ax.set_ylabel(r'$x^{\prime},y^{\prime}$ (mm)')
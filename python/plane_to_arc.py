import os
import numpy as np
import scipy as sp
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d

def _asin_(x, y):
    a = np.arcsin(y/np.sqrt(x**2+y**2))
    a = np.where(x<=0, np.pi-a, a)
    return a

def arc_length(a, b, x = 0):
    '''
    Parameters
      a, b: horizontal semi-axis and vertical semi-axis lengths, respectively, regardless which is the marjor axis
      x: position in the horizontal axis
    Returns
      larc: the length of the arc between x(=0) and x=a
    '''
    
    A = b
    m = 1-1.0*a*a/b/b
    p = np.arccos(x/a)
    
    larc = A*sp.special.ellipeinc(p, m)
    return larc

def farc2xy(a, b):
    '''
    Parameters
      a, b: horizontal semi-axis and vertical semi-axis lengths, respectively, regardless which is the marjor axis
    Returns
      f_arc2x, f_arc2y: interpolation functions from acr length to x and y coordinates
    '''

    xx = np.linspace(0, a, 999)
    yy = np.sqrt(1-xx**2/a**2)*b
    ll = arc_length(a, b, xx)
    
    f_arc2x = interp1d(ll, xx, bounds_error = False, fill_value = 0)
    f_arc2y = interp1d(ll, yy, bounds_error = False, fill_value = 0)
    
    return f_arc2x, f_arc2y

def plane_to_arc1(dist, R, A, B, R1 = None, R2 = None, out = 'dist1.ini'):
    data = np.loadtxt(dist)
    data[1:,5] += data[0,5]

    rmax = np.max(np.sqrt(data[:,0]**2+data[:,1]**2))
    
    ARC = arc_length(B, A) # one quarter of the ellipse perimeter
    r0 = (R+ARC)/rmax # length from cathode center to vertice of ellipse
    print(rmax, ARC, A, r0)

    data[:,0] *= r0 # zoom  x
    data[:,1] *= r0 # zoom y

    fig, ax = plt.subplots(figsize = (6, 4))
    ax.plot(data[::50,0]*1e3, data[::50,1]*1e3, 'r.')
    ax.grid()

    xx = data[:,0]
    yy = data[:,1]

    px = data[:,3]
    py = data[:,4]
    pz = data[:,5]

    #p0 = np.sqrt(data[:,5]**2)

    rr = np.sqrt(xx**2+yy**2)
    pr = np.sqrt(px**2+py**2)

    select = (rr>R) # select particle outside the flat surface
    
    xs = xx[select]
    ys = yy[select]

    pxs = px[select]
    pys = py[select]
    pzs = pz[select]

    rs = rr[select]
    prs = pr[select]
    
    f_arc2x, f_arc2y = farc2xy(B, A)
    
    ### Projection, not uniform
    #rn = (rs-R)*A/ARC      # new r from the ellipse center, vertical
    #dz = np.sqrt(1.0-rn**2/A**2)*B # distrance to the ellipse center, longitudinal

    dz = f_arc2x(rs-R)
    rn = f_arc2y(rs-R)
    #dz = np.sqrt(1.0-rn**2/A**2)*B
    
    kk = -dz/rn*A**2/B**2 # slope of tangent line 
    c_theta = -kk/np.sqrt(1+kk**2)
    s_theta = 1./np.sqrt(1+kk**2)
    
    theta_p = _asin_(pxs, pys)
    theta_x = _asin_(xs, ys)
    
    xn = (rn+R)*np.cos(theta_x) # new x
    yn = (rn+R)*np.sin(theta_x) # new y
    zn = -B+dz # new z

    prr = prs*np.cos(theta_x-theta_p) # radial momentum 
    prt = prs*np.sin(theta_x-theta_p) # azmuthal momentum

    pzn = pzs*c_theta-prr*s_theta
    #pzn[pzn<0] = 0
    prn = pzs*s_theta+prr*c_theta

    pxn =  prt*np.cos(np.pi/2-theta_x)+prn*np.cos(theta_x)
    pyn = -prt*np.sin(np.pi/2-theta_x)+prn*np.sin(theta_x)

    ax.plot(xn[::50]*1e3, yn[::50]*1e3, 'b.')
    
    data[select,0] = xn
    data[select,1] = yn
    data[select,2] = zn
    data[select,3] = pxn
    data[select,4] = pyn
    data[select,5] = pzn
    
    data[1:,5] -= data[0,5]
    
    rr = np.sqrt(data[:,0]**2+data[:,1]**2)
    select = (rr>=0)
    if R1 is not None:
        select = select*(rr>R1)
    if R2 is not None:
        select = select*(rr<R2)
    
    if select[0] is not True:
        select[0] = True
    data1 = data[select]
    
    ax.plot(data1[::50,0]*1e3, data1[::50,1]*1e3, 'g.')
    ax.legend(['all plane', 'arc section', 'selected'], loc = (1, 0.6))
    ax.set_aspect(1)
    fig.savefig('xy@'+out+'.png')
    
    fig, ax = plt.subplots()
    ax.plot(data1[::50,2]*1e3, np.sqrt(data1[::50,0]**2+data1[::50,1]**2)*1e3, 'r.')
    ax.grid()
    fig.savefig('rz@'+out+'.png')
    
    np.savetxt(out, data1, fmt = '%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%4d%4d')
    return

def plane_to_arc2(dist, R, A, B, R3 = None, R4 = None, out = 'dist2.ini'):
    data = np.loadtxt(dist)
    data[1:,5] += data[0,5]

    rmax = np.max(np.sqrt(data[:,0]**2+data[:,1]**2))
    
    ARC = arc_length(B, A) # one quarter of the ellipse perimeter
    r0 = (R+ARC)/rmax # length from cathode center to vertice of ellipse
    print(rmax, ARC, A, r0)

    data[:,0] *= r0 # zoom  x
    data[:,1] *= r0 # zoom y

    fig, ax = plt.subplots(figsize = (6, 4))
    ax.plot(data[::50,0]*1e3, data[::50,1]*1e3, 'r.')
    ax.grid()

    xx = data[:,0]
    yy = data[:,1]

    px = data[:,3]
    py = data[:,4]
    pz = data[:,5]

    rr = np.sqrt(xx**2+yy**2)
    pr = np.sqrt(px**2+py**2)

    select = (rr>R)*(rr<R+A) # select particle outside the flat surface
    
    xs = xx[select]
    ys = yy[select]

    pxs = px[select]
    pys = py[select]
    pzs = pz[select]

    rs = rr[select]
    prs = pr[select]
    
    f_arc2x, f_arc2y = farc2xy(B, A)
    
    #rn = (rs-R)*A/ARC      # new r from the ellipse center, vertical
    #dz = np.sqrt(1.0-rn**2/A**2)*B # distrance to the ellipse center, longitudinal

    dz = f_arc2x(-rs+R+A)
    rn = -f_arc2y(-rs+R+A); print np.min(rn), np.max(rn)
    #dz = np.sqrt(1.0-rn**2/A**2)*B
    
    kk = -dz/rn*A**2/B**2 # slope of tangent line 
    c_theta = -kk/np.sqrt(1+kk**2)
    s_theta = 1./np.sqrt(1+kk**2)
    #return

    theta_p = _asin_(pxs, pys)
    theta_x = _asin_(xs, ys)
    
    xn = (rn+R+A)*np.cos(theta_x) # new x
    yn = (rn+R+A)*np.sin(theta_x) # new y
    zn = -B+dz # new z

    prr = prs*np.cos(theta_x-theta_p) # radial momentum 
    prt = prs*np.sin(theta_x-theta_p) # azmuthal momentum

    pzn = pzs*c_theta-prr*s_theta
    pzn[pzn<0] = 0
    prn = pzs*s_theta+prr*c_theta

    P0 = pzn
    
    pxn =  prt*np.cos(np.pi/2-theta_x)+prn*np.cos(theta_x)
    pyn = -prt*np.sin(np.pi/2-theta_x)+prn*np.sin(theta_x)

    ax.plot(xn[::50]*1e3, yn[::50]*1e3, 'b.')
    
    data[select,0] = xn
    data[select,1] = yn
    data[select,2] = zn
    data[select,3] = pxn
    data[select,4] = pyn
    data[select,5] = pzn

    data[1:,5] -= data[0,5]
    
    rr = np.sqrt(data[:,0]**2+data[:,1]**2)
    select = (rr>=0)
    if R3 is not None:
        select = select*(rr>R3)
    if R4 is not None:
        select = select*(rr<R4)
    
    if select[0] is not True:
        select[0] = True
    data1 = data[select]
    
    ax.plot(data1[::50,0]*1e3, data1[::50,1]*1e3, 'g.')
    ax.legend(['all plane', 'arc section', 'selected'], loc = (1, 0.6))
    ax.set_aspect(1)
    fig.savefig('xy@'+out+'.png')
    
    fig, ax = plt.subplots()
    ax.plot(data1[::50,2]*1e3, np.sqrt(data1[::50,0]**2+data1[::50,1]**2)*1e3, 'r.')
    ax.grid()
    fig.savefig('rz@'+out+'.png')
    
    np.savetxt(out, data1, fmt = '%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%4d%4d')
    return

#workdir = r'C:\Users\lixiangk\Desktop\dist'
#os.chdir(workdir)

from astra_modules import *

os.chdir('C:\Users\lixiangk\Desktop\dist')

### Generate a uniform distribution with a radius of 1
gen = Generator1(FNAME = 'dist.ini', Lprompt = False, Species = 'electrons',\
                 IPart = 100000, Q_total = -4., Probe = False,\
                 Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',\
                 Dist_z = 'g', sig_clock = 36.37e-3, Cathode = True,\
                 Dist_x = 'r', sig_x = 1/2., Dist_px = 'g', Nemit_x = 0,\
                 Dist_y = 'r', sig_y = 1/2., Dist_py = 'g', Nemit_y = 0)
gen.write('gen.in')

# dist.ini is generated from Astra and has a uniform transverse distribution
dist, R, A, B = 'dist.ini', 5e-3, 3e-3, 0.4e-3
plane_to_arc1(dist, R, A, B, R1 = 6e-3, R2 = 7e-3, out = 'dist1.ini')

dist, R, A, B = 'dist.ini', 8.3e-3, 3e-3, 0.4e-3
plane_to_arc2(dist, R, A, B, R3 = 9e-3, R4 = 10e-3, out = 'dist2.ini')

# -*- coding: utf-8 -*-
"""
Created on Fri Feb 28 17:20:23 2020

@author: lixiangk
"""

from .Undulator import *

class FEL(object):
    hamonics = 0
    Alfven = 17e3
    scheme = NotImplemented
    def __init__(self, und = LCLS1(), gamma = 0, lam_s = 0, **kwargs):
        
        self.und = und
        
        if gamma:
            self.gamma = gamma
            self.lam_s = resonant_wavelength(und.K, und.lam_u, gamma)
            self.freq_s = g_c/self.lam_s
            self.omega_s = 2.0*np.pi*g_c/self.lam_s
        elif lam_s:
            self.lam_s = lam_s
            self.gamma = resonant_energy(und.K, und.lam_u, lam_s)
            self.freq_s = g_c/self.lam_s
            self.omega_s = 2.0*np.pi*g_c/self.lam_s
        else:
            message = 'Either `gamma` or `lam_s` should be defined in the arguments!'
            raise UndefinedError(message)
        self.set(**kwargs)
        
    def set(self, **kwargs):
        if len(kwargs):
            for key in kwargs.keys():
                self.__dict__[key] = kwargs[key]
                
class Superradiance(FEL):
    scheme = 'SR'
    def __init__(self, Qtot, Ipeak = 0, dist = 'Gaussian',
                 und = LCLS1(), sig_t = 0, gamma = 0, lam_s = 0, **kwargs):
        super(Superradiance, self).__init__(und = und, gamma = gamma,
              lam_s = lam_s, **kwargs)
        self.Qtot = Qtot
        
        if dist == 'Gaussian':
            if Ipeak:
                self.I = Ipeak
                self.sig_t = Qtot/Ipeak/np.sqrt(2.0*np.pi)
                self.FWHM = self.sig_t*2.355
                self.sig_z = self.sig_t*g_c
            elif sig_t:
                self.sig_t = sig_t
                self.I = Qtot/self.sig_t/np.sqrt(2.0*np.pi)
                self.FWHM = self.sig_t*2.355
                self.sig_z = self.sig_t*g_c
            else:
                message = 'Either `Ipeak` or `sig_t` should be defined in the arguments!'
                raise UndefinedError(message)
        elif dist == 'Flattop':
            if Ipeak:
                self.I = Ipeak
                self.sig_t = Qtot/Ipeak/np.sqrt(12.0)
                self.FWHM = Qtot/Ipeak
                self.sig_z = self.sig_t*g_c
            elif sig_t:
                self.sig_t = sig_t
                self.I = Qtot/self.sig_t/np.sqrt(12.0)
                self.FWHM = Qtot/self.I
                self.sig_z = self.sig_t*g_c
            else:
                message = 'Either `Ipeak` or `sig_t` should be defined in the arguments!'
                raise UndefinedError(message)
        else:
            message = 'Unknown distribution type, should be either "Gaussian" or "Falttop"'
            raise UndefinedError(message)
        
        
    def spectral_energy_density(self, ww, n = 0):
        '''
        The spectral energy density per electron of the radiation emittied forward
        direction for the m-th harmonic
        Parameters
          ww: angular frequency of interest
          m=2n+1: harmonic number
        Returns
          
        '''
        
        gamma, Nu, K = self.gamma, self.und.Nu, self.und.K
        w1 = self.omega_s
        
        m = 2*n+1
        wm = w1*m
        y = np.pi*(ww-wm)/w1
        
        c1 = g_qe**2*gamma**2*m**2*K**2/4.0/np.pi/g_eps0/g_c/(1+K**2/2)**2
        #print([K, n, JJ2(K, n)])
        return c1*sos(Nu, y)*JJ2(K, n)
    
    def angular_width(self, n = 0):
        '''
        Angular width of the radiation of the mth harmonic, m = 2*n+1
        '''
        
        gamma, Nu, K = self.gamma, self.und.Nu, self.und.K
        m = 2*n+1
        
        return 1.0/gamma*np.sqrt((1+K**2/2)/2./m/Nu)

    def solid_angle(self, n = 0):
        '''
        Solid angle of the radiation of the mth harmonic, m = 2*n+1
        '''
        
        gamma, Nu, K = self.gamma, self.und.Nu, self.und.K
        m = 2*n+1
        
        return 2.0*np.pi/gamma**2*(1+K**2/2)/2./m/Nu

    def fractional_bandwidth(self, n = 0):
        '''
        Fractional bandwidth of the radiation of the mth harmonic, m = 2*n+1
        '''
        
        w1 = self.omega_s
        
        Nu = self.und.Nu
        m=2*n+1
        wm=w1*m
        
        return 1.0/m/Nu*wm
    
    def radiation(self, ww, n = 0):
        '''
        Radiation from a single electron of the mth harmonic, m = 2*n+1
        '''
        
        #print([self.spectral_energy_density(ww, n), self.solid_angle(n), self.fractional_bandwidth(n)])
        return self.spectral_energy_density(ww, n)*self.solid_angle(n)*self.fractional_bandwidth(n)
    
    def radiation_from_bunch(self, ww, n = 0):
        '''
        Radiation from the electron bunch of the mth harmonic, m = 2*n+1
        '''
        
        lam_s = self.lam_s
        Qtot, sig_t  = self.Qtot,   self.sig_t
        
        Ne = Qtot/g_qe
        ff2 = form_factor(lam_s, sig_t)**2
        self.ff2 = ff2
        
        return self.radiation(ww, n)*(Ne+Ne*(Ne-1)*ff2)

class SASE(Superradiance):
    scheme = 'SASE'
    def __init__(self, Qtot, Ipeak, dist = 'Gaussian', sig_x = 1e-3,
                 und = LCLS1(), gamma = 0, lam_s = 0, **kwargs):
        super(SASE, self).__init__(Qtot, Ipeak, dist = dist,
             und = und, gamma = gamma, lam_s = lam_s, **kwargs)
        
        self.sig_x = sig_x
        self.rho = self.pierce_parameter()
        self.Lg = self.gain_length()
        self.Lc = self.cooperation_length()
        
    def pierce_parameter(self):
        '''
        Parameters
          K: undulator parameter
          gamma: energy of electron bunch, mec2
          lam_u: undulator period, meter
          B: magnetic field amplitude, Tesla
        Returns
          rho: pierce parameter, unitless
        '''
        
        gamma, I, sigma_x = self.gamma, self.I, self.sig_x
        K, lam_u = self.und.K, self.und.lam_u
        
        ku = 2*np.pi/lam_u
        
        rho = (1./16*I/self.Alfven*K**2*JJ2(K)/gamma**3/sigma_x**2/ku**2)**(1./3)
        return rho
    def pierce_parameter_3D(self):
        pass
    def gain_length(self):
        '''
        Parameters
          rho: pierce parameter
          lam_u: undulator period
        Returns
          Lg: gain length, meter
        '''
        
        lam_u, rho = self.und.lam_u, self.rho
        Lg = lam_u/4./np.pi/np.sqrt(3)/rho
        return Lg
    
    def cooperation_length(self):
        '''
        The cooperation length "may be interpreted as the slippage distance in one gain length" [1].
        Parameters
          rho: pierce parameter
          lam_s: radiation wavelength
        Returns
          Lc: cooperation length, meter
        Reference
          [1] R. Bonifacio, et al. Superradiance in the high-gain free-electron laser, 
              PHYSICAL REVIEW A VOLUME 40, NUMBER 8 OCTOBER 15, 1989
        '''
        
        lam_s, rho = self.lam_s, self.rho
        Lc = lam_s/4./np.pi/rho
        return Lc
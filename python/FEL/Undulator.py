# -*- coding: utf-8 -*-
"""
Created on Mon Jun  8 18:52:15 2020

@author: lixiangk
"""

from .basic import *

class Error(Exception):
    """Base class for exceptions in this module."""
    pass

class UndefinedError(Error):
    """Exception raised for errors of not defined argument.

    Attributes:
        message -- explanation of the error
    """
    
    def __init__(self, message):
        self.message = message

class UnknownError(Error):
    """Exception raised for errors of unknown values.

    Attributes:
        message -- explanation of the error
    """
    
    def __init__(self, message):
        self.message = message
        
class Undulator(object):
    Nu = NotImplemented
    lam_u = NotImplemented
    B = NotImplemented
    K = NotImplemented
    def __init__(self, **kwargs):
        if self.B is not NotImplemented and self.lam_u is not NotImplemented:
            self.K = undulator_parameter(self.B, self.lam_u)
        else:
            pass
        self.set(**kwargs)
    def set(self, **kwargs):
        if len(kwargs):
            for key in kwargs.keys():
                self.__dict__[key] = kwargs[key]
                
            if 'B' in kwargs.keys() and self.lam_u is not NotImplemented:
                self.K = undulator_parameter(self.B, self.lam_u)
            if 'K' in kwargs.keys() and self.lam_u is not NotImplemented:
                self.B = undulator_field(self.K, self.lam_u)
                
class LCLS1(Undulator):
    Nu = 113
    lam_u = 3e-2
    B = 1.28
    def __init__(self, **kwargs):
        super(LCLS1, self).__init__()  
        self.set(**kwargs)

# -*- coding: utf-8 -*-
"""
Created on Tue Feb  9 15:54:45 2021

@author: lixiangk
"""

from universal import *

workdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim\2020\MBI'
os.chdir(workdir)

reset_margin()

#%% EMSY1 and EMSY2
fig, ax = plt.subplots()

data = np.loadtxt('ParaScanSol22MeV_9ps_kicked.dat')
select = data[:,0] == 4500

ax.plot(data[select, 6], data[select,8])

data = np.loadtxt('ParaScanSol22MeV_9ps_EMSY2_kicked.dat')
select = data[:,0] == 4500

ax.plot(data[select, 6], data[select,12])

ax.grid()
ax.set_ylim(0, 10)

ax.set_xlabel(r'Imain (A)')
ax.set_ylabel(r'Norm. emittance (mmmrad)')

ax.legend(['EMSY1', 'EMSY2'])
fig.savefig('Xemit-EMSY1-vs-EMSY2-kicked.eps')

#
fig, ax = plt.subplots()

data = np.loadtxt('ParaScanSol22MeV_9ps_kicked.dat')
select = data[:,0] == 4500

ax.plot(data[select, 6], data[select,10])

data = np.loadtxt('ParaScanSol22MeV_9ps_EMSY2_kicked.dat')
select = data[:,0] == 4500

ax.plot(data[select, 6], data[select,10])

ax.grid()
ax.set_ylim(0, 5)

ax.set_xlabel(r'Imain (A)')
ax.set_ylabel(r'RMS size (mm)')

ax.legend(['EMSY1', 'EMSY2'])
fig.savefig('Xrms-EMSY1-vs-EMSY2-kicked.eps')

#%% kicked vs non-kicked in low section
fig, ax = plt.subplots()

data = np.loadtxt('ParaScanSol22MeV_9ps_nokick.dat')
select = data[:,0] == 4500

ax.plot(data[select, 6], data[select,8])


data = np.loadtxt('ParaScanSol22MeV_9ps_kicked4.dat')
select = data[:,0] == 4500

ax.plot(data[select, 6], data[select,9])
ax.grid()

ax.set_xlabel(r'Imain (A)')
ax.set_ylabel(r'Norm. emittance (mmmrad)')

ax.legend(['no kick', 'kicked'])
fig.savefig('Xemit-nokick-vs-kicked4.eps')

#%%
fig, ax = plt.subplots()

data = np.loadtxt('ParaScanSol22MeV_9ps_nokick.dat')
select = data[:,0] == 4500

ax.plot(data[select, 6], data[select,10])


data = np.loadtxt('ParaScanSol22MeV_9ps_kicked4.dat')
select = data[:,0] == 4500

ax.plot(data[select, 6], data[select,10])
ax.grid()

ax.set_xlabel(r'Imain (A)')
ax.set_ylabel(r'RMS size (mm)')

ax.legend(['no kick', 'kicked'])
fig.savefig('Xrms-nokick-vs-kicked4.eps')
#%% simulating slit scan

from universal import *
from image_process.ImageProcess import *
from image_process.PhaseSpace import *

workdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim\2020\MBI'
os.chdir(workdir)

workdir = r'Q-4500.00pC-D-4.20mm-E1-60.00MV_m-phi1-0.00deg-E2-18.59MV_m-phi2--20.00deg-I-386.00A'
os.chdir(workdir)

dx = 0e-6

data = pd_loadtxt('ast.0527.006')
data[1:,2] += data[0,2]+dx; data[1:,5]+=data[0,5]
select = data[:,-1]>0
data = data[select]

xp = data[:,3]/data[:,5]
yp = data[:,4]/data[:,5]

xx = data[:,0]+L*xp
yy = data[:,1]+L*yp
        
rr = [[np.min(xx), np.max(xx)], [np.min(yy), np.max(yy)]]

sscan = SlitScan(step = 50e-6, momentum = data[0,5]/1e6)

L = sscan.drift
slitWidth = sscan.slitWidth*2
gg = momentum2gamma(sscan.momentum)
step = sscan.step


xmax = 5e-3
xpmax = 1.5e-3

nrows = 150
nsteps = int(xmax*2/step)

PS = np.zeros((nrows, nsteps))

fig, ax = plt.subplots()

for i in np.arange(nsteps):
    xslit = -xmax+step/2.0+step*i
    
    select = (data[:,0]>xslit-slitWidth/2.0)*(data[:,0]<xslit+slitWidth/2.0)
    bl1 = data[select]
    
    dist = bl1
    
    if len(dist)>0:
        xp = dist[:,3]/dist[:,5]; print(np.std(xp)*1e3, ' mrad')
        yp = dist[:,4]/dist[:,5]
        
        xx = dist[:,0]+L*xp; print(np.std((xx-xslit)/L)*1e3, ' mrad')
        yy = dist[:,1]+L*yp
        
        hist, _ = np.histogram((xx-xslit)/L, bins = nrows,
                               range = (-xpmax, xpmax))
        print(hist.shape, PS.shape)
        PS[:,i] = hist[:]

        if np.mod(i, 5) == 0:
            
            counts, _, _ = np.histogram2d(xx, yy, bins = (500, 600), range = rr)
            if np.sum(counts)>0:
                
                counts[counts==0] = np.nan
                ax.imshow(counts.T)
                
                #ax.plot(xx*1e3, yy*1e3, '.')
                #ax.set_title('frame: %d' % i)
                ax.set_title('slit pos.: %.1f mm' % (xslit*1e3))
                ax.set_xlabel(r'$x$ (pixel)')
                ax.set_ylabel(r'$y$ (pixel)')
                
                fig.savefig('slit-frame-%d.png' % i)
                
                plt.pause(0.1)
                plt.cla()
                
#PS -= 0.001*np.max(PS); PS[PS<0] = 0
Xscale = step; Yscale = 2*xpmax/nrows
[xc, xpc, xrms, xprms] = calcRMS(PS[::-1], Xscale, Yscale)
[eps_x, beta_x, gamma_x, alpha_x] = calcTwiss(PS[::-1], Xscale, Yscale)

plotImage(PS, scale = [Xscale*1e3, Yscale*1e3], 
          extent = [-xmax*1e3, xmax*1e3, -xpmax*1e3, xpmax*1e3])
print(eps_x*gg)

#%% Booster BBA
workdir = 'booster-BBA-check'
#os.chdir(workdir)

P0 = 6.3e6 # eV/c

def RotateMatrix(theta, axis = 'x'):
    cs = np.cos(theta)
    ss = np.sin(theta)
    
    if axis in ['x', 'X']:
        M = np.array([[1, 0, 0],
                      [0, cs, -ss],
                      [0, ss, cs]])
    if axis in ['y', 'Y']:
        M = np.array([[cs, 0, ss],
                      [0, 1, 0],
                      [-ss, 0, cs]])
    if axis in ['z', 'Z']:
        M = np.array([[cs, -ss, 0],
                      [ss, cs, 0],
                      [0, 0, 1]])
    
    return M

def PhaseToPosition(dphi, P0 = 6.3e6, f0 = 1.3e9):
    
    beta = gamma2beta(M2G(P0/1e6))
    
    dz = dphi/360.0/f0*beta*g_c
    
    return dz
P2P = PhaseToPosition

a = [[0, 0, 0, 0, 0, 6.3e6, 0, 1, 1, 5]]
for x in np.linspace(-10, 10, 41):
    for xp in np.linspace(-10, 10, 81):
        M = RotateMatrix(xp*1e-3, 'y')

        P0 = [0, 0, 6.3e6]
        P1 = M @ np.asarray(P0)
        
        for dphi in np.linspace(-45, 45, 19):
            dz = PhaseToPosition(dphi)
            
            a.append([x*1e-3, 0, dz, P1[0], P1[1], P1[2], 0, 1, 1, 5])
            
a = np.array(a)

a[1:,5] -= a[0,5]

np.savetxt('refs-bf-booster.dat', a, fmt = ast_fmt)

#%%
workdir = 'booster-BBA-check'
#os.chdir(workdir)

dist = np.loadtxt('ast.0523.001')

nx, nxp = 41, 81
nphi = 19
it = 0
res = []
for x in np.linspace(-10, 10, nx):
    for xp in np.linspace(-10, 10, nxp):
        tmp = dist[1+1+it*nphi:1-1+it*nphi+nphi,0]
        #goal = np.std(tmp)
        goal = np.max(tmp)-np.min(tmp)
        it += 1
        
        res.append([x, xp, goal])
res = np.array(res)        
np.savetxt('booster-BBA-check.dat', res, fmt = '%14.6f%14.6f%14.6f', 
           header = 'x (mm)  xp (mm)  max. offset (m)')
#
XX = np.reshape(res[:,0], (nx, nxp))
YY = np.reshape(res[:,1], (nx, nxp))
ZZ = np.reshape(res[:,2], (nx, nxp))*1e3 # mm

ZZ[ZZ>0.5] = np.nan

levels = np.linspace(0, 1, 11)*0.5 #*np.max(ZZ)

fig, ax = plt.subplots()
#cax = ax.imshow(ZZ, extent = [-10, 10, -10, 10])
cax = ax.contourf(XX, YY, ZZ, levels)
cbar = fig.colorbar(cax, pad = 0.046, ax = ax)
cbar.ax.set_ylim(0, 0.5)
cbar.set_ticks([0, 0.1, 0.2, 0.3, 0.4, 0.5])
cbar.ax.set_ylabel(r'max. offset (mm)', labelpad = 2)

ax.grid()
ax.set_xlim(-5, 5)
ax.set_ylim(-5, 5)

ax.set_xlabel(r'$\langle x \rangle$ (mm)')
ax.set_ylabel(r'$\langle x^{\prime} \rangle$ (mrad)')

fig.savefig('booster-BBA-check.png')

#%% laser offset
workdir = 'booster-BBA-check'
#os.chdir(workdir)

P0 = 750 # eV/c

a = [[0, 0, 0, 0, 0, P0, 0, 1, 1, 5]]
for x in np.linspace(-10, 10, 41):

        a.append([x*1e-3*np.cos(np.pi/4), x*1e-3*np.sin(np.pi/4), 0, 0, 0, P0, 0, 1, 1, 5])
            
a = np.array(a)

a[1:,5] -= a[0,5]

np.savetxt('refs-at-cathode2.dat', a, fmt = ast_fmt)

#%%
dist = np.loadtxt('ast.0096.002')
dist[1:,5] += dist[0,5]

nx = 41
it = 0
res = []
for x in np.linspace(-10, 10, nx):
    tmp =dist[it+1]
    it += 1
    
    res.append([x, tmp[0], tmp[1], tmp[3]/tmp[5], tmp[4]/tmp[5]])
res = np.array(res)
res[:,1:] *= 1e3
np.savetxt('laser-offset-check.dat', res, fmt = '%14.6f', 
           header = 'x0 (mm)  x (mm)  y (mm)  xp (mrad) yp (mrad)')
#
fig, ax = plt.subplots()
ax.plot(res[:,0], res[:,1], '-<')
ax.plot(res[:,0], res[:,3], '->')
#ax.plot(np.sqrt(res[:,1]**2+res[:,2]**2), np.sqrt(res[:,3]**2+res[:,4]**2), '-D')
#ax.plot(res[:,1], res[:,2], '.')

#ax.plot(res[:,0], dist[1:,0]/dist[1:,1],'.')
#ax.plot(res[:,0], dist[1:,3]/dist[1:,4],'.')

ax.grid()

# ax.set_xlim(-10, 1)
# ax.set_ylim(-5, 5)

ax.set_xlabel(r'$\langle x \rangle$ (mm)')
ax.set_ylabel(r'$\langle x^{\prime} \rangle$ (mrad)')

fig.savefig('laser-offset-check.png')
"""
openPMD-viewer

Usage
-----
See the class OpenPMDTimeSeries to open a set of openPMD files
"""
# Make the OpenPMDTimeSeries object accessible from outside the package
from . import universal

# Define the version number
from .__version__ import __version__
__all__ = ['universal', '__version__']

# -*- coding: utf-8 -*-
"""
Created on Mon Mar  9 16:56:20 2020

@author: lixiangk
"""

from universal import *
from interface.Genesis import *
from FEL.basic import *

simdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim'



#%% Batch generate inputs

workdir = os.path.join(simdir, r'2019', 'LCLS-I-matching-60um4',
                       '_n200k-sig_x-1.05mm-sig_y-0.30mm-alp_x-6.40-alp_y-3.00',
                       'SASE-001')
#workdir = os.path.join(simdir, r'2018\LCLS-I\4.50mm\SASE5-flipped')
workdir = get_path()
os.chdir(workdir)

beamfile = '../pithz.2745.013'
maginfile = '../LCLS-I.lat'

P0 = 17 # MeV/c
#P0 = 2.054595E+01

Q = 2500e-12 # Coulumm

lam_s = 100e-6
Nu, lam_u, B = 120, 3.0e-2, 1.2799839
#Nu, lam_u, B = 150, 4.0e-2, 1.005783

K = undulator_parameter(B, lam_u); print ('undulator parameter: ', K)
#K = K*np.sqrt(2)
#K = 3.49

FWHM = 21.5e-12 # second
sigma_z = FWHM*g_c
curpeak = -Q/FWHM; print ('peak current: ', curpeak)

Ek = momentum2kinetic(P0) # MeV
gamma = kinetic2gamma(Ek); # unitless
delgam = gamma*0.5e-2


#lam_s = 60e-6
#lam_s=resonant_wavelength(K, lam_u, gamma)
freq_s = g_c/lam_s
print ('resonant wavelength from kinetic energy: ', lam_s*1e6, ' um')

gamma1 = resonant_energy(K,lam_u,lam_s)
delgam = gamma*0.5e-2
#print 'resonant momentum from radiation wavelength: ', kinetic2momentum(gamma1*g_mec2-g_mec2)

nslice = Nu+sigma_z/lam_s; print ('# of slice: ', nslice-Nu)
nslice = 180
ntail = 0

emit_x, emit_y = 4e-6, 4e-6
sigma_x, sigma_y = 1.58e-3, 0.18e-3
alpha_x, alpha_y = 7.18, 1.72

delz = 0.5
ipseed = 1
fname = 'pithz.%d' % ipseed

gen = Genesis()

# set the undulator
gen.set(aw0   = K/np.sqrt(2.),\
        awd   = K/np.sqrt(2.),\
        nwig  = Nu,\
        xlamd = lam_u)

# set the electron beam
gen.set(gamma0  = gamma,\
        delgam  = delgam,\
        curpeak = curpeak,\
        rxbeam  = sigma_x,\
        rybeam  = sigma_y,\
        emitx   = emit_x,\
        emity   = emit_y,\
        alphax  = alpha_x,\
        alphay  = alpha_y,\
        npart   = 8192*2)

# set the grids
gen.set(nslice = nslice,\
        ntail  = ntail,\
        iotail = 1,\
        curlen = sigma_z,\
        itdp   = 1,\
        nbins  = 16)

# set the radiation and control
gen.set(ffspec = 1, ippart = 0, ipradi = 0)
gen.set(xlamds = lam_s, zrayl = 3.5251E-02) 
#gen.set(xlamds = lam_s, zrayl = 3.0E-02) # test once, no impact on radiation
gen.set(nptr = 170, dgrid = 0.02, nscr = 2, nscz = 1)
gen.set(delz = delz, zstop = Nu*lam_u)

# gen.set(ipseed = ipseed, delaw = 0.0/np.sqrt(2.), iertyp = 1)
# gen.set(distfile = 'pithz.beam.in', outputfile = fname+'.out')

# gen.write(fname)
# gen.qsub(fname)

cmd = ''; cmd2 = ''
rdm = np.random.rand(100)
samples = []

for Ipeak in np.linspace(100, 100, 1):
    # Ipeak = 200
    # beamfile = '../pithz.%dA.g.beam' % Ipeak
    beamfile = beamfile
    for i in np.arange(1, 101):
        ipseed = i
        # iseed = np.random.randint(5000, 10000)
        fname = 'pithz.%d' % (ipseed)
        
        #rr = rdm[ipseed-1]
        Kerr = 0 # K/np.sqrt(2.)*(0.01+0.01*rr)
        #samples.append([ipseed, 0.01+0.01*rr])
        gen.set(ipseed = ipseed, delaw = Kerr, iertyp = 0)
        gen.set(beamfile = beamfile, outputfile = fname+'.out', maginfile = maginfile)

        gen.write(fname)
        gen.qsub(fname)

        cmd += 'qsub '+fname+'.sh\n'

        #cmd2 += 'head -n 405 '+fname+'.out | tail -n 227 > '+fname+'.txt\n'
#write2file(cmd, 'sub.sh')
#write2file(cmd2, 'fld.sh')

#%% Analyze one case
workdir = os.path.join(simdir, r'2019\THz-60um-3\_\SASE-014')
#workdir = get_path()
workdir = os.path.join(simdir, r'2019\LCLS-I-matching-60um2\_n200k-sig_x-1.50mm-sig_y-0.30mm-alp_x-15.25-alp_y-9.25\SASE-002')
workdir = os.path.join(simdir, r'2019\LCLS-I-matching-60um\_n200k-sig_x-1.50mm-sig_y-0.30mm-alp_x-3.75-alp_y-2.50\SASE-002')

workdir = os.path.join(simdir, r'2019\LCLS-I-matching-60um\_n200k-sig_x-1.50mm-sig_y-0.30mm-alp_x-3.75-alp_y-2.50')
workdir = os.path.join(simdir, r'2019\LCLS-I-matching-60um3\_n200k-sig_x-1.00mm-sig_y-0.30mm-alp_x-5.00-alp_y-3.00')

workdir = r'D:\PITZ\2020\SASE-scan\beam_100A_1.0nC__g'
workdir = get_path()
os.chdir(workdir)

gen = Genesis()
gen.readdata('SASE-001/pithz.1.out')

plt.figure()

i = 99
plt.title('slice No. %d' % i)

plt.plot(gen.get_slice(i)[:,6])

gen = Genesis()
gen.readdata('SASE-001/pithz.2.out')
plt.plot(gen.get_slice(i)[:,6])

plt.legend(['- chirp', '+ chirp'])

#f = h5py.File('pithz.5.out.h5')
#plt.plot(f['bunching'][:,i])

#%%
#path = get_path(); os.chdir(path)

plt.figure()

i = 20
gen1 = Genesis()
gen1.readdata('beam_200A_2.0nC_7\pithz.1.out')
x = np.arange(gen1.data.shape[1])*3e-2*0.5
plt.plot(x, gen1.get_slice(i)[:,6], 'r-')

i = 20
gen2 = Genesis()
gen2.readdata('beam_200A_3.0nC_7\pithz.1.out')
x = np.arange(gen2.data.shape[1])*3e-2*0.5
plt.plot(x, gen2.get_slice(i)[:,6], 'b-')


i = 20
gen3 = Genesis()
gen3.readdata('beam_200A_4.0nC_7\pithz.1.out')
x = np.arange(gen3.data.shape[1])*3e-2*0.5
plt.plot(x, gen3.get_slice(i)[:,6], 'g-')

i = 40
plt.plot(x, gen2.get_slice(i)[:,6], 'b--')

i = 55
plt.plot(x, gen3.get_slice(i)[:,6], 'g--')

plt.xlabel(r'$z$ (m)')
plt.ylabel(r'Bunching factor')
plt.legend(['2 nC, # 20', '3 nC, # 20', '4 nC, # 20',
            '3 nC, # 40', '4 nC, # 55'])

plt.xlim(2, 3.6)
plt.savefig('bunching-factor-N20+.eps')
#%%
fig, ax = plt.subplots()
        
data1 = gen1.data[:,:,0]
ncols = data1.shape[1]

data2 = gen2.data[:,:,0]
ncols = data2.shape[1]

data3 = gen3.data[:,:,0]
ncols = data3.shape[1]

x1 = np.arange(gen1.data.shape[0])*100e-6/g_c*1e12
x2 = np.arange(gen2.data.shape[0])*100e-6/g_c*1e12
x3 = np.arange(gen3.data.shape[0])*100e-6/g_c*1e12

ax.plot(x1, data1[:,-1]/1e6, '-')
ax.plot(x2, data2[:,-1]/1e6, '-')
ax.plot(x3, data3[:,-1]/1e6, '-')

ax.set_xlabel(r'Time (ps)')
ax.set_ylabel(r'Power (MW)')

ax.set_xlim(0, 50)
ax.legend(['2 nC', '3 nC', '4 nC'])

fig.savefig('THz-pulse.eps')
#%%
fig, ax = plt.subplots()
for i in np.arange(ncols):
    
    if (np.mod(i, 5)) == 0:
        ax.plot(x1, data1[:,i], '-')
        ax.plot(x2, data2[:,i], '-')
        ax.plot(x3, data3[:,i], '-')
        #ax.set_title(r'')
        #ax.set_xlabel(r'')
        #ax.set_ylabel(r'')
        
        plt.pause(0.3)
        #input("Press Enter to continue...")
        if i<ncols-1:
            plt.cla()

#%%
plt.figure()
gen1.data.shape
s1 = gen1.current[:,1]>0
plt.plot(gen1.data[s1,-1,6], '-')
s2 = gen2.current[:,1]>0
plt.plot(gen2.data[s2,-1,6], '-')
s3 = gen3.current[:,1]>0
plt.plot(gen3.data[s3,-1,6], '-')
plt.legend(['2 nC', '3 nC', '4 nC'])
#%%
plt.figure()

plt.plot(gen1.current[:,1], '-')
plt.plot(gen2.current[:,1], '-')
plt.plot(gen3.current[:,1], '-')
plt.legend(['2 nC', '3 nC', '4 nC'])
plt.xlabel(r'# of slice')
plt.ylabel(r'Current (A)')
plt.xlim(0, 100)
plt.savefig('current-profile.eps')
#%%
plt.figure()

gen = Genesis()

gen.readdata('pithz.101.out')
energy = gen.get_energy()

plt.plot(energy[:,0], energy[:,1]*1e6)

gen.readdata('pithz.1.out')
energy = gen.get_energy()

plt.plot(energy[:,0], energy[:,1]*1e6)

gen.readdata('pithz.2.out')
energy = gen.get_energy()

plt.plot(energy[:,0], energy[:,1]*1e6)

gen.readdata('pithz.4.out')
energy = gen.get_energy()

plt.plot(energy[:,0], energy[:,1]*1e6)

plt.legend(['0 0 170', '1 2 170', '4 4 170', '1 1 100'])

#%%
# Radation energy and power

workdir = os.path.join(simdir, '2020', 'SASE-scan2', 'beam_150A_2.0nC_3THz_')
#os.chdir(workdir)

gen = Genesis()
gen.readdata('pithz.1.out')

power = gen.get_power()
energy = gen.get_energy()

fig, [ax1, ax2] = plt.subplots(nrows = 1, ncols = 2, figsize = (10, 4))

ax1.plot(power[:,0], power[:,1]/1e6, 'b*-') # energy: MW
ax1.set_xlabel(r'$z$ (m)')
ax1.set_ylabel(r'power (MW)')
ax1.set_yscale('log')

ax2.plot(energy[:,0], energy[:,1]*1e6, 'g*-') # energy: uJ
ax2.set_xlabel(r'$z$ (m)')
ax2.set_ylabel(r'energy ($\mu$J)')

ax1.grid()
ax2.grid()

fig.tight_layout()

# Radiation spectrum
tspec = gen.get_power_at(); # print tspec
fspec = gen.get_spectrum_at()

fig, [ax1, ax2] = plt.subplots(nrows = 1, ncols = 2, figsize = (10, 4))
ax1.plot(tspec[:,0]*1e6, tspec[:,1], 'r*-')
ax1.set_xlabel(r'slice ($\mu$m)')
ax1.set_ylabel(r'power (W)')

ax2.plot(fspec[:,0]*1e6, fspec[:,1], 'r*-')
ax2.set_xlabel(r'$\lambda_s$ ($\mu$m)')
ax2.set_ylabel(r'intensity (arb. unit)')

ax1.grid()
ax2.grid()

fig.tight_layout()

# Radiation density and transverse size
rad_size = gen.get_radiation_size_at(); # print tspec
rad_density = gen.get_power_density_at()

fig, [ax1, ax2] = plt.subplots(nrows = 1, ncols = 2, figsize = (10, 4))
ax1.plot(rad_size[:,0]*1e6, rad_size[:,1], 'r*-')
ax1.set_xlabel(r'slice ($\mu$m)')
ax1.set_ylabel(r'rad. size (m)')

ax2.plot(rad_density[:,0]*1e6, rad_density[:,1], 'r*-')
ax2.set_xlabel(r'$\lambda_s$ ($\mu$m)')
ax2.set_ylabel(r'rad. density ()')

ax1.grid()
ax2.grid()

fig.tight_layout()

# Beam parameter
xrms = gen.get_rms_x()
yrms = gen.get_rms_y()

fig, ax = plt.subplots()
ax.plot(xrms[:,0], xrms[:,1]*1e3, '-')
ax.plot(yrms[:,0], yrms[:,1]*1e3, '-')
ax.grid()

#%%
workdir = os.path.join(simdir, r'2019', r'EMSY4000pC2und3')
#os.chdir(workdir)

legends = ['009', '007', '008']
files = ['slice@1.31um_0.20um.ini', 'slice@1.31um_0.39um.ini', 'slice@1.31um_0.40um.ini']
fig, ax = plt.subplots(nrows = 3, ncols = 3, figsize = (12, 12))
for f in files[0:3]:
    d0 = np.loadtxt(f, skiprows = 2)
    ax[0,0].plot(d0[:,0]*1e3, d0[:,1], '.')
    ax[0,1].plot(d0[:,0]*1e3, d0[:,2], '.'); 
    ax[0,2].plot(d0[:,0]*1e3, d0[:,13], '.')
    
    ax[1,0].plot(d0[:,0]*1e3, d0[:,3]*1e6, '.')
    ax[1,1].plot(d0[:,0]*1e3, d0[:,5], '.')
    ax[1,2].plot(d0[:,0]*1e3, d0[:,11], '.')
    
    ax[2,0].plot(d0[:,0]*1e3, d0[:,4]*1e6, '.')
    ax[2,1].plot(d0[:,0]*1e3, d0[:,6], '.')
    ax[2,2].plot(d0[:,0]*1e3, d0[:,11], '.')

ax[0,0].set_ylabel(r'$\gamma$')
ax[0,1].set_ylabel(r'$\Delta\gamma$'); ax[0,1].set_ylim(0, 0.02)
ax[0,2].set_ylabel(r'$I_{\rm peak}$ (A)')

ax[1,0].set_ylabel(u_emi_x);          ax[1,0].set_ylim(0, 5)
ax[1,1].set_ylabel(r'$\beta_x$ (m)'); ax[1,1].set_ylim(0, 60)
ax[1,2].set_ylabel(r'$\alpha_x$');    ax[1,2].set_ylim(0, 20)
ax[2,0].set_ylabel(u_emi_y);          ax[2,0].set_ylim(0, 5)
ax[2,1].set_ylabel(r'$\beta_y$ (m)'); ax[2,1].set_ylim(0, 2)
ax[2,2].set_ylabel(r'$\alpha_y$');    ax[2,2].set_ylim(0, 20)

for i in [0, 1, 2]:
    for j in [0, 1, 2]:
        ax[i, j].set_xlabel(r'$\xi$ (m)')
        ax[i, j].legend(legends)

plt.subplots_adjust(top=0.9, bottom=0.1, left=0.08, right=0.92, hspace=0.25, wspace=0.35)
fig.savefig('slice-parameters.eps')
#%% Batch analyze an `ipseed` parameter scan
workdir = os.path.join(simdir, r'2019\THz-60um-3\_\SASE-014-2')
workdir = os.path.join(simdir, r'2020\SASE-scan2\beam_1.00mm_0.25mm_3THz')
workdir = os.path.join(simdir, r'2020\SASE-scan2')
#workdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim\2019\LCLS-I-matching-60um2\_n200k-sig_x-1.50mm-sig_y-0.30mm-alp_x-15.25-alp_y-9.25\SASE-001'
#workdir = os.path.join(simdir, r'2019\LCLS-I-matching-60um3\_n200k-sig_x-1.00mm-sig_y-0.30mm-alp_x-5.00-alp_y-3.00\SASE-002')
#workdir = os.path.join(simdir, r'2019\LCLS-I-matching-60um\_n200k-sig_x-1.50mm-sig_y-0.30mm-alp_x-3.75-alp_y-2.50\SASE-001')
#workdir = os.path.join(simdir, r'2019\LCLS-I-matching-60um2\_n200k-sig_x-1.50mm-sig_y-0.30mm-alp_x-14.00-alp_y-9.00\SASE-002')

workdir = os.path.join(simdir, r'2019', 'LCLS-I-matching-60um4',
                       '_n200k-sig_x-1.05mm-sig_y-0.30mm-alp_x-6.40-alp_y-3.00',
                       'SASE-002')
#workdir = get_path()
#os.chdir(workdir)

ext = '.txt'

for xxp in [1.4]: # um
    #xxp = 1.3
    for yyp in [0, 0.2, 0.39, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4]: # um
        #xxp, yyp = 1.3, 0.6
        
        direc = '%.2fum_%.2fum' % (xxp, yyp); print(direc,)
        os.chdir(direc)
        
        ext = '.txt'
        # ext = '-%dA-%.1fps.txt' % (Ipeak, FWHM)
        remove_files(['power-z-ipseed'+ext, 'energy-z-ipseed'+ext,\
                      'spectrum-lamds-ipseed'+ext, 'power-t-ipseed'+ext, 'peak-power-z-ipseed'+ext])
        for i in np.arange(1, 51):
            try:
                ipseed = i+0; print(ipseed, end = ' ')
                fname = 'pithz.%d.out' % ipseed
                # fname = 'pithz.%dA.%.1fps.%03d.out' % (Ipeak, FWHM, ipseed)
                
                gen = Genesis()
                gen.readdata(fname)

                power = gen.get_power()
                energy = gen.get_energy()

                ppower = gen.get_peak_power()
                
                tspec = gen.get_power_at()
                fspec = gen.get_spectrum_at()

                if i == 1:
                    with open('./power-z-ipseed'+ext, 'a') as f_handle:
                        np.savetxt(f_handle,np.atleast_2d(power[:,0]),fmt='%15.6E')
                    with open('./energy-z-ipseed'+ext, 'a') as f_handle:
                        np.savetxt(f_handle,np.atleast_2d(energy[:,0]),fmt='%15.6E')
                    with open('./spectrum-lamds-ipseed'+ext, 'a') as f_handle:
                        np.savetxt(f_handle,np.atleast_2d(fspec[:,0]),fmt='%15.6E')
                    with open('./power-t-ipseed'+ext, 'a') as f_handle:
                        np.savetxt(f_handle,np.atleast_2d(tspec[:,0]),fmt='%15.6E')
                    with open('./peak-power-z-ipseed'+ext, 'a') as f_handle:
                        np.savetxt(f_handle,np.atleast_2d(ppower[:,0]),fmt='%15.6E')

                with open('./power-z-ipseed'+ext, 'a') as f_handle:
                    np.savetxt(f_handle,np.atleast_2d(power[:,1]),fmt='%15.6E')
                with open('./energy-z-ipseed'+ext, 'a') as f_handle:
                    np.savetxt(f_handle,np.atleast_2d(energy[:,1]),fmt='%15.6E')
                with open('./spectrum-lamds-ipseed'+ext, 'a') as f_handle:
                    np.savetxt(f_handle,np.atleast_2d(fspec[:,1]),fmt='%15.6E')
                with open('./power-t-ipseed'+ext, 'a') as f_handle:
                    np.savetxt(f_handle,np.atleast_2d(tspec[:,1]),fmt='%15.6E')
                with open('./peak-power-z-ipseed'+ext, 'a') as f_handle:
                    np.savetxt(f_handle,np.atleast_2d(ppower[:,1]),fmt='%15.6E')
            except Exception as err:
                print(err)
                print('Error got at: ', i, )
                pass
        print('\n')
        os.chdir('..')
#%%
workdir = os.path.join(simdir, r'LCLS-I-3', 'currentScan9')
#os.chdir(workdir)

PE = []
for xxp in [0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.31, 1.4, 1.5 ]: # um
    #xxp = 1.3
    for yyp in [0, 0.2, 0.39, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4]: # um
        #xxp, yyp = 1.3, 0.6
        
        direc = '%.2fum_%.2fum' % (xxp, yyp); print(direc)
        os.chdir(direc)
        
        try:
            ext = '.txt'
            #ext = '-%dA-%.1fps.txt' % (Ipeak, FWHM)
            pp = np.loadtxt('power-z-ipseed'+ext); pp = pp.T; #print pp.shape
            EE = np.loadtxt('energy-z-ipseed'+ext); EE = EE.T; #print EE.shape

            end = -1
            PE.append([xxp, yyp, np.mean(pp[-1,1:end])/1e6, np.std(pp[-1,1:end])/1e6, \
                      np.mean(EE[-1,1:end])*1e6, np.std(EE[-1,1:end])*1e6]) # 'MW' and 'uJ'
        except Exception as err:
            print(err)
            pass
        os.chdir('..')
        
PE = np.array(PE)

fname = 'power-and-energy-vs-xxp-yyp.dat'
header = '%14s%16s%16s%16s%16s%16s' % ('xxp (um)', 'yyp (um)', '<Power> (MW)', 'rms_P (MW)','<Energy> (uJ)', 'rms_E (uJ)')
np.savetxt(fname, PE, fmt = '%15.6E', header = header)

#%%
workdir = os.path.join(simdir, r'LCLS-I-3')
#os.chdir(workdir)

fname = 'power-and-energy-vs-xxp-yyp.dat'

PE = np.loadtxt(fname)
xxp_list = [0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5 ]

fig, ax = plt.subplots()

for xxp in xxp_list:
    select = (PE[:,0] == xxp)
    ax.errorbar(PE[select,1], PE[select,2], yerr = PE[select,3], capsize = 4)

ax.grid()
ax.set_xlabel(r'$yy^{\prime}$ (um)')
ax.set_ylabel(r'$P$ (MW)')
#ax.legend(['cor_Ek = 0.5 %', 'cor_Ek = 1.0%'])
ax.legend(['%.2f um' % xxp for xxp in xxp_list], labelspacing = 0.3)
fig.savefig('power-vs-charge-xxp-yyp.eps')

fig, ax = plt.subplots(figsize = (6, 6))

for xxp in xxp_list:
    select = (PE[:,0] == xxp)
    ax.errorbar(PE[select,1], PE[select,4]/1e3, yerr = PE[select,5]/1e3, capsize = 4)

ax.grid()
ax.set_xlabel(r'$\langle yy^{\prime}\rangle$ (um)')
ax.set_ylabel(r'$E$ (mJ)')
ax.set_ylim(0, 1.8)
#ax.legend(['cor_Ek = 0.5 %', 'cor_Ek = 1.0%'])
ax.legend(['%.2f um' % xxp for xxp in xxp_list], labelspacing = 0.3, ncol = 3)
fig.savefig('energy-vs-charge-xxp-yyp.png')

#%% Make plot for batch analysis: energy and power
#workdir = os.path.join(simdir, r'2019\THz-60um-3\_\SASE-014')
workdir = os.path.join(simdir, r'2019', 'LCLS-I-matching-60um4',
               '_n200k-sig_x-1.05mm-sig_y-0.30mm-alp_x-6.40-alp_y-3.00',
               'SASE-001')
#workdir = get_path()
#os.chdir(workdir)

ext = ''
pp = np.loadtxt('power-z-ipseed'+ext+'.txt'); pp = pp.T; print (pp.shape)
EE = np.loadtxt('energy-z-ipseed'+ext+'.txt'); EE = EE.T; print (EE.shape)

end = pp.shape[1]

output = f'P =: {np.mean(pp[-1,1:end])/1e6:.2f} +/- {np.std(pp[-1,1:end])/1e6:.2f} MW\n'
output += f'E =: {np.mean(EE[-1,1:end])*1e6:.2f} +/- {np.std(EE[-1,1:end])*1e6:.2f} uJ\n'
print(output)

with open('stat.dat', 'w') as f_handle:
    f_handle.write(output)

fig, ax1 = plt.subplots(figsize = (4, 4))
for i in np.arange(1, end):
    ax1.plot(pp[:,0], pp[:,i]/1e6, '-', color='grey') # energy: MW
ax1.plot(pp[:,0], np.mean(pp[:,1:], 1)/1e6, 'k-')

ax1.set_xlabel(r'$z$ (m)')
ax1.set_ylabel(r'power (MW)')
ax1.set_yscale('log')
ax1.grid()
fig.savefig('power-vs-z-ipseed'+ext+'.png')

fig, ax2 = plt.subplots(figsize = (4, 4))
for i in np.arange(1, end):
    print(i, end = ' ')
    ax2.plot(EE[:,0], EE[:,i]*1e6, '-', color='grey') # energy: uJ
ax2.plot(EE[:,0], np.mean(EE[:,1:], 1)*1e6, 'k-')
#res.append(EE[:,0])
#res.append(np.mean(EE[:,1:], 1))

ax2.set_xlabel(r'$z$ (m)', fontsize = 12)
ax2.set_ylabel(r'$E$ ($\mu$J)', fontsize = 12)
ax2.set_yscale('log')
#ax2.set_ylim(0, 150)
#ax2.set_yticks([1e-7, 1e-6, 1e-6, 1e])
ax2.grid()

#locmin = mpl.ticker.LogLocator(base = 10.0, subs = (0.2, 0.4, 0.6, 0.8), numticks = 12)
#ax2.yaxis.set_minor_locator(locmin)
#ax2.yaxis.set_minor_formatter(mpl.ticker.NullFormatter())

#ax2.text(0.925, 0.95, '($b$)', horizontalalignment='center', verticalalignment='center', transform=ax2.transAxes)

fig.savefig('energy-vs-z-ipseed'+ext+'.png', density = 300)

#%%

np.savetxt('avg-energy-vs-z-Ipeak.dat', res, fmt = '%16.6E')
fig, ax = plt.subplots()

ax.plot(res[:,0], res[:,1]*1e6, '-')
ax.plot(res[:,0], res[:,2]*1e6, '-')
ax.plot(res[:,0], res[:,3]*1e6, '-')
ax.plot(res[:,0], res[:,4]*1e6, '-')

ax.set_xlabel(r'$z$ (m)')
ax.set_ylabel(r'$E$ ($\mu$J)', labelpad = -5, fontsize = 12)
ax.set_yscale('log')
ax.grid()

ax.legend([r'$I_{\rm peak}$ = %.0f A' % I for I in [100, 150, 200, 250]])
fig.savefig('avg-energy-vs-z-Ipeak.eps')

#%% Make plots for batch analysis: spectrum
ext = '.txt'
ss = np.loadtxt('spectrum-lamds-ipseed'+ext); ss = ss.T; print (ss.shape)

end = pp.shape[1]

xlamds = []; width = []
fig, ax = plt.subplots(figsize = (4, 4))
for i in np.arange(9, end):
    ax.plot(ss[:,0]*1e6, ss[:,i]/1e8, '-', color='grey') # energy: MW
    xlamds.append([weighted_mean(ss[:,0], ss[:,i])*1e6])
    width.append([weighted_std(ss[:,0], ss[:,i])*1e6])
ax.plot(ss[:,0]*1e6, np.mean(ss[:,1:], 1)/1e8, 'k-')

output =  f'Centre wavelength: {np.mean(xlamds):.2f} +/- {np.std(xlamds):.2f} um\n'
output += f'Spectrum width: {np.mean(width):.2f} +/- {np.std(width):.2f} um\n'

ax.set_xlabel(r'$\lambda_s$ ($\mu$m)', fontsize = 12)
ax.set_ylabel(r'intensity (arb. unit)', fontsize = 12)
#ax.set_yscale('log')
ax.set_xlim(80, 120)
#ax.set_ylim(-0.05, 1.1)
ax.grid()

#ax.text(0.925, 0.95, '($b$)', horizontalalignment='center', verticalalignment='center', transform=ax.transAxes)
fig.savefig('spectrum-vs-lamds-ipseed.png')

ss = np.loadtxt('power-t-ipseed'+ext); ss = ss.T; print (ss.shape)

jitter = []
fig, ax = plt.subplots()
for i in np.arange(9, end):
    ax.plot(ss[:,0]/g_c*1e12-20, ss[:,i]/1e8, '-', color='grey') # energy: MW
    jitter.append([weighted_mean(ss[:,0], ss[:,i])/g_c*1e12-20])
ax.plot(ss[:,0]/g_c*1e12-20, np.mean(ss[:,1:], 1)/1e8, 'k-')

output += f'Arrival time jitter: {np.mean(jitter):.2f} +/- {np.std(jitter):.2f} ps\n'
print(output)

with open('stat.dat', 'a') as f_handle:
    f_handle.write(output)

ax.set_xlabel(r'$t$ (ps)')
ax.set_ylabel(r'intensity (arb. unit)')
#ax.set_yscale('log')
ax.set_xlim(-20, 20)
#ax.set_ylim(-0.005, 0.6)
ax.grid()
fig.savefig('power-vs-t-ipseed.png')

#%%
from astra_modules import *

path = get_path()
os.chdir(path)

fname = get_file()
dist = np.loadtxt(fname)
dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]

x, y, w = dist[:,2], dist[:,5], dist[:,7]
aX, aY, signal = plot2d(x, y, w = w, returned = True)

fname = get_file()
dist = np.loadtxt(fname)
dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]

x, y, w = dist[:,2], dist[:,5], dist[:,7]
aX2, aY2, signal2 = plot2d(x, y, w = w, returned = True)

#%%
fig, ax = plt.subplots()

v = np.linspace(0.001, 0.999)*np.max(signal)
ax.contourf(aX*1e3, aY/1e6, signal, v)

v = np.linspace(0.001, 0.999)*np.max(signal2)
ax.contourf(aX2*1e3, aY2/1e6, signal2, v)

ax.set_xlabel(r'$z-z_0$ (mm)')
ax.set_ylabel(r'$P_z$ (MeV/c)')

fig.savefig('phase-space-with-chirps.eps')
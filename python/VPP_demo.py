# -*- coding: utf-8 -*-
"""
Created on Wed Feb 26 17:59:58 2020

@author: lixiangk
"""

from universal import *
from image_process.PhaseSpace import *

VC2DIR = r'\\afs\ifh.de\group\pitz\doocs\measure\Laser\TransverseProfile\VC2\2020'
EMSYDIR = r'\\afs\ifh.de\group\pitz\doocs\measure\TransvPhSp\2020\ProjEmittance'

path = get_path()
os.chdir(path)

# step = 0.5e-4
# drift = 3.133 
# momentum = 17.882

psx = PhaseSpace()
psy = PhaseSpace(ROI = psx.ROI)

#%%
eps1 = np.array([[4.6, 6.1], [5.3, 5.9], [4.9, 5.9]])
eps2 = np.array([[6.0, 7.2], [6.1, 7.0], [6.1, 7.0]])

eps1_avg = np.mean(np.sqrt(eps1[:,0]*eps1[:,1]))
eps2_avg = np.mean(np.sqrt(eps2[:,0]*eps2[:,1]))
#%%
from image_process.PhaseSpace import *
vpp = VPP(psx, psy, save_data = False)

#%%

def twiss2rms(twiss, gamma):
    emit_x = twiss[0]/gamma
    
    rms_x   = np.sqrt(twiss[1]*emit_x)
    rms_xp  = np.sqrt(twiss[2]*emit_x)
    cov_xxp = -twiss[3]*emit_x
    
    rms = [rms_x, rms_xp, cov_xxp]
    return rms

def cross(ij, psx = psx, psy = psy):
    i, j = ij
    x0 = i*step
    y0 = -j*step
    xi = psx.imgs[i]
    yj = psy.imgs[j]
    print(i)
    if np.sum(xi)>0:
        if np.sum(yj)>0:
            xy = np.array(list(map(min, xi.ravel(), yj.ravel()*yfactor))).reshape(xi.shape)
            if np.sum(xy)>0:
                xc, yc, xrms, yrms = calcRMS(xy[::-1, ::-1], Xscale, Yscale)
                xp0 = (xc-x0)/drift
                yp0 = (yc-y0)/drift
                
                return xy
    return None

xy = cross((20, 20))

#%%
fullname = get_file('.pkl')
with open(fullname, 'rb') as f_handle:
    pkl = pickle.load(f_handle)
res = pkl['vpp']
bm = pkl['bm']
psx = pkl['psx']
psy = pkl['psy']
#%%
res = vpp.vpp*1e3
for i in range(4):
    res[:,i] -= weighted_mean(res[:,i], res[:,7])

#%%

fig, ax = plt.subplots(ncols = 2, nrows = 3, figsize = (8, 12))

u_x  = r'$x$ (mm)'
u_y  = r'$y$ (mm)'
u_xp = r'$x^{\prime}$ (mrad)'
u_yp = r'$y^{\prime}$ (mrad)'

data = res[res[:,-1]>0]
w= data[:,7]

ax[0][0].scatter(data[:,0], data[:,1], s = 5, c = w)
ax[0][0].set_title(r'$\langle xx^{\prime}\rangle$')
ax[0][0].set_xlabel(u_x);ax[0][0].set_ylabel(u_xp)

#plt.ylim(0.5, 2.5)

ax[0][1].scatter(data[:,2], data[:,3], s = 5, c = w);
ax[0][1].set_title(r'$\langle yy^{\prime}\rangle$')
ax[0][1].set_xlabel(u_y);ax[0][1].set_ylabel(u_yp)

ax[1][0].scatter(data[:,0], data[:,2], s = 2, c = w)
ax[1][0].set_title(r'$\langle xy\rangle$')
ax[1][0].set_xlabel(u_x);ax[1][0].set_ylabel(u_y)

ax[1][1].scatter(data[:,-2]/1e3, data[:,-1]/1e3, s = 2, c = w)
ax[1][1].set_title(r'$\langle xy\rangle$')
ax[1][1].set_xlabel(u_x);ax[1][0].set_ylabel(u_y)

#plt.axis([-5, 5, -5, 5])

#ax[1][1].scatter(data[:,0], data[:,3], s = 5, c = w)
#ax[1][1].set_title(r'$\langle xy^{\prime}\rangle$')
#ax[1][1].set_xlabel(u_x);ax[1][1].set_ylabel(u_yp)

ax[2][0].scatter(data[:,2], data[:,1], s = 5, c = w)
ax[2][0].set_title(r'$\langle yx^{\prime}\rangle$')
ax[2][0].set_xlabel(u_y);ax[2][0].set_ylabel(u_xp)

ax[2][1].scatter(data[:,1], data[:,3], s = 5, c = w)
ax[2][1].set_title(r'$\langle x^{\prime}y^{\prime}\rangle$')
ax[2][1].set_xlabel(u_xp);ax[2][1].set_ylabel(u_yp)

plt.tight_layout(pad=3)

fig.savefig('vpp.eps')

#%%
nrows = len(psx.imgs[1:-1:2])
ncols = len(psy.imgs[1:-1:2])

xx = vpp.vpp[:,0].reshape(nrows, ncols)
yy = vpp.vpp[:,1].reshape(nrows, ncols)
ss = vpp.vpp[:,-3].reshape(nrows, ncols)

xmin, xmax = np.min(xx)*2e3, np.max(xx)*2e3
ymin, ymax = np.min(yy)*2e3, np.max(yy)*2e3
xmin = -(xmax-xmin)/2; xmax = -xmin
ymin = -(ymax-ymin)/2; ymax = -ymin

fig, ax = plt.subplots()
tmp = np.copy(ss)
tmp[tmp==0] = np.nan
ax.imshow(tmp.T, extent = [xmin, xmax, ymin, ymax])
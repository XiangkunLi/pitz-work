# -*- coding: utf-8 -*-
"""
Created on Wed Feb 10 15:38:50 2021

@author: lixiangk
"""

from universal import *

#%% Steering demonstration
reset_margin(left = 0.05, right = 0.95, bottom = 0.05, top = 0.95)

fig, ax = plt.subplots()

rho = 1
alpha = 30

x = [0, 0]
y = [0, rho]
ax.plot(x, y, 'k-', lw = 1)

x = [0, rho*1.1]
y = [rho, rho]
ax.plot(x, y, 'k-', lw = 1)

# arc
theta = np.linspace(90-alpha, 90, 100)*np.pi/180
x = rho*np.cos(theta)
y = rho*np.sin(theta)
x1, y1 = x[0], y[0]
ax.plot(x, y, 'k-', lw = 1)

# denoting the angle of the arc
theta = np.linspace(90-alpha, 90, 100)*np.pi/180
x = 0.1*rho*np.cos(theta)
y = 0.1*rho*np.sin(theta)
ax.plot(x, y, 'k-', lw = 1)

x = [x1, x1]
y = [rho, y1]
ax.plot(x, y, 'k--', lw = 1)

# interception of the outgoing trajectory after deflection and the original trajectory
xi, yi = rho*np.tan(alpha*np.pi/180/2), rho
x = [xi, x1]
y = [yi, y1]
ax.plot(x, y, 'k--', lw = 1)

# denoting the deflecting angle
theta = np.linspace(-alpha, 0, 100)*np.pi/180
x = xi+0.1*rho*np.cos(theta)
y = yi+0.1*rho*np.sin(theta)
ax.plot(x, y, 'k-', lw = 1)


x = [0, x1]
y = [0, y1]
ax.plot(x, y, 'k--', lw = 1)

x = [x1, x1+0.5*rho*np.cos(alpha*np.pi/180)]
y = [y1, y1-0.5*rho*np.sin(alpha*np.pi/180)]
x2, y2 = x[-1], y[-1]
ax.plot(x, y, 'k-', lw = 1)

x = [x2, x2]
y = [y2, rho]
ax.plot(x, y, 'k--', lw = 1)


ax.axis('off')
#ax.set_xticks([])
#ax.set_yticks([])

ax.set_aspect(1)

fig.savefig('hard-edge-steering.png')
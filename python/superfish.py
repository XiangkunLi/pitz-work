# -*- coding: utf-8 -*-
"""
Created on Wed Jan 13 17:58:04 2021

@author: lixiangk
"""

from universal import *
from interface.Astra import *

TITLE = 'Superfish input file, generated automatically'
FREQ = 2856 # MHz
XDRI, YDRI = 0.8, 3.0

class NL(Namelist):
    starter = '&'
    ending = '&'

    def update(self, quoting = True):
        '''
        Update output to write into a file
        Returns
        -------
        None.

        '''
        output = self.starter+self.name.upper()+' \n'
        for k, v in self.kv.items():
            #k = k.lower()
            #print(k)
            if isinstance(v, (list, tuple, np.ndarray)):
                dim = len(np.asarray(v).shape)
                if dim == 1:
                    for i, vi in enumerate(v):
                        if isinstance(vi, str):
                            if quoting:
                                vi = '\''+vi+'\''
                        output += ' {}({})={}\n'.format(k, i+1, vi)
                elif dim == 2:
                    for i, vi in enumerate(v):
                        if k.upper() in ['D_GAP', 'MODULE', 'AP_GR', 'Q_MULT_A', 'Q_MULT_B']:
                            for j, vij in enumerate(vi):
                                if isinstance(vij, str):
                                    if quoting:
                                        vij = '\''+vij+'\''
                                output += ' {}({},{})={}\n'.format(k, j+1, i+1, vij)
                        elif k.upper() in ['D1', 'D2', 'D3', 'D4']:
                            output += ' {}({})=({},{})\n'.format(k, i+1, vi[0], vi[1])
                        else:
                            print('Unknown key!')
            else:
                if isinstance(v, str):
                    if quoting:
                        v = '\''+v+'\''
                output += ' {}={:f}\n'.format(k, v)
        
        output += self.ending+'\n\n'
        self.output = output
def add_arc(**kw):
    return NL('PO', nt = 2, **kw)
def add_line(**kw):
    return NL('PO', **kw)

def write_namelists(fname, namelists, mode = 'w', title = 'Superfish input'):
    with open(fname, mode) as f_handle:
        if title is not None:
            f_handle.write(title+'\n\n')
        for nl in namelists:
            f_handle.write(nl.output)
    return
#%% Example
reg = NL('REG', kprob = 1, mat = 1, freq = FREQ, freqd = FREQ, 
         beta = 1, kmethod = 1, dphi = 90,
         nbsup = 1, nbslo = 0, nbsrt = 0, nbslf = 1,
         lines = 1, icylin = 1, norm = 0, ezerot = 3e7,
         rmass = -2, epso = 1e-6, irtype = 0,
         xdri = XDRI, ydri = YDRI, dslope = -1, 
         dx = 0.02, dy = 0.02)

po =  []
po += [NL('PO', x = 0, y = 0)]
po += [NL('PO', x = 0, y = 4.417615088773)]
po += [NL('PO', x = 0.2, y = 4.417615088773)]
po += [NL('PO', nt = 2, x0 = 0.2, y0 = 2.917615088773,
         a = 1.5, aovrb = 1, x = 1.5, y = 0)]
po += [NL('PO', x = 1.7, y = 1.925)]
po += [NL('PO', nt = 2, x0 = 2.625, y0 = 1.925,
         a = 0.925, aovrb = 1, x = 0, y = -0.925)]
po += [NL('PO', x = 2.625, y = 0)]
po += [NL('PO', x = 0, y = 0)]

sf = [reg] + po
write_namelists('test9.af', sf)

#%% half cell
FREQ = 1300
reg = NL('REG', kprob = 1, mat = 1, freq = FREQ, freqd = FREQ, 
         beta = 1, kmethod = 1, dphi = 90,
         nbsup = 1, nbslo = 0, nbsrt = 0, nbslf = 1,
         lines = 1, icylin = 1, norm = 0, ezerot = 3e7,
         rmass = -2, epso = 1e-6, irtype = 0,
         xdri = XDRI, ydri = YDRI, dslope = -1, 
         dx = 0.02, dy = 0.02)

ratio = 0.1*1300/2856
ratio = 0.1

diameter1 = 95.385*2*ratio
length1 = 65*ratio
iris1 = 25*ratio

a1 = [25.5*ratio, 1]
a2 = [25.5*ratio, 1]
a3 = [10.0*ratio, 10.0/19.0]

#diameter1, length1, iris1 = 8.68, 2.96, 1.14
#a1, a2, a3 = [1.16, 1], [1.16, 1], [0.46, 0.53]

equator1 = length1-a1[0]-a2[0]-a3[0]
wall1 = diameter1/2.0-a2[0]/a2[1]-a3[0]/a3[1]-iris1


po = []
po += [add_line(x = 0, y = 0)]

a, aob = a1[:]
x0, y0 = a, diameter1/2.0-a/aob

po += [add_line(x = 0, y = y0)]
po += [add_arc(x0 = x0, y0 = y0, a = a, aovrb = aob, x = 0, y = a/aob)]

x, y = x0+equator1, diameter1/2.0
po += [add_line(x = x, y = y)]

a, aob = a2[:]
x0, y0 = x, y-a/aob
po += [add_arc(x0 = x0, y0 = y0, a = a, aovrb = aob, x = a, y = 0)]

x, y = x0+a, y0-wall1
po += [add_line(x = x, y = y)]

a, aob = a3[:]
x0, y0 = x+a, y
po += [add_arc(x0 = x0, y0 = y0, a = a, aovrb = aob, x = 0, y = -a/aob)]

x, y = x0, 0
po += [add_line(x = x, y = y)]

po += [add_line(x = 0, y = 0)]

sf = [reg] + po
write_namelists('test10.af', sf)

#%% full cell
reg = NL('REG', kprob = 1, mat = 1, freq = FREQ, freqd = FREQ, 
         beta = 1, kmethod = 1, dphi = 90,
         nbsup = 1, nbslo = 0, nbsrt = 0, nbslf = 0,
         lines = 1, icylin = 1, norm = 0, ezerot = 3e7,
         rmass = -2, epso = 1e-6, irtype = 0,
         xdri = XDRI, ydri = YDRI, dslope = -1, 
         dx = 0.02, dy = 0.02)

ratio = 0.1*1300/2856
ratio = 0.1

iris0 = 25*ratio
diameter1 = 95.385*2*ratio
length1 = 120.6*ratio
iris1 = 30*ratio

a0 = [10.1*ratio, 10.0/19.0]
a1 = [25.5*ratio, 1]
a2 = [39.885*ratio, 1]
a3 = [10.5*ratio, 10.5/19.0]

#iris0, diameter1, length1, iris1 = 1.14, 8.68, 5.48, 1.36
#a0, a1, a2, a3 = [0.46, 0.53], [1.16, 1], [1.82, 1], [0.48, 0.55]

equator1 = length1-a0[0]-a1[0]-a2[0]-a3[0]
wall1 = diameter1/2.0-a1[0]/a1[1]-a0[0]/a0[1]-iris0
wall2 = diameter1/2.0-a2[0]/a2[1]-a3[0]/a3[1]-iris1


po = []
po += [add_line(x = 0, y = 0)]

x, y = 0, iris0
po += [add_line(x = x, y = y)]

a, aob = a0[:]
x0, y0 = 0, y+a/aob

po += [add_arc(x0 = x0, y0 = y0, a = a, aovrb = aob, x = a, y = 0)]

x, y = x0+a, y0+wall1
po += [add_line(x = x, y = y)]

a, aob = a1[:]
x0, y0 = x+a, diameter1/2.0-a/aob

po += [add_arc(x0 = x0, y0 = y0, a = a, aovrb = aob, x = 0, y = a/aob)]

x, y = x0+equator1, diameter1/2.0
po += [add_line(x = x, y = y)]

a, aob = a2[:]
x0, y0 = x, y-a/aob
po += [add_arc(x0 = x0, y0 = y0, a = a, aovrb = aob, x = a, y = 0)]

x, y = x0+a, y0-wall2
po += [add_line(x = x, y = y)]

a, aob = a3[:]
x0, y0 = x+a, y
po += [add_arc(x0 = x0, y0 = y0, a = a, aovrb = aob, x = 0, y = -a/aob)]

x, y = x0+5, iris1
po += [add_line(x = x, y = y)]

x, y = x, 0
po += [add_line(x = x, y = y)]

po += [add_line(x = 0, y = 0)]

sf = [reg] + po
write_namelists('test11.af', sf)
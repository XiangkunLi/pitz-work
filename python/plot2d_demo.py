from plot2d import *

path = r'\\afs\ifh.de\group\pitz\data\shuguan-temp\2019.09.25\symmetry'
fname = r'face1_Bzfactor=1_862macro.txt'
#fname = r'face3_Bzfactor=1_678macro.txt'
skiprows = 7
col_x = 0
col_y = 1
col_w = 8
vmin = 0.001
vmax = 1
bins = [100, 100]
extent = [-30, 30, -30, 30]
fig_ext = '@'+fname+'_0.001.eps'

os.chdir(path)

import timeit

time1 = timeit.default_timer()

#beam = np.loadtxt(fname, skiprows = skiprows, usecols = (col_x, col_y, col_w))

#import pdb; pdb.set_trace()
import pandas as pd
df = pd.read_csv(fname, header = None, sep = '\s+', skiprows = skiprows, usecols = [col_x, col_y, col_w])
beam = df.values

time2 = timeit.default_timer()
print('Reading data into memory costs: ', time2-time1, ' s')

x, y, w = beam[:,0]*1e3, beam[:,1]*1e3, -beam[:,2]*1e12

'''
def beam2d(x, y, weights = None, xlabel = r'$x$ (mm)', ylabel = r'$y$ (mm)', figname = 'xy2d',\
           fig_ext = '.eps', vmin = 0.01, vmax = 1, bins = None, extent = None, **kwargs):
'''
time1 = timeit.default_timer()
beam2d(x = x, y = y, weights = w, vmin = vmin, vmax = vmax, bins = bins, extent = extent, fig_ext = fig_ext)
time2 = timeit.default_timer()
print('Making plot costs: ', time2-time1, ' s')

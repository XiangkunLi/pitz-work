# -*- coding: utf-8 -*-
"""
Created on Thu Jul 30 15:55:24 2020

@author: lixiangk
"""

from astra_modules import *

BSA = 2.5 # mm
sig_x = BSA/4
sig_y = BSA/4
C_sig_x = BSA/sig_x
C_sig_y = BSA/sig_y

LT = 10 # ps
RT = 0.001  # ps

Ipart = 500000
Q_total = 500e-3 # nC

genName = 'gen.in'
generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons',
                       Q_total = Q_total,
                       Ref_Ekin = 0.0e-6, LE = 0.55e-3*1, dist_pz = 'i',
                       Dist_z = 'g', sig_clock = 6e-3/2.355, Cathode = True,
                       Dist_x = '2', sig_x = sig_x, Dist_px = 'g', Nemit_x = 0,
                       Dist_y = '2', sig_y = sig_y, Dist_py = 'g', Nemit_y = 0,
                       C_sig_x = C_sig_x, C_sig_y = C_sig_y)
generator.set(Dist_x = 'r', sig_x = sig_x, Dist_y = 'r', sig_y = sig_y)
generator.set(Dist_z = 'p', Lt = LT*1e-3, Rt = RT*1e-3)


generator.write(genName)
os.system('generator '+genName)

#%%
# Define the modulated profile
sig_tg = 2.0 # global rms, ps
sig_tl = 0.2 # local rms
tl = np.arange(-LT//2+1, LT//2)  # centers of local profiles

tt = np.linspace(-LT/2, LT/2, 1000)
pdf = np.sum(np.array([np.exp(-(tt-ti)**2/2./sig_tl**2)*np.exp(-ti**2/2.0/sig_tg**2) for ti in tl]), axis = 0)

cdf = np.copy(pdf)
for i in np.arange(1, len(cdf)):
    cdf[i] += cdf[i-1]
cdf /= cdf[-1]

ft_CDF = interp1d(cdf, tt, bounds_error = False, fill_value = 0)
#%%
import numpy as np
from scipy import fft
from scipy.interpolate import interp1d

from matplotlib import pyplot as plt


def envelope(t, t0, s):
    arg = - (t-t0)**2 / (s ** 2)
    i = np.exp(arg)
    return i

def modulation(t, f, p):
    arg = 2 * np.pi * (t * f) + p
    i = (1 + np.sin(arg))
    return i

def intensity(t):
    factor = 26.109
    i = envelope(t, 0.5, 0.5) * modulation(t * factor, 0.4, 0.0)
    i = i ** 4
    i = i / sum(i)
    return i

LT = 1 # ps, found from generator input

T = np.linspace(0, LT, 2000)
pdf = intensity(T)

cdf = np.copy(pdf)
for i in np.arange(1, len(cdf)):
    cdf[i] += cdf[i-1]
cdf /= cdf[-1] # normalize

ft_CDF = interp1d(cdf, T-LT/2.0, bounds_error = False, fill_value = 0)

#%%
data = np.loadtxt('beam.ini')
time0 = data[:,6]*1e3 # ps

time1 = ft_CDF((time0+LT/2.0)/LT)

data[:,6] = time1*1e-3
np.savetxt('beam_modulated.ini', data,
           fmt = '%20.12E%20.12E%20.12E%20.12E%20.12E%20.12E%20.12E%20.12E%4d%4d')

fig, [ax0, ax1, ax2] = plt.subplots(ncols = 3, figsize = (12, 4))
ax0.plot(T, intensity(T), '-')
ax0.set_title("MBI Lyot function")

ax1.plot(T, cdf, '-')
ax1.set_title("CDF")

_ = ax2.hist(time0, bins = 500, histtype = r'step')
_ = ax2.hist(time1, bins = 500, histtype = r'step')
ax2.set_title('Modulated beam profile')
ax2.legend(['Flattop', 'Modulated'])

plt.tight_layout()
fig.savefig('Modulated.png')


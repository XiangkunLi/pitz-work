#get_ipython().magic(u'matplotlib inline')
#from universal import *
import numpy as np
import scipy as sp
import matplotlib as mpl
import matplotlib.pyplot as plt
import os

import pandas as pd
# ## Astra modules

def plot_config():
    from cycler import cycler
    from matplotlib.ticker import AutoMinorLocator

    fsize = 14 # a quarter of the paper width: 20 pt; half of the paper width: 12
    font = {'size' : fsize, 'family' : 'serif'}
    color_cycle = ['r', 'b', 'g', 'c', 'm', 'y', 'k']
    linestyle_cycle = ['-', '--', '-.', ':', (0, (5, 2, 5, 2)), (0, (10, 2, 5, 2, 2, 2)), (0, (12, 2, 2, 2))]
    marker_cycle = ['o', 'd', 'v', '^', '<', '>', '*']
    
    mpl.rc('font', **font)
    mpl.rc('xtick', labelsize = 10, direction = 'in', top   = True)
    mpl.rc('ytick', labelsize = 10, direction = 'in', right = True)
    mpl.rc('xtick.major', size = 5, width = 1)
    mpl.rc('ytick.major', size = 5, width = 1)
    mpl.rc('xtick.minor', size = 3, width = 0.7, visible = True)
    mpl.rc('ytick.minor', size = 3, width = 0.7, visible = True)
    
    mpl.rc('lines', linewidth=2, markersize=6, color='r')
    # mpl.rc('lines', linestyle = 'solid')
    mpl.rc('axes', labelpad = 0, prop_cycle=(cycler('color', color_cycle) + cycler('linestyle', linestyle_cycle) + cycler('marker', marker_cycle)))
    mpl.rc('legend', fontsize = 12, labelspacing = 0.05, handletextpad=0.4, frameon=False, handlelength=2.1)
    
    mpl.rc('figure', dpi = 100, figsize = (4, 4))
    mpl.rc('figure.subplot', bottom = 0.15, top = 0.9, left = 0.15, right = 0.9)
    
    mpl.rc('image', cmap = 'jet')
    
    return
plot_config()

def smooth_easy(y, box_pts):
    '''
    Smooth the data points using a moving box
    Parameters
      y: 1-D array to be smoothed
      box_pts: number of points in the moving box
    Returns
      y_smooth: smoothed 1-D array
    '''
    box = np.ones(box_pts)/box_pts
    y_smooth = np.convolve(y, box, mode='same')
    return y_smooth

def xdist(x, bins = 50, range = None, weights = None, fig_ext = '.eps', **fig_kw):
    '''
    Parameters
      x: horizontal coordinates in mm
    '''
    fig, ax = plt.subplots(**fig_kw)
    h = ax.hist(x, bins = bins, range = range, weights = weights, histtype = 'step')
    #ax.grid()
    ax.set_xlabel(r'$x$ (mm)')
    if range == None:
        ax.set_ylabel(r'd$Q/$d$x$ (arb. unit)')
    else:
        ax.set_ylabel(r'd$Q/$d$x$ (pC/mm)')
    #ax.set_xlim(-15, 15)
    #ax.set_ylim(0, 200)
    fig.savefig('dQ_dx-x'+fig_ext)
    return

def ydist(y, bins = 50, range = None, weights = None, fig_ext = '.eps', **fig_kw):
    '''
    Parameters
      x: vertical coordinates in mm
    '''
    fig, ax = plt.subplots(**fig_kw)
    h = ax.hist(x, bins = bins, range = range, weights = weights, histtype = 'step')
    #ax.grid()
    ax.set_xlabel(r'$y$ (mm)')
    if range == None:
        ax.set_ylabel(r'd$Q/$d$y$ (arb. unit)')
    else:
        ax.set_ylabel(r'd$Q/$d$y$ (pC/mm)')
    #ax.set_xlim(-15, 15)
    #ax.set_ylim(0, 200)
    fig.savefig('dQ_dy-y'+fig_ext)
    return

def zdist(z, bins = 50, range = None, weights = None, fig_ext = '.eps', **fig_kw):
    '''
    Parameters
      x: longitudinal coordinates in mm
    '''
    fig, ax = plt.subplots(**fig_kw)
    
    #h = ax.hist(z, bins = bins, range = range, weights = weights, histtype = 'step')
    
    counts, centers = hist_centered(z, bins = bins, range = range, weights = weights)
    dz = centers[1]-centers[0]
    currents = np.abs(counts*1e-9/dz*g_c); Ipeak0 = np.max(currents)
    ax.plot(centers*1e3, currents, '-')
    currents = smooth(currents, 11)
    ax.plot(centers*1e3, currents, '--'); Ipeak1 = np.max(currents)
    
    #ax.grid()
    ax.set_xlabel(r'$\xi$ (mm)')
    if range == None:
        ax.set_ylabel(r'$I$ (A)')
    else:
        ax.set_ylabel(r'$I$ (A)')
    #ax.set_xlim(-15, 15)
    #ax.set_ylim(0, 200)
    fig.savefig('dQ_dz-z'+fig_ext)
    return [Ipeak1, Ipeak0]

def pdist(z, bins = 50, range = None, weights = None, fig_ext = '.eps', **fig_kw):
    '''
    Parameters
      p: momentum in MeV/c
    '''
    fig, ax = plt.subplots(**fig_kw)
    h = ax.hist(z, bins = bins, range = range, weights = weights, histtype = 'step')
    #ax.grid()
    ax.set_xlabel(r'$P$ (MeV/c)')
    if range == None:
        ax.set_ylabel(r'd$Q/$d$P$ (arb. unit)')
    else:
        ax.set_ylabel(r'd$Q/$d$P$ (pC/mm)')
    #ax.set_xlim(-15, 15)
    #ax.set_ylim(0, 200)
    fig.savefig('dQ_dP-P'+fig_ext)
    return

def tdist(t, bins = 50, range = None, weights = None, fig_ext = '.eps', **fig_kw):
    '''
    Parameters
      t: temperal coordinates in ps
    '''
    fig, ax = plt.subplots(**fig_kw)
    h = ax.hist(t, bins = bins, range = range, weights = weights, histtype = 'step')
    #ax.grid()
    ax.set_xlabel(r'$t$ (ps)')
    if range == None:
        ax.set_ylabel(r'd$Q/$d$t$ (arb. unit)')
    else:
        ax.set_ylabel(r'd$Q/$d$t$ (pC/ps)')
    #ax.set_xlim(-15, 15)
    #ax.set_ylim(0, 200)
    fig.savefig('dQ_dt-t'+fig_ext)
    return

### begin: plot 2D phase space distributions
def hist_centered(x, weights = None, bins = None, range = None):
    '''
    Returns
      counts, centers: 1d arrays including each bin's count and center, respectively
    '''
    if bins == None:
        bins = 100

    counts, edges = np.histogram(x, bins = bins, range = range, weights = weights)
    centers = edges[1:]-0.5*(edges[1]-edges[0])
    return counts, centers
def hist2d_scattered(x, y, weights = None, bins = [100, 100], range = None):
    '''
    Returns
      xs, ys, cs: 1d arrays, (xs, ys) are the coordinates and cs the number of particles at (xs, ys)
    '''
    counts, xedges, yedges = np.histogram2d(x, y, bins = bins, range = range, weights = weights)
    xs = np.array([xedges[i]+0.5*(xedges[1]-xedges[0]) for i in np.arange(len(xedges)-1) for j in np.arange(len(yedges)-1)])
    ys = np.array([yedges[j]+0.5*(yedges[1]-yedges[0]) for i in np.arange(len(xedges)-1) for j in np.arange(len(yedges)-1)])
    cs = np.array([counts[i,j] for i in np.arange(len(xedges)-1) for j in np.arange(len(yedges)-1)])
    select = (cs>0)
    return xs[select], ys[select], cs[select]
def hist2d_contourf(x, y, weights = None, bins = [100, 100], range = None):
    '''
    Returns
      xs, ys, cs: 1d arrays, (xs, ys) are the coordinates and cs the number of particles at (xs, ys)
    '''
    counts, xedges, yedges = np.histogram2d(x, y, bins = bins, range = range, weights = weights)
    xs = np.array([xedges[i]+0.5*(xedges[1]-xedges[0]) for i in np.arange(len(xedges)-1)])
    ys = np.array([yedges[j]+0.5*(yedges[1]-yedges[0]) for j in np.arange(len(yedges)-1)])
    ax, ay = np.meshgrid(xs, ys, indexing = 'ij')
    return ax, ay, counts

def beam2d(x, y, weights = None, xlabel = r'$x$ (mm)', ylabel = r'$y$ (mm)', figname = 'xy2d',\
           fig_ext = '.eps', vmin = 0.01, vmax = 1, bins = None, extent = None, **kwargs):
    '''
    Parameters
      bins: None or [xbins, ybins], set the sampling frequencies for x and y
      extent: None or [xmin, xmax, ymin, ymax], set the lower limit and upper limit of the x-axis and y-axis
    '''
    if bins == None:
        xbins = ybins = 100
    else:
        xbins, ybins = bins
    if extent == None:
        xavg = np.mean(x); dx = np.max(x)-np.min(x)
        xmin, xmax = xavg-dx*0.75, xavg+dx*0.75
        yavg = np.mean(y); dy = np.max(y)-np.min(y)
        ymin, ymax = yavg-dy*0.75, yavg+dy*0.75
    else:
        xmin, xmax, ymin, ymax = extent
    
    fig = plt.figure(figsize=(4, 3.375))
    ax1 = fig.add_axes([0.18, 0.15, 0.675, 0.15]); ax1.patch.set_alpha(0)
    ax3 = fig.add_axes([0.18, 0.15, 0.15, 0.80]); ax3.patch.set_alpha(0)
    ax2 = fig.add_axes([0.18, 0.15, 0.75, 0.80]); ax2.patch.set_alpha(0)

    #import pdb; pdb.set_trace()
    cnts, cens = hist_centered(x, bins=xbins, range=(xmin,xmax), weights=weights)
    ax1.plot(cens, smooth_easy(cnts, 8), 'b-')
    
    # vmin is the lower limit of the colorbar, and is free to change
    # xs, ys, cs = hist2d_scattered(x, y, bins=[xbins, ybins], range=[[xmin, xmax],[ymin, ymax]], weights=weights)
    # cax = ax2.scatter(xs, ys, s=2, c=cs/np.max(cs), edgecolor='', vmin=vmin, norm=mpl.colors.LogNorm())
    
    ax, ay, signal = hist2d_contourf(x, y, bins=[xbins, ybins], range=[[xmin, xmax],[ymin, ymax]], weights=weights)
    signal = signal/np.max(signal)
    print(np.sum(weights), ' pC of ', len(weights[weights>0]), ' particles')
    print(np.max(signal[signal>0]), np.min(signal[signal>0]))
    v = np.linspace(vmin, vmax, 99)
    cax = ax2.contourf(ax, ay, signal, v)
    
    cbar = fig.colorbar(cax, fraction=0.09, pad=0.01)
    cbar.set_ticks(np.linspace(0, 1, 6))
    cbar.ax.tick_params(labelsize=11, pad=5)

    cnts, cens = hist_centered(y, bins=ybins, range=(ymin,ymax), weights=weights)
    ax3.plot(smooth_easy(cnts, 4), cens, 'b-')

    ax1.axis('off')
    ax1.set_xlim(xmin, xmax)

    ax2.set_xlim(xmin, xmax)
    ax2.set_ylim(ymin, ymax)
    ax2.set_xlabel(xlabel)
    ax2.set_ylabel(ylabel, labelpad=-1)
    ax2.minorticks_on()
    ax2.grid()

    ax3.axis('off')
    ax3.set_ylim(ymin, ymax)

    fig.savefig(figname+fig_ext)
    return
### end: plot 2D phase space distributions


def plot2d_xy(fname = 'ast.0500.001', fig_ext = '.eps', **kwargs):
    '''
    Parameters:
      **kwargs: bins = (xbins, ybins), extent = [xmin, xmax, ymin, ymax]
    Returns:
      A saved figure
    '''

    beam = np.loadtxt(fname)
    beam[1:,2] += beam[0, 2]
    beam[1:,5] += beam[0, 5]
    
    select = (beam[:,9] == 5)|(beam[:,9] == -1)|(beam[:,9] == -3)
    beam = beam[select]
    
    x, y, w = beam[:,0]*1e3, beam[:,1]*1e3, -beam[:,7]*1e3
    beam2d(x = x, y = y, weights = w, fig_ext = fig_ext, **kwargs)
   
    return

def plot2d_xpx(fname = 'ast.0500.001', fig_ext = '.eps', **kwargs):
    '''
    Parameters:
      **kwargs: bins = (xbins, ybins), extent = [xmin, xmax, ymin, ymax]
    Returns:
      A saved figure
    '''

    beam = np.loadtxt(fname)
    beam[1:,2] += beam[0, 2]
    beam[1:,5] += beam[0, 5]
    
    select = (beam[:,9] == 5)|(beam[:,9] == -1)|(beam[:,9] == -3)
    beam = beam[select]
    
    x, y, w = beam[:,0]*1e3, beam[:,3]/1e3, -beam[:,7]*1e3
    beam2d(x = x, y = y, weights = w, ylabel = r'$p_x$ (keV/$c$)', figname = 'xpx', fig_ext = fig_ext, **kwargs)
    
    return

def plot2d_ypy(fname = 'ast.0500.001', fig_ext = '.eps', **kwargs):
    '''
    Parameters:
      **kwargs: bins = (xbins, ybins), extent = [xmin, xmax, ymin, ymax]
    Returns:
      A saved figure
    '''

    beam = np.loadtxt(fname)
    beam[1:,2] += beam[0, 2]
    beam[1:,5] += beam[0, 5]
    
    select = (beam[:,9] == 5)|(beam[:,9] == -1)|(beam[:,9] == -3)
    beam = beam[select]
    
    x, y, w = beam[:,1]*1e3, beam[:,4]/1e3, -beam[:,7]*1e3
    beam2d(x = x, y = y, weights = w, xlabel = r'$y$ (mm)', ylabel = r'$p_y$ (keV/$c$)', figname = 'xpx', fig_ext = fig_ext, **kwargs)
    
    return

def plot2d_zpz(fname = 'ast.0500.001', fig_ext = '.eps', **kwargs):
    '''
    Parameters:
      **kwargs: bins = (xbins, ybins), extent = [xmin, xmax, ymin, ymax]
    Returns:
      A saved figure
    '''

    beam = np.loadtxt(fname)
    #beam[1:,2] += beam[0, 2]
    beam[0, 2] = 0
    beam[1:,5] += beam[0, 5]
    
    select = (beam[:,9] > 0)#|(beam[:,9] == -1)|(beam[:,9] == -3)
    beam = beam[select]
    
    x, y, w = beam[:,2]*1e3, beam[:,5]/1e6, -beam[:,7]*1e3
    beam2d(x = x, y = y, weights = w, xlabel = r'$\xi$ (mm)', ylabel = r'$p_z$ (MeV/$c$)', figname = 'zpz', fig_ext = fig_ext, **kwargs)
    
    return

def plot_zEk(prefix = 'ast', suffix = '001', fig_ext = '.eps', extent = None, **fig_kw):

    fig, ax = plt.subplots(**fig_kw)
    
    zemit = np.loadtxt(prefix+'.Zemit.'+suffix)
    
    ax.plot(zemit[:,0], zemit[:,6], 'r-', label = r'$\langle z\cdot E_{\rm k}\rangle$')
    ax.grid()
    ax.set_xlabel(r'$z$ (m)')
    ax.set_ylabel(r'$\langle z\cdot E_{\rm k}\rangle$ (keV)', color = 'r')
    
    ax.tick_params(axis='y', colors='r', which = 'both')
    # ax.spines['left'].set_color('blue')
    
    ax2 = ax.twinx()
    ax2.plot(zemit[:,0], zemit[:,4]/1e3/zemit[:,2]*100, 'b-', label = r'$\sigma_E/E$')
    ax2.set_ylabel(u_kinetic_rel, color = 'b')
    
    ax2.tick_params(axis='y', colors='b', which = 'both')
    ax2.spines['left'].set_color('red')
    ax2.spines['right'].set_color('blue')
    
    fig.savefig('zEkin-z'+fig_ext)
    return

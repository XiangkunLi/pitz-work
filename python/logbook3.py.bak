import nbformat as nbf
from nbformat import v4 as current

from PIL import Image, ImageGrab

import os
import re
import datetime
import shutil
import time

logbookname = 'my_logbook.ipynb'


"""
Grab a screeshot that supports multiple monitors
"""
from ctypes import windll, c_int, c_uint, c_char_p, c_buffer
from struct import calcsize, pack
#from PIL import Image

gdi32 = windll.gdi32

# Win32 functions
CreateDC = gdi32.CreateDCA
CreateCompatibleDC = gdi32.CreateCompatibleDC
GetDeviceCaps = gdi32.GetDeviceCaps
CreateCompatibleBitmap = gdi32.CreateCompatibleBitmap
BitBlt = gdi32.BitBlt
SelectObject = gdi32.SelectObject
GetDIBits = gdi32.GetDIBits
DeleteDC = gdi32.DeleteDC
DeleteObject = gdi32.DeleteObject

# Win32 constants
NULL = 0
HORZRES = 8
VERTRES = 10
SRCCOPY = 13369376
HGDI_ERROR = 4294967295
ERROR_INVALID_PARAMETER = 87


def grab_screen(bbox = None):
    """
    Grabs a screenshot. This is a replacement for PIL's ImageGrag.grab() method
    that supports multiple monitors. (SEE: https://github.com/python-pillow/Pillow/issues/1547)

    Returns a PIL Image, so PIL library must be installed.

    Usage:
        im = grab_screen() # grabs a screenshot of the primary monitor
        im = grab_screen([-1600, 0, -1, 1199]) # grabs a 1600 x 1200 screenshot to the left of the primary monitor
        im.save('screencap.jpg')
    """

    try:
        screen = CreateDC(c_char_p('DISPLAY'), NULL, NULL, NULL)
        screen_copy = CreateCompatibleDC(screen)

        if bbox:
            left,top,x2,y2 = bbox
            width = x2 - left + 1
            height = y2 - top + 1
        else:
            left = 0
            top = 0
            width = GetDeviceCaps(screen, HORZRES)
            height = GetDeviceCaps(screen, VERTRES)

        bitmap = CreateCompatibleBitmap(screen, width, height)
        if bitmap == NULL:
            print('grab_screen: Error calling CreateCompatibleBitmap. Returned NULL')
            return

        hobj = SelectObject(screen_copy, bitmap)
        if hobj == NULL or hobj == HGDI_ERROR:
            print('grab_screen: Error calling SelectObject. Returned {0}.'.format(hobj))
            return

        if BitBlt(screen_copy, 0, 0, width, height, screen, left, top, SRCCOPY) == NULL:
            print('grab_screen: Error calling BitBlt. Returned NULL.')
            return

        bitmap_header = pack('LHHHH', calcsize('LHHHH'), width, height, 1, 24)
        bitmap_buffer = c_buffer(bitmap_header)
        bitmap_bits = c_buffer(' ' * (height * ((width * 3 + 3) & -4)))
        got_bits = GetDIBits(screen_copy, bitmap, 0, height, bitmap_bits, bitmap_buffer, 0)
        if got_bits == NULL or got_bits == ERROR_INVALID_PARAMETER:
            print('grab_screen: Error calling GetDIBits. Returned {0}.'.format(got_bits))
            return

        import PIL
        image = PIL.Image.frombuffer('RGB', (width, height), bitmap_bits, 'raw', 'BGR', (width * 3 + 3) & -4, -1)
        return image
    finally:
        if 'bitmap' in locals():
            if bitmap:
                DeleteObject(bitmap)
        if 'screen_copy' in locals():
            DeleteDC(screen_copy)
        if 'screen' in locals():
            DeleteDC(screen)

def fileparts(fullname):
    '''
    Split the full name of a file into parts
    Parameters
      fullname: full name of the file
    Return
      [path, name, ext]: path, name and extension of the file
    '''
    [path, name] = os.path.split(fullname)
    [name, ext] = os.path.splitext(name)
    return [path, name, ext]
# [path, name, ext] = fileparts('/path/filename.ext')

def split2list(string, pattern = '[; ]+'):
    '''
    Split a long string into several sub-string separated by the pattern
    Parameters
      string: a long string
    Return
      a list of strings
    '''
    # import re
    tmp = re.split(pattern, string)
    return list(filter(None, tmp))

def get_bbox2():
    '''
    Click the mouse, drag it and then release it.
    '''
    from pynput.mouse import Listener
    
    bbox = []
    def on_click(x, y, button, pressed):
        # global bbox
        if pressed:
            bbox.extend([x, y])
        else:
            bbox.extend([x, y])
        if not pressed:
            listener.stop()
            # return False

    with Listener(on_click=on_click) as listener:
        listener.join()
        # here you can read xx and yy
    return bbox
def get_bbox():
    '''
    Click the mouse, move to the next place and click it again
    '''
    from pynput.mouse import Listener
    
    bbox = []
    # counter = 0
    def on_click(x, y, button, pressed):
        if pressed:
            bbox.extend([x, y])
            # counter = counter+1
        if len(bbox) == 4:
            listener.stop()

    with Listener(on_click = on_click) as listener:
        listener.join()

    return bbox
# bbox = get_bbox()

def get_screenshot():
    bbox = get_bbox()
    #img = ImageGrab.grab(bbox = list(bbox))
    img = grab_screen(bbox = list(bbox))
    # img = img.resize((800,600))
    return img
# img = get_screenshot()

def save_screenshot(path = '.', screen_file = 'screenshot', figtype = '.png', stamp = True):

    img = get_screenshot()
    # img = img.resize((800,600))
    
    if stamp:
        screen_file = datetime.datetime.now().strftime("screenshot__%d.%m.%Y__%H_%M_%S__")
    
    screen_file = path+os.sep+screen_file+figtype
    img.save(screen_file)
    return screen_file
# s = save_screenshot()

def convert(in_fig, out_format = None, **kwargs):
    '''quality = 90, dpi=(300, 300)'''
    #from PIL import Image
    if out_format is None:
        out_format = '.png'
    [path, name, ext] = fileparts(in_fig)
    if path is '':
        out_fig = name+out_format
    else:
        out_fig = path+os.sep+name+out_format
    img = Image.open(in_fig)
    img.save(out_fig, **kwargs)
#convert('peak-current-vs-emission-charge-BSA2__21.11.2018__15_41_31__ .eps')


# In[2]:


default_path = r'backup'
# default_path = r'\\afs\ifh.de\group\pitz\data\lixiangk\work\backup'

def to_logbook(filename = 'my_logbook.ipynb', **kwargs):
    
    # Save the current logbook
    from sys import platform
    if platform == "linux" or platform == "linux2":
        # linux
        pass
    elif platform == "darwin":
        # OS X
        pass
    elif platform == "win32":
        # Windows...
        pass
    shutil.copyfile(filename, '%s.bak' % filename)
    
    global default_path
    default_path = default_path.replace('\\', os.sep)

    #filename = default_path+os.sep+filename
    if os.path.isfile(filename):
        nb = nbf.read(filename, 4)
    else:
        nb = current.new_notebook()
        nb.cells = current.new_markdown_cell()
    cells = nb.cells

    today = datetime.datetime.now()
    year = today.year
    week = today.isocalendar()[1]
    weektitle = str.format('# Week %02d' % week)
    daytitle = str.format('## %02d%02d%02d' % (today.year, today.month, today.day))
    year_dir = default_path+os.sep+'%02d' % year
    week_dir = year_dir+os.sep+'%02d' % week
    day_dir = week_dir+os.sep+'%02d%02d%02d' % (today.year, today.month, today.day)
    
    # Check if first cell is this week's weektitle or not
    if isinstance(cells, list):
        first_cell = cells[0]
    else:
        first_cell = cells
    
    match = re.search(weektitle, first_cell.source)
    if match is None:
        source = '%s\n' % (weektitle)
        cell = current.new_markdown_cell(source);
        if isinstance(cells, list):
            cells.insert(0, cell)
        else:
            cells = [cell, cells]
    
    # Check if second cell is today's daytitle or not
    if isinstance(cells, list) and len(cells)>1:
        second_cell = cells[1]
        match = re.search(daytitle, second_cell.source)
    else:
        match = None
    
    if match is None:
        source = '%s\n' % (daytitle)
        cell = current.new_markdown_cell(source);
        cells.insert(1, cell)
    
    
    # Copy the images or data files to backup
    new_data_file = []
    new_image_file = []
    try:
        if not os.path.exists(year_dir):
            os.mkdir(year_dir)
        if not os.path.exists(week_dir):
            os.mkdir(week_dir)
        if not os.path.exists(day_dir):
            os.mkdir(day_dir)

        timestamp = '__%02d.%02d.%02d__%02d_%02d_%02d__' % (today.day, today.month, today.year,                                              today.hour, today.minute, today.second)
        if 'image_file' in kwargs.keys():
            image_file = kwargs['image_file']

            if not isinstance(image_file, list):
                image_file = [image_file]

            new_image_file = []
            for image in image_file:
                time.sleep(1)
                [path, name, ext] = fileparts(image)
                new_image = day_dir+os.sep+name+timestamp+ext
                shutil.copy(image, new_image)
                
                if ext == '.eps':
                    convert(new_image, quality = 90, dpi=(300, 300))
                    new_image = day_dir+os.sep+name+timestamp+'.png'

                new_image_file.append(new_image)
            print image_file
            
        if 'data_file' in kwargs.keys():
            data_file = kwargs['data_file']

            if not isinstance(data_file, list):
                data_file = [data_file]
            new_data_file = []
            for data in data_file:
                time.sleep(1)
                [path, name, ext] = fileparts(data)
                print path, name, ext
                new_data = day_dir+os.sep+name+timestamp+ext
                new_data_file.append(new_data)
                shutil.copyfile(data, new_data)
            print data_file
            
        if 'screenshot' in kwargs.keys():
            screenshot = kwargs['screenshot']
            if isinstance(screenshot, str):
                screen_file = screenshot
            elif isinstance(screenshot, bool):
                screen_file = save_screenshot(path = day_dir)
        
        # End of copying the images or data files to backup
    except:
        r = 'Error when copying files'
    
    # Create the new cell
    source = ''
    if 'title' in kwargs.keys():
        title = kwargs['title']
        source = str.format('%s### %s\n' % (source, title))
    
    stamp = '__%02d.%02d.%02d %02d:%02d:%02d__' % (today.day, today.month, today.year,                                          today.hour, today.minute, today.second)
    source = str.format('%s%s\n\n' % (source, stamp))
    if 'description' in kwargs.keys():
        description = str(kwargs['description'])
        source = source+description+'\n' # '%s%s\n' % (source, description)
    if 'image_file' in kwargs.keys():
        for image in new_image_file:
            source = "%s\n![image](%s)" % (source, image)
    if 'data_file' in kwargs.keys():
        for data in new_data_file:
            source = "%s\n- (%s) has been saved." % (source, data)
    if 'screenshot' in kwargs.keys():
        screenshot = kwargs['screenshot']
        if isinstance(screenshot, (bool, str)):
            source = "%s\n![screenshot](%s)" % (source, screen_file)
                
    cell = current.new_markdown_cell(source); # print cell
    # End of creating new cell

    if isinstance(cells, list):
        cells.insert(2, cell)
    else:
        cells = [cell, cells]
    nb.cells = cells
    # print nb.cells
    
    with open(filename, 'w') as f:
        nbf.write(nb, f, 4)
    return


# In[3]:


# to_logbook(title = 'If the image is formatted in `eps` , make a `png` copy to show in `logbook`', \
#           image_file = 'solenoid.eps')
# to_logbook(title = 'Convert and save a picture in a new format', \
#           description = 'To initiate it, just pass `data_file = your_data_file` to **kwargs**', \
#           data_file = 'logbook.ipynb', \
#           screenshot = True)
# to_logbook(title = 'Check is everything is fine', description = 'A description')


# In[52]:


from Tkinter import *
from tkMessageBox import *
from tkFileDialog import askopenfilename, askopenfilenames

# Select all the text in the current widget
def select_all(event):
    w = event.widget
    if isinstance(w, Text):
        w.tag_add(SEL, "1.0", END)
        w.mark_set(INSERT, "1.0")
        w.see(INSERT)
    elif isinstance(w, Entry):
        w.select_range(0, END)
    return 'break'

g_screenshot = ''
def screenshot_callback():
    print 'Make a screen shot!'
    global default_path, g_screenshot
    today = datetime.datetime.now()
    year = today.year
    week = today.isocalendar()[1]
    year_dir = default_path+os.sep+'%02d' % year
    week_dir = year_dir+os.sep+'%02d' % week
    day_dir = week_dir+os.sep+'%02d%02d%02d' % (today.year, today.month, today.day)
    
    g_screenshot = save_screenshot(path = day_dir)
    return
    try:
        g_screenshot = save_screenshot(path = day_dir)
        print 'Screen shot saved to '+g_screenshot
    except:
        pass
def add_callback(ent1, txt2, ent3, ent4):
    global g_screenshot
    title = ent1.get()
    descrip = txt2.get("1.0","end-1c")
    
    image_file = split2list(ent3.get(), '; ')
    data_file = split2list(ent4.get(), '; ')
    
    screenshot = g_screenshot
    kw = {}
    if title is not '':
        kw.update({'title':title})
    if descrip is not '':
        kw.update({'description':descrip})
    if image_file is not []:
        kw.update({'image_file':image_file})
    if data_file is not []:
        kw.update({'data_file':data_file})
    if screenshot is not '':
        kw.update({'screenshot':screenshot})
    print kw
    to_logbook(logbookname, **kw)
    g_screenshot = ''

def clear_callback(edit):
    for e in edit:
        if isinstance(e, Entry):
            e.delete(0, END)
        elif isinstance(e, Text):
            e.delete('1.0', END)
def browse_callback(ent):
    names = askopenfilenames()
    for name in names:
        ent.insert(0, name+'; ')
    
root = Tk(screenName = 'MyLogbook V0.1')
root.bind_class("Entry", "<Control-a>", func = select_all)
root.bind_class("Text", "<Control-a>", func = select_all)

row0 = Frame(root)
lab0 = Label(row0, text = 'Fill the following items to add a new ENTRY!')
row0.pack(side = TOP, fill = X, padx = 5, pady = 5)
lab0.pack(side = TOP)

row1 = Frame(root)
lab1 = Label(row1, text = "Title", width = 10, anchor = 'w')
ent1 = Entry(row1, width = 50)
row1.pack(side = TOP, fill = X, padx = 20, pady = 5)
lab1.pack(side = LEFT)
ent1.pack(side = RIGHT, expand = YES, fill = X)

row2 = Frame(root)
lab2 = Label(row2, text = "Description", width = 10, anchor = 'w')
scr2 = Scrollbar(row2)
txt2 = Text(row2, width = 50, height = 15)
row2.pack(side = TOP, fill = X, padx = 20, pady = 5)
lab2.pack(side = LEFT)
scr2.pack(side = RIGHT, fill = Y)
txt2.pack(side = RIGHT, expand = YES, fill = X)
scr2.config(command = txt2.yview)
txt2.config(yscrollcommand=scr2.set)

row3 = Frame(root)
lab3 = Label(row3, text = "Images", width = 10, anchor = 'w')
ent3 = Entry(row3, width = 50)
but3 = Button(row3, text = 'Browse', width = 6, anchor = 'w')
row3.pack(side = TOP, fill = X, padx = 20, pady = 5)
lab3.pack(side = LEFT)
ent3.pack(side = RIGHT, expand = YES, fill = X, padx = 5)
but3.pack(side = RIGHT, fill = X, padx = 0)
but3.config(command = (lambda e3 = ent3: browse_callback(e3)))

row4 = Frame(root)
lab4 = Label(row4, text = "Files", width = 10, anchor = 'w')
ent4 = Entry(row4, width = 50)
but4 = Button(row4, text = 'Browse', width = 6, anchor = 'w')
row4.pack(side = TOP, fill = X, padx = 20, pady = 5)
lab4.pack(side = LEFT)
ent4.pack(side = RIGHT, expand = YES, fill = X, padx = 5)
but4.pack(side = RIGHT, fill = X, padx = 0)
but4.config(command = (lambda e4 = ent4: browse_callback(e4)))

b1 = Button(root, text='Screenshot', width = 15, command = screenshot_callback)
b1.pack(side = LEFT, padx = 80, pady = 10)

b2 = Button(root, text='Add entry', width = 15,\
            command = (lambda e1 = ent1, t2 = txt2, e3 = ent3, e4 = ent4: add_callback(e1, t2, e3, e4)))
b2.pack(side = RIGHT, padx = 80, pady = 10)

editables = [ent1, txt2, ent3, ent4]
b3 = Button(root, text='Clear all', width = 15, command = (lambda edit = editables: clear_callback(edit)))
b3.pack(side = LEFT, padx = 80, pady = 10)

root.bind('<Return>', (lambda event, x=0: x))

root.mainloop()


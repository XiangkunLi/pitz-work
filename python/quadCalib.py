# -*- coding: utf-8 -*-
"""
Created on Sat Feb  6 03:32:06 2021

@author: lixiangk
"""
from universal import *
from transfer import *
from my_object import *

def G2K(G, P0, qn = 1.0):
    '''
    Parameters
      G: gradient, T/m
      P0: momentum, MeV/c
      qn = q/qe: ratio of charge to the electron charge, therefore qn=1 for electrons
    Returns
      K: focal strength, m^-2
    '''
    return G*qn/(P0*1e6/g_c)

def quadrupole(K, L):
    '''
    Parameters
      K: focal strength, m^-2
      L: effective length, m
    '''
    
    if K>0:
        kk = np.sqrt(K); kl = kk*L
        cc = np.cos(kl); ss = np.sin(kl)
        tr = np.array([[cc, 1./kk*ss], [-kk*ss, cc]])
    elif K<0:
        kk = np.sqrt(-K); kl = kk*L
        cc = np.cosh(kl); ss = np.sinh(kl)
        tr = np.array([[cc, 1./kk*ss], [kk*ss, cc]])
        #print('K<0', end = ' ')
    else:
        tr = np.array([[1, L], [0, 1]])
    return tr

def constructor(G, LQUAD = LQUAD, L1 = 1.0, L2 = 1.0, P0 = P0):
    K = G2K(G, P0)
    D1 = drift(L1)
    Q1 = quadrupole(K, LQUAD)
    D2 = drift(L2)
    
    M = multiply(D1, Q1, D2)
    return M

def quad_calib(M12, st, quad, scr, P0 = 19.99, LQUAD = 0.0675, plot = False):
    '''
    Calculate the quad gradient from calibrated M12 or R12 element.
    Parameters
    ----------
    M12 : float
        M12 or R12 element of the transfer matrix, calibrated experimentally
    st : string
        Steerer name.
    quad : string
        Quadrupole name.
    scr : string
        Screen monitor name.
    P0 : float, optional
        DESCRIPTION. The default is 19.99.
    LQUAD : float, optional
        Effective length of quadrupole used to compute the transfer matrix. The default is 0.0675.
    plot : boolean, optional
        If plot or not. The default is False.

    Returns
    -------
    grad: float
        The equivalent gradient of the quad which produces the same `M12` as measured.

    '''
    polarity = 1

    M12_exp = M12
    

    L1 = quads[quad]-steerers[st]-LQUAD/2.-0.00/2 # drift from steerer to quad
    L2 = scrns[scr]-quads[quad]-LQUAD/2. # drift from quad to screen
    #print('\nL1: ', L1, 'L2: ', L2)

    # correction of steere length
    rr = (scrns[scr]-steerers[st]-0.00/2)/(scrns[scr]-steerers[st]-0.00/2); 
    M12_0 = [M12_exp*rr*polarity]
    #print('M12_0: ', M12_0)

    x = np.linspace(-10, 10, 100) # gradient: T/m
    m12 = [constructor(xi, LQUAD, L1, L2, P0)[0,1] for xi in x]

    func = interp1d(m12, x)
    grad = func(M12_0)
    #print('Effective grad: ', grad, 'T/m')
    print('Peak grad: ', grad*astra_to_sco, 'T/m')
    #print('K: ', G2K(1, P0), 'm^-2')

    if plot:
        fig, ax = plt.subplots()
        ax.plot(x, m12, '-o')
        ax.plot(grad, M12_0, 'D')
    
        ax.grid()
        ax.set_xlabel(r'$G$ (T/m)')
        ax.set_ylabel(r'$M_{\rm 12}$ (m)')

    #print('Transfer matrix: \n', constructor(grad[0], LQUAD, L1, L2, P0))
    
    return grad[0]*astra_to_sco

M12 = 5
st, quad, scr = 'HIGH1.ST1', 'HIGH1.Q4', 'HIGH1.SCR4'
func = lambda x: quad_calib(x, st, quad, scr, P0 = 19.99, LQUAD = 0.0675)
#func(M12)
#%% High1.Q4 x

st, quad, scr = 'HIGH1.ST1', 'HIGH1.Q4', 'HIGH1.SCR4'
func = lambda x: quad_calib(x, st, quad, scr, P0 = 19.99, LQUAD = 0.0675)

I = np.array([-5, -4, -3, -2.0, -1.5, -1.0, -0.5, 
              0, 0.5, 1.0, 1.5, 2, 3])
M12 = np.array([-1.607, -0.595, 0.462, 1.536, 2.049, 2.570, 3.057, 
                3.496, 3.942, 4.455, 4.988, 5.532, 6.652])
# 4 A -> 7.841
# 5 A -> 9.210
#9.108, 7.955, 6.678, 5.583, 5.002, 4.457, 3.949
grads = np.array([func(m) for m in M12])

Ix, Gx = I, grads

tmp = np.linspace(-5, 5, 100)

fig, ax = plt.subplots()

select = I>0
popt, pcov = curve_fit(linear, I[select], grads[select]); 
#print(popt, np.sqrt(np.diag(pcov)))

ax.plot(I[select], grads[select], 'r<')
ax.plot(tmp, linear(tmp, *popt), 'r-')

##
popt, pcov = curve_fit(linear, grads[select], I[select]); 
print(popt, np.sqrt(np.diag(pcov)))
##

select = I<0
popt, pcov = curve_fit(linear, I[select], grads[select]); 
#print(popt, np.sqrt(np.diag(pcov)))

ax.plot(I[select], grads[select], 'b>')
ax.plot(tmp, linear(tmp, *popt), 'b-')

##
popt, pcov = curve_fit(linear, grads[select], I[select]); 
print(popt, np.sqrt(np.diag(pcov)))
##

select = np.abs(I)<1e-3
ax.plot(I[select], grads[select], 'ko')
ax.plot(tmp, linear(tmp, popt[0], 0), 'k--')

ax.grid()
ax.set_xlabel(r'current (A)')
ax.set_ylabel(r'gradient (T/m)')
#fig.savefig('quad_calib_H1Q4x.png')

#%% High1.Q4 y

st, quad, scr = 'HIGH1.STA1', 'HIGH1.Q4', 'HIGH1.SCR4'
func = lambda x: quad_calib(x, st, quad, scr, P0 = 19.99, LQUAD = 0.0675)

I =   np.array([-5,    -4,    -3,    -2,    -1.5,  -1,    -0.5,   
                0, 0.5,   1.0,   1.5,   2,     3,     4,     5])
M12 = np.array([ 5.374, 4.973, 4.401, 3.892, 3.650, 3.380, 3.175, 
                2.985, 2.784, 2.559, 2.332, 2.082, 1.601, 1.110, 0.631])

grads = np.array([func(m) for m in M12])
Iy, Gy = I[:-2], grads[:-2]

tmp = np.linspace(-5, 5, 100)

fig, ax = plt.subplots()

select = I>0
popt, pcov = curve_fit(linear, I[select], grads[select]); 
#print(popt, np.sqrt(np.diag(pcov)))

ax.plot(I[select], grads[select], 'r<')
ax.plot(tmp, linear(tmp, *popt), 'r-')

##
popt, pcov = curve_fit(linear, grads[select], I[select]); 
print(popt, np.sqrt(np.diag(pcov)))
##

select = I<0
popt, pcov = curve_fit(linear, I[select], grads[select]); 
#print(popt, np.sqrt(np.diag(pcov)))

ax.plot(I[select], grads[select], 'b>')
ax.plot(tmp, linear(tmp, *popt), 'b-')

##
popt, pcov = curve_fit(linear, grads[select], I[select]); 
print(popt, np.sqrt(np.diag(pcov)))
##

select = np.abs(I)<1e-3
ax.plot(I[select], grads[select], 'ko')
ax.plot(tmp, linear(tmp, popt[0], 0), 'k--')

ax.grid()
ax.set_xlabel(r'current (A)')
ax.set_ylabel(r'gradient (T/m)')
fig.savefig('quad_calib_H1Q4y.png')


#%%
Gxy = np.sqrt(Gx**2+Gy**2)
Gxy[Ix>0] = -Gxy[Ix>0]

select = Ix>-10

popt, pcov = curve_fit(linear, Ix[select], Gxy[select]); 
print(popt, np.sqrt(np.diag(pcov)))

fig, ax = plt.subplots()

ax.plot(Ix[select], Gxy[select], 'r<')
ax.plot(tmp, linear(tmp, *popt), 'r-')
ax.grid()

#%% High1.Q6 x

st, quad, scr = 'HIGH1.ST1', 'HIGH1.Q6', 'HIGH1.SCR4'
func = lambda x: quad_calib(x, st, quad, scr, P0 = 19.99, LQUAD = 0.0675)

I = np.array([-5, -4, -3, -2.0, -1.5, -1.0, -0.5, 
              0, 0.5, 1.0, 1.5, 2, 3, 4])
M12 = np.array([-2.834, -1.538, -0.220, 1.114, 1.761, 2.404, 2.988, 
                3.507, 4.082, 4.711, 5.377, 6.064, 7.450, 8.865])
# 5 A -> 10.486

grads = np.array([func(m) for m in M12])

tmp = np.linspace(-5, 5, 100)

fig, ax = plt.subplots()

select = np.abs(I)<1e-3
ax.plot(I[select], grads[select], 'ko')

select = I>0
popt, pcov = curve_fit(linear, I[select], grads[select]); 
print(popt, np.sqrt(np.diag(pcov)))

ax.plot(I[select], grads[select], 'r<')
ax.plot(tmp, linear(tmp, *popt), 'r-')


select = I<0
popt, pcov = curve_fit(linear, I[select], grads[select]); 
print(popt, np.sqrt(np.diag(pcov)))

ax.plot(I[select], grads[select], 'b>')
ax.plot(tmp, linear(tmp, *popt), 'b-')

ax.grid()
ax.set_xlabel(r'current (A)')
ax.set_ylabel(r'gradient (T/m)')
fig.savefig('quad_calib_H1Q6x.png')

#%% High1.Q6 y

st, quad, scr = 'HIGH1.STA1', 'HIGH1.Q6', 'HIGH1.SCR4'
func = lambda x: quad_calib(x, st, quad, scr, P0 = 19.99, LQUAD = 0.0675)

I =   np.array([-5,    -4,    -3,    -2,    -1.5,  -1,    -0.5,   
                0,     0.5,   1.0,   1.5,   2,     3,     4,     5])
M12 = np.array([ 7.881, 6.861, 5.825, 4.810, 4.317, 3.829, 3.387, 
                3.007, 2.571, 2.126, 1.648, 1.160, 0.184, -0.794, -1.767])

grads = np.array([func(m) for m in M12])

tmp = np.linspace(-5, 5, 100)

fig, ax = plt.subplots()

select = np.abs(I)<1e-3
ax.plot(I[select], grads[select], 'ko')

select = I>0
popt, pcov = curve_fit(linear, I[select], grads[select]); 
print(popt, np.sqrt(np.diag(pcov)))

ax.plot(I[select], grads[select], 'r<')
ax.plot(tmp, linear(tmp, *popt), 'r-')


select = I<0
popt, pcov = curve_fit(linear, I[select], grads[select]); 
print(popt, np.sqrt(np.diag(pcov)))

ax.plot(I[select], grads[select], 'b>')
ax.plot(tmp, linear(tmp, *popt), 'b-')

ax.grid()
ax.set_xlabel(r'current (A)')
ax.set_ylabel(r'gradient (T/m)')
fig.savefig('quad_calib_H1Q6y.png')

#%%

st, quad, scr = 'HIGH1.ST1', 'HIGH1.Q5', 'HIGH1.SCR5'
func = lambda x: quad_calib(x, st, quad, scr, P0 = 19.262, LQUAD = 0.0675, plot = 0)

# [5, 13.837],
I, M12 = np.array([[4, 11.311],
                   [3, 9.448],
                   [2, 7.487],
                   [1, 5.763],
                   [0, 3.998],
                   [-1, 2.477],
                   [-2, 0.719],
                   [-3, -1.11],
                   [-4, -2.902],
                   [-5, -4.763]]).T[:]
grads = np.array([func(m) for m in M12])

tmp = np.linspace(-5, 5, 100)

fig, ax = plt.subplots()

select = np.abs(I)<1e-3
ax.plot(I[select], grads[select], 'ko')

select = I>0
popt, pcov = curve_fit(linear, I[select], grads[select]); print(popt[0])

ax.plot(I[select], grads[select], 'r<')
ax.plot(tmp, linear(tmp, *popt), 'r-')


select = I<0
popt, pcov = curve_fit(linear, I[select], grads[select]); print(popt[0])

ax.plot(I[select], grads[select], 'b>')
ax.plot(tmp, linear(tmp, *popt), 'b-')

ax.grid()
ax.set_xlabel(r'current (A)')
ax.set_ylabel(r'gradient (T/m)')
fig.savefig('quad_calib_H1Q5x.png')

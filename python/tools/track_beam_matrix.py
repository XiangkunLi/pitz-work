import sys
if sys.platform == "linux" or sys.platform == "linux2":
    sys.path.append(r'/afs/ifh.de/group/pitz/data/lixiangk/work/sync/python')
    rootdir = r'//afs/ifh.de/group/pitz/data/lixiangk/work'
elif sys.platform == "win32":
    sys.path.append(r'\\afs\ifh.de\group\pitz\data\lixiangk\work\sync\python')
    rootdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\work'
elif sys.platform == "darwin":
    print("OS X")
else:
    print("Unknown platform!")

from my_object import *

from platypus import *
#from platypus.mpipool import MPIPool
import sys
import logging

import timeit
import pickle
import numpy as np

import os,shutil
import time

def gen_particle(x = [0, 0, 0, 0]):
    mean = [0, 0, 0, 0, 0, 19.97e6]

    x2, xxp, xp2 = 1.53e-3**2, -0.183e-6, 0.217e-3**2
    y2, yyp, yp2 = 1.12e-3**2, -0.105e-6, 0.240e-3**2
    #xy, xpy, xyp, xpyp = 0, 0, 0, 0
    x = [xi*1e-6 for xi in x]
    xy, xpy, xyp, xpyp = x[:];print(xpyp*1e6)

    #C = 0.06e-12
    #xpyp = (xpy*xyp-C)/xy;print(xpyp*1e6)
    
    print(xy*xpyp-xyp*xpy)
    
    z2, zPz, Pz2 = 1.50e-3**2, -0e3*1.50e-3, 30e3**2
    cov = [[x2, xxp, xy, xyp, 0, 0], [xxp, xp2, xpy, xpyp, 0, 0], [xy, xpy, y2, yyp, 0, 0], [xyp, xpyp, yyp, yp2, 0, 0],\
           [0, 0, 0, 0, z2, zPz], [0, 0, 0, 0, zPz, Pz2]]
    #print(np.array(cov)[0:4,0:4])
    cov = np.array(cov)
    
    cov0 = np.array([[ 2.30464823e+00, -3.91386668e-01,  1.65147710e-01,  0.85590687e-03],
                     [-3.91386668e-01,  6.98328687e-02,  1.20487169e-02,  1.35171682e-03],
                     [ 1.65147710e-01, -1.20487169e-02,  1.29239787e+00, -2.50897238e-01],
                     [ 6.85590687e-03,  1.35171682e-03, -2.50897238e-01,  5.25824222e-02]])
    cov0 = np.array([[ 2.24222854e-06, -1.04822275e-07, -6.76522173e-08,  6.72156667e-09],
                     [-1.04822275e-07,  3.99899629e-08,  7.38503933e-09, -8.46297007e-11],
                     [-6.76522173e-08,  7.38503933e-09,  1.10440187e-06, -8.83595366e-08],
                     [ 6.72156667e-09, -8.46297007e-11, -8.83595366e-08,  4.73692479e-08]])*1e6

    cov0 = np.array([[ 1.851129E-06,   3.479085E-07,   1.952076E-07,   5.569377E-08],
                     [ 3.479085E-07,   7.571832E-08,   2.801270E-08,   8.741910E-09],
                     [ 1.952076E-07,   2.801270E-08,   7.065937E-07,   1.133220E-07],
                     [ 5.569377E-08,   8.741910E-09,   1.133220E-07,   2.366359E-08]])*1e6

    cov0 = np.array([[ 3.06607457e-08, -1.49144008e-08,  1.44998298e-10, -1.38840301e-09],
                     [-1.49144008e-08,  3.10046248e-08, -8.48432415e-10,  4.46063286e-09],
                     [ 1.44998298e-10, -8.48432415e-10,  2.76200654e-08, -8.58145048e-09],
                     [-1.38840301e-09,  4.46063286e-09, -8.58145048e-09,  3.26708690e-08]])*1e6
    
    cov[0:4,0:4] = cov0[:,:]*1e-6
    
    nop = 200000
    p = np.random.multivariate_normal(mean, cov, nop)

    out = np.zeros((nop, 10), dtype = float)
    out[:,0] = p[:,0]
    out[:,1] = p[:,2]
    out[:,2] = p[:,4]
    out[:,3] = p[:,1]*p[:,5]
    out[:,4] = p[:,3]*p[:,5]
    out[:,5] = p[:,5]

    out[0,:] = 0
    out[0,2] = 5.277
    out[0,5] = mean[-1]
    out[1:,5] -= mean[-1]

    out[:,7] = -0.25/nop
    out[:,8] = 1
    out[:,9] = 5

    np.savetxt('ast.0528.001', out, fmt = '%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%4d%4d')
    return

def run_sco(x, particle = None, save_data = False):

    if particle is None:
        particle = 'ast.0528.001'

    #y = [x[0], x[1], 1.53, 1.12, x[2], x[3]]
    #obj_undScan999(y)

    direc='%.2f'%x[0]
    for i in x[1:]:
        direc += '_%.2f' % i
    cmd = 'mkdir '+direc
    os.system(cmd)
    
    os.chdir(direc)
    gen_particle(x)
    
    #os.system('generator gen.in')
    #time.sleep(2)

    optics = 'beamline.txt'
    
    shutil.copyfile('..'+os.sep+'optimizer.dat', 'optimizer.dat')
    shutil.copyfile('..'+os.sep+optics, optics)
    #shutil.copyfile('..'+os.sep+particle, particle)
    
    output = optics+'''
'''+particle
    
    filename = 'inputs.txt'
    ff = open(filename, 'w')
    ff.write(output)
    ff.close()
    
    cmd = 'sco < inputs.txt'
    os.system(cmd)
    
    diag = np.loadtxt('BeamDynamics.dat')
    fx = interp1d(diag[:,0], diag[:,3])
    fy = interp1d(diag[:,0], diag[:,4])
    
    zpos = np.array([6.250, 7.125, 8.41, 8.92])
    Xrms = np.array([1.78, 2.06, 2.50, 2.77])
    Yrms = np.array([1.37, 1.81, 2.34, 2.52])
    
    d_rms_x = np.abs(fx(zpos-5.277)-Xrms)
    d_rms_y = np.abs(fy(zpos-5.277)-Yrms)
    
    res = np.sqrt(d_rms_x**2+d_rms_y**2)
    
    if save_data == False:
        cmd = 'del BeamDynamics.dat'
        os.system(cmd)
        cmd = 'del ast.0528.001'
        os.system(cmd)
    
    os.chdir('..')
    
    return res

#run_sco([10.3, 8.9, 0.92, 0.61], save_data = True)
#run_sco([1.51, 0.35, -0.33, -0.25], save_data = True)
run_sco([0.25, 0, 0, 0], save_data = True)
exit()

def obj_run_sco(x):

    obj =  run_sco(x)

    r = list(x) + list(obj)
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle, np.atleast_2d(r), fmt='%14.6E')

    return obj

#obj = obj_run_sco([12, 12, -0.3, -0.3]); print(obj)
#exit()

class GeneratorFromFile(Generator):
    
    def __init__(self, filename, usecols = None):
        super(GeneratorFromFile, self).__init__()
        self.x = np.loadtxt(filename, usecols = usecols)
        self.NCOUNT = 0
    def generate(self, problem):
        solution = Solution(problem)
        
        nvars = problem.nvars
        nobjs = problem.nobjs
        
        solution.variables =  self.x[self.NCOUNT, 0:nvars]
        #solution.objectives = self.x[self.NCOUNT, nvars:nvars+nobjs]

        #solution.feasible = True
	#solution.evaluated = False
        
        self.NCOUNT += 1
        
        return solution


class NSGAII_resume(AbstractGeneticAlgorithm):
    
    def __init__(self, problem,
                 population_size = 100,
                 generator = RandomGenerator(),
                 selector = TournamentSelector(2),
                 variator = None,
                 archive = None,
                 nth_run = 1,
                 **kwargs):
        super(NSGAII_resume, self).__init__(problem, population_size, generator, **kwargs)
        self.selector = selector
        self.variator = variator
        self.archive = archive
        self.nth_run = nth_run
        
    def step(self):
        if self.nfe == 0:
            self.initialize()
        else:
            self.iterate()

        if self.archive is not None:
            self.result = self.archive
        else:
            self.result = self.population

        self.dump()

    def dump(self):

        ### save the current population
        current = []
        for k in np.arange(len(self.population)):
            r_vars = self.population[k].variables[:]
            r_objs = self.population[k].objectives[:]
            current.append(list(r_vars)+list(r_objs))
        with open('population@%04d.%03d' % (self.nfe, self.nth_run), 'w') as f_handle:
            print(self.nfe, np.atleast_2d(current)[-1])
            np.savetxt(f_handle, np.atleast_2d(current), fmt='%14.6f')
        ###

        ### print/save the current result, added on September 13, 2019
        current = []
        for k in np.arange(len(self.result)):
            r_vars = self.result[k].variables[:]
            r_objs = self.result[k].objectives[:]
            current.append(list(r_vars)+list(r_objs))
        with open('result@%04d.%03d' % (self.nfe, self.nth_run), 'w') as f_handle:
            print(self.nfe, np.atleast_2d(current)[-1])
            np.savetxt(f_handle, np.atleast_2d(current), fmt='%14.6f')
        ###
        
        ### save the attributes except evaluator
        fullname = 'attribute@%04d.pkl' % self.nfe
        att = self.__dict__.copy()
        att['evaluator'] = []
        #print('att is: ', att)
        with open(fullname, 'wb') as f_handler:
            pickle.dump(att, f_handler)
        ###
        
        ### save the algorithm w/o evaluator
        evaluator = self.evaluator
        self.evaluator = None
        
        fullname = 'algorithm@%04d.%03d.pkl' % (self.nfe, self.nth_run)
        with open(fullname, 'wb') as f_handler:
            pickle.dump(self, f_handler)
        
        self.evaluator = evaluator

    def initialize(self):
        super(NSGAII_resume, self).initialize()
        
        if self.archive is not None:
            self.archive += self.population
        
        if self.variator is None:
            self.variator = default_variator(self.problem)

    def initialize_to_resume(self, generator = None):
        if generator is None:
            generator = self.generator
        
        self.population = [generator.generate(self.problem) for _ in range(self.population_size)]

        self.evaluate_all(self.population)
        
        if self.archive is not None:
            self.archive += self.population
        
        if self.variator is None:
            self.variator = default_variator(self.problem)

        self.dump()

    def iterate(self):
        offspring = []
        
        while len(offspring) < self.population_size:
            parents = self.selector.select(self.variator.arity, self.population)
            offspring.extend(self.variator.evolve(parents))

        #retrieve the simulation manually
        #offspring = [self.generator.generate(self.problem) for _ in range(self.population_size)]
        
        self.evaluate_all(offspring)

        ### print/save the current offspring, added on September 23, 2019
        import numpy as np
        current = []
        for k in np.arange(len(offspring)):
            r_vars = offspring[k].variables[:]
            r_objs = offspring[k].objectives[:]
            current.append(list(r_vars)+list(r_objs))
        with open('offspring@%04d.%03d' % (self.nfe, self.nth_run), 'w') as f_handle:
            print(self.nfe, np.atleast_2d(current)[-1])
            np.savetxt(f_handle, np.atleast_2d(current), fmt='%14.6f')
        ###
        
        offspring.extend(self.population)
        nondominated_sort(offspring)
        self.population = nondominated_truncate(offspring, self.population_size)

        
        if self.archive is not None:
            self.archive.extend(self.population)

def initialize_algorithm(fullname):
    #fullname = 'algorithm@0095.001.pkl'
    with open(fullname, 'rb') as f_handle:
        algo = pickle.load(f_handle)

    print(algo.__dict__)
    return algo

logging.basicConfig(level=logging.INFO)

if __name__ == "__main__":
    # define the problem definition
    problem = Problem(4, 4)
    bounds = [1.79, 0.65, 1.19, 0.52]
    problem.types[0] = Real(-bounds[0], bounds[0])
    problem.types[1] = Real(-bounds[1], bounds[1])
    problem.types[2] = Real(-bounds[2], bounds[2])
    problem.types[3] = Real(-bounds[3], bounds[3])
    problem.function = obj_run_sco
    
    #from platypus import NSGAII, DTLZ2
    #problem = DTLZ2()

    # define the generator for initialization
    #generator_init = GeneratorFromFile('result@.003') # Continue after the first run
    
    
    time1 = timeit.default_timer()
    
        
    algorithm = NSGAII_resume(problem, population_size = 100)
    #algorithm.initialize_to_resume(generator_init)
        
    #algorithm = initialize_algorithm('algorithm@3626.003.pkl')
    #algorithm.problem = algorithm0.problem
    #algorithm.evaluator = algorithm0.evaluator
    #algorithm.nth_run = 4

    #retrieve the optimization process
    #algorithm.generator = generator_init

    #algorithm.dump()
        
    algorithm.run(5000)
    
    # display the results
    for solution in algorithm.result:
        print(solution.objectives)


    time2 = timeit.default_timer()
    print('time elapsed: ', time2-time1)

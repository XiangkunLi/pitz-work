from FEL.basic import *
from tracker.FieldMap3D import *

def RFGun3D():
    
    gun3d = build_cavity(field_maps+os.sep+'gun42cavity.txt', 0, 1, 1.3e9, 90)
    #boo3d = build_cavity(field_maps+os.sep+'CDS14_15mm.txt', 2.675, 12.50e6, 1.3e9, 28.20-20)
    #boo3d = build_cavity(field_maps+os.sep+'CDS14_15mm.txt', 2.675, 12.50e6, 1.3e9, 105.81-20)
    #sol3d = build_solenoid(field_maps+os.sep+'gunsolenoidsPITZ.txt', 0, Bsol) # 270 A

    Xmax, Ymax, Zmax = 30e-3, 30e-3, 0.32
    Nx, Ny, Nz = 120, 120, 128
    dx, dy, dz = Xmax/Nx, Ymax/Ny, Zmax/Nz
    fm3d = FieldMap3D(dx, dy, dz, 0, Zmax, Xmax, Ymax, gun3d)
    fm3d.export3D3('gun42_3D', ext = ['.ex', '.ey', '.ez', '.bx', '.by', '.bz'])
    #undul.export_3D_GPT('3Dund', ext = ['.bx', '.by', '.bz'])
    return
#RFGun3D()

def booster3D():
    
    #gun3d = build_cavity(field_maps+os.sep+'gun42cavity.txt', 0, 1, 1.3e9, 90)
    boo3d = build_cavity(field_maps+os.sep+'CDS14_15mm.txt', 2.675, 1, 1.3e9, 90)
    #boo3d = build_cavity(field_maps+os.sep+'CDS14_15mm.txt', 2.675, 12.50e6, 1.3e9, 105.81-20)
    #sol3d = build_solenoid(field_maps+os.sep+'gunsolenoidsPITZ.txt', 0, Bsol) # 270 A

    Xmax, Ymax, Zmax = 15e-3, 15e-3, 1.72237
    Nx, Ny, Nz = 60, 60, 700-1
    dx, dy, dz = Xmax/Nx, Ymax/Ny, Zmax/Nz
    fm3d = FieldMap3D(dx, dy, dz, 0, Zmax, Xmax, Ymax, boo3d)
    fm3d.export_3D3('CDS14_15mm_3D', ext = ['.ex', '.ey', '.ez', '.bx', '.by', '.bz'])
    #undul.export_3D_GPT('3Dund', ext = ['.bx', '.by', '.bz'])
    return
#booster3D()

def solenoid3D():
    
    #gun3d = build_cavity(field_maps+os.sep+'gun42cavity.txt', 0, 1, 1.3e9, 90)
    #boo3d = build_cavity(field_maps+os.sep+'CDS14_15mm.txt', 2.675, 12.50e6, 1.3e9, 28.20-20)
    #boo3d = build_cavity(field_maps+os.sep+'CDS14_15mm.txt', 2.675, 12.50e6, 1.3e9, 105.81-20)
    sol3d = build_solenoid(field_maps+os.sep+'gunsolenoidsPITZ.txt', 0, 1) # 270 A

    Xmax, Ymax, Zmin, Zmax = 30e-3, 30e-3, -0.2, 2.0
    Nx, Ny, Nz = 120, 120, 220
    dx, dy, dz = Xmax/Nx, Ymax/Ny, (Zmax-Zmin)/Nz
    fm3d = FieldMap3D(dx, dy, dz, Zmin, Zmax, Xmax, Ymax, sol3d)
    fm3d.export_3D3('gunsolenoidsPITZ_3D', ext = ['.bx', '.by', '.bz'])
    #undul.export_3D_GPT('3Dund', ext = ['.bx', '.by', '.bz'])
    return
#solenoid3D()

def und3DGPT():
    sample = 64
    lam_u, Nu, trim1, trim2 = 7.4e-2, 13, 0.25, 0.75
    ku = 2*np.pi/lam_u
    Lu = lam_u*Nu

    u3d = Undulator3D(Nu, lam_u, trim1 = trim1, trim2 = trim2)

    Xmax, Ymax = 20e-3, 20e-3
    Nx, Ny = 10, 10
    dx, dy, dz = Xmax/Nx, Ymax/Ny, lam_u/sample
    
    undul = FieldMap3D(dx, dy, dz, -Lu/2., Lu/2., Xmax, Ymax, u3d.EM3D)
    undul.export3D('3Dund40mmx40mm', ext = ['.bx', '.by', '.bz'])
    #undul.export_3D_GPT('3Dund', ext = ['.bx', '.by', '.bz'])
    return
#und3DGPT()


def und3D():
    sample = 64
    lam_u, Nu, trim1, trim2 = 40e-3, 150, 0.25, 0.75
    ku = 2*np.pi/lam_u
    Lu = lam_u*Nu

    u3d = U3DFromRaw(Nu, lam_u, trim1 = trim1, trim2 = trim2)

    Xmax, Ymax = 12e-3, 8e-3
    Nx, Ny = 12, 8
    dx, dy, dz = Xmax/Nx, Ymax/Ny, lam_u/sample
    undul = Field3D(dx, dy, dz, -Lu/2., Lu/2., Xmax, Ymax, u3d.EM3D)
    undul.export3D('3Dund40mm_6m', ext = ['.bx', '.by', '.bz'])
    #undul.export_3D_GPT('3Dund', ext = ['.bx', '.by', '.bz'])
    return
#und3D()

def und3D2():
    
    filename = 'fld26.txt'
    
    _, basename, _ = fileparts(filename)
    
    Nh = 25
    Nu = 120; lam_u = 0.03; Lu = Nu*lam_u
    
    u3d = U3DFromRaw(Nu, lam_u, filename, Nh, ratio = [1e-3, 1],
                     floor_cut = 1, taper_coef = 0)
    
    sample = 64
    Zmin, Zmax = -Lu/2., Lu/2.
    dz = lam_u/sample
    
    Xmax, Ymax = 10e-3, 5e-3
    Nx, Ny = 10, 5
    dx, dy = Xmax/Nx, Ymax/Ny
    undul = FieldMap3D(dx, dy, dz, Zmin, Zmax, Xmax, Ymax, u3d.EM3D)
    undul.export3D('3DDESY26-20x10mm-uniform', ext = ['.bx', '.by', '.bz'])
#und3D2()

#%%
def EM3D(x, y, z, t=0, *args):
            
    if np.isscalar(x):
        x = [x]
    if np.isscalar(y):
        y = [y]
    if np.isscalar(z):
        z = [z]
    x = np.asarray(x).flatten()
    y = np.asarray(y).flatten()
    z = np.asarray(z).flatten()
    
    nn = np.max([x.size, y.size, z.size])
    
    if x.size == 1:
        x = np.ones(nn)*x[0]
    if y.size == 1:
        y = np.ones(nn)*y[0]
    if z.size == 1:
        z = np.ones(nn)*z[0]
    
    if not np.isscalar(t):
        t = np.asarray(t)
        
    F2d = np.zeros((nn, 6))
    F2d[:,0] = 1e6
    
    return F2d

def By():
    
    LB = 1
    Zmin, Zmax = -LB/2., LB/2.
    dz = 0.1
    
    Xmax, Ymax = 20e-3, 20e-3
    Nx, Ny = 11, 11
    dx, dy = Xmax/Nx, Ymax/Ny
    
    
    undul = FieldMap3D(dx, dy, dz, Zmin, Zmax, Xmax, Ymax, EM3D)
    undul.export3D('DY_3D100cm-20x20mm', ext = ['.bx', '.by', '.bz'])
    
#By()

def Ex():
    
    LB = 1
    Zmin, Zmax = -LB/2., LB/2.
    dz = 0.1
    
    Xmax, Ymax = 100e-3, 20e-3
    Nx, Ny = 51, 11
    dx, dy = Xmax/Nx, Ymax/Ny
    
    
    undul = FieldMap3D(dx, dy, dz, Zmin, Zmax, Xmax, Ymax, EM3D)
    undul.export3D('Ex_3D_100x20x1000mm', ext = ['.ex', '.ey', '.ez'])
    
Ex()

#%%
# awk '{if(NR>2) print($1, $2, $3, $4, $6, $8)}' B-Field[Ms].txt  > B-Field[Ms]_Re.txt

workdir = r'\\afs\ifh.de\group\pitz\data\anusorn\sim1\undulator\fix_mesh_problem'
workdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim\2020\air-coil'
os.chdir(workdir)

fname = 'B-Field[Ms]_202011Re.txt'
fig_ext = '@'+fname+'.png'
fields = pd_loadtxt(fname) # at 20 A

#%%
xx = np.linspace(-20, 20, 41)*1e-3
yy = np.linspace(-10, 10, 21)*1e-3
zz = np.linspace(-2000, 2000, 4001)*1e-3

Bx = np.reshape(fields[:,3], (4001, 21, 41))
By = np.reshape(fields[:,4], (4001, 21, 41))
Bz = np.reshape(fields[:,5], (4001, 21, 41))

fBx3D = RegularGridInterpolator((zz, yy, xx), Bx,
                                bounds_error = False, fill_value = 0)
fBy3D = RegularGridInterpolator((zz, yy, xx), By,
                                bounds_error = False, fill_value = 0)
fBz3D = RegularGridInterpolator((zz, yy, xx), Bz,
                                bounds_error = False, fill_value = 0)



on_axis_max = np.max(np.abs(fBy3D((zz, 0, 0))))

#amp0 = 231.75e-6 # on-axis max is 230.8 uT
#ratio = amp0/on_axis_max; print(ratio) # 

ratio = 0.3950308 # 0.3950308*707240245=279.38 uT for 17 MeV/c used in 2020
ratio = -0.36748686631968 # for the latest field map received from Anusorn in Jan. 2021
#ratio = 0.24 # 322.8877 uT for 25 MeV/c

def EM3D(x, y, z, t = 0, *args):
    if np.isscalar(x):
        x = [x]
    if np.isscalar(y):
        y = [y]
    if np.isscalar(z):
        z = [z]
        
    if len(args)>0:
        dt  = t-args[0]
    else:
        dt = 0
    
    x = np.asarray(x)
    y = np.asarray(y)
    z = np.asarray(z)
    
    n = np.max([len(x), len(y), len(z)])
    
    Bx = fBx3D((-z, y, -x))
    By = fBy3D((-z, y, -x))
    Bz = fBz3D((-z, y, -x))
    
    # return normalied E and B in lab frame
    F2d = np.zeros((n, 6))
    
    F2d[:,3] = Bx*ratio
    F2d[:,4] = By*ratio
    F2d[:,5] = Bz*ratio
    
    return F2d

fig, ax = plt.subplots(ncols = 1, figsize = (4, 3))

By2 = EM3D( 0, 1e-3, zz)[:,4] # fBy3D((zz, 0, 0))*ratio
By1 = EM3D( 0, 0, zz)[:,4]
By3 = EM3D( 0, 2e-3, zz)[:,4] 

ax.plot(zz*1e2, By1*1e6, '-')
ax.plot(zz*1e2, By2*1e6, '-*')
ax.plot(zz*1e2, By3*1e6, '-<')
ax.set_xlabel(r'$z$ (cm)')
ax.set_ylabel(r'$B_y$ ($\mu$T)')
ax.legend([r'$y$ = 0 mm', '1 mm', '2 mm'],
          ncol = 3, handlelength=1, columnspacing = 0.1)
ax.grid()

# By1 = EM3D(xx, -5e-3, 0)[:,4]
# By2 = EM3D(xx, 0, 0)[:,4]
# By3 = EM3D(xx, 5e-3, 0)[:,4]
# ax1.plot(xx*1e3, By1*1e6, '-')
# ax1.plot(xx*1e3, By2*1e6, '-')
# ax1.plot(xx*1e3, By3*1e6, '-')
# ax1.set_xlabel(r'$x$ (mm)')
# ax1.set_ylabel(r'$B_y$ ($\mu$T)')
# ax1.legend([r'$y$ = %d mm' % i for i in [-5, 0, 5] ])
# ax1.grid()

#ax.set_xlim(-3, 3)
#ax.set_ylim(150, 300)
fig.tight_layout()
fig.savefig('Coil-By-vs-z'+fig_ext)

reset_margin(bottom = 0.1, top = 0.9, left = 0.1, right = 0.9)

fig, ax = plt.subplots(ncols = 2, nrows = 2, figsize = (6, 4.5))

Bx_x = EM3D(xx, 0, 0)[:,3]
ax[0][0].plot(xx*1e3, Bx_x*1e6, '-*')

By_x = EM3D(xx, 0, 0)[:,4]
ax[0][1].plot(xx*1e3, By_x*1e6, '-*')

Bx_y = EM3D(0, yy, 0)[:,3]
ax[1][0].plot(yy*1e3, Bx_y*1e6, '-*')

By_y = EM3D(0, yy, 0)[:,4]
ax[1][1].plot(yy*1e3, By_y*1e6, '-*')

ax[0][0].set_ylim(-1, 1)
ax[0][0].set_xlabel(r'$x$ (mm)')
ax[0][1].set_xlabel(r'$x$ (mm)')

ax[1][0].set_xlabel(r'$y$ (mm)')
ax[1][1].set_xlabel(r'$y$ (mm)')

ax[0][0].set_ylabel(r'$B_x$ ($\mu$T)')
ax[0][1].set_ylabel(r'$B_y$ ($\mu$T)')

ax[1][0].set_ylabel(r'$B_x$ ($\mu$T)')
ax[1][1].set_ylabel(r'$B_y$ ($\mu$T)')

for i in [0, 1]:
    for j in [0, 1]:
        ax[i][j].grid()

fig.tight_layout()
fig.savefig('Coil-Bxy-vs-xy'+fig_ext)

reset_margin()
#%%
def coil3D():
    
    Lu = 4.
    
    sample = 64
    Zmin, Zmax = -Lu/2., Lu/2.
    dz = 0.001
    
    Xmax, Ymax = 10e-3, 5e-3
    Nx, Ny = 10, 5
    dx, dy = Xmax/Nx, Ymax/Ny
    fm3d = FieldMap3D(dx, dy, dz, Zmin, Zmax, Xmax, Ymax, EM3D)
    fm3d.export3D('3DCoil+iron-20x10mm-2021', ext = ['.bx', '.by', '.bz'])
coil3D()

#%%
workdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim\2020\U3D'
os.chdir(workdir)

filename = 'slac25.txt'
#filename = 'slacBySym6.txt'
filename = 'fld26.txt'

_, basename, _ = fileparts(filename)

Nh = 25
Nu = 120; lam_u = 0.03; Lu = Nu*lam_u

u3d = U3DFromRaw(Nu, lam_u, filename, Nh, ratio = [1e-3, 1],
                 floor_cut = 1)
#u3d.plot()
u3d.fieldIntegral()
u3d.phaseIntegral()
#%%
zI = u3d.zI
I1 = u3d.I1
I2 = u3d.I2

PA = u3d.PA
SA = u3d.SA

PE = u3d.PE

reset_margin(bottom = 0.125, top = 0.95, left = 0.1, right = 0.95)

fig, ax = plt.subplots(figsize = (9, 4))
ax.plot(zI, I1*1e3, '-')
ax.grid()
ax.set_xlabel(r'$z$ (m)')
ax.set_ylabel(r'$I_1(z)$ (T mm)')
ax.set_xlim(-Lu/2, Lu/2)
#ax.set_ylim(-2.0, 2.0)

fig.savefig(basename+'-I1-z.eps')


fig, ax = plt.subplots(figsize = (9, 4))
ax.plot(zI, I2*1e6, '-')
ax.grid()
ax.set_xlabel(r'$z$ (m)')
ax.set_ylabel(r'$I_2(z)$ (T mm$^2$)')
ax.set_xlim(-Lu/2, Lu/2)
#ax.set_ylim(-2.0, 2.0)

fig.savefig(basename+'-I2-z.eps')


fig, ax = plt.subplots(figsize = (9, 4))
ax.plot(zI, PA/2./np.pi, '-'); print(PA[-1]/2./np.pi)
ax.grid()
ax.set_xlabel(u_z)
ax.set_ylabel(r'phase advance / $2\pi$')
#ax.set_xlim(-Lu/2, Lu/2)
#ax.set_ylim(0, 10)

fig.savefig(basename+'-PA-z.eps')


fig, ax = plt.subplots(figsize = (9, 4))
ax.plot(zI, SA*1e3, '-')
ax.grid()
ax.set_xlabel(u_z)
ax.set_ylabel(r'slippage (mm)'); print(SA[-1]*1e3/0.1)
ax.set_xlim(-Lu/2, Lu/2)
#ax.set_ylim(0, 10)

fig.savefig(basename+'-PATH-z.eps')


fig, ax = plt.subplots(figsize = (9, 4))
ax.plot(PE[:,0], PE[:,1]*180/np.pi, '-'); 
print('RMS Phase jitter: ', np.std(PE[:,1]*180/np.pi))
ax.grid()
ax.set_xlabel(u_z)
ax.set_ylabel(r'phase jitter (deg)')
ax.set_xlim(-Lu/2, Lu/2)
#ax.set_ylim(0, 10)
fig.savefig(basename+'-PE-z.eps')

Bmax = u3d.Bmax[20:-20]
Bavg = np.mean(Bmax[:,1])
Bstd = np.std(Bmax[:,1]); print('RMS field error: ', Bstd/Bavg*100, ' %')

fig, ax = plt.subplots(figsize = (9, 4))
ax.plot(Bmax[:,0], (Bmax[:,1]-Bavg)/Bavg*100, '-')
ax.set_xlim(-Lu/2, Lu/2)
ax.set_ylim(-1, 1)
ax.grid()
ax.set_xlabel(r'$z$ (m)')
ax.set_ylabel(r'$\Delta B/B$ (%)')
fig.savefig(basename+'-dB-B.eps')

reset_margin()

#%%
reset_margin(bottom = 0.125, top = 0.95, left = 0.1, right = 0.95)

fig, ax = plt.subplots(figsize = (7.2, 3.2))
ax.plot(slac26.zI, slac26.I1*1e3, '-')
ax.plot(desy26.zI, desy26.I1*1e3, '-')
ax.plot(slac07.zI, slac07.I1*1e3, '-')
ax.grid()
ax.set_xlabel(r'$z$ (m)')
ax.set_ylabel(r'$I_1(z)$ (T mm)')
ax.set_xlim(-Lu/2, Lu/2)
ax.set_ylim(-10, 10)
ax.legend([r'SLAC26', r'DESY26', r'SLAC07'],
          ncol = 3)
fig.savefig('I1-z-07vs26.eps')


fig, ax = plt.subplots(figsize = (7.2, 3.2))
ax.plot(slac26.zI, slac26.I2*1e6, '-')
ax.plot(desy26.zI, desy26.I2*1e6, '-')
ax.plot(slac07.zI, slac07.I2*1e6, '-')
ax.grid()
ax.set_xlabel(r'$z$ (m)')
ax.set_ylabel(r'$I_2(z)$ (T mm$^2$)')
ax.set_xlim(-Lu/2, Lu/2)
ax.set_ylim(-300, 300)
ax.legend([r'SLAC26', r'DESY26', r'SLAC07'],
          ncol = 3)
fig.savefig('I2-z-07vs26.eps')


fig, ax = plt.subplots(figsize = (7.2, 3.2))
ax.plot(slac26.PE[:,0], slac26.PE[:,1]*180/np.pi, '-'); 
ax.plot(desy26.PE[:,0], desy26.PE[:,1]*180/np.pi, '-'); 
ax.plot(slac07.PE[:,0], slac07.PE[:,1]*180/np.pi, '-'); 
print('RMS Phase jitter: ', np.std(slac26.PE[:,1]*180/np.pi))
print('RMS Phase jitter: ', np.std(desy26.PE[:,1]*180/np.pi))
print('RMS Phase jitter: ', np.std(slac07.PE[:,1]*180/np.pi))
ax.grid()
ax.set_xlabel(u_z)
ax.set_ylabel(r'phase jitter (deg)')
ax.set_xlim(-Lu/2, Lu/2)
ax.set_ylim(-15, 15)
ax.legend([r'SLAC26', r'DESY26', r'SLAC07'],
          ncol = 3)
fig.savefig('PE-z-07vs26.eps')


fig, ax = plt.subplots(figsize = (7.2, 3.2))
Bmax = slac26.Bmax[20:-20]
Bavg = np.mean(Bmax[:,1])
Bstd = np.std(Bmax[:,1]); print('RMS field error: ', Bstd/Bavg*100, ' %')
ax.plot(Bmax[:,0], (Bmax[:,1]-Bavg)/Bavg*100, '-*')
Bmax = desy26.Bmax[20:-20]
Bavg = np.mean(Bmax[:,1])
Bstd = np.std(Bmax[:,1]); print('RMS field error: ', Bstd/Bavg*100, ' %')
ax.plot(Bmax[:,0], (Bmax[:,1]-Bavg)/Bavg*100, '-*')
Bmax = slac07.Bmax[20:-20]
Bavg = np.mean(Bmax[:,1])
Bstd = np.std(Bmax[:,1]); print('RMS field error: ', Bstd/Bavg*100, ' %')
ax.plot(Bmax[:,0], (Bmax[:,1]-Bavg)/Bavg*100, '-*')
ax.set_xlim(-Lu/2, Lu/2)
ax.set_ylim(-1, 1)
ax.grid()
ax.set_xlabel(r'$z$ (m)')
ax.set_ylabel(r'$\Delta B/B$ (%)')
ax.legend([r'SLAC26', r'DESY26', r'SLAC07'],
          ncol = 3, loc = 'upper right')
fig.savefig('dB-B-07vs26.eps')

reset_margin()
#%%
NumberOfHarmonics = 25
# set the undulator parameters
N_u = 120; lam_u = 0.03; L_u = N_u*lam_u

ratio_z = 1
filename = 'slac26.txt'
#filename = 'slacBySym6.txt'
#filename = 'fld26.txt'
_, basename, ext = fileparts(filename)

data = np.loadtxt(filename)

z = data[:,0]/ratio_z; 
By = data[:,1]; dz = z[1]-z[0]
Bx = data[:,2];

# find the rough center
z0 = np.sum(z*np.abs(By))/np.sum(np.abs(By))
z = z-z0

#fig, ax = plt.subplots(figsize = (12, 4))
#ax.plot(z, By, '-')
#ax.grid()


# find exact center where By=0
for _ in np.arange(2):
    index, _ = index_min(np.abs(z))
    Nz_quater = int(lam_u/4./dz)
    fz_By = interp1d(By[index-Nz_quater:index+Nz_quater], z[index-Nz_quater:index+Nz_quater])
    z00 = fz_By(0); print('z0 = ', z00)
    z = z-z00

#ax.plot(z, By, '-')
#ax.set_xlim(-0.01, 0.01)

select = (z<=L_u/2.)*(z>=-L_u/2.)
z = z[select]
By = By[select]
Bx = Bx[select]
z2 = z
By02 = By
Bx02 = Bx
#%%
# Fourier analysis
nharm = np.arange(0, N_u*NumberOfHarmonics+1)

k_n = 2.*np.pi*nharm/L_u

xi = 0
ky_n = k_n/np.sqrt(1+xi**2)
kx_n = xi*ky_n

a_0 = np.sum(By*dz)/L_u; print('a_0: ', a_0)
#b_0 = 0

a_n = [2./L_u*np.sum(By*np.cos(k_n[i]*z)*dz) for i in nharm]
b_n = [2./L_u*np.sum(By*np.sin(k_n[i]*z)*dz) for i in nharm]
    
a_n = np.array(a_n); a_n[0] = a_n[0]/2.; #print a_n[0]
b_n = np.array(b_n); #print b_n[0]

spectrum = np.sqrt(a_n**2+b_n**2)

mpl.rc('figure.subplot', bottom = 0.1, top = 0.95, left = 0.1, right = 0.95)
fig, ax = plt.subplots(figsize = (9, 4))
ax.plot(1.0*nharm/N_u, spectrum, '-')
ax.set_yscale('log')
ax.set_xlim(0, NumberOfHarmonics)
ax.set_xticks(np.arange(NumberOfHarmonics+1))
ax.set_ylim(1e-10, 1e5)
ax.set_yticks([1e-10, 1e-5, 1e0, 1e5])
ax.grid()
ax.set_xlabel(r'$n/N_{\rm u}$')
ax.set_ylabel(r'$(a_n^2+b_n^2)^{1/2}$')

fig.savefig(basename+'_ham.eps')

#ham = np.concatenate((1.0*nharm/N_u, spectrum)).reshape((2, len(spectrum))).T
#np.savetxt(basename+'_ham.txt', ham, fmt = '%12.6E')
#%%
a_n[0] = 0
def EM3D(x, y, z, t=0):
    
    if np.isscalar(x):
        x = [x]
    if np.isscalar(y):
        y = [y]
    if np.isscalar(z):
        z = [z]
    x = np.asarray(x).flatten()
    y = np.asarray(y).flatten()
    z = np.asarray(z).flatten()
    
    nn = np.max([x.size, y.size, z.size])
    
    if x.size == 1:
        x = np.ones(nn)*x[0]
    if y.size == 1:
        y = np.ones(nn)*y[0]
    if z.size == 1:
        z = np.ones(nn)*z[0]
    
    if not np.isscalar(t):
        t = np.asarray(t)
        
    #Ex, Ey, Ez = 0, 0, 0
    #Bx, By, Bz = 0, 0, 0
    #By = np.sum(( a_n*np.cos(k_n*z)+b_n*np.sin(k_n*z))*np.cosh(k_n*y))
    #Bz = np.sum((-a_n*np.sin(k_n*z)+b_n*np.cos(k_n*z))*np.sinh(k_n*y))
    
    z1 = z #-z0
    B1y = [np.sum(( a_n*np.cos(k_n*z1[i])+b_n*np.sin(k_n*z1[i]))*np.cosh(k_n*y[i])) for i in np.arange(nn)]
    B1z = [np.sum((-a_n*np.sin(k_n*z1[i])+b_n*np.cos(k_n*z1[i]))*np.sinh(k_n*y[i])) for i in np.arange(nn)]
    
    B1y = np.where(z1<-L_u/2., 0, B1y)
    B1y = np.where(z1> L_u/2., 0, B1y)
    
    B1z = np.where(z1<-L_u/2., 0, B1z)
    B1z = np.where(z1> L_u/2., 0, B1z)
    
    F2d = np.zeros((nn, 6))
    F2d[:,4] = B1y
    F2d[:,5] = B1z

    return F2d

z1 = z
By1 = EM3D(0, 0, z)[:,4]
#%%
mpl.rc('figure.subplot', bottom = 0.1, top = 0.95, left = 0.1, right = 0.95)
fig, ax = plt.subplots(figsize = (9, 4))
ax.plot(z1, By1, '-')
ax.plot(z1, (By1-By)*1e3, '-')
ax.grid()
ax.set_xlabel(r'$z$ (m)')
ax.set_ylabel(r'$B_y(z)$ (T)')
ax.set_xlim(-1.8, 1.8)
ax.set_ylim(-2.0, 2.0)

ax.legend([r'$B_{y1}$', r'$[B_{y1}-B_y^{\rm DESY26}]\times 10^3$'], ncol = 2)

#fig.savefig(basename+'-By-z.eps')
mpl.rc('figure.subplot', bottom = 0.15, top = 0.9, left = 0.15, right = 0.9)

#%%

fBy1 = interp1d(desy26.z, desy26.By, bounds_error = False, fill_value = 0)
fBy2 = interp1d(slac26.z, slac26.By, bounds_error = False, fill_value = 0)

mpl.rc('figure.subplot', bottom = 0.125, top = 0.95, left = 0.1, right = 0.95)
fig, ax = plt.subplots(figsize = (7.2, 3.2))
ax.plot(desy26.z, desy26.By, '-')
#ax.plot(slac26.z, slac26.By, '-')
ax.plot(desy26.z, (desy26.By-fBy2(desy26.z))*1e1, '-')
ax.grid()
ax.set_xlabel(r'$z$ (m)')
ax.set_ylabel(r'$B_y(z)$')
ax.set_xlim(-1.8, 1.8)
ax.set_ylim(-2.0, 2.0)


ax.legend([r'$B_{y}^{\rm DESY}$', r'$[B_{y}^{\rm DESY}-B_{y}^{\rm SLAC}]\times$',
           r'$B_{y}^{\rm DESY}$'], 
          ncol = 3, loc = 'upper right', fontsize = 11)

fig.savefig('Bx-z-26vs26.eps')
mpl.rc('figure.subplot', bottom = 0.15, top = 0.9, left = 0.15, right = 0.9)

#%%
mpl.rc('figure.subplot', bottom = 0.1, top = 0.95, left = 0.1, right = 0.95)
fig, ax = plt.subplots(figsize = (9, 4))

ham1 = np.loadtxt('slac26_ham.txt')
ham2 = np.loadtxt('fld26_ham.txt')
ham3 = np.loadtxt('slacBySym6_ham.txt')

ax.plot(ham1[:,0], ham1[:,1], '-')
ax.plot(ham2[:,0], ham2[:,1], '-')
ax.plot(ham3[:,0], ham3[:,1], '-')

ax.set_yscale('log')
ax.set_xlim(0, NumberOfHarmonics)
ax.set_xticks(np.arange(NumberOfHarmonics+1))
#ax.set_xlim(0, 3)
ax.set_ylim(1e-10, 1e1)
ax.set_yticks([1e-10, 1e-5, 1e0])
ax.grid()
ax.set_xlabel(r'$n/N_{\rm u}$')
ax.set_ylabel(r'$(a_n^2+b_n^2)^{1/2}$')

ax.legend([r'SLAC26', r'DESY26', r'SLAC07'],
          ncol = 3)

fig.savefig('ham3.eps')
mpl.rc('figure.subplot', bottom = 0.15, top = 0.9, left = 0.15, right = 0.9)


#%%
fBy1 = interp1d(z1, By01, bounds_error = False, fill_value = 0)
fBy2 = interp1d(z2, By02, bounds_error = False, fill_value = 0)

mpl.rc('figure.subplot', bottom = 0.1, top = 0.95, left = 0.1, right = 0.95)
fig, ax = plt.subplots(figsize = (6, 4))
ax.plot(z1, By01, '-')
ax.plot(z2, By02, '-')
ax.plot(z2, (By02-fBy1(z2))*1e1, '-')
ax.grid()
ax.set_xlabel(r'$z$ (m)')
ax.set_ylabel(r'$B_y(z)$ (T)', labelpad=-3)
ax.set_xlim(-1.51, -1.42)
#ax.set_xlim(-1.8, 1.8)
#ax.set_xlim(-1.8, -1.5)
#ax.set_xlim(1.5, 1.8)
ax.set_ylim(-2.0, 2.0)

ax.legend([r'$B_{y}^{\rm SLAC26}$', r'$B_y^{\rm DESY26}$',
           r'$[B_y^{\rm DESY26}-B_y^{\rm SLAC26}]\times 10$'],
          ncol = 3, fontsize = 11)

fig.savefig('By0-z26vs26_local0.eps')
mpl.rc('figure.subplot', bottom = 0.15, top = 0.9, left = 0.15, right = 0.9)


#%%
fBx1 = interp1d(z1, Bx01, bounds_error = False, fill_value = 0)
fBx2 = interp1d(z2, Bx02, bounds_error = False, fill_value = 0)

mpl.rc('figure.subplot', bottom = 0.125, top = 0.95, left = 0.1, right = 0.95)
fig, ax = plt.subplots(figsize = (9, 4))
ax.plot(z1, Bx01*1e3, '-')
ax.plot(z2, Bx02*1e3, '--')
ax.plot(z2, (Bx02-fBx1(z2))*1e3, '-')
ax.grid()
ax.set_xlabel(r'$z$ (m)')
ax.set_ylabel(r'$B_x(z)$ (mT)', labelpad=-3)
ax.set_xlim(-1.8, 1.8)
#ax.set_xlim(-1.8, -1.5)
#ax.set_xlim(1.5, 1.8)
ax.set_ylim(-10, 5)

ax.legend([r'$B_x^{\rm SLAC26}$', r'$B_x^{\rm DESY26}$',
           r'$[B_x^{\rm DESY26}-B_x^{\rm SLAC26}]$'],
          ncol = 3, loc = 'upper right')

fig.savefig('Bx0-z26vs26.eps')
mpl.rc('figure.subplot', bottom = 0.15, top = 0.9, left = 0.15, right = 0.9)

#%%
data = np.loadtxt('fld26_taper.txt')

fig, ax = plt.subplots(figsize = (4, 3))
ax.plot(data[:,0], data[:,1])

ax.set_xlabel(r'$x$ (mm)')
ax.set_ylabel(r'$K$')

ax.grid()

popt, pcov = curve_fit(linear, data[:,0], data[:,1])

ax.plot(data[:,0], linear(data[:,0], *popt))

ax.legend(['$K-x$', 'linear fit'])

fig.savefig('fld26-K-x.eps')

#%%
und1 = Magnet3D('3DDESY26-20x10mm-uniform', 1)
und2 = Magnet3D('3DDESY26-20x10mm', 1)
coil = Magnet3D('3DCoil+iron-20x10mm', 1)

#%%
zz = np.linspace(-1.8, 1.8, 10000)

By1 = und1.EM3D(0, 0, zz)[:,4]
By2 = und2.EM3D(0, 0, zz)[:,4]

fig, ax = plt.subplots(figsize = (6, 3))
ax.plot(zz, By2)
ax.grid()
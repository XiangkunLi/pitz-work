# -*- coding: utf-8 -*-
"""
Created on Mon Feb 24 15:19:58 2020

@author: lixiangk
"""

from Beam import *
beam = Beam(r'\\afs\ifh.de\group\pitz\data\lixiangk\sim\2020\THz\20200211N\emission2\D-4.00mm-Q-4000pC\trk.0005.001')
#%%
beam.get_sc3d()

#%%
from SpaceCharge3DFFT import *
sc3d = SpaceCharge3DFFT()
sc3d.get_rho_3D(beam.dist)
sc3d.build()

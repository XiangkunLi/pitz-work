# -*- coding: utf-8 -*-
"""
Created on Wed Mar 10 16:31:28 2021

@author: lixiangk
"""

from universal import *

A2S = lambda A, nu_TDS = 2.998e9: 1e-3*A*360*nu_TDS/3e8

phase = np.arange(-99, -92)
y0scan = np.array([-4.0029,   -3.4415,   -2.3707,   -1.5307,   -1.7217,   -1.1445,   -0.6124])

#y0scan = np.array([3.3933,    3.4227,    3.1016,    2.4800,    1.8958,    0.8720,   -0.0427])

p0 = phase
y0 = y0scan-y0scan[0]

nTrials = 1
maxTrials = 1

popt, pcov = curve_fit(linear, y0, p0)
#p0 = linear(y0, *popt)
k0 = popt[0]

while nTrials<maxTrials:
    nTrials += 1
    popt, pcov = curve_fit(linear, y0, p0)
    p0 = linear(y0, *popt)
    k0 = popt[0]
    

fig, ax = plt.subplots()
ax.plot(phase, y0scan-y0scan[0], 'D')
ax.plot(p0, y0, '<-')
ax.plot(linear(y0, *popt), y0, '-')

print('S parameter: ', A2S(1/k0))

#%%

data = np.loadtxt('TDS4.txt')

fig, ax = plt.subplots()
ax.plot(data[:,0], data[:,1]*data[:,2], 'rD')
popt, pcov = curve_fit(linear, data[:,0], data[:,1]*data[:,2])
tmp = np.linspace(0, np.max(data[:,0])*1.1)
ax.plot(tmp, linear(tmp, *popt), 'r--')

ax.plot(data[:,0], data[:,3]*data[:,4], 'bo')
popt, pcov = curve_fit(linear, data[:,0], data[:,3]*data[:,4])
tmp = np.linspace(0, np.max(data[:,0])*1.1)
ax.plot(tmp, linear(tmp, *popt), 'b--')

ax.set_xlabel('TDS SP')
ax.set_ylabel('FWHM*S (ps)')
ax.set_xlim(0, 0.15)
ax.set_xticks([0, 0.05, 0.10, 0.15])
ax.grid()

ax.legend(['phase scan 1', 'fit 1', 'phase scan 2', 'fit 2'])
fig.savefig('TDS4.png')


#%%
workdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim\2020\Shift\20210303N'
os.chdir(workdir)

data = np.loadtxt('TDS.txt')
x = np.arange(1, 9)

fig, ax = plt.subplots(figsize = (5, 3))

ax.errorbar(x, data[:,1], data[:,2])
ax.errorbar(x, data[:,3], data[:,4])

ax.set_xlabel('# of measurement')
ax.set_ylabel('FWHM (ps)')
ax.grid()

fig.savefig('FWHM.png')

fig, ax = plt.subplots(figsize = (5, 3))

#data[0:4,5:9] *= (0.5/0.7)
ax.errorbar(x, data[:,5], data[:,6])
ax.errorbar(x, -data[:,7], data[:,8])

ax.set_xlabel('# of measurement')
ax.set_ylabel('S-parameter')
ax.grid()

fig.savefig('S-parameter.png')
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 29 15:31:26 2020

@author: lixiangk
"""

#from platypus import *
from algorithms.NSGAII_resume import *

import timeit

if __name__ == "__main__":
    # define the problem definition
    #problem = Problem(4, 3)
    #problem.types[0] = Real(2.4, 4.8)
    #problem.types[1] = Real(-15, 15)
    #problem.types[2] = Real(-30, -5)
    #problem.types[3] = Real(340, 380)
    #problem.function = obj_EMSY4000pC_NSGAII_60um

    # or define the problem definition
    problem = DTLZ2()
    
    # define the generator for initialization
    #generator = GeneratorFromFile('result@0569.001') # Continue after the first run
    #population = [generator.generate(problem) for _ in range(95)]
    
    time1 = timeit.default_timer()
    
    # instantiate the optimization algorithm to run in parallel
    #with PoolEvaluator(pool) as evaluator:
    with ProcessPoolEvaluator(4) as evaluator:
        
        #algorithm = NSGAII_resume(problem, evaluator=evaluator, population_size = 95)

        algorithm = initialize_algorithm('algorithm@0287.001.pkl')
        algorithm.evaluator = evaluator
        algorithm.nth_run = 3

        #algorithm.dump()
        
        algorithm.run(200)
    
    # display the results
    for solution in algorithm.result:
        print(solution.objectives)

    time2 = timeit.default_timer()
    print('time elapsed: ', time2-time1)
    
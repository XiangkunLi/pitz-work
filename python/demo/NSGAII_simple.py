# -*- coding: utf-8 -*-
"""
Created on Mon Jun 29 11:05:06 2020

@author: lixiangk
"""

#from platypus import *
from algorithms.NSGAII_resume import *

# define the problem definition
problem = DTLZ2(3)

# instantiate the optimization algorithm
algorithm = NSGAII_resume(problem, divisions_outer = 24)

# optimize the problem using 10,000 function evaluations
algorithm.run(5000)

# plot the results using matplotlib
import matplotlib.pyplot as plt

plt.figure()
plt.scatter([s.objectives[0] for s in algorithm.result],
            [s.objectives[1] for s in algorithm.result])
plt.xlim([0, 1.1])
plt.ylim([0, 1.1])
plt.xlabel("$f_1(x)$")
plt.ylabel("$f_2(x)$")
plt.show()

#%% Example
import logging
logging.basicConfig(level=logging.INFO)

if __name__ == "__main__":
    # define the problem definition
    #problem = Problem(3, 4)
    #problem.types[0] = Real(0, 4)
    #problem.types[1] = Real(-4, 0)
    #problem.types[2] = Real(0, 4)
    #problem.function = obj_run_sco

    #from platypus import NSGAII, DTLZ2
    problem = DTLZ2()

    # define the generator for initialization
    #generator_init = GeneratorFromFile('result@.003') # Continue after the first run
    
    
    time1 = timeit.default_timer() 
    
    # initialize here from zero
    #algorithm = NSGAIII_resume(problem, population_size = 100)
    ##algorithm.initialize_to_resume(generator_init)
    
    # or initialize from a saved archive
    algorithm = initialize_algorithm('algorithm@2004.001.pkl')
    algorithm.evaluator = default_evaluator
    
    # reset run number
    algorithm.nth_run = 2

    #retrieve the optimization process
    #algorithm.generator = generator_init

    #algorithm.dump()
    
    algorithm.run(2000)
    
    # display the results
    for solution in algorithm.result:
        print(solution.objectives)


    time2 = timeit.default_timer()
    print('time elapsed: ', time2-time1)

#%%
from mpl_toolkits.mplot3d import Axes3D

result = algorithm.result

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter([s.objectives[0] for s in result],
           [s.objectives[1] for s in result],
           [s.objectives[2] for s in result])
ax.set_title(algorithm)
ax.set_xlim([0, 1.1])
ax.set_ylim([0, 1.1])
ax.set_zlim([0, 1.1])
ax.view_init(elev=30.0, azim=15.0)
ax.locator_params(nbins=4)


fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter([s.variables[0] for s in result],
           [s.variables[1] for s in result],
           [s.variables[2] for s in result])
ax.set_title(algorithm)
ax.set_xlim([0, 1.1])
ax.set_ylim([0, 1.1])
ax.set_zlim([0, 1.1])
ax.view_init(elev=30.0, azim=15.0)
ax.locator_params(nbins=4)
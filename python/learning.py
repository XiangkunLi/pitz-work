# -*- coding: utf-8 -*-
"""
Created on Thu Jul  2 14:02:36 2020

@author: lixiangk
"""

import numpy as np
from keras.models import load_model

from sklearn import preprocessing

# Load data

data = None

# Normalize data

data_normaliser = preprocessing.MinMaxScaler()
data_normalised = data_normaliser.fit_transform(data)

y_normaliser = preprocessing.MinMaxScaler()
y_normaliser.fit(next_day_open_values)

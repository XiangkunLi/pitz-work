# -*- coding: utf-8 -*-
"""
Created on Fri Feb 28 17:20:23 2020

@author: lixiangk
"""

from superradiance import *

color_cycle = ['r', 'b', 'g', 'c', 'm', 'y', 'k', 'n', 'o']
linestyle_cycle = ['-', '--', '-.', ':', (0, (5, 2, 5, 2)), (0, (10, 2, 5, 2, 2, 2)), (0, (12, 2, 2, 2))]
marker_cycle = ['o', 'd', 'v', '^', '<', '>', '*']

def plot_config(*args, **kwargs):
    from cycler import cycler
    from matplotlib.ticker import AutoMinorLocator

    fsize = 14 # a quarter of the paper width: 20 pt; half of the paper width: 12
    font = {'size' : fsize, 'family' : 'serif'}
    color_cycle = ['r', 'b', 'g', 'c', 'm', 'y', 'k']
    linestyle_cycle = ['-', '--', '-.', ':', (0, (5, 2, 5, 2)), (0, (10, 2, 5, 2, 2, 2)), (0, (12, 2, 2, 2))]
    marker_cycle = ['o', 'd', 'v', '^', '<', '>', '*']
    
    mpl.rc('font', **font)
    mpl.rc('xtick', labelsize = 12, direction = 'in', top   = True)
    mpl.rc('ytick', labelsize = 12, direction = 'in', right = True)
    mpl.rc('xtick.major', size = 5, width = 1)
    mpl.rc('ytick.major', size = 5, width = 1)
    mpl.rc('xtick.minor', size = 3, width = 0.7, visible = True)
    mpl.rc('ytick.minor', size = 3, width = 0.7, visible = True)
    
    mpl.rc('lines', linewidth=2, markersize=6, color='r')
    # mpl.rc('lines', linestyle = 'solid')
    mpl.rc('axes', labelpad = 0,\
           prop_cycle=(cycler('color', color_cycle) + cycler('linestyle', linestyle_cycle) + cycler('marker', marker_cycle)))
    mpl.rc('legend', fontsize = 12, labelspacing = 0.05, handletextpad=0.4, frameon=False, handlelength=2.1)
    
    mpl.rc('figure', dpi = 100, figsize = (4, 4))
    mpl.rc('figure.subplot', bottom = 0.15, top = 0.9, left = 0.15, right = 0.9)
    
    mpl.rc('image', cmap = 'jet')
    
    mpl.RcParams(*args, **kwargs)
    
    return
plot_config()

#%%
B = np.linspace(0.5, 1.5)
K = np.linspace(1, 4)
J2 = JJ2(K)**(1.0/3)

fig, ax = plt.subplots()
ax.plot(K, J2, '-')
#%% Example
lam_u, Nu = 40e-3, 100

Ek = 30
gamma = 1+Ek/g_mec2

freq = np.linspace(1, 30, 100)*1e12
lam_s = g_c/freq

K = resonant_undulator_parameter(lam_s, lam_u, gamma)
B = undulator_field(K, lam_u)

fig, ax = plt.subplots()

ax.plot(freq/1e12, B, '-')
ax.grid()

ax.set_xlabel(r'Frequency (THz)')
ax.set_ylabel(r'Magnetic field (T)')

#%%
lam_u, Nu = 40e-3, 100

Ek = 30
gamma = 1+Ek/g_mec2

freq = np.linspace(1, 30, 100)*1e12
lam_s = g_c/freq

K = undulator_parameter(1, lam_u)
gamma = resonant_energy(K, lam_u, lam_s)

fig, ax = plt.subplots()

ax.plot(freq/1e12, g_mec2*(gamma-1), '-')
ax.grid()

ax.set_xlabel(r'Frequency (THz)')
ax.set_ylabel(r'Kinetic energy (MeV)')

#%% Calculate the FEL parameter, gain length and cooperation length
lam_u, Nu = 100e-3, 40

Ek = 30
gamma = 1+Ek/g_mec2

freq = 10e12
lam_s = g_c/freq

K = resonant_undulator_parameter(lam_s, lam_u, gamma)
B = undulator_field(K, lam_u)

und = Undulator(lam_u = lam_u, Nu = Nu, K = K)

Ipeak = np.linspace(100, 200, 101)
Qtot = [1e-9, 2e-9, 3e-9]

res = []
for I in Ipeak:
    for Q in Qtot:
        
        ss = SASE(Q, I, dist = 'Gaussian', und = und, lam_s = lam_s, sig_x = 1e-3)
        
        pp = Q*Ek*1e6*ss.rho
        res.append([I, Q, ss.rho, ss.Lg, ss.Lc, pp])
        
res = np.array(res)

# Make plot
fig, [ax1, ax2, ax3] = plt.subplots(ncols = 3, figsize = (12, 4))

Qtot = [1e-9, 2e-9, 3e-9]

for Q in Qtot:
    select = np.abs(1-res[:,1]/Q)<0.1
    
    d1 = res[select]
    
    ax1.plot(d1[:,0], d1[:,4]*1e6)
    ax2.plot(d1[:,0], d1[:,3]*1e3)
    ax3.plot(d1[:,0], d1[:,5]*1e6)

ax1.set_xlabel(r'$I_{\rm peak}$ (A)')
ax1.set_ylabel(r'Cooperation length ($\mu$m)')

ax2.set_xlabel(r'$I_{\rm peak}$ (A)')
ax2.set_ylabel(r'Gain length (mm)')

ax3.set_xlabel(r'$I_{\rm peak}$ (A)')
ax3.set_ylabel(r'FEL parameter (%)')

fig.tight_layout()

#%%
lam_u, Nu = 100e-3, 100

Ek = 30
gamma = 1+Ek/g_mec2

freq = np.linspace(1, 30, 101)*1e12
lam_s = g_c/freq

res = []
for ll in lam_s:
    Q = 1e-9
    I = 200
    
    K = resonant_undulator_parameter(ll, lam_u, gamma)
    B = undulator_field(K, lam_u)
    und = Undulator(lam_u = lam_u, Nu = Nu, K = K)
    
    ss = SASE(Q, I, dist = 'Gaussian', und = und, lam_s = ll, sig_x = 1e-3)
    
    pp = Q*Ek*1e6*ss.rho
    res.append([ll, Q, ss.rho, ss.Lg, ss.Lc, pp])
    
res = np.array(res)

d1 = res

#%% Make plot
fig, [ax1, ax2, ax3] = plt.subplots(ncols = 3, figsize = (12, 4))

ax1.plot(freq/1e12, d1[:,2]*1e2, '-')
ax2.plot(freq/1e12, 22*d1[:,3], '-')
ax3.plot(freq/1e12, d1[:,5]*1e6, '-')

ax1.plot(freq/1e12, d2[:,2]*1e2, '-')
ax2.plot(freq/1e12, 22*d2[:,3], '-')
ax3.plot(freq/1e12, d2[:,5]*1e6, '-')

ax1.plot(freq/1e12, d3[:,2]*1e2, '-')
ax2.plot(freq/1e12, 22*d3[:,3], '-')
ax3.plot(freq/1e12, d3[:,5]*1e6, '-')

ax1.legend(['30 mm', '40 mm', '50 mm'])

ax1.set_xlabel(r'$f$ (THz)')
ax1.set_ylabel(r'FEL parameter (%)')

ax2.set_xlabel(r'$f$ (THz)')
ax2.set_ylabel(r'22 $\times$ Gain length (m)')

ax3.set_xlabel(r'$f$ (THz)')
ax3.set_ylabel(r'THz energy ($\mu$J)')

fig.tight_layout()

#%%
Nu = 100

lam_u = np.array([30, 40, 50])*1e-3

freq_s = np.linspace(1, 20, 50)*1e12
lam_s = g_c/freq_s

Ekin = np.linspace(15, 35, 100)

I, Q = 200, 2e-9

data = []
res = []
for lu in lam_u:
    for ll in lam_s:
        temp = []
        for Ek in Ekin:
            gamma = 1+Ek/g_mec2
            K = resonant_undulator_parameter(ll, lu, gamma)
            B = undulator_field(K, lu)

            und = Undulator(lam_u = lu, Nu = Nu, K = K)

            ss = SASE(Q, I, dist = 'Gaussian', und = und, lam_s = ll, sig_x = 1e-3)
        
            pp = Q*Ek*1e6*ss.rho
            
            temp.append([I, Q, lu, ll, Ek, ss.rho, ss.Lg, ss.Lc, pp, B])
            data.append([I, Q, lu, ll, Ek, ss.rho, ss.Lg, ss.Lc, pp, B])
            
        temp = np.array(temp)
        idx, v = index_min(temp[:, 6]); print(idx, v)
        
        res.append(list(temp[idx,:]))
        
res = np.array(res)
data = np.array(data)

ss = res[:,5]>0
res = res[ss]

ss = data[:,5]>0
data = data[ss]

# Make plot
fig, [ax1, ax2, ax3] = plt.subplots(ncols = 3, figsize = (12, 4))

for v in lam_u:
    select = np.abs(1-data[:,2]/v)<0.1
    
    d1 = data[select]
    
    ax1.plot(g_c/d1[:,3]/1e12, d1[:,5]*1e2, '.', markersize = 0.75)
    ax2.plot(g_c/d1[:,3]/1e12, 22*d1[:,6], '.', markersize = 0.75)
    ax3.plot(g_c/d1[:,3]/1e12, d1[:,8]*1e6, '.', markersize = 0.75)

ax1.set_xlabel(r'$f$ (THz)')
ax1.set_ylabel(r'FEL parameter (%)')

ax2.set_xlabel(r'$f$ (THz)')
ax2.set_ylabel(r'22 $\times$ Gain length (m)')

ax3.set_xlabel(r'$f$ (THz)')
ax3.set_ylabel(r'THz energy ($\mu$J)')

ax1.legend(['30 mm', '40 mm', '50 mm'])
ax2.legend(['30 mm', '40 mm', '50 mm'])
ax3.legend(['30 mm', '40 mm', '50 mm'])

ax1.grid()
ax2.grid()
ax3.grid()

fig.tight_layout()

# Make plot
fig, [ax1, ax2] = plt.subplots(ncols = 2, figsize = (8, 4))

markers = ['.', '*', '+']
for i, v in enumerate(lam_u[1:2]):
    select = np.abs(1-data[:,2]/v)<0.1
    
    d1 = data[select]
    
    ax1.plot(g_c/d1[:,3]/1e12, d1[:,4], 'b.', markersize = 3)
    ax2.plot(g_c/d1[:,3]/1e12, d1[:,-1], 'b.', markersize = 3)

ax1.set_xlabel(r'$f$ (THz)')
ax1.set_ylabel(r'Kinetic energy (MeV)')

ax2.set_xlabel(r'$f$ (THz)')
ax2.set_ylabel(r'Peak magnetic field (T)')

ax1.legend(['40 mm', '40 mm', '50 mm'])
ax2.legend(['40 mm', '40 mm', '50 mm'])

ax1.grid()
ax2.grid()

fig.tight_layout()

#%%
# Make plot
fig, ax1 = plt.subplots(ncols = 1, figsize = (4, 4))

select = np.abs(1-data[:,2]/40e-3)<0.1

d1 = data[select]

ax1.plot(g_c/d1[:,3]/1e12, 22*d1[:,6], '.')

ax1.set_xlabel(r'$f$ (THz)')
ax1.set_ylabel(r'Peak magnetic field (T)')


fig.tight_layout()

#%%
Nu = 150

lam_u = np.array([36, 40, 44])*1e-3

freq_s = np.linspace(1, 15, 50)*1e12
lam_s = g_c/freq_s

Ekin = np.linspace(15, 35, 100)

I, Q = 200, 2e-9

data = []
res = []
for lu in lam_u:
    for ll in lam_s:
        temp = []
        for Ek in Ekin:
            gamma = 1+Ek/g_mec2
            K = resonant_undulator_parameter(ll, lu, gamma)
            B = undulator_field(K, lu)

            und = Undulator(lam_u = lu, Nu = Nu, K = K)

            ss = SASE(Q, I, dist = 'Gaussian', und = und, lam_s = ll, sig_x = 0.5e-3)
        
            pp = Q*Ek*1e6*ss.rho
            
            temp.append([I, Q, lu, ll, Ek, ss.rho, ss.Lg, ss.Lc, pp, B])
            data.append([I, Q, lu, ll, Ek, ss.rho, ss.Lg, ss.Lc, pp, B])
            
        temp = np.array(temp)
        ss = temp[:,5]>0
        temp = temp[ss]
        idx, v = index_min(temp[:, 6]); print(idx, v)
        
        res.append(list(temp[idx,:]))
        
res = np.array(res)
data = np.array(data)

#ss = res[:,5]>0
#res = res[ss]

#ss = data[:,5]>0
#data = data[ss]

#%% Make plot
fig, ax3 = plt.subplots(ncols = 1, figsize = (4, 4))

for v in lam_u:
    select = np.abs(1-res[:,2]/v)<0.01
    
    d1 = res[select]
    
    #ax1.plot(g_c/d1[:,3]/1e12, d1[:,5]*1e2, '-', markersize = 0.75)
    #ax2.plot(g_c/d1[:,3]/1e12, d1[:,6], '-', markersize = 0.75)
    ax3.plot(g_c/d1[:,3]/1e12, d1[:,8]*1e6, '-', markersize = 0.75)

#ax1.set_xlabel(r'$f$ (THz)')
#ax1.set_ylabel(r'FEL parameter (%)')
#ax1.legend(['36 mm', '40 mm', '44 mm'])
#ax1.grid()

#ax2.set_xlabel(r'$f$ (THz)')
#ax2.set_ylabel(r'Gain length (m)')
#ax2.legend(['36 mm', '40 mm', '44 mm'])
#ax2.grid()

ax3.set_xlabel(r'$f$ (THz)')
ax3.set_ylabel(r'THz energy ($\mu$J)')
ax3.legend(['36 mm', '40 mm', '44 mm'])
ax3.grid()

#fig.tight_layout()
fig.savefig('energy-vs-lam_s.eps')

#%%
Nu = 150

lam_u = 40e-3

freq_s = np.linspace(1, 15, 50)*1e12
lam_s = g_c/freq_s

Ekin = np.linspace(15, 35, 100)

Ipeak = np.linspace(100, 200, 50)

Q = 2e-9

data = []
res = []
for I in Ipeak:
    for ll in lam_s:   
        temp = []
        for Ek in Ekin:
            gamma = 1+Ek/g_mec2
            K = resonant_undulator_parameter(ll, lu, gamma)
            B = undulator_field(K, lu)

            lu = lam_u
            und = Undulator(lam_u = lu, Nu = Nu, K = K)
            
            ss = SASE(Q, I, dist = 'Gaussian', und = und, lam_s = ll,
                      sig_x = 0.5e-3)
            
            pp = Q*Ek*1e6*ss.rho
            
            temp.append([I, Q, lu, ll, Ek, ss.rho, ss.Lg, ss.Lc, pp, B])
            data.append([I, Q, lu, ll, Ek, ss.rho, ss.Lg, ss.Lc, pp, B])
            
        temp = np.array(temp)
        ss = temp[:,5]>0
        temp = temp[ss]
        idx, v = index_min(temp[:, 6]); print(idx, v)
        
        res.append(list(temp[idx,:]))
        
res = np.array(res)
data = np.array(data)

#ss = res[:,5]>0
#res = res[ss]

#ss = data[:,5]>0
#data = data[ss]
#%%
aX, aY = np.meshgrid(freq_s, Ipeak)
signal = np.reshape(res[:,6]*100, aX.shape)

fig, ax = plt.subplots(ncols = 1, figsize = (5, 4))

plottype = 'contourf'
if plottype is 'contourf':
    
    # Noise level for 2D plot
    noise = 0.3
    v = np.linspace(noise, 1, 99)*np.max(signal)
    v = np.linspace(np.min(signal), np.max(signal), 99)
    #v = np.linspace(0.05, 0.2, 99)
    
    cax = ax.contourf(aX/1e12, aY, signal, v, linestyles = None)#, zorder=-9)
    ax.contour(aX/1e12, aY, signal, 20, colors = 'k', linewidths = 0.5)
    #ax.set_rasterization_zorder(-1)
    ax.set_aspect('auto')
    
cbar = fig.colorbar(cax, fraction = 0.046, pad = 0.04)
cbar.set_ticks(np.linspace(0, 5, 6))
cbar.set_ticks(np.linspace(4, 20, 5))
cbar.ax.tick_params(labelsize = 12, pad = 2)
cbar.ax.set_ylabel(r'FEL parameter (%)')
cbar.ax.set_ylabel(r'Gain length (cm)')

ax.set_xlabel(r'$f$ (THz)', fontsize = 12)
ax.set_ylabel(r'$I_{\rm peak}$ (A)', fontsize = 12)

fig.savefig('Lg0-vs-Ipeak-f.eps')
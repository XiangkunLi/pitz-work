"""
image_process

Usage
-----
See the class ImageProcess
"""
# Make the ImageProcess object accessible from outside the package
from . import ImageProcess, PhaseSpace

# Define the version number
from .__version__ import __version__
__all__ = ['ImageProcess', 'PhaseSpace', '__version__']

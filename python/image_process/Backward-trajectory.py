# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 14:28:24 2021

@author: lixiangk
"""
from universal import *

class Steerer:
    def __init__(self, name, position, strength = 0, to_next = -1, to_last = -1, current = 0):
        self.name = name
        self.position = position
        self.strength = strength
        self.to_next = to_next
        self.to_last = to_last
        self.current = current
        
    def set(self, prop, val):
        self.__dict__[prop] = val

#%% x in 2021 in the night shift 20210301N
# Steerer strength: mrad * MeV/c / A
# The colomns are strength, current and drift length, respectively
cathode= Steerer('Cathode', 0, 0, 0.492, 0, current = 0)

lowst1 = Steerer('Low.ST1', 0.492, 7.5323, 0.963-0.492, 0.492, current = 1)
lowst2 = Steerer('Low.ST2', 0.963, 11.8958, 1.27-0.963, 0.963-0.492, current = -0.6)
lowst3 = Steerer('Low.ST3', 1.27, 21.9353, 1.463-1.27, 1.27-0.963, current = 1.20)
lowst4 = Steerer('Low.ST4', 1.463, 20.546, 1.986-1.463, 1.463-1.27, current = 0.0)
lowst5 = Steerer('Low.ST5', 1.986, 2.6246, 2.675-1.986, 1.986-1.463, current = 0)

st_list = [lowst5, lowst4, lowst3, lowst2, lowst1, cathode]

#%% x in 2021 in the morining shift 20210301M
# Steerer strength: mrad * MeV/c / A
# The colomns are strength, current and drift length, respectively
cathode= Steerer('Cathode', 0, 0, 0.492, 0, current = 0)

lowst1 = Steerer('Low.ST1', 0.492, 7.5323, 0.963-0.492, 0.492, current = 0.7)
lowst2 = Steerer('Low.ST2', 0.963, 11.8958, 1.27-0.963, 0.963-0.492, current = 0.75)
lowst3 = Steerer('Low.ST3', 1.27, 21.9353, 1.463-1.27, 1.27-0.963, current = 0.6)
lowst4 = Steerer('Low.ST4', 1.463, 20.546, 1.986-1.463, 1.463-1.27, current = 0.05)
lowst5 = Steerer('Low.ST5', 1.986, 2.6246, 2.675-1.986, 1.986-1.463, current = 4.7)

st_list = [lowst5, lowst4, lowst3, lowst2, lowst1, cathode]

#%% y in 2021 new
# Steerer strength: mrad * MeV/c / A
# The colomns are strength, current and drift length, respectively
lowst1 = Steerer('Low.ST1', 0.492, 7.5323, 0.963-0.492, 0.492, current = 0.150)
lowst2 = Steerer('Low.ST2', 0.963, 11.8958, 1.27-0.963, 0.963-0.492, current = 0)
lowst3 = Steerer('Low.ST3', 1.27, 21.9353, 1.463-1.27, 1.27-0.963, current = -0.150)
lowst4 = Steerer('Low.ST4', 1.463, 20.546, 1.986-1.463, 1.463-1.27, current = 0.05)
lowst5 = Steerer('Low.ST5', 1.986, 2.413, 2.675-1.986, 1.986-1.463, current = 4.7)

st_list = [lowst5, lowst4, lowst3, lowst2, lowst1, cathode]


#%% x in 2021
# Steerer strength: mrad * MeV/c / A
# The colomns are strength, current and drift length, respectively
cathode= Steerer('Cathode', 0, 0, 0.492, 0, current = 0)

lowst1 = Steerer('Low.ST1', 0.492, 7.5323, 0.963-0.492, 0.492, current = -0.050)
lowst2 = Steerer('Low.ST2', 0.963, 11.8958, 1.27-0.963, 0.963-0.492, current = 0.700)
lowst3 = Steerer('Low.ST3', 1.27, 21.9353, 1.463-1.27, 1.27-0.963, current = 0.600)
lowst4 = Steerer('Low.ST4', 1.463, 20.546, 1.986-1.463, 1.463-1.27, current = 0.235)
lowst5 = Steerer('Low.ST5', 1.986, 2.6246, 2.675-1.986, 1.986-1.463, current = 6)

st_list = [lowst5, lowst4, lowst3, lowst2, lowst1, cathode]

#%% y in 2021
# Steerer strength: mrad * MeV/c / A
# The colomns are strength, current and drift length, respectively
lowst1 = Steerer('Low.ST1', 0.492, 7.5323, 0.963-0.492, 0.492, current = 0.150)
lowst2 = Steerer('Low.ST2', 0.963, 11.8958, 1.27-0.963, 0.963-0.492, current = 0)
lowst3 = Steerer('Low.ST3', 1.27, 21.9353, 1.463-1.27, 1.27-0.963, current = -0.470)
lowst4 = Steerer('Low.ST4', 1.463, 20.546, 1.986-1.463, 1.463-1.27, current = 0.400)
lowst5 = Steerer('Low.ST5', 1.986, 2.413, 2.675-1.986, 1.986-1.463, current = 5)

st_list = [lowst5, lowst4, lowst3, lowst2, lowst1, cathode]

#%% x in 2019
# Steerer strength: mrad * MeV/c / A
# The colomns are strength, current and drift length, respectively
lowst1 = Steerer('Low.ST1', 0.492, 7.5323, 0.963-0.492, 0.492, current = -0.50)
lowst2 = Steerer('Low.ST2', 0.963, 11.8958, 1.27-0.963, 0.963-0.492, current = -0.67)
lowst3 = Steerer('Low.ST3', 1.27, 21.9353, 1.463-1.27, 1.27-0.963, current = 0.500)
lowst4 = Steerer('Low.ST4', 1.463, 20.546, 1.986-1.463, 1.463-1.27, current = -0.25)
lowst5 = Steerer('Low.ST5', 1.986, 2.6246, 0.5, 1.986-1.463, current = 4)

st_list = [lowst5, lowst4, lowst3, lowst2, lowst1, cathode]

#%% y in 2019
# Steerer strength: mrad * MeV/c / A
# The colomns are strength, current and drift length, respectively
lowst1 = Steerer('Low.ST1', 0.492, 7.5323, 0.963-0.492, 0.492, current = -0.150)
lowst2 = Steerer('Low.ST2', 0.963, 11.8958, 1.27-0.963, 0.963-0.492, current = 0)
lowst3 = Steerer('Low.ST3', 1.27, 21.9353, 1.463-1.27, 1.27-0.963, current = 0.50)
lowst4 = Steerer('Low.ST4', 1.463, 20.546, 1.986-1.463, 1.463-1.27, current = 0.20)
lowst5 = Steerer('Low.ST5', 1.986, 2.413, 0.5, 1.986-1.463, current = -1.55)

st_list = [lowst5, lowst4, lowst3, lowst2, lowst1, cathode]

#%% x in 2020
# Steerer strength: mrad * MeV/c / A
# The colomns are strength, current and drift length, respectively
lowst1 = Steerer('Low.ST1', 0.492, 7.5323, 0.963-0.492, 0.492, current = -0.050)
lowst2 = Steerer('Low.ST2', 0.963, 11.8958, 1.27-0.963, 0.963-0.492, current = 0.55)
lowst3 = Steerer('Low.ST3', 1.27, 21.9353, 1.463-1.27, 1.27-0.963, current = 0.600)
lowst4 = Steerer('Low.ST4', 1.463, 20.546, 1.986-1.463, 1.463-1.27, current = 0.135)
lowst5 = Steerer('Low.ST5', 1.986, 2.6246, 0.5, 1.986-1.463, current = 4.7)

st_list = [lowst5, lowst4, lowst3, lowst2, lowst1, cathode]

#%% y in 2020
# Steerer strength: mrad * MeV/c / A
# The colomns are strength, current and drift length, respectively
lowst1 = Steerer('Low.ST1', 0.492, 7.5323, 0.963-0.492, 0.492, current = 0.150)
lowst2 = Steerer('Low.ST2', 0.963, 11.8958, 1.27-0.963, 0.963-0.492, current = 0)
lowst3 = Steerer('Low.ST3', 1.27, 21.9353, 1.463-1.27, 1.27-0.963, current = -0.150)
lowst4 = Steerer('Low.ST4', 1.463, 20.546, 1.986-1.463, 1.463-1.27, current = 0.550)
lowst5 = Steerer('Low.ST5', 1.986, 2.413, 0.5, 1.986-1.463, current = 2)

st_list = [lowst5, lowst4, lowst3, lowst2, lowst1, cathode]

#%%
z, x, xp = 2.675,0, 0, #3, 2e-3
P = 6.7

res = [[z, x, xp]]
for st in st_list:
    s, I, L = st.strength, st.current, st.to_next
    
    z -= L     # position after the kick
    x -= xp*L  # offset after the kick
    
    res.append([z+0.003, x, xp])

    xp -= s*I/P*1e-3 # slope before the kick
    
    res.append([z-0.003, x, xp])
    
res = np.array(res)

reset_margin(left = 0.15, right = 0.85)

fig, ax = plt.subplots(figsize = (5, 4))

ax.plot(res[:,0], res[:,1]*1e3, 'r<-')
ax.plot([], [], 'b>-')
ax.grid()

ax.set_xlabel(r'$z$ (m)')

ax1 = ax.twinx()
ax1.plot(res[:,0], res[:,2]*1e3, 'b>-')
ax1.grid()

if 1:
    ax.set_ylabel(r'$x_c$ (mm)')
    ax1.set_ylabel('$x_c^{\prime}$ (mrad)')
    
    ax.set_ylim(-10, 10)
    ax1.set_ylim(-10, 10)
    
    ax.legend(['$x_c$', '$x_c^{\prime}$'])
    #ig.savefig('xc-z-2019.png')

else:
    ax.set_ylabel(r'$y_c$ (mm)')
    ax1.set_ylabel('$y_c^{\prime}$ (mrad)')
    
    #x.set_ylim(-4, 4)
   #ax1.set_ylim(-4, 4)
    
    ax.legend(['$y_c$', '$y_c^{\prime}$'])
    #ig.savefig('yc-z-2019.png')
    
#%%
from my_object import *
workdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim\2020\MBI'
os.chdir(workdir)
for I in np.linspace(360, 400, 1):
    print(I)
    os.chdir('Q-4500.00pC-D-4.20mm-E1-60.00MV_m-phi1-0.00deg-E2-18.59MV_m-phi2--20.00deg-I-%.2fA' % I)
    data = pd_loadtxt('ast.0049.003')
    #data[1:,2] += data[0,2]
    data[1:,5] += data[0,5]
    
    if data[0,-1]<0:
        data[0,-1] = 5
    select = data[:,-1]>0
    dist = data[select]
    
    def RotateMatrix(theta, axis = 'x'):
        cs = np.cos(theta)
        ss = np.sin(theta)
        
        if axis in ['x', 'X']:
            M = np.array([[1, 0, 0],
                          [0, cs, -ss],
                          [0, ss, cs]])
        if axis in ['y', 'Y']:
            M = np.array([[cs, 0, ss],
                          [0, 1, 0],
                          [-ss, 0, cs]])
        if axis in ['z', 'Z']:
            M = np.array([[cs, -ss, 0],
                          [ss, cs, 0],
                          [0, 0, 1]])
            
        return M
    
    theta = 45*np.pi/180
    theta, offset = 6e-3, 4.5e-3
    theta, offset = 0, -7.5e-3
    M = RotateMatrix(theta, 'y')
    
    # v = [0, 0, 0]
    # u = M @ np.asarray(v)
    
    temp = np.copy(dist)
    temp[0,2] = 0
    
    for i, v in enumerate(dist[:], start = 0):
        v1 = v[0:3]
        u1 = M @ np.asarray(v1)
        
        v2 = v[3:6]
        u2 = M @ np.asarray(v2)
        
        temp[i,0:3] = u1[:]
        temp[i,3:6] = u2[:]
    
    temp[0,2] = dist[0,2]
    temp[:,0] -= offset
    
    temp[1:,5] -= temp[0, 5]
    
    np.savetxt('ast.0050.005', temp[::1], fmt = ast_fmt)
    
    os.chdir('..')
#%%
fig, ax = plt.subplots()

ax.plot(temp[1::99,2], temp[1::99,0], '.')

fig, ax = plt.subplots()

ax.plot(temp[1::99,5], temp[1::99,3], '.')

#%%
theta = 6e-3
LB = 1.5
B = temp[0,5]*theta/g_c/LB; print(B)

#%%
LB = 0.1
theta = 4.2e-3
B = temp[0,5]*theta/g_c/LB; print(B)

data = np.loadtxt('ast.Xemit.003')
fig, ax = plt.subplots()
ax.plot(data[:,0], data[:,2])
ax.grid()

FitSlope(data[:,0], data[:,2]*1e-3, xmin = 2, plot = 0)
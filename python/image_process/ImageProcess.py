# -*- coding: utf-8 -*-
"""
Created on Fri Jan 31 11:29:53 2020

@author: lixiangk

Requirement: 
    General modules
      scipy
      numpy
      time, for counting running time
      zlib, for loading and saving raw frames
      cv2, for getting the range of interest (ROI)
    
    Other modules/libraries:
      PyTine, for reading and writing doocs variables

"""

#%matplotlib inline
#%matplotlib notebook
from universal import *
import numpy as np
import time

#%%
class CamInfo:
    def __init__(self, width, height, bpp, bpp_effective, scale):
        self.width = width
        self.height = height
        self.bpp = bpp
        self.bpp_effective = bpp_effective
        self.scale = scale

try:
    import zlib
    
    def PITZ_loadImage(fname):
        '''
        Parameters
          fname: full name of the images in formats of '.imc' or '.bkc'
        Returns
          a: 3D arrays of the images with the first dimension the frames and other two the 2D arrays of pixels of each frame
          h: headers of frames
          n: number of frames
        '''
        
        f = open(fname, 'rb')
        #data = f.read()
        
        width = np.frombuffer(f.read(4), dtype = np.uint32)[0]
        height = np.frombuffer(f.read(4), dtype = np.uint32)[0]
        bpp = np.frombuffer(f.read(2), dtype = np.uint16)[0]
        bpp_effective = np.frombuffer(f.read(2), dtype = np.uint16)[0]
    
        n = np.frombuffer(f.read(4), dtype = np.uint32)[0]
    
        a = np.zeros((n, height, width), dtype = np.uint16)
        h = np.empty(n, CamInfo)
        
        #X = 16
        for i in np.arange(n):
    
            scale = np.frombuffer(f.read(8), dtype = np.double)[0]
            len_compressed = np.frombuffer(f.read(4), dtype = np.uint32)[0]
            length = np.frombuffer(f.read(4), dtype = np.uint32)[0]
            
            #print('The compressed length of the current frame is: ', len_compressed)
            #print('The length of the current frame is: ', length)
            
            img = np.frombuffer(zlib.decompress(f.read(len_compressed)), dtype = np.uint16)
            #print('The length of the raw data is: ', len(img))
    
            h[i] = CamInfo(width, height, bpp, bpp_effective, scale)
            a[i] = img.reshape(height, width)
    
            #X = X+16+len_compressed
        f.close()
        return [a.astype(np.float), h, n]

    def PITZ_saveImage(fname, a, h):
        '''
        Parameters
          fname: full name of the images in formats of '.imc' or '.bkc'
          a: 3D arrays of the images with the first dimension the frames and other two the 2D arrays of pixels of each frame
          h: headers of frames
        Returns
          no
        '''
        #a = a.astype(np.uint16)
        
        f_handle = open(fname, 'wb')
        
        x = np.array([h[0].width, h[0].height], dtype = np.uint32); #print(x)
        x.tofile(f_handle)
        
        x = np.array([h[0].bpp, h[0].bpp_effective], dtype = np.uint16); #print(x)
        x.tofile(f_handle)
        
        n = len(h)
        
        x = np.array([n], dtype = np.uint32); #print(x)
        x.tofile(f_handle)
        
        i = 0
        while i<n:
            x = np.array([h[i].scale], dtype = np.double); #print(x)
            x.tofile(f_handle)
            
            #print(a[i].shape)
            a_bytes = a[i].flatten().tobytes()
            length = len(a_bytes)
            
            a_compressed = zlib.compress(a_bytes)
            len_compressed = len(a_compressed)
            
            #print('The compressed length of the current frame is: ', len_compressed)
            #print('The length of the current frame is: ', length)
            
            x = np.array([len_compressed, length], dtype = np.uint32)
            x.tofile(f_handle)
            
            f_handle.write(a_compressed)
            i += 1
        
        f_handle.close()
        return
    
except Exception as err:
    print(err)
    
try:
    import PyTine as pt
    
    def PITZ_getImage(tineAddress, nFrame = 1, prop = 'Frame.Sched'):
        '''
        Parameters
          tineAddress: address of the frame grabber, e.g., 'PITZ.VIDEO/TV2S.PROS.1/Output'
          nFrame: number of frame to take
        Returns
          a: 3D arrays of the images with the first dimension the frames and other two the 2D arrays of pixels of each frame
          h: headers of frames
        '''
        
        #prop = 'Frame'
        
        i = 0; trial = 0
        while i<nFrame:
            try:
                frame = pt.get(tineAddress, property = prop)
                stamp = frame.get('timestamp')
                trial += 1
                
                while i>0 and stamp <= stamp_old:
                    time.sleep(0.1)
                    frame = pt.get(tineAddress, property = prop)
                    stamp = frame.get('timestamp')
                    trial += 1
                    print('Trials ', trial, 'with time stamp ', stamp)
                stamp_old = stamp
                
                #for key in ['status', 'timestamp', 'sysstamp', 'usrstamp', 'timestring']:
                #    print(key, ':', frame.get(key))
                
                cameraPortName= frame.get('data').get('sourceHeader').get('cameraPortName')
                print('Get ', i+1, ' frames from ', cameraPortName)
                
                frameHeader = frame.get('data').get('frameHeader')
                keys = ['sourceWidth', 'sourceHeight', 'bytesPerPixel', 'effectiveBitsPerPixel', 'xScale']
                args = [frameHeader.get(key) for key in keys]; args[2] *= 8
                caminfo = CamInfo(*args)
                
                imageBytes = frame.get('data').get('imageBytes')
                imageRaw = np.frombuffer(imageBytes, dtype = np.uint16)
                image = imageRaw.reshape(caminfo.height, caminfo.width)
            
                if i == 0:
                    h = np.empty(nFrame, CamInfo)
                    a = np.zeros((nFrame, caminfo.height, caminfo.width), dtype = np.uint16)
            
                h[i] = caminfo
                a[i] = image
                i += 1
            except Exception as err:
                print(err)
                time.sleep(0.1)
                
            #print(i, ' frames taken with ', trial, ' attempts!')
            
        return [a, h]

    def PITZ_getImage_noBreak(tineAddress, nFrame, prop = 'Frame.Sched'):
        '''
        Parameters
          tineAddress: address of the frame grabber, e.g., 'PITZ.VIDEO/TV2S.PROS.1/Output'
          nFrame: number of frame to take
        Returns
          a: 3D arrays of the images with the first dimension the frames and other two the 2D arrays of pixels of each frame
          h: headers of frames
        '''
        
        h = []
        a = []
        def cb(id, cc, frame):
            #global lid
            if cc == 0:
                
                print(type(frame))
                frameHeader = frame.get('data').get('frameHeader')
                keys = ['sourceWidth', 'sourceHeight', 'bytesPerPixel', 'effectiveBitsPerPixel', 'xScale']
                args = [frameHeader.get(key) for key in keys]; args[2] *= 8
                caminfo = CamInfo(*args)
                
                imageBytes = frame.get('data').get('imageBytes')
                imageRaw = np.frombuffer(imageBytes, dtype = np.uint16)
                image = imageRaw.reshape(caminfo.height, caminfo.width)
                
                h.append(caminfo)
                a.append(image)
                
                camPortName = frame.get('data').get('sourceHeader').get('cameraPortName')
                print('Get ', len(h), ' frames from ', camPortName)
                
                if len(h) >= nFrame:
                    pt.detach(lid)
        
        lid = pt.attach(tineAddress, property = prop, callback = cb,\
                        interval = 100)
        
        while len(h)<nFrame:
            time.sleep(0.1)
            
        #return [a, h]
        return [np.array(a), np.array(h)]
        
    def PITZ_frameToImage(frames, nFrame = 1):
        '''
        Parameters
          frames: frames returned from the server
          nFrame: number of frame to take
        Returns
          a: 3D arrays of the images with the first dimension the frames and other two the 2D arrays of pixels of each frame
          h: headers of frames
        '''
        
        if nFrame>len(frames):
            print('Only use the frames available.')
            nFrame = len(frames)
        
        i = 0;
        while i<nFrame:
            try:
                
                frame = frames[i]
                
                cameraPortName= frame.get('data').get('sourceHeader').get('cameraPortName')
                print('Process ', i+1, ' frames from ', cameraPortName)
                
                frameHeader = frame.get('data').get('frameHeader')
                keys = ['sourceWidth', 'sourceHeight', 'bytesPerPixel', 'effectiveBitsPerPixel', 'xScale']
                args = [frameHeader.get(key) for key in keys]; args[2] *= 8
                caminfo = CamInfo(*args)
                
                imageBytes = frame.get('data').get('imageBytes')
                imageRaw = np.frombuffer(imageBytes, dtype = np.uint16)
                image = imageRaw.reshape(caminfo.height, caminfo.width)
                
                if i == 0:
                    h = np.empty(nFrame, CamInfo)
                    a = np.zeros((nFrame, caminfo.height, caminfo.width), dtype = np.uint16)
                    
                h[i] = caminfo
                a[i] = image
                i += 1
            except Exception as err:
                print(err)
                time.sleep(0.1)
            
        return [a, h]
except Exception as err:
    print(err)
    

#%%
try:
    import scipy.ndimage
    def emcalcFilter(img, std, cut = 1, nXOR = 1):
        '''
        Parameters
          img: image with backgroud subtracted, 2D array
          std: standard deviation of backgrouds
          cut: pixels less than cut*std are removed
          nXOR: n times the XOR filter is applied
        Returns
          img: filtered imgage, 2D array
        '''
        
        temp1 = np.copy(img)
        temp1[temp1<=cut*std] = 0
        
        temp = np.zeros(temp1.shape)
        temp[1:-1,1:-1] = temp1[1:-1,1:-1] # cut 1 pixel border
        
        if nXOR>0:
            for i in np.arange(nXOR):
                a = temp[2:,:-2]*temp[2:,1:-1]*temp[2:,2:]*temp[1:-1,:-2]*temp[1:-1,2:]*temp[:-2,:-2]*temp[:-2,1:-1]*temp[:-2,2:]
                #temp[1:-1,1:-1] = temp[1:-1,1:-1]*(a>0)
                temp[1:-1,1:-1][a<=0] = 0
            
            H = np.ones((3,3))
            for i in np.arange(nXOR):
                
                tr = scipy.ndimage.convolve(temp, H, mode='nearest')
                temp = np.copy(img)
                temp[tr<=0] = 0
        
        return temp
except Exception as err:
    print(err)
    
try:
    import cv2
    def getROI(a):
        '''
        To use it, drag the cursor to select the region of interest then press Enter key.
        To refer to the cropped image: [ROI[1]:ROI[1]+ROI[3], ROI[0]:ROI[0]+ROI[2]], e.g.,
          imCrop = im[ROI[1]:ROI[1]+ROI[3], ROI[0]:ROI[0]+ROI[2]]
        Parameteres
          a: 2D array representing the pixel counts of an image
        Returns
          ROI = [x0, y0, width, height]
        '''
        
        a[a<0] = 0
        #a[a == 0] = np.nan
        a = a/np.max(a)*255
        a = a.astype(np.uint8)
        
        ROI = cv2.selectROI("Select the region and press ENTER", a[::2,::2])
        ROI = [r*2 for r in ROI]
        cv2.imshow("Image in the selected region", a[ROI[1]:ROI[1]+ROI[3], ROI[0]:ROI[0]+ROI[2]])
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        
        return ROI
except Exception as err:
    print(err)

def getBackground(bkgs, Nbkg = None, treat = None):
    '''
    Parameteres
      bkgs: 3D array, each sub 2D array representing the pixel counts of an image frame
      Nbkg: number of frames to be considered
      treat: the method to treat the backgrouds
        - None in default, returns [env, agv, std]
        - 'ENVELOPE', returns envelope of each pixel
        - 'Average', returns [avg, std] of each pixel
    Returns
      [env, avg, std]: envelope, average and standard deviation of backgrouds
    '''
    
    if Nbkg is None or Nbkg > len(bkgs):
        Nbkg = len(bkgs)
    
    if treat is None:
        env = np.max(bkgs, axis = 0)
        avg = np.mean(bkgs, axis = 0)
        std = np.std(bkgs, axis = 0)
        return [env, avg, std]
    elif treat.upper() == 'ENVELOPE':
        env = np.max(bkgs, axis = 0)
        return env
    if treat.upper() == 'AVERAGE':
        avg = np.mean(bkgs, axis = 0)
        std = np.std(bkgs, axis = 0)
        return [avg, std]

def calcRMS(a, Xscale = 1, Yscale = None, Xshift = 0, Yshift = 0, debug = False):
    '''
    Parameters
      a: 2D array representing the pixel counts of an image
      Xscale, Yscale: sizes of each pixel
    Returns
      [xc, yc, xrms, yrms]: centers and rms sizes in x and y directions
    '''
    
    h, w = a.shape
    
    if Yscale is None:
        Yscale = Xscale
    
    X = (np.arange(0, w))*Xscale+Xshift
    Y = (np.arange(0, h))*Yscale+Yshift
    aX, aY = np.meshgrid(X, Y)

    ss = np.sum(np.sum(a))
    xc = np.sum(np.sum(aX*a))/ss
    yc = np.sum(np.sum(aY*a))/ss
    x2 = np.sum(np.sum(aX**2*a))/ss
    y2 = np.sum(np.sum(aY**2*a))/ss

    xrms = np.sqrt(x2-xc**2);
    yrms = np.sqrt(y2-yc**2);

    if debug:
        print('Xc, Yc, Xrms, Yrms: ', xc, yc, xrms, yrms)
    return [xc, yc, xrms, yrms]

def calcCov(a, Xscale = 1, Yscale = None, debug = False, Xshift = 0, Yshift = 0):
    '''
    Parameters
      a: 2D array representing the pixel counts of an image
      Xscale, Yscale: sizes of each pixel
    Returns
      [xc, yc, xrms, yrms]: centers and rms sizes in x and y directions
    '''
    
    h, w = a.shape
    
    if Yscale is None:
        Yscale = Xscale
    
    X = np.arange(0, w)*Xscale+Xshift
    Y = np.arange(0, h)*Yscale+Yshift
    aX, aY = np.meshgrid(X, Y)

    ss = np.sum(a)
    xc = np.sum(aX*a)/ss
    yc = np.sum(aY*a)/ss
    x2 = np.sum(aX**2*a)/ss
    y2 = np.sum(aY**2*a)/ss
    xy = np.sum(aX*aY*a)/ss
    
    x2 = (x2-xc**2);
    y2 = (y2-yc**2);
    xy = xy-xc*yc
    
    if debug:
        print('Xc, Yc, X2, Y2, XY: ', xc, yc, x2, y2, xy)
    return [xc, yc, x2, y2, xy]

def calcTwiss(a, Xscale = 1, Yscale = None, debug = False):
    '''
    Parameters
      a: 2D array representing the pixel counts of the phase space
      Xscale, Yscale: sizes of each pixel
    Returns
      [eps_x, beta_x, gamma_x, alpha_x]: RMS emittance and twiss parameters
    '''
    
    h, w = a.shape
    
    if Yscale is None:
        Yscale = Xscale
    
    X = np.arange(0, w)*Xscale
    Y = np.arange(0, h)*Yscale
    aX, aY = np.meshgrid(X, Y)

    ss = np.sum(np.sum(a))
    xc = np.sum(np.sum(aX*a))/ss
    yc = np.sum(np.sum(aY*a))/ss
    x2 = np.sum(np.sum(aX**2*a))/ss
    y2 = np.sum(np.sum(aY**2*a))/ss

    xy = np.sum(np.sum(aX*aY*a))/ss-xc*yc

    xrms = np.sqrt(x2-xc**2);
    yrms = np.sqrt(y2-yc**2);
    
    eps_x = np.sqrt(xrms**2*yrms**2-xy**2)
    beta_x = xrms**2/eps_x
    gamma_x = yrms**2/eps_x
    alpha_x = -xy/eps_x
    
    if debug:
        print('eps_x, sig_x, sig_xp, avg_xxp: ', eps_x, xrms*1e3, yrms*1e3, xy*1e6)
        print('eps_x, beta_x, gamma_x, alpha_x: ', eps_x, beta_x, gamma_x, alpha_x)
        print('beta_x*gamma_x-alpha_x^2: ', beta_x*gamma_x-alpha_x**2)
        
    return [eps_x, beta_x, gamma_x, alpha_x]

#%%
def getValue(var, fname):
    with open(fname, 'r') as f_handle:
        for i, line in enumerate(f_handle):
            if var in line:
                break
    cols = line.split(' ')
    return np.float(cols[-1])

def getTranspose(X1, fX1, X2, fX2):
    XX = np.linspace(np.min([X1[0], X2[0]]), np.max([X1[-1], X2[-1]]), 100)
    funcX1 = interp1d(X1, fX1, bounds_error = False, fill_value = 0)
    funcX2 = interp1d(X2, fX2, bounds_error = False, fill_value = 0)

    def obj_func(dx):
        f1 = funcX1(XX)
        f2 = funcX2(XX-dx)
        f1 = f1/np.sum(f1)
        f2 = f2/np.sum(f2)
        return np.sum(np.abs(f1-f2))

    from scipy.optimize import minimize
    res = minimize(obj_func, 0., method='Nelder-Mead', tol=1e-6)
    
    if res.success:
        dx = res.x
    else:
        dx = None
    
    return dx

#%%
def plotImage(signal, scale = 1, fname = None, title = None, extent = None,
              plottype = 'imshow', circle_fit = False, circle_cut = 1):
        
    '''
    Parameters
      plottype: 'imshow' or 'contourf'
    '''
    
    #signal = np.copy(self.image)
    #info = self.info
    #fname = self.fname
    
    if isinstance(scale, (list, np.ndarray, tuple)):
        Xscale, Yscale = scale
    else:
        Xscale, Yscale = scale, scale
        
    [xc, yc, xrms, yrms] = calcRMS(signal[::-1,::-1], Xscale, Yscale)
    [xc, yc, x2, y2, xy] = calcCov(signal[::-1,::-1], Xscale, Yscale)
    
    if title:
        title = str.format('Xrms = %.2f mm, Yrms = %.2f mm' % (xrms, yrms))
    
    reset_margin(bottom = 0.125, top = 0.925, left = 0.125, right = 0.925)
    fig, ax = plt.subplots(figsize = (4, 3.2))
    
    if plottype is 'contourf':
        h, w = signal.shape
        X = np.arange(0, w)*Xscale-xc
        Y = np.arange(0, h)*Yscale-yc
        aX, aY = np.meshgrid(X, Y)
        
        # Noise level for 2D plot
        noise = 0.001
        v = np.linspace(noise, 1, 99)*np.max(signal)
    
        cax = ax.contourf(aX, aY, signal[::-1,::-1], v,
                          linestyles = None, zorder=-9)
        ax.set_rasterization_zorder(-1)
        ax.set_aspect('auto')
        
    elif plottype is 'imshow':
        h, w = signal.shape
        X = np.arange(0, w)*Xscale-xc
        Y = np.arange(0, h)*Yscale-yc
        
        tmp = np.copy(signal)
        tmp[tmp==0] = np.nan
        cax = ax.imshow(tmp[:,::-1], extent = (X[0], X[-1], Y[0], Y[-1]),\
                        aspect = 'auto')
        del tmp
    
    cbar = fig.colorbar(cax, fraction = 0.046*3, pad = 0.04)
    #cbar.set_ticks(np.linspace(0, 900, 10))
    cbar.ax.tick_params(labelsize = 11, pad = 2)
    cbar.ax.set_ylabel('Pixels', labelpad = 2)
    cbar.ax.ticklabel_format(style = 'sci', scilimits = (0,0))
    #ax.grid()
    
    if title:
        ax.set_title(title, fontsize = 11)
    ax.set_xlabel(r'$x$ (mm)')
    ax.set_ylabel(r'$y$ (mm)')
    
    if circle_fit:
        tmp = np.copy(signal)
        tmp[tmp<=circle_cut] = 0
        tmp[tmp>circle_cut] = 1
        xg, yg, xrms, yrms = calcRMS(tmp[::-1,::-1], Xscale, Yscale)
        xyrms = np.sqrt(xrms*yrms)
        del tmp
        
        phi = np.linspace(-np.pi, np.pi, 361)
        xx = xg-xc+2*xyrms*np.cos(phi)
        yy = yg-yc+2*xyrms*np.sin(phi)
        ax.plot(xx, yy, '-', color = 'gray')
    
    if extent is not None:
        ax.axis(extent)
    else:
        ax.set_xlim(-5, 5)
        ax.set_ylim(-5, 5)
    
    if fname is not None:
        path, name, ext = fileparts(fname)
        if ext not in ['.png', '.eps']:
            fname += '.png'
        fig.savefig(fname)
    
    reset_margin()
    return
    
def plotPhaseSpace(ps, extent = None, plottype = 'imshow'):
    
    PS = np.copy(ps.PS)
    Xscale, Yscale = ps.step, ps.info.scale/1e3 # meter
    drift = ps.drift
    slit = ps.slit
    
    [xc, xpc, xrms, xprms] = calcRMS(PS[::-1], Xscale, Yscale/drift)
        
    h, w = PS.shape
    XX = np.arange(0, w)*Xscale-xc
    XP = np.arange(0, h)*Yscale/drift-xpc
    
    #reset_margin(bottom = 0.125, top = 0.925,
    #       left = 0.125, right = 0.925)
    reset_margin(bottom = 0.25, top = 0.925,
           left = 0.125, right = 0.925)
    fig, ax = plt.subplots(figsize = (4, 2))
        
    if plottype is 'contourf':
        
        aXX, aXP = np.meshgrid(XX, XP)
        
        # Noise level for 2D plot
        noise = 0.001
        v = np.linspace(noise, 1, 99)*np.max(PS)
        
        cax = ax.contourf(aXX*1e3, aXP*1e3, PS[::-1], v,
                          linestyles = None, zorder=-9)
        ax.set_rasterization_zorder(-1)
        ax.set_aspect('auto')
        
    elif plottype is 'imshow':
        tmp = np.copy(PS)
        tmp[tmp==0] = np.nan
        cax = ax.imshow(tmp, extent = (XX[0]*1e3, XX[-1]*1e3, XP[0]*1e3, XP[-1]*1e3),
                        aspect = 'auto')
        del tmp
        
    cbar = fig.colorbar(cax, fraction = 0.046*3, pad = 0.04)
    cbar.ax.tick_params(labelsize = 11, pad = 2)
    cbar.ax.set_ylabel('Intensity (arb. unit)', labelpad = 2)
    
    #from matplotlib import ticker
    #@ticker.FuncFormatter
    #def my_format(x, pos):
    #    return "[%.1f]" % (x/1e3)
    #cbar.ax.yaxis.set_major_formatter(my_format)
    
    cbar.ax.ticklabel_format(style = 'sci', scilimits = (3, 4), useMathText=True)
    
    
    if slit is 'X':
        ax.set_xlabel(r'$x$ (mm)', fontsize=12, labelpad=-5)
        ax.set_ylabel(r'$x^{\prime}$ (mrad)', fontsize=12)
    elif slit is 'Y':
        ax.set_xlabel(r'$y$ (mm)', fontsize=12, labelpad=-5)
        ax.set_ylabel(r'$y^{\prime}$ (mrad)', fontsize=12)
    
    if extent is not None:
        ax.axis(extent)
        ax.yaxis.set_major_locator(plt.MultipleLocator(1))
    
    fig.savefig(ps.fname[0:-4]+'.png')
    
    #reset_margin()
    return
   
#%%

class PITZ_Image():
    
    def __init__(self, fname = None, ROI = None, treat = 'envelope',
                 emcalc_filter = False, cut = 1, plot = True):
        
        if fname is None:
            fname = get_file('.imc')
        imgs, camInfos, n = PITZ_loadImage(fname)
        
        fname = fname[0:-4]+'.bkc'
        bkgs, camInfos, n = PITZ_loadImage(fname)
        
        info = camInfos[0]; print('Scale: ', info.scale)
        
        # Select ROI
        if ROI is None:
            ROI = getROI(np.mean(imgs, axis = 0)); print('ROI', ROI)
            
        imgs = imgs[:, ROI[1]:ROI[1]+ROI[3], ROI[0]:ROI[0]+ROI[2]]
        bkgs = bkgs[:, ROI[1]:ROI[1]+ROI[3], ROI[0]:ROI[0]+ROI[2]]
        
        sop = np.sum(imgs)/n
        print('Sum of pixels: ', sop)
        
        # Get background
        env, avg, std = getBackground(bkgs, treat = None); 
        print('Average background level: ', np.mean(env))
        
        if treat is 'average':
            for i in np.arange(len(imgs)):
                img = imgs[i]-avg; # print(i, np.max(img))
                img[img<0] = 0
                
                if emcalc_filter is True:
                    img = emcalcFilter(img, std, 1, 1)
            
                imgs[i] = img
        elif treat is 'envelope':
            for i in np.arange(len(imgs)):
                img = imgs[i]-env; # print(i, np.max(img))
                img[img<0] = 0
                
                if emcalc_filter is True:
                    img = emcalcFilter(img, std, 1, 1)
                    
                imgs[i,:,:] = img[:,:]
        
        signal = np.mean(imgs, axis = 0)
        
        # Filter out noisy pixels
        signal[signal<=cut] = 0
        
        self.fname = fname
        self.image = signal
        self.info = info
        self.ROI = ROI
        self.sop = sop
        
        self.x1 = calcRMS(signal[::-1,::-1], info.scale)
        self.x2 = calcCov(signal[::-1,::-1], info.scale)
        
        if plot:
            self.plot()
            
    def plot(self, fig_ext = '.png', **kwargs):
        
        '''
        Parameters
          plottype: 'imshow' or 'contourf'
        '''
        
        signal = np.copy(self.image)
        scale = self.info.scale
        fname = self.fname[0:-4]+fig_ext
        
        plotImage(signal, scale = scale, fname = fname, **kwargs)
        return
    
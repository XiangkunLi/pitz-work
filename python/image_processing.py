# -*- coding: utf-8 -*-
"""
Created on Fri Jan 31 11:29:53 2020

@author: lixiangk
"""

#%matplotlib inline
#%matplotlib notebook
from image_process.ImageProcess import *
from universal import *

VC2DIR = r'\\afs\ifh.de\group\pitz\doocs\measure\Laser\TransverseProfile\VC2\2020'
EMSYDIR = r'\\afs\ifh.de\group\pitz\doocs\measure\TransvPhSp\2020\ProjEmittance'

#%% Load phase space matrix produced by emcalc3_test
# workdir = r'\\afs\ifh.de\group\pitz\doocs\measure\TransvPhSp\2020\ProjEmittance\reports'
# os.chdir(workdir)

# fig, [ax1, ax2] = plt.subplots(nrows = 2, figsize=(4, 4))
fig, ax1= plt.subplots(nrows = 1, figsize=(4, 4))

# x
# path = '2020_01_28__10_34_15'
fname = get_file('.txt')
dataX = np.loadtxt(fname)
h, w = dataX.shape; print('Shape:', h, w)

details = fname[0:-4]+'_details.txt'
XMAX = getValue('X_MAX', details)
YMAX = getValue('Y_MAX', details)

if XMAX == 0:
    SCALE_FACTOR = getValue('SCALE_FACTOR', details)
    XMAX = SCALE_FACTOR*w
if YMAX == 0:
    SCALE_FACTOR = getValue('SCALE_FACTOR', details)
    YMAX = SCALE_FACTOR*h
print('XMAX, YMAX: ', XMAX, YMAX)

Xscale = XMAX/(w-1)
Yscale = YMAX/(h-1)
[xc, yc, xrms, yrms] = getRMS(dataX, Xscale, Yscale)
print('Xc, Yc, Xrms, Yrms: ', xc, yc, xrms, yrms)

xx = np.linspace(0, XMAX, w)-xc
xp = np.linspace(0, YMAX, h)-yc

[aXX, aXP] = np.meshgrid(xx, xp, indexing = 'xy');
v = np.linspace(0.01, 1, 99)*np.max(dataX)

###
#import numpy.random as rd
#
#for i in np.arange(5):
#    ss = np.sum(dataX.astype('int'))
#    dataX = dataX*200000/ss
#    #print(np.sum(dataX.astype('int')))
#dataX[100, 100] +=1    
#dataX = dataX.astype('int')
#print('X: ', np.sum(dataX))
#
#dXX = xx[1]-xx[0]
#dXP = xp[1]-xp[0]
#
#XXP = []
#for i in np.arange(h):
#    for j in np.arange(w):
#        for k in np.arange(dataX[i,j]):
#            XXP.append([aXX[i,j]+dXX*(rd.rand()-0.5), aXP[i,j]+dXP*(rd.rand()-0.5)])
###

ax1.contourf(aXX, aXP, 1.0*dataX, v, linestyles = None, zorder=-9)
ax1.set_rasterization_zorder(-1)
ax1.set_aspect('equal')

#ax1.set_xlim(-20, 20)
#ax1.set_ylim(-25, 25)

ax1.set_xlabel(u_x)
ax1.set_ylabel(r'$x^{\prime}$ (mrad)')

labels = [item.get_text() for item in ax1.get_xticklabels()]
empty_string_labels = ['']*len(labels)
#ax1.set_xticklabels(empty_string_labels)

#ax1.text(0.075, 0.905, '($b$)', horizontalalignment='center', verticalalignment='center', transform=ax1.transAxes)


#%% y
#path = '2020_01_28__10_42_10'
fname = get_file('.txt')
dataY = np.loadtxt(fname)
h, w = dataY.shape; print('Shape:', h, w)

details = fname[0:-4]+'_details.txt'
XMAX = getValue('X_MAX', details)
YMAX = getValue('Y_MAX', details)

Xscale = XMAX/(w-1)
Yscale = YMAX/(h-1)
[xc, yc, xrms, yrms] = getRMS(dataY, Xscale, Yscale)
print('Xc, Yc, Xrms, Yrms: ', xc, yc, xrms, yrms)

yy = np.linspace(0, XMAX, w)-xc
yp = np.linspace(0, YMAX, h)-yc

[aYY, aYP] = np.meshgrid(yy, yp, indexing = 'xy')
#v = np.linspace(1e-5, 1, 96)

##
for i in np.arange(9):
    ss = np.sum(dataY.astype('int'))
    dataY = dataY*200000/ss
dataY = dataY.astype('int')
print('Y: ', np.sum(dataY))

dYY = yy[1]-yy[0]
dYP = yp[1]-yp[0]

YYP = []
for i in np.arange(h):
    for j in np.arange(w):
        for k in np.arange(dataY[i,j]):
            YYP.append([aYY[i,j]+dYY*(rd.rand()-0.5), aYP[i,j]+dYP*(rd.rand()-0.5)])
##

ax2.contourf(aYY, aYP, dataY/np.max(dataY), v, linestyles = None)

ax2.set_xlim(-5, 5)
ax2.set_ylim(-1.5, 1.5)

ax2.set_xlabel(u_y)
ax2.set_ylabel(r'$y^{\prime}$ (mrad)')
#ax2.text(0.075, 0.905, '($c$)', horizontalalignment='center', verticalalignment='center', transform=ax2.transAxes)

#fig.savefig('phase-space@4nC.eps')

#%% Load .imc and .bkc, process the image and plot
fname = get_file('.imc')
imgs, camInfos, n = PITZ_loadImage(fname)

fname = fname[0:-4]+'.bkc'
bkgs, camInfos, n = PITZ_loadImage(fname)

info = camInfos[0]; print('Scale: ', info.scale)


# Select ROI
ROI = getROI(np.mean(imgs, axis = 0)); print('ROI', ROI)
#ROI = [0, 0, w, h]

imgs = imgs[:, ROI[1]:ROI[1]+ROI[3], ROI[0]:ROI[0]+ROI[2]]
bkgs = bkgs[:, ROI[1]:ROI[1]+ROI[3], ROI[0]:ROI[0]+ROI[2]]

sop = np.sum(np.sum(np.sum(imgs)))/n
#r.append(sop)

# Get background
env, avg, std = getBackground(bkgs, treat = None); print(np.mean(np.mean(env)))

treat = 'average'
#treat = 'envelope'

emcalc_filter = 1

for i in np.arange(len(imgs)):
    if treat is 'average':
        img = imgs[i]-avg; # print(i, np.max(img))
    elif treat is 'envelope':
        img = imgs[i]-env; # print(i, np.max(img))
        
    img[img<0] = 0
    
    if emcalc_filter:
        img = emcalcFilter(img, std, 1, 1)
    
    imgs[i] = img

img = np.mean(imgs, axis = 0)
signal = img

# Filter out noisy pixels
signal[signal<20] = 0

#signal = np.mean(imgs, axis = 0)
#signal = avg

[xc, yc, xrms, yrms] = calcRMS(signal[::-1], info.scale)
title = str.format('Xrms = %.2f mm, Yrms = %.2f mm' % (xrms, yrms))

plottype = 'imshow'
#plottype = 'contourf'

fig, ax = plt.subplots(figsize = (4, 3))

if plottype is 'contourf':
    h, w = signal.shape
    X = np.arange(0, w)*info.scale-xc
    Y = np.arange(0, h)*info.scale-yc
    aX, aY = np.meshgrid(X, Y)
    
    # Noise level for 2D plot
    noise = 0.001
    v = np.linspace(noise, 1, 99)*np.max(signal)

    cax = ax.contourf(aX, aY, signal[::-1,::-1], v, linestyles = None, zorder=-9)
    ax.set_rasterization_zorder(-1)
    #ax.set_aspect('auto')
    
elif plottype is 'imshow':
    h, w = signal.shape
    X = np.arange(0, w)*info.scale-xc
    Y = np.arange(0, h)*info.scale-yc
    
    tmp = np.copy(signal)
    tmp[tmp==0] = np.nan
    cax = ax.imshow(tmp[:,::-1], extent = (X[0], X[-1], Y[0], Y[-1]),
                    aspect = 'auto')
    del tmp

# cbar = fig.colorbar(cax, fraction = 0.046, pad = 0.04) #, format=format)
# #cbar.set_ticks(np.linspace(0, 900, 10))
# cbar.ax.tick_params(labelsize = 9, pad = 2)
# cbar.ax.set_ylabel('Pixels', labelpad = 2)
# #ax.grid()

ax.set_title(title, fontsize = 11)
ax.set_xlabel(r'$x$ (mm)')
ax.set_ylabel(r'$y$ (mm)')

#ax.set_xlim(-4, 4)
#ax.set_ylim(-4, 4)

fig.savefig(fname[0:-4]+'.png')

#%%
fig, ax = plt.subplots(ncols = 2, figsize = (12, 4))

X_EMSY = X
fX_EMSY = np.sum(signal, axis = 0)/250000

X_PS = xx
fX_PS = np.sum(dataX, axis = 0)/2500

dx = getTranspose(X_EMSY, fX_EMSY, X_PS, fX_PS)

ax[0].plot(X_EMSY, fX_EMSY, '-')
ax[0].plot(X_PS+dx, fX_PS, '-')

Y_EMSY = Y
fY_EMSY = np.sum(signal, axis = 1)/250000

Y_PS = yy
fY_PS = np.sum(dataY, axis = 0)/2500

dy = getTranspose(Y_EMSY, fY_EMSY, Y_PS, fY_PS)

ax[1].plot(Y_EMSY, fY_EMSY, '-')
ax[1].plot(Y_PS+dy, fY_PS, '-')

#ax[0].grid()
#ax[1].grid()

#%%
from scipy.interpolate import RegularGridInterpolator

#xgrid = X-xgc; Nx = len(xgrid); hx = xgrid[1]-xgrid[0]
#ygrid = Y-ygc; Ny = len(ygrid); hy = ygrid[1]-ygrid[0]

f2D_XXP = RegularGridInterpolator((xx+dx, xp), dataX.T, bounds_error = False, fill_value = None)
f2D_YYP = RegularGridInterpolator((yy+dy, yp), dataY.T, bounds_error = False, fill_value = None)
f2D_XY = RegularGridInterpolator((X, Y), signal.T, bounds_error = False, fill_value = None)






#%% Analysis of fast scan raw data
fname = get_file('.imc')
imgs, camInfos, n = PITZ_loadImage(fname)

fname = fname[0:-4]+'.bkc'
bkgs, camInfos, n = PITZ_loadImage(fname)

info = camInfos[0]; print('Scale: ', info.scale)

#fname = fname[0:-4]+'.log'
#steps = np.loadtxt(fname, usecols =(4))

path, name, ext = fileparts(fname)

# Select ROI
#ROI = getROI(np.mean(imgs, axis = 0))

imgs = imgs[:, ROI[1]:ROI[1]+ROI[3], ROI[0]:ROI[0]+ROI[2]]
bkgs = bkgs[:, ROI[1]:ROI[1]+ROI[3], ROI[0]:ROI[0]+ROI[2]]

# Get background
env, avg, std = getBackground(bkgs, treat = None); print(np.mean(np.mean(env)))

# Slit scan setting
slit = 'Y'
slit = name[5]; print('slit: ', slit)

step = 0.5e-3*0.1 # actuator step, m
drift = 3.133 # drift length, m


P0 = 17 # MeV/c
gg = kinetic2gamma(momentum2kinetic(P0))
gb = gamma2bg(gg)

if slit is 'X':
    PS = np.zeros((ROI[2], n))
elif slit is 'Y':
    PS = np.zeros((ROI[3], n))

fig, ax = plt.subplots()

res = []
Xscale, Yscale = step, info.scale/1e3 # meter
for i in np.arange(n):
    
    if slit is 'X':
        x0 = i*step  # x0 increases since the slit moves from right (negative) to left (positive)
    if slit is 'Y':
        x0 = -i*step # x0 decreases since the slit moves from top (positive) to bottom (negative)
        
    img = imgs[i]-avg
    img[img<0] = 0
    img = emcalcFilter(img, std, 1, 1)

    [xc, xpc, xrms, xprms] = calcRMS(img[::-1,::-1], Xscale, Yscale)
    res.append([x0, xc, xpc, xrms, xprms])
    
    signal = img
    
    ncols = len(PS)
    if slit is 'X':
        shift = np.mod(int(x0/Yscale), ncols) 
        # shift toward smaller divergence which is on the right side of the array
        PS[:,i] = np.roll(np.sum(signal, axis = 0), shift) 
    elif slit is 'Y':
        shift = np.mod(int(x0/Yscale), ncols)
        # shift toward smaller divergence which is on the bottom side of the array
        PS[:,n-i-1] =  np.roll(np.sum(signal, axis = 1), shift)
    
    if (np.mod(i,3)) == 0:
        tmp = np.copy(signal)
        tmp[tmp==0] = np.nan
        ax.imshow(tmp)
        ax.set_title('frame: %d' % i)
        
        #fig.savefig('frame-%d.png' % i)
        
        plt.pause(0.01)
        plt.cla()
        
#plt.close('all')

[xc, xpc, xrms, xprms] = calcRMS(PS[::-1], Xscale, Yscale/drift)
[eps_x, beta_x, gamma_x, alpha_x] = calcTwiss(PS[::-1], Xscale, Yscale/drift)

eps_x = eps_x*gb
print('Normalized emittance: ', eps_x*1e6, ' um')

plottype = 'contourf'

fig, ax = plt.subplots()

if plottype is 'contourf':
    h, w = PS.shape
    XX = np.arange(0, w)*Xscale-xc
    XP = np.arange(0, h)*Yscale/drift-xpc
    aXX, aXP = np.meshgrid(XX, XP)
    
    # Noise level for 2D plot
    noise = 0.001
    v = np.linspace(noise, 1, 99)*np.max(PS)

    cax = ax.contourf(aXX*1e3, aXP*1e3, PS[::-1], v, linestyles = None, zorder=-9)
    ax.set_rasterization_zorder(-1)
    ax.set_aspect('auto')
    
elif plottype is 'imshow':
    h, w = PS.shape
    XX = np.arange(0, w)*Xscale-xc
    XP = np.arange(0, h)*Yscale/drift-xpc
    
    PS[PS==0] = np.nan
    cax = ax.imshow(PS, extent = (XX[0]*1e3, XX[-1]*1e3, XP[0]*1e3, XP[-1]*1e3), aspect = 'auto')
    
cbar = fig.colorbar(cax, fraction = 0.046, pad = 0.04)
cbar.ax.tick_params(labelsize = 11, pad = 2)

if slit is 'X':
    ax.set_xlabel(r'$x$ (mm)')
    ax.set_ylabel(r'$x^{\prime}$ (mrad)')
elif slit is 'Y':
    ax.set_xlabel(r'$y$ (mm)')
    ax.set_ylabel(r'$y^{\prime}$ (mrad)')

#ax.set_xlim(-4, 4)
#ax.set_ylim(-2, 2)

#fig.savefig(fname[0:-4]+'.png')
    

#%% Load .imc and .bkc, process the image and plot, then 2D Gaussian fit
fname = get_file('.imc', initialdir = VC2DIR)
imgs, camInfos, n = PITZ_loadImage(fname)

fname = fname[0:-4]+'.bkc'
bkgs, camInfos, n = PITZ_loadImage(fname)

info = camInfos[0]; print('Scale: ', info.scale)


# Select ROI
tmp = np.mean(imgs, axis = 0)
ROI = getROI(tmp); print('ROI', ROI)
#ROI = [0, 0, w, h]

imgs = imgs[:, ROI[1]:ROI[1]+ROI[3], ROI[0]:ROI[0]+ROI[2]]
bkgs = bkgs[:, ROI[1]:ROI[1]+ROI[3], ROI[0]:ROI[0]+ROI[2]]


#sop = np.sum(np.sum(np.sum(imgs)))/n
#r.append(sop)

# Get background
env, avg, std = getBackground(bkgs, treat = None)
print(np.mean(np.mean(env)))

treat = 'average'
treat = 'envelope'
#treat = None

emcalc_filter = False

fig, ax = plt.subplots()

res = []
for i in np.arange(len(imgs)):
    if treat == 'average':
        tmp = imgs[i]-avg; # print(i, np.max(img))
    elif treat == 'envelope':
        tmp = imgs[i]-env; # print(i, np.max(img))
    else:
        pass
            
    tmp[tmp<0] = 0
    
    if emcalc_filter is True:
        tmp = emcalcFilter(tmp, std, 1, 1)
        
    imgs[i] = tmp
    
    
    [xc, yc, xrms, yrms] = calcRMS(tmp[::-1,::-1], info.scale)
    [xc, yc, x2, y2, xy] = calcCov(tmp[::-1,::-1], info.scale)
    res.append([xc, yc, xrms, yrms])
    print([xc, yc, xrms, yrms])
    print([xc, yc, x2, y2, xy])
    

    title = str.format('Xrms = %.2f mm, Yrms = %.2f mm' % (xrms, yrms))


    ax.imshow(tmp)
    ax.set_title('frame: %d' % i)
    plt.pause(0.01)
    plt.cla()

img = np.mean(imgs, axis = 0)
signal = img

# Filter out noisy pixels
#signal[signal<=20] = 0
#signal[signal>2] = 1

[xc, yc, xrms, yrms] = calcRMS(signal[::-1,::-1], info.scale)
[xc, yc, x2, y2, xy] = calcCov(signal[::-1,::-1], info.scale)

title = str.format('Xrms = %.2f mm, Yrms = %.2f mm' % (xrms, yrms))
#%
plottype = 'imshow'
#plottype = 'contourf'
#plottype= 'pcolormesh'

fig, ax = plt.subplots(figsize = (4, 4))

if plottype is 'contourf':
    h, w = signal.shape
    X = np.arange(0, w)*info.scale-xc
    Y = np.arange(0, h)*info.scale-yc
    aX, aY = np.meshgrid(X, Y)
    
    # Noise level for 2D plot
    noise = 0.001
    v = np.linspace(noise, 1, 99)*np.max(signal)
    
    # 2020/04/13 - make x positive on the right side
    cax = ax.contourf(aX, aY, signal[::-1,::-1], v, linestyles = None, zorder=-9)
    
    #cax = ax.contourf(aX, aY, signal[::-1,::-1], v, linestyles = None, zorder=-9)
    
    ax.set_rasterization_zorder(-1)
    ax.set_aspect('auto')
    
elif plottype is 'imshow':
    h, w = signal.shape
    X = np.arange(0, w)*info.scale-xc
    Y = np.arange(0, h)*info.scale-yc
    
    tmp = np.copy(signal)
    tmp[tmp==0] = np.nan
    # 2020/04/13 - make x positive on the right side
    cax = ax.imshow(tmp[:,::-1], extent = (X[0], X[-1], Y[0], Y[-1]),\
                    aspect = 'auto')
    #cax = ax.imshow(tmp, extent = (X[0], X[-1], Y[0], Y[-1]),\
    #                aspect = 'auto')
    
    ax.set_rasterization_zorder(-1)
    del tmp
    
elif plottype is 'pcolormesh':
    pass

cbar = fig.colorbar(cax, fraction = 0.046, pad = 0.04)
#cbar.set_ticks(np.linspace(0, 900, 10))
cbar.ax.tick_params(labelsize = 12, pad = 2)

#ax.grid()

#ax.set_title(title, fontsize = 12)
ax.set_xlabel(r'$x$ (mm)', fontsize = 12)
ax.set_ylabel(r'$y$ (mm)', fontsize = 12)
mpl.rc('xtick', labelsize = 11, direction = 'in', top   = True)
mpl.rc('ytick', labelsize = 11, direction = 'in', right = True)

fig.savefig(fname[0:-4]+'.png')

#%%
tmp = np.copy(signal)
tmp[tmp<10] = 0
tmp[tmp>=10] = 1
xg, yg, xrms, yrms = calcRMS(tmp[::-1,::-1], info.scale)
xyrms = np.sqrt(xrms*yrms)

xyrms = 1.05
phi = np.linspace(-np.pi, np.pi, 361)
xx = xg-xc+2*xyrms*np.cos(phi)
yy = yg-yc+2*xyrms*np.sin(phi)
ax.plot(xx, yy, '-', color = 'gray')
#ax.set_xlim(-5, 5)
#ax.set_ylim(-5, 5)

fig.savefig(fname[0:-4]+'.png')
#%%
def Gaussian2D(xy, amplitude, xo, yo, sigma_x, sigma_y, theta, offset = 0):
    
    x, y = xy
    xo = float(xo)
    yo = float(yo)    
    a = (np.cos(theta)**2)/(2*sigma_x**2) + (np.sin(theta)**2)/(2*sigma_y**2)
    b = -(np.sin(2*theta))/(4*sigma_x**2) + (np.sin(2*theta))/(4*sigma_y**2)
    c = (np.sin(theta)**2)/(2*sigma_x**2) + (np.cos(theta)**2)/(2*sigma_y**2)
    g = offset + amplitude*np.exp( - (a*((x-xo)**2) + 2*b*(x-xo)*(y-yo) 
                            + c*((y-yo)**2)))
    return g.ravel()

x, y = np.meshgrid(X, Y)
#signal[signal==np.nan] = 0

initial_guess = (800, 0, 0, 1, 1, 0)
popt, pcov = curve_fit(Gaussian2D, (x, y), signal[::-1,:].ravel(),\
                       p0=initial_guess)

data_fitted = Gaussian2D((x, y), *popt).reshape(signal.shape)

fig, ax = plt.subplots(1, 1)
#ax.hold(True)
ax.imshow(signal, cmap=plt.cm.jet,
    extent=(x.min(), x.max(), y.min(), y.max()))
#data_fitted[300:-1] = 0
ax.contour(x, y, data_fitted, 8, colors='w')

plt.grid()
plt.show()

fig, [ax1, ax2] = plt.subplots(1, 2)
ax1.plot(X, np.sum(signal, axis = 0), '-*')
ax1.plot(X, np.sum(data_fitted.reshape(signal.shape), axis = 0), '-')
ax1.grid()

ax2.plot(Y, np.sum(signal, axis = 1)[::-1], '-*')
ax2.plot(Y, np.sum(data_fitted.reshape(signal.shape), axis = 1), '-')
ax2.grid()

#%% 2D interpolation of VC2 image
from scipy.interpolate import RegularGridInterpolator

h, w = signal.shape
X = np.arange(0, w)*info.scale-xg
Y = np.arange(0, h)*info.scale-yg

f2D = RegularGridInterpolator((X, Y), signal[::-1,::-1].T, bounds_error = False, fill_value = 0)

fname = 'beam_500k.ini'
#fname = get_file()

dist = np.loadtxt(fname)
x = dist[:,0]
y = dist[:,1]
w = dist[:,7]

charge = np.sum(w)

r = np.sqrt(x**2+y**2)

Rmax = xyrms
Rmax = 0.75
w = np.where(r<=2*Rmax*1e-3, f2D((x*1e3, y*1e3))*w, 0)

dist[:,7] = w

dist[:,7] *= charge/np.sum(w)

fname = 'beam2D_500k.ini'
#fname = get_file_to_save()
np.savetxt(fname, dist, fmt = '%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%12.4E%4d%4d')

#%% 2D interpolation of QE map
from scipy.interpolate import RegularGridInterpolator

workdir = r'\\afs\ifh.de\group\pitz\doocs\measure\Cathodes\QEmap\2019\20190211N'
workdir = r'\\afs\ifh.de\group\pitz\doocs\measure\Cathodes\QEmap\2020\20200223N'

#os.chdir(workdir)

QE = np.loadtxt('fineQEmap@20200120M.QEs.txt')
aX = np.loadtxt('fineQEmap@20200120M.Xs.txt')
aY = np.loadtxt('fineQEmap@20200120M.Ys.txt')

X = aX[0]
Y = aY[:,0]

#QE[0:16, 1:16] = 0.

f2D_QE = RegularGridInterpolator((X, Y), QE.T/np.max(QE),
                                 bounds_error = False, fill_value = 0)

fig, ax = plt.subplots()

v = np.linspace(0, 1, 49)*np.max(QE)
ax.contourf(aX, aY, QE, v)

#%% Overlap VC2 image with QE map
h, w = signal.shape
X = np.arange(0, w)*info.scale-xg
Y = np.arange(0, h)*info.scale-yg

aX, aY = np.meshgrid(X, Y)
XY = [xy for i, xy in enumerate(zip(aX.flatten(), aY.flatten()))]
ratio = f2D_QE(XY) 

tmp = np.copy(signal[::-1, ::-1])*np.reshape(ratio, aX.shape)
#%%

[xc, yc, xrms, yrms] = calcRMS(tmp, info.scale)
title = str.format('Xrms = %.2f mm, Yrms = %.2f mm' % (xrms, yrms))

#plottype = 'imshow'
plottype = 'contourf'

fig, ax = plt.subplots(figsize = (4, 4))

if plottype is 'contourf':
    h, w = signal.shape
    X = np.arange(0, w)*info.scale-xc
    Y = np.arange(0, h)*info.scale-yc
    aX, aY = np.meshgrid(X, Y)
    
    # Noise level for 2D plot
    noise = 0.001
    v = np.linspace(noise, 1, 99)*np.max(tmp)
    
    # 2020/04/13
    cax = ax.contourf(aX, aY, tmp, v, linestyles = None, zorder=-9)
    
    #cax = ax.contourf(aX, aY, signal[::-1,::-1], v, linestyles = None, zorder=-9)
    
    ax.set_rasterization_zorder(-1)
    ax.set_aspect('auto')
    
elif plottype is 'imshow':
    h, w = signal.shape
    X = np.arange(0, w)*info.scale-xc
    Y = np.arange(0, h)*info.scale-yc
    
    tmp1 = np.copy(tmp)
    tmp1[tmp1==0] = np.nan
    # 2020/04/13
    cax = ax.imshow(tmp1, extent = (X[0], X[-1], Y[0], Y[-1]),\
                    aspect = 'auto')
    #cax = ax.imshow(tmp, extent = (X[0], X[-1], Y[0], Y[-1]),\
    #                aspect = 'auto')
    del tmp1
cbar = fig.colorbar(cax, fraction = 0.046, pad = 0.04)
#cbar.set_ticks(np.linspace(0, 900, 10))
cbar.ax.tick_params(labelsize = 12, pad = 2)

#ax.grid()

ax.set_title(title, fontsize = 12)
ax.set_xlabel(r'$x$ (mm)', fontsize = 12)
ax.set_ylabel(r'$y$ (mm)', fontsize = 12)
mpl.rc('xtick', labelsize = 11, direction = 'in', top   = True)
mpl.rc('ytick', labelsize = 11, direction = 'in', right = True)

fig.savefig(fname[0:-4]+'_QE.png')

#%%
workdir = r'\\afs\ifh.de\group\pitz\doocs\measure\Laser\TemporalProfile\OSS\2019\20190214A'
os.chdir(workdir)

data = np.loadtxt('1644.txt')
select = (data[:,0]<3)|(data[:,0]>15)
baseline = np.mean(data[select][:,1])

data[:,1] -= baseline

func = lambda x, mu, sigma, amp: amp*np.exp(-(x-mu)**2/2/sigma**2)
popt, pcov = curve_fit(func, data[:,0], data[:,1])
mu, sigma, amp = popt

fig, ax = plt.subplots()
ax.plot(data[:,0]-mu, data[:,1]/amp, '.')
ax.plot(data[:,0]-mu, func(data[:,0], *popt)/amp, '-')
ax.grid()

ax.set_xlabel(r'time (ps)')
ax.set_ylabel(r'intensity (arb. unit)')
ax.set_ylim(-0.1, 1.25)
ax.legend(['OSS measurement', 'Gaussian fit'], loc = 'upper left')
fig.savefig('1644oss.eps')

#%%
x = np.array([1.5, 1.7, 2])
y = np.array([2.357, 2.767, 3.150])
z = np.array([2.260, 2.068, 1.248])

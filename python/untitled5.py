# -*- coding: utf-8 -*-
"""
Created on Tue May 26 15:01:00 2020

@author: lixiangk
"""

from universal import *

#%%
workdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim\2020\U3D'
os.chdir(workdir)

data = np.loadtxt('SN26 x+00000_y+000_bscanz.dat', skiprows=48)

fig, ax = plt.subplots(figsize = (12, 4))
ax.plot(data[:,0], data[:,6], '-')

#%%
data = np.loadtxt('fld26.txt')

fig, ax = plt.subplots(figsize = (12, 4))
ax.plot(data[:,0], data[:,1], '-')
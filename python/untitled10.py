# -*- coding: utf-8 -*-
"""
Created on Thu Jun  4 15:31:16 2020

@author: lixiangk
"""

beam0 = pd_loadtxt('../../beam_und_250k_4_shot.warp')
beam0[:,2] -= 0.01

with open('data/data-16.pkl', 'rb') as f:
    pkl = pickle.load(f)
beam1 = pkl['beam']

#%%
k = 2*np.pi/100e-6; ku = 2*np.pi/3e-2; t = 0

theta = (k+ku)*np.copy(beam0[:,2])-k*g_c*t
bf0 = np.mean(np.exp(-theta[:]*1j))
print(1/np.abs(bf0)**2, (4e-9/g_qe), np.abs(bf0)**2*(4e-9/g_qe))

theta = (k+ku)*np.copy(beam1[:,2])-k*g_c*t
bf0 = np.mean(np.exp(-theta[:]*1j))
print(np.abs(bf0)**2, (4e-9/g_qe), np.abs(bf0)**2*(4e-9/g_qe))

#%%
fig, ax = plt.subplots(ncols = 3, figsize = (12, 4))

ax[0].hist(beam0[:,0]*1e3, bins = 100, histtype = r'step')
ax[0].hist(beam1[:,0]*1e3, bins = 100, histtype = r'step')

ax[1].hist(beam0[:,1]*1e3, bins = 100, histtype = r'step')
ax[1].hist(beam1[:,1]*1e3, bins = 100, histtype = r'step')

ax[2].hist(beam0[:,2]*1e3, bins = 100, histtype = r'step')
ax[2].hist(beam1[:,2]*1e3, bins = 100, histtype = r'step')

for axis in ax:
    axis.grid()
    
fig.tight_layout()

#%%
fig, ax = plt.subplots(ncols = 2, figsize = (8, 4))

ax[0].plot(beam0[:,0]*1e3, beam0[:,1]*1e3, '.')
ax[1].plot(beam1[:,0]*1e3, beam1[:,1]*1e3, '.')

for axis in ax:
    axis.grid()
    axis.set_xlim(-7.5, 7.5)
    axis.set_ylim(-2.5, 2.5)
    
fig.tight_layout()

#%%
fig, ax = plt.subplots(ncols = 2, figsize = (8, 4))

ax[0].plot(beam0[:,0]*1e3, beam0[:,3], '.')
ax[1].plot(beam1[:,0]*1e3, beam1[:,3], '.')

for axis in ax:
    axis.grid()
    axis.set_xlim(-7.5, 7.5)
    axis.set_ylim(-0.15, 0.15)
    
fig.tight_layout()

#%%
fig, ax = plt.subplots(ncols = 2, figsize = (8, 4))

ax[0].plot(beam0[:,1]*1e3, beam0[:,4], '.')
ax[1].plot(beam1[:,1]*1e3, beam1[:,4], '.')

for axis in ax:
    axis.grid()
    axis.set_xlim(-2.5, 2.5)
    axis.set_ylim(-0.3, 0.3)
    
fig.tight_layout()
import numpy as np
from scipy.optimize import differential_evolution
from my_object import *

### General functions for beam transfer

def sigma_matrix(fname = None, dist = None):
    if fname is not None:
        diag = BeamDiagnostics(fname)
        sigma_x = np.array([[diag.beta_x, -diag.alpha_x],[-diag.alpha_x, diag.gamma_x]])*diag.emit_x
        sigma_y = np.array([[diag.beta_y, -diag.alpha_y],[-diag.alpha_y, diag.gamma_y]])*diag.emit_y
    if dist is not None:
        diag = BeamDiagnostics(dist = dist)
        sigma_x = np.array([[diag.beta_x, -diag.alpha_x],[-diag.alpha_x, diag.gamma_x]])*diag.emit_x
        sigma_y = np.array([[diag.beta_y, -diag.alpha_y],[-diag.alpha_y, diag.gamma_y]])*diag.emit_y
    if fname is None and dist is None:
        sigma_x = np.array([[0, 0],[0, 0]])
        sigma_y = np.array([[0, 0],[0, 0]])
    return [sigma_y, simga_y]

def drift(L):
    '''
    Parameters
      L: length of the drift, m
    '''
    return np.array([[1, L], [0, 1]])

def G2K(G, P0, qn = 1.):
    '''
    Parameters
      G: gradient, T/m
      P0: momentum, MeV/c
      qn = q/qe: number of charge
    Returns
      K: focal strength, m^-2
    '''
    return G*qn/(P0*1e6/g_c)

def K2G(K, P0, qn = 1.):
    '''
    Parameters
      K: focal strength, m^-2
      P0: momentum, MeV/c
      qn = q/qe: number of charge
    Returns
      G: gradient, T/m
    '''
    return K/(qn/(P0*1e6/g_c))

def quadrupole(K, L):
    '''
    Parameters
      K: focal strength, m^-2
      L: effective length, m
    '''
    
    if K>0:
        kk = np.sqrt(K); kl = kk*L
        cc = np.cos(kl); ss = np.sin(kl)
        tr = np.array([[cc, 1./kk*ss], [-kk*ss, cc]])
    elif K<0:
        kk = np.sqrt(-K); kl = kk*L
        cc = np.cosh(kl); ss = np.sinh(kl)
        tr = np.array([[cc, 1./kk*ss], [kk*ss, cc]])
    else:
        tr = np.array([[1, L], [0, 1]])
    return tr

def multiply(m0, *argv):
    '''
    Parameters
      *argv = m1, m2, m3, ...: variable transfer matrix
    Returns
      Multiplication of the matrix: m3*m2*m1*m0
    '''
    mm = m0
    if len(argv):
        for mi in argv:
            mm = np.matmul(mi, mm)
    return mm

def transport(sigma0, m0, *argv):
    '''
    Parameters
      sigma0: sigma matrix of the beam before the transfer
      m0: first transfer matrix
      *argv: other transfer matrix if any
    Returns
      sigma: sigma matrix of the beam after the transfer, sigma = M*sigma0*M^T
    '''
    M = multiply(m0, *argv)
    sigma = multiply(M.transpose(), sigma0, M)
    return sigma

### Guess the gradients of the second and third quadrupoles for the given gradient of the first, the goal is to focus the beam equally horizontally and vertically

LQUAD = 0.0675 # length of quads
def grads_guess(grad1, quadNames, fname):
    grad1 = grad1/astra_to_sco
    
    diag = BeamDiagnostics(fname)
    sigma10 = np.array([[diag.beta_x, -diag.alpha_x],[-diag.alpha_x, diag.gamma_x]])*diag.emit_x
    sigma20 = np.array([[diag.beta_y, -diag.alpha_y],[-diag.alpha_y, diag.gamma_y]])*diag.emit_y 
    z0 = diag.avg_z
    P0 = kinetic2momentum(diag.Ekin)
    
    zquad = [quads[name] for name in quadNames]
    
    def constructor(x):
        K = [G2K(G, P0) for G in x]
        D1 = drift(zquad[0]-z0-LQUAD/2.)
        Q1 = quadrupole(K[0], LQUAD)
        D2 = drift(zquad[1]-zquad[0]-LQUAD)
        Q2 = quadrupole(K[1], LQUAD)
        D3 = drift(zquad[2]-zquad[1]-LQUAD)
        Q3 = quadrupole(K[2], LQUAD)
        D4 = drift(0.1)

        M = multiply(D1, Q1, D2, Q2, D3, Q3, D4)
        return M

    def obj(x):
        grads = np.array([grad1]+list(x))
    
        M = constructor(grads)
        sigma1 = transport(sigma10, M)

        M = constructor(-grads)
        sigma2 = transport(sigma20, M)
    
        return np.sum(np.abs(1-sigma1/sigma2))

    bounds = [(-grad1*2, -grad1*0.5), (grad1*0.5, grad1*2)]
    res = differential_evolution(obj, bounds = bounds, strategy = 'best1bin', maxiter=10000, init = 'random', atol = 1e-6)
    print(res)
    return res.x*astra_to_sco

def grad2_guess(grad1, quadNames, fname):
    grad1 = grad1/astra_to_sco
    
    diag = BeamDiagnostics(fname)
    sigma10 = np.array([[diag.beta_x, -diag.alpha_x],[-diag.alpha_x, diag.gamma_x]])*diag.emit_x
    sigma20 = np.array([[diag.beta_y, -diag.alpha_y],[-diag.alpha_y, diag.gamma_y]])*diag.emit_y 
    z0 = diag.avg_z
    P0 = kinetic2momentum(diag.Ekin)
    
    zquad = [quads[name] for name in quadNames]
    
    def constructor(x):
        K = [G2K(G, P0) for G in x]
        D1 = drift(zquad[0]-z0-LQUAD/2.)
        Q1 = quadrupole(K[0], LQUAD)
        D2 = drift(zquad[1]-zquad[0]-LQUAD)
        Q2 = quadrupole(K[1], LQUAD)
        D3 = drift(zquad[2]-zquad[1]-LQUAD/2.)

        M = multiply(D1, Q1, D2, Q2, D3)
        return M

    def obj(x):
        grads = np.array([grad1]+list(x))
    
        M = constructor(grads)
        sigma1 = transport(sigma10, M)

        M = constructor(-grads)
        sigma2 = transport(sigma20, M)
    
        return np.abs(1-sigma1[0,0]/sigma2[0,0])

    bounds = [(-grad1*2.5, -grad1*0.5)]
    res = differential_evolution(obj, bounds = bounds, strategy = 'best1bin', maxiter=10000, init = 'random', atol = 1e-6)
    print(res)
    grad2 = res.x[0]
    return grad2*astra_to_sco

def grad3_guess(grad1, quadNames, fname):
    grad1 = grad1/astra_to_sco
    
    diag = BeamDiagnostics(fname)
    sigma10 = np.array([[diag.beta_x, -diag.alpha_x],[-diag.alpha_x, diag.gamma_x]])*diag.emit_x
    sigma20 = np.array([[diag.beta_y, -diag.alpha_y],[-diag.alpha_y, diag.gamma_y]])*diag.emit_y 
    z0 = diag.avg_z
    P0 = kinetic2momentum(diag.Ekin)
    
    zquad = [quads[name] for name in quadNames]

    def constructor(x):
        K = [G2K(G, P0) for G in x]

        D1 = drift(zquad[2]-z0-LQUAD/2.)
        Q1 = quadrupole(K[0], LQUAD)
        D2 = drift(0.1)
    
        M = multiply(D1, Q1, D2)
        return M

    def obj(x):
        grads = np.array(list(x))
    
        M = constructor(grads)
        sigma1 = transport(sigma10, M)

        M = constructor(-grads)
        sigma2 = transport(sigma20, M)
    
        return np.sum(np.abs(1-sigma1/sigma2))

    bounds = [(grad1*0.5, grad1*2)]
    res = differential_evolution(obj, bounds = bounds, strategy = 'best1bin', maxiter=20000, init = 'random', atol = 1e-6)
    print(res)
    grad3 = res.x[0]
    return grad3*astra_to_sco


###
def obj_triplet_Q2(x, *args, **kwargs):
    direc = obj_quads(x, *args, **kwargs); print(direc)
    if 1:
        Run = 1
        if 'Run' in kwargs.keys():
            Run = kwargs['Run']
        if 'Screen' in kwargs.keys():
            Screen = kwargs['Screen']
        fname = str.format('ast.%04.0f.%03d' % (Screen[-1]*100, Run)); # print fname
        
        #if len(args)>0:
        #    direc = args[0]
        #else:
        #    direc = '.'
        #os.chdir(direc)
        
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag = BeamDiagnostics(dist = dist)
        
        std_x,   std_y   =  diag.std_x,   diag.std_y
        cov_x,   cov_y   = -diag.alpha_x*diag.emit_x*1e6, -diag.alpha_y*diag.emit_y*1e6
        
        obj = np.sqrt((1-std_x/std_y)**2+(1-cov_x/cov_y)**2)
        
        os.chdir('..')
    #except:
    #    pass
    
    res = list(x)+list([obj])+list(diag.x)
    filename = '.'+os.sep+'results_Q2_scan_Q1_%.2fT_m.dat' % x[0]
    with open(filename,'a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')
    
    return obj

def obj_triplet_Q3(x, *args, **kwargs):
    direc = obj_quads(x, *args, **kwargs); print(direc)
    if 1:
        if len(args)>0:
            z3 = quads[args[0][2]]
        
        Run = 1
        if 'Run' in kwargs.keys():
            Run = kwargs['Run']
        if 'Screen' in kwargs.keys():
            Screen = kwargs['Screen']
        if 'Zstop' in kwargs.keys():
            Zstop = kwargs['Zstop']
        fname = str.format('ast.%04.0f.%03d' % (Zstop*100, Run)); # print fname
        
        #os.chdir(direc)
        
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag = BeamDiagnostics(dist = dist)
        
        xemit = np.loadtxt('ast.Xemit.%03d' % Run)
        yemit = np.loadtxt('ast.Yemit.%03d' % Run)
        
        select = (xemit[:,0]>z3+0.1)*(xemit[:,0]<Screen[-1])
        xemit = xemit[select]; yemit = yemit[select]
        
        obj = np.sum(xemit[:,3]-yemit[:,3])
        
        os.chdir('..')
    #except:
        #return 0
    
    res = list(x)+list([obj])+list(diag.x)
    filename = '.'+os.sep+'results_Q3_scan_Q1_%.2fT_m.dat' % x[0]
    with open(filename,'a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')
    
    return obj
import sys
if sys.platform == "linux" or sys.platform == "linux2":
    sys.path.append(r'/afs/ifh.de/group/pitz/data/lixiangk/work/sync/python')
    rootdir = r'//afs/ifh.de/group/pitz/data/lixiangk/work'
elif sys.platform == "win32":
    sys.path.append(r'\\afs\ifh.de\group\pitz\data\lixiangk\work\sync\python')
    rootdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\work'
elif sys.platform == "darwin":
    print("OS X")
else:
    print("Unknown platform!")

from astra_modules import *

from timeit import default_timer
import time

from scipy.interpolate import interp1d, interp2d
from scipy.optimize import curve_fit

field_maps = os.path.join(rootdir, 'sync', 'field-maps')

I2B = lambda I: -(0.0000372+0.000588*I)
B2I = lambda B: (-B-0.0000372)/0.000588

# Prepare the interpolation function for the get_MaxE_booster()
data_gun = np.loadtxt(field_maps+os.sep+'phiGun42_scan.dat')
fEG_gun = interp1d(data_gun[:,0], data_gun[:,1])

data_booster = np.loadtxt(field_maps+os.sep+'phi2_scan.dat')
EG_booster = np.reshape(data_booster[:,2], (71, 121)) #-6.2678

phi_booster = np.linspace(-40, 30, 71)
E_booster   = np.linspace(10, 22, 121)

fEG_booster = interp2d(E_booster, phi_booster, EG_booster)

astra_to_sco = 1.590444459638447
TDS_1MV_ratio = 0.07345637 # TO use this ratio, the input Efield will be the TDS voltage in the unit of MV

def get_MaxE_booster(MaxE_gun = 60, phi_gun = 0, phi_booster = 0, Ef = 17.05):

    Eb = np.linspace(10, 22, 121*5)
    EG = fEG_booster(Eb, phi_booster)
    fEb_EG = interp1d(EG, Eb)

    MaxE_booster = fEb_EG(Ef-fEG_gun(phi_gun))
    return np.asscalar(MaxE_booster)

#quads = {'HIGH1.Q1':4.79, 'HIGH1.Q2':5.005, 'HIGH1.Q3':5.6025, 'HIGH1.Q4':5.8525, 'HIGH1.Q5':6.6475,\
#        'HIGH1.Q6':6.8925, 'HIGH1.Q7':8.18, 'HIGH1.Q8':8.655, 'HIGH1.Q9':10.208, 'HIGH1.Q10':10.388,\
#        'PST.QM1':12.088, 'PST.QM2':12.468, 'PST.QM3':12.848, 'PST.QT1':13.228, 'PST.QT2':13.608,\
#        'PST.QT3':13.988, 'PST.QT4':14.368, 'PST.QT5':14.748, 'PST.QT6':15.128, 'HIGH2.Q1':16.635,\
#        'HIGH2.Q2':16.735, 'HIGH2.Q3':19.587, 'HIGH2.Q4':20.8545, 'HIGH2.Q5':22.122, 'HIGH3.Q1':26.350,\
#        'HIGH3.Q2':26.750, 'HIGH3.Q3':27.150}
#quads = {'HIGH1.Q1':4.79, 'HIGH1.Q2':5.005, 'HIGH1.Q3':5.6025, 'HIGH1.Q4':5.8525, 'HIGH1.Q5':6.6475,\
#        'HIGH1.Q6':6.8925, 'HIGH1.Q7':8.18, 'HIGH1.Q8':8.655, 'HIGH1.Q9':10.208, 'HIGH1.Q10':10.388,\
#        'PST.QM1':12.088, 'PST.QM2':12.468, 'PST.QM3':12.848, 'PST.QT1':13.228, 'PST.QT2':13.608,\
#        'PST.QT3':13.988, 'PST.QT4':14.368, 'PST.QT5':14.748, 'PST.QT6':15.128, 'HIGH2.Q1':16.635,\
#        'HIGH2.Q2':16.735, 'HIGH2.Q3':19.587, 'HIGH2.Q4':21.600, 'HIGH2.Q5':23.013, 'HIGH3.Q1':26.350,\
#        'HIGH3.Q2':26.750, 'HIGH3.Q3':27.150} # 2019.10.05
quads = {'HIGH1.Q1':4.79, 'HIGH1.Q2':5.005, 'HIGH1.Q3':5.6025, 'HIGH1.Q4':5.8525, 'HIGH1.Q5':6.6475,\
        'HIGH1.Q6':6.8925, 'HIGH1.Q7':8.18, 'HIGH1.Q8':8.655, 'HIGH1.Q9':10.208, 'HIGH1.Q10':10.388,\
        'PST.QM1':12.088, 'PST.QM2':12.468, 'PST.QM3':12.848, 'PST.QT1':13.228, 'PST.QT2':13.608,\
        'PST.QT3':13.988, 'PST.QT4':14.368, 'PST.QT5':14.748, 'PST.QT6':15.128, 'HIGH2.Q1':16.635,\
        'HIGH2.Q2':16.735, 'HIGH2.Q3':19.587, 'HIGH2.Q4':21.600, 'HIGH2.Q5':23.013, 'HIGH3.Q1':26.350,\
        'HIGH3.Q2':26.750, 'HIGH3.Q3':27.150, 'BIO.Q1':28.10, 'BIO.Q2':28.25, 'BIO.Q3':28.40,
        'V.Q1':0.23, 'V.Q2':1.5175, 'V.Q3':2.5575, 'TEMP.Q1':5.25, 'TEMP.Q2':5.50, 'TEMP.Q3':5.75}

#quads = {'HIGH1.Q1':5.25, 'HIGH1.Q2':5.75, 'HIGH1.Q3':6.25, 'HIGH1.Q4':9.15, 'HIGH1.Q5':9.45, 'HIGH1.Q6':9.75}

scrns = {'LOW.SCR1':0.8030, 'LOW.SCR2':1.3790, 'LOW.SCR3':1.7080, 'HIGH1.SCR1':5.2770, 'HIGH1.SCR2':6.2500,\
        'HIGH1.SCR3':7.1250, 'HIGH1.SCR4':8.4100, 'HIGH1.SCR5':8.9200, 'PST.SCR1':12.2780, 'PST.SCR2':13.0380,\
        'PST.SCR3':13.7980, 'PST.SCR4':14.5580, 'PST.SCR5':15.3180, 'HIGH2.SCR1':16.3030, 'HIGH2.SCR2':18.2620,\
        'HIGH2.SCR3':22.86, 'HIGH3.SCR1':26.950}

steerers = {'LOW.ST1':0.4920, 'LOW.ST2':0.9630, 'LOW.ST3':1.2700, 'LOW.ST4':1.4630, 'LOW.ST5-A':1.9860, 'HIGH1.ST1':4.8950,\
            'HIGH1.STA1':5.4270, 'HIGH1.ST2':7.0400, 'HIGH1.STA2':7.2975, 'HIGH1.ST3':8.1280, 'HIGH1.ST4':9.8230, 'PST.ST1':11.600, \
            'PST.ST2 ':12.390, 'PST.ST3 ':13.150, 'PST.ST4':13.910, 'PST.ST5':14.670, 'PST.ST6':14.981, 'HIGH2.ST1':16.453,\
            'HIGH2.ST2':16.832, 'HIGH2.ST3':19.137, 'HIGH2.ST4':21.400, 'HIGH2.ST5':21.972, 'HIGH3.ST1':26.250, 'HIGH3.ST2':26.550}

# define the objective function
# weights for emittance, beta and alpha, in x and y directions, respectively
WEIGHTS0 = [0, 0, 1, 1, 2, 2]               
# objectives for emittance, beta and alpha, in x and y directions, respectively
OBJECTS0 = [4, 4, 18.056, 0.8726, 8.08, 4.73]

WEIGHTS = WEIGHTS0
OBJECTS = OBJECTS0

P0 = 17.05 # MeV/c
Ek = momentum2kinetic(P0); gamma_r = kinetic2gamma(Ek); bg_r = gamma2bg(gamma_r)

nemit_x, nemit_y = 4e-6, 4e-6 # m*rad
sigma_x, sigma_y, alpha_x, alpha_y = 1.47e-3, 0.32e-3, 8.08, 4.73
cov_x, cov_y = -alpha_x*nemit_x/bg_r, -alpha_y*nemit_y/bg_r

OBJECTS0 = [nemit_x*1e6, nemit_y*1e6, sigma_x*1e3, sigma_y*1e3, cov_x*1e6, cov_y*1e6]

def obj_calc(fname, OBJECTS = OBJECTS0, WEIGHTS = WEIGHTS0):
    
    dist = np.loadtxt(fname)
    dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
    diag = BeamDiagnostics(dist = dist)

    if diag.loss_cathode<=50 and diag.loss_aperture<=2000: 
        # when there is little beam loss to the cathode or to the aperture
        nemit_x, nemit_y =  diag.nemit_x, diag.nemit_y
        # beta_x,  beta_y  =  diag.beta_x,  diag.beta_y
        # alpha_x, alpha_y =  diag.alpha_x, diag.alpha_y
        
        std_x,   std_y   =  diag.std_x,   diag.std_y
        cov_x,   cov_y   = -diag.alpha_x*diag.emit_x*1e6, -diag.alpha_y*diag.emit_y*1e6
        
        obj = np.sqrt(WEIGHTS[0]*(1-nemit_x/OBJECTS[0])**2+WEIGHTS[1]*(1-nemit_y/OBJECTS[1])**2+ 
                      WEIGHTS[2]*(1-std_x/OBJECTS[2])**2+WEIGHTS[3]*(1-std_y/OBJECTS[3])**2+
                      WEIGHTS[4]*(1-cov_x/OBJECTS[4])**2+WEIGHTS[5]*(1-cov_y/OBJECTS[5])**2)
        #obj = np.sqrt(WEIGHTS[0]*(nemit_x-OBJECTS[0])**2+WEIGHTS[1]*(nemit_y-OBJECTS[1])**2+ 
        #              WEIGHTS[2]*(std_x-OBJECTS[2])**2+WEIGHTS[3]*(std_y-OBJECTS[3])**2+
        #              WEIGHTS[4]*(cov_x-OBJECTS[4])**2+WEIGHTS[5]*(cov_y-OBJECTS[5])**2)
    else:
        # if there is beam loss, then set the objective to 1000
        obj = 1e3
    return [obj, nemit_x, nemit_y, std_x, std_y, cov_x, cov_y, diag.alpha_x, diag.alpha_y]

def obj_calc4(fname, OBJECTS = OBJECTS0, WEIGHTS = WEIGHTS0):
    
    dist = np.loadtxt(fname)
    dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
    diag = BeamDiagnostics(dist = dist)

    if diag.loss_cathode<=50 and diag.loss_aperture<=2000: 
        # when there is little beam loss to the cathode or to the aperture
        nemit_x, nemit_y =  diag.nemit_x, diag.nemit_y
        # beta_x,  beta_y  =  diag.beta_x,  diag.beta_y
        # alpha_x, alpha_y =  diag.alpha_x, diag.alpha_y
        
        std_x,   std_y   =  diag.std_x,   diag.std_y
        cov_x,   cov_y   = -diag.alpha_x*diag.emit_x*1e6, -diag.alpha_y*diag.emit_y*1e6
        
        #obj = np.sqrt(WEIGHTS[0]*(1-nemit_x/OBJECTS[0])**2+WEIGHTS[1]*(1-nemit_y/OBJECTS[1])**2+ 
        #              WEIGHTS[2]*(1-std_x/OBJECTS[2])**2+WEIGHTS[3]*(1-std_y/OBJECTS[3])**2+
        #              WEIGHTS[4]*(1-cov_x/OBJECTS[4])**2+WEIGHTS[5]*(1-cov_y/OBJECTS[5])**2)
        obj = np.sqrt(WEIGHTS[0]*(nemit_x-OBJECTS[0])**2+WEIGHTS[1]*(nemit_y-OBJECTS[1])**2+ 
                      WEIGHTS[2]*(std_x-OBJECTS[2])**2+WEIGHTS[3]*(std_y-OBJECTS[3])**2+
                      WEIGHTS[4]*(cov_x-OBJECTS[4])**2+WEIGHTS[5]*(cov_y-OBJECTS[5])**2)
    else:
        # if there is beam loss, then set the objective to 1000
        obj = 1e3
    return [obj, nemit_x, nemit_y, std_x, std_y, cov_x, cov_y, diag.alpha_x, diag.alpha_y]

def obj_FlashScan(x, *args):
    
    '''
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''
    
    WEIGHTS = [0.25, 0.25, 0., 0., 0., 0.]   # weights for emittance, beta and gamma and
    OBJECTS = [0,       0, 0,  0,  0,  0]    # objectives for emittance, beta and gamma, in x and y directions, respectively
    
    Ipart = 250000
    BSA = x[1]
    
    sigma_x = sigma_y = BSA/4.
    C_sigma_x = C_sigma_y = 0
    
    #sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    #C_sigma_x = C_sigma_y = BSA/2./sigma_x
    
    #sigma_x = sigma_y = 1.25 #0.96 # Gaussian truncated
    #C_sigma_x = C_sigma_y = BSA/2./sigma_x
    
    MaxE = x[2:9:2] 
    phi = x[3:10:2]
    
    MaxB = x[10:12]
    
    Q_total = x[0]/1e3
    
    field_maps = rootdir+os.sep+'sync'+os.sep+'field-maps3'
    
    generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'positrons', Q_total = Q_total,\
                          Ref_Ekin = 0.3e-6, dist_pz = 'g',\
                          Dist_z = 'g', sig_clock = 8e-3, C_sig_clock = 3, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0.7,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0.7,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)
    #generator.set(Dist_x = 'r', sig_x = BSA/4.0, Dist_y = 'r', sig_y = BSA/4.0)
    #generator.set(Dist_z = 'p', Lt = x[-1]*1e-3, Rt = 2e-3)
    
    Distribution = 'beam.ini'
    Distribution = 'ast.0500.003'
    
    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = Distribution, CathodeS = True,\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    newrun.set(Xoff = 0., Yoff = 0., Run = 10)
    #newrun.set(Qbunch = Q_total)
    #newrun.set(Run = 1, XYrms = sigma_x)
    
    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 20, Nlong_in = 50, N_min = 50, Max_scale = 0.05, Max_count = 20)
    charge.set(L2D_3D = True, Z_TRANS = 4.8, NXF = 32, NYF = 32, NZF = 32, min_grid_trans = 0.03e-3)
    
    
    fieldNames = ['DCgun.dat', 'Buncher-20cm.dat', 'tesla-9Cell-01.dat', 'tesla-9Cell-02.dat']
    cavity = Module('Cavity', LEfield = True, File_Efield = 
                    [field_maps+os.sep+fieldName for fieldName in fieldNames]+['..'+os.sep+'DC-3D_100x20x1000mm'],\
                    MaxE = list(MaxE)+[1], Phi = list(phi)+[0], Ex_stat = [False, False, False, False, True],
                    C_pos = [0., 0.8, 1.89388, 3.28388, 6.5], Nue = [0, 1.3, 1.3, 1.3, 0])
    
    fieldNames = ['Solenoid20160422-covered3.dat', 'Solenoid20160329.dat']
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+fieldName for fieldName in fieldNames],\
                    MaxB = MaxB, S_pos = [0.2425, 1.2], S_xrot = [0*1e-3], S_yrot = [0])
    
    output = Module('Output', Zstart = 0, Zstop = 8, Zemit = 800, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, C_EmitS = True, LPROJECT_EMIT = True,\
                    LOCAL_EMIT = True, Screen = [7.5, 8.0])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    
    #Qsol = 0 #-0.0034*x[-2]/365.0
    #Qsol_zrot = 0 # -0.4363 #x[-1]/180.0*np.pi
    #quadru = Module('Quadrupole', LQuad = True, Q_type = ['../Qsol.data'], Q_pos = [0], Q_grad = [Qsol], Q_zrot = [Qsol_zrot])
    
    gg = [0.817382*astra_to_sco, -1.579726*astra_to_sco, 0.856139*astra_to_sco]
    quadru = Module('Quadrupole', LQuad = True,\
                    Q_type = ['../Q3.data', '../Q3.data', '../Q3.data'],\
                    Q_pos = [quads['TEMP.Q1'], quads['TEMP.Q2'], quads['TEMP.Q3']], Q_grad = gg)
    
    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, quadru, output])
    
    
    #direc = str.format('Q-%.2fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
    #                   (np.abs(Q_total)*1e3, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    
    direc = str.format('B1-%.0fGs-B2-%.0fGs' % (MaxB[0]*1e4, MaxB[1]*1e4))
    #direc = str.format('B1-%.0fGs-B2-%.0fGs-B-%.2fdeg-A1-%.0fdeg' % (MaxB[0]*1e4, MaxB[1]*1e4, phi[-3], phi[-2]))
    ###
    os.system('mkdir -p '+direc)
    #os.chdir(direc)
    
    job_name = direc
    #job_name = 'myjob.'+('BSA-%.2fmm-I-%.2fA' % (x[0], x[3]))
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, gen_name, direc)
    
    #os.system('qsub '+job_name+'.sh')
    return
    ###
    
    os.system('mkdir -p '+direc)
    os.chdir(direc)
    
    generator.write('gen.in')
    astra.write('ast.in')
    
    os.system('generator gen.in 2>&1 | tee gen.log')
    os.system('astra ast.in 2>&1 | tee ast.log')
    
    return
    
    try:
        fname = 'ast.0528.001'
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag = BeamDiagnostics(dist = dist)
        
        fname = 'ast.Xemit.001'
        xemit = np.loadtxt(fname)
        select = (xemit[:,0]>5.0)
        xemit = xemit[select]
        nemit_x = np.mean(xemit[:,5])
        
        fname = 'ast.Zemit.001'
        zemit = np.loadtxt(fname)       
        select = (zemit[:,0]>5.0)
        zemit = zemit[select]
        
        f = lambda x, a, b: a+b*x
        popt, pcov = curve_fit(f, zemit[:,0], zemit[:,6])
        cov = f(29.25, *popt)
        
        if diag.loss_cathode<=500 and diag.loss_aperture<=500: 
            # when there is little beam loss to the cathode or to the aperture
            if cov>0:
                obj = nemit_x/4+np.fabs(cov/10)
            else:
                obj = nemit_x/4+np.fabs(cov/50)
        else:
            # if there is beam loss, then set the objective to 1000
            obj = 1e3   
        
    except:
    #    # if no output beam file, then set the objective to 10000
        obj = 1e4
        os.chdir('..')
        return
    #    print('Error: not accelerated well!')

    os.chdir('..'); #os.system('rm -r '+direc)

    res = [Ipart, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, nemit_x, cov, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')
    
    return obj

def obj_FlashScan2(x, *args):
    
    '''
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''
    
    WEIGHTS = [0.25, 0.25, 0., 0., 0., 0.]   # weights for emittance, beta and gamma and
    OBJECTS = [0,       0, 0,  0,  0,  0]    # objectives for emittance, beta and gamma, in x and y directions, respectively
    
    Ipart = 250000
    BSA = x[1]
    
    sigma_x = sigma_y = BSA/4.
    C_sigma_x = C_sigma_y = 0
    
    #sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    #C_sigma_x = C_sigma_y = BSA/2./sigma_x
    
    #sigma_x = sigma_y = 1.25 #0.96 # Gaussian truncated
    #C_sigma_x = C_sigma_y = BSA/2./sigma_x
    
    MaxE = x[2:9:2] 
    phi = x[3:10:2]
    
    MaxB = x[10:12]
    
    Q_total = x[0]/1e3
    
    field_maps = rootdir+os.sep+'sync'+os.sep+'field-maps3'
    
    generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'positrons', Q_total = Q_total,\
                          Ref_Ekin = 0.3e-6, dist_pz = 'g',\
                          Dist_z = 'g', sig_clock = 8e-3, C_sig_clock = 3, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0.7,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0.7,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)
    #generator.set(Dist_x = 'r', sig_x = BSA/4.0, Dist_y = 'r', sig_y = BSA/4.0)
    #generator.set(Dist_z = 'p', Lt = x[-1]*1e-3, Rt = 2e-3)
    
    Distribution = 'beam.ini'
    #Distribution = 'ast.0050.005'
    
    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = Distribution, CathodeS = True,\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    newrun.set(Xoff = 0., Yoff = 0., Run = 1)
    #newrun.set(Qbunch = Q_total)
    #newrun.set(Run = 1, XYrms = sigma_x)
    
    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 20, Nlong_in = 50, N_min = 50, Max_scale = 0.05, Max_count = 20)
    #charge.set(L2D_3D = True, Z_TRANS = 0.35, NXF = 32, NYF = 32, NZF = 32, min_grid_trans = 0.03e-3)
    
    fieldNames = ['DCgun.dat', 'Buncher-20cm.dat', 'tesla-4Cell-01.dat', 'tesla-4Cell-02.dat']
    cavity = Module('Cavity', LEfield = True, File_Efield = 
                    [field_maps+os.sep+fieldName for fieldName in fieldNames],\
                    MaxE = MaxE, Phi = phi,\
                    C_pos = [0., 0.8, 2.039, 2.865], Nue = [0, 1.3, 1.3, 1.3])
    
    fieldNames = ['Solenoid20160422-covered3.dat', 'Solenoid20160329.dat']
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+fieldName for fieldName in fieldNames],\
                    MaxB = MaxB, S_pos = [0.2425, 1.2], S_xrot = [0*1e-3], S_yrot = [0])
    
    output = Module('Output', Zstart = 0, Zstop = 5, Zemit = 1000, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, C_EmitS = True, LPROJECT_EMIT = True,\
                    LOCAL_EMIT = True, Screen = [4.0])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    
    #Qsol = 0 #-0.0034*x[-2]/365.0
    #Qsol_zrot = 0 # -0.4363 #x[-1]/180.0*np.pi
    #quadru = Module('Quadrupole', LQuad = True, Q_type = ['../Qsol.data'], Q_pos = [0], Q_grad = [Qsol], Q_zrot = [Qsol_zrot])
    
    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, output])
    
    
    #direc = str.format('Q-%.2fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
    #                   (np.abs(Q_total)*1e3, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    
    direc = str.format('B3-%.0fGs-B4-%.0fGs' % (MaxB[0]*1e4, MaxB[1]*1e4))
    ###
    os.system('mkdir -p '+direc)
    #os.chdir(direc)
    
    job_name = direc
    #job_name = 'myjob.'+('BSA-%.2fmm-I-%.2fA' % (x[0], x[3]))
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, gen_name, direc)
    
    #os.system('qsub '+job_name+'.sh')
    return
    ###
    
    os.system('mkdir -p '+direc)
    os.chdir(direc)
    
    generator.write('gen.in')
    astra.write('ast.in')
    
    os.system('generator gen.in 2>&1 | tee gen.log')
    os.system('astra ast.in 2>&1 | tee ast.log')
    
    return
    
    try:
        fname = 'ast.0528.001'
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag = BeamDiagnostics(dist = dist)
        
        fname = 'ast.Xemit.001'
        xemit = np.loadtxt(fname)
        select = (xemit[:,0]>5.0)
        xemit = xemit[select]
        nemit_x = np.mean(xemit[:,5])
        
        fname = 'ast.Zemit.001'
        zemit = np.loadtxt(fname)       
        select = (zemit[:,0]>5.0)
        zemit = zemit[select]
        
        f = lambda x, a, b: a+b*x
        popt, pcov = curve_fit(f, zemit[:,0], zemit[:,6])
        cov = f(29.25, *popt)
        
        if diag.loss_cathode<=500 and diag.loss_aperture<=500: 
            # when there is little beam loss to the cathode or to the aperture
            if cov>0:
                obj = nemit_x/4+np.fabs(cov/10)
            else:
                obj = nemit_x/4+np.fabs(cov/50)
        else:
            # if there is beam loss, then set the objective to 1000
            obj = 1e3   
        
    except:
    #    # if no output beam file, then set the objective to 10000
        obj = 1e4
        os.chdir('..')
        return
    #    print('Error: not accelerated well!')

    os.chdir('..'); #os.system('rm -r '+direc)

    res = [Ipart, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, nemit_x, cov, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')
    
    return obj


def obj_2nC_100um(x):
    '''
    Created on November 13, 2019
    Simulation from photocathode via EMSY1 at 5.277 m to 20 m
    Goal function is a combination of average emittance and correlated energy spread exterpolated at undulator center, 29.25 m
    Laser spot fixed at 4 mm, gun phase at MMMG
    Variables to be optimized: gun and booster phases, solenoid current
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    temp = np.loadtxt('booster-grad2-vs-phase2.dat')
    func_phase2grad = interp1d(temp[:,0], temp[:,1])

    WEIGHTS = [0.25, 0.25, 0., 0., 0., 0.]   # weights for emittance, beta and gamma and
    OBJECTS = [0,       0, 0,  0,  0,  0]    # objectives for emittance, beta and gamma, in x and y directions, respectively
    
    Ipart = 100000
    
    BSA = x[0]
    sigma_x = sigma_y = BSA/4.
    C_sigma_x = C_sigma_y = 0
    
    #sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    #C_sigma_x = C_sigma_y = BSA/2./sigma_x
    
    
    MaxE_gun = x[1]
    phi_gun = x[2]
    MaxB = x[3]
    
    #MaxE_booster = get_MaxE_booster(MaxE_gun, phi_gun, phi_booster, 22.0)
    MaxE_booster1 = x[4]
    phi_booster1 = x[5]
    MaxE_booster2 = x[6]
    phi_booster2 = x[7]
    r = func_phase2grad([phi_booster2])
    MaxE_booster2 = r[0]
    #print(type(MaxE_booster2))
    
    Q_total = -2.0
    
    field_maps = rootdir+os.sep+'sync'+os.sep+'field-maps2'
    field_maps = '..'+os.sep+'field-maps2'
    
    generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'g', sig_clock = 10e-3/np.sqrt(2*np.pi), Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)
    
    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = 'beam.ini',\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000, Qbunch = Q_total)
    #newrun.set(Run = 1, Track_All = False)
    
    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 40, Nlong_in = 50, N_min = 50, Max_scale = 0.05, Max_count = 20)
    #charge.set(LSPCH = False, Lmirror = False)
    
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun_PITZ.dat', field_maps+os.sep+'tesla-9Cell-01.dat', field_maps+os.sep+'tesla-9Cell-02.dat'],\
                    MaxE = [MaxE_gun, MaxE_booster1, MaxE_booster2], C_pos = [0., 2.09388, 3.48388], Nue = [1.3, 1.3, 1.3], Phi = [phi_gun, phi_booster1, phi_booster2])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'solen_MaRIE.dat'], MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = 10, Zemit = 200, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, C_EmitS = True, LPROJECT_EMIT = True,\
                    LOCAL_EMIT = True, Screen = [6, 9])
    #apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    
    
    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, output])

    direc = str.format('Q%.1fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-E3-%.2fMV_m-phi3-%.2fdeg-I-%.3fT' %\
                       (Q_total*1e3, BSA, MaxE_gun, phi_gun, MaxE_booster1, phi_booster1, MaxE_booster2, phi_booster2, MaxB))
    #direc = 'temp'
    os.system('mkdir -p '+direc)
    
    job_name = 'D-%.2fmm-B-%.3fT-boo2_%.0fdeg' % (BSA, MaxB, phi_booster2)
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, gen_name, direc)
    return 0
    
    os.chdir(direc)
    
    generator.write('gen.in')
    astra.write('ast.in')
    #os.system('generator gen.in 2>&1 | tee gen.log')
    #os.system('astra ast.in 2>&1 | tee ast.log')
    
    #fname = 'ast.0600.001'
    #data = np.loadtxt(fname)
    #data[1:,2] += data[0,2]; data[1:,5] += data[0,5]
    
    #plot2d_zpz(fname, fig_ext = '@boo2_%.0fdeg.png' % (phi_booster2), vmin = 0.01, bins = (200, 200))
    os.chdir('..')
    
    astra.qsub(job_name, ast_name, gen_name, direc)
    
    return 0
    
    #astra.run0('ast.in')
    #import pdb; pdb.set_trace();
    print(x)
    try:
        fname = 'ast.0528.001'
        #dist = np.loadtxt(fname)
        #dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        #diag = BeamDiagnostics(fname = fname)
        
        fname = 'ast.ref.001'
        ref = np.loadtxt(fname)
        
        res = list(x) + list(ref[-1])
        with open('..'+os.sep+'ParaScan2.dat','a') as f_handle:
            np.savetxt(f_handle, np.atleast_2d(res), fmt='%14.6E')

    except:
        # if no output beam file, then set the objective to 10000
        nemit_x, cov, I1 = 99, 99, 99
        obj = [99, 99, 99]
        print('Error: not accelerated well!')
    
    os.chdir('..'); #os.system('rm -r '+direc)
    
    return 0

def obj_2nC_100um2(x):
    '''
    Created on November 13, 2019
    Simulation from photocathode via EMSY1 at 5.277 m to 20 m
    Goal function is a combination of average emittance and correlated energy spread exterpolated at undulator center, 29.25 m
    Laser spot fixed at 4 mm, gun phase at MMMG
    Variables to be optimized: gun and booster phases, solenoid current
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    temp = np.loadtxt('booster-grad-vs-phase.dat')
    func_phase2grad = interp1d(temp[:,0], temp[:,1])

    WEIGHTS = [0.25, 0.25, 0., 0., 0., 0.]   # weights for emittance, beta and gamma and
    OBJECTS = [0,       0, 0,  0,  0,  0]    # objectives for emittance, beta and gamma, in x and y directions, respectively
    
    Ipart = 200000
    
    BSA = x[0]
    sigma_x = sigma_y = BSA/4.
    C_sigma_x = C_sigma_y = 0
    
    #sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    #C_sigma_x = C_sigma_y = BSA/2./sigma_x
    
    MaxE_gun = x[1]
    phi_gun = x[2]
    MaxB = x[3]
    
    #MaxE_booster = get_MaxE_booster(MaxE_gun, phi_gun, phi_booster, 22.0)
    MaxE_booster1 = x[4]
    phi_booster1 = x[5]
    
    MaxE_booster2 = x[6]
    phi_booster2 = x[7]
    
    r = func_phase2grad([phi_booster2])
    MaxE_booster2 = r[0]
    
    phi_booster1 = phi_booster2 
    MaxE_booster1 = MaxE_booster2
    #print(type(MaxE_booster2))
    
    Q_total = -2.0
    
    field_maps = rootdir+os.sep+'sync'+os.sep+'field-maps2'
    field_maps = '..'+os.sep+'field-maps2'
    
    generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'g', sig_clock = 10e-3/np.sqrt(2*np.pi), Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)
    
    newrun = Module('Newrun', Run = 2, Head = 'PITZ beam line simulation', Distribution = 'beam.ini',\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False,
                    Max_step=200000, Qbunch = Q_total)
    #newrun.set(Run = 1, Track_All = False)
    
    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 40, Nlong_in = 50, N_min = 50, Max_scale = 0.05, Max_count = 20)
    #charge.set(LSPCH = False, Lmirror = False)
    
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun_PITZ.dat', field_maps+os.sep+'tesla-9Cell-01.dat', field_maps+os.sep+'tesla-9Cell-02.dat'],\
                    MaxE = [MaxE_gun, MaxE_booster1, MaxE_booster2], C_pos = [0., 2.09388-0.6, 3.48388-0.6], Nue = [1.3, 1.3, 1.3], Phi = [phi_gun, phi_booster1, phi_booster2])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'solen_MaRIE.dat'], MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = 10, Zemit = 200, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, C_EmitS = True, LPROJECT_EMIT = True,\
                    LOCAL_EMIT = True, Screen = [5, 9])
    #apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    
    
    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, output])

    direc = str.format('Q%.1fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-E3-%.2fMV_m-phi3-%.2fdeg-I-%.3fT' %\
                       (Q_total*1e3, BSA, MaxE_gun, phi_gun, MaxE_booster1, phi_booster1, MaxE_booster2, phi_booster2, MaxB))
    #direc = 'temp'
    os.system('mkdir -p '+direc)
    
    job_name = 'D-%.2fmm-B-%.3fT-boo_%.0fdeg' % (BSA, MaxB, phi_booster2)
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, gen_name, direc)
    return 0
    
    os.chdir(direc)
    
    generator.write('gen.in')
    astra.write('ast.in')
    os.system('generator gen.in 2>&1 | tee gen.log')
    os.system('astra ast.in 2>&1 | tee ast.log')
    
    #fname = 'ast.0600.001'
    #data = np.loadtxt(fname)
    #data[1:,2] += data[0,2]; data[1:,5] += data[0,5]
    
    #plot2d_zpz(fname, fig_ext = '@boo_%.0fdeg.png' % (phi_booster2), vmin = 0.01, bins = (200, 200))
    #os.chdir('..')
    
    #astra.qsub(job_name, ast_name, gen_name, direc)
    
    #return 0
    
    #astra.run0('ast.in')
    #import pdb; pdb.set_trace();
    print(x)
    try:
        fname = 'ast.0528.001'
        #dist = np.loadtxt(fname)
        #dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        #diag = BeamDiagnostics(fname = fname)
        
        fname = 'ast.ref.001'
        ref = np.loadtxt(fname)
        
        res = list(x) + list(ref[-1])
        with open('..'+os.sep+'BooParaScan.dat','a') as f_handle:
            np.savetxt(f_handle, np.atleast_2d(res), fmt='%14.6E')

    except:
        # if no output beam file, then set the objective to 10000
        nemit_x, cov, I1 = 99, 99, 99
        obj = [99, 99, 99]
        print('Error: not accelerated well!')
    
    os.chdir('..'); #os.system('rm -r '+direc)
    
    return 0

####
def RotationMatrix(theta):
    cc = np.cos(theta)
    ss = np.sin(theta)
    return np.array([[cc, -ss], [ss, cc]])

def RotatedBy(X, theta, Xc = [0, 0]):
    M = RotationMatrix(theta)
    X = np.reshape(X, (len(X), 1))
    Xc = np.reshape(Xc, (len(Xc), 1))
    try:
        #M1 = M @ (X-Xc)+Xc
        M1 = np.matmul(M, X-Xc)+Xc
    except Exception as err:
        print(err)
        return
    return M1

def PrepareDipoleCoordinates(rho, theta, poleWidth = 0, X0 = [0, 0],
                             theta0 = 0, beta1 = 0, beta2 = 0):
    '''
    Return four pairs of coordinates for the four corners of the pole face, in the form of 
    (x, z) in unit of meter.
    |------
    |       ---           
    A1          - |
    |          --B1  
    A          |
    |        B
    A2     | 
    |   B2
    | |
    C
    '''
    if poleWidth == 0:
        poleWidth = rho
    width = poleWidth/2
    sg = np.sign(theta)
    
    X0 = np.reshape(X0, (2, 1))
    Xa = np.reshape([0, 0], (2, 1))
    Xc = np.reshape([0, rho*sg], (2, 1))
    
    Xa1 = np.reshape([0,  width], (2, 1))+Xa
    Xa2 = np.reshape([0, -width], (2, 1))+Xa
    
    Xb = RotatedBy(Xa-Xc, theta)+Xc
    Xb1 = RotatedBy(Xa1-Xc, theta)+Xc
    Xb2 = RotatedBy(Xa2-Xc, theta)+Xc
    
    Xa1 = RotatedBy(Xa1-Xa, beta1)+Xa
    Xa2 = RotatedBy(Xa2-Xa, beta1)+Xa
    
    Xb1 = RotatedBy(Xb1-Xb, beta2)+Xb
    Xb2 = RotatedBy(Xb2-Xb, beta2)+Xb
    
    #
    V = np.concatenate((Xa1, Xa2, Xb1, Xb2), axis=1) # 2x4
    M = RotationMatrix(theta0) # 2x2
    #V = (M@V).T # (2x4).T -> 4x2
    V = np.matmul(M, V).T
    
    V[:,0] += X0[1]
    V[:,1] += X0[0]
    
    return [vi[::-1] for vi in V]


def obj_dogleg(x, *args, **kwargs):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameters:
      x: an array or list of the variables to be optimized, here the gradients of quads
      *args: the names of quads
      **kwargs: key-value pairs of Astra module property, e.g., 'Run', 1
    Returns:
      direc: the directory where Astra simulation runs
    '''
    
    z0 = 26
    lq = 0.0675
    
    L, rho, theta = 1.5, 0.4, np.pi/3
    
    poleWidth = 0.36
    gap = 50e-3
    Bdip = 0.1077634*1.5
    
    l1, l2, l3, g1, g2, rr = x
    
    Run = 10
    Distribution = '..'+os.sep+'ast.2550.007'
    Zstop = 28
    Screen = [27.5, 27.8]
    if len(kwargs)>0:
        if 'Run' in list(kwargs.keys()):
            Run = kwargs['Run']

        if 'Distribution' in list(kwargs.keys()):
            Distribution = kwargs['Distribution']

        if 'Zstop' in list(kwargs.keys()):
            Zstop = kwargs['Zstop']

        if 'Screen' in list(kwargs.keys()):
            Screen = kwargs['Screen']

    newrun = Module('Newrun', Run = Run, Head = 'PITZ beam line simulation', Distribution = Distribution,\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    #newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 30, Nlong_in = 50, N_min = 10,\
                    NXF = 32, NYF = 32, NZF = 32, L2D_3D = True, Z_TRANS = 5.0, Max_scale = 0.05, Max_count = 20)
    #charge.set(LSPCH = False, L2D_3D = False)
    #charge.set(NXF = 16, NYF = 16, NZF = 16)
    
    output = Module('Output', Zstart = 0, Zstop = Zstop, Zemit = int(Zstop*50), Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = Screen)
    
    # Quadrupole
    Xb = np.array([rho*(np.cos(theta)-1), z0+rho*np.sin(theta)]) # (x, z)
    L0 = (L+Xb[0]*2)/np.sin(theta);
    
    L1 = np.array([l1+lq/2, l1+lq+l2+lq/2, L0/2+l3+lq/2, L0-l1-lq/2]); print(L1, L0, L)
    Qs = np.array([Xb[0]-L1*np.sin(theta), Xb[1]+L1*np.cos(theta)]) # (x, z)
    
    quadru = Module('Quadrupole', LQuad = True, 
                    Q_type = ['../Q3.data' for _ in np.arange(4)],
                    Q_pos = Qs[1], 
                    Q_grad = [g1*rr, g2, g2, g1*rr],
                    Q_xoff = Qs[0],
                    Q_xrot = [-theta for _ in np.arange(4)])
    
    # Dipole
    X11, X12, X13, X14 = PrepareDipoleCoordinates(rho, -theta, poleWidth = poleWidth, theta0 = 0, X0 = [0, z0]) # (x, z)
    
    X0 = [Xb[0]-L0*np.sin(theta), Xb[1]+L0*np.cos(theta)] # (x, z)
    X21, X22, X23, X24 = PrepareDipoleCoordinates(rho, theta, poleWidth = poleWidth, theta0 = -theta, X0 = X0)
    
    D_type = ['hor', 'hor']; D_strength = [-Bdip, Bdip]; D_radius = [rho, -rho]
    D1 = [X11, X21]; D2 = [X12, X22]; D3 = [X13, X23]; D4 = [X14, X24]; #print(D1)
    
    D_Gap = [[gap, gap], [gap, gap]]
    
    dipole = Module('Dipole', LDipole = True, D_type = D_type, D1 = D1, D2 = D2, D3 = D3, D4 = D4,
                    D_Gap = D_Gap, D_strength = D_strength, D_radius = D_radius)
                    
    
    astra = Astra()
    astra.add_modules([newrun, charge, quadru, dipole, output])
    
    direc = ''
    for i in np.arange(len(x)):
        direc += str.format('%.2f-' %  (x[i]))
    direc = direc[:-1]
    
    ###
    
    os.system('mkdir -p '+direc)
    #os.chdir(direc)
    
    job_name = 'myjob_'+direc
    #gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    #generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, None, direc)
    
    return direc
    ###
    
    #os.system('mkdir -p '+direc)
    try:
        os.mkdir(direc)
        pass
    except OSError as err:
        #if err.errno != errno.EEXIST:
        #    raise
        pass
    
    os.chdir(direc)
    
    generator.write('gen.in')
    astra.write('ast.in')
    
    #os.system('generator gen.in 2>&1 | tee gen.log')
    #os.system('astra ast.in 2>&1 | tee ast.log')
    
    os.system('astra ast.in')
    
    #os.chdir('..')
    
    return direc
####
def obj_achromat(x, *args, **kwargs):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameters:
      x: an array or list of the variables to be optimized, here the gradients of quads
      *args: the names of quads
      **kwargs: key-value pairs of Astra module property, e.g., 'Run', 1
    Returns:
      direc: the directory where Astra simulation runs
    '''
    
    z0 = 8
    lq = 0.0675
    
    
    rho, theta = 0.2, np.pi/4
    L = 1.2-rho*(1-np.cos(theta))*2 # length of the projection onto z of the straight line in between the dipoles 
    Larm = 0.5*2+lq*3
    
    beta = 6.504661e-2
    dz = rho*np.sin(beta)
    dx = dz*np.tan(beta)
    dxoff = rho*np.cos(beta)+dx-rho
    
    
    poleWidth = 0.20
    gap = 20e-3
    Bdip = 2.56895e-1
    
    #l1, l2, l3, g1, g2, rr = x
    l1, l2 = 0.5998, 0.0646
    
    Run = 19
    Distribution = '.'+os.sep+'ast.0800.012'
    Distribution = '.'+os.sep+'ast.1037.017'
    Zstop = 8+rho*np.sin(theta)*2+(l1*2+l2*2+lq*3)*np.cos(theta)+Larm; print(Zstop)
    #Zstop = 10.40
    
    Screen = [Zstop-0.2]
    if len(kwargs)>0:
        if 'Run' in list(kwargs.keys()):
            Run = kwargs['Run']

        if 'Distribution' in list(kwargs.keys()):
            Distribution = kwargs['Distribution']

        if 'Zstop' in list(kwargs.keys()):
            Zstop = kwargs['Zstop']

        if 'Screen' in list(kwargs.keys()):
            Screen = kwargs['Screen']

    newrun = Module('Newrun', Run = Run, Head = 'PITZ beam line simulation', Distribution = Distribution,\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    #newrun.set(Track_All = False)
    
    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 30, Nlong_in = 50, N_min = 10,\
                    NXF = 32, NYF = 32, NZF = 32, L2D_3D = True, Z_TRANS = 5.0, Max_scale = 0.05, Max_count = 20)
    #charge.set(LSPCH = False, L2D_3D = False)
    #charge.set(NXF = 16, NYF = 16, NZF = 16)
    
    output = Module('Output', Zstart = 0, Zstop = Zstop, Zemit = int(Zstop*50), Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = Screen)
    
    # Quadrupole
    Xb = np.array([rho*(1-np.cos(theta)), z0+rho*np.sin(theta)]) # (x, z)
    print(Xb)
    L0 = (L+Xb[0]*2)/np.sin(theta);
    
    
    L1 = np.array([l1+lq/2, l1+l2+lq*1.5, l1+l2+l2+lq*2.5]); print(L1, L0*np.sin(theta), L)
    Qs = np.array([Xb[0]+L1*np.sin(theta), Xb[1]+L1*np.cos(theta)]) # (x, z)
    
    zpos2 = np.array([0.3+lq/2, 0.3+0.2+lq*1.5, 0.3+0.2*2+lq*2.5])+rho*np.sin(theta)*2+(l1*2+l2*2+lq*3)*np.cos(theta)+8
    
    grads = np.array([0.86490, -1.14237, 0.70270, 0.96854, -0.82980, 0.94215])*1e-1/2.15e-2*astra_to_sco
    grads = np.array([0.69469, -1.36792, 0.76453, 1.05337, -0.90191, 1.00440])*1e-1/2.15e-2*astra_to_sco
    
    quadru = Module('Quadrupole', LQuad = True, 
                    Q_type = ['../Q3.data' for _ in np.arange(6)],
                    Q_pos = list(Qs[1])+list(zpos2), 
                    Q_grad = grads,
                    Q_xoff = list(Qs[0]+0.00258)+[1.2+0.0019 for _ in np.arange(3)],
                    Q_xrot = [theta for _ in np.arange(3)]+[0 for _ in np.arange(3)])
    
    # Dipole
    X11, X12, X13, X14 = PrepareDipoleCoordinates(rho, theta-beta, poleWidth = poleWidth, theta0 = beta, X0 = [dx-dxoff, z0+dz]) # (x, z)
    
    X0 = [-Xb[0]+L0*np.sin(theta), Xb[1]+L0*np.cos(theta)] # (x, z)
    X0 = [Xb[0]+L, Xb[1]+L] # (x, z)
    X21, X22, X23, X24 = PrepareDipoleCoordinates(rho, -theta, poleWidth = poleWidth, theta0 = theta, X0 = X0)
    
    X31, X32, X33, X34 = PrepareDipoleCoordinates(0.2, -2*theta, poleWidth = poleWidth, theta0 = 0, X0 = [1.2, Zstop])
    
    D_type = ['hor']; D_strength = [-Bdip, Bdip, Bdip]; D_radius = [rho, -rho, -rho]
    D_strength = [-0.26001, 0.25460]
    D_strength = [0.25460]
    
    D1 = [X11, X21, X31]; D2 = [X12, X22, X32]; D3 = [X13, X23, X33]; D4 = [X14, X24, X34]; #print(D1)
    D1 = [X31]; D2 = [X32]; D3 = [X33]; D4 = [X34]; #print(D1)
    
    D_Gap = [[gap], [gap]]
    
    dipole = Module('Dipole', LDipole = True, D_type = D_type, D1 = D1, D2 = D2, D3 = D3, D4 = D4,
                    D_Gap = D_Gap, D_strength = D_strength) # , D_radius = D_radius
                    
    
    astra = Astra()
    astra.add_modules([newrun, charge, quadru, dipole, output])
    
    direc = ''
    for i in np.arange(len(x)):
        direc += str.format('%.2f-' %  (x[i]))
    direc = direc[:-1]
    direc = 'temp'
    ###
    
    os.system('mkdir -p '+direc)
    #os.chdir(direc)
    
    job_name = 'myjob_'+direc
    #gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    #generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, None, direc)
    
    return direc
    ###
    
    #os.system('mkdir -p '+direc)
    try:
        os.mkdir(direc)
        pass
    except OSError as err:
        #if err.errno != errno.EEXIST:
        #    raise
        pass
    
    os.chdir(direc)
    
    generator.write('gen.in')
    astra.write('ast.in')
    
    #os.system('generator gen.in 2>&1 | tee gen.log')
    #os.system('astra ast.in 2>&1 | tee ast.log')
    
    os.system('astra ast.in')
    
    #os.chdir('..')
    
    return direc
####
def obj_quads2(x, *args, **kwargs):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameters:
      x: an array or list of the variables to be optimized, here the gradients of quads
      *args: the names of quads
      **kwargs: key-value pairs of Astra module property, e.g., 'Run', 1
    Returns:
      direc: the directory where Astra simulation runs
    '''
    
    z0 = 26
    lq = 0.0675
    
    L, rho, theta = 1.5, 0.4, np.pi/3
    
    poleWidth = 0.36
    gap = 50e-3
    Bdip = 0.1077634*1.5
    
    l1, l2, l3, g1, g2, rr = x
    
    Run = 10
    Distribution = '..'+os.sep+'ast.2550.007'
    Zstop = 28
    Screen = [27.5, 27.8]
    if len(kwargs)>0:
        if 'Run' in list(kwargs.keys()):
            Run = kwargs['Run']

        if 'Distribution' in list(kwargs.keys()):
            Distribution = kwargs['Distribution']

        if 'Zstop' in list(kwargs.keys()):
            Zstop = kwargs['Zstop']

        if 'Screen' in list(kwargs.keys()):
            Screen = kwargs['Screen']

    newrun = Module('Newrun', Run = Run, Head = 'PITZ beam line simulation', Distribution = Distribution,\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    #newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 30, Nlong_in = 50, N_min = 10,\
                    NXF = 32, NYF = 32, NZF = 32, L2D_3D = True, Z_TRANS = 5.0, Max_scale = 0.05, Max_count = 20)
    #charge.set(LSPCH = False, L2D_3D = False)
    #charge.set(NXF = 16, NYF = 16, NZF = 16)
    
    output = Module('Output', Zstart = 0, Zstop = Zstop, Zemit = int(Zstop*50), Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = Screen)
    
    # Quadrupole
    Xb = np.array([rho*(np.cos(theta)-1), z0+rho*np.sin(theta)]) # (x, z)
    L0 = (L+Xb[0]*2)/np.sin(theta);
    
    L1 = np.array([l1+lq/2, l1+lq+l2+lq/2, L0/2+l3+lq/2, L0-l1-lq/2]); print(L1, L0, L)
    Qs = np.array([Xb[0]-L1*np.sin(theta), Xb[1]+L1*np.cos(theta)]) # (x, z)
    
    quadru = Module('Quadrupole', LQuad = True, 
                    Q_type = ['../Q3.data' for _ in np.arange(4)],
                    Q_pos = Qs[1], 
                    Q_grad = [g1*rr, g2, g2, g1*rr],
                    Q_xoff = Qs[0],
                    Q_xrot = [-theta for _ in np.arange(4)])
    
    # Dipole
    X11, X12, X13, X14 = PrepareDipoleCoordinates(rho, -theta, poleWidth = poleWidth, theta0 = 0, X0 = [0, z0]) # (x, z)
    
    X0 = [Xb[0]-L0*np.sin(theta), Xb[1]+L0*np.cos(theta)] # (x, z)
    X21, X22, X23, X24 = PrepareDipoleCoordinates(rho, theta, poleWidth = poleWidth, theta0 = -theta, X0 = X0)
    
    D_type = ['hor', 'hor']; D_strength = [-Bdip, Bdip]; D_radius = [rho, -rho]
    D1 = [X11, X21]; D2 = [X12, X22]; D3 = [X13, X23]; D4 = [X14, X24]; #print(D1)
    
    D_Gap = [[gap, gap], [gap, gap]]
    
    dipole = Module('Dipole', LDipole = True, D_type = D_type, D1 = D1, D2 = D2, D3 = D3, D4 = D4,
                    D_Gap = D_Gap, D_strength = D_strength, D_radius = D_radius)
                    
    
    astra = Astra()
    astra.add_modules([newrun, charge, quadru, dipole, output])
    
    direc = ''
    for i in np.arange(len(x)):
        direc += str.format('%.2f-' %  (x[i]))
    direc = direc[:-1]
    
    ###
    
    os.system('mkdir -p '+direc)
    #os.chdir(direc)
    
    job_name = 'myjob_'+direc
    #gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    #generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, None, direc)
    
    return direc
    ###
    
    #os.system('mkdir -p '+direc)
    try:
        os.mkdir(direc)
        pass
    except OSError as err:
        #if err.errno != errno.EEXIST:
        #    raise
        pass
    
    os.chdir(direc)
    
    generator.write('gen.in')
    astra.write('ast.in')
    
    #os.system('generator gen.in 2>&1 | tee gen.log')
    #os.system('astra ast.in 2>&1 | tee ast.log')
    
    os.system('astra ast.in')
    
    #os.chdir('..')
    
    return direc


def obj_undScan999(x = None, *args, **kwargs):
    '''
    Optimize the beam size and beam covariance, given the emittance.
    Parameter:
      x: an array or list of the variables to be optimized, sigma_x, sigma_y, alpha_x and alpha_y
    Return:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    kv = kwargs
    
    N_u = 120; lam_u = 0.03; L_u = N_u*lam_u
    By_u_max = 1.28 # T
    pos_u = L_u/2.
    
    P0 = 17.0 # MeV/c
    Ek = momentum2kinetic(P0); gamma_r = kinetic2gamma(Ek); bg_r = gamma2bg(gamma_r)
    #nemit_x = nemit_y = 4e-6 # m*rad
    
    scale = 1.0
    ###
    nemit_x, nemit_y = x[0]*1e-6*scale**2, x[1]*1e-6*scale**2
    ###
    
    emit_x = nemit_x/bg_r; emit_y = nemit_y/bg_r
    
    Ek = momentum2kinetic(P0)
    sigma_Ek = -P0/(Ek+g_mec2)*155.9
    sigma_Ek = -P0/(Ek+g_mec2)*83.2
    #sigma_Ek = momentum2kinetic(155.9e-3)*1e3 # keV
    
    ###
    #if x is not None:
    #    # sigma_x in mm, alpha_x dimensionless
    #    sigma_x, sigma_y, alpha_x, alpha_y = x 
    #else:
    #    sigma_x, sigma_y, alpha_x, alpha_y = 1.576772E+00,   1.775836E-01,   7.183778E+00,   1.716220E+00
    #sigma_x, sigma_y, alpha_x, alpha_y = 1.419*scale, 1.390*scale, x[2], x[3]
    #sigma_x, sigma_y, alpha_x, alpha_y = 1.481*scale, 1.454*scale, x[2], x[3]
    sigma_x, sigma_y, alpha_x, alpha_y = x[2]*scale, x[3]*scale, x[4], x[5]
    ###
    
    beta_x, beta_y = sigma_x**2/emit_x*1e-6, sigma_y**2/emit_y*1e-6
    gamma_x, gamma_y = (1.+alpha_x**2)/beta_x, (1.+alpha_y**2)/beta_y
    
    cor_px = -alpha_x/beta_x*sigma_x
    cor_py = -alpha_y/beta_y*sigma_y
    
    if len(args) > 0:
        execute = args[0]
    else:
        execute = 1
    #Distribution = '../ast.2700.001'
    #if 'Distribution' in kv.keys():
    #    Distribution = kv['Distribution']
        
    Run = 1
    if 'Run' in list(kv.keys()):
        Run = kv['Run']
    
    Ipart = 200000
    generator = Generator1(FNAME = 'ast.0528.001', IPart = Ipart, Species = 'electrons', Q_total = -4,\
                          Ref_Ekin = Ek, sig_Ekin = 0, cor_Ekin = sigma_Ek, Dist_pz = 'g', Cathode = False,\
                          Dist_z = 'g', sig_z = 20e-12/np.sqrt(2*np.pi)*g_c*1e3,\
                          Dist_x = 'g', sig_x = sigma_x, Dist_px = 'g', Nemit_x = nemit_x*1e6, cor_px = cor_px,\
                          Dist_y = 'g', sig_y = sigma_y, Dist_py = 'g', Nemit_y = nemit_y*1e6, cor_py = cor_py)
    
    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = 'beam_und.ini',\
                    Auto_Phase = True, Track_All = True, Track_On_Axis = False, check_ref_part = False,\
                    Lprompt = False)
    newrun.set(Run = Run)
    
    charge = Module('Charge', LSPCH = True, LSPCH3D = True, Nxf = 32, Nyf = 32, Nzf = 32,\
                    Max_scale = 0.05, Max_count = 20)
    
    cavity = Module('Cavity', LEfield = True, By_stat = [True], File_Efield = [field_maps+os.sep+'3Dund-15x5mm'],\
                    MaxE = [By_u_max], C_pos = [pos_u], Nue = [0], Phi = [0])
    
    output = Module('Output', Zstart = 0, Zstop = 3.6, Zemit = 180, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [1.8, 3.6])

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, output])
    
    ###
    direc = str.format('eps_x-%.2fum-eps_y-%.2fum-alp_x-%.2f-alp_y-%.2f' % (x[0], x[1], x[4], x[5]))
    #direc = str.format('nemit_x-%.2fum-nemit_y-%.2fum' %\
    #                   (x[0], x[1]))
    ###
    #direc = '.'
    os.system('mkdir '+direc)
    # os.chdir(direc)
    
    job_name = '%s' % direc
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    #astra.write(direc+os.sep+ast_name+'.in')
    #astra.qsub(job_name, ast_name, gen_name, direc)
    
    # os.chdir('../')
    
    # os.system('generator gen.in > gen.log')
    # os.system('astra ast.in > ast.log')
    return

def obj_pharosScan(x, *args):
    '''
    Created on Nov 17, 2019
    Simulation from photocathode via EMSY1 at 5.277 m to 20 m
    Goal function is a combination of average emittance and correlated energy spread exterpolated at undulator center, 29.25 m
    Laser spot fixed at 4 mm, gun phase at MMMG
    Variables to be optimized: gun and booster phases, solenoid current
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''
    
    WEIGHTS = [0.25, 0.25, 0., 0., 0., 0.]   # weights for emittance, beta and gamma and
    OBJECTS = [0,       0, 0,  0,  0,  0]    # objectives for emittance, beta and gamma, in x and y directions, respectively
    
    Ipart = 100000
    BSA = x[1]
    sigma_x = sigma_y = BSA/4.
    C_sigma_x = C_sigma_y = 0
    
    #sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    #C_sigma_x = C_sigma_y = BSA/2./sigma_x
    
    phi_gun, phi_booster = x[3], x[5]
    Imain = x[6]
    
    MaxE_gun = x[2]
    MaxE_booster = x[4] # get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = -x[0]/1e3
    
    field_maps = rootdir+os.sep+'sync'+os.sep+'field-maps'
    
    generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',\
                          Dist_z = 'p', LT = 10e-3, RT = 2e-3, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)
    
    Distribution = '..'+os.sep+'beam.ini'
    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = Distribution, CathodeS = True,\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    newrun.set(Qbunch = Q_total)
    newrun.set(Run = 1, XYrms = sigma_x)
    
    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 40, Nlong_in = 50, N_min = 20, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt', field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = 6, Zemit = 120, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, C_EmitS = True, LPROJECT_EMIT = True,\
                    LOCAL_EMIT = True, Screen = [5.2770, 18])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, output])
    
    direc = str.format('Q%.2fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Q_total*1e3, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    
    ###
    os.system('mkdir -p '+direc)
    #os.chdir(direc)
    
    job_name = 'myjob.'+('BSA-%.2fmm-I-%.2fA' % (x[1], x[6]))
    #job_name = 'myjob.'+('BSA-%.2fmm-I-%.2fA' % (x[0], x[3]))
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, None, direc)
    #os.system('qsub '+)
    return
    ###
    
    os.system('mkdir -p '+direc)
    os.chdir(direc)
    
    generator.write('gen.in')
    astra.write('ast.in')
    
    os.system('generator gen.in 2>&1 | tee gen.log')
    os.system('astra ast.in 2>&1 | tee ast.log')
    
    return
    
    try:
        fname = 'ast.0528.001'
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag = BeamDiagnostics(dist = dist)
        
        fname = 'ast.Xemit.001'
        xemit = np.loadtxt(fname)
        select = (xemit[:,0]>5.0)
        xemit = xemit[select]
        nemit_x = np.mean(xemit[:,5])
        
        fname = 'ast.Zemit.001'
        zemit = np.loadtxt(fname)       
        select = (zemit[:,0]>5.0)
        zemit = zemit[select]
        
        f = lambda x, a, b: a+b*x
        popt, pcov = curve_fit(f, zemit[:,0], zemit[:,6])
        cov = f(29.25, *popt)
        
        if diag.loss_cathode<=500 and diag.loss_aperture<=500: 
            # when there is little beam loss to the cathode or to the aperture
            if cov>0:
                obj = nemit_x/4+np.fabs(cov/10)
            else:
                obj = nemit_x/4+np.fabs(cov/50)
        else:
            # if there is beam loss, then set the objective to 1000
            obj = 1e3   
        
    except:
    #    # if no output beam file, then set the objective to 10000
        obj = 1e4
        os.chdir('..')
        return
    #    print('Error: not accelerated well!')

    os.chdir('..'); #os.system('rm -r '+direc)

    res = [Ipart, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, nemit_x, cov, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')
    
    return obj

def obj_MBIScan2(x, *args):
    
    '''
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''
    
    WEIGHTS = [0.25, 0.25, 0., 0., 0., 0.]   # weights for emittance, beta and gamma and
    OBJECTS = [0,       0, 0,  0,  0,  0]    # objectives for emittance, beta and gamma, in x and y directions, respectively
    
    Ipart = 250000
    BSA = x[1]
    
    sigma_x = sigma_y = BSA/4.
    C_sigma_x = C_sigma_y = 0
    
    #sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    #C_sigma_x = C_sigma_y = BSA/2./sigma_x
    
    sigma_x = sigma_y = 1.25 #0.96 # Gaussian truncated
    C_sigma_x = C_sigma_y = BSA/2./sigma_x
    
    phi_gun, phi_booster = x[3], x[5]
    Imain = x[6]
    
    MaxE_gun = x[2]
    MaxE_booster = get_MaxE_booster(MaxE_gun, phi_gun, phi_booster, 22)
    MaxB = I2B(Imain)
    
    Q_total = -x[0]/1e3
    
    field_maps = rootdir+os.sep+'sync'+os.sep+'field-maps'
    
    generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3*1, dist_pz = 'i',\
                          Dist_z = 'g', sig_clock = 9e-3/2.355, Cathode = True,\
                          Dist_x = '2', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = '2', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)
    #generator.set(Dist_x = 'r', sig_x = BSA/4.0, Dist_y = 'r', sig_y = BSA/4.0)
    #generator.set(Dist_z = 'p', Lt = x[-1]*1e-3, Rt = 2e-3)
    
    Distribution = 'beam.ini'
    #Distribution = 'ast.0050.005'
    
    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = Distribution, CathodeS = True,\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    newrun.set(Xoff = 0., Yoff = 0., Run = 9)
    #newrun.set(Qbunch = Q_total)
    #newrun.set(Run = 1, XYrms = sigma_x)
    
    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 40, Nlong_in = 50, N_min = 50, Max_scale = 0.05, Max_count = 20)
    #charge.set(L2D_3D = True, Z_TRANS = 0.35, NXF = 32, NYF = 32, NZF = 32, min_grid_trans = 0.03e-3)
    
    cavity = Module('Cavity', LEfield = True, File_Efield = 
                    [field_maps+os.sep+'gun42cavity.txt', field_maps+os.sep+'CDS14_15mm.txt',
                     '..'+os.sep+'DY_3D10cm-20x20mm', '..'+os.sep+'DY_3D100cm-20x20mm'],\
                    MaxE = [MaxE_gun, MaxE_booster, 0.0009303556262245928, -0.0001329079466], 
                    By_stat = [False, False, True, True],
                    C_pos = [0., 2.675, 0.4, 1.5], Nue = [1.3, 1.3, 0, 0], Phi = [phi_gun, phi_booster, 0, 0])
    
    #cavity = Module('Cavity', LEfield = True, By_stat = [True], File_Efield = ['..'+os.sep+'DY_3D-20x20mm'],\
    #                MaxE = [By_u_max], C_pos = [pos_u], Nue = [0], Phi = [0])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'],
                    MaxB = [-MaxB], S_pos = [0.], S_xrot = [0*1e-3], S_yrot = [0])
    
    output = Module('Output', Zstart = 0, Zstop = 12, Zemit = 1200, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, C_EmitS = True, LPROJECT_EMIT = True,\
                    LOCAL_EMIT = True, Screen = [0.5, 2, 5.28])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    
    #Qsol = 0 #-0.0034*x[-2]/365.0
    #Qsol_zrot = 0 # -0.4363 #x[-1]/180.0*np.pi
    #quadru = Module('Quadrupole', LQuad = True, Q_type = ['../Qsol.data'], Q_pos = [0], Q_grad = [Qsol], Q_zrot = [Qsol_zrot])
    
    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, output])
    
    
    direc = str.format('Q-%.2fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (np.abs(Q_total)*1e3, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    
    #direc = str.format('D-%.2fmm-LT-%.0fps' % (x[1], x[-1]))
    ###
    os.system('mkdir -p '+direc)
    #os.chdir(direc)
    
    job_name = direc
    #job_name = 'myjob.'+('BSA-%.2fmm-I-%.2fA' % (x[0], x[3]))
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, None, direc)
    
    #os.system('qsub '+job_name+'.sh')
    return
    ###
    
    os.system('mkdir -p '+direc)
    os.chdir(direc)
    
    generator.write('gen.in')
    astra.write('ast.in')
    
    os.system('generator gen.in 2>&1 | tee gen.log')
    os.system('astra ast.in 2>&1 | tee ast.log')
    
    return
    
    try:
        fname = 'ast.0528.001'
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag = BeamDiagnostics(dist = dist)
        
        fname = 'ast.Xemit.001'
        xemit = np.loadtxt(fname)
        select = (xemit[:,0]>5.0)
        xemit = xemit[select]
        nemit_x = np.mean(xemit[:,5])
        
        fname = 'ast.Zemit.001'
        zemit = np.loadtxt(fname)       
        select = (zemit[:,0]>5.0)
        zemit = zemit[select]
        
        f = lambda x, a, b: a+b*x
        popt, pcov = curve_fit(f, zemit[:,0], zemit[:,6])
        cov = f(29.25, *popt)
        
        if diag.loss_cathode<=500 and diag.loss_aperture<=500: 
            # when there is little beam loss to the cathode or to the aperture
            if cov>0:
                obj = nemit_x/4+np.fabs(cov/10)
            else:
                obj = nemit_x/4+np.fabs(cov/50)
        else:
            # if there is beam loss, then set the objective to 1000
            obj = 1e3   
        
    except:
    #    # if no output beam file, then set the objective to 10000
        obj = 1e4
        os.chdir('..')
        return
    #    print('Error: not accelerated well!')

    os.chdir('..'); #os.system('rm -r '+direc)

    res = [Ipart, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, nemit_x, cov, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')
    
    return obj

def obj_MBIScan(x, *args):
    
    '''
    Created on Nov 17, 2019
    Simulation from photocathode via EMSY1 at 5.277 m to 20 m
    Goal function is a combination of average emittance and correlated energy spread exterpolated at undulator center, 29.25 m
    Laser spot fixed at 4 mm, gun phase at MMMG
    Variables to be optimized: gun and booster phases, solenoid current
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''
    
    WEIGHTS = [0.25, 0.25, 0., 0., 0., 0.]   # weights for emittance, beta and gamma and
    OBJECTS = [0,       0, 0,  0,  0,  0]    # objectives for emittance, beta and gamma, in x and y directions, respectively
    
    Ipart = 250000
    BSA = x[1]
    
    sigma_x = sigma_y = BSA/4.
    C_sigma_x = C_sigma_y = 0
    
    #sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    #C_sigma_x = C_sigma_y = BSA/2./sigma_x
    
    sigma_x = sigma_y = 1.25 #0.96 # Gaussian truncated
    C_sigma_x = C_sigma_y = BSA/2./sigma_x
    
    phi_gun, phi_booster = x[3], x[5]
    Imain = x[6]
    
    MaxE_gun = x[2]
    MaxE_booster = get_MaxE_booster(MaxE_gun, phi_gun, phi_booster, 22)
    MaxB = I2B(Imain)
    
    Q_total = -x[0]/1e3
    
    field_maps = rootdir+os.sep+'sync'+os.sep+'field-maps'
    
    generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3*1, dist_pz = 'i',\
                          Dist_z = 'g', sig_clock = 12e-3/2.355, Cathode = True,\
                          Dist_x = '2', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = '2', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)
    #generator.set(Dist_x = 'r', sig_x = BSA/4.0, Dist_y = 'r', sig_y = BSA/4.0)
    #generator.set(Dist_z = 'p', Lt = x[-1]*1e-3, Rt = 2e-3)
    
    Distribution = 'beam.ini'
    #Distribution = 'ast.0050.001'
    
    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = Distribution, CathodeS = True,\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    newrun.set(Xoff = 0., Yoff = 0., Run = 11)
    #newrun.set(Qbunch = Q_total)
    #newrun.set(Run = 1, XYrms = sigma_x)
    
    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 40, Nlong_in = 50, N_min = 50, Max_scale = 0.05, Max_count = 20)
    #charge.set(L2D_3D = True, Z_TRANS = 0.5, NXF = 32, NYF = 32, NZF = 32, min_grid_trans = 0.03e-3)
    
    cavity = Module('Cavity', LEfield = True, File_Efield = 
                    [field_maps+os.sep+'gun42cavity.txt', field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'],
                    MaxB = [-MaxB], S_pos = [0.], S_xrot = [0*1e-3], S_yrot = [0])
    
    output = Module('Output', Zstart = 0, Zstop = 12, Zemit = 1200, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, C_EmitS = True, LPROJECT_EMIT = True,\
                    LOCAL_EMIT = True, Screen = [0.5, 2, 5.28])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    
    #Qsol = 0 #-0.0034*x[-2]/365.0
    #Qsol_zrot = 0 # -0.4363 #x[-1]/180.0*np.pi
    #quadru = Module('Quadrupole', LQuad = True, Q_type = ['../Qsol.data'], Q_pos = [0], Q_grad = [Qsol], Q_zrot = [Qsol_zrot])
    
    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, output])
    
    
    direc = str.format('Q-%.2fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (np.abs(Q_total)*1e3, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    
    #direc = str.format('D-%.2fmm-LT-%.0fps' % (x[1], x[-1]))
    ###
    os.system('mkdir -p '+direc)
    #os.chdir(direc)
    
    job_name = direc
    #job_name = 'myjob.'+('BSA-%.2fmm-I-%.2fA' % (x[0], x[3]))
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, gen_name, direc)
    
    #os.system('qsub '+job_name+'.sh')
    return
    ###
    
    os.system('mkdir -p '+direc)
    os.chdir(direc)
    
    generator.write('gen.in')
    astra.write('ast.in')
    
    os.system('generator gen.in 2>&1 | tee gen.log')
    os.system('astra ast.in 2>&1 | tee ast.log')
    
    return
    
    try:
        fname = 'ast.0528.001'
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag = BeamDiagnostics(dist = dist)
        
        fname = 'ast.Xemit.001'
        xemit = np.loadtxt(fname)
        select = (xemit[:,0]>5.0)
        xemit = xemit[select]
        nemit_x = np.mean(xemit[:,5])
        
        fname = 'ast.Zemit.001'
        zemit = np.loadtxt(fname)       
        select = (zemit[:,0]>5.0)
        zemit = zemit[select]
        
        f = lambda x, a, b: a+b*x
        popt, pcov = curve_fit(f, zemit[:,0], zemit[:,6])
        cov = f(29.25, *popt)
        
        if diag.loss_cathode<=500 and diag.loss_aperture<=500: 
            # when there is little beam loss to the cathode or to the aperture
            if cov>0:
                obj = nemit_x/4+np.fabs(cov/10)
            else:
                obj = nemit_x/4+np.fabs(cov/50)
        else:
            # if there is beam loss, then set the objective to 1000
            obj = 1e3   
        
    except:
    #    # if no output beam file, then set the objective to 10000
        obj = 1e4
        os.chdir('..')
        return
    #    print('Error: not accelerated well!')

    os.chdir('..'); #os.system('rm -r '+direc)

    res = [Ipart, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, nemit_x, cov, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')
    
    return obj

def obj_emissionScan(x, *args):
    '''
    Created on Nov 17, 2019
    Simulation from photocathode via EMSY1 at 5.277 m to 20 m
    Goal function is a combination of average emittance and correlated energy spread exterpolated at undulator center, 29.25 m
    Laser spot fixed at 4 mm, gun phase at MMMG
    Variables to be optimized: gun and booster phases, solenoid current
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''
    
    WEIGHTS = [0.25, 0.25, 0., 0., 0., 0.]   # weights for emittance, beta and gamma and
    OBJECTS = [0,       0, 0,  0,  0,  0]    # objectives for emittance, beta and gamma, in x and y directions, respectively
    
    Ipart = 200000
    BSA = x[1]
    
    sigma_x = sigma_y = BSA/4.
    C_sigma_x = C_sigma_y = 0
    
    #sigma_x = sigma_y = 0.96 # Gaussian truncated
    #C_sigma_x = C_sigma_y = BSA/2./sigma_x
    
    phi_gun, phi_booster = x[3], x[5]
    Imain = x[6]
    
    MaxE_gun = x[2]
    MaxE_booster = x[4] # get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)
    
    Q_total = -x[0]/1e3
    
    field_maps = rootdir+os.sep+'sync'+os.sep+'field-maps'
    
    generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',\
                          Dist_z = 'uniform ellipsoid', Lt = 7.64e-3, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)
    
    Distribution = '.'+os.sep+'beam.ini'
    newrun = Module('Newrun', Run = 2, Head = 'PITZ beam line simulation', Distribution = Distribution, CathodeS = True,\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    newrun.set(Qbunch = Q_total)
    #newrun.set(Run = 1, XYrms = sigma_x)
    
    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 40, Nlong_in = 50, N_min = 20, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt', field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = 0.5, Zemit = 50, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, C_EmitS = True, LPROJECT_EMIT = True,\
                    LOCAL_EMIT = True, Screen = [5.2770, 18])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, output])
    
    direc = str.format('Q%.2fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Q_total*1e3, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    
    ###
    os.system('mkdir -p '+direc)
    #os.chdir(direc)
    
    job_name = 'myjob.'+('Q-%.2fpC-BSA-%.2fmm' % (x[0], x[1]))
    #job_name = 'myjob.'+('BSA-%.2fmm-I-%.2fA' % (x[0], x[3]))
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, gen_name, direc)
    #os.system('qsub '+)
    return
    ###
    
    os.system('mkdir -p '+direc)
    os.chdir(direc)
    
    generator.write('gen.in')
    astra.write('ast.in')
    
    os.system('generator gen.in 2>&1 | tee gen.log')
    os.system('astra ast.in 2>&1 | tee ast.log')
    
    return
    
    try:
        fname = 'ast.0528.001'
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag = BeamDiagnostics(dist = dist)
        
        fname = 'ast.Xemit.001'
        xemit = np.loadtxt(fname)
        select = (xemit[:,0]>5.0)
        xemit = xemit[select]
        nemit_x = np.mean(xemit[:,5])
        
        fname = 'ast.Zemit.001'
        zemit = np.loadtxt(fname)       
        select = (zemit[:,0]>5.0)
        zemit = zemit[select]
        
        f = lambda x, a, b: a+b*x
        popt, pcov = curve_fit(f, zemit[:,0], zemit[:,6])
        cov = f(29.25, *popt)
        
        if diag.loss_cathode<=500 and diag.loss_aperture<=500: 
            # when there is little beam loss to the cathode or to the aperture
            if cov>0:
                obj = nemit_x/4+np.fabs(cov/10)
            else:
                obj = nemit_x/4+np.fabs(cov/50)
        else:
            # if there is beam loss, then set the objective to 1000
            obj = 1e3   
        
    except:
    #    # if no output beam file, then set the objective to 10000
        obj = 1e4
        os.chdir('..')
        return
    #    print('Error: not accelerated well!')

    os.chdir('..'); #os.system('rm -r '+direc)

    res = [Ipart, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, nemit_x, cov, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')
    
    return obj

def obj_500pC(x, *args):
    '''
    Created on Nov 17, 2019
    Simulation from photocathode via EMSY1 at 5.277 m to 20 m
    Goal function is a combination of average emittance and correlated energy spread exterpolated at undulator center, 29.25 m
    Laser spot fixed at 4 mm, gun phase at MMMG
    Variables to be optimized: gun and booster phases, solenoid current
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''
    
    WEIGHTS = [0.25, 0.25, 0., 0., 0., 0.]   # weights for emittance, beta and gamma and
    OBJECTS = [0,       0, 0,  0,  0,  0]    # objectives for emittance, beta and gamma, in x and y directions, respectively
    
    Ipart = 500000
    BSA = x[0]
    sigma_x = sigma_y = BSA/4.
    C_sigma_x = C_sigma_y = 0
    
    #sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    #C_sigma_x = C_sigma_y = BSA/2./sigma_x
    
    phi_gun, phi_booster = x[1], x[2]
    Imain = x[3]
    
    MaxE_gun = 60
    MaxE_booster = get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = -0.5
    
    field_maps = rootdir+os.sep+'sync'+os.sep+'field-maps'
    
    generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',\
                          Dist_z = 'g', sig_clock = 6e-3/2.355, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)
    
    newrun = Module('Newrun', Run = 2, Head = 'PITZ beam line simulation', Distribution = 'beam.ini', CathodeS = True,\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000, Qbunch = Q_total)
    #newrun.set(Run = 2, XYrms = sigma_x)
    
    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 40, Nlong_in = 50, N_min = 100, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt', field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = 18, Zemit = 360, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, C_EmitS = True, LPROJECT_EMIT = True,\
                    LOCAL_EMIT = True, Screen = [5.2770, 18])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, output])
    
    direc = str.format('Q%.1fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Q_total*1e3, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    
    ###
    os.system('mkdir -p '+direc)
    #os.chdir(direc)
    
    job_name = 'myjob.'+('phi2-%.2fdeg-I-%.2fA' % (x[2], x[3]))
    #job_name = 'myjob.'+('BSA-%.2fmm-I-%.2fA' % (x[0], x[3]))
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, gen_name, direc)
    
    return
    ###
    
    os.system('mkdir -p '+direc)
    os.chdir(direc)
    
    generator.write('gen.in')
    astra.write('ast.in')
    
    os.system('generator gen.in 2>&1 | tee gen.log')
    os.system('astra ast.in 2>&1 | tee ast.log')
    
    return
    
    try:
        fname = 'ast.0528.001'
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag = BeamDiagnostics(dist = dist)
        
        fname = 'ast.Xemit.001'
        xemit = np.loadtxt(fname)
        select = (xemit[:,0]>5.0)
        xemit = xemit[select]
        nemit_x = np.mean(xemit[:,5])
        
        fname = 'ast.Zemit.001'
        zemit = np.loadtxt(fname)       
        select = (zemit[:,0]>5.0)
        zemit = zemit[select]
        
        f = lambda x, a, b: a+b*x
        popt, pcov = curve_fit(f, zemit[:,0], zemit[:,6])
        cov = f(29.25, *popt)
        
        if diag.loss_cathode<=500 and diag.loss_aperture<=500: 
            # when there is little beam loss to the cathode or to the aperture
            if cov>0:
                obj = nemit_x/4+np.fabs(cov/10)
            else:
                obj = nemit_x/4+np.fabs(cov/50)
        else:
            # if there is beam loss, then set the objective to 1000
            obj = 1e3   
        
    except:
    #    # if no output beam file, then set the objective to 10000
        obj = 1e4
        os.chdir('..')
        return
    #    print('Error: not accelerated well!')

    os.chdir('..'); #os.system('rm -r '+direc)

    res = [Ipart, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, nemit_x, cov, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')
    
    return obj

def obj_1000pC(x, *args):
    '''
    Created on Nov 17, 2019
    Simulation from photocathode via EMSY1 at 5.277 m to 20 m
    Goal function is a combination of average emittance and correlated energy spread exterpolated at undulator center, 29.25 m
    Laser spot fixed at 4 mm, gun phase at MMMG
    Variables to be optimized: gun and booster phases, solenoid current
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''
    
    WEIGHTS = [0.25, 0.25, 0., 0., 0., 0.]   # weights for emittance, beta and gamma and
    OBJECTS = [0,       0, 0,  0,  0,  0]    # objectives for emittance, beta and gamma, in x and y directions, respectively
    
    Ipart = 500000
    BSA = x[0]
    sigma_x = sigma_y = BSA/4.
    C_sigma_x = C_sigma_y = 0
    
    #sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    #C_sigma_x = C_sigma_y = BSA/2./sigma_x
    
    phi_gun, phi_booster = x[1], x[2]
    Imain = x[3]
    
    MaxE_gun = 60
    MaxE_booster = get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = -1.0
    
    field_maps = rootdir+os.sep+'sync'+os.sep+'field-maps'
    
    generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',\
                          Dist_z = 'g', sig_clock = 6e-3/2.355, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)
    
    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = '../beam.ini', CathodeS = True,\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000, Qbunch = Q_total)
    newrun.set(Run = 2, XYrms = sigma_x)
    
    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 40, Nlong_in = 50, N_min = 100, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt', field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = 18, Zemit = 360, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, C_EmitS = True, LPROJECT_EMIT = True,\
                    LOCAL_EMIT = True, Screen = [5.2770, 18.0])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, output])
    
    direc = str.format('Q%.1fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Q_total*1e3, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    
    ###
    os.system('mkdir -p '+direc)
    #os.chdir(direc)
    
    job_name = 'myjob.'+('phi2-%.2fdeg-I-%.2fA' % (x[2], x[3]))
    #job_name = 'myjob.'+('BSA-%.2fmm-I-%.2fA' % (x[0], x[3]))
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, gen_name, direc)
    
    return
    ###
    
    os.system('mkdir -p '+direc)
    os.chdir(direc)
    
    generator.write('gen.in')
    astra.write('ast.in')
    
    os.system('generator gen.in 2>&1 | tee gen.log')
    os.system('astra ast.in 2>&1 | tee ast.log')
    
    return
    
    try:
        fname = 'ast.0528.001'
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag = BeamDiagnostics(dist = dist)
        
        fname = 'ast.Xemit.001'
        xemit = np.loadtxt(fname)
        select = (xemit[:,0]>5.0)
        xemit = xemit[select]
        nemit_x = np.mean(xemit[:,5])
        
        fname = 'ast.Zemit.001'
        zemit = np.loadtxt(fname)       
        select = (zemit[:,0]>5.0)
        zemit = zemit[select]
        
        f = lambda x, a, b: a+b*x
        popt, pcov = curve_fit(f, zemit[:,0], zemit[:,6])
        cov = f(29.25, *popt)
        
        if diag.loss_cathode<=500 and diag.loss_aperture<=500: 
            # when there is little beam loss to the cathode or to the aperture
            if cov>0:
                obj = nemit_x/4+np.fabs(cov/10)
            else:
                obj = nemit_x/4+np.fabs(cov/50)
        else:
            # if there is beam loss, then set the objective to 1000
            obj = 1e3   
        
    except:
    #    # if no output beam file, then set the objective to 10000
        obj = 1e4
        os.chdir('..')
        return
    #    print('Error: not accelerated well!')

    os.chdir('..'); #os.system('rm -r '+direc)

    res = [Ipart, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, nemit_x, cov, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')
    
    return obj

def obj_40pC(x, *args):
    '''
    Created on Nov 17, 2019
    Simulation from photocathode via EMSY1 at 5.277 m to 20 m
    Goal function is a combination of average emittance and correlated energy spread exterpolated at undulator center, 29.25 m
    Laser spot fixed at 4 mm, gun phase at MMMG
    Variables to be optimized: gun and booster phases, solenoid current
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''
    
    WEIGHTS = [0.25, 0.25, 0., 0., 0., 0.]   # weights for emittance, beta and gamma and
    OBJECTS = [0,       0, 0,  0,  0,  0]    # objectives for emittance, beta and gamma, in x and y directions, respectively
    
    Ipart = 500000
    BSA = x[0]
    sigma_x = sigma_y = BSA/4.
    C_sigma_x = C_sigma_y = 0
    
    #sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    #C_sigma_x = C_sigma_y = BSA/2./sigma_x
    
    phi_gun, phi_booster = x[1], x[2]
    Imain = x[3]
    
    MaxE_gun = 60
    MaxE_booster = get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = -0.04
    
    field_maps = rootdir+os.sep+'sync'+os.sep+'field-maps'
    
    generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',\
                          Dist_z = 'g', sig_clock = 6e-3/2.355, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)
    
    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = 'beam.ini', CathodeS = True,\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000, Qbunch = Q_total)
    newrun.set(Run = 2)
    
    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 40, Nlong_in = 50, N_min = 100, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt', field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = 18, Zemit = 360, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, C_EmitS = True, LPROJECT_EMIT = True,\
                    LOCAL_EMIT = True, Screen = [5.2770, 18.0])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, output])
    
    direc = str.format('Q%.1fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Q_total*1e3, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    
    ###
    os.system('mkdir -p '+direc)
    #os.chdir(direc)
    
    job_name = 'myjob.'+('phi2-%.2fdeg-I-%.2fA' % (x[2], x[3]))
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, gen_name, direc)
    
    return
    ###
    
    os.system('mkdir -p '+direc)
    os.chdir(direc)
    
    generator.write('gen.in')
    astra.write('ast.in')
    
    os.system('generator gen.in 2>&1 | tee gen.log')
    os.system('astra ast.in 2>&1 | tee ast.log')
    
    return
    
    try:
        fname = 'ast.0528.001'
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag = BeamDiagnostics(dist = dist)
        
        fname = 'ast.Xemit.001'
        xemit = np.loadtxt(fname)
        select = (xemit[:,0]>5.0)
        xemit = xemit[select]
        nemit_x = np.mean(xemit[:,5])
        
        fname = 'ast.Zemit.001'
        zemit = np.loadtxt(fname)       
        select = (zemit[:,0]>5.0)
        zemit = zemit[select]
        
        f = lambda x, a, b: a+b*x
        popt, pcov = curve_fit(f, zemit[:,0], zemit[:,6])
        cov = f(29.25, *popt)
        
        if diag.loss_cathode<=500 and diag.loss_aperture<=500: 
            # when there is little beam loss to the cathode or to the aperture
            if cov>0:
                obj = nemit_x/4+np.fabs(cov/10)
            else:
                obj = nemit_x/4+np.fabs(cov/50)
        else:
            # if there is beam loss, then set the objective to 1000
            obj = 1e3   
        
    except:
    #    # if no output beam file, then set the objective to 10000
        obj = 1e4
        os.chdir('..')
        return
    #    print('Error: not accelerated well!')

    os.chdir('..'); #os.system('rm -r '+direc)

    res = [Ipart, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, nemit_x, cov, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')
    
    return obj

def obj_100pC(x, *args):
    '''
    Created on Nov 13, 2019
    Simulation from photocathode via EMSY1 at 5.277 m to 20 m
    Goal function is a combination of average emittance and correlated energy spread exterpolated at undulator center, 29.25 m
    Laser spot fixed at 4 mm, gun phase at MMMG
    Variables to be optimized: gun and booster phases, solenoid current
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    WEIGHTS = [0.25, 0.25, 0., 0., 0., 0.]   # weights for emittance, beta and gamma and
    OBJECTS = [0,       0, 0,  0,  0,  0]    # objectives for emittance, beta and gamma, in x and y directions, respectively
    
    Ipart = 500000
    BSA = x[0]
    sigma_x = sigma_y = BSA/4.
    C_sigma_x = C_sigma_y = 0
    
    #sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    #C_sigma_x = C_sigma_y = BSA/2./sigma_x
    
    phi_gun, phi_booster = x[1], x[2]
    Imain = x[3]
    
    MaxE_gun = 60
    MaxE_booster = get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = -0.1
    
    field_maps = rootdir+os.sep+'sync'+os.sep+'field-maps'
    
    generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',\
                          Dist_z = 'g', sig_clock = 6e-3/2.355, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)
    
    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = 'beam.ini', CathodeS = True,\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000, Qbunch = Q_total)
    newrun.set(Run = 2)
    
    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 40, Nlong_in = 50, N_min = 100, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt', field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = 18, Zemit = 360, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, C_EmitS = True, LPROJECT_EMIT = True,\
                    LOCAL_EMIT = True, Screen = [5.2770, 18.0])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, output])
    
    direc = str.format('Q%.1fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Q_total*1e3, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    
    ###
    os.system('mkdir -p '+direc)
    #os.chdir(direc)
    
    job_name = 'myjob.'+('phi2-%.2fdeg-I-%.2fA' % (x[2], x[3]))
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, gen_name, direc)
    
    return
    ###
    
    os.system('mkdir -p '+direc)
    os.chdir(direc)
    
    generator.write('gen.in')
    astra.write('ast.in')
    
    os.system('generator gen.in 2>&1 | tee gen.log')
    os.system('astra ast.in 2>&1 | tee ast.log')
    
    return
    
    try:
        fname = 'ast.0528.001'
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag = BeamDiagnostics(dist = dist)
        
        fname = 'ast.Xemit.001'
        xemit = np.loadtxt(fname)
        select = (xemit[:,0]>5.0)
        xemit = xemit[select]
        nemit_x = np.mean(xemit[:,5])
        
        fname = 'ast.Zemit.001'
        zemit = np.loadtxt(fname)       
        select = (zemit[:,0]>5.0)
        zemit = zemit[select]
        
        f = lambda x, a, b: a+b*x
        popt, pcov = curve_fit(f, zemit[:,0], zemit[:,6])
        cov = f(29.25, *popt)
        
        if diag.loss_cathode<=500 and diag.loss_aperture<=500: 
            # when there is little beam loss to the cathode or to the aperture
            if cov>0:
                obj = nemit_x/4+np.fabs(cov/10)
            else:
                obj = nemit_x/4+np.fabs(cov/50)
        else:
            # if there is beam loss, then set the objective to 1000
            obj = 1e3   
        
    except:
    #    # if no output beam file, then set the objective to 10000
        obj = 1e4
        os.chdir('..')
        return
    #    print('Error: not accelerated well!')

    os.chdir('..'); #os.system('rm -r '+direc)

    res = [Ipart, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, nemit_x, cov, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')
    
    return obj

def obj_EMSY4000pC_NSGAII_100um(x):
    '''
    Created on November 13, 2019
    Simulation from photocathode via EMSY1 at 5.277 m to 20 m
    Goal function is a combination of average emittance and correlated energy spread exterpolated at undulator center, 29.25 m
    Laser spot fixed at 4 mm, gun phase at MMMG
    Variables to be optimized: gun and booster phases, solenoid current
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    WEIGHTS = [0.25, 0.25, 0., 0., 0., 0.]   # weights for emittance, beta and gamma and
    OBJECTS = [0,       0, 0,  0,  0,  0]    # objectives for emittance, beta and gamma, in x and y directions, respectively

    Ipart = 100000
    BSA = x[0]
    sigma_x = sigma_y = BSA/4.
    C_sigma_x = C_sigma_y = 0
    
    #sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    #C_sigma_x = C_sigma_y = BSA/2./sigma_x
    
    phi_gun, phi_booster = x[1], x[2]
    Imain = x[3]
    
    MaxE_gun = 60
    MaxE_booster = get_MaxE_booster(MaxE_gun, phi_gun, phi_booster, 17.0)
    MaxB = I2B(Imain)

    Q_total = -4.0
    
    field_maps = rootdir+os.sep+'sync'+os.sep+'field-maps'
    
    generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = Q_total,
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'p', Lt = 21.5e-3, rt = 2e-3, Cathode = True,
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)
    
    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = 'beam.ini',
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, 
                    Max_step=200000, Qbunch = Q_total)
    newrun.set(Run = 1)
    
    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 40, Nlong_in = 50, N_min = 50, 
                    Max_scale = 0.05, Max_count = 20)
    #charge.set(LSPCH = False, Lmirror = False)
    
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt',
                                                             field_maps+os.sep+'CDS14_15mm.txt'],
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], 
                    Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], 
                    MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = 20, Zemit = 400, Zphase = 1, RefS = True, EmitS = True,
                    PhaseS = True, TrackS = False, LandFS = True, C_EmitS = True, LPROJECT_EMIT = True,
                    LOCAL_EMIT = True, Screen = [5.2770])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    
    
    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, output])

    direc = str.format('Q%.1fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %
                       (Q_total*1e3, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    os.system('mkdir -p '+direc)
    os.chdir(direc)
    
    ast_basename = 'ast'
    gen_basename = 'gen'
    job_basename = 'myjob.%s' % direc
    
    generator.write(gen_basename+'.in')
    astra.write(ast_basename+'.in')
    
    #### for Farm
    os.chdir('..')
    astra.qsub(job_basename, ast_basename, gen_basename, direc)
    return 0
    ####
    
    os.system('generator gen.in 2>&1 | tee gen.log')
    os.system('astra ast.in 2>&1 | tee ast.log')
    
    print(x)
    try:
        
        fname = 'ast.Xemit.001'
        xemit = np.loadtxt(fname)
        select = (xemit[:,0]>5.0)
        xemit = xemit[select]
        nemit_x_std = np.std(xemit[:,5])
        nemit_x_avg = np.mean(xemit[:,5])
        
        
        fname = 'ast.Zemit.001'
        zemit = np.loadtxt(fname)       
        select = (zemit[:,0]>5.0)
        zemit = zemit[select]
        
        f = lambda x, a, b: a+b*x
        popt, pcov = curve_fit(f, zemit[:,0], zemit[:,6])
        cov = f(29.25, *popt)
        
        fname = 'ast.0528.001'
        diag = BeamDiagnostics(fname = fname)
        
        fname = 'ast.2000.001'
        diag2 = BeamDiagnostics(fname = fname)
        I1 = diag2.I1
        
        if diag.loss_cathode<=500 and diag.loss_aperture<=500: 
            # if there is little beam loss to the cathode or to the aperture
            obj = [4*nemit_x_avg*nemit_x_std, (np.fabs(cov+50.0)/50.0)**2, 200.0/I1]
        else:
            # if there is beam loss, then set the objective to 1000
            obj = [1e3, 1e3, 1e3]   
        
        res = [Ipart, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain]+list(diag.x)
        with open('..'+os.sep+'diag@5.28m.dat','a') as f_handle:
            np.savetxt(f_handle, np.atleast_2d(res), fmt='%14.6E')

        res = [Ipart, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain]+list(diag2.x)
        with open('..'+os.sep+'diag@20m.dat','a') as f_handle:
            np.savetxt(f_handle, np.atleast_2d(res), fmt='%14.6E')
        
    except:
        # if no output beam file, then set the objective to 10000
        nemit_x, cov, I1 = 999, 999, 999
        obj = [nemit_x, cov, I1]
        print('Error: not accelerated well!')
    
    os.chdir('..'); #os.system('rm -r '+direc)
    
    res = [Ipart, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, nemit_x_avg, nemit_x_std, cov, I1]+obj
    with open('results.002.dat','a') as f_handle:
        np.savetxt(f_handle, np.atleast_2d(res), fmt='%14.6E')
    
    print(obj)
    return obj


def obj_EMSY4000pC_NSGAII(x):
    '''
    Created on September 3, 2019
    Simulation from photocathode via EMSY1 at 5.277 m to 20 m
    Goal function is a combination of average emittance and correlated energy spread exterpolated at undulator center, 29.25 m
    Laser spot fixed at 4 mm, gun phase at MMMG
    Variables to be optimized: gun and booster phases, solenoid current
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    WEIGHTS = [0.25, 0.25, 0., 0., 0., 0.]   # weights for emittance, beta and gamma and
    OBJECTS = [0,       0, 0,  0,  0,  0]    # objectives for emittance, beta and gamma, in x and y directions, respectively

    Ipart = 100000
    BSA = x[0]
    #sigma_x = sigma_y = BSA/4.
    
    sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    C_sigma_x = C_sigma_y = BSA/2./sigma_x
    
    phi_gun, phi_booster = x[1], x[2]
    Imain = x[3]
    
    MaxE_gun = 60
    MaxE_booster = get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = -4.0
    
    field_maps = rootdir+os.sep+'sync'+os.sep+'field-maps'
    
    generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'p', Lt = 21.5e-3, rt = 2e-3, Cathode = True,\
                          Dist_x = '2', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = '2', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)
    
    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = 'beam.ini',\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000, Qbunch = Q_total)
    #newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 40, Nlong_in = 50, N_min = 50, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt', field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = 20, Zemit = 400, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, C_EmitS = True, LPROJECT_EMIT = True,\
                    LOCAL_EMIT = True, Screen = [5.2770])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, output])

    direc = str.format('Q%.1fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Q_total*1e3, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    os.system('mkdir -p '+direc)
    os.chdir(direc)
    
    generator.write('gen.in')
    astra.write('ast.in')
    
    os.system('generator gen.in 2>&1 | tee gen.log')
    #os.system('astra ast.in 2>&1 | tee ast.log')
    astra.run('ast.in')
    #import pdb; pdb.set_trace();
    if 1:
        fname = 'ast.0528.001'
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag = BeamDiagnostics(dist = dist)
        
        fname = 'ast.Xemit.001'
        xemit = np.loadtxt(fname)
        select = (xemit[:,0]>5.0)
        xemit = xemit[select]
        nemit_x = np.mean(xemit[:,5])
        
        fname = 'ast.Zemit.001'
        zemit = np.loadtxt(fname)       
        select = (zemit[:,0]>5.0)
        zemit = zemit[select]
        
        f = lambda x, a, b: a+b*x
        popt, pcov = curve_fit(f, zemit[:,0], zemit[:,6])
        cov = f(29.25, *popt)
        
        fname = 'ast.2000.001'
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag2 = BeamDiagnostics(dist = dist)
        I1 = diag2.I1
        
        if diag.loss_cathode<=500 and diag.loss_aperture<=500: 
            # when there is little beam loss to the cathode or to the aperture
            if cov>0:
                obj = [nemit_x/4, np.fabs(cov/10), 200.0/I1]
            else:
                obj = [nemit_x/4, np.fabs(cov/50), 200.0/I1]
        else:
            # if there is beam loss, then set the objective to 1000
            obj = [1e3, 1e3, 1e3]   
        
    #except:
    #    # if no output beam file, then set the objective to 10000
    #    obj = 1e4
    #    print('Error: not accelerated well!')
    
    os.chdir('..'); #os.system('rm -r '+direc)
    
    res = [Ipart, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, nemit_x, cov, I1]+obj
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')
    
    print(obj)
    return obj

def obj_SCgunScan4(x, *args):
    '''
    PITZ gun solenoid scan for gun operation under CW SC gun condition, modified on 16.08.2019
    Parameter:
      x: an array or list of the variables to be optimized
    Return:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''
    
    #para = np.loadtxt('..'+os.sep+'para-gaussian.txt')
    #select = (para[:,0]==x[0]); para = para[select]
    
    BSA = x[0]
    sigma_x = sigma_y = BSA/4. # uniform
    C_sigma_x = C_sigma_y = 0
    #sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    #C_sigma_x = C_sigma_y = x[0]/2./sigma_x
    
    phi_gun, phi_booster = 0, 0
    Imain = x[1]
    
    MaxE_gun = 30 #para[0,3]
    MaxE_booster = x[2] #para[0,4] #get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)
    
    Q_total = 100.0/1e3 #/20e3*para[0,5]
    
    Run = 1
    if len(args)>0:
        Run = args[0]
        
    Ipart = int(Q_total*100e3)
    if Ipart<50e3:
        Ipart = 50e3
    Ipart = 100000
    
    #Distribution = '..'+os.sep+'CH_beam50k_%.1fmm.ini' % BSA
    Distribution = '..'+os.sep+'beam.ini'
    
    #Lt = 0.75*np.sqrt(2.*np.pi)*4e-3 #6.0e-3/2.35482
    #generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -Q_total,\
    #                      Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
    #                      Dist_z = 'uniform ellipsoid', LT = Lt, Cathode = True,\
    #                      Dist_x = 'uniform ellipsoid', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
    #                      Dist_y = 'uniform ellipsoid', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0)
    
    generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',\
                          Dist_z = 'g', sig_clock = 6.0e-3/2.35482, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)

    newrun = Module('Newrun', Run = Run, Head = 'PITZ beam line simulation', Distribution = Distribution,\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000, Qbunch = -Q_total)
    newrun.set(XYrms = sigma_x)
    
    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 40, Nlong_in = 50, N_min = 50, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt', field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    screen = [0.5, 0.8030, 1.3790, 1.7080, 5.2770, 6.2500, 7.1250, 8.4100, \
              8.9200, 12.2780, 13.0380, 13.7980, 14.5580, 15.3180, 16.3030, 18.2620]
    output = Module('Output', Zstart = 0, Zstop = 5.28, Zemit = 528, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, C_EmitS = True, LPROJECT_EMIT = True,\
                    LOCAL_EMIT = True, CathodeS = True, Screen = [0.5])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    
    
    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, output])
    
    direc = str.format('Q%.1fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Q_total*1e3, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    os.system('mkdir -p '+direc)
    # os.chdir(direc)
    
    job_name = 'myjob.'+('D-%.2fmm-E2-%.2fMV-I-%.2fA' % (BSA, MaxE_booster, Imain))
    #job_name = 'myjob.'+('D-%.2fmm-Q-%.2fpC' % (BSA, Q_total*1e3))
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, gen_name, direc)
    
    return

def obj_SCgunScan32(x, *args):
    '''
    CW SC gun scan with CW SC gun field profile, corresponding solenoid and tesla cavities
    Parameter:
      x: an array or list of the variables to be optimized
    Return:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''
    
    #para = np.loadtxt('..'+os.sep+'para-gaussian.txt')
    #select = (para[:,0]==x[0]); para = para[select]
    
    sigma_x = sigma_y = x[0]/4 # uniform
    C_sigma_x = C_sigma_y = 0
    #sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    #C_sigma_x = C_sigma_y = x[0]/2./sigma_x
    
    phi_gun, phi_booster = x[1], 0
    Imain = x[2]
    
    MaxE_gun = 40 #para[0,3]
    MaxE_booster = 26.94 #para[0,4] #get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)
    
    Q_total = 100/1e3 # nC
    
    Run = 1
    if len(args)>0:
        Run = args[0]
        
    Ipart = int(Q_total*100e3)
    if Ipart<50e3:
        Ipart = 50e3
    Ipart = 100000
    
    #Distribution = '..'+os.sep+'..'+os.sep+'CH_beam200k_%.1fmm.ini' % 0.6
    #Distribution = '..'+os.sep+'cath055eV.ini'
    Distribution = '..'+os.sep+'beam.ini'
    
    Lt = 0.75*np.sqrt(2.*np.pi)*4e-3 #6.0e-3/2.35482
    generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'uniform ellipsoid', LT = Lt, Cathode = True,\
                          Dist_x = 'uniform ellipsoid', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'uniform ellipsoid', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0)
    
    #generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -Q_total,\
    #                      Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
    #                      Dist_z = 'g', sig_clock = 6.0e-3/2.35482, Cathode = True,\
    #                      Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
    #                      Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
    #                      C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)

    newrun = Module('Newrun', Run = Run, Head = 'PITZ beam line simulation', Distribution = Distribution,\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000, Qbunch = -Q_total,
                    Xrms = sigma_x, Yrms = sigma_y, H_max = 0.001, H_min = 0, Lmonitor = False)
    #newrun.set(Run = Run)
    
    field_maps = '..'
    File_tesla = [field_maps+os.sep+'tesla1cav2.dat' for kkk in np.arange(8)]
    MaxE_tesla = [26.94 for kkk in np.arange(8)]
    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 40, Nlong_in = 50, N_min = 30, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'k452.dat']+File_tesla,\
                    MaxE = [MaxE_gun]+MaxE_tesla, C_pos = [0., 4.04, 5.42, 6.80, 8.18, 9.56, 10.94, 12.32, 13.70],\
                    Nue = [1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3], Phi = [phi_gun, 0, 0, 0, 0, 0, 0, 0, 0])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'Bsol_with_pipe_m100.txt'], MaxB = MaxB, S_pos = [0.4])
    
    screen = [0.5, 0.8030, 1.3790, 1.7080, 5.2770, 6.2500, 7.1250, 8.4100, \
              8.9200, 12.2780, 13.0380, 13.7980, 14.5580, 15.3180, 16.3030, 18.2620]
    output = Module('Output', Zstart = 0, Zstop = 14.35, Zemit = 500, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, C_EmitS = True, LPROJECT_EMIT = False,\
                    LOCAL_EMIT = False)
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    
    
    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, output])
    
    direc = str.format('Q%.1fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Q_total*1e3, x[0], MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    os.system('mkdir -p '+direc)
    # os.chdir(direc)
    
    job_name = 'myjob.'+('D-%.2fmm-phi1-%.2fdeg-I-%.1fA' % (x[0], x[1], x[2]))
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, gen_name, direc)
    
    return

def obj_SCgunScan31(x, *args):
    '''
    CW SC gun scan with CW SC gun field profile, corresponding solenoid and tesla cavities
    Parameter:
      x: an array or list of the variables to be optimized
    Return:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''
    
    #para = np.loadtxt('..'+os.sep+'para-gaussian.txt')
    #select = (para[:,0]==x[0]); para = para[select]
    
    sigma_x = sigma_y = x[0]/4 # uniform
    C_sigma_x = C_sigma_y = 0
    #sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    #C_sigma_x = C_sigma_y = x[0]/2./sigma_x
    
    phi_gun, phi_booster = x[1], 0
    Imain = x[2]
    
    MaxE_gun = 40 #para[0,3]
    MaxE_booster = 26.94 #para[0,4] #get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)
    
    Q_total = 100/1e3 # nC
    
    Run = 1
    if len(args)>0:
        Run = args[0]
        
    Ipart = int(Q_total*100e3)
    if Ipart<50e3:
        Ipart = 50e3
    Ipart = 200000
    
    #Distribution = '..'+os.sep+'..'+os.sep+'CH_beam200k_%.1fmm.ini' % 0.6
    #Distribution = '..'+os.sep+'cath055eV.ini'
    Distribution = '..'+os.sep+'beam.ini'
    
    generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'p', LT = 16*1e-3, RT = 2*1e-3, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)
    
    #generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -Q_total,\
    #                      Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
    #                      Dist_z = 'g', sig_clock = 6.0e-3/2.35482, Cathode = True,\
    #                      Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
    #                      Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
    #                      C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)

    newrun = Module('Newrun', Run = Run, Head = 'PITZ beam line simulation', Distribution = Distribution,\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000, Qbunch = -Q_total,
                    XYrms = sigma_x, H_max = 0.001, H_min = 0, Lmonitor = False)
    #newrun.set(Run = Run)
    
    field_maps = '..'
    File_tesla = [field_maps+os.sep+'tesla1cav2.dat' for kkk in np.arange(8)]
    MaxE_tesla = [26.94 for kkk in np.arange(8)]
    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 40, Nlong_in = 50, N_min = 30, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'k452.dat']+File_tesla,\
                    MaxE = [MaxE_gun]+MaxE_tesla, C_pos = [0., 4.04, 5.42, 6.80, 8.18, 9.56, 10.94, 12.32, 13.70],\
                    Nue = [1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3], Phi = [phi_gun, 0, 0, 0, 0, 0, 0, 0, 0])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'Bsol_with_pipe_m100.txt'], MaxB = MaxB, S_pos = [0.4])
    
    screen = [0.5, 0.8030, 1.3790, 1.7080, 5.2770, 6.2500, 7.1250, 8.4100, \
              8.9200, 12.2780, 13.0380, 13.7980, 14.5580, 15.3180, 16.3030, 18.2620]
    output = Module('Output', Zstart = 0, Zstop = 14.35, Zemit = 500, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, C_EmitS = True, LPROJECT_EMIT = False,\
                    LOCAL_EMIT = False)
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    
    
    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, output])
    
    direc = str.format('Q%.1fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Q_total*1e3, x[0], MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    os.system('mkdir -p '+direc)
    # os.chdir(direc)
    
    job_name = 'myjob.'+('D-%.2fmm-phi1-%.2fdeg-I-%.1fA' % (x[0], x[1], x[2]))
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, gen_name, direc)
    
    return


def obj_SCgunScan3(x, *args):
    '''
    CW SC gun scan with CW SC gun field profile, corresponding solenoid and tesla cavities
    Parameter:
      x: an array or list of the variables to be optimized
    Return:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''
    
    #para = np.loadtxt('..'+os.sep+'para-gaussian.txt')
    #select = (para[:,0]==x[0]); para = para[select]
    
    sigma_x = sigma_y = x[0]/4 # uniform
    C_sigma_x = C_sigma_y = 0
    #sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    #C_sigma_x = C_sigma_y = x[0]/2./sigma_x
    
    phi_gun, phi_booster = x[1], 0
    Imain = x[2]
    
    MaxE_gun = 40 #para[0,3]
    MaxE_booster = 26.94 #para[0,4] #get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)
    
    Q_total = 100/1e3 # nC
    
    Run = 1
    if len(args)>0:
        Run = args[0]
        
    Ipart = int(Q_total*100e3)
    if Ipart<50e3:
        Ipart = 50e3
    Ipart = 200000
    
    #Distribution = '..'+os.sep+'..'+os.sep+'CH_beam200k_%.1fmm.ini' % 0.6
    Distribution = '..'+os.sep+'cath055eV.ini'
    
    generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'g', sig_clock = 6.0e-3/2.35482, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)

    newrun = Module('Newrun', Run = Run, Head = 'PITZ beam line simulation', Distribution = Distribution,\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000, Qbunch = -Q_total,
                    XYrms = sigma_x, H_max = 0.001, H_min = 0, Lmonitor = False)
    #newrun.set(Run = Run)
    
    field_maps = '..'
    File_tesla = [field_maps+os.sep+'tesla1cav2.dat' for kkk in np.arange(8)]
    MaxE_tesla = [26.94 for kkk in np.arange(8)]
    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 50, Nlong_in = 100, N_min = 30, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'k452.dat']+File_tesla,\
                    MaxE = [MaxE_gun]+MaxE_tesla, C_pos = [0., 4.04, 5.42, 6.80, 8.18, 9.56, 10.94, 12.32, 13.70],\
                    Nue = [1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3], Phi = [phi_gun, 0, 0, 0, 0, 0, 0, 0, 0])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'Bsol_with_pipe_m100.txt'], MaxB = MaxB, S_pos = [0.4])
    
    screen = [0.5, 0.8030, 1.3790, 1.7080, 5.2770, 6.2500, 7.1250, 8.4100, \
              8.9200, 12.2780, 13.0380, 13.7980, 14.5580, 15.3180, 16.3030, 18.2620]
    output = Module('Output', Zstart = 0, Zstop = 14.35, Zemit = 500, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, C_EmitS = True, LPROJECT_EMIT = False,\
                    LOCAL_EMIT = False)
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    
    
    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, output])
    
    direc = str.format('Q%.1fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Q_total*1e3, x[0], MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    os.system('mkdir -p '+direc)
    # os.chdir(direc)
    
    job_name = 'myjob.'+('D-%.2fmm-phi1-%.2fdeg-I-%.1fA' % (x[0], x[1], x[2]))
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, gen_name, direc)
    
    return

def obj_SCgunScan2(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameter:
      x: an array or list of the variables to be optimized
    Return:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''
    
    para = np.loadtxt('..'+os.sep+'para-flattop2.txt')
    select = (para[:,0]==x[0]); para = para[select]; print((x, para.shape))
    
    sigma_x = sigma_y = x[0]/4. # uniform
    C_sigma_x = C_sigma_y = 0
    #sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    #C_sigma_x = C_sigma_y = x[0]/2./sigma_x
    
    phi_gun, phi_booster = 0, 0
    Imain = x[1]
    
    MaxE_gun = para[0,5]
    MaxE_booster = para[0,6] #get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)
    
    Q_total = 0.1 # nC
    
    Run = 1
    if len(args)>0:
        Run = args[0]
        
    Ipart = int(Q_total*100e3)
    if Ipart<50e3:
        Ipart = 50e3
    Ipart = 200000
    
    Distribution = '..'+os.sep+'CH_beam200k_%.1fmm.ini' % x[0]
    Distribution = 'beam.ini'
    
    generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'p', LT = para[0,3]*1e-3, RT = para[0,4]*1e-3, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)

    newrun = Module('Newrun', Run = Run, Head = 'PITZ beam line simulation',\
                    Distribution = Distribution, CathodeS = True,\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000, Qbunch = -Q_total)
    #newrun.set(Run = Run)
    
    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 30, Nlong_in = 50, N_min = 30, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt', field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    screen = [0.5, 0.8030, 1.3790, 1.7080, 5.2770, 6.2500, 7.1250, 8.4100, \
              8.9200, 12.2780, 13.0380, 13.7980, 14.5580, 15.3180, 16.3030, 18.2620]
    output = Module('Output', Zstart = 0, Zstop = 6, Zemit = 120, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, C_EmitS = True, LPROJECT_EMIT = True,\
                    LOCAL_EMIT = True, Screen = [0.5, 5.28])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    
    
    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, output])
    
    direc = str.format('Q%.1fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Q_total*1e3, x[0], MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    os.system('mkdir -p '+direc)
    # os.chdir(direc)
    
    job_name = 'myjob.'+('D-%.2fmm-I-%.0fA' % (x[0], x[1]))
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, gen_name, direc)
    
    return

def obj_SCgunScan(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameter:
      x: an array or list of the variables to be optimized
    Return:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''
    
    #para = np.loadtxt('..'+os.sep+'para-gaussian.txt')
    #select = (para[:,0]==x[0]); para = para[select]
    
    sigma_x = sigma_y = 0.6/4. # uniform
    C_sigma_x = C_sigma_y = 0
    #sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    #C_sigma_x = C_sigma_y = x[0]/2./sigma_x
    
    phi_gun, phi_booster = 0, 0
    Imain = x[1]
    
    MaxE_gun = 40 #para[0,3]
    MaxE_booster = 15.2 #para[0,4] #get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)
    
    Q_total = x[0]/1e3 #/20e3*para[0,5]
    
    Run = 1
    if len(args)>0:
        Run = args[0]
        
    Ipart = int(Q_total*100e3)
    if Ipart<50e3:
        Ipart = 50e3
    Ipart = 200000
    
    #Distribution = '..'+os.sep+'..'+os.sep+'CH_beam200k_%.1fmm.ini' % 0.6
    Distribution = 'beam.ini'
    
    generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'g', sig_clock = 6.0e-3/2.35482, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)

    newrun = Module('Newrun', Run = Run, Head = 'PITZ beam line simulation', Distribution = Distribution,\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000, Qbunch = -Q_total)
    #newrun.set(Run = Run)
    
    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 30, Nlong_in = 50, N_min = 10, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt', field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    screen = [0.5, 0.8030, 1.3790, 1.7080, 5.2770, 6.2500, 7.1250, 8.4100, \
              8.9200, 12.2780, 13.0380, 13.7980, 14.5580, 15.3180, 16.3030, 18.2620]
    output = Module('Output', Zstart = 0, Zstop = 6, Zemit = 300, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, C_EmitS = True, LPROJECT_EMIT = True,\
                    LOCAL_EMIT = True, Screen = [0.5, 5.28])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    
    
    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, output])
    
    direc = str.format('Q%.1fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Q_total*1e3, 0.6, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    os.system('mkdir -p '+direc)
    # os.chdir(direc)
    
    job_name = 'myjob.'+('Q-%.0fpC-I-%.0fA' % (x[0], x[1]))
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, gen_name, direc)
    
    return

def obj_Twiss2Beam(x = None, *args, **kwargs):
    '''
    Optimize the beam size and beam covariance, given the emittance.
    Parameter:
      x: an array or list of the variables to be optimized, sigma_x, sigma_y, alpha_x and alpha_y
    Return:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    kv = kwargs
     
    P0 = 17.05 # MeV/c
    Ek = momentum2kinetic(P0); gamma_r = kinetic2gamma(Ek); bg_r = gamma2bg(gamma_r)
    nemit_x = nemit_y = 4e-6 # m*rad
    
    ###
    nemit_x, nemit_y = 4.086*1e-6, 3.655*1e-6
    ###
    
    emit_x = nemit_x/bg_r; emit_y = nemit_y/bg_r
    sigma_Ek = 0.5e-2*Ek*1e3 # keV
    
    ###
    #if x is not None:
    #    # sigma_x in mm, alpha_x dimensionless
    #    sigma_x, sigma_y, alpha_x, alpha_y = x 
    #else:
    #    sigma_x, sigma_y, alpha_x, alpha_y = 1.576772E+00,   1.775836E-01,   7.183778E+00,   1.716220E+00
    sigma_x, sigma_y, alpha_x, alpha_y = 1.47, 0.32, 8.08*4e-6/nemit_x, 4.73*4e-6/nemit_y
    ###
    beta_x, beta_y = sigma_x**2/emit_x*1e-6, sigma_y**2/emit_y*1e-6
    
    beta_x, beta_y, alpha_x, alpha_y = 16.5, 19.9, -4.21, -4.00
    sigma_x, sigma_y = np.sqrt(beta_x*emit_x)*1e3, np.sqrt(beta_y*emit_y)*1e3
    
    gamma_x, gamma_y = (1.+alpha_x**2)/beta_x, (1.+alpha_y**2)/beta_y
    
    cor_px = -alpha_x/beta_x*sigma_x
    cor_py = -alpha_y/beta_y*sigma_y
    
    #OBJECTS[2], OBJECTS[3], OBJECTS[4], OBJECTS[5] = beta_x, beta_y, -alpha_x, -alpha_y
    
    WEIGHTS[0], WEIGHTS[1], WEIGHTS[2], WEIGHTS[3], WEIGHTS[4], WEIGHTS[5] = \
        0, 0, 5./55,   11./55,   5./55,                    11./55    
    OBJECTS[0], OBJECTS[1], OBJECTS[2], OBJECTS[3], OBJECTS[4], OBJECTS[5] = \
        0, 0, sigma_x, sigma_y, -(-alpha_x*nemit_x/bg_r), -(-alpha_y*nemit_y/bg_r)
    
    if len(args) > 0:
        execute = args[0]
    else:
        execute = 1
    #Distribution = '../ast.2700.001'
    #if 'Distribution' in kv.keys():
    #    Distribution = kv['Distribution']
        
    Run = 1
    if 'Run' in list(kv.keys()):
        Run = kv['Run']
    
    Ipart = 200000
    generator = Generator1(FNAME = 'ast.0528.001', IPart = Ipart, Species = 'electrons', Q_total = -2.5,\
                          Ref_Ekin = Ek, sig_Ekin = 0, cor_Ekin = sigma_Ek, Dist_pz = 'g', Cathode = False,\
                          Dist_z = 'g', sig_clock = 16*1e-3/2.35482,\
                          Dist_x = 'g', sig_x = sigma_x, Dist_px = 'g', Nemit_x = nemit_x*1e6, cor_px = cor_px,\
                          Dist_y = 'g', sig_y = sigma_y, Dist_py = 'g', Nemit_y = nemit_y*1e6, cor_py = cor_py)
    
    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = 'beam_und.ini',\
                    Auto_Phase = True, Track_All = True, Track_On_Axis = False, check_ref_part = False,\
                    Lprompt = False)
    newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, LSPCH3D = True, Nxf = 16, Nyf = 16, Nzf = 16,\
                    Max_scale = 0.05, Max_count = 20)
    
    cavity = Module('Cavity', LEfield = True, By_stat = [True], File_Efield = [field_maps+os.sep+'3Dund-15x5mm'],\
                    MaxE = [0], C_pos = [0], Nue = [0], Phi = [0])
    
    output = Module('Output', Zstart = 0, Zstop = 3.6, Zemit = 180, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [1.8, 3.6])

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, output])

    ###
    #direc = str.format('n%.0fk-sig_x-%.2fmm-sig_y-%.2fmm-alp_x-%.2f-alp_y-%.2f' %\
    #                   (Ipart/1000., sigma_x, sigma_y, alpha_x, alpha_y))
    direc = str.format('nemit_x-%.2fum-nemit_y-%.2fum' %\
                       (x[0], x[1]))
    ###
    direc = '.'
    # os.system('mkdir -p '+direc)
    # os.chdir(direc)

    job_name = '%s' % direc
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    #astra.write(direc+os.sep+ast_name+'.in')
    #astra.qsub(job_name, ast_name, gen_name, direc)
    
    # os.chdir('../')
    
    # os.system('generator gen.in > gen.log')
    # os.system('astra ast.in > ast.log')
    return

    direc = str.format('n%.0fk-sig_x-%.2fmm-sig_y-%.2fmm-alp_x-%.2f-alp_y-%.2f' %\
                       (Ipart/1000., sigma_x, sigma_y, alpha_x, alpha_y))
    
    os.system('mkdir -p '+direc)
    os.chdir(direc)

    generator.write()
    astra.write()
    
    if not execute:
        os.system('generator gen.in 2>&1 | tee gen.log')
        os.chdir('../')
        obj = -1
    else:
        os.system('generator gen.in 2>&1 | tee gen.log')
        os.system('astra ast.in 2>&1 | tee ast.log')

        try:
            fname = 'ast.0360.001'
            beam = np.loadtxt(fname)
            beam[1:,2] += beam[0,2]; beam[1:,5] += beam[0,5]
            diag = BeamDiagnostics(beam)

            # when there is no beam loss to the cathode or to the aperture -alpha_x*nemit_x
            if diag.loss_cathode<=50 and diag.loss_aperture<=50: 
                obj = np.sqrt(WEIGHTS[0]*np.fabs(diag.nemit_x-OBJECTS[0])+WEIGHTS[1]*np.fabs(diag.nemit_y-OBJECTS[1])+
                              WEIGHTS[2]*np.fabs(diag.std_x  -OBJECTS[2])+WEIGHTS[3]*np.fabs(diag.std_y  -OBJECTS[3])+
                              WEIGHTS[4]*np.fabs(-diag.alpha_x*diag.emit_x*1e6-OBJECTS[4])+
                              WEIGHTS[5]*np.fabs(-diag.alpha_y*diag.emit_y*1e6-OBJECTS[5]))
            else:
                obj = 1e3
        except:
            obj = 1e4
            print('Error: not accelerated well!')

        os.chdir('../')

        res = [Ipart, sigma_x, sigma_y, alpha_x, alpha_y, obj]
        with open('results.dat','a') as f_handle:
            np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_THzMatching_3nC(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    data = np.loadtxt('opt-quads_.dat')
    grad2 = interp1d(data[:,0], data[:,1])
    grad3 = interp1d(data[:,0], data[:,2])
    
    y = np.array([x[0], grad2(x[0]), grad3(x[0]), x[1], x[2], x[3]])*astra_to_sco
    x = list(y)
    
    #sigma_x = sigma_y = 1.8/4. # uniform
    #C_sigma_x = C_sigma_y = 0
    sigma_x = sigma_y = 4.0/2.355 # Gaussian truncated
    C_sigma_x = C_sigma_y = 4/2./sigma_x
    
    phi_gun, phi_booster = 0, -20
    Imain = 380
    
    MaxE_gun = 58.75
    MaxE_booster = 12.72 # get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = 2.5
    FWHM = 6 # ps
    
    Run = 1
    if len(args)>0:
        Run = args[0]

    Ipart = 1000 #int(Q_total*100e3)
    
    generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'g', sig_clock = FWHM*1e-3/2.35482, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)

    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = '../ast.0625.001',\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    #newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 30, Nlong_in = 50, N_min = 10,\
                    NXF = 16, NYF = 16, NZF = 16, L2D_3D = True, Z_TRANS = 5.0, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt',\
                                                             field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = 16.5, Zemit = 825, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [0.50, 15.320, 16.3030])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    quadru = Module('Quadrupole', LQuad = True,\
                    Q_type = ['../Q3.data', '../Q3.data', '../Q3.data', '../Q3.data', '../Q3.data', '../Q3.data'],\
                    Q_pos = [position['HIGH1.Q6'], position['HIGH1.Q8'], position['HIGH1.Q9'],\
                             position['PST.QT3'], position['PST.QT4'], position['PST.QT5']], Q_grad = x)
    
    astra = Astra()
    astra.add_modules([newrun, charge, quadru, apertu, output])

    direc = str.format('Q1-%.2fT_m-Q2-%.2fT_m-Q3-%.2fT_m-Q4-%.2fT_m-Q5-%.2fT_m-Q6-%.2fT_m' %\
                       tuple(x))
    
    os.system('mkdir -p '+direc)
    os.chdir(direc)

    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(gen_name+'.in')
    astra.write(ast_name+'.in')
    
    #os.chdir(direc)
    #os.system('generator gen.in > gen.log')
    os.system('astra ast.in > ast.log')
    
    try:
        fname = 'ast.1532.001'
        obj = obj_calc4(fname)
    except:
        # if no output beam file, then set the objective to 10000
        obj = [1e4]
        print('Error: not accelerated well!')

    try:
        os.system('rm ast.0650.001')
        os.system('rm ast.1650.001')
    except:
        pass
    
    os.chdir('..'); #os.system('rm -r '+direc)

    res = list(x)+list(obj)
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj[0]

def obj_THzMatching4(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    data = np.loadtxt('opt-quads_.dat')
    grad2 = interp1d(data[:,0], data[:,1])
    grad3 = interp1d(data[:,0], data[:,2])
    
    y = np.array([x[0], grad2(x[0]), grad3(x[0]), x[1], x[2], x[3]])*astra_to_sco
    x = list(y)
    
    #sigma_x = sigma_y = 1.8/4. # uniform
    #C_sigma_x = C_sigma_y = 0
    sigma_x = sigma_y = 4.0/2.355 # Gaussian truncated
    C_sigma_x = C_sigma_y = 4/2./sigma_x
    
    phi_gun, phi_booster = 0, -20
    Imain = 380
    
    MaxE_gun = 58.75
    MaxE_booster = 12.72 # get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = 2.5
    FWHM = 6 # ps
    
    Run = 1
    if len(args)>0:
        Run = args[0]

    Ipart = 1000 #int(Q_total*100e3)
    
    generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'g', sig_clock = FWHM*1e-3/2.35482, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)

    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = '../ast.0650.001',\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    #newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 30, Nlong_in = 50, N_min = 10,\
                    NXF = 16, NYF = 16, NZF = 16, L2D_3D = True, Z_TRANS = 5.0, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt',\
                                                             field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = 16.5, Zemit = 825, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [0.50, 15.320, 16.3030])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    quadru = Module('Quadrupole', LQuad = True,\
                    Q_type = ['../Q3.data', '../Q3.data', '../Q3.data', '../Q3.data', '../Q3.data', '../Q3.data'],\
                    Q_pos = [position['HIGH1.Q6'], position['HIGH1.Q8'], position['HIGH1.Q9'],\
                             position['PST.QT3'], position['PST.QT4'], position['PST.QT5']], Q_grad = x)
    
    astra = Astra()
    astra.add_modules([newrun, charge, quadru, apertu, output])

    direc = str.format('Q1-%.2fT_m-Q2-%.2fT_m-Q3-%.2fT_m-Q4-%.2fT_m-Q5-%.2fT_m-Q6-%.2fT_m' %\
                       tuple(x))
    
    os.system('mkdir -p '+direc)
    os.chdir(direc)

    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(gen_name+'.in')
    astra.write(ast_name+'.in')
    
    #os.chdir(direc)
    #os.system('generator gen.in > gen.log')
    os.system('astra ast.in > ast.log')
    
    try:
        fname = 'ast.1532.001'
        obj = obj_calc4(fname)
    except:
        # if no output beam file, then set the objective to 10000
        obj = [1e4]
        print('Error: not accelerated well!')

    try:
        os.system('rm ast.0650.001')
        os.system('rm ast.1650.001')
    except:
        pass
    
    os.chdir('..'); #os.system('rm -r '+direc)

    res = list(x)+list(obj)
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj[0]

def obj_triplet2(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    #sigma_x = sigma_y = 1.8/4. # uniform
    #C_sigma_x = C_sigma_y = 0
    sigma_x = sigma_y = 4.0/2.355 # Gaussian truncated
    C_sigma_x = C_sigma_y = x[0]/2./sigma_x
    
    phi_gun, phi_booster = 0, -20
    Imain = 380
    
    MaxE_gun = 58.75
    MaxE_booster = 12.72 # get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = 2.5
    FWHM = 6 # ps
    
    Run = 1
    if len(args)>0:
        Run = args[0]

    Ipart = 1000 #int(Q_total*100e3)
    
    generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'g', sig_clock = FWHM*1e-3/2.35482, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)

    newrun = Module('Newrun', Run = 2, Head = 'PITZ beam line simulation', Distribution = '../ast.0528.001',\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    #newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 30, Nlong_in = 50, N_min = 10,\
                    NXF = 16, NYF = 16, NZF = 16, L2D_3D = True, Z_TRANS = 5.0, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt',\
                                                             field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = 8.18, Zemit = 200, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [0.50, 7.5, 8.18])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    quadru = Module('Quadrupole', LQuad = True,\
                    Q_type = ['../Q3.data', '../Q3.data'],\
                    Q_pos = [position['HIGH1.Q4'], position['HIGH1.Q6']], Q_grad = x)
    
    astra = Astra()
    astra.add_modules([newrun, charge, quadru, output])

    ##direc = str.format('Q1-%.2fT_m-Q2-%.2fT_m-Q3-%.2fT_m-Q4-%.2fT_m-Q5-%.2fT_m-Q6-%.2fT_m' %\
    ##                   tuple(x))
    
    ##os.system('mkdir -p '+direc)
    ##os.chdir(direc)

    ##gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    ##generator.write(gen_name+'.in')
    astra.write(ast_name+'.in')
    
    ##os.chdir(direc)
    ##os.system('generator gen.in > gen.log')
    os.system('astra ast.in > ast.log')
    
    try:
        fname = 'ast.0818.002'
        
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag = BeamDiagnostics(dist = dist)
    
        obj = [np.abs(1-diag.std_x/diag.std_y)]+list(diag.x)
    except:
        # if no output beam file, then set the objective to 10000
        obj = [1e4]
        print('Error: not accelerated well!')

    try:
        os.system('rm ast.0528.002')
        #os.system('rm ast.0820.002')
    except:
        pass
    
    ##os.chdir('..'); #os.system('rm -r '+direc)

    res = list(x)+list(obj)
    with open('results2.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj[0]

def obj_triplet3(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    #sigma_x = sigma_y = 1.8/4. # uniform
    #C_sigma_x = C_sigma_y = 0
    sigma_x = sigma_y = 4.0/2.355 # Gaussian truncated
    C_sigma_x = C_sigma_y = x[0]/2./sigma_x
    
    phi_gun, phi_booster = 0, -20
    Imain = 380
    
    MaxE_gun = 58.75
    MaxE_booster = 12.72 # get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = 2.5
    FWHM = 6 # ps
    
    Run = 1
    if len(args)>0:
        Run = args[0]

    Ipart = 1000 #int(Q_total*100e3)
    
    generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'g', sig_clock = FWHM*1e-3/2.35482, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)

    newrun = Module('Newrun', Run = 3, Head = 'PITZ beam line simulation', Distribution = 'ast.0750.002',\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    #newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 30, Nlong_in = 50, N_min = 10,\
                    NXF = 16, NYF = 16, NZF = 16, L2D_3D = True, Z_TRANS = 5.0, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt',\
                                                             field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = 12, Zemit = 600, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [0.50, 10])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    quadru = Module('Quadrupole', LQuad = True,\
                    Q_type = ['../Q3.data', '../Q3.data', '../Q3.data'],\
                    Q_pos = [position['HIGH1.Q4'], position['HIGH1.Q6'], position['HIGH1.Q7']], Q_grad = x)
    
    astra = Astra()
    astra.add_modules([newrun, charge, quadru, output])

    ##direc = str.format('Q1-%.2fT_m-Q2-%.2fT_m-Q3-%.2fT_m-Q4-%.2fT_m-Q5-%.2fT_m-Q6-%.2fT_m' %\
    ##                   tuple(x))
    
    ##os.system('mkdir -p '+direc)
    ##os.chdir(direc)

    ##gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    ##generator.write(gen_name+'.in')
    astra.write(ast_name+'.in')
    
    ##os.chdir(direc)
    ##os.system('generator gen.in > gen.log')
    os.system('astra ast.in > ast.log')
    
    try:
        fname = 'ast.1000.003'
        
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag = BeamDiagnostics(dist = dist)
    
        std_x,   std_y   =  diag.std_x,   diag.std_y
        cov_x,   cov_y   = -diag.alpha_x*diag.emit_x*1e6, -diag.alpha_y*diag.emit_y*1e6
        
        #obj = [np.sqrt((1-std_x/std_y)**2+(1-cov_x/cov_y)**2)]+list(diag.x)
        
        xemit = np.loadtxt('ast.Xemit.003')
        yemit = np.loadtxt('ast.Yemit.003')
        
        plot_rms_xy(suffix = '003', extent = [7, 12, 0, 4], fig_ext = str.format('@Q3_%.2fT_m.eps' % x[2]))
        
        select = (xemit[:,0]>9.5)*(xemit[:,0]<10.5)
        xemit = xemit[select]; yemit = yemit[select]
        rr = np.sum(xemit[:,3]-yemit[:,3]); #print 'obj = ', rr
        obj = [rr]+list(diag.x)
        
    except:
        # if no output beam file, then set the objective to 10000
        obj = [1e4]
        print('Error: not accelerated well!')

    try:
        os.system('rm ast.0750.003')
    except:
        pass
    
    ##os.chdir('..'); #os.system('rm -r '+direc)

    res = list(x)+list(obj)
    with open('results3.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj[0]

def obj_quads(x, *args, **kwargs):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameters:
      x: an array or list of the variables to be optimized, here the gradients of quads
      *args: the names of quads
      **kwargs: key-value pairs of Astra module property, e.g., 'Run', 1
    Returns:
      direc: the directory where Astra simulation runs
    '''

    #sigma_x = sigma_y = 1.8/4. # uniform
    #C_sigma_x = C_sigma_y = 0
    sigma_x = sigma_y = 4.0/2.355 # Gaussian truncated
    C_sigma_x = C_sigma_y = x[0]/2./sigma_x
    
    phi_gun, phi_booster = 0, -20
    Imain = 380
    
    MaxE_gun = 58.75
    MaxE_booster = 12.72 # get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = 4
    FWHM = 6 # ps

    if len(args)>0:
        quadNames = args[0]
        nquads = len(quadNames)
        
    if len(x) < len(quadNames):
        dN = len(quadNames)-len(x)
        x = list(x) + [0 for _ in np.arange(dN)]
    
    #x = [-1.50, 1.35, -1.600000, 2.328905, -1.526572, -1.600000, 2.754909, -1.789562, -0.795222]+list(x[2:4])
    #x = [-1.50, 1.35, -1.600000, 2.328905, -1.526572, 1.250000, -2.632771, 1.473200, -0.795222]+list(x[2:4])
    #quadNames = ['HIGH1.Q7', 'HIGH1.Q8', 'PST.QM3', 'PST.QT1', 'PST.QT2', 'PST.QT4', 'PST.QT5', 'PST.QT6', 'HIGH2.Q1']
    
    #x = [0, 0, -1.275000, 1.923496, -1.207427]+list(x[2:4])
    #x = [0, 0, -1.250000, 1.885753, -1.158009]+list(x[2:4]) 
    #quadNames = ['HIGH1.Q7', 'HIGH1.Q8', 'PST.QT2', 'PST.QT4', 'PST.QT6']
    
    #x = [-1.50, 1.35, -1.050000, 1.282803, -1.094932]+list(x[-2:]); print(x)
    #x = [-1.50, 1.35, -1.000000, 1.2219178, -0.95243607]+list(x[-2:]); print(x)
    #x = [-1.50, 1.35, -1.100000, 1.3427995, -1.3113866*1.2]+list(x[-2:]); print(x)
    #quadNames = ['HIGH1.Q7', 'HIGH1.Q8', 'PST.QT3', 'PST.QT6', 'HIGH2.Q1']
    
    #x = [-1.50, 1.35, -0.950000, 1.29770407, -1.0610348*0.9]+list(x[-2:]); print(x)
    #quadNames = ['HIGH1.Q7', 'HIGH1.Q8', 'PST.QM3', 'PST.QT3', 'PST.QT6']
    
    x = [-1.50, 1.35, -0.950000, 1.160221281083949, -0.83125852]+list(x[-2:]); print(x)
    quadNames = ['HIGH1.Q7', 'HIGH1.Q8', 'PST.QT3', 'PST.QT6', 'HIGH2.Q1']
    
    nquads = len(quadNames)
    
    Run = 2
    Distribution = '..'+os.sep+'ast.0528.001'
    Zstop = scrns['HIGH2.SCR2']
    
    Screen = [0.5]
    if len(kwargs)>0:
        if 'Run' in list(kwargs.keys()):
            Run = kwargs['Run']

        if 'Distribution' in list(kwargs.keys()):
            Distribution = kwargs['Distribution']

        if 'Zstop' in list(kwargs.keys()):
            Zstop = kwargs['Zstop']

        if 'Screen' in list(kwargs.keys()):
            Screen = [0.5, 10]+kwargs['Screen']

    Ipart = 1000 #int(Q_total*100e3)
    Distribution = 'ast.1228.002'
    
    generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'g', sig_clock = FWHM*1e-3/2.35482, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)

    newrun = Module('Newrun', Run = Run, Head = 'PITZ beam line simulation', Distribution = Distribution,\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    newrun.set(Run = 9)
    #newrun.set(Track_All = False, Track_On_Axis = True)
    
    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 30, Nlong_in = 50, N_min = 10,\
                    NXF = 32, NYF = 32, NZF = 32, L2D_3D = True, Z_TRANS = 5.0, Max_scale = 0.05, Max_count = 20)
    #charge.set(LSPCH = False, L2D_3D = False)
    #charge.set(NXF = 16, NYF = 16, NZF = 16)
    #charge.set(LSPCH = False)
    
    cavity = Module('Cavity', LEfield = True, 
                    File_Efield = [field_maps+os.sep+'gun42cavity.txt',
                                   field_maps+os.sep+'CDS14_15mm.txt',
                                   '../../TDS_10/DY_3D_TDS_ImRe',
                                   '../../TDS_10/DY_3D_TDS_ReIm'],
                    MaxE = [MaxE_gun, MaxE_booster], 
                    C_noscale = [False, False, True, True],
                    C_pos = [0., 2.675, 10.7000, 10.7000],
                    Com_grid = [None, None, 'all', 'all'],
                    Nue = [1.3, 1.3, 2.9980 , 2.9980], 
                    Phi = [phi_gun, phi_booster, 90, 0])
    
    module = Module('Modules', Lmodule = True, Module = [['Cavity(3)'], ['Cavity(4)']], Mod_Efield = x[-2]*TDS_1MV_ratio, Mod_Phase = x[-1])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = Zstop, Zemit = int(Zstop*50), Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = Screen)
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    
    
    quadru = Module('Quadrupole', LQuad = True,\
                    Q_type = ['..'+os.sep+'Q3.data' for i in x[0:nquads]],\
                    Q_pos = [quads[name] for name in quadNames], Q_grad = x[0:nquads])
    print(quadNames, x)
    
    astra = Astra()
    astra.add_modules([newrun, charge, cavity, module, quadru, output])
    
    direc = ''
    for i in np.arange(len(x[:2])):
        direc += str.format('Q%d-%.2fT_m-' %  (i+7, x[i]))
    direc = direc[:-1]
    direc += str.format('-%.2f-%2f' % (x[-2], x[-1]))
    
    
    ### for Farm
    os.system('mkdir -p '+direc)
    #os.chdir(direc)
    
    job_name = direc
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, None, direc)
    
    return direc
    
    
    #os.system('mkdir -p '+direc)
    try:
        os.mkdir(direc)
        pass
    except OSError as err:
        #if err.errno != errno.EEXIST:
        #    raise
        pass
    
    os.chdir(direc)
    
    #generator.write('gen.in')
    astra.write('ast.in')
    
    #os.system('generator gen.in 2>&1 | tee gen.log')
    #os.system('astra ast.in 2>&1 | tee ast.log')
    
    os.system('astra ast.in')
    
    #os.chdir('..')
    
    return direc


def obj_triplet20(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    #sigma_x = sigma_y = 1.8/4. # uniform
    #C_sigma_x = C_sigma_y = 0
    sigma_x = sigma_y = 4.0/2.355 # Gaussian truncated
    C_sigma_x = C_sigma_y = x[0]/2./sigma_x
    
    phi_gun, phi_booster = 0, -20
    Imain = 380
    
    MaxE_gun = 58.75
    MaxE_booster = 12.72 # get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = 2.5
    FWHM = 6 # ps
    
    Run = 1
    if len(args)>0:
        Run = args[0]

    Ipart = 1000 #int(Q_total*100e3)
    
    generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'g', sig_clock = FWHM*1e-3/2.35482, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)

    newrun = Module('Newrun', Run = 2, Head = 'PITZ beam line simulation', Distribution = '../ast.0528.001',\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    #newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 30, Nlong_in = 50, N_min = 10,\
                    NXF = 16, NYF = 16, NZF = 16, L2D_3D = True, Z_TRANS = 5.0, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt',\
                                                             field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = 8.2, Zemit = 100, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [0.50, 7.5, 8.18])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    quadru = Module('Quadrupole', LQuad = True,\
                    Q_type = ['../Q3.data', '../Q3.data'],\
                    Q_pos = [position['HIGH1.Q4'], position['HIGH1.Q6']], Q_grad = x)
    
    astra = Astra()
    astra.add_modules([newrun, charge, quadru, output])

    ##direc = str.format('Q1-%.2fT_m-Q2-%.2fT_m-Q3-%.2fT_m-Q4-%.2fT_m-Q5-%.2fT_m-Q6-%.2fT_m' %\
    ##                   tuple(x))
    
    ##os.system('mkdir -p '+direc)
    ##os.chdir(direc)

    ##gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    ##generator.write(gen_name+'.in')
    astra.write(ast_name+'.in')
    
    ##os.chdir(direc)
    ##os.system('generator gen.in > gen.log')
    os.system('astra ast.in > ast.log')
    
    try:
        fname = 'ast.0818.002'
        
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag = BeamDiagnostics(dist = dist)
    
        obj = [np.abs(1-diag.std_x/diag.std_y)]+list(diag.x)
    except:
        # if no output beam file, then set the objective to 10000
        obj = [1e4]
        print('Error: not accelerated well!')

    try:
        os.system('rm ast.0528.002')
        os.system('rm ast.0820.002')
    except:
        pass
    
    ##os.chdir('..'); #os.system('rm -r '+direc)

    res = list(x)+list(obj)
    with open('results2.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj[0]

def obj_triplet30(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    #sigma_x = sigma_y = 1.8/4. # uniform
    #C_sigma_x = C_sigma_y = 0
    sigma_x = sigma_y = 4.0/2.355 # Gaussian truncated
    C_sigma_x = C_sigma_y = x[0]/2./sigma_x
    
    phi_gun, phi_booster = 0, -20
    Imain = 380
    
    MaxE_gun = 58.75
    MaxE_booster = 12.72 # get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = 2.5
    FWHM = 6 # ps
    
    Run = 1
    if len(args)>0:
        Run = args[0]

    Ipart = 1000 #int(Q_total*100e3)
    
    generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'g', sig_clock = FWHM*1e-3/2.35482, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)

    newrun = Module('Newrun', Run = 3, Head = 'PITZ beam line simulation', Distribution = 'ast.0750.002',\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    #newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 30, Nlong_in = 50, N_min = 10,\
                    NXF = 16, NYF = 16, NZF = 16, L2D_3D = True, Z_TRANS = 5.0, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt',\
                                                             field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = 9.0, Zemit = 900, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [0.50, 8.5, 9])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    quadru = Module('Quadrupole', LQuad = True,\
                    Q_type = ['../Q3.data', '../Q3.data', '../Q3.data'],\
                    Q_pos = [position['HIGH1.Q4'], position['HIGH1.Q6'], position['HIGH1.Q7']], Q_grad = x)
    
    astra = Astra()
    astra.add_modules([newrun, charge, quadru, output])

    ##direc = str.format('Q1-%.2fT_m-Q2-%.2fT_m-Q3-%.2fT_m-Q4-%.2fT_m-Q5-%.2fT_m-Q6-%.2fT_m' %\
    ##                   tuple(x))
    
    ##os.system('mkdir -p '+direc)
    ##os.chdir(direc)

    ##gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    ##generator.write(gen_name+'.in')
    astra.write(ast_name+'.in')
    
    ##os.chdir(direc)
    ##os.system('generator gen.in > gen.log')
    os.system('astra ast.in > ast.log')
    
    try:
        fname = 'ast.0900.003'
        
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag = BeamDiagnostics(dist = dist)
    
        std_x,   std_y   =  diag.std_x,   diag.std_y
        cov_x,   cov_y   = -diag.alpha_x*diag.emit_x*1e6, -diag.alpha_y*diag.emit_y*1e6
        
        #obj = [np.sqrt((1-std_x/std_y)**2+(1-cov_x/cov_y)**2)]+list(diag.x)
        
        xemit = np.loadtxt('ast.Xemit.003')
        yemit = np.loadtxt('ast.Yemit.003')
        select = (xemit[:,0]>8.5)*(xemit[:,0]<10.0)
        xemit = xemit[select]; yemit = yemit[select]
        rr = np.sum(xemit[:,3]-yemit[:,3]); #print 'obj = ', rr
        obj = [rr]+list(diag.x)
        
    except:
        # if no output beam file, then set the objective to 10000
        obj = [1e4]
        print('Error: not accelerated well!')

    try:
        os.system('rm ast.0750.003')
    except:
        pass
    
    ##os.chdir('..'); #os.system('rm -r '+direc)

    res = list(x)+list(obj)
    with open('results3.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj[0]

def obj_THzMatching3(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    data = np.loadtxt('opt-quads.dat')
    grad2 = interp1d(data[:,0], data[:,1])
    grad3 = interp1d(data[:,0], data[:,2])
    
    y = np.array([x[0], grad2(x[0]), grad3(x[0]), x[1], x[2], x[3]])*astra_to_sco
    x = list(y)
    
    #sigma_x = sigma_y = 1.8/4. # uniform
    #C_sigma_x = C_sigma_y = 0
    sigma_x = sigma_y = 4.0/2.355 # Gaussian truncated
    C_sigma_x = C_sigma_y = 4/2./sigma_x
    
    phi_gun, phi_booster = 0, -20
    Imain = 380
    
    MaxE_gun = 58.75
    MaxE_booster = 12.72 # get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = 2.5
    FWHM = 6 # ps
    
    Run = 1
    if len(args)>0:
        Run = args[0]

    Ipart = 1000 #int(Q_total*100e3)
    
    generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'g', sig_clock = FWHM*1e-3/2.35482, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)

    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = '../ast.0528.001',\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    #newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 30, Nlong_in = 50, N_min = 10,\
                    NXF = 16, NYF = 16, NZF = 16, L2D_3D = True, Z_TRANS = 5.0, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt',\
                                                             field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = 16.5, Zemit = 825, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [0.50, 15.320, 16.3030])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    quadru = Module('Quadrupole', LQuad = True,\
                    Q_type = ['../Q3.data', '../Q3.data', '../Q3.data', '../Q3.data', '../Q3.data', '../Q3.data'],\
                    Q_pos = [position['HIGH1.Q4'], position['HIGH1.Q6'], position['HIGH1.Q7'],\
                             position['PST.QT3'], position['PST.QT4'], position['PST.QT5']], Q_grad = x)
    
    astra = Astra()
    astra.add_modules([newrun, charge, quadru, apertu, output])

    direc = str.format('Q1-%.2fT_m-Q2-%.2fT_m-Q3-%.2fT_m-Q4-%.2fT_m-Q5-%.2fT_m-Q6-%.2fT_m' %\
                       tuple(x))
    
    os.system('mkdir -p '+direc)
    os.chdir(direc)

    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(gen_name+'.in')
    astra.write(ast_name+'.in')
    
    #os.chdir(direc)
    #os.system('generator gen.in > gen.log')
    os.system('astra ast.in > ast.log')
    
    try:
        fname = 'ast.1532.001'
        obj = obj_calc4(fname)
    except:
        # if no output beam file, then set the objective to 10000
        obj = [1e4]
        print('Error: not accelerated well!')

    try:
        os.system('rm ast.0528.001')
        os.system('rm ast.1650.001')
    except:
        pass
    
    os.chdir('..'); #os.system('rm -r '+direc)

    res = list(x)+list(obj)
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj[0]

def obj_THzMatching(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    #sigma_x = sigma_y = 1.8/4. # uniform
    #C_sigma_x = C_sigma_y = 0
    sigma_x = sigma_y = 4.0/2.355 # Gaussian truncated
    C_sigma_x = C_sigma_y = x[0]/2./sigma_x
    
    phi_gun, phi_booster = 0, -20
    Imain = 380
    
    MaxE_gun = 58.75
    MaxE_booster = 12.72 # get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = 2.5
    FWHM = 6 # ps
    
    Run = 1
    if len(args)>0:
        Run = args[0]

    Ipart = 1000 #int(Q_total*100e3)
    
    generator = Generator1(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'g', sig_clock = FWHM*1e-3/2.35482, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)

    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = '../ast.0528.001',\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    #newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 30, Nlong_in = 50, N_min = 10,\
                    NXF = 16, NYF = 16, NZF = 16, L2D_3D = True, Z_TRANS = 5.0, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt',\
                                                             field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = 16.5, Zemit = 825, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [0.50, 15.320, 16.3030])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    quadru = Module('Quadrupole', LQuad = True,\
                    Q_type = ['../Q3.data', '../Q3.data', '../Q3.data', '../Q3.data', '../Q3.data', '../Q3.data'],\
                    Q_pos = [position['HIGH1.Q4'], position['HIGH1.Q6'], position['HIGH1.Q7'],\
                             position['PST.QT3'], position['PST.QT4'], position['PST.QT5']], Q_grad = x)
    
    astra = Astra()
    astra.add_modules([newrun, charge, quadru, apertu, output])

    direc = str.format('Q1-%.2fT_m-Q2-%.2fT_m-Q3-%.2fT_m-Q4-%.2fT_m-Q5-%.2fT_m-Q6-%.2fT_m' %\
                       tuple(x))
    
    os.system('mkdir -p '+direc)
    os.chdir(direc)

    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(gen_name+'.in')
    astra.write(ast_name+'.in')
    
    #os.chdir(direc)
    #os.system('generator gen.in > gen.log')
    os.system('astra ast.in > ast.log')
    
    try:
        fname = 'ast.1532.001'
        obj = obj_calc(fname)
    except:
        # if no output beam file, then set the objective to 10000
        obj = [1e4]
        print('Error: not accelerated well!')

    try:
        os.system('rm ast.0528.001')
        os.system('rm ast.1650.001')
    except:
        pass
    
    os.chdir('..'); #os.system('rm -r '+direc)

    res = list(x)+list(obj)
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj[0]

def obj_quad2Scan(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameter:
      x: an array or list of the variables to be optimized
    Return:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    #sigma_x = sigma_y = x[0]/4. # uniform
    #C_sigma_x = C_sigma_y = 0
    sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    C_sigma_x = C_sigma_y = 1.8/2./sigma_x
    
    phi_gun, phi_booster = 0, 0
    Imain = 330
    
    MaxE_gun = 51.64
    MaxE_booster = 18.06 # get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = 0.001
    
    grad1 = x[0]
    grad2 = x[1]
    
    Run = 1
    if len(args)>0:
        Run = args[0]
        
    Ipart = int(Q_total*200e3)
    generator = Generator(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'g', sig_clock = 0.25e-3/2.35482, Cathode = True,\
                          Dist_x = '2', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = '2', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)

    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = '../ast.0841.002',\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    #newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 50, Nlong_in = 50, N_min = 10,\
                    NXF = 16, NYF = 16, NZF = 16, L2D_3D = True, Z_TRANS = 4.5, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt', field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = 14, Zemit = 700, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [0.25, 0.50, 5.277, 12.2780])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
      
    quadru = Module('Quadrupole', LQuad = True, Q_type = ['../Q3.data', '../Q3.data'],\
                    Q_pos = [position['HIGH1.Q9'], position['HIGH1.Q10']], Q_grad = [grad1*astra_to_sco, grad2*astra_to_sco])
                                   
    astra = Astra()
    astra.add_modules([newrun, charge, quadru, apertu, output])

    direc = str.format('Q1-%.2fT_m-Q2-%.2fT_m_5' %\
                       (grad1, grad2))
    os.system('mkdir -p '+direc)
    # os.chdir(direc)

    job_name = 'myjob.'+('Q1-%.2fT_m-Q2-%.2fT_m' % (grad1, grad2))
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, gen_name, direc)
    
    # os.chdir('../')
    
    # os.system('generator gen.in > gen.log')
    # os.system('astra ast.in > ast.log')
    return
    
    try:
        fname = 'ast.0528.001'
        beam = np.loadtxt(fname)
        beam[1:,2] += beam[0,2]; beam[1:,5] += beam[0,5]
        tmp = astra_post(beam)

        if tmp[20]<=50 and tmp[21]<=50: # when there is no beam loss to the cathode or to the aperture
            nemit_x, nemit_y, beta_x, beta_y, alpha_x, alpha_y = tmp[1], tmp[2], tmp[13], tmp[17], tmp[12], tmp[16]
            obj = np.sqrt(WEIGHTS[0]*(nemit_x-OBJECTS[0])**2+WEIGHTS[1]*(nemit_y-OBJECTS[1])**2+ 
                          WEIGHTS[2]*(beta_x -OBJECTS[2])**2+WEIGHTS[3]*(beta_y -OBJECTS[3])**2+
                          WEIGHTS[4]*(alpha_x-OBJECTS[4])**2+WEIGHTS[5]*(alpha_y-OBJECTS[5])**2)
        else:
            obj = 1e2
    except:
        obj = 1e3
        print('Error: not accelerated well!')

    os.chdir('..'+os.sep); #os.system('rm -r '+direc)

    res = [Ipart, sigma_x*4, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_undScan_12THz(x = None, *args, **kwargs):
    '''
    Optimize the beam size and beam covariance, given the emittance.
    Parameter:
      x: an array or list of the variables to be optimized, sigma_x, sigma_y, alpha_x and alpha_y
    Return:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    kv = kwargs
    
    N_u = 150; lam_u = 0.04; L_u = N_u*lam_u
    By_u_max = 0.8497768868897678 # T
    pos_u = L_u/2.
    
    P0 = 20.0 # MeV/c
    Ek = momentum2kinetic(P0); 
    Ek = 35
    gamma_r = kinetic2gamma(Ek); bg_r = gamma2bg(gamma_r)
    nemit_x = nemit_y = 3e-6 # m*rad
    
    ###
    #nemit_x, nemit_y = x[0]*1e-6, x[1]*1e-6
    ###
    
    emit_x = nemit_x/bg_r; emit_y = nemit_y/bg_r
    cor_Ekin = -0.5e-2*Ek*1e3 # keV
    sig_Ekin = 0.05e-2*Ek*1e3 # keV
    
    ###
    if x is not None:
        # sigma_x in mm, alpha_x dimensionless
        sigma_x, sigma_y, alpha_x, alpha_y = x 
    #else:
    #    sigma_x, sigma_y, alpha_x, alpha_y = 1.576772E+00,   1.775836E-01,   7.183778E+00,   1.716220E+00
    #sigma_x, sigma_y, alpha_x, alpha_y = 1.47, 0.32, 8.08*4e-6/nemit_x, 4.73*4e-6/nemit_y
    ###
    
    beta_x, beta_y = sigma_x**2/emit_x*1e-6, sigma_y**2/emit_y*1e-6
    gamma_x, gamma_y = (1.+alpha_x**2)/beta_x, (1.+alpha_y**2)/beta_y
    
    cor_px = -alpha_x/beta_x*sigma_x
    cor_py = -alpha_y/beta_y*sigma_y
    
    #OBJECTS[2], OBJECTS[3], OBJECTS[4], OBJECTS[5] = beta_x, beta_y, -alpha_x, -alpha_y
    
    WEIGHTS[0], WEIGHTS[1], WEIGHTS[2], WEIGHTS[3], WEIGHTS[4], WEIGHTS[5] = \
        0, 0, 5./55,   11./55,   5./55,                    11./55    
    OBJECTS[0], OBJECTS[1], OBJECTS[2], OBJECTS[3], OBJECTS[4], OBJECTS[5] = \
        0, 0, sigma_x, sigma_y, -(-alpha_x*nemit_x/bg_r), -(-alpha_y*nemit_y/bg_r)
    
    if len(args) > 0:
        execute = args[0]
    else:
        execute = 1
    #Distribution = '../ast.2700.001'
    #if 'Distribution' in kv.keys():
    #    Distribution = kv['Distribution']
    
    Run = 1
    if 'Run' in list(kv.keys()):
        Run = kv['Run']
    
    Ipart = 2000
    generator = Generator1(FNAME = 'beam_und.ini', IPart = Ipart, Species = 'electrons', Q_total = -2,\
                          Ref_Ekin = Ek, sig_Ekin = sig_Ekin, cor_Ekin = cor_Ekin, Dist_pz = 'g', Cathode = False,\
                          Dist_z = 'g', sig_z = 13.3e-12/np.sqrt(2*np.pi)*g_c*1e3,\
                          Dist_x = 'g', sig_x = sigma_x, Dist_px = 'g', Nemit_x = nemit_x*1e6, cor_px = cor_px,\
                          Dist_y = 'g', sig_y = sigma_y, Dist_py = 'g', Nemit_y = nemit_y*1e6, cor_py = cor_py)

    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = 'beam_und.ini',\
                    Auto_Phase = True, Track_All = True, Track_On_Axis = False, check_ref_part = False,\
                    Lprompt = False)
    newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, LSPCH3D = True, Nxf = 32, Nyf = 32, Nzf = 32,\
                    Max_scale = 0.05, Max_count = 20)
    charge.set(LSPCH = False, LSPCH3D = False)
    
    field_maps = '..'
    cavity = Module('Cavity', LEfield = True, By_stat = [True], File_Efield = [field_maps+os.sep+'3Dund40mm_6m'],\
                    MaxE = [By_u_max], C_pos = [pos_u], Nue = [0], Phi = [0])
    
    output = Module('Output', Zstart = 0, Zstop = 6, Zemit = 300, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [6])
    
    astra = Astra()
    astra.add_modules([newrun, charge, cavity, output])
    
    ###
    direc = str.format('n%.0fk-sig_x-%.2fmm-sig_y-%.2fmm-alp_x-%.2f-alp_y-%.2f' %\
                       (Ipart/1000., sigma_x, sigma_y, alpha_x, alpha_y))
    #direc = str.format('nemit_x-%.2fum-nemit_y-%.2fum' %\
    #                   (x[0], x[1]))
    ###
    direc = 'temp'
    os.system('mkdir -p '+direc)
    # os.chdir(direc)
    
    job_name = '%s' % direc
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    
    #astra.qsub(job_name, ast_name, gen_name, direc) 
    #return

    os.chdir(direc)
    os.system('generator gen.in > gen.log')
    os.system('astra ast.in > ast.log')
    
    fname = 'ast.Xemit.001'
    data = np.loadtxt(fname)
    obj = data[-1,3]
    os.chdir('..')
    
    return obj

def obj_undScan_3THz(x = None, *args, **kwargs):
    '''
    Optimize the beam size and beam covariance, given the emittance.
    Parameter:
      x: an array or list of the variables to be optimized, sigma_x, sigma_y, alpha_x and alpha_y
    Return:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    kv = kwargs
    
    N_u = 150; lam_u = 0.04; L_u = N_u*lam_u
    By_u_max = 1.005783 # T
    pos_u = L_u/2.
    
    P0 = 20.0 # MeV/c
    Ek = momentum2kinetic(P0); 
    Ek = 20
    gamma_r = kinetic2gamma(Ek); bg_r = gamma2bg(gamma_r)
    nemit_x = nemit_y = 3e-6 # m*rad
    
    ###
    #nemit_x, nemit_y = x[0]*1e-6, x[1]*1e-6
    ###
    
    emit_x = nemit_x/bg_r; emit_y = nemit_y/bg_r
    cor_Ekin = -0.5e-2*Ek*1e3 # keV
    sig_Ekin = 0.05e-2*Ek*1e3 # keV
    
    ###
    if x is not None:
        # sigma_x in mm, alpha_x dimensionless
        sigma_x, sigma_y, alpha_x, alpha_y = x 
    #else:
    #    sigma_x, sigma_y, alpha_x, alpha_y = 1.576772E+00,   1.775836E-01,   7.183778E+00,   1.716220E+00
    #sigma_x, sigma_y, alpha_x, alpha_y = 1.47, 0.32, 8.08*4e-6/nemit_x, 4.73*4e-6/nemit_y
    ###
    
    beta_x, beta_y = sigma_x**2/emit_x*1e-6, sigma_y**2/emit_y*1e-6
    gamma_x, gamma_y = (1.+alpha_x**2)/beta_x, (1.+alpha_y**2)/beta_y
    
    cor_px = -alpha_x/beta_x*sigma_x
    cor_py = -alpha_y/beta_y*sigma_y
    
    #OBJECTS[2], OBJECTS[3], OBJECTS[4], OBJECTS[5] = beta_x, beta_y, -alpha_x, -alpha_y
    
    WEIGHTS[0], WEIGHTS[1], WEIGHTS[2], WEIGHTS[3], WEIGHTS[4], WEIGHTS[5] = \
        0, 0, 5./55,   11./55,   5./55,                    11./55    
    OBJECTS[0], OBJECTS[1], OBJECTS[2], OBJECTS[3], OBJECTS[4], OBJECTS[5] = \
        0, 0, sigma_x, sigma_y, -(-alpha_x*nemit_x/bg_r), -(-alpha_y*nemit_y/bg_r)
    
    if len(args) > 0:
        execute = args[0]
    else:
        execute = 1
    #Distribution = '../ast.2700.001'
    #if 'Distribution' in kv.keys():
    #    Distribution = kv['Distribution']
    
    Run = 1
    if 'Run' in list(kv.keys()):
        Run = kv['Run']
    
    Ipart = 1000
    generator = Generator1(FNAME = 'beam_und.ini', IPart = Ipart, Species = 'electrons', Q_total = -2,\
                          Ref_Ekin = Ek, sig_Ekin = sig_Ekin, cor_Ekin = cor_Ekin, Dist_pz = 'g', Cathode = False,\
                          Dist_z = 'g', sig_z = 13.3e-12/np.sqrt(2*np.pi)*g_c*1e3,\
                          Dist_x = 'g', sig_x = sigma_x, Dist_px = 'g', Nemit_x = nemit_x*1e6, cor_px = cor_px,\
                          Dist_y = 'g', sig_y = sigma_y, Dist_py = 'g', Nemit_y = nemit_y*1e6, cor_py = cor_py)

    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = 'beam_und.ini',\
                    Auto_Phase = True, Track_All = True, Track_On_Axis = False, check_ref_part = False,\
                    Lprompt = False)
    newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, LSPCH3D = True, Nxf = 32, Nyf = 32, Nzf = 32,\
                    Max_scale = 0.05, Max_count = 20)
    charge.set(LSPCH = False, LSPCH3D = False)
    
    field_maps = '..'
    cavity = Module('Cavity', LEfield = True, By_stat = [True], File_Efield = [field_maps+os.sep+'3Dund40mm_6m'],\
                    MaxE = [By_u_max], C_pos = [pos_u], Nue = [0], Phi = [0])
    
    output = Module('Output', Zstart = 0, Zstop = 6, Zemit = 300, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [6])
    
    astra = Astra()
    astra.add_modules([newrun, charge, cavity, output])
    
    ###
    direc = str.format('n%.0fk-sig_x-%.2fmm-sig_y-%.2fmm-alp_x-%.2f-alp_y-%.2f' %\
                       (Ipart/1000., sigma_x, sigma_y, alpha_x, alpha_y))
    #direc = str.format('nemit_x-%.2fum-nemit_y-%.2fum' %\
    #                   (x[0], x[1]))
    ###
    direc = 'temp'
    os.system('mkdir -p '+direc)
    # os.chdir(direc)
    
    job_name = '%s' % direc
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    #astra.qsub(job_name, ast_name, gen_name, direc)
    
    #return
    os.chdir(direc)
    os.system('generator gen.in > gen.log')
    os.system('astra ast.in > ast.log')
    
    fname = 'ast.Xemit.001'
    data = np.loadtxt(fname)
    obj = data[-1,3]
    os.chdir('..')
    
    return obj

def obj_undScan_60um100(x = None, *args, **kwargs):
    '''
    Optimize the beam size and beam covariance, given the emittance.
    Parameter:
      x: an array or list of the variables to be optimized, sigma_x, sigma_y, alpha_x and alpha_y
    Return:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    kv = kwargs
    
    N_u = 120; lam_u = 0.03; L_u = N_u*lam_u
    By_u_max = 1.28 # T
    pos_u = L_u/2.
    
    P0 = 17.05 # MeV/c
    P0 = 22 # MeV/c
    Ek = momentum2kinetic(P0); gamma_r = kinetic2gamma(Ek); bg_r = gamma2bg(gamma_r)
    nemit_x = nemit_y = 4e-6 # m*rad
    nemit_x = x[4]*1e-6
    nemit_y = x[5]*1e-6
    
    ###
    #nemit_x, nemit_y = x[0]*1e-6, x[1]*1e-6
    ###
    
    emit_x = nemit_x/bg_r; emit_y = nemit_y/bg_r
    cor_Ekin = -0.5e-2*Ek*1e3 # keV
    sig_Ekin = 0.05e-2*Ek*1e3 # keV
    
    ###
    if x is not None:
        # sigma_x in mm, alpha_x dimensionless
        #sigma_x, sigma_y, alpha_x, alpha_y = x[0:4]
        sigma_x, sigma_y, xxp, yyp = x[0:4]
    #else:
    #    sigma_x, sigma_y, alpha_x, alpha_y = 1.576772E+00,   1.775836E-01,   7.183778E+00,   1.716220E+00
    #sigma_x, sigma_y, alpha_x, alpha_y = 1.47, 0.32, 8.08*4e-6/nemit_x, 4.73*4e-6/nemit_y
    ###
    alpha_x, alpha_y = xxp*1e-6/emit_x, yyp*1e-6/emit_y
    
    beta_x, beta_y = sigma_x**2/emit_x*1e-6, sigma_y**2/emit_y*1e-6
    gamma_x, gamma_y = (1.+alpha_x**2)/beta_x, (1.+alpha_y**2)/beta_y
    
    cor_px = -alpha_x/beta_x*sigma_x
    cor_py = -alpha_y/beta_y*sigma_y
    
    #OBJECTS[2], OBJECTS[3], OBJECTS[4], OBJECTS[5] = beta_x, beta_y, -alpha_x, -alpha_y
    
    WEIGHTS[0], WEIGHTS[1], WEIGHTS[2], WEIGHTS[3], WEIGHTS[4], WEIGHTS[5] = \
        0, 0, 5./55,   11./55,   5./55,                    11./55    
    OBJECTS[0], OBJECTS[1], OBJECTS[2], OBJECTS[3], OBJECTS[4], OBJECTS[5] = \
        0, 0, sigma_x, sigma_y, -(-alpha_x*nemit_x/bg_r), -(-alpha_y*nemit_y/bg_r)
    
    if len(args) > 0:
        execute = args[0]
    else:
        execute = 1
    
    Distribution = 'beam_und.ini'
    if 'Distribution' in kv.keys():
        Distribution = kv['Distribution']
        
    Run = 1
    if 'Run' in list(kv.keys()):
        Run = kv['Run']
    
    Ipart = 250000
    generator = Generator1(FNAME = Distribution, IPart = Ipart, Species = 'electrons', Q_total = -4,\
                          Ref_Ekin = Ek, sig_Ekin = sig_Ekin, cor_Ekin = cor_Ekin, Dist_pz = 'g', Cathode = False,\
                          Dist_z = 'p', Lz = 20e-12*g_c*1e3, rz = 2e-12*g_c*1e3,\
                          Dist_x = 'g', sig_x = sigma_x, Dist_px = 'g', Nemit_x = nemit_x*1e6, cor_px = cor_px,\
                          Dist_y = 'g', sig_y = sigma_y, Dist_py = 'g', Nemit_y = nemit_y*1e6, cor_py = cor_py)
    generator.set(Dist_z = 'g', sig_z = 9e-12*g_c*1e3, Lz = 0, rz = 0)
    
    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = Distribution,
                    Auto_Phase = True, Track_All = True, Track_On_Axis = False, check_ref_part = False,\
                    Lprompt = False)
    newrun.set(Run = Run)
    
    charge = Module('Charge', LSPCH = True, LSPCH3D = True, Nxf = 32, Nyf = 32, Nzf = 32,\
                    Max_scale = 0.05, Max_count = 20)
    #charge.set(LSPCH = False, LSPCH3D = False)
    
    field_maps = '..'
    cavity = Module('Cavity', LEfield = True, By_stat = [True], File_Efield = [field_maps+os.sep+'3DDESY26-20x10mm-uniform'],\
                    MaxE = [By_u_max], C_pos = [pos_u], Nue = [0], Phi = [0])
    
    output = Module('Output', Zstart = 0, Zstop = 3.6, Zemit = 180, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [3.6])

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, output])

    ###
    direc = str.format('n%.0fk-sig_x-%.2fmm-sig_y-%.2fmm-alp_x-%.2f-alp_y-%.2f-nemit_x-%.2fum_nemit_y-%.2fum' %\
                       (Ipart/1000., sigma_x, sigma_y, alpha_x, alpha_y, nemit_x*1e6, nemit_y*1e6))
    #direc = '%.2fum_%.2fum' % (x[2], x[3])
    ###
    # direc = 'dist'
    os.system('mkdir -p '+direc)
    # os.chdir(direc)
    
    job_name = 'myjob_%s' % direc
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    
    astra.qsub(job_name, ast_name, gen_name, direc)
    return
    
    os.chdir(direc)
    os.system('generator gen.in > gen.log')
    #os.system('astra ast.in > ast.log')
    
    #fname = 'ast.Xemit.001'
    #data = np.loadtxt(fname)
    #obj = data[-1,3]
    os.chdir('..')
    
    return obj

    direc = str.format('n%.0fk-sig_x-%.2fmm-sig_y-%.2fmm-alp_x-%.2f-alp_y-%.2f' %\
                       (Ipart/1000., sigma_x, sigma_y, alpha_x, alpha_y))
    
    os.system('mkdir -p '+direc)
    os.chdir(direc)
    
    generator.write()
    astra.write()
    
    if not execute:
        os.system('generator gen.in 2>&1 | tee gen.log')
        os.chdir('../')
        obj = -1
    else:
        os.system('generator gen.in 2>&1 | tee gen.log')
        os.system('astra ast.in 2>&1 | tee ast.log')

        try:
            fname = 'ast.0360.001'
            beam = np.loadtxt(fname)
            beam[1:,2] += beam[0,2]; beam[1:,5] += beam[0,5]
            diag = BeamDiagnostics(beam)

            # when there is no beam loss to the cathode or to the aperture -alpha_x*nemit_x
            if diag.loss_cathode<=50 and diag.loss_aperture<=50: 
                obj = np.sqrt(WEIGHTS[0]*np.fabs(diag.nemit_x-OBJECTS[0])+WEIGHTS[1]*np.fabs(diag.nemit_y-OBJECTS[1])+
                              WEIGHTS[2]*np.fabs(diag.std_x  -OBJECTS[2])+WEIGHTS[3]*np.fabs(diag.std_y  -OBJECTS[3])+
                              WEIGHTS[4]*np.fabs(-diag.alpha_x*diag.emit_x*1e6-OBJECTS[4])+
                              WEIGHTS[5]*np.fabs(-diag.alpha_y*diag.emit_y*1e6-OBJECTS[5]))
            else:
                obj = 1e3
        except:
            obj = 1e4
            print('Error: not accelerated well!')

        os.chdir('../')

        res = [Ipart, sigma_x, sigma_y, alpha_x, alpha_y, obj]
        with open('results.dat','a') as f_handle:
            np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_undScan_60um99(x = None, *args, **kwargs):
    '''
    Optimize the beam size and beam covariance, given the emittance.
    Parameter:
      x: an array or list of the variables to be optimized, sigma_x, sigma_y, alpha_x and alpha_y
    Return:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    kv = kwargs
    
    N_u = 120; lam_u = 0.03; L_u = N_u*lam_u
    By_u_max = 1.28 # T
    pos_u = L_u/2.
    
    P0 = 17.05 # MeV/c
    P0 = 22 # MeV/c
    Ek = momentum2kinetic(P0); gamma_r = kinetic2gamma(Ek); bg_r = gamma2bg(gamma_r)
    #nemit_x = nemit_y = 4e-6 # m*rad
    nemit_x = x[4]*1e-6
    nemit_y = x[5]*1e-6
    
    ###
    #nemit_x, nemit_y = x[0]*1e-6, x[1]*1e-6
    ###
    
    emit_x = nemit_x/bg_r; emit_y = nemit_y/bg_r
    cor_Ekin = 0.5e-2*Ek*1e3 # keV
    sig_Ekin = 0.05e-2*Ek*1e3 # keV
    
    ###
    if x is not None:
        # sigma_x in mm, alpha_x dimensionless
        #sigma_x, sigma_y, alpha_x, alpha_y = x[0:4]
        sigma_x, sigma_y, xxp, yyp = x[0:4]
    #else:
    #    sigma_x, sigma_y, alpha_x, alpha_y = 1.576772E+00,   1.775836E-01,   7.183778E+00,   1.716220E+00
    #sigma_x, sigma_y, alpha_x, alpha_y = 1.47, 0.32, 8.08*4e-6/nemit_x, 4.73*4e-6/nemit_y
    ###
    alpha_x, alpha_y = xxp*1e-6/emit_x, yyp*1e-6/emit_y
    
    beta_x, beta_y = sigma_x**2/emit_x*1e-6, sigma_y**2/emit_y*1e-6
    gamma_x, gamma_y = (1.+alpha_x**2)/beta_x, (1.+alpha_y**2)/beta_y
    
    cor_px = -alpha_x/beta_x*sigma_x
    cor_py = -alpha_y/beta_y*sigma_y
    
    #OBJECTS[2], OBJECTS[3], OBJECTS[4], OBJECTS[5] = beta_x, beta_y, -alpha_x, -alpha_y
    
    WEIGHTS[0], WEIGHTS[1], WEIGHTS[2], WEIGHTS[3], WEIGHTS[4], WEIGHTS[5] = \
        0, 0, 5./55,   11./55,   5./55,                    11./55    
    OBJECTS[0], OBJECTS[1], OBJECTS[2], OBJECTS[3], OBJECTS[4], OBJECTS[5] = \
        0, 0, sigma_x, sigma_y, -(-alpha_x*nemit_x/bg_r), -(-alpha_y*nemit_y/bg_r)
    
    if len(args) > 0:
        execute = args[0]
    else:
        execute = 1
    
    Distribution = 'beam_und.ini'
    if 'Distribution' in kv.keys():
        Distribution = kv['Distribution']
        
    Run = 1
    if 'Run' in list(kv.keys()):
        Run = kv['Run']
    
    Ipart = 250000
    generator = Generator1(FNAME = Distribution, IPart = Ipart, Species = 'electrons', Q_total = -4,\
                          Ref_Ekin = Ek, sig_Ekin = sig_Ekin, cor_Ekin = cor_Ekin, Dist_pz = 'g', Cathode = False,\
                          Dist_z = 'p', Lz = 20e-12*g_c*1e3, rz = 2e-12*g_c*1e3,\
                          Dist_x = 'g', sig_x = sigma_x, Dist_px = 'g', Nemit_x = nemit_x*1e6, cor_px = cor_px,\
                          Dist_y = 'g', sig_y = sigma_y, Dist_py = 'g', Nemit_y = nemit_y*1e6, cor_py = cor_py)
    
    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = Distribution,
                    Auto_Phase = True, Track_All = True, Track_On_Axis = False, check_ref_part = False,\
                    Lprompt = False)
    newrun.set(Run = Run)
    
    charge = Module('Charge', LSPCH = True, LSPCH3D = True, Nxf = 32, Nyf = 32, Nzf = 32,\
                    Max_scale = 0.05, Max_count = 20)
    #charge.set(LSPCH = False, LSPCH3D = False)
    
    field_maps = '..'
    cavity = Module('Cavity', LEfield = True, By_stat = [True], File_Efield = [field_maps+os.sep+'3DDESY26-20x10mm-uniform'],\
                    MaxE = [By_u_max], C_pos = [pos_u], Nue = [0], Phi = [0])
    
    output = Module('Output', Zstart = 0, Zstop = 3.6, Zemit = 180, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [3.6])

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, output])

    ###
    direc = str.format('n%.0fk-sig_x-%.2fmm-sig_y-%.2fmm-alp_x-%.2f-alp_y-%.2f-nemit_x-%.2fum_nemit_y-%.2fum' %\
                       (Ipart/1000., sigma_x, sigma_y, alpha_x, alpha_y, nemit_x*1e6, nemit_y*1e6))
    #direc = '%.2fum_%.2fum' % (x[2], x[3])
    ###
    # direc = 'dist'
    os.system('mkdir -p '+direc)
    # os.chdir(direc)
    
    job_name = 'myjob_%s' % direc
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    
    astra.qsub(job_name, ast_name, gen_name, direc)
    return
    
    os.chdir(direc)
    os.system('generator gen.in > gen.log')
    #os.system('astra ast.in > ast.log')
    
    #fname = 'ast.Xemit.001'
    #data = np.loadtxt(fname)
    #obj = data[-1,3]
    os.chdir('..')
    
    return obj

    direc = str.format('n%.0fk-sig_x-%.2fmm-sig_y-%.2fmm-alp_x-%.2f-alp_y-%.2f' %\
                       (Ipart/1000., sigma_x, sigma_y, alpha_x, alpha_y))
    
    os.system('mkdir -p '+direc)
    os.chdir(direc)
    
    generator.write()
    astra.write()
    
    if not execute:
        os.system('generator gen.in 2>&1 | tee gen.log')
        os.chdir('../')
        obj = -1
    else:
        os.system('generator gen.in 2>&1 | tee gen.log')
        os.system('astra ast.in 2>&1 | tee ast.log')

        try:
            fname = 'ast.0360.001'
            beam = np.loadtxt(fname)
            beam[1:,2] += beam[0,2]; beam[1:,5] += beam[0,5]
            diag = BeamDiagnostics(beam)

            # when there is no beam loss to the cathode or to the aperture -alpha_x*nemit_x
            if diag.loss_cathode<=50 and diag.loss_aperture<=50: 
                obj = np.sqrt(WEIGHTS[0]*np.fabs(diag.nemit_x-OBJECTS[0])+WEIGHTS[1]*np.fabs(diag.nemit_y-OBJECTS[1])+
                              WEIGHTS[2]*np.fabs(diag.std_x  -OBJECTS[2])+WEIGHTS[3]*np.fabs(diag.std_y  -OBJECTS[3])+
                              WEIGHTS[4]*np.fabs(-diag.alpha_x*diag.emit_x*1e6-OBJECTS[4])+
                              WEIGHTS[5]*np.fabs(-diag.alpha_y*diag.emit_y*1e6-OBJECTS[5]))
            else:
                obj = 1e3
        except:
            obj = 1e4
            print('Error: not accelerated well!')

        os.chdir('../')

        res = [Ipart, sigma_x, sigma_y, alpha_x, alpha_y, obj]
        with open('results.dat','a') as f_handle:
            np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_undScan_60um(x = None, *args, **kwargs):
    '''
    Optimize the beam size and beam covariance, given the emittance.
    Parameter:
      x: an array or list of the variables to be optimized, sigma_x, sigma_y, alpha_x and alpha_y
    Return:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    kv = kwargs
    
    N_u = 120; lam_u = 0.03; L_u = N_u*lam_u
    By_u_max = 1.28 # T
    pos_u = L_u/2.
    
    P0 = 22.0 # MeV/c
    Ek = momentum2kinetic(P0); gamma_r = kinetic2gamma(Ek); bg_r = gamma2bg(gamma_r)
    nemit_x = nemit_y = 4e-6 # m*rad
    
    ###
    #nemit_x, nemit_y = x[0]*1e-6, x[1]*1e-6
    ###
    
    emit_x = nemit_x/bg_r; emit_y = nemit_y/bg_r
    cor_Ekin = -0.5e-2*Ek*1e3 # keV
    sig_Ekin = 0.05e-2*Ek*1e3 # keV
    
    ###
    if x is not None:
        # sigma_x in mm, alpha_x dimensionless
        sigma_x, sigma_y, alpha_x, alpha_y = x 
    #else:
    #    sigma_x, sigma_y, alpha_x, alpha_y = 1.576772E+00,   1.775836E-01,   7.183778E+00,   1.716220E+00
    #sigma_x, sigma_y, alpha_x, alpha_y = 1.47, 0.32, 8.08*4e-6/nemit_x, 4.73*4e-6/nemit_y
    ###
    
    beta_x, beta_y = sigma_x**2/emit_x*1e-6, sigma_y**2/emit_y*1e-6
    gamma_x, gamma_y = (1.+alpha_x**2)/beta_x, (1.+alpha_y**2)/beta_y
    
    cor_px = -alpha_x/beta_x*sigma_x
    cor_py = -alpha_y/beta_y*sigma_y
    
    #OBJECTS[2], OBJECTS[3], OBJECTS[4], OBJECTS[5] = beta_x, beta_y, -alpha_x, -alpha_y
    
    WEIGHTS[0], WEIGHTS[1], WEIGHTS[2], WEIGHTS[3], WEIGHTS[4], WEIGHTS[5] = \
        0, 0, 5./55,   11./55,   5./55,                    11./55    
    OBJECTS[0], OBJECTS[1], OBJECTS[2], OBJECTS[3], OBJECTS[4], OBJECTS[5] = \
        0, 0, sigma_x, sigma_y, -(-alpha_x*nemit_x/bg_r), -(-alpha_y*nemit_y/bg_r)
    
    if len(args) > 0:
        execute = args[0]
    else:
        execute = 1
    #Distribution = '../ast.2700.001'
    #if 'Distribution' in kv.keys():
    #    Distribution = kv['Distribution']
        
    Run = 5
    if 'Run' in list(kv.keys()):
        Run = kv['Run']
    
    Ipart = 200000
    generator = Generator1(FNAME = 'beam_und.ini', IPart = Ipart, Species = 'electrons', Q_total = -4,\
                          Ref_Ekin = Ek, sig_Ekin = sig_Ekin, cor_Ekin = cor_Ekin, Dist_pz = 'g', Cathode = False,\
                          Dist_z = 'p', Lz = 20e-12*g_c*1e3, rz = 2e-12*g_c*1e3,\
                          Dist_x = 'g', sig_x = sigma_x, Dist_px = 'g', Nemit_x = nemit_x*1e6, cor_px = cor_px,\
                          Dist_y = 'g', sig_y = sigma_y, Dist_py = 'g', Nemit_y = nemit_y*1e6, cor_py = cor_py)

    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = 'beam_und.ini',\
                    Auto_Phase = True, Track_All = True, Track_On_Axis = False, check_ref_part = False,\
                    Lprompt = False)
    newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, LSPCH3D = True, Nxf = 32, Nyf = 32, Nzf = 32,\
                    Max_scale = 0.05, Max_count = 20)
    #charge.set(LSPCH = False, LSPCH3D = False)
    
    field_maps = '..'
    cavity = Module('Cavity', LEfield = True, By_stat = [True], File_Efield = [field_maps+os.sep+'3Dund-15x5mm'],\
                    MaxE = [By_u_max], C_pos = [pos_u], Nue = [0], Phi = [0])
    
    output = Module('Output', Zstart = 0, Zstop = 3.6, Zemit = 180, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [3.6])

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, output])

    ###
    direc = str.format('n%.0fk-sig_x-%.2fmm-sig_y-%.2fmm-alp_x-%.2f-alp_y-%.2f' %\
                       (Ipart/1000., sigma_x, sigma_y, alpha_x, alpha_y))
    #direc = str.format('nemit_x-%.2fum-nemit_y-%.2fum' %\
    #                   (x[0], x[1]))
    ###
    #direc = 'temp'
    os.system('mkdir -p '+direc)
    # os.chdir(direc)
    
    job_name = '%s' % direc
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    
    astra.qsub(job_name, ast_name, gen_name, direc)
    return
    
    os.chdir(direc)
    os.system('generator gen.in > gen.log')
    os.system('astra ast.in > ast.log')
    
    fname = 'ast.Xemit.001'
    data = np.loadtxt(fname)
    obj = data[-1,3]
    os.chdir('..')
    
    return obj

    direc = str.format('n%.0fk-sig_x-%.2fmm-sig_y-%.2fmm-alp_x-%.2f-alp_y-%.2f' %\
                       (Ipart/1000., sigma_x, sigma_y, alpha_x, alpha_y))
    
    os.system('mkdir -p '+direc)
    os.chdir(direc)
    
    generator.write()
    astra.write()
    
    if not execute:
        os.system('generator gen.in 2>&1 | tee gen.log')
        os.chdir('../')
        obj = -1
    else:
        os.system('generator gen.in 2>&1 | tee gen.log')
        os.system('astra ast.in 2>&1 | tee ast.log')

        try:
            fname = 'ast.0360.001'
            beam = np.loadtxt(fname)
            beam[1:,2] += beam[0,2]; beam[1:,5] += beam[0,5]
            diag = BeamDiagnostics(beam)

            # when there is no beam loss to the cathode or to the aperture -alpha_x*nemit_x
            if diag.loss_cathode<=50 and diag.loss_aperture<=50: 
                obj = np.sqrt(WEIGHTS[0]*np.fabs(diag.nemit_x-OBJECTS[0])+WEIGHTS[1]*np.fabs(diag.nemit_y-OBJECTS[1])+
                              WEIGHTS[2]*np.fabs(diag.std_x  -OBJECTS[2])+WEIGHTS[3]*np.fabs(diag.std_y  -OBJECTS[3])+
                              WEIGHTS[4]*np.fabs(-diag.alpha_x*diag.emit_x*1e6-OBJECTS[4])+
                              WEIGHTS[5]*np.fabs(-diag.alpha_y*diag.emit_y*1e6-OBJECTS[5]))
            else:
                obj = 1e3
        except:
            obj = 1e4
            print('Error: not accelerated well!')

        os.chdir('../')

        res = [Ipart, sigma_x, sigma_y, alpha_x, alpha_y, obj]
        with open('results.dat','a') as f_handle:
            np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_undScan(x = None, *args, **kwargs):
    '''
    Optimize the beam size and beam covariance, given the emittance.
    Parameter:
      x: an array or list of the variables to be optimized, sigma_x, sigma_y, alpha_x and alpha_y
    Return:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    kv = kwargs
    
    N_u = 120; lam_u = 0.03; L_u = N_u*lam_u
    By_u_max = 1.28 # T
    pos_u = L_u/2.
    
    P0 = 17.05 # MeV/c
    Ek = momentum2kinetic(P0); gamma_r = kinetic2gamma(Ek); bg_r = gamma2bg(gamma_r)
    nemit_x = nemit_y = 4e-6 # m*rad
    
    ###
    #nemit_x, nemit_y = x[0]*1e-6, x[1]*1e-6
    ###
    
    emit_x = nemit_x/bg_r; emit_y = nemit_y/bg_r
    sigma_Ek = 0.5e-2*Ek*1e3 # keV
    
    ###
    if x is not None:
        # sigma_x in mm, alpha_x dimensionless
        sigma_x, sigma_y, alpha_x, alpha_y = x 
    #else:
    #    sigma_x, sigma_y, alpha_x, alpha_y = 1.576772E+00,   1.775836E-01,   7.183778E+00,   1.716220E+00
    #sigma_x, sigma_y, alpha_x, alpha_y = 1.47, 0.32, 8.08*4e-6/nemit_x, 4.73*4e-6/nemit_y
    ###
    
    beta_x, beta_y = sigma_x**2/emit_x*1e-6, sigma_y**2/emit_y*1e-6
    gamma_x, gamma_y = (1.+alpha_x**2)/beta_x, (1.+alpha_y**2)/beta_y
    
    cor_px = -alpha_x/beta_x*sigma_x
    cor_py = -alpha_y/beta_y*sigma_y
    
    #OBJECTS[2], OBJECTS[3], OBJECTS[4], OBJECTS[5] = beta_x, beta_y, -alpha_x, -alpha_y
    
    WEIGHTS[0], WEIGHTS[1], WEIGHTS[2], WEIGHTS[3], WEIGHTS[4], WEIGHTS[5] = \
        0, 0, 5./55,   11./55,   5./55,                    11./55    
    OBJECTS[0], OBJECTS[1], OBJECTS[2], OBJECTS[3], OBJECTS[4], OBJECTS[5] = \
        0, 0, sigma_x, sigma_y, -(-alpha_x*nemit_x/bg_r), -(-alpha_y*nemit_y/bg_r)
    
    if len(args) > 0:
        execute = args[0]
    else:
        execute = 1
    #Distribution = '../ast.2700.001'
    #if 'Distribution' in kv.keys():
    #    Distribution = kv['Distribution']
        
    Run = 1
    if 'Run' in list(kv.keys()):
        Run = kv['Run']
    
    Ipart = 200000
    generator = Generator(FNAME = 'beam_und.ini', IPart = Ipart, Species = 'electrons', Q_total = -2.5,\
                          Ref_Ekin = Ek, sig_Ekin = 0, cor_Ekin = sigma_Ek, Dist_pz = 'g', Cathode = False,\
                          Dist_z = 'p', Lz = 21.5e-12*g_c*1e3, rz = 2e-12*g_c*1e3,\
                          Dist_x = 'g', sig_x = sigma_x, Dist_px = 'g', Nemit_x = nemit_x*1e6, cor_px = cor_px,\
                          Dist_y = 'g', sig_y = sigma_y, Dist_py = 'g', Nemit_y = nemit_y*1e6, cor_py = cor_py)

    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = 'beam_und.ini',\
                    Auto_Phase = True, Track_All = True, Track_On_Axis = False, check_ref_part = False,\
                    Lprompt = False)
    newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, LSPCH3D = True, Nxf = 32, Nyf = 32, Nzf = 32,\
                    Max_scale = 0.05, Max_count = 20)
    
    cavity = Module('Cavity', LEfield = True, By_stat = [True], File_Efield = [field_maps+os.sep+'3Dund-15x5mm'],\
                    MaxE = [By_u_max], C_pos = [pos_u], Nue = [0], Phi = [0])
    
    output = Module('Output', Zstart = 0, Zstop = 3.6, Zemit = 180, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [1.8, 3.6])

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, output])

    ###
    direc = str.format('n%.0fk-sig_x-%.2fmm-sig_y-%.2fmm-alp_x-%.2f-alp_y-%.2f' %\
                       (Ipart/1000., sigma_x, sigma_y, alpha_x, alpha_y))
    #direc = str.format('nemit_x-%.2fum-nemit_y-%.2fum' %\
    #                   (x[0], x[1]))
    ###
    os.system('mkdir -p '+direc)
    # os.chdir(direc)

    job_name = '%s' % direc
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, gen_name, direc)
    
    # os.chdir('../')
    
    # os.system('generator gen.in > gen.log')
    # os.system('astra ast.in > ast.log')
    return

    direc = str.format('n%.0fk-sig_x-%.2fmm-sig_y-%.2fmm-alp_x-%.2f-alp_y-%.2f' %\
                       (Ipart/1000., sigma_x, sigma_y, alpha_x, alpha_y))
    
    os.system('mkdir -p '+direc)
    os.chdir(direc)

    generator.write()
    astra.write()
    
    if not execute:
        os.system('generator gen.in 2>&1 | tee gen.log')
        os.chdir('../')
        obj = -1
    else:
        os.system('generator gen.in 2>&1 | tee gen.log')
        os.system('astra ast.in 2>&1 | tee ast.log')

        try:
            fname = 'ast.0360.001'
            beam = np.loadtxt(fname)
            beam[1:,2] += beam[0,2]; beam[1:,5] += beam[0,5]
            diag = BeamDiagnostics(beam)

            # when there is no beam loss to the cathode or to the aperture -alpha_x*nemit_x
            if diag.loss_cathode<=50 and diag.loss_aperture<=50: 
                obj = np.sqrt(WEIGHTS[0]*np.fabs(diag.nemit_x-OBJECTS[0])+WEIGHTS[1]*np.fabs(diag.nemit_y-OBJECTS[1])+
                              WEIGHTS[2]*np.fabs(diag.std_x  -OBJECTS[2])+WEIGHTS[3]*np.fabs(diag.std_y  -OBJECTS[3])+
                              WEIGHTS[4]*np.fabs(-diag.alpha_x*diag.emit_x*1e6-OBJECTS[4])+
                              WEIGHTS[5]*np.fabs(-diag.alpha_y*diag.emit_y*1e6-OBJECTS[5]))
            else:
                obj = 1e3
        except:
            obj = 1e4
            print('Error: not accelerated well!')

        os.chdir('../')

        res = [Ipart, sigma_x, sigma_y, alpha_x, alpha_y, obj]
        with open('results.dat','a') as f_handle:
            np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_THzScan(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameter:
      x: an array or list of the variables to be optimized
    Return:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    #sigma_x = sigma_y = x[0]/4. # uniform
    #C_sigma_x = C_sigma_y = 0
    sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    C_sigma_x = C_sigma_y = x[0]/2./sigma_x
    
    phi_gun, phi_booster = 0, -20
    Imain = x[1]
    
    MaxE_gun = 58.75
    MaxE_booster = 12.72 #get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = 3.0
    
    Run = 1
    if len(args)>0:
        Run = args[0]
        
    Ipart = int(Q_total*50e3)
    generator = Generator(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'g', sig_clock = 6e-3/2.35482, Cathode = True,\
                          Dist_x = '2', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = '2', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)

    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = 'beam.ini',\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    #newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 30, Nlong_in = 50, N_min = 10, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt', field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    screen = [0.5, 0.8030, 1.3790, 1.7080, 5.2770, 6.2500, 7.1250, 8.4100, \
              8.9200, 12.2780, 13.0380, 13.7980, 14.5580, 15.3180, 16.3030, 18.2620]
    output = Module('Output', Zstart = 0, Zstop = 15, Zemit = 300, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [5.277, 6.2500, 10.9854, 12.2780, 18.2620])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, output])

    direc = str.format('Q%.1fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Q_total*1e3, x[0], MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    os.system('mkdir -p '+direc)
    # os.chdir(direc)

    job_name = 'myjob.'+('Q%.1fpC-phi2-%.0fdeg' % (Q_total*1e3, x[1]))
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, gen_name, direc)
    
    # os.chdir('../')
    
    # os.system('generator gen.in > gen.log')
    # os.system('astra ast.in > ast.log')
    return
    
    try:
        fname = 'ast.0528.001'
        beam = np.loadtxt(fname)
        beam[1:,2] += beam[0,2]; beam[1:,5] += beam[0,5]
        tmp = astra_post(beam)

        if tmp[20]<=50 and tmp[21]<=50: # when there is no beam loss to the cathode or to the aperture
            nemit_x, nemit_y, beta_x, beta_y, alpha_x, alpha_y = tmp[1], tmp[2], tmp[13], tmp[17], tmp[12], tmp[16]
            obj = np.sqrt(WEIGHTS[0]*(nemit_x-OBJECTS[0])**2+WEIGHTS[1]*(nemit_y-OBJECTS[1])**2+ 
                          WEIGHTS[2]*(beta_x -OBJECTS[2])**2+WEIGHTS[3]*(beta_y -OBJECTS[3])**2+
                          WEIGHTS[4]*(alpha_x-OBJECTS[4])**2+WEIGHTS[5]*(alpha_y-OBJECTS[5])**2)
        else:
            obj = 1e2
    except:
        obj = 1e3
        print('Error: not accelerated well!')

    os.chdir('..'+os.sep); #os.system('rm -r '+direc)

    res = [Ipart, sigma_x*4, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_solenoidScan(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameter:
      x: an array or list of the variables to be optimized
    Return:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    sigma_x = sigma_y = 1/4. # uniform
    C_sigma_x = C_sigma_y = 0
    #sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    #C_sigma_x = C_sigma_y = x[0]/2./sigma_x
    
    phi_gun, phi_booster = 0, 0
    Imain = x[0]
    
    MaxE_gun = 55.91
    MaxE_booster = x[1] # get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = 0.250
    
    Run = 1
    if len(args)>0:
        Run = args[0]
        
    Ipart = 200000
    generator = Generator(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'g', sig_clock = 6e-3/2.35482, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)

    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = 'beam.ini',\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=300000)
    #newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 50, Nlong_in = 50, N_min = 10, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt', field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    screen = [0.5, 0.8030, 1.3790, 1.7080, 5.2770, 6.2500, 7.1250, 8.4100, \
              8.9200, 12.2780, 13.0380, 13.7980, 14.5580, 15.3180, 16.3030, 18.2620]
    output = Module('Output', Zstart = 0, Zstop = 13, Zemit = 650, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [4.50, 5.277, 8.4100, 12.2780])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, apertu, output])

    direc = str.format('Q%.2fnC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA_' %\
                       (Q_total, sigma_x*4, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    os.system('mkdir -p '+direc)
    # os.chdir(direc)

    job_name = 'myjob.'+('B%.2fMV_m-I%.0fA' % (MaxE_booster, Imain))
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, gen_name, direc)
    
    # os.chdir('../')
    
    # os.system('generator gen.in > gen.log')
    # os.system('astra ast.in > ast.log')
    return
    
    try:
        fname = 'ast.0528.001'
        beam = np.loadtxt(fname)
        beam[1:,2] += beam[0,2]; beam[1:,5] += beam[0,5]
        tmp = astra_post(beam)

        if tmp[20]<=50 and tmp[21]<=50: # when there is no beam loss to the cathode or to the aperture
            nemit_x, nemit_y, beta_x, beta_y, alpha_x, alpha_y = tmp[1], tmp[2], tmp[13], tmp[17], tmp[12], tmp[16]
            obj = np.sqrt(WEIGHTS[0]*(nemit_x-OBJECTS[0])**2+WEIGHTS[1]*(nemit_y-OBJECTS[1])**2+ 
                          WEIGHTS[2]*(beta_x -OBJECTS[2])**2+WEIGHTS[3]*(beta_y -OBJECTS[3])**2+
                          WEIGHTS[4]*(alpha_x-OBJECTS[4])**2+WEIGHTS[5]*(alpha_y-OBJECTS[5])**2)
        else:
            obj = 1e2
    except:
        obj = 1e3
        print('Error: not accelerated well!')

    os.chdir('..'+os.sep); #os.system('rm -r '+direc)

    res = [Ipart, sigma_x*4, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_THzOpt4(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    sigma_x = sigma_y = 1.8/4. # uniform
    C_sigma_x = C_sigma_y = 0
    #sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    #C_sigma_x = C_sigma_y = x[0]/2./sigma_x
    
    phi_gun, phi_booster = -5, -80
    Imain = 310
    
    MaxE_gun = 51.64
    MaxE_booster = 18.06 # get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = 500e-3
    FWHM = 0.4 # ps
    
    Run = 1
    if len(args)>0:
        Run = args[0]

    Ipart = 50000 #int(Q_total*100e3)
    
    generator = Generator(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'g', sig_clock = FWHM*1e-3/2.35482, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)

    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = '../ast.0600.001',\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    #newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 30, Nlong_in = 50, N_min = 10,\
                    NXF = 16, NYF = 16, NZF = 16, L2D_3D = True, Z_TRANS = 5.0, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt',\
                                                             field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = 14, Zemit = 700, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [0.50, 5.277, 13.038])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    quadru = Module('Quadrupole', LQuad = True, Q_type = ['../Q3.data', '../Q3.data'],\
                    Q_pos = [position['HIGH1.Q9'], position['HIGH1.Q10'], position['PST.QM1']], Q_grad = x)
                                   
    astra = Astra()
    astra.add_modules([newrun, charge, quadru, apertu, output])

    direc = str.format('Q1-%.2fT_m-Q2-%.2fT_m-Q3-%.2fT_m' %\
                       tuple(x))
    
    os.system('mkdir -p '+direc)
    os.chdir(direc)

    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(gen_name+'.in')
    astra.write(ast_name+'.in')
    
    #os.chdir(direc)
    #os.system('generator gen.in > gen.log')
    os.system('astra ast.in > ast.log')
    
    try:
        fname = 'ast.1304.001'
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag = BeamDiagnostics(dist)   
        
        if 1: 
            # when there is little beam loss to the cathode or to the aperture
            obj = np.sqrt(diag.std_x**2+diag.std_y**2+diag.std_z**2)
        else:
            # if there is beam loss, then set the objective to 1000
            obj = 1e3   
        
    except:
        # if no output beam file, then set the objective to 10000
        obj = 1e4
        print('Error: not accelerated well!')

    os.chdir('..'); #os.system('rm -r '+direc)

    res = list(x)+[obj]+list(diag.x)
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_THzOpt3(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    sigma_x = sigma_y = 1.8/4. # uniform
    C_sigma_x = C_sigma_y = 0
    #sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    #C_sigma_x = C_sigma_y = x[0]/2./sigma_x
    
    phi_gun, phi_booster = -5, -80
    Imain = 310
    
    MaxE_gun = 51.64
    MaxE_booster = 18.06 # get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = 500e-3
    FWHM = 0.4 # ps
    
    grad1, grad2 = x
    
    Run = 1
    if len(args)>0:
        Run = args[0]

    Ipart = 50000 #int(Q_total*100e3)
    
    generator = Generator(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'g', sig_clock = FWHM*1e-3/2.35482, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)

    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = '../ast.0600.001',\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    #newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 30, Nlong_in = 50, N_min = 10,\
                    NXF = 16, NYF = 16, NZF = 16, L2D_3D = True, Z_TRANS = 5.0, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt',\
                                                             field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = 14, Zemit = 700, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [0.50, 5.277, 13.038])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    quadru = Module('Quadrupole', LQuad = True, Q_type = ['../Q3.data', '../Q3.data'],\
                    Q_pos = [position['HIGH1.Q9'], position['HIGH1.Q10']], Q_grad = [grad1, grad2])
                                   
    astra = Astra()
    astra.add_modules([newrun, charge, quadru, apertu, output])

    direc = str.format('Q1-%.2fT_m-Q2-%.2fT_m' %\
                       (grad1, grad2))
    
    os.system('mkdir -p '+direc)
    os.chdir(direc)

    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(gen_name+'.in')
    astra.write(ast_name+'.in')
    
    #os.chdir(direc)
    #os.system('generator gen.in > gen.log')
    os.system('astra ast.in > ast.log')
    
    try:
        fname = 'ast.1304.001'
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag = BeamDiagnostics(dist)   
        
        if 1: 
            # when there is little beam loss to the cathode or to the aperture
            obj = np.sqrt(diag.std_x**2+diag.std_y**2+diag.std_z**2)
        else:
            # if there is beam loss, then set the objective to 1000
            obj = 1e3   
        
    except:
        # if no output beam file, then set the objective to 10000
        obj = 1e4
        print('Error: not accelerated well!')

    os.chdir('..'); #os.system('rm -r '+direc)

    res = list(x)+[obj]+list(diag.x)
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_THzScan5(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameter:
      x: an array or list of the variables to be optimized
    Return:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    sigma_x = sigma_y = 1.8/4. # uniform
    C_sigma_x = C_sigma_y = 0
    #sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    #C_sigma_x = C_sigma_y = 1.8/2./sigma_x
    
    phi_gun, phi_booster = x[0], x[1]
    Imain = 340
    
    MaxE_gun = 51.64
    MaxE_booster = 18.06 # get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = 0.500
    
    Run = 1
    if len(args)>0:
        Run = args[0]
        
    Ipart = int(Q_total*200e3)
    Ipart = 50e3
    generator = Generator(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'g', sig_clock = 0.4e-3/2.35482, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)

    newrun = Module('Newrun', Run = 2, Head = 'PITZ beam line simulation', Distribution = 'beam.ini',\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    #newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 30, Nlong_in = 50, N_min = 10,\
                    NXF = 16, NYF = 16, NZF = 16, L2D_3D = True, Z_TRANS = 5.0, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt', field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    screen = [0.5, 0.8030, 1.3790, 1.7080, 5.2770, 6.2500, 7.1250, 8.4100, \
              8.9200, 12.2780, 13.0380, 13.7980, 14.5580, 15.3180, 16.3030, 18.2620]
    output = Module('Output', Zstart = 0, Zstop = 14, Zemit = 700, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [5.277, 6.5, 7.5, 9.5, 13.038, 18.2620])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, apertu, output])

    direc = str.format('Q%.1fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Q_total*1e3, sigma_x*4, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    os.system('mkdir -p '+direc)
    # os.chdir(direc)

    job_name = 'myjob.'+('phi1-%.2fdeg-phi2-%.2fdeg' % (x[0], x[1]))
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, gen_name, direc)
    
    # os.chdir('../')
    
    # os.system('generator gen.in > gen.log')
    # os.system('astra ast.in > ast.log')
    return
    
    try:
        fname = 'ast.0528.001'
        beam = np.loadtxt(fname)
        beam[1:,2] += beam[0,2]; beam[1:,5] += beam[0,5]
        tmp = astra_post(beam)

        if tmp[20]<=50 and tmp[21]<=50: # when there is no beam loss to the cathode or to the aperture
            nemit_x, nemit_y, beta_x, beta_y, alpha_x, alpha_y = tmp[1], tmp[2], tmp[13], tmp[17], tmp[12], tmp[16]
            obj = np.sqrt(WEIGHTS[0]*(nemit_x-OBJECTS[0])**2+WEIGHTS[1]*(nemit_y-OBJECTS[1])**2+ 
                          WEIGHTS[2]*(beta_x -OBJECTS[2])**2+WEIGHTS[3]*(beta_y -OBJECTS[3])**2+
                          WEIGHTS[4]*(alpha_x-OBJECTS[4])**2+WEIGHTS[5]*(alpha_y-OBJECTS[5])**2)
        else:
            obj = 1e2
    except:
        obj = 1e3
        print('Error: not accelerated well!')

    os.chdir('..'+os.sep); #os.system('rm -r '+direc)

    res = [Ipart, sigma_x*4, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_THzScan4(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameter:
      x: an array or list of the variables to be optimized
    Return:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    sigma_x = sigma_y = 1.8/4. # uniform
    C_sigma_x = C_sigma_y = 0
    #sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    #C_sigma_x = C_sigma_y = 1.8/2./sigma_x
    
    phi_gun, phi_booster = -5, x[0]
    Imain = x[1]
    
    MaxE_gun = 51.64
    MaxE_booster = 18.06 # get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = 0.500
    
    Run = 1
    if len(args)>0:
        Run = args[0]
        
    Ipart = int(Q_total*200e3)
    Ipart = 50e3
    generator = Generator(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'g', sig_clock = 0.4e-3/2.35482, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)

    newrun = Module('Newrun', Run = 2, Head = 'PITZ beam line simulation', Distribution = 'beam.ini',\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    #newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 30, Nlong_in = 50, N_min = 10, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt', field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    screen = [0.5, 0.8030, 1.3790, 1.7080, 5.2770, 6.2500, 7.1250, 8.4100, \
              8.9200, 12.2780, 13.0380, 13.7980, 14.5580, 15.3180, 16.3030, 18.2620]
    output = Module('Output', Zstart = 0, Zstop = 14, Zemit = 700, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [5.277, 13.038])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, apertu, output])

    direc = str.format('Q%.1fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Q_total*1e3, 1.8, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    os.system('mkdir -p '+direc)
    # os.chdir(direc)

    job_name = 'myjob.'+('phi2-%.2fdegree-I-%.2fA' % (x[0], x[1]))
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, gen_name, direc)
    
    # os.chdir('../')
    
    # os.system('generator gen.in > gen.log')
    # os.system('astra ast.in > ast.log')
    return
    
    try:
        fname = 'ast.0528.001'
        beam = np.loadtxt(fname)
        beam[1:,2] += beam[0,2]; beam[1:,5] += beam[0,5]
        tmp = astra_post(beam)

        if tmp[20]<=50 and tmp[21]<=50: # when there is no beam loss to the cathode or to the aperture
            nemit_x, nemit_y, beta_x, beta_y, alpha_x, alpha_y = tmp[1], tmp[2], tmp[13], tmp[17], tmp[12], tmp[16]
            obj = np.sqrt(WEIGHTS[0]*(nemit_x-OBJECTS[0])**2+WEIGHTS[1]*(nemit_y-OBJECTS[1])**2+ 
                          WEIGHTS[2]*(beta_x -OBJECTS[2])**2+WEIGHTS[3]*(beta_y -OBJECTS[3])**2+
                          WEIGHTS[4]*(alpha_x-OBJECTS[4])**2+WEIGHTS[5]*(alpha_y-OBJECTS[5])**2)
        else:
            obj = 1e2
    except:
        obj = 1e3
        print('Error: not accelerated well!')

    os.chdir('..'+os.sep); #os.system('rm -r '+direc)

    res = [Ipart, sigma_x*4, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_THzScan2(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameter:
      x: an array or list of the variables to be optimized
    Return:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    #sigma_x = sigma_y = x[0]/4. # uniform
    #C_sigma_x = C_sigma_y = 0
    sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    C_sigma_x = C_sigma_y = 1.8/2./sigma_x
    
    phi_gun, phi_booster = 0, 0
    Imain = 330
    
    MaxE_gun = 51.64
    MaxE_booster = 18.06 # get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = 0.5
    
    grad9 = x[0]
    grad10 = x[1]
    
    Run = 1
    if len(args)>0:
        Run = args[0]
        
    Ipart = int(Q_total*200e3)
    generator = Generator(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'g', sig_clock = 0.25e-3/2.35482, Cathode = True,\
                          Dist_x = '2', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = '2', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)

    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = '../ast.0600.001',\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    #newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 30, Nlong_in = 50, N_min = 10,\
                    NXF = 16, NYF = 16, NZF = 16, L2D_3D = True, Z_TRANS = 5.0, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt', field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = 14, Zemit = 700, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [0.25, 0.50, 5.277, 12.2780, 13.0380, 13.7980])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
      
    quadru = Module('Quadrupole', LQuad = True, Q_type = ['../Q3.data', '../Q3.data'],\
                    Q_pos = [position['HIGH1.Q9'], position['HIGH1.Q10']], Q_grad = [grad9, grad10])
                                   
    astra = Astra()
    astra.add_modules([newrun, charge, quadru, apertu, output])

    direc = str.format('Q1-%.2fT_m-Q2-%.2fT_m' %\
                       (grad9, grad10))
    os.system('mkdir -p '+direc)
    # os.chdir(direc)

    job_name = 'myjob.'+('Q1-%.2fT_m-Q2-%.2fT_m' % (grad9, grad10))
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, gen_name, direc)
    
    # os.chdir('../')
    
    # os.system('generator gen.in > gen.log')
    # os.system('astra ast.in > ast.log')
    return
    
    try:
        fname = 'ast.0528.001'
        beam = np.loadtxt(fname)
        beam[1:,2] += beam[0,2]; beam[1:,5] += beam[0,5]
        tmp = astra_post(beam)

        if tmp[20]<=50 and tmp[21]<=50: # when there is no beam loss to the cathode or to the aperture
            nemit_x, nemit_y, beta_x, beta_y, alpha_x, alpha_y = tmp[1], tmp[2], tmp[13], tmp[17], tmp[12], tmp[16]
            obj = np.sqrt(WEIGHTS[0]*(nemit_x-OBJECTS[0])**2+WEIGHTS[1]*(nemit_y-OBJECTS[1])**2+ 
                          WEIGHTS[2]*(beta_x -OBJECTS[2])**2+WEIGHTS[3]*(beta_y -OBJECTS[3])**2+
                          WEIGHTS[4]*(alpha_x-OBJECTS[4])**2+WEIGHTS[5]*(alpha_y-OBJECTS[5])**2)
        else:
            obj = 1e2
    except:
        obj = 1e3
        print('Error: not accelerated well!')

    os.chdir('..'+os.sep); #os.system('rm -r '+direc)

    res = [Ipart, sigma_x*4, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_THzOpt2(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    sigma_x = sigma_y = x[0]/4. # uniform
    C_sigma_x = C_sigma_y = 0
    #sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    #C_sigma_x = C_sigma_y = x[0]/2./sigma_x
    
    phi_gun, phi_booster = x[1], x[2]
    Imain = x[3]
    
    MaxE_gun = 51.64
    MaxE_booster = 18.06 # get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = 1000e-3
    FWHM = x[4] # ps
    
    Run = 1
    if len(args)>0:
        Run = args[0]

    Ipart = 50000 #int(Q_total*100e3)
    
    generator = Generator(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'g', sig_clock = FWHM*1e-3/2.35482, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)

    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = 'beam.ini',\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    #newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 30, Nlong_in = 50, N_min = 10, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt',\
                                                             field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = 14, Zemit = 700, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [0.50, 5.277, 13.038])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, apertu, output])
    
    direc = str.format('Q%.1fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA-L-%.2fps' %\
                       (Q_total*1e3, x[0], MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, FWHM))
    os.system('mkdir -p '+direc)
    os.chdir(direc)

    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(gen_name+'.in')
    astra.write(ast_name+'.in')
    
    #os.chdir(direc)
    os.system('generator gen.in > gen.log')
    os.system('astra ast.in > ast.log')
    
    try:
        fname = 'ast.0528.001'
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag = BeamDiagnostics(dist)
        
        fname = 'ast.Xemit.001'
        xemit = np.loadtxt(fname)
        select = (xemit[:,0]>5.0)
        xemit = xemit[select]
        nemit_x = np.mean(xemit[:,5]) # 
        
        fname = 'ast.Zemit.001'
        zemit = np.loadtxt(fname)       
        select = (zemit[:,0]>5.0)
        zemit = zemit[select]
        
        f = interp1d(zemit[:,0], zemit[:,3])
        rms_z = f(13.038) # mm
        
        if diag.loss_cathode<=50 and diag.loss_aperture<=50: 
            # when there is little beam loss to the cathode or to the aperture
            obj = np.sqrt(nemit_x**2+rms_z**2/0.1**2)
        else:
            # if there is beam loss, then set the objective to 1000
            obj = 1e3   
        
    except:
        # if no output beam file, then set the objective to 10000
        nemit_x = -1
        rms_z = -1
        obj = 1e4
        print('Error: not accelerated well!')

    os.chdir('..'); #os.system('rm -r '+direc)

    res = [Ipart, x[0], MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, FWHM, nemit_x, rms_z, obj]
    with open('results1.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_THzOpt1(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    sigma_x = sigma_y = x[0]/4. # uniform
    C_sigma_x = C_sigma_y = 0
    #sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    #C_sigma_x = C_sigma_y = x[0]/2./sigma_x
    
    phi_gun, phi_booster = x[1], x[2]
    Imain = x[3]
    
    MaxE_gun = 51.64
    MaxE_booster = 18.06 # get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = 500e-3
    FWHM = x[4] # ps
    
    Run = 1
    if len(args)>0:
        Run = args[0]

    Ipart = 50000 #int(Q_total*100e3)
    
    generator = Generator(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'g', sig_clock = FWHM*1e-3/2.35482, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)

    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = 'beam.ini',\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    #newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 30, Nlong_in = 50, N_min = 10, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt',\
                                                             field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = 14, Zemit = 700, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [0.50, 5.277, 13.038])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, apertu, output])
    
    direc = str.format('Q%.1fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA-L-%.2fps' %\
                       (Q_total*1e3, x[0], MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, FWHM))
    os.system('mkdir -p '+direc)
    os.chdir(direc)

    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(gen_name+'.in')
    astra.write(ast_name+'.in')
    
    #os.chdir(direc)
    os.system('generator gen.in > gen.log')
    os.system('astra ast.in > ast.log')
    
    try:
        fname = 'ast.0528.001'
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag = BeamDiagnostics(dist)
        
        fname = 'ast.Xemit.001'
        xemit = np.loadtxt(fname)
        select = (xemit[:,0]>5.0)
        xemit = xemit[select]
        nemit_x = np.mean(xemit[:,5]) # 
        
        fname = 'ast.Zemit.001'
        zemit = np.loadtxt(fname)       
        select = (zemit[:,0]>5.0)
        zemit = zemit[select]
        
        f = interp1d(zemit[:,0], zemit[:,3])
        rms_z = f(13.038) # mm
        
        if diag.loss_cathode<=50 and diag.loss_aperture<=50: 
            # when there is little beam loss to the cathode or to the aperture
            obj = np.sqrt(nemit_x**2+rms_z**2/0.1**2)
        else:
            # if there is beam loss, then set the objective to 1000
            obj = 1e3   
        
    except:
        # if no output beam file, then set the objective to 10000
        nemit_x = -1
        rms_z = -1
        obj = 1e4
        print('Error: not accelerated well!')

    os.chdir('..'); #os.system('rm -r '+direc)

    res = [Ipart, x[0], MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, FWHM, nemit_x, rms_z, obj]
    with open('results1.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_THzScan1(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameter:
      x: an array or list of the variables to be optimized
    Return:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    #sigma_x = sigma_y = x[0]/4. # uniform
    #C_sigma_x = C_sigma_y = 0
    sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    C_sigma_x = C_sigma_y = x[0]/2./sigma_x
    
    phi_gun, phi_booster = 0, 0
    Imain = 330
    
    MaxE_gun = 51.64
    MaxE_booster = 18.06 # get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = x[1]
    
    Run = 1
    if len(args)>0:
        Run = args[0]
        
    Ipart = int(Q_total*200e3)
    generator = Generator(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'g', sig_clock = 0.25e-3/2.35482, Cathode = True,\
                          Dist_x = '2', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = '2', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)

    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = 'beam.ini',\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    #newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 30, Nlong_in = 50, N_min = 10, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt', field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    screen = [0.5, 0.8030, 1.3790, 1.7080, 5.2770, 6.2500, 7.1250, 8.4100, \
              8.9200, 12.2780, 13.0380, 13.7980, 14.5580, 15.3180, 16.3030, 18.2620]
    output = Module('Output', Zstart = 0, Zstop = 0.5, Zemit = 500, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [0.25, 0.50, 5.277, 10.9854, 12.2780, 18.2620])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, apertu, output])

    direc = str.format('Q%.1fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Q_total*1e3, x[0], MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    os.system('mkdir -p '+direc)
    # os.chdir(direc)

    job_name = 'myjob.'+('Q%.1fpC-D-%.2fmm' % (Q_total*1e3, x[0]))
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, gen_name, direc)
    
    # os.chdir('../')
    
    # os.system('generator gen.in > gen.log')
    # os.system('astra ast.in > ast.log')
    return
    
    try:
        fname = 'ast.0528.001'
        beam = np.loadtxt(fname)
        beam[1:,2] += beam[0,2]; beam[1:,5] += beam[0,5]
        tmp = astra_post(beam)

        if tmp[20]<=50 and tmp[21]<=50: # when there is no beam loss to the cathode or to the aperture
            nemit_x, nemit_y, beta_x, beta_y, alpha_x, alpha_y = tmp[1], tmp[2], tmp[13], tmp[17], tmp[12], tmp[16]
            obj = np.sqrt(WEIGHTS[0]*(nemit_x-OBJECTS[0])**2+WEIGHTS[1]*(nemit_y-OBJECTS[1])**2+ 
                          WEIGHTS[2]*(beta_x -OBJECTS[2])**2+WEIGHTS[3]*(beta_y -OBJECTS[3])**2+
                          WEIGHTS[4]*(alpha_x-OBJECTS[4])**2+WEIGHTS[5]*(alpha_y-OBJECTS[5])**2)
        else:
            obj = 1e2
    except:
        obj = 1e3
        print('Error: not accelerated well!')

    os.chdir('..'+os.sep); #os.system('rm -r '+direc)

    res = [Ipart, sigma_x*4, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_gunPhaseScan(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    WEIGHTS = [1, 1, 0., 0., 0., 0.]                # weights for emittance, beta and gamma and
    OBJECTS = [0, 0, 0,  0,  0,  0]    # objectives for emittance, beta and gamma, in x and y directions, respectively

    Ipart = 1000
    sigma_x = sigma_y = 1.2/4.
    phi_gun, phi_booster = x[0], 0
    Imain = 0
    
    MaxE_gun = 60
    MaxE_booster = 0
    MaxB = I2B(Imain)

    direc = str.format('n%.0fk-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Ipart/1000., sigma_x*4, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    
    os.system('mkdir -p '+direc)    

    field_maps = rootdir+os.sep+'sync'+os.sep+'field-maps'
    # cwd = os.getcwd()
    # field_maps = '../'+os.path.relpath(field_maps, cwd)+'/'

    gen_astra(direc+os.sep+'gen.in', direc+os.sep+'ast.in', field_maps = field_maps,\
              Ipart = Ipart, sig_x = sigma_x, sig_y = sigma_y, LSPCH = True,\
              MaxE = [MaxE_gun, MaxE_booster], Phi = [phi_gun, phi_booster],\
              MaxB = [MaxB], Lquad = False, zstop = 0.5, zemit = 200,\
              Screen = [0.5], Track_all = False)
    
    os.chdir(direc)
    os.system('generator gen.in > gen.log')
    os.system('astra ast.in > ast.log') 
    
    try:
        obj = get_ref_momentum()   
        
    except:
        # if no output beam file, then set the objective to 10000
        obj = 1e4
        print('Error: not accelerated well!')

    os.chdir('..'); #os.system('rm -r '+direc)

    res = [Ipart, sigma_x*4, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_BSAscan2(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameter:
      x: an array or list of the variables to be optimized
    Return:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    #sigma_x = sigma_y = x[0]/4. # uniform
    #C_sigma_x = C_sigma_y = 0
    sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    C_sigma_x = C_sigma_y = x[0]/2./sigma_x
    
    phi_gun, phi_booster = 0, 0
    Imain = 340
    
    MaxE_gun = 60
    MaxE_booster = get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = x[1]
    
    Run = 1
    if len(args)>0:
        Run = args[0]
        
    Ipart = 50000
    generator = Generator(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'p', Lt = 21.5e-3, rt = 2e-3, Cathode = True,\
                          Dist_x = '2', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = '2', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)

    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = 'beam.ini',\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    #newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 50, Nlong_in = 50, N_min = 10, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt', field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    screen = [0.5, 0.8030, 1.3790, 1.7080, 5.2770, 6.2500, 7.1250, 8.4100, \
              8.9200, 12.2780, 13.0380, 13.7980, 14.5580, 15.3180, 16.3030, 18.2620]
    output = Module('Output', Zstart = 0, Zstop = 20, Zemit = 600, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [0.5, 5.277, 10.9854, 12.2780])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, apertu, output])

    direc = str.format('Q%.1fnC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Q_total, x[0], MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    os.system('mkdir -p '+direc)
    # os.chdir(direc)

    job_name = 'myjob.'+('Q%.1fnC-D%.1fmm' % (Q_total, x[0]))
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, gen_name, direc)
    
    # os.chdir('../')
    
    # os.system('generator gen.in > gen.log')
    # os.system('astra ast.in > ast.log')
    return
    
    try:
        fname = 'ast.0528.001'
        beam = np.loadtxt(fname)
        beam[1:,2] += beam[0,2]; beam[1:,5] += beam[0,5]
        tmp = astra_post(beam)

        if tmp[20]<=50 and tmp[21]<=50: # when there is no beam loss to the cathode or to the aperture
            nemit_x, nemit_y, beta_x, beta_y, alpha_x, alpha_y = tmp[1], tmp[2], tmp[13], tmp[17], tmp[12], tmp[16]
            obj = np.sqrt(WEIGHTS[0]*(nemit_x-OBJECTS[0])**2+WEIGHTS[1]*(nemit_y-OBJECTS[1])**2+ 
                          WEIGHTS[2]*(beta_x -OBJECTS[2])**2+WEIGHTS[3]*(beta_y -OBJECTS[3])**2+
                          WEIGHTS[4]*(alpha_x-OBJECTS[4])**2+WEIGHTS[5]*(alpha_y-OBJECTS[5])**2)
        else:
            obj = 1e2
    except:
        obj = 1e3
        print('Error: not accelerated well!')

    os.chdir('..'+os.sep); #os.system('rm -r '+direc)

    res = [Ipart, sigma_x*4, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_BSAscan(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameter:
      x: an array or list of the variables to be optimized
    Return:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    #sigma_x = sigma_y = x[0]/4. # uniform
    #C_sigma_x = C_sigma_y = 0
    sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    C_sigma_x = C_sigma_y = x[0]/2./sigma_x
    
    phi_gun, phi_booster = 0, 0
    Imain = x[1]
    
    MaxE_gun = 51.64
    MaxE_booster = 18.06 # get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = 15
    
    Run = 1
    if len(args)>0:
        Run = args[0]
        
    Ipart = 1000
    generator = Generator(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'g', sig_clock = 9e-3/2.35482, Cathode = True,\
                          Dist_x = '2', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = '2', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)

    newrun = Module('Newrun', Run = 6, Head = 'PITZ beam line simulation', Distribution = '../beam5.ini',\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=300000)
    #newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 30, Nlong_in = 50, N_min = 10, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt', field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    screen = [0.5, 0.8030, 1.3790, 1.7080, 5.2770, 6.2500, 7.1250, 8.4100, \
              8.9200, 12.2780, 13.0380, 13.7980, 14.5580, 15.3180, 16.3030, 18.2620]
    output = Module('Output', Zstart = 0, Zstop = 20, Zemit = 500, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [0.5, 5.277, 10.9854, 12.2780, 18.2620])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, apertu, output])

    direc = str.format('Q%.1fnC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Q_total, x[0], MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    os.system('mkdir -p '+direc)
    # os.chdir(direc)

    job_name = 'myjob.'+('Q%.1fnC-I%.0fA' % (Q_total, x[1]))
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, gen_name, direc)
    
    # os.chdir('../')
    
    # os.system('generator gen.in > gen.log')
    # os.system('astra ast.in > ast.log')
    return
    
    try:
        fname = 'ast.0528.001'
        beam = np.loadtxt(fname)
        beam[1:,2] += beam[0,2]; beam[1:,5] += beam[0,5]
        tmp = astra_post(beam)

        if tmp[20]<=50 and tmp[21]<=50: # when there is no beam loss to the cathode or to the aperture
            nemit_x, nemit_y, beta_x, beta_y, alpha_x, alpha_y = tmp[1], tmp[2], tmp[13], tmp[17], tmp[12], tmp[16]
            obj = np.sqrt(WEIGHTS[0]*(nemit_x-OBJECTS[0])**2+WEIGHTS[1]*(nemit_y-OBJECTS[1])**2+ 
                          WEIGHTS[2]*(beta_x -OBJECTS[2])**2+WEIGHTS[3]*(beta_y -OBJECTS[3])**2+
                          WEIGHTS[4]*(alpha_x-OBJECTS[4])**2+WEIGHTS[5]*(alpha_y-OBJECTS[5])**2)
        else:
            obj = 1e2
    except:
        obj = 1e3
        print('Error: not accelerated well!')

    os.chdir('..'+os.sep); #os.system('rm -r '+direc)

    res = [Ipart, sigma_x*4, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_boosterGradScan(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    WEIGHTS = [1, 1, 0., 0., 0., 0.]                # weights for emittance, beta and gamma and
    OBJECTS = [0, 0, 0,  0,  0,  0]    # objectives for emittance, beta and gamma, in x and y directions, respectively

    Ipart = 1000
    sigma_x = sigma_y = 1./4.
    phi_gun, phi_booster = x[1], 0
    Imain = 0
    
    MaxE_gun = 55.91
    MaxE_booster = x[0]
    MaxB = I2B(Imain)

    direc = str.format('n%.0fk-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Ipart/1000., sigma_x*4, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    
    os.system('mkdir -p '+direc)    

    field_maps = rootdir+os.sep+'sync'+os.sep+'field-maps'
    # cwd = os.getcwd()
    # field_maps = '../'+os.path.relpath(field_maps, cwd)+'/'

    gen_astra(direc+os.sep+'gen.in', direc+os.sep+'ast.in', field_maps = field_maps,\
              Ipart = Ipart, sig_x = sigma_x, sig_y = sigma_y, LSPCH = True,\
              MaxE = [MaxE_gun, MaxE_booster], Phi = [phi_gun, phi_booster],\
              MaxB = [MaxB], Lquad = False, zstop = 5, zemit = 200,\
              Screen = [0.5], Track_all = False)
    
    os.chdir(direc)
    os.system('generator gen.in > gen.log')
    os.system('astra ast.in > ast.log') 
    
    try:
        obj = get_ref_momentum()   
        
    except:
        # if no output beam file, then set the objective to 10000
        obj = 1e4
        print('Error: not accelerated well!')

    os.chdir('..'); #os.system('rm -r '+direc)

    res = [Ipart, sigma_x*4, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_gunGradScan(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    WEIGHTS = [1, 1, 0., 0., 0., 0.]                # weights for emittance, beta and gamma and
    OBJECTS = [0, 0, 0,  0,  0,  0]    # objectives for emittance, beta and gamma, in x and y directions, respectively

    Ipart = 1000
    sigma_x = sigma_y = 1.2/4.
    phi_gun, phi_booster = x[1], 0
    Imain = 0
    
    MaxE_gun = x[0]
    MaxE_booster = 0
    MaxB = I2B(Imain)

    direc = str.format('n%.0fk-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Ipart/1000., sigma_x*4, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    
    os.system('mkdir -p '+direc)    

    field_maps = rootdir+os.sep+'sync'+os.sep+'field-maps'
    # cwd = os.getcwd()
    # field_maps = '../'+os.path.relpath(field_maps, cwd)+'/'

    gen_astra(direc+os.sep+'gen.in', direc+os.sep+'ast.in', field_maps = field_maps,\
              Ipart = Ipart, sig_x = sigma_x, sig_y = sigma_y, LSPCH = True,\
              MaxE = [MaxE_gun, MaxE_booster], Phi = [phi_gun, phi_booster],\
              MaxB = [MaxB], Lquad = False, zstop = 0.5, zemit = 200,\
              Screen = [0.5], Track_all = False)
    
    os.chdir(direc)
    os.system('generator gen.in > gen.log')
    os.system('astra ast.in > ast.log') 
    
    try:
        obj = get_ref_momentum()   
        
    except:
        # if no output beam file, then set the objective to 10000
        obj = 1e4
        print('Error: not accelerated well!')

    os.chdir('..'); #os.system('rm -r '+direc)

    res = [Ipart, sigma_x*4, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_EMSY500pC(x, *args):
    '''
    Created on July 8, 2019
    Simulation from photocathode via EMSY1 at 5.277 m to 20 m
    Goal function is a combination of average emittance and correlated energy spread exterpolated at undulator center, 29.25 m
    Laser spot fixed at 4 mm, gun phase at MMMG
    Variables to be optimized: gun and booster phases, solenoid current
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    WEIGHTS = [0.25, 0.25, 0., 0., 0., 0.]   # weights for emittance, beta and gamma and
    OBJECTS = [0,       0, 0,  0,  0,  0]    # objectives for emittance, beta and gamma, in x and y directions, respectively

    Ipart = 200000
    BSA = x[0]
    #sigma_x = sigma_y = BSA/4.
    
    sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    C_sigma_x = C_sigma_y = BSA/2./sigma_x
    
    phi_gun, phi_booster = x[1], x[2]
    Imain = x[3]
    
    MaxE_gun = 60
    MaxE_booster = get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = -0.5
    
    field_maps = rootdir+os.sep+'sync'+os.sep+'field-maps'
    
    generator = Generator(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'p', Lt = 21.5e-3, rt = 2e-3, Cathode = True,\
                          Dist_x = '2', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = '2', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)
    
    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = 'beam.ini', CathodeS = True,\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000, Qbunch = Q_total)
    #newrun.set(Run = Run)
    
    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 40, Nlong_in = 50, N_min = 30, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt', field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = 20, Zemit = 400, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, C_EmitS = True, LPROJECT_EMIT = True,\
                    LOCAL_EMIT = True, Screen = [5.2770, 6.25, 7.125, 8.41, 12.2780, 18.2620])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, output])
    
    direc = str.format('Q%.1fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Q_total*1e3, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    
    ###
    os.system('mkdir -p '+direc)
    os.chdir(direc)
    """
    job_name = 'myjob.'+('phi2-%.2fdeg-I-%.2fA' % (x[2], x[3]))
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, gen_name, direc)
    
    return
    ###
    
    os.system('mkdir -p '+direc)
    os.chdir(direc)
    
    generator.write('gen.in')
    astra.write('ast.in')
    
    os.system('generator gen.in 2>&1 | tee gen.log')
    os.system('astra ast.in 2>&1 | tee ast.log')
    
    return
    """
    try:
        fname = 'ast.0528.001'
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag = BeamDiagnostics(dist = dist)
        
        fname = 'ast.Xemit.001'
        xemit = np.loadtxt(fname)
        select = (xemit[:,0]>5.0)
        xemit = xemit[select]
        nemit_x = np.mean(xemit[:,5])
        
        fname = 'ast.Zemit.001'
        zemit = np.loadtxt(fname)       
        select = (zemit[:,0]>5.0)
        zemit = zemit[select]
        
        f = lambda x, a, b: a+b*x
        popt, pcov = curve_fit(f, zemit[:,0], zemit[:,6])
        cov = f(29.25, *popt)
        
        if diag.loss_cathode<=500 and diag.loss_aperture<=500: 
            # when there is little beam loss to the cathode or to the aperture
            if cov>0:
                obj = nemit_x/4+np.fabs(cov/10)
            else:
                obj = nemit_x/4+np.fabs(cov/50)
        else:
            # if there is beam loss, then set the objective to 1000
            obj = 1e3   
        
    except:
    #    # if no output beam file, then set the objective to 10000
        obj = 1e4
        os.chdir('..')
        return
    #    print('Error: not accelerated well!')

    os.chdir('..'); #os.system('rm -r '+direc)

    res = [Ipart, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, nemit_x, cov, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')
    
    return obj
    
    
def obj_EMSY1000pC(x, *args):
    '''
    Created on July 8, 2019
    Simulation from photocathode via EMSY1 at 5.277 m to 20 m
    Goal function is a combination of average emittance and correlated energy spread exterpolated at undulator center, 29.25 m
    Laser spot fixed at 4 mm, gun phase at MMMG
    Variables to be optimized: gun and booster phases, solenoid current
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    WEIGHTS = [0.25, 0.25, 0., 0., 0., 0.]   # weights for emittance, beta and gamma and
    OBJECTS = [0,       0, 0,  0,  0,  0]    # objectives for emittance, beta and gamma, in x and y directions, respectively

    Ipart = 200000
    BSA = x[0]
    #sigma_x = sigma_y = BSA/4.
    
    sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    C_sigma_x = C_sigma_y = BSA/2./sigma_x
    
    phi_gun, phi_booster = x[1], x[2]
    Imain = x[3]
    
    MaxE_gun = 60
    MaxE_booster = get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = -1.0
    
    field_maps = rootdir+os.sep+'sync'+os.sep+'field-maps'
    
    generator = Generator(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'p', Lt = 21.5e-3, rt = 2e-3, Cathode = True,\
                          Dist_x = '2', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = '2', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)
    
    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = 'beam.ini', CathodeS = True,\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000, Qbunch = Q_total)
    #newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 40, Nlong_in = 50, N_min = 30, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt', field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = 20, Zemit = 400, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, C_EmitS = True, LPROJECT_EMIT = True,\
                    LOCAL_EMIT = True, Screen = [5.2770, 6.25, 7.125, 8.41, 12.2780, 18.2620])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, output])

    direc = str.format('Q%.1fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Q_total*1e3, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    
    ###
    os.system('mkdir -p '+direc)
    os.chdir(direc)
    """
    job_name = 'myjob.'+('phi2-%.2fdeg-I-%.2fA' % (x[2], x[3]))
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, gen_name, direc)
    
    return
    ###
    
    os.system('mkdir -p '+direc)
    os.chdir(direc)
    
    generator.write('gen.in')
    astra.write('ast.in')
    
    os.system('generator gen.in 2>&1 | tee gen.log')
    os.system('astra ast.in 2>&1 | tee ast.log')
    """
    try:
        fname = 'ast.0528.001'
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag = BeamDiagnostics(dist = dist)
        
        fname = 'ast.Xemit.001'
        xemit = np.loadtxt(fname)
        select = (xemit[:,0]>5.0)
        xemit = xemit[select]
        nemit_x = np.mean(xemit[:,5])
        
        fname = 'ast.Zemit.001'
        zemit = np.loadtxt(fname)       
        select = (zemit[:,0]>5.0)
        zemit = zemit[select]
        
        f = lambda x, a, b: a+b*x
        popt, pcov = curve_fit(f, zemit[:,0], zemit[:,6])
        cov = f(29.25, *popt)
        
        if diag.loss_cathode<=500 and diag.loss_aperture<=500: 
            # when there is little beam loss to the cathode or to the aperture
            if cov>0:
                obj = nemit_x/4+np.fabs(cov/10)
            else:
                obj = nemit_x/4+np.fabs(cov/50)
        else:
            # if there is beam loss, then set the objective to 1000
            obj = 1e3   
        
    except:
    #    # if no output beam file, then set the objective to 10000
        obj = 1e4
        os.chdir('..')
        return
    #    print('Error: not accelerated well!')

    os.chdir('..'); #os.system('rm -r '+direc)

    res = [Ipart, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, nemit_x, cov, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj
    
def obj_simple(x, *args, **kwargs):
    '''
    Created on July 8, 2019
    Simulation from photocathode via EMSY1 at 5.277 m to 20 m
    Goal function is a combination of average emittance and correlated energy spread exterpolated at undulator center, 29.25 m
    Laser spot fixed at 4 mm, gun phase at MMMG
    Variables to be optimized: gun and booster phases, solenoid current
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    WEIGHTS = [0.25, 0.25, 0., 0., 0., 0.]   # weights for emittance, beta and gamma and
    OBJECTS = [0,       0, 0,  0,  0,  0]    # objectives for emittance, beta and gamma, in x and y directions, respectively

    Ipart = 200000
    Q_total = -1.0
    Run = 1
    
    if len(kwargs)>0:
        if 'Ipart' in list(kwargs.keys()):
            Ipart   = kwargs['Ipart']
        if 'Q_total' in list(kwargs.keys()):
            Q_total = kwargs['Q_total']
        if 'Run' in list(kwargs.keys()):
            Run     = kwargs['Run']
    
    BSA = x[0]
    #sigma_x = sigma_y = BSA/4.
    
    sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    C_sigma_x = C_sigma_y = BSA/2./sigma_x
    
    phi_gun, phi_booster = x[1], x[2]
    Imain = x[3]
    
    MaxE_gun = 60
    MaxE_booster = get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)
       
    field_maps = rootdir+os.sep+'sync'+os.sep+'field-maps'
    
    generator = Generator(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'p', Lt = 21.5e-3, rt = 2e-3, Cathode = True,\
                          Dist_x = '2', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = '2', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)
    
    newrun = Module('Newrun', Run = Run, Head = 'PITZ beam line simulation', Distribution = 'beam.ini', CathodeS = True,\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000, Qbunch = Q_total)

    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 40, Nlong_in = 50, N_min = 30, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt', field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = 20, Zemit = 400, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, C_EmitS = True, LPROJECT_EMIT = True,\
                    LOCAL_EMIT = True, Screen = [5.2770, 6.25, 7.125, 8.41, 12.2780, 18.2620])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    
    
    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, output])

    direc = str.format('Q%.1fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Q_total*1e3, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    
    ###
    os.system('mkdir -p '+direc)
    os.chdir(direc)
    """
    job_name = 'myjob.'+('phi2-%.2fdeg-I-%.2fA' % (x[2], x[3]))
    gen_name = 'gen' #+`Run`
    ast_name = 'ast' #+`Run`
    
    generator.write(direc+os.sep+gen_name+'.in')
    astra.write(direc+os.sep+ast_name+'.in')
    astra.qsub(job_name, ast_name, gen_name, direc)
    
    return
    ###
    
    os.system('mkdir -p '+direc)
    os.chdir(direc)
    
    generator.write('gen.in')
    astra.write('ast.in')
    
    os.system('generator gen.in 2>&1 | tee gen.log')
    os.system('astra ast.in 2>&1 | tee ast.log')
    
    #return
    """
    try:
        fname = 'ast.0528.001'
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag = BeamDiagnostics(dist = dist)
        
        fname = 'ast.Xemit.001'
        xemit = np.loadtxt(fname)
        select = (xemit[:,0]>5.0)
        xemit = xemit[select]
        nemit_x = np.mean(xemit[:,5])
        
        fname = 'ast.Zemit.001'
        zemit = np.loadtxt(fname)       
        select = (zemit[:,0]>5.0)
        zemit = zemit[select]
        
        f = lambda x, a, b: a+b*x
        popt, pcov = curve_fit(f, zemit[:,0], zemit[:,6])
        cov = f(29.25, *popt)
        
        if diag.loss_cathode<=500 and diag.loss_aperture<=500: 
            # when there is little beam loss to the cathode or to the aperture
            if cov>0:
                obj = nemit_x/4+np.fabs(cov/10)
            else:
                obj = nemit_x/4+np.fabs(cov/50)
        else:
            # if there is beam loss, then set the objective to 1000
            obj = 1e3   
        
    except:
    #    # if no output beam file, then set the objective to 10000
        obj = 1e4
        os.chdir('..')
        return
    #    print('Error: not accelerated well!')

    os.chdir('..'); #os.system('rm -r '+direc)

    res = [Ipart, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, nemit_x, cov, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_EMSY4000pC(x, *args):
    '''
    Created on July 8, 2019
    Simulation from photocathode via EMSY1 at 5.277 m to 20 m
    Goal function is a combination of average emittance and correlated energy spread exterpolated at undulator center, 29.25 m
    Laser spot fixed at 4 mm, gun phase at MMMG
    Variables to be optimized: gun and booster phases, solenoid current
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    WEIGHTS = [0.25, 0.25, 0., 0., 0., 0.]   # weights for emittance, beta and gamma and
    OBJECTS = [0,       0, 0,  0,  0,  0]    # objectives for emittance, beta and gamma, in x and y directions, respectively

    Ipart = 50000
    BSA = x[0]
    #sigma_x = sigma_y = BSA/4.
    
    sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    C_sigma_x = C_sigma_y = BSA/2./sigma_x
    
    phi_gun, phi_booster = x[1], x[2]
    Imain = x[3]
    
    MaxE_gun = 60
    MaxE_booster = get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = -4.0
    
    field_maps = rootdir+os.sep+'sync'+os.sep+'field-maps'
    
    generator = Generator(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'p', Lt = 21.5e-3, rt = 2e-3, Cathode = True,\
                          Dist_x = '2', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = '2', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)
    
    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = 'beam.ini',\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000, Qbunch = Q_total)
    #newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 40, Nlong_in = 50, N_min = 10, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt', field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = 20, Zemit = 400, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, C_EmitS = True, LPROJECT_EMIT = True,\
                    LOCAL_EMIT = True, Screen = [5.2770, 12.2780, 18.2620])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, output])

    direc = str.format('Q%.1fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Q_total*1e3, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    os.system('mkdir -p '+direc)
    os.chdir(direc)
    
    generator.write('gen.in')
    astra.write('ast.in')
    
    os.system('generator gen.in 2>&1 | tee gen.log')
    os.system('astra ast.in 2>&1 | tee ast.log')
    
    if 1:
        fname = 'ast.0528.001'
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag = BeamDiagnostics(dist = dist)
        
        fname = 'ast.Xemit.001'
        xemit = np.loadtxt(fname)
        select = (xemit[:,0]>5.0)
        xemit = xemit[select]
        nemit_x = np.mean(xemit[:,5])
        
        fname = 'ast.Zemit.001'
        zemit = np.loadtxt(fname)       
        select = (zemit[:,0]>5.0)
        zemit = zemit[select]
        
        f = lambda x, a, b: a+b*x
        popt, pcov = curve_fit(f, zemit[:,0], zemit[:,6])
        cov = f(29.25, *popt)
        
        if diag.loss_cathode<=500 and diag.loss_aperture<=500: 
            # when there is little beam loss to the cathode or to the aperture
            if cov>0:
                obj = nemit_x/4+np.fabs(cov/10)
            else:
                obj = nemit_x/4+np.fabs(cov/50)
        else:
            # if there is beam loss, then set the objective to 1000
            obj = 1e3   
        
    #except:
    #    # if no output beam file, then set the objective to 10000
    #    obj = 1e4
    #    print('Error: not accelerated well!')

    os.chdir('..'); #os.system('rm -r '+direc)

    res = [Ipart, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, nemit_x, cov, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_EMSY2500pC2(x, *args):
    '''
    Created on July 8, 2019
    Simulation from photocathode via EMSY1 at 5.277 m to 20 m
    Goal function is a combination of average emittance and correlated energy spread exterpolated at undulator center, 29.25 m
    Laser spot fixed at 4 mm, gun phase at MMMG
    Variables to be optimized: gun and booster phases, solenoid current
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    WEIGHTS = [0.25, 0.25, 0., 0., 0., 0.]   # weights for emittance, beta and gamma and
    OBJECTS = [0,       0, 0,  0,  0,  0]    # objectives for emittance, beta and gamma, in x and y directions, respectively

    Ipart = 50000
    BSA = x[0]
    #sigma_x = sigma_y = BSA/4.
    
    sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    C_sigma_x = C_sigma_y = BSA/2./sigma_x
    
    phi_gun, phi_booster = x[1], x[2]
    Imain = x[3]
    
    MaxE_gun = 60
    MaxE_booster = get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = -2.5
    
    field_maps = rootdir+os.sep+'sync'+os.sep+'field-maps'
    
    generator = Generator(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'g', sig_clock = 6e-3/2.355, Cathode = True,\
                          Dist_x = '2', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = '2', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)

    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = 'beam.ini',\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000, Qbunch = Q_total)
    #newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 40, Nlong_in = 50, N_min = 10, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt', field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = 20, Zemit = 400, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, C_EmitS = True, LPROJECT_EMIT = True,\
                    LOCAL_EMIT = True, Screen = [0.5, 5.2770, 12.2780, 18.2620])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, output])

    direc = str.format('Q%.1fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Q_total*1e3, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    os.system('mkdir -p '+direc)
    os.chdir(direc)
    
    generator.write('gen.in')
    astra.write('ast.in')
    
    os.system('generator gen.in 2>&1 | tee gen.log')
    os.system('astra ast.in 2>&1 | tee ast.log')
    
    if 1:
        fname = 'ast.0528.001'
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag = BeamDiagnostics(dist = dist)
        
        fname = 'ast.Xemit.001'
        xemit = np.loadtxt(fname)
        select = (xemit[:,0]>5.0)
        xemit = xemit[select]
        nemit_x = np.mean(xemit[:,5])
        
        fname = 'ast.Zemit.001'
        zemit = np.loadtxt(fname)       
        select = (zemit[:,0]>5.0)
        zemit = zemit[select]
        
        f = lambda x, a, b: a+b*x
        popt, pcov = curve_fit(f, zemit[:,0], zemit[:,6])
        cov = f(29.25, *popt)
        
        if diag.loss_cathode<=50 and diag.loss_aperture<=50: 
            # when there is little beam loss to the cathode or to the aperture
            if cov>0:
                obj = nemit_x/4+np.fabs(cov/10)
            else:
                obj = nemit_x/4+np.fabs(cov/50)
        else:
            # if there is beam loss, then set the objective to 1000
            obj = 1e3   
        
    #except:
    #    # if no output beam file, then set the objective to 10000
    #    obj = 1e4
    #    print('Error: not accelerated well!')

    os.chdir('..'); #os.system('rm -r '+direc)

    res = [Ipart, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, nemit_x, cov, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_EMSY2500pC(x, *args):
    '''
    Created on July 8, 2019
    Simulation from photocathode via EMSY1 at 5.277 m to 20 m
    Goal function is a combination of average emittance and correlated energy spread exterpolated at undulator center, 29.25 m
    Laser spot fixed at 4 mm, gun phase at MMMG
    Variables to be optimized: gun and booster phases, solenoid current
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    WEIGHTS = [0.25, 0.25, 0., 0., 0., 0.]   # weights for emittance, beta and gamma and
    OBJECTS = [0,       0, 0,  0,  0,  0]    # objectives for emittance, beta and gamma, in x and y directions, respectively

    Ipart = 50000
    BSA = 4
    #sigma_x = sigma_y = BSA/4.
    
    sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    C_sigma_x = C_sigma_y = BSA/2./sigma_x
    
    phi_gun, phi_booster = 0, x[0]
    Imain = x[1]
    
    MaxE_gun = 60
    MaxE_booster = get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Q_total = -2.5
    
    field_maps = rootdir+os.sep+'sync'+os.sep+'field-maps'
    
    generator = Generator(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = Q_total,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'g', sig_clock = 6e-3/2.355, Cathode = True,\
                          Dist_x = '2', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = '2', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0,\
                          C_sig_x = C_sigma_x, C_sig_y = C_sigma_y)

    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = 'beam.ini',\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000, Qbunch = Q_total)
    #newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 40, Nlong_in = 50, N_min = 10, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun42cavity.txt', field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    
    output = Module('Output', Zstart = 0, Zstop = 20, Zemit = 400, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, C_EmitS = True, LPROJECT_EMIT = True,\
                    LOCAL_EMIT = True, Screen = [0.5, 5.2770, 12.2780, 18.2620])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, output])

    direc = str.format('Q%.1fpC-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Q_total*1e3, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    os.system('mkdir -p '+direc)
    os.chdir(direc)
    
    generator.write('gen.in')
    astra.write('ast.in')
    
    os.system('generator gen.in 2>&1 | tee gen.log')
    os.system('astra ast.in 2>&1 | tee ast.log')
    
    if 1:
        fname = 'ast.0528.001'
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag = BeamDiagnostics(dist = dist)
        
        fname = 'ast.Xemit.001'
        xemit = np.loadtxt(fname)
        select = (xemit[:,0]>5.0)
        xemit = xemit[select]
        nemit_x = np.mean(xemit[:,5])
        
        fname = 'ast.Zemit.001'
        zemit = np.loadtxt(fname)       
        select = (zemit[:,0]>5.0)
        zemit = zemit[select]
        
        f = lambda x, a, b: a+b*x
        popt, pcov = curve_fit(f, zemit[:,0], zemit[:,6])
        cov = f(29.25, *popt)
        
        if diag.loss_cathode<=50 and diag.loss_aperture<=50: 
            # when there is little beam loss to the cathode or to the aperture
            if cov>0:
                obj = nemit_x/4+np.fabs(cov/10)
            else:
                obj = nemit_x/4+np.fabs(cov/50)
        else:
            # if there is beam loss, then set the objective to 1000
            obj = 1e3   
        
    #except:
    #    # if no output beam file, then set the objective to 10000
    #    obj = 1e4
    #    print('Error: not accelerated well!')

    os.chdir('..'); #os.system('rm -r '+direc)

    res = [Ipart, BSA, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, nemit_x, cov, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_EMSY6(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    WEIGHTS = [0.25, 0.25, 0., 0., 0., 0.]   # weights for emittance, beta and gamma and
    OBJECTS = [0,       0, 0,  0,  0,  0]    # objectives for emittance, beta and gamma, in x and y directions, respectively

    Ipart = 50000
    sigma_x = sigma_y = x[0]/4.
    
    sigma_x = sigma_y = 3.0/2.355 # Gaussian truncated
    C_sigma_x = C_sigma_y = x[0]/2./sigma_x
    
    phi_gun, phi_booster = x[1], x[2]
    Imain = x[3]
    
    MaxE_gun = 60
    MaxE_booster = get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    # direc = 'tmp'
    direc = str.format('n%.0fk-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Ipart/1000., sigma_x*4, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    
    os.system('mkdir -p '+direc)    

    field_maps = rootdir+os.sep+'sync'+os.sep+'field-maps'
    # cwd = os.getcwd()
    # field_maps = '../'+os.path.relpath(field_maps, cwd)+'/'

    screen = [0.5, 5.2770, 12.2780, 18.2620]
    gen_astra(direc+os.sep+'gen.in', direc+os.sep+'ast.in', field_maps = field_maps,\
              Ipart = Ipart, sig_x = sigma_x, sig_y = sigma_y, LSPCH = True,\
              Dist_x = '2', Dist_y = '2', C_sig_x = C_sigma_x, C_sig_y  = C_sigma_y,\
              Q_total = -4, dist_z = 'p', Lt = 21.5e-3, rt = 2e-3,\
              MaxE = [MaxE_gun, MaxE_booster], Phi = [phi_gun, phi_booster],\
              MaxB = [MaxB], Lquad = False, zstop = 18.3, zemit = 366,\
              Screen = screen)
    
    os.chdir(direc)
    os.system('generator gen.in > gen.log')
    os.system('astra ast.in > ast.log')
    
    try:
        fname = 'ast.0528.001'
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag = BeamDiagnostics(dist)
        
        fname = 'ast.Xemit.001'
        xemit = np.loadtxt(fname)
        select = (xemit[:,0]>5.0)
        xemit = xemit[select]
        nemit_x = np.mean(xemit[:,5])
        
        fname = 'ast.Zemit.001'
        zemit = np.loadtxt(fname)       
        select = (zemit[:,0]>5.0)
        zemit = zemit[select]
        
        f = lambda x, a, b: a+b*x
        popt, pcov = curve_fit(f, zemit[:,0], zemit[:,6])
        cov = f(30, *popt)
        
        if diag.loss_cathode<=50 and diag.loss_aperture<=50: 
            # when there is little beam loss to the cathode or to the aperture
            if cov>0:
                obj = nemit_x/4+np.fabs(cov/10)
            else:
                obj = nemit_x/4+np.fabs(cov/40)
        else:
            # if there is beam loss, then set the objective to 1000
            obj = 1e3   
        
    except:
        # if no output beam file, then set the objective to 10000
        obj = 1e4
        print('Error: not accelerated well!')

    os.chdir('..'); #os.system('rm -r '+direc)

    res = [Ipart, x[0], MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, nemit_x, cov, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_EMSY5(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    WEIGHTS = [0.25, 0.25, 0., 0., 0., 0.]   # weights for emittance, beta and gamma and
    OBJECTS = [0,       0, 0,  0,  0,  0]    # objectives for emittance, beta and gamma, in x and y directions, respectively

    Ipart = 50000
    sigma_x = sigma_y = x[0]/4.
    phi_gun, phi_booster = x[1], x[2]
    Imain = x[3]
    
    MaxE_gun = 60
    MaxE_booster = get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    # direc = 'tmp'
    direc = str.format('n%.0fk-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Ipart/1000., sigma_x*4, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    
    os.system('mkdir -p '+direc)    

    field_maps = rootdir+os.sep+'sync'+os.sep+'field-maps'
    # cwd = os.getcwd()
    # field_maps = '../'+os.path.relpath(field_maps, cwd)+'/'

    screen = [0.5, 5.2770, 12.2780, 18.2620]
    gen_astra(direc+os.sep+'gen.in', direc+os.sep+'ast.in', field_maps = field_maps,\
              Ipart = Ipart, sig_x = sigma_x, sig_y = sigma_y, LSPCH = True,\
              Q_total = -4, dist_z = 'p', Lt = 21.5e-3, rt = 2e-3,\
              MaxE = [MaxE_gun, MaxE_booster], Phi = [phi_gun, phi_booster],\
              MaxB = [MaxB], Lquad = False, zstop = 18.3, zemit = 366,\
              Screen = screen)
    
    os.chdir(direc)
    os.system('generator gen.in > gen.log')
    os.system('astra ast.in > ast.log')
    
    try:
        fname = 'ast.0528.001'
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag = BeamDiagnostics(dist)
        
        fname = 'ast.Xemit.001'
        xemit = np.loadtxt(fname)
        select = (xemit[:,0]>5.0)
        xemit = xemit[select]
        nemit_x = np.mean(xemit[:,5])
        
        fname = 'ast.Zemit.001'
        zemit = np.loadtxt(fname)       
        select = (zemit[:,0]>5.0)
        zemit = zemit[select]
        
        f = lambda x, a, b: a+b*x
        popt, pcov = curve_fit(f, zemit[:,0], zemit[:,6])
        cov = f(30, *popt)
        
        if diag.loss_cathode<=50 and diag.loss_aperture<=50: 
            # when there is little beam loss to the cathode or to the aperture
            if cov>0:
                obj = nemit_x/4+np.fabs(cov/10)
            else:
                obj = nemit_x/4+np.fabs(cov/40)
        else:
            # if there is beam loss, then set the objective to 1000
            obj = 1e3   
        
    except:
        # if no output beam file, then set the objective to 10000
        obj = 1e4
        print('Error: not accelerated well!')

    os.chdir('..'); #os.system('rm -r '+direc)

    res = [Ipart, sigma_x*4, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, nemit_x, cov, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_EMSY2(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    WEIGHTS = [1, 1, 0., 0., 0., 0.]                # weights for emittance, beta and gamma and
    OBJECTS = [0, 0, 0,  0,  0,  0]    # objectives for emittance, beta and gamma, in x and y directions, respectively

    Ipart = 50000
    sigma_x = sigma_y = 3.0/4.
    phi_gun, phi_booster = 0, x[0]
    Imain = x[1]
    
    MaxE_gun = 51.64
    MaxE_booster = 18.06 # get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    # direc = 'tmp'
    direc = str.format('n%.0fk-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Ipart/1000., sigma_x*4, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    
    os.system('mkdir -p '+direc)    

    field_maps = rootdir+os.sep+'sync'+os.sep+'field-maps'
    # cwd = os.getcwd()
    # field_maps = '../'+os.path.relpath(field_maps, cwd)+'/'

    screen = [0.5, 0.8030, 1.3790, 1.7080, 5.2770, 6.2500, 7.1250, 8.4100, \
              8.9200, 12.2780, 13.0380, 13.7980, 14.5580, 15.3180, 16.3030, 18.2620, 20]
    gen_astra(direc+os.sep+'gen.in', direc+os.sep+'ast.in', field_maps = field_maps,\
              Ipart = Ipart, sig_x = sigma_x, sig_y = sigma_y, LSPCH = True,\
              Q_total = -1.6, Lt = 0, dist_z = 'g', sigma_clock = 8.15e-3/2.35482,\
              MaxE = [MaxE_gun, MaxE_booster], Phi = [phi_gun, phi_booster],\
              MaxB = [MaxB], Lquad = False, zstop = 20, zemit = 200,\
              Screen = screen)
    
    os.chdir(direc)
    os.system('generator gen.in > gen.log')
    os.system('astra ast.in > ast.log')
    
    try:
        fname = 'ast.0528.001'
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag1 = BeamDiagnostics(dist)
        
        fname = 'ast.2000.001'
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag2 = BeamDiagnostics(dist)

        diag = diag1; cov1 = diag.cor_zEk;
        if diag.loss_cathode<=50 and diag.loss_aperture<=50: 
            # when there is little beam loss to the cathode or to the aperture
            nemit_x, nemit_y, beta_x, beta_y, alpha_x, alpha_y = \
                          diag.nemit_x, diag.nemit_y, diag.beta_x, diag.beta_y, diag.alpha_x, diag.alpha_y
            obj1 = np.sqrt(WEIGHTS[0]*(nemit_x-OBJECTS[0])**2+WEIGHTS[1]*(nemit_y-OBJECTS[1])**2+ 
                          WEIGHTS[2]*(beta_x -OBJECTS[2])**2+WEIGHTS[3]*(beta_y -OBJECTS[3])**2+
                          WEIGHTS[4]*(alpha_x-OBJECTS[4])**2+WEIGHTS[5]*(alpha_y-OBJECTS[5])**2)
        else:
            # if there is beam loss, then set the objective to 1000
            obj1 = 1e3   
        
        diag = diag2; cov2 = diag.cor_zEk;
        if 1: 
            # when there is little beam loss to the cathode or to the aperture
            nemit_x, nemit_y, beta_x, beta_y, alpha_x, alpha_y = \
                          diag.nemit_x, diag.nemit_y, diag.beta_x, diag.beta_y, diag.alpha_x, diag.alpha_y
            obj2 = np.sqrt(WEIGHTS[0]*(nemit_x-OBJECTS[0])**2+WEIGHTS[1]*(nemit_y-OBJECTS[1])**2+ 
                          WEIGHTS[2]*(beta_x -OBJECTS[2])**2+WEIGHTS[3]*(beta_y -OBJECTS[3])**2+
                          WEIGHTS[4]*(alpha_x-OBJECTS[4])**2+WEIGHTS[5]*(alpha_y-OBJECTS[5])**2)
        
        obj = (obj1+obj2)/2.0
        
        cov = linear_interp(28.8, [5.28, cov1], [20.0, cov2])
        obj = np.sqrt(obj**2+cov**2);
        
    except:
        # if no output beam file, then set the objective to 10000
        obj = 1e4
        print('Error: not accelerated well!')

    os.chdir('..'); #os.system('rm -r '+direc)

    res = [Ipart, sigma_x*4, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_EMSY1to18m(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameters:
      x: an array or list of the variables to be optimized
    Returns:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    #n50k-D-3.56mm-E1-60.00MV_m-phi1--2.11deg-E2-13.11MV_m-phi2--19.79deg-I-348.18A
    Ipart = 50000
    sigma_x = sigma_y = 3.56/4.
    phi_gun, phi_booster = -2.11, -19.79
    Imain = 348.18
    
    MaxE_gun = 60.
    MaxE_booster = get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    direc = str.format('n%.0fk-' % (Ipart/1000.))+str.format('%.2f-%.2f-%.2f-%.2f-%.2f-%.2f' % tuple(x))
    
    os.system('mkdir -p '+direc)    

    field_maps = rootdir+os.sep+'sync'+os.sep+'field-maps'
    cwd = os.getcwd()
    #field_maps = '..'+os.sep+os.path.relpath(field_maps, cwd)

    gen_astra(direc+os.sep+'gen.in', direc+os.sep+'ast.in', field_maps = field_maps,\
              SCO_z0 = 5.277, SCO_BEAMLINE = 'beamlineA2.txt', SCO_OPTIMIZED = x, \
              Ipart = Ipart, sig_x = sigma_x, sig_y = sigma_y,\
              Distribution = '..'+os.sep+'ast.0528.001', LSPCH = True,\
              L2D_3D = True, z_trans = 5.3, Nxf = 16, Nyf = 16, Nzf = 16,\
              MaxE = [MaxE_gun, MaxE_booster], Phi = [phi_gun, phi_booster],\
              MaxB = [MaxB], Lquad = True, zstart = 5, zstop = 18, zemit = 130,\
              Screen = [18])
    
    os.chdir(direc)
    #os.system('generator gen.in > gen.log')
    os.system('astra ast.in > ast.log')
    
    try:
        fname = 'ast.1800.001'
        dist = np.loadtxt(fname)
        dist[1:,2] += dist[0,2]; dist[1:,5] += dist[0,5]
        diag = BeamDiagnostics(dist)
        
        if diag.loss_cathode<=50 and diag.loss_aperture<=50: 
            # when there is little beam loss to the cathode or to the aperture
            nemit_x, nemit_y, beta_x, beta_y, alpha_x, alpha_y = \
                          diag.nemit_x, diag.nemit_y, diag.beta_x, diag.beta_y, diag.alpha_x, diag.alpha_y
            obj = np.sqrt(WEIGHTS[0]*(nemit_x-OBJECTS[0])**2+WEIGHTS[1]*(nemit_y-OBJECTS[1])**2+ 
                          WEIGHTS[2]*(beta_x -OBJECTS[2])**2+WEIGHTS[3]*(beta_y -OBJECTS[3])**2+
                          WEIGHTS[4]*(alpha_x-OBJECTS[4])**2+WEIGHTS[5]*(alpha_y-OBJECTS[5])**2)
        else:
            # if there is beam loss, then set the objective to 1000
            obj = 1e3   
    except:
        # if no output beam file, then set the objective to 10000
        obj = 1e4
        print('Error: not accelerated well!')

    os.chdir('..'); #os.system('rm -r '+direc)

    if isinstance(x, list):
        res = [Ipart]+x.tolist()+[obj]
    else:
        res = [Ipart]+x.tolist()+[obj]
    with open('results2.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_emsy1(x, *args):
    '''
    Photocathode to the EMSY1 at 5.277 m
    Parameter:
      x: an array or list of the variables to be optimized
    Return:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    sigma_x = sigma_y = x[0]/4.
    phi_gun, phi_booster = x[1], x[2]
    Imain = x[3]
    
    MaxE_gun = 60.
    MaxE_booster = get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Ipart = 50000
    generator = Generator(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -4.,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'p', Lt = 21.5e-3, rt = 2e-3, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0)

    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = './ast.0528.001',\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    newrun.set(Run = 2)


    charge = Module('Charge', LSPCH = True, Lmirror = True, Nrad = 50, Nlong_in = 50, N_min = 10, Max_scale = 0.05, Max_count = 20)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun45cavity.txt', field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    output = Module('Output', Zstart = 0, Zstop = 25.0, Zemit = 2500, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [25.0])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, apertu, output])

    direc = str.format('n%.0fk-D-%.2fmm-E1-%.2fMV_m-phi1-%.2fdeg-E2-%.2fMV_m-phi2-%.2fdeg-I-%.2fA' %\
                       (Ipart/1000., sigma_x*4, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain))
    #os.system('mkdir -p '+direc)
    print(direc)
    os.chdir(direc)

    #generator.write()
    astra.write('ast.2.in')
    astra.qsub('ast.2')
    os.chdir('../')
    
    #os.system('generator gen.in > gen.log')
    #os.system('astra ast.in > ast.log')
    return
    
    try:
        fname = 'ast.0528.001'
        beam = np.loadtxt(fname)
        beam[1:,2] += beam[0,2]; beam[1:,5] += beam[0,5]
        tmp = astra_post(beam)

        if tmp[20]<=50 and tmp[21]<=50: # when there is no beam loss to the cathode or to the aperture
            nemit_x, nemit_y, beta_x, beta_y, alpha_x, alpha_y = tmp[1], tmp[2], tmp[13], tmp[17], tmp[12], tmp[16]
            obj = np.sqrt(WEIGHTS[0]*(nemit_x-OBJECTS[0])**2+WEIGHTS[1]*(nemit_y-OBJECTS[1])**2+ 
                          WEIGHTS[2]*(beta_x -OBJECTS[2])**2+WEIGHTS[3]*(beta_y -OBJECTS[3])**2+
                          WEIGHTS[4]*(alpha_x-OBJECTS[4])**2+WEIGHTS[5]*(alpha_y-OBJECTS[5])**2)
        else:
            obj = 1e2
    except:
        obj = 1e3
        print('Error: not accelerated well!')

    os.chdir('..'+os.sep); #os.system('rm -r '+direc)

    res = [Ipart, sigma_x*4, MaxE_gun, phi_gun, MaxE_booster, phi_booster, Imain, obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

cwd = os.getcwd()
#field_maps = '..'+os.sep+os.path.relpath(field_maps, cwd)+os.sep

def obj_emsy1towall(x = None, *args, **kwargs):
    '''
    EMSY1 to the center of the wall
    Parameter:
      x: an array or list of the variables to be optimized
    Return:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    kv = kwargs
    
    sigma_x = sigma_y = 3.501037E+00/4.    
    phi_gun, phi_booster = -3.885682E+00, -2.306022E+01
    Imain = 3.603969E+02

    MaxE_gun = 60.
    MaxE_booster = get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Ipart = 50000
    if 'Ipart' in list(kv.keys()):
        Ipart = kv['Ipart']
    generator = Generator(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -4.,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'p', Lt = 21.5e-3, rt = 2e-3, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0)

    Distribution = '..'+os.sep+'ast.0528.001'
    if 'Distribution' in list(kv.keys()):
        Distribution = kv['Distribution']
        
    Run = 1
    if 'Run' in list(kv.keys()):
        Run = kv['Run']
        
    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = Distribution,\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    newrun.set(Run = Run)


    charge = Module('Charge', LSPCH = True, L2D_3D = True, Lmirror = True, Nrad = 50, Nlong_in = 50, N_min = 10,\
                    Max_scale = 0.05, Max_count = 20, z_trans = 5.0, Nxf = 16, Nyf = 16, Nzf = 16)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun45cavity.txt', field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    output = Module('Output', Zstart = 5, Zstop = 18, Zemit = 130, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [18])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    
    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, apertu, output])
    
    # prepare for the quadrupoles from SCO optimization results
    try:
        otvet_file = 'Otvet.dat'
        beamline_file = 'beamlineY.txt'
        if 'otvet_file' in list(kv.keys()):
            otvet_file = kv['otvet_file']
        if 'beamline_file' in list(kv.keys()):
            beamline_file = kv['beamline_file']
        
        [Q_pos, Q_grad] = readfromsco(z0 = 5.277, otvet_file = otvet_file, beamline_file = beamline_file)

        if x != None:
            Q_grad[0], Q_grad[2], Q_grad[13], Q_grad[16], Q_grad[20], Q_grad[21] = x[0], x[1], x[2], x[3], x[4], x[5]
        else:
            x = [Q_grad[0], Q_grad[2], Q_grad[13], Q_grad[16], Q_grad[20], Q_grad[21]]
        Q_type = [field_maps+os.sep+'Q3.data' for i in Q_pos]
        Q_noscale = [False for i in Q_pos]
        
        quadru = Module('Quadrupole', Lquad = True, Q_grad = Q_grad, Q_pos = Q_pos, Q_type = Q_type)#, Q_noscale = Q_noscale)

        astra.add_module(quadru)
    except IOError:
        print('IOError: No input files\n')
        exit()

    direc = str.format('n50k-%.2f-%.2f-%.2f-%.2f-%.2f-%.2f' % tuple(x))
    #direc = '200k-3'
    os.system('mkdir -p '+direc)
    os.chdir(direc)

    generator.write()
    astra.write()

    os.system('generator gen.in > gen.log')
    os.system('astra ast.in > ast.log')

    try:
        fname = 'ast.2500.001'
        beam = np.loadtxt(fname)
        beam[1:,2] += beam[0,2]; beam[1:,5] += beam[0,5]
        tmp = astra_post(beam)

        if tmp[20]<=50 and tmp[21]<=50: # when there is no beam loss to the cathode or to the aperture
            nemit_x1, nemit_y1, beta_x1, beta_y1, alpha_x1, alpha_y1 = tmp[1], tmp[2], tmp[3], tmp[4],\
                     -tmp[12]*tmp[15], -tmp[16]*tmp[19]
            
            obj = np.sqrt(WEIGHTS[0]*(nemit_x1-OBJECTS[0])**2+WEIGHTS[1]*(nemit_y1-OBJECTS[1])**2+
                          WEIGHTS[2]*(beta_x1 -OBJECTS[2])**2+WEIGHTS[3]*(beta_y1 -OBJECTS[3])**2+
                          WEIGHTS[4]*(alpha_x1-OBJECTS[4])**2+WEIGHTS[5]*(alpha_y1-OBJECTS[5])**2)
        else:
            obj = 1e2

    except:
        obj = 1e4
        print('Error: not accelerated well!')

    os.chdir('../'); #os.system('rm -r '+direc)

    res = [x[0], x[1], x[2], x[3], x[4], x[5], obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_walltound(x = None, *args, **kwargs):
    '''
    Parameter:
      x: an array or list of the variables to be optimized
    Return:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    kv = kwargs
    
    sigma_x = sigma_y = 3.501037E+00/4.    
    phi_gun, phi_booster = -3.885682E+00, -2.306022E+01
    Imain = 3.603969E+02

    MaxE_gun = 60.
    MaxE_booster = get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Ipart = 200000
    if 'Ipart' in list(kv.keys()):
        Ipart = kv['Ipart']
    generator = Generator(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -4.,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'p', Lt = 21.5e-3, rt = 2e-3, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0)

    Distribution = '../ast.0528.001'
    if 'Distribution' in list(kv.keys()):
        Distribution = kv['Distribution']
        
    Run = 1
    if 'Run' in list(kv.keys()):
        Run = kv['Run']
        
    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = Distribution,\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    newrun.set(Run = Run)


    charge = Module('Charge', LSPCH = True, L2D_3D = True, Lmirror = True, Nrad = 50, Nlong_in = 50, N_min = 10,\
                    Max_scale = 0.05, Max_count = 20, z_trans = 5.0, Nxf = 16, Nyf = 16, Nzf = 16)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun45cavity.txt', field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    output = Module('Output', Zstart = 0, Zstop = 27, Zemit = 1000, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [5.28, 24.5, 25.0, 25.5, 27.0])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    

    # prepare for the quadrupoles from SCO optimization results
    try:
        otvet_file = 'Otvet.dat'
        beamline_file = 'beamlineY.txt'
        if 'otvet_file' in list(kv.keys()):
            otvet_file = kv['otvet_file']
        if 'beamline_file' in list(kv.keys()):
            beamline_file = kv['beamline_file']
        
        [Q_pos, Q_grad] = readfromsco(z0 = 25.0, otvet_file = otvet_file, beamline_file = beamline_file)

        if x != None:
            Q_grad[0], Q_grad[1], Q_grad[2] = x[0], x[1], x[2]
        else:
            x = [Q_grad[0], Q_grad[1], Q_grad[2]]
        Q_type = [field_maps+os.sep+'Q3.data' for i in Q_pos]
        Q_noscale = [False for i in Q_pos]
        
        quadru = Module('Quadrupole', Lquad = True, Q_grad = Q_grad, Q_pos = Q_pos, Q_type = Q_type)#, Q_noscale = Q_noscale)

    except IOError:
        print('IOError: No input files\n')
        exit()

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, apertu, output, quadru])

    direc = str.format('%.2f-%.2f-%.2f' % tuple(x))
    #direc = '200k-3'
    os.system('mkdir -p '+direc)
    os.chdir(direc)

    generator.write()
    astra.write()

    os.system('generator gen.in > gen.log')
    os.system('astra ast.in > ast.log')

    try:
        fname = 'ast.2700.001'
        beam = np.loadtxt(fname)
        beam[1:,2] += beam[0,2]; beam[1:,5] += beam[0,5]
        tmp = astra_post(beam)

        if tmp[20]<=50 and tmp[21]<=50: # when there is no beam loss to the cathode or to the aperture
            nemit_x1, nemit_y1, beta_x1, beta_y1, alpha_x1, alpha_y1 = tmp[1], tmp[2], tmp[3], tmp[4],\
                     -tmp[12]*tmp[15], -tmp[16]*tmp[19]
            
            obj = np.sqrt(WEIGHTS[0]*(nemit_x1-OBJECTS[0])**2+WEIGHTS[1]*(nemit_y1-OBJECTS[1])**2+
                          WEIGHTS[2]*(beta_x1 -OBJECTS[2])**2+WEIGHTS[3]*(beta_y1 -OBJECTS[3])**2+
                          WEIGHTS[4]*(alpha_x1-OBJECTS[4])**2+WEIGHTS[5]*(alpha_y1-OBJECTS[5])**2)
        else:
            obj = 1e2

    except:
        obj = 1e4
        print('Error: not accelerated well!')

    os.chdir('../'); #os.system('rm -r '+direc)

    res = [x[0], x[1], x[2], x[3], x[4], x[5], obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_emsy1toent(x = None, *args, **kwargs):
    '''
    EMSY1 to the center of the wall
    Parameter:
      x: an array or list of the variables to be optimized
    Return:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    kv = kwargs
    
    sigma_x = sigma_y = 3.501037E+00/4.    
    phi_gun, phi_booster = -3.885682E+00, -2.306022E+01
    Imain = 3.603969E+02

    MaxE_gun = 60.
    MaxE_booster = get_MaxE_booster(MaxE_gun, phi_gun, phi_booster)
    MaxB = I2B(Imain)

    Ipart = 200000
    if 'Ipart' in list(kv.keys()):
        Ipart = kv['Ipart']
    generator = Generator(FNAME = 'beam.ini', IPart = Ipart, Species = 'electrons', Q_total = -4.,\
                          Ref_Ekin = 0.0e-6, LE = 0.55e-3, dist_pz = 'i',
                          Dist_z = 'p', Lt = 21.5e-3, rt = 2e-3, Cathode = True,\
                          Dist_x = 'r', sig_x = sigma_x, Dist_px = 'g', Nemit_x = 0,\
                          Dist_y = 'r', sig_y = sigma_y, Dist_py = 'g', Nemit_y = 0)

    Distribution = '../ast.0528.001'
    if 'Distribution' in list(kv.keys()):
        Distribution = kv['Distribution']
        
    Run = 1
    if 'Run' in list(kv.keys()):
        Run = kv['Run']
        
    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = Distribution,\
                    Auto_Phase = True, Track_All = True, check_ref_part = False, Lprompt = False, Max_step=200000)
    newrun.set(Run = Run)


    charge = Module('Charge', LSPCH = True, L2D_3D = True, Lmirror = True, Nrad = 50, Nlong_in = 50, N_min = 10,\
                    Max_scale = 0.05, Max_count = 20, z_trans = 5.0, Nxf = 16, Nyf = 16, Nzf = 16)
    cavity = Module('Cavity', LEfield = True, File_Efield = [field_maps+os.sep+'gun45cavity.txt', field_maps+os.sep+'CDS14_15mm.txt'],\
                    MaxE = [MaxE_gun, MaxE_booster], C_pos = [0., 2.675], Nue = [1.3, 1.3], Phi = [phi_gun, phi_booster])
    
    soleno = Module('Solenoid', LBfield = True, File_Bfield = [field_maps+os.sep+'gunsolenoidsPITZ.txt'], MaxB = MaxB, S_pos = [0.])
    output = Module('Output', Zstart = 0, Zstop = 27, Zemit = 1000, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [5.28, 24.5, 25.0, 25.5, 27.0])
    apertu = Module('Aperture', LApert = True, File_Aperture = [field_maps+os.sep+'app.txt'])
    
    astra = Astra()
    astra.add_modules([newrun, charge, cavity, soleno, apertu, output])
    
    # prepare for the quadrupoles from SCO optimization results
    try:
        otvet_file = 'Otvet.dat'
        beamline_file = 'beamlineY.txt'
        if 'otvet_file' in list(kv.keys()):
            otvet_file = kv['otvet_file']
        if 'beamline_file' in list(kv.keys()):
            beamline_file = kv['beamline_file']
        
        [Q_pos, Q_grad] = readfromsco(z0 = 5.277, otvet_file = otvet_file, beamline_file = beamline_file)

        if x != None:
            Q_grad[0], Q_grad[2], Q_grad[13], Q_grad[16], Q_grad[20], Q_grad[21], Q_grad[22], Q_grad[23], Q_grad[24] =\
            x[0], x[1], x[2], x[3], x[4], x[5], x[6], x[7], x[8]
        else:
            x = [Q_grad[0], Q_grad[2], Q_grad[13], Q_grad[16], Q_grad[20], Q_grad[21], Q_grad[22], Q_grad[23], Q_grad[24]]
        Q_type = [field_maps+os.sep+'Q3.data' for i in Q_pos]
        Q_noscale = [False for i in Q_pos]
        
        quadru = Module('Quadrupole', Lquad = True, Q_grad = Q_grad, Q_pos = Q_pos, Q_type = Q_type)#, Q_noscale = Q_noscale)

        astra.add_module(quadru)
    except IOError:
        print('IOError: No input files\n')
        exit()

    direc = str.format('n200k-%.2f-%.2f-%.2f-%.2f-%.2f-%.2f-%.2f-%.2f-%.2f' % tuple(x))
    #direc = '200k-3'
    os.system('mkdir -p '+direc)
    os.chdir(direc)

    generator.write()
    astra.write()

    os.system('generator gen.in 2>&1 | tee gen.log')
    os.system('astra ast.in 2>&1 | tee ast.log')

    try:
        fname = 'ast.2700.001'
        beam = np.loadtxt(fname)
        beam[1:,2] += beam[0,2]; beam[1:,5] += beam[0,5]
        tmp = astra_post(beam)

        if tmp[20]<=50 and tmp[21]<=50: # when there is no beam loss to the cathode or to the aperture
            nemit_x1, nemit_y1, beta_x1, beta_y1, alpha_x1, alpha_y1 = tmp[1], tmp[2], tmp[3], tmp[4],\
                     -tmp[12]*tmp[15], -tmp[16]*tmp[19]
            
            obj = np.sqrt(WEIGHTS[0]*(nemit_x1-OBJECTS[0])**2+WEIGHTS[1]*(nemit_y1-OBJECTS[1])**2+
                          WEIGHTS[2]*(beta_x1 -OBJECTS[2])**2+WEIGHTS[3]*(beta_y1 -OBJECTS[3])**2+
                          WEIGHTS[4]*(alpha_x1-OBJECTS[4])**2+WEIGHTS[5]*(alpha_y1-OBJECTS[5])**2)
        else:
            obj = 1e2

    except:
        obj = 1e4
        print('Error: not accelerated well!')

    os.chdir('../'); #os.system('rm -r '+direc)

    res = [x[0], x[1], x[2], x[3], x[4], x[5], obj]
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_und(x = None, *args, **kwargs):
    '''
    Optimize the beam size and beam covariance, given the emittance.
    Parameter:
      x: an array or list of the variables to be optimized, sigma_x, sigma_y, alpha_x and alpha_y
    Return:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    kv = kwargs
    
    N_u = 120; lam_u = 0.03; L_u = N_u*lam_u
    By_u_max = 1.28 # T
    pos_u = L_u/2.
    
    P0 = 17.05 # MeV/c
    Ek = momentum2kinetic(P0); gamma_r = kinetic2gamma(Ek); bg_r = gamma2bg(gamma_r)
    nemit_x = nemit_y = 4e-6 # mm mrad
    emit_x = nemit_x/bg_r; emit_y = nemit_y/bg_r
    sigma_Ek = 0.5e-2*Ek*1e3 # keV
    
    if x is not None:
        # sigma_x in mm, alpha_x dimensionless
        sigma_x, sigma_y, alpha_x, alpha_y = x 
    else:
        sigma_x, sigma_y, alpha_x, alpha_y = 1.576772E+00,   1.775836E-01,   7.183778E+00,   1.716220E+00
    beta_x, beta_y = sigma_x**2/emit_x*1e-6, sigma_y**2/emit_y*1e-6
    gamma_x, gamma_y = (1.+alpha_x**2)/beta_x, (1.+alpha_y**2)/beta_y
    
    cor_px = -alpha_x/beta_x*sigma_x
    cor_py = -alpha_y/beta_y*sigma_y
    
    #OBJECTS[2], OBJECTS[3], OBJECTS[4], OBJECTS[5] = beta_x, beta_y, -alpha_x, -alpha_y
    
    WEIGHTS[0], WEIGHTS[1], WEIGHTS[2], WEIGHTS[3], WEIGHTS[4], WEIGHTS[5] = \
        0, 0, 5./55,   11./55,   5./55,                    11./55    
    OBJECTS[0], OBJECTS[1], OBJECTS[2], OBJECTS[3], OBJECTS[4], OBJECTS[5] = \
        0, 0, sigma_x, sigma_y, -(-alpha_x*nemit_x/bg_r), -(-alpha_y*nemit_y/bg_r)
    
    if len(args) > 0:
        execute = args[0]
    else:
        execute = 1
    #Distribution = '../ast.2700.001'
    #if 'Distribution' in kv.keys():
    #    Distribution = kv['Distribution']
        
    Run = 1
    if 'Run' in list(kv.keys()):
        Run = kv['Run']
    
    Ipart = 200000
    generator = Generator(FNAME = 'beam_und.ini', IPart = Ipart, Species = 'electrons', Q_total = -2.5,\
                          Ref_Ekin = Ek, sig_Ekin = sigma_Ek, Dist_pz = 'g', Cathode = False,\
                          Dist_z = 'p', Lz = 21.5e-12*g_c*1e3, rz = 2e-12*g_c*1e3,\
                          Dist_x = 'g', sig_x = sigma_x, Dist_px = 'g', Nemit_x = nemit_x*1e6, cor_px = cor_px,\
                          Dist_y = 'g', sig_y = sigma_y, Dist_py = 'g', Nemit_y = nemit_y*1e6, cor_py = cor_py)

    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = 'beam_und.ini',\
                    Auto_Phase = True, Track_All = True, Track_On_Axis = False, check_ref_part = False,\
                    Lprompt = False)
    newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, LSPCH3D = True, Nxf = 32, Nyf = 32, Nzf = 32,\
                    Max_scale = 0.05, Max_count = 20)
    
    cavity = Module('Cavity', LEfield = True, By_stat = [True], File_Efield = [field_maps+os.sep+'3Dund-15x5mm'],\
                    MaxE = [By_u_max], C_pos = [pos_u], Nue = [0], Phi = [0])
    
    output = Module('Output', Zstart = 0, Zstop = 3.6, Zemit = 180, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [3.6])

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, output])

    direc = str.format('n%.0fk-sig_x-%.2fmm-sig_y-%.2fmm-alp_x-%.2f-alp_y-%.2f' %\
                       (Ipart/1000., sigma_x, sigma_y, alpha_x, alpha_y))
    
    os.system('mkdir -p '+direc)
    os.chdir(direc)

    generator.write()
    astra.write()
    
    if not execute:
        os.system('generator gen.in 2>&1 | tee gen.log')
        os.chdir('../')
        obj = -1
    else:
        os.system('generator gen.in 2>&1 | tee gen.log')
        os.system('astra ast.in 2>&1 | tee ast.log')

        try:
            fname = 'ast.0360.001'
            beam = np.loadtxt(fname)
            beam[1:,2] += beam[0,2]; beam[1:,5] += beam[0,5]
            diag = BeamDiagnostics(dist = beam)

            # when there is no beam loss to the cathode or to the aperture -alpha_x*nemit_x
            if diag.loss_cathode<=50 and diag.loss_aperture<=50: 
                obj = np.sqrt(WEIGHTS[0]*np.fabs(diag.nemit_x-OBJECTS[0])+WEIGHTS[1]*np.fabs(diag.nemit_y-OBJECTS[1])+
                              WEIGHTS[2]*np.fabs(diag.std_x  -OBJECTS[2])+WEIGHTS[3]*np.fabs(diag.std_y  -OBJECTS[3])+
                              WEIGHTS[4]*np.fabs(-diag.alpha_x*diag.emit_x*1e6-OBJECTS[4])+
                              WEIGHTS[5]*np.fabs(-diag.alpha_y*diag.emit_y*1e6-OBJECTS[5]))
            else:
                obj = 1e3
        except:
            obj = 1e4
            print('Error: not accelerated well!')
        os.system('rm beam_und.ini')
        os.chdir('../')

        res = [Ipart, sigma_x, sigma_y, alpha_x, alpha_y, obj]
        with open('results.dat','a') as f_handle:
            np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def resonant_energy(K, lam_u, lam_s):
    '''
    Calcualte the resonant energy given the wavelength
    Parameters
      K: undulator parameter
      lam_u: undulator period
      lam_s: radiation wavelength
    Returns
      gamma: Lorentz factor of particles with the resonant energy 
    '''
    return np.sqrt(lam_u/2./lam_s*(1+K**2/2.))
def resonant_undulator_parameter(lam_s, lam_u, gamma):
    return np.sqrt(2.*(2*gamma**2*lam_s/lam_u-1))

def obj_und3(x = None, *args, **kwargs):
    '''
    Optimize the beam size and beam covariance, given the emittance.
    Parameter:
      x: an array or list of the variables to be optimized, sigma_x, sigma_y, alpha_x and alpha_y
    Return:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''
    
    kv = kwargs
    
    N_u = 150; lam_u = 0.04; L_u = N_u*lam_u
    By_u_max = 1.28 # T
    pos_u = L_u/2.
    
    P0 = 17.05 # MeV/c
    Ek = momentum2kinetic(P0); 
    Ek = 20
    gamma_r = kinetic2gamma(Ek); bg_r = gamma2bg(gamma_r)
    
    nemit_x = nemit_y = 3e-6 # mm mrad
    emit_x = nemit_x/bg_r; emit_y = nemit_y/bg_r
    
    cor_Ekin = 0.5e-2*Ek*1e3 # keV
    sig_Ekin = 0.05e-2*Ek*1e3
    
    if x is not None:
        # sigma_x in mm, alpha_x dimensionless
        sigma_x, sigma_y, alpha_x, alpha_y = x
        print((sigma_x, sigma_y, alpha_x, alpha_y))
    else:
        sigma_x, sigma_y, alpha_x, alpha_y = 1.576772E+00,   1.775836E-01,   7.183778E+00,   1.716220E+00
    beta_x, beta_y = sigma_x**2/emit_x*1e-6, sigma_y**2/emit_y*1e-6
    gamma_x, gamma_y = (1.+alpha_x**2)/beta_x, (1.+alpha_y**2)/beta_y
    
    cor_px = -alpha_x/beta_x*sigma_x
    cor_py = -alpha_y/beta_y*sigma_y

    if len(args) > 0:
        execute = args[0]
    else:
        execute = 1
    
    if len(args) > 1:
        Ipeak = args[1]
    else:
        Ipeak = 4e-9/21.5e-12
    if len(args) > 2:
        Q_total = args[2]
    else:
        FWHM = 4;
    #ratio = (4e-9/21.5e-12)/Ipeak
    
    Lz = Q_total*1e-9/Ipeak*g_c*1e3; print(Lz)
    sig_z = Q_total*1e-9/Ipeak*g_c*1e3/np.sqrt(2.*np.pi)
    #FWHM = 13.3e-12
    #Distribution = '../ast.2700.001'
    #if 'Distribution' in kv.keys():
    #    Distribution = kv['Distribution']
    
    Run = 1
    if 'Run' in list(kv.keys()):
        Run = kv['Run']
    
    Ipart = int(100000*Q_total)
    
    fname = 'beam_%.0fA_%.1fnC.ini' % (Ipeak, Q_total)
    #fname = 'beam_%.2fmm_%.2fmm' % (x[0], x[1])
    generator = Generator1(FNAME = fname, IPart = Ipart, Species = 'electrons', Q_total = Q_total,\
                          Ref_Ekin = Ek, sig_Ekin = sig_Ekin, cor_Ekin = cor_Ekin, Dist_pz = 'g', Cathode = False,\
                          Dist_z = 'g', sig_z = sig_z,\
                          Dist_x = 'g', sig_x = sigma_x, Dist_px = 'g', Nemit_x = nemit_x*1e6, cor_px = cor_px,\
                          Dist_y = 'g', sig_y = sigma_y, Dist_py = 'g', Nemit_y = nemit_y*1e6, cor_py = cor_py)
                          # \Dist_z = 'g', sig_z = ratio*21.5e-12*g_c*1e3/np.sqrt(2.*np.pi),\
                          # Dist_z = 'p', Lz = Lz, rz = 1e-12*g_c*1e3,
    
    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = 'beam_und.ini',\
                    Auto_Phase = True, Track_All = True, Track_On_Axis = False, check_ref_part = False,\
                    Lprompt = False)
    newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, LSPCH3D = True, Nxf = 16, Nyf = 16, Nzf = 16,\
                    Max_scale = 0.05, Max_count = 20)
    
    cavity = Module('Cavity', LEfield = True, By_stat = [True], File_Efield = [field_maps+os.sep+'3Dund-15x5mm'],\
                    MaxE = [By_u_max], C_pos = [pos_u], Nue = [0], Phi = [0])
    
    output = Module('Output', Zstart = 0, Zstop = 3.6, Zemit = 180, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [1.8, 3.6])

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, output])

    #direc = str.format('n%.0fk-sig_x-%.2fmm-sig_y-%.2fmm-alp_x-%.2f-alp_y-%.2f' %\
    #                   (Ipart/1000., sigma_x, sigma_y, alpha_x, alpha_y))
    
    direc = 'dist'
    
    os.system('mkdir -p '+direc)
    os.chdir(direc)

    generator.write()
    #astra.write()
    
    if not execute:
        os.system('generator gen.in 2>&1 | tee gen.log')
        os.chdir('../')
        obj = -1
    else:
        os.system('generator gen.in 2>&1 | tee gen.log')
        os.system('astra ast.in 2>&1 | tee ast.log')

        try:
            fname = 'ast.0360.001'
            beam = np.loadtxt(fname)
            beam[1:,2] += beam[0,2]; beam[1:,5] += beam[0,5]
            diag = BeamDiagnostics(beam)

            # when there is no beam loss to the cathode or to the aperture -alpha_x*nemit_x
            if diag.loss_cathode<=50 and diag.loss_aperture<=50: 
                obj = np.sqrt(WEIGHTS[0]*np.fabs(diag.nemit_x-OBJECTS[0])+WEIGHTS[1]*np.fabs(diag.nemit_y-OBJECTS[1])+
                              WEIGHTS[2]*np.fabs(diag.std_x  -OBJECTS[2])+WEIGHTS[3]*np.fabs(diag.std_y  -OBJECTS[3])+
                              WEIGHTS[4]*np.fabs(-diag.alpha_x*diag.emit_x*1e6-OBJECTS[4])+
                              WEIGHTS[5]*np.fabs(-diag.alpha_y*diag.emit_y*1e6-OBJECTS[5]))
            else:
                obj = 1e3
        except:
            obj = 1e4
            print('Error: not accelerated well!')

        os.chdir('../')

        res = [Ipart, sigma_x, sigma_y, alpha_x, alpha_y, obj]
        with open('results.dat','a') as f_handle:
            np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_und2(x = None, *args, **kwargs):
    '''
    Optimize the beam size and beam covariance, given the emittance.
    Parameter:
      x: an array or list of the variables to be optimized, sigma_x, sigma_y, alpha_x and alpha_y
    Return:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''
    
    kv = kwargs
    
    N_u = 120; lam_u = 0.03; L_u = N_u*lam_u
    By_u_max = 1.28 # T
    pos_u = L_u/2.
    
    P0 = 17.05 # MeV/c
    Ek = momentum2kinetic(P0); gamma_r = kinetic2gamma(Ek); bg_r = gamma2bg(gamma_r)
    nemit_x = nemit_y = 4e-6 # mm mrad
    emit_x = nemit_x/bg_r; emit_y = nemit_y/bg_r
    
    cor_Ekin = 0.5e-2*Ek*1e3 # keV
    sig_Ekin = 0.05e-2*Ek*1e3
    
    if x is not None:
        # sigma_x in mm, alpha_x dimensionless
        sigma_x, sigma_y, alpha_x, alpha_y = x
        print((sigma_x, sigma_y, alpha_x, alpha_y))
    else:
        sigma_x, sigma_y, alpha_x, alpha_y = 1.576772E+00,   1.775836E-01,   7.183778E+00,   1.716220E+00
    beta_x, beta_y = sigma_x**2/emit_x*1e-6, sigma_y**2/emit_y*1e-6
    gamma_x, gamma_y = (1.+alpha_x**2)/beta_x, (1.+alpha_y**2)/beta_y
    
    cor_px = -alpha_x/beta_x*sigma_x
    cor_py = -alpha_y/beta_y*sigma_y

    if len(args) > 0:
        execute = args[0]
    else:
        execute = 1
    
    if len(args) > 1:
        Ipeak = args[1]
    else:
        Ipeak = 4e-9/21.5e-12
    if len(args) > 2:
        Q_total = args[2]
    else:
        FWHM = 4;
    #ratio = (4e-9/21.5e-12)/Ipeak
    
    Lz = Q_total*1e-9/Ipeak*g_c*1e3; print(Lz)
    sig_z = Q_total*1e-9/Ipeak*g_c*1e3/np.sqrt(2.*np.pi)
    
    #Distribution = '../ast.2700.001'
    #if 'Distribution' in kv.keys():
    #    Distribution = kv['Distribution']
    
    Run = 1
    if 'Run' in list(kv.keys()):
        Run = kv['Run']
    
    Ipart = int(100000*Q_total)
    
    fname = './beam_%.0fA_%.1fnC.ini' % (Ipeak, Q_total)
    generator = Generator1(FNAME = fname, IPart = Ipart, Species = 'electrons', Q_total = Q_total,\
                          Ref_Ekin = Ek, sig_Ekin = sig_Ekin, cor_Ekin = cor_Ekin, Dist_pz = 'g', Cathode = False,\
                          Dist_z = 'p', Lz = Lz, rz = 1e-12*g_c*1e3,
                          Dist_x = 'g', sig_x = sigma_x, Dist_px = 'g', Nemit_x = nemit_x*1e6, cor_px = cor_px,\
                          Dist_y = 'g', sig_y = sigma_y, Dist_py = 'g', Nemit_y = nemit_y*1e6, cor_py = cor_py)
                          # \Dist_z = 'g', sig_z = ratio*21.5e-12*g_c*1e3/np.sqrt(2.*np.pi),\
    
    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = 'beam_und.ini',\
                    Auto_Phase = True, Track_All = True, Track_On_Axis = False, check_ref_part = False,\
                    Lprompt = False)
    newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, LSPCH3D = True, Nxf = 16, Nyf = 16, Nzf = 16,\
                    Max_scale = 0.05, Max_count = 20)
    
    cavity = Module('Cavity', LEfield = True, By_stat = [True], File_Efield = [field_maps+os.sep+'3Dund-15x5mm'],\
                    MaxE = [By_u_max], C_pos = [pos_u], Nue = [0], Phi = [0])
    
    output = Module('Output', Zstart = 0, Zstop = 3.6, Zemit = 180, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [1.8, 3.6])

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, output])

    #direc = str.format('n%.0fk-sig_x-%.2fmm-sig_y-%.2fmm-alp_x-%.2f-alp_y-%.2f' %\
    #                   (Ipart/1000., sigma_x, sigma_y, alpha_x, alpha_y))
    
    direc = 'dist'
    
    os.system('mkdir -p '+direc)
    os.chdir(direc)

    generator.write()
    #astra.write()
    
    if not execute:
        os.system('generator gen.in 2>&1 | tee gen.log')
        os.chdir('../')
        obj = -1
    else:
        os.system('generator gen.in 2>&1 | tee gen.log')
        os.system('astra ast.in 2>&1 | tee ast.log')

        try:
            fname = 'ast.0360.001'
            beam = np.loadtxt(fname)
            beam[1:,2] += beam[0,2]; beam[1:,5] += beam[0,5]
            diag = BeamDiagnostics(beam)

            # when there is no beam loss to the cathode or to the aperture -alpha_x*nemit_x
            if diag.loss_cathode<=50 and diag.loss_aperture<=50: 
                obj = np.sqrt(WEIGHTS[0]*np.fabs(diag.nemit_x-OBJECTS[0])+WEIGHTS[1]*np.fabs(diag.nemit_y-OBJECTS[1])+
                              WEIGHTS[2]*np.fabs(diag.std_x  -OBJECTS[2])+WEIGHTS[3]*np.fabs(diag.std_y  -OBJECTS[3])+
                              WEIGHTS[4]*np.fabs(-diag.alpha_x*diag.emit_x*1e6-OBJECTS[4])+
                              WEIGHTS[5]*np.fabs(-diag.alpha_y*diag.emit_y*1e6-OBJECTS[5]))
            else:
                obj = 1e3
        except:
            obj = 1e4
            print('Error: not accelerated well!')

        os.chdir('../')

        res = [Ipart, sigma_x, sigma_y, alpha_x, alpha_y, obj]
        with open('results.dat','a') as f_handle:
            np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj

def obj_und1(x = None, *args, **kwargs):
    '''
    Optimize the beam size and beam covariance, given the emittance.
    Parameter:
      x: an array or list of the variables to be optimized, sigma_x, sigma_y, alpha_x and alpha_y
    Return:
      energy: the quantity that is envisoned as the "energy" of the sample
    '''

    kv = kwargs
    
    N_u = 120; lam_u = 0.03; L_u = N_u*lam_u
    By_u_max = 1.28 # T
    pos_u = L_u/2.
    
    P0 = 17.05 # MeV/c
    Ek = momentum2kinetic(P0); gamma_r = kinetic2gamma(Ek); bg_r = gamma2bg(gamma_r)
    nemit_x = nemit_y = 4e-6 # mm mrad
    emit_x = nemit_x/bg_r; emit_y = nemit_y/bg_r
    sigma_Ek = 0.5e-2*Ek*1e3 # keV
    
    if x is not None:
        # sigma_x in mm, alpha_x dimensionless
        sigma_x, sigma_y, alpha_x, alpha_y = x
        print((sigma_x, sigma_y, alpha_x, alpha_y))
    else:
        sigma_x, sigma_y, alpha_x, alpha_y = 1.576772E+00,   1.775836E-01,   7.183778E+00,   1.716220E+00
    beta_x, beta_y = sigma_x**2/emit_x*1e-6, sigma_y**2/emit_y*1e-6
    gamma_x, gamma_y = (1.+alpha_x**2)/beta_x, (1.+alpha_y**2)/beta_y
    
    cor_px = -alpha_x/beta_x*sigma_x
    cor_py = -alpha_y/beta_y*sigma_y

    #OBJECTS[2], OBJECTS[3], OBJECTS[4], OBJECTS[5] = beta_x, beta_y, -alpha_x, -alpha_y
    
    WEIGHTS[0], WEIGHTS[1], WEIGHTS[2], WEIGHTS[3], WEIGHTS[4], WEIGHTS[5] = \
        0, 0, 5./55,   11./55,   5./55,                    11./55    
    OBJECTS[0], OBJECTS[1], OBJECTS[2], OBJECTS[3], OBJECTS[4], OBJECTS[5] = \
        0, 0, sigma_x, sigma_y, -(-alpha_x*nemit_x/bg_r), -(-alpha_y*nemit_y/bg_r)
    
    if len(args) > 0:
        execute = args[0]
    else:
        execute = 1
    
    if len(args) > 1:
        Ipeak = args[1]
    else:
        Ipeak = 4e-9/21.5e-12
    if len(args) > 2:
        FWHM = args[2]
    else:
        FWHM = 21.5;
    #ratio = (4e-9/21.5e-12)/Ipeak
    
    Q_total = -Ipeak*FWHM*1e-12*1e9 # nC
    Lz = -Q_total*1e-9/Ipeak*g_c*1e3; print(Lz)
    
    #Distribution = '../ast.2700.001'
    #if 'Distribution' in kv.keys():
    #    Distribution = kv['Distribution']
    
    Run = 1
    if 'Run' in list(kv.keys()):
        Run = kv['Run']
    
    Ipart = 1000000
    fname = '../beam_%.0fA_%.1fps.ini' % (Ipeak, FWHM)
    generator = Generator(FNAME = fname, IPart = Ipart, Species = 'electrons', Q_total = Q_total,\
                          Ref_Ekin = Ek, sig_Ekin = 0, cor_Ekin = sigma_Ek, Dist_pz = 'g', Cathode = False,\
                          Dist_z = 'p', Lz = Lz, rz = 2e-12*g_c*1e3,
                          Dist_x = 'g', sig_x = sigma_x, Dist_px = 'g', Nemit_x = nemit_x*1e6, cor_px = cor_px,\
                          Dist_y = 'g', sig_y = sigma_y, Dist_py = 'g', Nemit_y = nemit_y*1e6, cor_py = cor_py)
                          # \Dist_z = 'g', sig_z = ratio*21.5e-12*g_c*1e3/np.sqrt(2.*np.pi),\
    
    newrun = Module('Newrun', Run = 1, Head = 'PITZ beam line simulation', Distribution = 'beam_und.ini',\
                    Auto_Phase = True, Track_All = True, Track_On_Axis = False, check_ref_part = False,\
                    Lprompt = False)
    newrun.set(Run = Run)

    charge = Module('Charge', LSPCH = True, LSPCH3D = True, Nxf = 16, Nyf = 16, Nzf = 16,\
                    Max_scale = 0.05, Max_count = 20)
    
    cavity = Module('Cavity', LEfield = True, By_stat = [True], File_Efield = [field_maps+os.sep+'3Dund-15x5mm'],\
                    MaxE = [By_u_max], C_pos = [pos_u], Nue = [0], Phi = [0])
    
    output = Module('Output', Zstart = 0, Zstop = 3.6, Zemit = 180, Zphase = 1, RefS = True, EmitS = True,\
                    PhaseS = True, TrackS = False, LandFS = True, Screen = [1.8, 3.6])

    astra = Astra()
    astra.add_modules([newrun, charge, cavity, output])

    direc = str.format('n%.0fk-sig_x-%.2fmm-sig_y-%.2fmm-alp_x-%.2f-alp_y-%.2f' %\
                       (Ipart/1000., sigma_x, sigma_y, alpha_x, alpha_y))
    
    os.system('mkdir -p '+direc)
    os.chdir(direc)

    generator.write()
    astra.write()
    
    if not execute:
        os.system('generator gen.in 2>&1 | tee gen.log')
        os.chdir('../')
        obj = -1
    else:
        os.system('generator gen.in 2>&1 | tee gen.log')
        os.system('astra ast.in 2>&1 | tee ast.log')

        try:
            fname = 'ast.0360.001'
            beam = np.loadtxt(fname)
            beam[1:,2] += beam[0,2]; beam[1:,5] += beam[0,5]
            diag = BeamDiagnostics(beam)

            # when there is no beam loss to the cathode or to the aperture -alpha_x*nemit_x
            if diag.loss_cathode<=50 and diag.loss_aperture<=50: 
                obj = np.sqrt(WEIGHTS[0]*np.fabs(diag.nemit_x-OBJECTS[0])+WEIGHTS[1]*np.fabs(diag.nemit_y-OBJECTS[1])+
                              WEIGHTS[2]*np.fabs(diag.std_x  -OBJECTS[2])+WEIGHTS[3]*np.fabs(diag.std_y  -OBJECTS[3])+
                              WEIGHTS[4]*np.fabs(-diag.alpha_x*diag.emit_x*1e6-OBJECTS[4])+
                              WEIGHTS[5]*np.fabs(-diag.alpha_y*diag.emit_y*1e6-OBJECTS[5]))
            else:
                obj = 1e3
        except:
            obj = 1e4
            print('Error: not accelerated well!')

        os.chdir('../')

        res = [Ipart, sigma_x, sigma_y, alpha_x, alpha_y, obj]
        with open('results.dat','a') as f_handle:
            np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')

    return obj


# In[56]:



# Simple parameter scan
# var1 = np.linspace(-30, 30, 61) #np.array([350])
# var2 = np.linspace(10, 17, 71) #np.array([80000, 100000, 150000])
# combi = np.array([[v1, v2] for v1 in var1 for v2 in var2])
#
# for i in combi:
#     res = obj_fun(i)
#     with open('phi2_scan.dat','a') as f_handle:
#         np.savetxt(f_handle,np.atleast_2d(res),fmt='%14.6E')
#exit()

# Parameter scan with differential evolution
# from joblib import Parallel, delayed
#
# num_cores = 8
# results = Parallel(n_jobs=num_cores)(delayed(obj_fun)(i) for i in combi)
# results = np.array(results)
# np.savetxt('phi2_scan.dat', results, fmt = '%12.6E')
# print results
# exit()

# t1 = default_timer()
#
# num_cores = 8
# bounds = [(2, 5), (-15, 15), (-30, 30), (300, 400)]
# result = differential_evolution(obj_fun, bounds, strategy='best1bin', num_cores = num_cores)
# 
# t2 = default_timer()
#
# print 'Best solution is:', result.x, result.fun
# print 'Total time consumed: ', t2-t1, ' s'

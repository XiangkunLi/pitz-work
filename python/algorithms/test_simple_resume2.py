import sys
if sys.platform == "linux" or sys.platform == "linux2":
    sys.path.append(r'/afs/ifh.de/group/pitz/data/lixiangk/work/sync/python')
    rootdir = r'//afs/ifh.de/group/pitz/data/lixiangk/work'
elif sys.platform == "win32":
    sys.path.append(r'\\afs\ifh.de\group\pitz\data\lixiangk\work\sync\python')
    rootdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\work'
elif sys.platform == "darwin":
    print("OS X")
else:
    print("Unknown platform!")

#from my_object import *
from astra_modules import *

from platypus import *
#from platypus.mpipool import MPIPool
import sys
import logging

import timeit
import pickle
import numpy as np

import os,shutil

def twiss2rms(twiss, gamma):
    emit_x = twiss[0]/gamma
    emit_y = twiss[1]/gamma
    
    rms_x = np.sqrt(twiss[2]*emit_x)
    rms_y = np.sqrt(twiss[3]*emit_y)
    cov_x = -twiss[4]*emit_x
    cov_y = -twiss[5]*emit_y
    
    rms = [rms_x, rms_y, cov_x, cov_y]
    return rms

def rms2beta(rms, nemit, gamma):
    emit = nemit/gamma
    beta = rms**2/emit
    return beta
def beta2rms(beta, nemit, gamma):
    emit = nemit/gamma
    rms = np.sqrt(beta*emit)
    return rms
    
def cov2alpha(cov, nemit, gamma):
    emit = nemit/gamma
    alpha = -cov/emit
    return alpha
def alpha2cov(alpha, nemit, gamma):
    emit = nemit/gamma
    cov = -alpha*emit
    return cov

particle = 'ast.2550.001'
diag = BeamDiagnostics(fname = particle, energy = True)
gamma0 = diag.Ekin/g_mec2+1; print(gamma0)

nemit_x, nemit_y, rms_x, rms_y, alp_x, alp_y = 4.0, 4.0, 1.25, 0.30, 7.0273, 3.50
beta_x = rms2beta(rms_x, nemit_x, gamma0)
beta_y = rms2beta(rms_y, nemit_y, gamma0)
twiss0 = [nemit_x, nemit_y, beta_x, beta_y, alp_x, alp_y]
opt = twiss2rms(twiss0, gamma0)
print(twiss0)
print(opt)
exit()

def run_sco(grads, particle = None):

    x = grads

    if particle is None:
        particle = 'ast.2550.001'

    direc = ''
    for i in np.arange(len(x)):
        direc += str.format('Q%d-%.2fT_m-' %  (i+1, x[i]))
    direc = direc[:-1]; print(direc)

    cmd = r'mkdir %s' % direc; print(cmd)
    os.system(cmd)

    os.chdir(direc)

    beamline = '''O    0.816250    0.000000    0.000000    0.000000     0
Q    0.067500    '''+str.format('%.6f' % x[0])+'''    0.000000    3.000000     0
O    0.332500    0.000000    0.000000    0.000000     0
Q    0.067500    '''+str.format('%.6f' % x[1])+'''   -3.000000    0.000000     0
O    0.332500    0.000000    0.000000    0.000000     0
Q    0.067500    '''+str.format('%.6f' % x[2])+'''    0.000000    3.000000     0
O    0.266250    0.000000    0.000000    0.000000     0
'''
    
    optics = 'beamline.txt'
    ff = open(optics, 'w')
    ff.write(beamline)
    ff.close()
    
    shutil.copyfile('..'+os.sep+'optimizer.dat', 'optimizer.dat')
    
    #shutil.copyfile('..'+os.sep+optics, optics)
    #shutil.copyfile('..'+os.sep+particle, particle)
    
    output = optics+'''
..'''+os.sep+particle
    
    filename = 'inputs.txt'
    ff = open(filename, 'w')
    ff.write(output)
    ff.close()
    
    cmd = 'sco < inputs.txt'
    os.system(cmd)

    diag = np.loadtxt('BeamDynamics.dat')

    nemit_x = diag[-1,1]
    nemit_y = diag[-1,2]
    beta_x = diag[-1,6]
    beta_y = diag[-1,7]
    alpha_x = diag[-1,8]
    alpha_y = diag[-1,9]

    twiss = [nemit_x, nemit_y, beta_x, beta_y, alpha_x, alpha_y]
    res = twiss2rms(twiss, gamma0)
    
    cmd = 'del BeamDynamics.dat'
    os.system(cmd)
    
    os.chdir('..')
    
    return res

#run_sco([1, 2, -1], 'ast.2550.001')
#exit()

#exit()

def obj_run_sco(x):
    
    res =  run_sco(x)
    obj = [ np.abs(1-res[i]/opt[i]) for i in np.arange(len(opt)) ]

    r = list(x) + res + obj
    with open('results.dat','a') as f_handle:
        np.savetxt(f_handle, np.atleast_2d(r), fmt='%14.6E')

    return obj

#obj = obj_run_sco([0.86, -1.95, 1.75]); print(obj)
#exit()

class GeneratorFromFile(Generator):
    
    def __init__(self, filename, usecols = None):
        super(GeneratorFromFile, self).__init__()
        self.x = np.loadtxt(filename, usecols = usecols)
        self.NCOUNT = 0
    def generate(self, problem):
        solution = Solution(problem)
        
        nvars = problem.nvars
        nobjs = problem.nobjs
        
        solution.variables =  self.x[self.NCOUNT, 0:nvars]
        #solution.objectives = self.x[self.NCOUNT, nvars:nvars+nobjs]

        #solution.feasible = True
	#solution.evaluated = False
        
        self.NCOUNT += 1
        
        return solution


class NSGAII_resume(AbstractGeneticAlgorithm):
    
    def __init__(self, problem,
                 population_size = 100,
                 generator = RandomGenerator(),
                 selector = TournamentSelector(2),
                 variator = None,
                 archive = None,
                 nth_run = 1,
                 **kwargs):
        super(NSGAII_resume, self).__init__(problem, population_size, generator, **kwargs)
        self.selector = selector
        self.variator = variator
        self.archive = archive
        self.nth_run = nth_run
        
    def step(self):
        if self.nfe == 0:
            self.initialize()
        else:
            self.iterate()

        if self.archive is not None:
            self.result = self.archive
        else:
            self.result = self.population

        self.dump()

    def dump(self):

        ### save the current population
        current = []
        for k in np.arange(len(self.population)):
            r_vars = self.population[k].variables[:]
            r_objs = self.population[k].objectives[:]
            current.append(list(r_vars)+list(r_objs))
        with open('population@%04d.%03d' % (self.nfe, self.nth_run), 'w') as f_handle:
            print(self.nfe, np.atleast_2d(current)[-1])
            np.savetxt(f_handle, np.atleast_2d(current), fmt='%14.6f')
        ###

        ### print/save the current result, added on September 13, 2019
        current = []
        for k in np.arange(len(self.result)):
            r_vars = self.result[k].variables[:]
            r_objs = self.result[k].objectives[:]
            current.append(list(r_vars)+list(r_objs))
        with open('result@%04d.%03d' % (self.nfe, self.nth_run), 'w') as f_handle:
            print(self.nfe, np.atleast_2d(current)[-1])
            np.savetxt(f_handle, np.atleast_2d(current), fmt='%14.6f')
        ###
        
        ### save the attributes except evaluator
        fullname = 'attribute@%04d.pkl' % self.nfe
        att = self.__dict__.copy()
        att['evaluator'] = []
        #print('att is: ', att)
        with open(fullname, 'wb') as f_handler:
            pickle.dump(att, f_handler)
        ###
        
        ### save the algorithm w/o evaluator
        evaluator = self.evaluator
        self.evaluator = None
        
        fullname = 'algorithm@%04d.%03d.pkl' % (self.nfe, self.nth_run)
        with open(fullname, 'wb') as f_handler:
            pickle.dump(self, f_handler)
        
        self.evaluator = evaluator

    def initialize(self):
        super(NSGAII_resume, self).initialize()
        
        if self.archive is not None:
            self.archive += self.population
        
        if self.variator is None:
            self.variator = default_variator(self.problem)

    def initialize_to_resume(self, generator = None):
        if generator is None:
            generator = self.generator
        
        self.population = [generator.generate(self.problem) for _ in range(self.population_size)]

        self.evaluate_all(self.population)
        
        if self.archive is not None:
            self.archive += self.population
        
        if self.variator is None:
            self.variator = default_variator(self.problem)

        self.dump()

    def iterate(self):
        offspring = []
        
        while len(offspring) < self.population_size:
            parents = self.selector.select(self.variator.arity, self.population)
            offspring.extend(self.variator.evolve(parents))

        #retrieve the simulation manually
        #offspring = [self.generator.generate(self.problem) for _ in range(self.population_size)]
        
        self.evaluate_all(offspring)

        ### print/save the current offspring, added on September 23, 2019
        import numpy as np
        current = []
        for k in np.arange(len(offspring)):
            r_vars = offspring[k].variables[:]
            r_objs = offspring[k].objectives[:]
            current.append(list(r_vars)+list(r_objs))
        with open('offspring@%04d.%03d' % (self.nfe, self.nth_run), 'w') as f_handle:
            print(self.nfe, np.atleast_2d(current)[-1])
            np.savetxt(f_handle, np.atleast_2d(current), fmt='%14.6f')
        ###
        
        offspring.extend(self.population)
        nondominated_sort(offspring)
        self.population = nondominated_truncate(offspring, self.population_size)

        
        if self.archive is not None:
            self.archive.extend(self.population)

def initialize_algorithm(fullname):
    #fullname = 'algorithm@0095.001.pkl'
    with open(fullname, 'rb') as f_handle:
        algo = pickle.load(f_handle)

    print(algo.__dict__)
    return algo

logging.basicConfig(level=logging.INFO)

if __name__ == "__main__":
    # define the problem definition
    problem = Problem(3, 4)
    problem.types[0] = Real(0, 1.5)
    problem.types[1] = Real(-3, 0)
    problem.types[2] = Real(0, 3)
    problem.function = obj_run_sco

    #from platypus import NSGAII, DTLZ2
    #problem = DTLZ2()

    # define the generator for initialization
    #generator_init = GeneratorFromFile('result@.003') # Continue after the first run
    
    
    time1 = timeit.default_timer()
    
    
    algorithm = NSGAII_resume(problem, population_size = 100)
    #algorithm.initialize_to_resume(generator_init)
    
    #algorithm = initialize_algorithm('algorithm@5016.001.pkl')
    #algorithm.evaluator = evaluator
    from platypus.config import PlatypusConfig
    #algorithm.evaluator = PlatypusConfig.default_evaluator

    algorithm.nth_run = 1

    #retrieve the optimization process
    #algorithm.generator = generator_init

    #algorithm.dump()
    
    algorithm.run(5000)
    
    # display the results
    for solution in algorithm.result:
        print(solution.objectives)


    time2 = timeit.default_timer()
    print('time elapsed: ', time2-time1)

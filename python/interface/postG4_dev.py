# -*- coding: utf-8 -*-
"""
Created on Fri Jun 26 14:57:04 2020

@author: lixiangk
"""

from universal import *
import h5py

from numpy.fft import fftshift,fft

def calcSpectrum(amp, phase, lambda0 = 100e-6, freq0 = None):
    '''
    Calculate the spectrum from samples

    Parameters
    ----------
    amp : 1D array
        Amplitudes of samples of the signal.
    phase : 1D array
        Phases of samples of the signal.
    lambda0 : double, optional
        Seperation (or wavelength) of sampling in meter. The default is 100e-6.
    freq0 : double, optional
        Sampling frequency. If defined, it dominates `lambda0`. The default is None.

    Returns
    -------
    wavelength : 1D array
        Wavelength of the signal transformed in frequency domain.
    spectrum : 1D array
        Spectra intensity of the signal transformed in frequency domain.

    '''
    
    #amp = self.data[:, col, 2]
    #phase = self.data[:, col, 3]
    
    nsample = len(amp) # number of samples
    signal = np.zeros(nsample, dtype = complex)
    
    for i in np.arange(nsample):
        signal[i] = np.sqrt(amp[i])*np.complex(np.cos(phase[i]), np.sin(phase[i]))
    
    spectrum = np.abs(fftshift(fft(signal, nsample)))
    spectrum = spectrum*spectrum
    
    if freq0 is None and lambda0 is not None:
        freq0 = 1./lambda0*g_c # sampling frequency
    
    F = 1.0*np.arange(-nsample/2, nsample/2,)/nsample*freq0+freq0 # Frequency
    wavelength = g_c/F # Frequency to wavelength
    
    return wavelength, spectrum
    
class PostGenesis:
    version = 4
    def __init__(self, fname = None, **kwargs):
        '''
        
        Parameters
        ----------
        fname : str
            Genesis V4 main output file.
        **kwargs : TYPE
            DESCRIPTION.
            
        Returns
        -------
        None.

        '''
        if fname is None:
            fname = get_file('.h5'); print(fname)
        
        _, _, ext = fileparts(fname); print(ext)
        
        if ext.upper() in ['.H5']:
            self.version = 4
            self.load4(fname)
        elif ext.upper() in ['.OUT']:
            self.version = 2
            self.load2(fname)    
        else:
            print('Unknown file extension!')
            return
        
        debug = 0
        if len(kwargs)>0:
            if 'debug' in kwargs.keys():
                debug = kwargs['debug']    
            
        if debug:  
            self.plot_current()
            self.plot_power()
            self.plot_energy()
            self.plot_spectrum()
        
    def print_all(self):
        if self.version == 2:
            print('Only work for version 2!')
            return
        
        print('./')
        for key in self.file.keys():
            print('  - %s' % key)
            for subkey in self.file.get(key).keys():
                print('    - %s' % subkey)
        
    def load4(self, fname):
        file = h5py.File(fname, 'r')
        
        tmp = {}
        for k, v in file.get('Beam').items():
            tmp.update({k:v[:]})
        self.beam = tmp
        
        tmp = {}
        for k, v in file.get('Field').items():
            tmp.update({k:v[:]})
        self.field = tmp
        
        nstep, nslice = file.get('Field').get('power').shape
        
        meta = file.get('Meta').get('InputFile')[0].decode().split()
        
        kv = {}
        for _, a in enumerate(meta):
            tmp = a.split('=')
            if len(tmp)>1:
                kv.update({tmp[0]:tmp[1]})
                
        self.file = file
        self.nslice = nslice
        self.nstep = nstep
        self.kv = kv
        
        self.sample = file.get('Global/sample')[0]
        self.lambdaref = file.get('Global/lambdaref')[0]
        self.gamma0 = file.get('Global/gamma0')[0]
        self.one4one = file.get('Global/one4one')[0]
        self.scan = file.get('Global/scan')[0]
        self.slen = file.get('Global/slen')[0]
        self.time = file.get('Global/time')[0]
        
        lslice = self.lambdaref/self.sample
        
        self.lslice = lslice
        #self.zbunch = np.linspace(lslice/2, self.slen-lslice/2, nslice)
        self.zbunch = np.arange(nslice)*self.lslice+self.lslice/2
        self.current = file.get('Beam/current')[:]
        
        #self.bunching = file.get('Beam/bunching')
        
        self.zplot = file.get('Lattice/zplot')[:]
        self.zstep = self.zplot[1]-self.zplot[0]
        
        self.power_z = np.sum(file.get('Field/power')[:], axis = 1)
        self.energy_z = self.power_z*self.lslice/g_c
    load = load4
    
    def load2(self, fname):
        
        '''
        Read the standard output (in raw format and stored in `fname`) 
        into memories for further analysis
        '''
        
        file = open(fname, 'r')
        line = file.readline()

        kv = {}
        while True:
            if not line:
                break

            match = re.search(r'^[^=]*=[^=]*$' , line)
            if match:
                # print line
                key, value = line.split('=')
                match = re.search(r'file', key)
                if match:
                    kv.update({key.strip():value})
                    line = file.readline()
                    continue

                if len(value.split()) == 1:
                    value = np.float(value.replace("D", "E"))
                else:
                    value = [np.float(v.replace("D", "E")) for v in value.split()]
                kv.update({key.strip():value})          

            match = re.search(r'^[ ]*z\[m\][ ]*aw[ ]*qfld[ ]*$', line)
            if match:
                # print line
                nstep = np.int(kv['zstop']/kv['delz']/kv['xlamd'])+1
                field = np.zeros((nstep, 3))
                for i in np.arange(nstep):
                    line = file.readline()
                    field[i] = np.array([np.float(v.replace("D", "E")) for v in line.split()])
                break
            line = file.readline()

        nslice = np.int(kv['nslice'])
        nc = np.int(np.sum(kv['lout'])); #print nc # number of output items

        current = np.zeros((nslice, 1))
        data = np.zeros((nslice, nstep, nc)); #print data.shape

        islice = 0
        while True:
            if not line:
                break
            match = re.search(r'[ E]* current', line)
            if match:
                icurrent, tmp = line.split()
                icurrent = np.float(icurrent)
                current[islice, 0] = icurrent
        
                line = file.readline()
                line = file.readline()
                line = file.readline()

                outputs = line.split()

                for j in np.arange(nstep):
                    line = file.readline(); #print line
                    line = line.replace('NaN', '  0')
                    data[islice, j] = np.array([np.float(v.replace("D", "E")) for v in line.split()])
                #break
                islice += 1
            line = file.readline()
            
        file.close()
        
        self.kv = kv
        self.nslice = nslice; self.nstep = nstep; self.nc = nc
        self.field = field
        
        self.data = data
        
        self.gamma0 = self.kv['gamma0']
        self.time = self.kv['itdp']
        self.lambdaref = self.kv['xlamds']
        self.sample = 1.0/self.kv['zsep']
        
        self.lslice = self.lambdaref/self.sample # number of slices
        
        self.zbunch = np.arange(nslice)*self.lslice+self.lslice/2
        self.current = current
        
        self.zplot = field[:,0]; self.zstep = self.zplot[1]-self.zplot[0]
        
        self.power_z = self.data[:,:,0].sum(axis = 0)
        self.energy_z = self.power_z*self.lslice/g_c
        
        self.wavelength, self.spectrum = self.get_spectrum_at()
        return
    
    def get_spectrum_at(self, z = None):
        '''
        Get the radiation spectrum at position z
        Parameters
          z: longitudinal position along the undulator, used to calculate the
          nth step of interest
        Returns
        -------
        wavelength : 1D array
            Wavelength of the signal transformed in frequency domain.
        spectrum : 1D array
            Intensity of the signal transformed in frequency domain.
        '''
        
        if z == None:
            z = self.zplot[-1]
        col = np.int(z/self.zstep)
        
        if self.version<4:
            amp = self.data[:, col, 2]
            phase = self.data[:, col, 3]
        else:
            amp = self.file.get('Field/intensity-nearfield')[:][col]
            phase = self.file.get('Field/phase-nearfield')[:][col]
        
        return calcSpectrum(amp, phase, self.lslice)
    
    def plot_current(self):
        
        fig, ax = plt.subplots()
        ax.plot(self.zbunch/g_c*1e12, self.current, '-')
        ax.set_xlabel(r'Time (ps)')
        ax.set_ylabel(r'Current (A)')
        ax.grid()
    
    def plot_spectrum(self):
        
        fig, ax = plt.subplots()
        ax.plot(self.wavelength*1e6, self.spectrum, '-')
        ax.set_xlabel(r'Wavelength ($\mu$m)')
        ax.set_ylabel(r'Intensity (arb. unit)')
        ax.grid()
        
    def plot_power(self):
        
        fig, ax = plt.subplots(ncols = 1, figsize = (4, 4))
        ax.plot(self.zplot, self.power_z/1e6, '-')
        ax.set_xlabel(r'$z$ (m)')
        ax.set_ylabel(r'Power (MW)')
        ax.set_yscale('log')
        ax.grid()
        
    def plot_energy(self):
        
        fig, ax = plt.subplots(ncols = 1, figsize = (4, 4))
        ax.plot(self.zplot, self.energy_z*1e6, '-')
        ax.set_xlabel(r'$z$ (m)')
        ax.set_ylabel(r'Energy ($\mu$J)')
        ax.set_yscale('log')
        ax.grid()
        
    def get_data(self, name):
        try:
            data = self.file[name][:]
        except Exception as err:
            print(err)
            data = None
        return data
    
#%%
tmp = {}
for k, v in f.get('Field').items():
    tmp.update({k:v[:]})
field = tmp
#%%
workdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim\2020\Genesis-demo\feedback2'
os.chdir(workdir)

pg4 = PostGenesis('test.out.h5')

#%%

    
f = h5py.File('test.out.h5', 'r')
f.keys()
#%%
f = h5py.File('test.200.par.h5', 'r')

slice = f['slice000001']

current = slice.get('current')[0]
print('The current is ', current)

x = slice.get('x')[:]
y = slice.get('y')[:]
px = slice.get('px')[:]
py = slice.get('py')[:]
theta = slice.get('theta')[:]
gamma = slice.get('gamma')[:]

# fig, [ax, ax2] = plt.subplots(ncols = 2, figsize = (8 ,4))
# ax.plot(x, px, '.')
# ax.plot(y, py, '.')
# ax.grid()

ax2.plot(theta, gamma, '.')
ax2.grid()

#%%

plt.figure()
plt.xlabel('theta')
plt.ylabel('counts')

for step in np.linspace(0, 240, 240//5+1):
    f = h5py.File('test.%d.par.h5' % step, 'r')

    slice = f['slice000001']
    theta = slice.get('theta')[:]

    plt.hist(np.mod(theta, 2*np.pi), 100, histtype = r'step')
    plt.xlim(-30, 30)
    
    plt.pause(0.1)
    if step<240:
        plt.cla()


#%%
#f = h5py.File('../Benchmark.out.h5', 'r')
f = h5py.File('test.out.h5', 'r')

beam = f.get('Beam')

xsize = beam.get('xsize')[:]
ysize = beam.get('ysize')[:]
current = beam.get('current')[:]
bunching = beam.get('bunching')[:]

lattice = f.get('Lattice')
z = lattice.get('z')[:]
aw = lattice.get('aw')[:]

fig, [ax1, ax2, ax3] = plt.subplots(ncols = 3, figsize = (12, 4))

ax1.plot(z, aw, '-o')
ax1.set_xlabel(r'$z$ (m)')
ax1.set_ylabel(r'$a_W$')
ax1.grid()

ax2.plot(xsize, '-')
ax2.plot(ysize, '-')
ax2.set_xlabel(r'$z$ (m)')
ax2.set_ylabel(r'RMS size (mm)')
ax2.legend(['x', 'y'])
ax2.grid()

#ax3.plot(current, '-')
ax3.plot(bunching, '-')
ax3.set_xlabel(r'$z$ (m)')
ax3.set_ylabel(r'bunching')
ax3.grid()

fig.tight_layout()
fig.savefig('test.png')


#%% Field
f = h5py.File('test.out.h5', 'r')

Field = f.get('Field')
power = Field.get('power')[:]

lam_s = 100e-6
energy = np.sum(power, axis = 1)*lam_s/g_c

zpos = np.linspace(0, 3.6, power.shape[0])

fig, ax = plt.subplots()
ax.plot(zpos, energy*1e6, '-o')
ax.set_xlabel(r'$z$ (m)')
ax.set_ylabel(r'Energy ($\mu$J)')
ax.set_yscale('log')

#%%
tmp = f.get('Beam')
for key in list(tmp.keys()):
    try:
        print(key, ': ', tmp.get(key)[:].T)
    except Exception as err:
        print(key, ': ')
        pass
    
#%%
betax = 20.772
betay = 0.270
alphax = 7.180000
alphay = 1.720000

print(233.52666069/alphax, 57.17006118/alphay, 20.30348682/betax, 0.26965361/betay)


#%%
workdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim\2020\Warp'
os.chdir(workdir)

beam1 = pd_loadtxt('beam_und_250k_4_shot.ini')

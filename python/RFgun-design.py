# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28 16:24:18 2021

@author: lixiangk
"""

from universal import *

def power_dissipation(t, beta = 1, tau = 1, Pin = 1):
    
    Pc = Pin*(1-((1-np.exp(-t/tau))*2*beta/(1+beta)-1)**2)
    
    return Pc

t = np.linspace(0, 100, 1000)

fig, ax = plt.subplots(ncols = 2, figsize =(8, 4))

Pc = power_dissipation(t, beta = 0.9)
ax[0].plot(t, Pc, '-')
ax[1].plot(t, 1-Pc, '-')

Pc = power_dissipation(t, beta = 1)
ax[0].plot(t, Pc, '-')
ax[1].plot(t, 1-Pc, '-')

Pc = power_dissipation(t, beta = 1.1)
ax[0].plot(t, Pc, '-')
ax[1].plot(t, 1-Pc, '-')

for axis in ax:
    axis.set_ylim(0, 1.1)
    axis.grid()


#%% Qe and Q0 vs L_cp, W_cp = 15
# position of WG defined with R2: (R2+2)
data = np.array([[20,	8369.1989887538,    1.29897990481255e+04], 
                 [21,	7478.0190492756,    1.18518064594247e+04],
                 [22,	7303.0497278313,    1.07670078471003e+04], 
                 [23,	7800.2244556743,    9910.90579135 ],
                 [24,	8908.9561872788,    9339.5309231504],
                 [24.2, 9208.7415835439,    9254.970952321],
                 [25,	1.05155260981332e+04,   8992.1311369036]])

# fix the position of WG: 44+3
data = np.array([[20,	5961.2107757538, 1.562199644314e+04],
                 [22,	4046.197806031, 1.33321326342881e+04],
                 [24,	4130.0622397805, 1.03239442488839e+04],
                 [26,	6774.1351179202, 8970.2204971197],
                 [26.5,	7796.8163487908,  8833.0297817446],
                 [26.7,	8236.4103889899,  8788.1306517032],
                 [26.9,	8636.1276628085,  8754.2740699782],
                 [27.1,	9084.6349416168,  8723.0763150303],
                 [27.3,	9555.1783485008,  8693.7254085847],
                 [27.5,	1.00284584024838e+04,  8668.2056062071],
                 [28,	1.1195262442594e+04, 8616.3410112547],
                 [30,	1.56890971583942e+04, 8507.7042859513]])

fig, ax = plt.subplots()
ax.plot(data[:,0], data[:,1])
ax.plot(data[:,0], data[:,2])
ax.grid()

# The problem of the above tuning is that the resonant frequency and the
# coupling coefficient are correlated.
# After tuning of coupling coefficient, the resonant frequency drops ~3MHz 
# because of the openning.
# If reducing the cavity radius, the resonant frequency increases slowly,
# with the price of a big drop of Qext
#%% freq vs R2
data = np.array([[43.8,	2878.8151073463],
                 [43.9,	2871.7811027405],
                 [44	,   2864.9081076562],
                 [44.1,	2858.430886833],
                 [44.15,	2855.74076277],
                 [44.2,	2853.9825190994]])

func = lambda x, a, b: a+b*x
popt, pcov = curve_fit(func, data[3:,0], data[3:,1])

fig, ax = plt.subplots()
ax.plot(data[:,0], data[:,1], '*')
xx = np.linspace(43.8, 44.2)
ax.plot(xx, func(xx, *popt,), '-')
ax.grid()

#%% Use tappered waveguide.
# The idea is that with the tapered WG, the coupling slot could be much smaller
# and therefore has less effect on the resonant frequency

# Scan length of the coupling slot
data = np.array([[16,	2.46371518607497e+05, 1.30511240561684e+04],
                 [17,	1.69518325595055e+05, 1.2681352011696e+04],
                 [18,	1.21359133113956e+05, 1.22660513426871e+04],
                 [19,	9.06676363501496e+04, 1.18329584400419e+04],
                 [20,	7.05687049488089e+04, 1.13826954003707e+04]])

# Scan width of the slot
data = np.array([[8,	7.05687049487933e+04, 1.13826954003815e+04],
                 [9,  	6.49207561895533e+04, 1.10083675442159e+04],
                 [10,	6.14881238805858e+04, 1.06735864929329e+04],
                 [11,	5.95564181454851e+04, 1.03892471457756e+04],
                 [12,	5.8723840185781e+04, 1.01430843687571e+04]])

fig, ax = plt.subplots()
ax.plot(data[:,0], data[:,1])
ax.plot(data[:,0], data[:,2])
ax.grid()

#%% Then it's found that the WG length shouldn't be arbitrary, otherwise, it
# is still too sensitive for one parameter when tuning the other
# The WG length is optimized in such a way that the S11 at the input port is 0
# while the other end of the WG is set as an output port


# Scan width of the slot
data = np.array([[10,	1.64377449759239e+04,	1.44956132873972e+04],
                [11,	1.48486198536304e+04,	1.41337444169226e+04],
                [12,	1.37114743299081e+04,	1.37744762939447e+04],
                [13,	1.28864524649972e+04,	1.33764376151424e+04],
                [14,	1.23867702273709e+04,	1.30181530678406e+04],
                [15,	1.21010215722441e+04,	1.26316518391751e+04]])

fig, ax = plt.subplots()
#ax.plot(data[:,0], data[:,1])
#ax.plot(data[:,0], data[:,2])
ax.plot(data[:,0], data[:,2]/data[:,1])
ax.grid()

#%% For each W_cp, the cavity frequency is tuned at 2856 MHz
# note that the loaded frequency is slightly smaller
# L_cp, W_cp, H_cp, fL, f0, Qe, Q0, Q0/Qe, fr
data = np.array([[20, 11.5, 5, 44.2707, 2855.91, 2856.02, 14613.5, 13731, 1.04, 0.94],
                 [20, 12.0, 5, 44.2668, 2855.91, 2856.02, 13614.7, 13752, 1.00, 1.01],
                 [20, 12.5, 5, 44.2641, 2855.88, 2856.00, 13017.1, 13738, 1.04, 1.06],
                 [20, 13.0, 5, 44.2605, 2855.88, 2856.01, 12334.3, 13751, 1.05, 1.11],
                 [20, 13.5, 5, 44.2575, 2855.87, 2856.00, 11776.1, 13750, 1.04, 1.17]])
#                 [20, 13.0, 5, 44.2582, 2855.99, 2856.12, 12132.7, 13879, 1.07, 1.14],
fig, ax = plt.subplots()
#ax.plot(data[:,0], data[:,1])
#ax.plot(data[:,0], data[:,2])
ax.plot(data[:,1], data[:,-1])
ax.grid()
ax.set_xlabel(r'width of coupling slot (mm)')
ax.set_ylabel(r'coupling coefficient')
fig.savefig('beta-vs-coupling-slot-width.png')

#%% For normal WG
#[20, 14.0, 5, 44.2502, 2856.07, 2856.00, 21967.3, 13292, 0.96, 0.61],
                 
data = np.array([[20, 15.0, 5, 44.2438, 2855.92, 2856.01, 19205.0, 13745, 1.05, 0.72],
                 [21, 15.0, 5, 44.2288, 2855.89, 2856.00, 14113.2, 13727, 1.05, 0.97],
                 [21.5, 15, 5, 44.2211, 2855.86, 2855.99, 12179.9, 13724, 1.04, 1.13],
                 [22, 15.0, 5, 44.2128, 2855.84, 2855.99, 10538.9, 13711, 1.04, 1.30]])

fig, ax = plt.subplots()
#ax.plot(data[:,0], data[:,1])
#ax.plot(data[:,0], data[:,2])
ax.plot(data[:,0], data[:,-1])
ax.grid()
ax.set_xlabel(r'length of coupling slot (mm)')
ax.set_ylabel(r'coupling coefficient')
#fig.savefig('beta-vs-coupling-slot-length.png')
#%%

S11 = np.loadtxt('S11.dat', skiprows = 24)
S21 = np.loadtxt('S21.dat', skiprows = 24)
S11_2 = S11[:,1]**2+S11[:,2]**2
S21_2 = S21[:,1]**2+S21[:,2]**2

plt.figure()
#plt.plot(S11_2)
plt.plot(S21[:,0], S21_2**2)


#%% For normal WG and after changing RF design for water cooling channels
data = np.array([[22, 16,   8, 44.0589, 2856.05, 2856, 22325.3, 12460, 1.03, 0.558],
                 [23, 16,   8, 44.0225, 2856.08, 2856, 16170.3, 12442, 1.03, 0.769],
                 [24, 16,   8, 43.9835, 2856.10, 2856, 11937.5, 12421, 1.03, 1.041],
                 [24.2, 16, 8, 43.9835, 2856.10, 2856, 11243.2, 12411, 1.03, 1.104],
                 [24.5, 16, 8, 43.9629, 2856.12, 2856, 10288.8, 12409, 1.03, 1.206]])

fig, ax = plt.subplots()
#ax.plot(data[:,0], data[:,1])
#ax.plot(data[:,0], data[:,2])
ax.plot(data[:,0], data[:,-1])
ax.grid()
ax.set_xlabel(r'length of coupling slot (mm)')
ax.set_ylabel(r'coupling coefficient')
fig.savefig('beta-vs-coupling-slot-length-with-dummy-port.png')
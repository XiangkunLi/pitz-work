from astra_modules import *
#from my_object import *
from FEL.basic import *
import pickle

simdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim'

#%%
workdir = os.path.join(simdir,r'2020',  r'Warp', r'32x2x2_1', r'lab_diags')
workdir = r'D:\PITZ\2020\Warp\24x2x2_8\lab_diags'
#workdir = r'D:\PITZ\2019\Warp3\16x1x1_4\diags'
os.chdir(workdir)

xmax, ymax = 5.5e-3, 2.5e-3
xmin, ymin = -xmax, -ymax

it0, it1 = 0, 31
write_step = 1
write_format = 'pickle'
#%% Density modulation
if write_format == 'hdf5':
    from opmd_viewer.openpmd_timeseries import OpenPMDTimeSeries
    from opmd_viewer.addons import LpaDiagnostics
    import h5py

    ts = OpenPMDTimeSeries('./hdf5/', check_all_files=False)

for it in np.arange(it0, it1, 1):
    it *= write_step
    if write_format == 'pickle':
        fullname = os.path.join('data', 'data-%d.pkl' % it)
        print(fullname)
        with open(fullname, 'rb') as f_handle:
            pkl = pickle.load(f_handle)
        beam = pkl['beam']
        z = beam[:,2]
    elif write_format == 'ascii': 
        z = np.loadtxt(os.path.join(r'data', 'beam-%d.dat' % it), usecols = (2))
        # z1 = np.loadtxt(os.path.join(simdir, r'2019', r'Warp3', r'32197', r'data', 'beam-%d.dat' % it), usecols = (2))
    elif write_format == 'hdf5':
        z = ts.get_particle(var_list=['z'], iteration=it, species='beam', plot=False)
        z = z[0]/1e6
            
    fig, [ax, ax2] = plt.subplots(nrows = 2, figsize = (6, 6))
    r = ax.hist((z-np.mean(z))*1e3, bins = 2000, histtype = r'step')
    #r = ax.hist((z1-np.mean(z1))*1e3, bins = 2000, histtype = r'step', ls = '-')
    ax.grid()
    ax.set_xlabel(r'$z$ (mm)')
    ax.set_ylabel(r'Intensity (arb. unit)')
    #ax.set_xlim(-5, 5)
    #ax.set_ylim(0, 320)
    ax.set_title(r'$z = %.2f$ m' % np.mean(z))
    ax.legend(['$\delta z = \lambda_s/4$', '$\delta z = \lambda_s/16$'], loc = 'upper left')
    #fig.savefig('z-hist2-'+`it`+'.eps')

    #fig, ax = plt.subplots(figsize = (6, 3))
    r = ax2.hist((z-np.mean(z))*1e6, bins = 2000, histtype = r'step')
    #r = ax2.hist((z1-np.mean(z1))*1e6, bins = 2000, histtype = r'step', ls = '-')
    ax2.grid()
    ax2.set_xlabel(r'$z$ ($\mu$m)')
    ax2.set_ylabel(r'Intensity (arb. unit)')
    ax2.set_xlim(0, 1000)
    #ax2.set_ylim(0, 320)
    #ax2.set_title(r'$z = %.2f$ m' % np.mean(z))
    #ax2.legend(['$\delta z = \lambda_s/4$', '$\delta z = \lambda_s/16$'], loc = 'upper left')
    fig.tight_layout()
    fig.savefig('z-hist2-all-%d.png' % it, density = 300)

#%% Spectrum
write_format = 'pickle'
for it in np.arange(it0, it1, 100):
    it *= write_step
    it = 29
    
    if write_format == 'pickle':
        fullname = os.path.join('data', 'data-%d.pkl' % it)
        with open(fullname, 'rb') as f_handle:
            pkl = pickle.load(f_handle)
        beam = pkl['beam']
        data = pkl['Ex']
        it, time, zmin, zmax, xmin, xmax = pkl['extent'][:]
        
        z1 = beam[:,2]

    elif write_format == 'ascii': 
        data = np.loadtxt('Ex-%d.dat' % it)
        
        it, time, zmin, zmax, xmin, xmax = np.loadtxt('extent.dat')[it,:]
        
        z1 = np.loadtxt('beam-%d.dat' % it, usecols = (2))
    
    Nx, Nz = data.shape; print(Nx, Nz)
    print(zmin, zmax, xmin, xmax)
    
    xx = np.linspace(xmin, xmax, Nx)
    zz = np.linspace(zmin, zmax, Nz)
    #zz = zz-zz[0]; 
    dz = zz[1]-zz[0]
    
    for i in np.arange(Nx):
        background = smooth_easy(data[i,:], 24)
        data[i] = data[i,:]-background
    
    signal = data[Nx//2]/1e6
    u_env, l_env = envelope(signal)
    temp = np.arange(len(u_env))
    gc = int(np.sum(u_env*temp)/np.sum(u_env)); print(gc)
    
    ll = 1500 #750
    if gc<ll:
        gc = ll
    t = zz/g_c; t -= t[0]
    t = t[gc-ll:gc+ll]
    signal = signal[gc-ll:gc+ll]
    
    #select = (zz*1e3>zz[0]*1e3+17-it/2)*(zz*1e3<zz[0]*1e3+32-it/2)
    #t = zz[select]/g_c; t -= t[0]
    #signal = data[Nx//2, select]/1e6#-data0[Nx/4, 250:1200]/1e6

    fig, [ax1, ax2] = plt.subplots(nrows = 2, figsize = (6, 4))
    ax1.plot(t*1e12, signal, '-')
    ax1.grid()

    ax1.set_ylabel(r"$E_x$ (MV/m)")
    ax1.set_xlabel("Time (ps)")
    #ax1.set_xlim(0, 30)
    #ax1.set_ylim(-60, 60)
    #ax1.set_title(r'$z = %.2f$ m' % np.mean(z1))
    
    fft= np.fft.fft(signal); # print np.abs(fft)[:N//2]*1/N
    
    T = t[1]-t[0]
    N = signal.size
    
    f = np.linspace(0, 1./T, N)
    
    ax2.plot(f[:N//2]/1e12, np.abs(fft)[:N//2]*1/N, '-')  # 1 / N is a normalization factor
    ax2.grid()

    ax2.set_ylabel("Intensity", labelpad=15)
    ax2.set_xlabel("Frequency (THz)")

    ax2.set_xlim(0, 10)
    fig.tight_layout()
    fig.savefig('spectrum-%d.png' % it, density = 300)

#exit()
#%% Transverse distribution
from opmd_viewer.openpmd_timeseries import OpenPMDTimeSeries
from opmd_viewer.addons import LpaDiagnostics
import h5py

ts = OpenPMDTimeSeries('./hdf5/', check_all_files=False)
#%%
energy = []
write_format = 'pickle'
for it in np.arange(it0, it1, 1):
    it *= write_step
    #it = 20
    
    Ex, info_Ex = ts.get_field( iteration=it, slicing = None, field = 'E',
                               coord = 'x', m = 'all', plot = True)
    Nx, Ny, Nz = Ex.shape
    
    for i in np.arange(Nx):
        for j in np.arange(Ny):
            background = smooth_easy(Ex[i,j,:], 24)
            Ex[i,j,:] = Ex[i,j,:]-background
    
    if write_format == 'pickle':
        fullname = os.path.join('data', 'data-%d.pkl' % it)
        with open(fullname, 'rb') as f:
            pkl = pickle.load(f, encoding='latin1')
        beam = pkl['beam']
        z1 = beam[:,2]
        #it, time, zmin, zmax, xmin, xmax = pkl['extent']
    elif write_format == 'ascii': 
        fullname = os.path.join(r'data', 'beam-%d.dat' % it)
        z1 = np.loadtxt(fullname, usecols = (2))
        it, time, zmin, zmax, xmin, xmax = np.loadtxt('extent.dat')[it,:]
    
    zmin, zmax, ymin, ymax, xmin, xmax = info_Ex.imshow_extent[:]
    print(it, xmin, xmax, ymin, ymax, zmin, zmax)
    
    xx = np.linspace(xmin, xmax, Nx)
    yy = np.linspace(ymin, ymax, Ny)
    zz = np.linspace(zmin, zmax, Nz)
    #zz = zz-zz[0]; 
    dz = zz[1]-zz[0]
    dx = xx[1]-xx[0]
    dy = yy[1]-yy[0]
    
    cut = 0
    if cut>0:
        data = np.sum(g_c*g_eps0*Ex[cut:-cut,cut:-cut,:]**2*dz/g_c/((zmax-zmin)/g_c), axis = 2)
    else:
        data = np.sum(g_c*g_eps0*Ex[:,:,:]**2*dz/g_c/((zmax-zmin)/g_c), axis = 2)
    np.savetxt('intensity-%d' % it +'.dat', data, fmt = '%14.6E')
    
    fig, ax = plt.subplots(figsize = (5, 4))
    cax = ax.imshow(data.T/1e6/1e4, extent = [xmin*1e3, xmax*1e3, ymin*1e3, ymax*1e3])
    cbar = plt.colorbar(cax, fraction = 0.046, pad = 0.04)
    cbar.set_label('intensity (MW/cm$^2$)')
    ax.set_xlabel(r'$x$ (mm)')
    ax.set_ylabel(r'$y$ (mm)')
    
    ax.set_title(r'$z = %.2f$ m' % np.mean(z1))
    fig.tight_layout()
    fig.savefig('intensity-%d.png' % it)
    
    e1 = np.sum(np.sum(data[:,:]))*dx*dy*((zmax-zmin)/g_c); print(it, e1)
    energy.append([np.mean(z1), e1])
np.savetxt('energy-z2-1.dat', energy, fmt = '%12.4E')

#%% Save Ex on x-z plane
energy = []
write_format = 'pickle'
for it in np.arange(it0, it1, 1):
    it *= write_step
    
    Ex, info_Ex = ts.get_field( iteration=it, slicing = None, field = 'E',
                               coord = 'x', m = 'all', plot = True)
    Nx, Ny, Nz = Ex.shape
    
    for i in np.arange(Nx):
        for j in np.arange(Ny):
            background = smooth_easy(Ex[i,j,:], 24)
            Ex[i,j,:] = Ex[i,j,:]-background
    
    if write_format == 'pickle':
        fullname = os.path.join('data', 'data-%d.pkl' % it)
        with open(fullname, 'rb') as f:
            pkl = pickle.load(f, encoding='latin1')
        beam = pkl['beam']
        z1 = beam[:,2]
        #it, time, zmin, zmax, xmin, xmax = pkl['extent']
    elif write_format == 'ascii': 
        fullname = os.path.join(r'data', 'beam-%d.dat' % it)
        z1 = np.loadtxt(fullname, usecols = (2))
        it, time, zmin, zmax, xmin, xmax = np.loadtxt('extent.dat')[it,:]
    
    zmin, zmax, ymin, ymax, xmin, xmax = info_Ex.imshow_extent[:]
    print(it, xmin, xmax, ymin, ymax, zmin, zmax)
    
    xx = np.linspace(xmin, xmax, Nx)
    yy = np.linspace(ymin, ymax, Ny)
    zz = np.linspace(zmin, zmax, Nz)
    #zz = zz-zz[0]; 
    dz = zz[1]-zz[0]
    dx = xx[1]-xx[0]
    dy = yy[1]-yy[0]
    
    start, end = Nz//5, Nz//2
    data = Ex[:,Ny//2,:]
    np.savetxt('Ex-xz-%d' % it +'.dat', data, fmt = '%14.6E')
    
    fig, ax = plt.subplots(figsize = (6, 3))
    cax = ax.imshow(data, extent = [zz[start]*1e3, zz[end]*1e3, xmin*1e3, xmax*1e3],
                    aspect = 'auto')
    cbar = plt.colorbar(cax, fraction = 0.046, pad = 0.04)
    cbar.set_label(r'$E_x$ (V/m)')
    ax.set_xlabel(r'$x$ (mm)')
    ax.set_ylabel(r'$y$ (mm)')
    
    ax.set_title(r'$z = %.2f$ m' % np.mean(z1))
    fig.tight_layout()
    fig.savefig('Ex-xz-%d.png' % it)
    
    
    
    start, end = 0, -1
    data = Ex[Nx//2,:,:]
    np.savetxt('Ex-xz-%d' % it +'.dat', data, fmt = '%14.6E')
    
    fig, ax = plt.subplots(figsize = (6, 3))
    cax = ax.imshow(data, extent = [zz[start]*1e3, zz[end]*1e3, ymin*1e3, ymax*1e3],
                    aspect = 'auto')
    cbar = plt.colorbar(cax, fraction = 0.046, pad = 0.04)
    cbar.set_label(r'$E_x$ (V/m)')
    ax.set_xlabel(r'$x$ (mm)')
    ax.set_ylabel(r'$y$ (mm)')
    
    ax.set_title(r'$z = %.2f$ m' % np.mean(z1))
    fig.tight_layout()
    fig.savefig('Ex-xz-%d.png' % it)
    
#%% Same output as the previous cell but starting from saved fields
energy = []
write_format = 'pickle'
for it in np.arange(it0, it1, 1):
    it *= write_step
    #it = 20
    if write_format == 'pickle':
        fullname = os.path.join('data', 'data-%d.pkl' % it)
        with open(fullname, 'rb') as f:
            pkl = pickle.load(f, encoding='latin1')
        beam = pkl['beam']
        z1 = beam[:,2]
        it, time, zmin, zmax, xmin, xmax = pkl['extent']
    elif write_format == 'ascii': 
        fullname = os.path.join(r'data', 'beam-%d.dat' % it)
        z1 = np.loadtxt(fullname, usecols = (2))
        it, time, zmin, zmax, xmin, xmax = np.loadtxt('extent.dat')[it,:]
        
    data = np.loadtxt('intensity-%d' % it +'.dat')
    Nx, Ny = data.shape
    
    #ymin, ymax = -3e-3, 3e-3
    dx = (xmax-xmin)/Nx 
    dy = (ymax-ymin)/Ny
    #np.savetxt('intensity-%d' % it +'.dat', data, fmt = '%14.6E')
    
    fig, ax = plt.subplots(figsize = (5, 2.35))
    cax = ax.imshow(data.T[::-1,:]*1e3/1e4*(zmax-zmin)/g_c, extent = [xmin*1e3, xmax*1e3, ymin*1e3, ymax*1e3])
    cbar = plt.colorbar(cax, fraction = 0.046, pad = 0.04)
    cbar.set_label('intensity (mJ/cm$^2$)')
    ax.set_xlabel(r'$x$ (mm)')
    ax.set_ylabel(r'$y$ (mm)')
    
    ax.set_title(r'$z = %.2f$ m' % np.mean(z1))
    fig.tight_layout()
    fig.savefig('intensity-%d.png' % it, density = 300)
    
    energy.append([np.mean(z1), np.sum(np.sum(data))*dx*dy*((zmax-zmin)/g_c)])
#np.savetxt('energy-z2.dat', energy, fmt = '%12.4E')
#exit()

#%%
energy = []
write_format = 'pickle'
for it in np.arange(it0, it1, 1):
    it *= write_step
    #it = 20
    
    if write_format == 'pickle':
        fullname = os.path.join('data', 'data-%d.pkl' % it)
        with open(fullname, 'rb') as f:
            pkl = pickle.load(f, encoding='latin1')
        beam = pkl['beam']
        z1 = beam[:,2]
        it, time, zmin, zmax, xmin, xmax = pkl['extent']
    elif write_format == 'ascii': 
        fullname = os.path.join(r'data', 'beam-%d.dat' % it)
        z1 = np.loadtxt(fullname, usecols = (2))
        it, time, zmin, zmax, xmin, xmax = np.loadtxt('extent.dat')[it,:]
        
    data = np.loadtxt('intensity-%d' % it +'.dat')
    Nx, Ny = data.shape
    
    #ymin, ymax = -3e-3, 3e-3
    dx = (xmax-xmin)/Nx 
    dy = (ymax-ymin)/Ny
    #np.savetxt('intensity-%d' % it +'.dat', data, fmt = '%14.6E')
    
    fig = plt.figure(figsize=(6, 4))
    
    ax  = fig.add_axes([0.30, 0.45, 0.60, 0.40]); # ax.patch.set_alpha(0)
    ax.set_xticks([])
    ax.set_yticks([])
    
    cax = ax.imshow(data.T[::-1,:]*1e3/1e4*(zmax-zmin)/g_c, extent = [xmin*1e3, xmax*1e3, ymin*1e3, ymax*1e3], aspect = 'equal')
    cbar = plt.colorbar(cax, ax = ax, fraction=0.046, pad=0.04)
    cbar.set_label('intensity (mJ/cm$^2$)')
    
    pos = ax.get_position()
    x0, y0, w0, h0 = pos.x0, pos.y0, pos.width, pos.height
    print(x0, y0, w0, h0)
    
    ax1 = fig.add_axes([x0-0.15, y0, 0.15, h0])
    ax2 = fig.add_axes([0.30, y0-0.225, w0, 0.225])
    
    xx = np.linspace(xmin, xmax, Nx)
    
    #signal = data[:,int(Ny/2)]
    signalx = np.sum(data, axis = 1)
    signalx /= np.max(signalx)
    
    ax2.plot(xx*1e3, signalx, '-')
    ax2.set_xlim(xmin*1e3, xmax*1e3)
    ax2.set_ylim(1.1, 0.000001)
    ax2.set_xlabel(r'$x$ (mm)')
    
    yy = np.linspace(ymin, ymax, Ny)
    
    #signal = data[int(Nx/2),:]
    signaly = np.sum(data, axis = 0)
    signaly /= np.max(signaly)
    
    ax1.plot(signaly, yy*1e3, '-')
    ax1.set_xlim(1.1, 0.000001)
    ax1.set_ylim(ymin*1e3, ymax*1e3)
    ax1.set_yticks([ymin*1e3, 0, ymax*1e3])
    ax1.set_ylabel(r'$y$ (mm)')
    
    ax.set_title(r'$z = %.2f$ m' % np.mean(z1))
    #fig.tight_layout()
    fig.savefig('intensity3-%d.png' % it, density = 300)
    
    energy.append([np.mean(z1), np.sum(np.sum(data))*dx*dy*((zmax-zmin)/g_c)])
    
#%% Field distribution with Energy vs z
energy = np.loadtxt('energy-z2.dat')
for it in np.arange(it0, it1, 1):
    it *= write_step
    
    data = np.loadtxt('intensity-%d' % it +'.dat')
    
    fullname = os.path.join('data', 'data-%d.pkl' % it)
    with open(fullname, 'rb') as f:
        pkl = pickle.load(f, encoding='latin1')
    beam = pkl['beam']
    z1 = beam[:,2]
    
    #z1 = np.loadtxt('./data/beam-%d.dat' % it, usecols = (2))
    
    #xmin, xmax, ymin, ymax = -6e-3, 6e-3, -3e-3, 3e-3
    
    #fig, [ax, ax2] = plt.subplots(figsize = (4, 8), nrows = 2)
    
    fig = plt.figure(figsize=(5, 6))
    ax  = fig.add_axes([0.10, 0.50, 0.75, 0.45]); # ax.patch.set_alpha(0)
    ax2 = fig.add_axes([0.15, 0.15, 0.75, 0.25])
    
    cax = ax.imshow(data.T/1e6/1e4, extent = [xmin*1e3, xmax*1e3, ymin*1e3, ymax*1e3])
    #cbar = plt.colorbar(cax, ax = ax, fraction=0.09, pad=0.01)
    cbar = plt.colorbar(cax, ax = ax, fraction=0.046, pad=0.04)
    
    cbar.set_label('intensity (MW/cm$^2$)')
    ax.set_xlabel(r'$x$ (mm)')
    ax.set_ylabel(r'$y$ (mm)')
    
    ax.set_title(r'$z = %.2f$ m' % np.mean(z1))
    
    ax2.plot(energy[:it,0], energy[:it,1]*1e6, '-*')
    ax2.grid()
    ax2.set_xlabel(r'$z$ (m)')
    ax2.set_ylabel(r'radiation energy ($\mu$J)')
    ax2.set_xlim(0, 3.9)
    ax2.set_ylim(0, 2000)

    #fig.tight_layout()
    fig.savefig('intensity2-%d.png' % it, density = 300)

#exit()
#%% Transverse profile fit
rms = []
write_format = 'pickle'
for it in np.arange(it0, it1, 1):
    it *= write_step
    
    if write_format == 'pickle':
        fullname = os.path.join('data', 'data-%d.pkl' % it)
        with open(fullname, 'rb') as f:
            pkl = pickle.load(f, encoding='latin1')
        beam = pkl['beam']
        z1 = beam[:,2]
        it, time, zmin, zmax, xmin, xmax = pkl['extent']
    elif write_format == 'ascii': 
        fullname = os.path.join(r'data', 'beam-%d.dat' % it)
        z1 = np.loadtxt(fullname, usecols = (2))
        it, time, zmin, zmax, xmin, xmax = np.loadtxt('extent.dat')[it,:]
        
    data = np.loadtxt('intensity-%d' % it +'.dat')
    Nx, Ny = data.shape
    
    #ymin, ymax = -3e-3, 3e-3
    dx = (xmax-xmin)/Nx 
    dy = (ymax-ymin)/Ny
    #np.savetxt('intensity-%d' % it +'.dat', data, fmt = '%14.6E')
    
    xx = np.linspace(xmin, xmax, Nx)
    
    signal = data[:,int(Ny/2)]
    signalx = np.sum(data, axis = 1)
    signalx /= np.max(signalx)

    fig, ax = plt.subplots()
    ax.plot(xx*1e3, signalx, '-')
    ax.grid()
    
    def f(x, sig_x, amp, x0):
        return amp*np.exp(-(x-x0)**2/2.0/sig_x**2)

    popt, pcov = curve_fit(f, xx*1e3, signalx, p0 = (2, 1, 0))
    print(popt)

    sig_x = popt[0]
    
    #ax.plot(xx*1e3, f(xx*1e3, *popt), '--')
    ax.set_ylim(0, 1.1)
    #ax.set_title('$\sigma_x$ = %.2f mm' % sig_x)
    ax.set_title(r'$z = %.2f$ m' % np.mean(z1))
    fig.savefig('I2-x-%d.png' % it, density = 300)
    
    
    yy = np.linspace(ymin, ymax, Ny)
    print(data.shape)
    signal = data[int(Nx/2),:]
    signaly = np.sum(data, axis = 0)
    signaly /= np.max(signaly)
    
    fig, ax = plt.subplots()
    ax.plot(yy*1e3, signaly, '-')
    ax.grid()
    

    popt, pcov = curve_fit(f, yy*1e3, signaly, p0 = (2, 1, 0))
    print(popt)
    
    sig_y = popt[0]
    
    #ax.plot(yy*1e3, f(yy*1e3, *popt), '--')
    ax.set_ylim(0, 1.1)
    #ax.set_title('$\sigma_y$ = %.2f mm' % sig_y)
    ax.set_title(r'$z = %.2f$ m' % np.mean(z1))
    fig.savefig('I2-y-%d.png' % it, density = 300)
    
    rms.append([np.mean(z1), sig_x, sig_y])
#rms = np.array(rms)
#np.savetxt('rms-z.dat', rms, fmt = '%12.4E')


rms = np.loadtxt('rms-z.dat')
fig, ax = plt.subplots(figsize = (5, 4))
ax.plot(rms[:,0], rms[:,1], '-<')
ax.plot(rms[:,0], rms[:,2], '-D')

def g(x, s0, x0):
    lam0 = 0.1
    Zr = 4*np.pi*s0**2/lam0
    return 2*s0*np.sqrt(1+(x-x0)**2/Zr**2)

popt, pcov = curve_fit(g, rms[4:18,0]*1e3, rms[4:18,2], p0 = (2, 2000))
print(popt)

#ax.plot(rms[:,0], g(rms[:,0]*1e3, *popt), 'r-')

ax.set_xlabel(r'$z$ (m)')
ax.set_ylabel(r'RMS size (mm)')

ax.legend([r'$x$', r'$y$'])

ax.set_xlim(0, 3.9)
ax.set_ylim(0, 6)
ax.grid()
fig.savefig('rms-z.png', density = 300)

#%%
#workdir = os.path.join(simdir,r'2020',  r'Warp', r'16x2x2_17', r'lab_diags')
workdir = r'D:\PITZ\2020\Warp'
os.chdir(workdir)

e1 = np.loadtxt('16x2x2_14\lab_diags\energy-z2.dat')
e2 = np.loadtxt('16x2x2_15\lab_diags\energy-z2.dat')
e3 = np.loadtxt('16x2x2_16\lab_diags\energy-z2.dat')

fig_kw = {}
fig, ax = plt.subplots(**fig_kw)
ax.plot(e1[:,0], e1[:,1], '-*')
ax.plot(e2[:,0], e2[:,1], '-o')
ax.plot(e3[:,0], e3[:,1], '-D')

#ax.set_yscale('log')
ax.grid()

#%% Stream plot of electric field
write_format = 'pickle'
for it in np.arange(1, 27, 100):
    it = 20
    #field = 'E'
    #coord = 'x'
    for field in ['E']:
        for coord in ['x', 'y', 'z']:
        
            Fld, info_Fld = ts.get_field( iteration=it, slicing = None, field = field,
                                       coord = coord, m = 'all', plot = True)
            
            Nx, Ny, Nz = Fld.shape; print(Nx, Ny, Nz)
            
            
            for i in np.arange(Nx):
                for j in np.arange(Ny):
                    background = smooth_easy(Fld[i,j,:], 16)
                    Fld[i,j,:] = Fld[i,j,:]-background
            
            Ffile = field+coord+'3D-%d.dat' % it
            remove_files([Ffile])
            ss = 2
            with open(Ffile, 'a') as fx:
                np.savetxt(fx, np.atleast_2d(info_Fld.x[::ss]), fmt='%15.6E',
                           header = str.format('x grid, dx = %.2f mm' % (info_Fld.dx*1e3*ss)))
                np.savetxt(fx, np.atleast_2d(info_Fld.y[::ss]), fmt='%15.6E',
                           header = str.format('y grid, dy = %.2f mm' % (info_Fld.dy*1e3*ss)))
                np.savetxt(fx, np.atleast_2d(info_Fld.z[:3601]), fmt='%15.6E',
                           header = str.format('z grid, dz = %.2f um' % (info_Fld.dz*1e6)))
                for k in np.arange(3601):
                    np.savetxt(fx, np.atleast_2d(Fld[::ss,::ss,k]), fmt='%14.6E',
                               header = str.format('SliceZ = %d' % k))
            
#%%            
if 1:      
    if write_format == 'pickle':
        fullname = os.path.join('data', 'data-%d.pkl' % it)
        with open(fullname, 'rb') as f:
            pkl = pickle.load(f, encoding='latin1')
        beam = pkl['beam']
        z1 = beam[:,2]
        #it, time, zmin, zmax, xmin, xmax = pkl['extent']
    elif write_format == 'ascii': 
        fullname = os.path.join(r'data', 'beam-%d.dat' % it)
        z1 = np.loadtxt(fullname, usecols = (2))
        it, time, zmin, zmax, xmin, xmax = np.loadtxt('extent.dat')[it,:]
    
    zmin, zmax, ymin, ymax, xmin, xmax = info_Ex.imshow_extent[:]
    print(it, xmin, xmax, ymin, ymax, zmin, zmax)
    
    xx = np.linspace(xmin, xmax, Nx)
    yy = np.linspace(ymin, ymax, Ny)
    zz = np.linspace(zmin, zmax, Nz)
    #zz = zz-zz[0]; 
    dz = zz[1]-zz[0]
    dx = xx[1]-xx[0]
    dy = yy[1]-yy[0]
    
    data = np.sum(g_c*g_eps0*Ex**2*dz/g_c/((zmax-zmin)/g_c), axis = 2)
    #np.savetxt('intensity-%d' % it +'.dat', data, fmt = '%14.6E')
    
    fig, ax = plt.subplots(figsize = (5, 4))
    cax = ax.streamplot(xx*1e3, yy*1e3, Ex[:,:,2639].T, Ey[:,:,2639].T)
    cbar = plt.colorbar(cax.lines)
    
    ax.set_xlabel(r'$z$ (mm)')
    ax.set_ylabel(r'$x$ (mm)')
    ax.set_xlim(-5.5, 5.5)
    ax.set_ylim(-2.5, 2.5)
    
    ax.set_title(r'$z = %.2f$ m' % np.mean(z1))
    fig.tight_layout()
    fig.savefig('E-vs-xz-%d.png' % it)
    
#%% Radiation energy vs z

reset_margin(bottom = 0.2, top = 0.9)
fig, ax = plt.subplots(figsize = (3, 4))

#energy = np.loadtxt('16x2x2_41/lab_diags/energy-z2.dat')
energy = np.loadtxt('energy-z2.dat')
ax.plot(energy[:,0], energy[:,1]*1e3, '-*')

# energy = np.loadtxt('16x2x2_37/lab_diags/energy-z2.dat')
# ax.plot(energy[:,0], energy[:,1]*1e3, '-*')

# energy = np.loadtxt('16x2x2_38/lab_diags/energy-z2.dat')
# ax.plot(energy[:,0], energy[:,1]*1e3, '-*')

ax.grid()
ax.set_xlabel(r'$z$ (m)')
ax.set_ylabel(r'Energy (mJ)')
ax.set_xlim(0, 4)
ax.set_ylim(0, 1.2)

#ax.legend(['No tran. grad.', 'Tran. grad. + coils'])

fig.savefig('Energy-vs-z2.png')

#%% Beam center vs z

fig, ax = plt.subplots()

energy = np.loadtxt('beam.dat')
ax.plot(energy[:,2], energy[:,12]*1e3, '-*')

energy = np.loadtxt('../../beam.dat')
#ax.plot(energy[:,2], energy[:,12]*1e3, '-*')

ax.grid()
ax.set_xlabel(r'$z$ (m)')
ax.set_ylabel(r'$x_c$ (mm)')
ax.set_xlim(0, 4)
ax.set_ylim(-1.5, 1.5)

#ax.legend(['w/o coil', 'with coil'])

fig.savefig('../../beam-center-vs-z-without-coil.png')

#%%
a, b = 11e-3, 5e-3

def fEx(x, y, z, kx, ky, ks):
    
    kz = np.sqrt(ks**2-kx**2-ky**2)
    vz = kz/ks*g_c
    t = z/vz
    return np.cos(kx*(x+a/2))*np.sin(ky*(y+b/2))*np.cos(kz*z-ks*g_c*t)

lam_s = 100e-6
ks = 2*np.pi/lam_s

m, n = 2, 1
kx = m*np.pi/a
ky = n*np.pi/b

xx = np.linspace(-a/2, a/2, 201)
yy = np.linspace(-b/2, b/2, 11)
zz = np.linspace(0, 4.95, 12)

aX, aY = np.meshgrid(xx, yy)

fig, ax = plt.subplots(ncols = 3, nrows = 4, figsize = (9, 6),
                       sharex = True, sharey = True)
ax = ax.flatten()

for i, axis in enumerate(ax):
    z1 = zz[i]
    
    m, n = 0, 1
    kx = m*np.pi/a
    ky = n*np.pi/b
    aEx = fEx(aX, aY, z1, kx, ky, ks)
    
    m, n = 2, 1
    kx = m*np.pi/a
    ky = n*np.pi/b
    aEx -= fEx(aX, aY, z1, kx, ky, ks)
    #aEx = fEx(aX, aY, z1, kx, ky, ks)
    
    cax = axis.imshow(aEx**2, extent = (-a/2*1e3, a/2*1e3, -b/2*1e3, b/2*1e3))
    axis.set_title('$z$ = %.2f m' % z1)
    
cbar = plt.colorbar(cax, ax = ax)
#fig.tight_layout()

#fig.savefig('TE01+TE21.png')
#%%
d1 = np.loadtxt('ast.Xemit.018')

d2 = np.loadtxt('1x2x2_42/diags/beam.dat')

d3 = np.loadtxt('1x2x2_44/diags/beam.dat')
d3 = d3[d3[:,2]<=3.6]

d4 = np.loadtxt('1x2x2_64/beam.dat')

reset_margin(top = 0.95)
fig, ax = plt.subplots(nrows = 3, figsize = (5, 6), sharex = True)

beam_zc = -5e-3
ax[0].plot(d1[:,0]+beam_zc, d1[:,3], '-')
ax[0].plot(d2[:,2], d2[:,5], '-o')
ax[0].plot(d3[:,2], d3[:,5], '-*')
ax[0].plot(d4[:,0], d4[:,3], '-<')

d1 = np.loadtxt('ast.Yemit.001')
ax[1].plot(d1[:,0]+beam_zc, d1[:,3], '-')
ax[1].plot(d2[:,2], d2[:,6], '-o')
ax[1].plot(d3[:,2], d3[:,6], '-*')
ax[1].plot(d4[:,0], d4[:,4], '-<')

d1 = np.loadtxt('ast.Zemit.001')
ax[2].plot(d1[:,0]+beam_zc, d1[:,3], '-')
ax[2].plot(d2[:,2], d2[:,7], '-o')
ax[2].plot(d3[:,2], d3[:,7], '-*')
ax[2].plot(d4[:,0], d4[:,5], '-<')

for axis in ax:
    axis.grid()
ax[2].set_xlabel(r'$z$ (m)')

ax[0].set_ylabel(r'$\sigma_x$ (mm)')
ax[1].set_ylabel(r'$\sigma_y$ (mm)')
ax[2].set_ylabel(r'$\sigma_z$ (mm)')

fig.tight_layout()
#%%
d1 = np.loadtxt('ast.Xemit.099')

d2 = np.loadtxt('16x2x2_47/lab_diags/beam.dat')

d3 = np.loadtxt('16x2x2_50/lab_diags/beam.dat')
d3 = np.loadtxt('16x2x2_58/beam.dat')
d3 = d3[d3[:,0]<=3.6]

d4 = np.loadtxt('tracker/beam.dat')
d4 = np.loadtxt('16x2x2_58/beam.dat')
d4 = np.loadtxt('1x2x2_61/beam.dat')
d4 = np.loadtxt('test2/beam.dat')

reset_margin(top = 0.95)
fig, ax = plt.subplots(nrows = 3, figsize = (5, 6), sharex = True)

beam_zc = -5e-3
ax[0].plot(d1[:,0]+beam_zc, d1[:,3], '-')
ax[0].plot(d2[:,2], d2[:,5], '-o')
ax[0].plot(d3[:,0], d3[:,3], '-*')
ax[0].plot(d4[:,0], d4[:,3], '-<')

d1 = np.loadtxt('ast.Yemit.099')
ax[1].plot(d1[:,0]+beam_zc, d1[:,3], '-')
ax[1].plot(d2[:,2], d2[:,6], '-o')
ax[1].plot(d3[:,0], d3[:,4], '-*')
ax[1].plot(d4[:,0], d4[:,4], '-<')

d1 = np.loadtxt('ast.Zemit.099')
ax[2].plot(d1[:,0]+beam_zc, d1[:,3], '-')
ax[2].plot(d2[:,2], d2[:,7], '-o')
ax[2].plot(d3[:,0], d3[:,5], '-*')
ax[2].plot(d4[:,0], d4[:,5], '-<')

for axis in ax:
    axis.grid()
ax[2].set_xlabel(r'$z$ (m)')

ax[0].set_ylabel(r'$\sigma_x$ (mm)')
ax[1].set_ylabel(r'$\sigma_y$ (mm)')
ax[2].set_ylabel(r'$\sigma_z$ (mm)')

fig.tight_layout()
#%%
reset_margin(top = 0.95)
fig, ax = plt.subplots(nrows = 3, figsize = (5, 6), sharex = True)

d1 = np.loadtxt('ast.Xemit.099')
ax[0].plot(d1[:,0], d1[:,2], '-')
ax[0].plot(d2[:,2], d2[:,12]*1e3, '-o')
ax[0].plot(d3[:,2], d3[:,12]*1e3, '-*')
ax[0].plot(d4[:,0], d4[:,10]*1e3, '-<')

d1 = np.loadtxt('ast.Yemit.099')
ax[1].plot(d1[:,0], d1[:,2], '-')
ax[1].plot(d2[:,2], d2[:,13]*1e3, '-o')
ax[1].plot(d3[:,2], d3[:,13]*1e3, '-*')
ax[1].plot(d4[:,0], d4[:,11]*1e3, '-<')

d1 = np.loadtxt('ast.Zemit.099')
ax[2].plot(d1[:,0], d1[:,3], '-')
ax[2].plot(d2[:,2], d2[:,7], '-o')
ax[2].plot(d3[:,2], d3[:,7], '-*')

for axis in ax:
    axis.grid()
ax[2].set_xlabel(r'$z$ (m)')

ax[0].set_ylabel(r'$x_c$ (mm)')
ax[1].set_ylabel(r'$y_c$ (mm)')
ax[2].set_ylabel(r'$\sigma_z$ (mm)')

fig.tight_layout()

#%% check ref particle trajectory in boosted frame
data = np.loadtxt('ref-part-12.dat')
x, y, z, ux, uy, uz, t, _ = data[177:,:].T[:]
ux = ux/g_c
uy = uy/g_c
gg = np.sqrt(1+ux**2+uy**2+uz**2)

fig, ax = plt.subplots(nrows = 3)
ax[0].plot(z*1e3, x*1e3)

ax[1].plot(z*1e3, ux/gg)

ax[2].plot(t*1e12, z*1e3)

lbf = LorentzBoost(12.25)

x0, y0, z0, t0 = lbf.inverse_spacetime(x, y, z, t)
vx0, vy0, vz0 = lbf.inverse_velocity(ux/gg*g_c, uy/gg*g_c, uz/gg*g_c)

fig, ax = plt.subplots(nrows = 3)
ax[0].plot(z0*1e3, x0*1e3)

ax[1].plot(z0*1e3, vx0/g_c)

ax[2].plot(t0*1e12, z0*1e3)

#%%
def unique_lexsort(data):
    sorted_data = data[np.lexsort(data.T),:]
    row_mask = np.append([True], np.any(np.diff(sorted_data,axis=0),1))
    return sorted_data[row_mask]

beam = np.loadtxt('beam-0.dat')

fig, ax = plt.subplots()
ax.hist(beam[:,-1], 100)

#%%
import h5py

for it in np.arange(1, 25):
    it *= 1
    f2 = h5py.File('data%08d.h5' % it, 'r')
    print(it, len(f2['data/%d/particles/beam/id' % it][:]))

it = 1
f2 = h5py.File('data%08d.h5' % it, 'r')
x = f2['data/%d/particles/beam/position/x' % it][:]
z = f2['data/%d/particles/beam/position/z' % it][:]
uz = f2['data/%d/particles/beam/momentum/z' % it][:]

fig, ax = plt.subplots()
ax.plot(z, uz, '.')
#%% 
beam = pd_loadtxt('beam_und_boosted.warp')

fig, ax = plt.subplots()
ax.plot(beam[:,2], beam[:,0]*1e3, '.')

#%%
beam = pd_loadtxt('beam_und_250k_ham_sampled.warp')
select = (np.abs(beam[:,0])<6e-3)*(np.abs(beam[:,1])<3e-3)
print(np.sum(select))

np.savetxt('beam_und_250k_ham_sampled2.warp', beam[select], fmt = '%14.6E')

x, y, z, ux, uy, uz, w = beam.T[:]

t = 0
z -= 5e-3 # beam center in the lab frame
vx, vy, vz = momentum2velocity(ux, uy, uz)

x1 = x-(z-0)/vz*vx #*lbf.gamma

### Boosted factor
gamma_boost = 12.25 # 12.25
lbf = LorentzBoost(gamma_boost)

### Transform into boosted frame
xp, yp, zp, tp = lbf.spacetime(x, y, z, t)
vxp, vyp, vzp = lbf.velocity(vx, vy, vz)


fig, ax = plt.subplots()
#ax.plot(beam[:,2], beam[:,0]*1e3, '.')
ax.plot(z, x, '.')
ax.plot(z, x1, '.')

#%%
temp1 = pd_loadtxt('test9/temp6') # no waveguide
temp2 = pd_loadtxt('test9/temp7') # w/ waveguide
temp3 = pd_loadtxt('test9/temp8') # w/ narrower waveguide 
#temp4 = pd_loadtxt('test9/temp9') # w/ dirichlet boundaries 


temp1 = pd_loadtxt('test9/temp100') # no wg
#temp2 = pd_loadtxt('test9/temp101') # after one step with wg
temp3 = pd_loadtxt('test9/Ex0_vs_xz.dat') # after 13 steps with wg

Nx, Nz = temp1.shape

fig, ax = plt.subplots()
ax.plot(temp1[:,450])
ax.plot(temp2[:,450])
ax.plot(temp3[:,450])
#ax.plot(np.arange(len(temp4))+10, temp4[:,450])

fig, ax = plt.subplots(figsize = (6, 6), nrows = 3)
ax[0].imshow(temp1)
ax[1].imshow(temp2)
ax[2].imshow(temp3)

#%%
beam_zc = -5e-3
gamma_boost = 12.25
beta_boost = gamma2beta(gamma_boost)
zminwg, zmaxwg = (1.8+beam_zc-1.7)/gamma_boost, (1.8+beam_zc+1.7)/gamma_boost

t = np.linspace(0, 1000e-12)
z1 = zminwg-beta_boost*g_c*t
z2 = zmaxwg+beta_boost*g_c*t

plt.figure(1)
plt.plot(t*1e12, z1)
plt.plot(t*1e12, z2)

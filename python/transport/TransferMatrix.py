from numpy import *
from sympy import *

from universal import *
from transfer import *

np.set_printoptions(precision = 5, suppress = True)

def G2K(G, P0, qn = 1.):
    '''
    Parameters
      G: gradient, T/m
      P0: momentum, MeV/c
      qn = q/qe: number of charge
    Returns
      K: focal strength, m^-2
    '''
    return G*qn/(P0*1e6/g_c)

def K2G(K, P0, qn = 1.):
    '''
    Parameters
      K: focal strength, m^-2
      P0: momentum, MeV/c
      qn = q/qe: number of charge
    Returns
      G: gradient, T/m
    '''
    return K/(qn/(P0*1e6/g_c))

def BendingRadius(B, P0 = 19.5):
    return P0*1e6/B/g_c

def BendingField(rho, P0 = 19.5):
    return P0*1e6/rho/g_c

def Drift(L): 
    '''
    Transfer matrix of a drift.

    Parameters
    ----------
    L : float or `sympy` symbol, meter
        Length of the drift.

    Returns
    -------
    tr : `sympy` type 6x6
        6D transfer matrix of the drift.

    '''
    
    L = np.abs(L)
    tr = Matrix([[1, L, 0, 0, 0, 0], [0, 1, 0, 0, 0, 0], 
                 [0, 0, 1, L, 0, 0], [0, 0, 0, 1, 0, 0],
                 [0, 0, 0, 0, 1, 0], [0, 0, 0, 0, 0, 1]])
    return tr

def NMatrix(x, *args, func = Drift):
    tr = []
    for v in x:
        tr.append(func(v, *args))
    return tr

def Dipole(rho, theta, gamma = 1):
    '''
    Transfer matrix of a dipole magnet.

    Parameters
    ----------
    rho : float or `sympy` symbol, meter
        Bending radius of a particle in the magnetic field.
    theta : float or `sympy` symbol, radian
        Bending angle of a particle in the magnetic field.
    gamma : float, optional
        Relativistic factor of the particle. The default is 1.

    Returns
    -------
    tr : `sympy` type 6x6
        6D transfer matrix of the dipole magnet.

    '''
    L = rho*theta
    tr = Matrix([[cos(theta), rho*sin(theta), 0, 0, 0, rho*(1-cos(theta))], 
                 [-1.0/rho*sin(theta), cos(theta), 0, 0, 0, sin(theta)],
                 [0, 0, 1, L, 0, 0], 
                 [0, 0, 0, 1, 0, 0],
                 [sin(theta), rho*(1-cos(theta)), 0, 0, 1,
                  -rho*(theta-sin(theta)+L/gamma**2)],
                 [0, 0, 0, 0, 0, 1]])
    return tr

def Wedge(rho, theta, gamma = 1):
    '''
    Transfer matrix of a wedge dipole magnet.

    Parameters
    ----------
    rho : float or `sympy` symbol, meter
        Bending radius of a particle in the magnetic field.
    theta : float or `sympy` symbol, radian
        Bending angle of a particle in the magnetic field.
    gamma : float or `sympy` symbol, optional
        Relativistic factor of the particle. The default is 1.

    Returns
    -------
    tr : `sympy` type 6x6
        6D transfer matrix of the wedge dipole magnet.

    '''
    L = rho*theta
    tr = Matrix([[cos(theta), rho*sin(theta), 0, 0, 0, rho*(1-cos(theta))], 
                 [-1.0/rho*sin(theta), cos(theta), 0, 0, 0, sin(theta)],
                 [0, 0, 1, L, 0, 0], 
                 [0, 0, 0, 1, 0, 0],
                 [sin(theta), rho*(1-cos(theta)), 0, 0, 1,
                  -rho*(theta-sin(theta)+L/gamma**2)],
                 [0, 0, 0, 0, 0, 1]])
    return tr

def PoleFaceRotation(beta, rho = 0, gap = 0):
    '''
    Transfer matrix for the pole face rotation of a dipole magnet.
    
    Parameters
    ----------
    beta : float or `sympy` symbol, radian
        Bending angle of the pole with respect to the plane normal to the incoming axis.
    rho : float or `sympy` symbol, meter, optional
        Bending radius of a particle in the magnetic field. The default is 0.
    gap : float or `sympy` symbol, meter, optional
        Gap between the poles. The default is 0.

    Returns
    -------
    tr : `sympy` type 6x6
        6D transfer matrix for the pole face rotation.

    '''
    if gap == 0 or rho == 0:
        psi = 0
    else:
        k1 = 0.5
        psi = k1*gap/rho*(1+sin(beta)**2)/cos(beta)
        
    tr = Matrix([[1, 0, 0, 0, 0, 0],
                 [tan(beta)/rho, 1, 0, 0, 0, 0],
                 [0, 0, 1, 0, 0, 0],
                 [0, 0, -tan(beta-psi)/rho, 1, 0, 0],
                 [0, 0, 0, 0, 1, 0],
                 [0, 0, 0, 0, 0, 1]])
    return tr

def Sector(rho, theta, beta1 = 0, beta2 = 0, gamma = 1, gap = 0):
    '''
    Transfer matrix of a sector dipole magnet.

    Parameters
    ----------
    rho : float or `sympy` symbol, meter
        Bending radius of a particle in the magnetic field.
    theta : float or `sympy` symbol, radian
        Bending angle of a particle in the magnetic field.
    beta1 : float or `sympy` symbol, radian, optional
        Bending angle of the pole with respect to the plane normal to the incoming axis.. The default is 0.
    beta2 : float or `sympy` symbol, radian, optional
        Bending angle of the pole with respect to the plane normal to the outcoming axis.. The default is 0.
    gamma : float or `sympy` symbol, optional
        Relativistic factor of the particle. The default is 1.
    gap : float or `sympy` symbol, meter, optional
        Gap between the poles. The default is 0.

    Returns
    -------
    tr : `sympy` type 6x6
        6D transfer matrix of the sector dipole magnet.

    '''
    
    tr1 = PoleFaceRotation(beta1, rho, gap)
    trd = Wedge(rho, theta, gamma)
    tr2 = PoleFaceRotation(beta2, rho, gap)
    tr = tr2*trd*tr1
    return tr

def QuadF(K, L):
    '''
    Transfer matrix of a quadruple magnet focusing in x.

    Parameters
    ----------
    K : float or `sympy` symbol, m^-2
        Strength of the quadrupole.
    L : float or `sympy` symbol, meter
        Effective length of the quadrupole.

    Returns
    -------
    tr : `sympy` type 6x6
        6D transfer matrix of the quadruple magnet.

    '''
    
    kk = sqrt(np.abs(K)); kl = kk*L
    cc = cos(kl);  ss = sin(kl)
    ch = cosh(kl); sh = sinh(kl)
    tr = Matrix([[ cc,    1./kk*ss, 0,     0,        0, 0],
                 [-kk*ss, cc,       0,     0,        0, 0],
                 [ 0,     0,        ch,    1./kk*sh, 0, 0],
                 [ 0,     0,        kk*sh, ch,       0, 0],
                 [ 0,     0,        0,     0,        1, 0],
                 [ 0,     0,        0,     0,        0, 1]])
    
    return tr

def QuadD(K, L):
    '''
    Transfer matrix of a quadruple magnet defocusing in x.

    Parameters
    ----------
    K : float or `sympy` symbol, m^-2
        Strength of the quadrupole.
    L : float or `sympy` symbol, meter
        Effective length of the quadrupole.

    Returns
    -------
    tr : `sympy` type 6x6
        6D transfer matrix of the quadruple magnet.

    '''
    
    kk = sqrt(np.abs(K)); kl = kk*L
    cc = cos(kl);  ss = sin(kl)
    ch = cosh(kl); sh = sinh(kl)
    tr = Matrix([[ch,    1./kk*sh, 0,      0,        0, 0],
                 [kk*sh, ch,       0,      0,        0, 0],
                 [0,     0,        cc,     1./kk*ss, 0, 0],
                 [0,     0,       -kk*ss, cc,        0, 0],
                 [0,     0,        0,     0,         1, 0],
                 [0,     0,        0,     0,         0, 1]])
    
    return tr

def Quadrupole(K, L):
    if K>=0:
        tr = QuadF(K, L)
    else:
        tr = QuadD(K, L)
    return tr
Quad = Quadrupole

def BetatronTransportMatrix(tr):
    '''
    Convert a transfer matrix to betatron transport matrix,
    e.g., transport of [beta, alpha, gamma]

    Parameters
    ----------
    tr : 2x2 array
        A transfer matrix.

    Returns
    -------
    tr3 : 3x3 array
        Betatron transport matrix

    '''
    
    tr3 = Matrix([[ tr[0,0]**2,     -2*tr[0,0]*tr[0,1],                tr[0,1]**2],
                  [-tr[0,0]*tr[1,0], tr[0,0]*tr[1,1]+tr[0,1]*tr[1,0], -tr[1,1]*tr[0,1]],
                  [ tr[1,0]**2,      2*tr[1,1]*tr[1,0],                tr[1,1]**2]])
    return tr3

class TransferMatrix():
    dimension = 6
    matrix = np.eye(dimension)
    length = 0
    
    def __init__(self, M, L):
        self.matrix = M
        self.length = L
        self.dim = M.shape[0]
        
    def __mul__(self, other):
        return self.matrix @ other.matrix
    
class TrDrift(TransferMatrix):
    def __init__(self, L):
        self.matrix = Drift(L)
        self.length = L
        self.dim = self.matrix.shape[0]
        
def SigmaMatrix6D(fname = None, dist = None):
    '''
    
    Generate the sigma matrix and the beam momentum Pc/GeV from `Astra` output

    Parameters
    ----------
    fname : string, optional
        Filename of the output from `Astra`. The default is None.
    dist : n-D array, optional
        Distribution of the beam particles. The default is None.

    Returns
    -------
    sigma : nxn array, n = 2, 4, or 6
        The sigma matrix of the beam, i.e., the covariance matrix of x, xp, y, yp, z and delta P/P. 
        The units are mm, mrad, mm, mrad, mm and 0.1%, respectively.
    Pc : float
        The average momentum of the beam in GeV/c.

    '''
    if fname is not None:
        data = pd_loadtxt(fname)
        data[0,2] = 0
        #data[1:,2]=data[1:,2]+data[0,2]
        data[1:,5] += data[0,5]
    elif dist is not None:
        data = dist
    else:
        print('Error: no input!')
        return None

    data[:,0]= data[:,0]*1000 # x: m->mm
    data[:,1] = data[:,1]*1000 # y: m->mm
    data[:,2] = data[:,2]*1000 # z: m->mm
    
    data[:,6] = np.sqrt(data[:,3]**2+data[:,4]**2+data[:,5]**2)/1e9 # Pc=P*c: eV->GeV
    data[:,3] = data[:,3]/data[:,5]*1e3 # xp: mrad
    data[:,4] = data[:,4]/data[:,5]*1e3 # yp: mrad
    data[:,5] = data[:,6] # pz->Pc

    data[:,6] = data[:,1] # 6th column: y
    data[:,1] = data[:,3] # 2nd column: xp

    data[:,3] = data[:,4] # 4th column: yp
    data[:,4] = -data[:,2] # 5th column: z
    data[:,2] = data[:,6] # 3rd column: y

    Pc = np.mean(data[:,5])
    data[:,5] = (data[:,5]-Pc)/Pc*1e3 # dp/P: 0.1 %

    sigma = np.cov(data[:,0:6].T)

    return sigma, Pc

def Transport6D(sigma, R1, *args, **kwargs):
    '''
    Transport a beam represented by a 6D sigma matrix by a series of transfer matrix.

    Parameters
    ----------
    sigma : nxn array, n = 2, 4, or 6
        Initial sigma matrix of the beam, i.e., the covariance matrix of x, xp, y, yp, z and delta P/P. 
        The units are m, rad, m, rad, m and %, respectively.
    R1 : nxn array, n = 2, 4, or 6
        Transfer matrix of a drift or quadrupole or others.
    *args : same as R1
        More transfer matrix following R1.
    **kwargs : optional setup
        dim: 2, 4 or 6, the dimension of the transport.
        
    Returns
    -------
    sigmas : a list of nxn array, n = 2, 4, or 6
        Sigma matrix of the beam along the transport.
    transfers : a list of nxn array, n = 2, 4, or 6
        Transfer matrix for the initial position to the end of each element.

    '''
    dim = 6
    if len(kwargs)>0:
        if 'dim' in kwargs.keys():
            dim = kwargs['dim']
    if len(sigma)<dim and len(sigma)//2 == 0:
        dim = len(sigma)
    
    flag = 0
    if len(R1)<dim:
        flag = 1
    if len(args)>0:
        for temp in args:
            if len(temp)<dim:
                flag = 1
    if flag:
        print('Error: transfer matrix dimension not matching!')
        return None, None
    
    sigmas = [sigma[0:dim, 0:dim]]
    transfers = [np.eye(dim)]
    
    sigmas.append(R1[0:dim, 0:dim] @ sigmas[-1] @ R1[0:dim, 0:dim].T)
    transfers.append(R1[0:dim, 0:dim] @ transfers[-1])
    if len(args)>0:
        for R2 in args:
            sigmas.append(R2[0:dim, 0:dim] @ sigmas[-1] @ R2[0:dim, 0:dim].T)
            transfers.append(R2[0:dim, 0:dim] @ transfers[-1])
    return sigmas, transfers

def FitSlope(x, y, xmin = None, xmax = None, plot = False):
    '''
    Fit the slope of a trajectory with respect to x axis.

    Parameters
    ----------
    x : 1D array
        Positions of the particle trajectory in x axis.
    y : 1D array
        Positions of the particle trajectory in y axis.
    xmin : float, optional
        lower limit of x for the fit. The default is None.
    xmax : float, optional
        upper limit of x for the fit. The default is None.
    plot : Boolean, optional
        If making plot or not. The default is False.

    Returns
    -------
    slope : float
        The slope of the trajectory with respect to x.

    '''
    select = np.ones(len(x), dtype = 'bool')
    if xmin is not None:
        select *= (x>xmin)
    if xmax is not None:
        select *= (x<xmax)
        
    func = lambda x, a, b: a+b*x
    popt, pcov = curve_fit(func, x[select], y[select])
    a, b = popt
    slope = np.arctan(b)
    print('Slope is %.6E mrad' % (slope*1e3))
    slope = slope/np.pi*180
    
    if plot:
        fig, ax = plt.subplots()
        ax.plot(x, y, '.')
        a#x.plot(x[select], func(x[select], *popt), '-')
        ax.plot(x, func(x, *popt), '-')
        ax.grid()
        ax.set_title('Slope = %.2f degree' % slope)
    return slope

def FitSlopeOfRef(fname, *args, index_x = 0, index_y = 5, scale_x = 1e-3, **kwargs):
    '''
    Fit the slope of a bended trajectory.

    Parameters
    ----------
    fname : string
        File saving reference particle from `Astra` simulation.
    *args : optional input for future functions
        DESCRIPTION.
    **kwargs : optional setup for the fit, includes:
        xmin : float, optional
            lower limit of x for the fit. The default is None.
        xmax : float, optional
            upper limit of x for the fit. The default is None.
        plot : Boolean, optional
            If making plot or not. The default is False.
        index_x: 0
        index_y: 5

    Returns
    -------
    None.

    '''
    data = np.loadtxt(fname)
    FitSlope(data[:,index_x], data[:,index_y]*scale_x, *args, plot = True, **kwargs)

#%% An example on the transport of beam in a dogleg
#####
'''
workdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim\2020\BioRad\Case2\ARCScan3'
os.chdir(workdir)

lq = 0.0675
Rquad = 2.15e-1 # cm

rho, theta = 0.4, np.pi/3.0
beta1, beta2, gap = np.pi/6.0, np.pi/6.0, 0.05

sigma0, P0 = SigmaMatrix6D('ast.2550.007')
P0 *= 1e3; print('P0 = ', P0, ' MeV/c')

gamma = kinetic2gamma(M2K(P0))

### Solution 1:
l1, l2, l3 = 0.2573, 0.0427, 0.0846
l3 = 0.384615-l1-l2
g1, g2 = -1.42020/2.15e-1, 3.14620/2.15e-1
###

### Solution 10: with rectangular pole shape, Achromatic, xx'=0, yy'=0
l1, l2, l3 = 0.62155, 0.2, 0.1 
g1, g2 = 0.88/Rquad, -0.56175/Rquad
###

k1, k2 = G2K(g1, P0), G2K(-g2, P0)
x = [k1, k2, l1, l2, l3]

l0 = 0.5
D0 = Drift(l0)
Dip1 = Sector(rho, theta, beta1, beta2, gamma, gap)

D1 = Drift(l1)
Q1 = QuadF(k1, lq)

D2= Drift(l2)
Q2 = QuadD(k2, lq)

D3 = Drift(l3)

D9 = Drift(l0)

# Unit conversion matrix for TRANSPORT
TR = np.array([[1,  0.1, 1,  0.1, 1,  1],
               [10, 1,   10, 1,   10, 10],
               [1,  0.1, 1,  0.1, 1,  1],
               [10, 1,   10, 1,   10, 10],
               [1,  0.1, 1,  0.1, 1,  1],
               [1,  0.1, 1,  0.1, 1,  1]])
TR = np.ones(6)

D0, Dip1, D1, Q1, D2, Q2, D3, D9 = [TR*np.array(M.evalf(5), dtype = 'double')
                                for M in [D0, Dip1, D1, Q1, D2, Q2, D3, D9]]

# Matrix from dipole entrance to the plane of symmetry
M1 = D3 @ Q2 @ D2 @ Q1 @ D1 @ Dip1

# Matrix used to calculate the transfer matrix of a transfer line
# which is anti-symmetric to a known one
TS = np.diag([-1, 1, 1, -1, -1, 1]) # TS = TS^-1

# Matrix from the plane of symmetry to the dipole exit
# M2 = TS @ np.linalg.inv(M1) @ TS

# Matrix of the second dipole of a dogleg
Dip2 = TS @ np.linalg.inv(Dip1) @ TS

# Overall transfer matrix of a dogleg
M1_inv = np.linalg.inv(M1)
Mx = TS @ M1_inv @ TS @ M1


sigma, transfer = Transport6D(sigma0, D0, Dip1, D1, Q1, D2, Q2, D3,
                    D3, Q2, D2, Q1, D1, Dip2, D9)

# Lengths of the elements in a transfer line
ss = np.array([0, l0, rho*theta, l1, lq, l2, lq, l3, 
               l3, lq, l2, lq, l1,rho*theta, l0])
for i in np.arange(1, len(ss)):
    ss[i] += ss[i-1]
    
def Demo(sigma, transfer = None, fig_ext = '.png'):
    res = []
    for s in sigma:
        xemit = np.sqrt(np.linalg.det(s[0:2,0:2]))*gamma
        yemit = np.sqrt(np.linalg.det(s[2:4,2:4]))*gamma
        res.append([s[i,i]**0.5 for i in np.arange(5)]+[xemit, yemit])
    res = np.array(res)
    
    #reset_margin(bottom=0.1, top=0.9)
    fig, [ax1, ax2, ax3]= plt.subplots(nrows = 3, figsize = (4.5, 5),
                                       sharex = True)
    
    for i in [0, 2, 4]:
        ax1.plot(ss, res[:,i])
    ##    ax2.plot(res[:,i])
    
    for i in [5, 6]:
        ax2.plot(ss, res[:,i])   
    
    #ax3.plot(ss, res[:,0]**2/res[:,5]*gg)
    #ax3.plot(ss, res[:,2]**2/res[:,6]*gg)
    
    if transfer is not None:
        R16 = [ma[0,5] for ma in transfer[1:-1]]
        R26 = [ma[1,5] for ma in transfer[1:-1]]
        R56 = [ma[4,5] for ma in transfer[1:-1]]
        
        ax32 = ax3.twinx()
        ax32.plot(ss[1:-1], R16, 'c')
        ax32.plot(ss[1:-1], R26, 'm')
        ax32.plot(ss[1:-1], R56, 'k')
        ax32.set_ylabel(r'$D$', labelpad=-2)
        ax32.legend([r'$R_{16}$', r'$R_{26}$', r'$R_{56}$'],
                    loc = 'upper right')
        ax32.set_ylim(-1, 1)
        #ax32.set_yticks([-0.6, -0.3, 0, 0.3, 0.6])
    
    ax3.set_xlabel(r'$s$ (m)')
    ax1.set_ylabel(r'RMS (mm)', labelpad=3)
    ax2.set_ylabel(r'Emit. ($\mu$m)', labelpad=-2)
    ax3.set_ylabel(r'$\beta_{x, y}$ (m)')
    ax1.set_ylim(0, 4)
    ax3.set_xlim(0, 4)
    ax3.set_ylim(0, 20)
    ax1.grid()
    ax2.grid()
    ax3.grid()
    
    ax2.set_yscale('log')
    ax1.legend([r'$x$', r'$y$', r'$z$'], loc = 'upper left', ncol=2)
    ax2.legend([r'$x$', r'$y$'], loc = 'upper left')
    ax3.legend([r'$x$', r'$y$'], loc = 'upper left')        
    
    plt.tight_layout(h_pad = 0)
    #ig.savefig('Transport-'+fig_ext)
    
    return
Demo(sigma, transfer, 'dispersion-free10-edge-focusing.png')
'''
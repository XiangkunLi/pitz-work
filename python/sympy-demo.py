# -*- coding: utf-8 -*-
"""
Created on Mon Jul 27 12:39:59 2020

@author: lixiangk
"""

from numpy import *
from sympy import *

from universal import *
from transfer import *

Drift = lambda x: Matrix([[1, x, 0], [0, 1, 0], [0, 0, 1]])

def Dipole(rho, theta):
    Mdip = Matrix([[cos(theta), rho*sin(theta), rho*(1-cos(theta))],
               [-1.0/rho*sin(theta), cos(theta), sin(theta)],
               [0, 0, 1]])
    return Mdip

def QuadF(K, L):
    
    '''
    Parameters
      K: focal strength, m^-2
      L: effective length, m
    '''
    
    kk = sqrt(K); kl = kk*L
    cc = cos(kl); ss = sin(kl)
    tr = Matrix([[cc, 1./kk*ss, 0], [-kk*ss, cc, 0], [0, 0, 1]])
    
    return tr

def QuadD(K, L):
    
    '''
    Parameters
      K: focal strength, m^-2
      L: effective length, m
    '''
    
    kk = sqrt(K); kl = kk*L
    cc = cosh(kl); ss = sinh(kl)
    tr = Matrix([[cc, 1./kk*ss, 0], [kk*ss, cc, 0], [0, 0, 1]])
    
    return tr

#%%
rho, theta, l1, k1, l2, k2, l3 = symbols('rho, theta, l1, k1, l2, k2, l3');

lq = 0.0675
l2 = 1.03923/2-lq-l1
rho, theta = 0.6, pi/3.0

Dip1x = Dipole(rho, theta)
Dip1y = Drift(rho*theta)

D1 = Drift(l1)
D2= Drift(l2)
D3 = Drift(l3)

Q1x = QuadF(k1, lq)
Q2x = QuadD(k2, lq)

Q1y = QuadD(k1, lq)
Q2y = QuadF(k2, lq)

Dx = D2*Q1x*D1*Dip1x
Dx02 = Dx[0,2]
#Dx02 = simplify(Dx0)

func = lambdify((k1, l1), Dx02, 'numpy')

kk = linspace(60, 100)

ff = func(kk, 0.2)
plt.plot(kk, ff)
plt.grid()

popt, pcov = curve_fit(linear, ff, kk)
k0 = linear(0, *popt)
print(k0)

P0 = 19.355
G0 = K2G(k0, P0)
print(G0)

#%%
rho, theta, l1, k1, l2, k2, l3 = symbols('rho, theta, l1, k1, l2, k2, l3');

lq = 0.0675
#l3 = 1.03923/2-lq*2-l1-l2
#l3 = 0.6-l1-l2

rho, theta = 0.6, pi/3.0

Dip1x = Dipole(rho, theta)
Dip1y = Drift(rho*theta)

D1 = Drift(l1)
D2= Drift(l2)
D3 = Drift(l3)

Q1x = QuadD(k1, lq)
Q2x = QuadF(k2, lq)

Q1y = QuadF(k1, lq)
Q2y = QuadD(k2, lq)

Dx = D3*Q2x*D2*Q1x*D1*Dip1x
Dy = D3*Q2y*D2*Q1y*D1*Dip1y

f1 = lambdify((k1, k2, l1, l2, l3), Dx[0,2], 'numpy')
f2 = lambdify((k1, k2, l1, l2, l3), Dx[0,0], 'numpy')
f3 = lambdify((k1, k2, l1, l2, l3), Dx[1,1], 'numpy')

f4 = lambdify((k1, k2, l1, l2, l3), Dy[0,0], 'numpy')
f5 = lambdify((k1, k2, l1, l2, l3), Dy[1,1], 'numpy')

fDx = lambdify((k1, k2, l1, l2, l3), Dx, 'numpy')
fDy = lambdify((k1, k2, l1, l2, l3), Dy, 'numpy')

def obj_PItransform(x):
    r = [np.abs(f1(*x)), np.abs(f2(*x)), np.abs(f3(*x)), np.abs(f4(*x)), np.abs(f5(*x))]
    return r, [x[2]+x[3]-0.384615]

k10, k20, l10, l20  = 82.24252597864945, 1e-9, 0.2, 0.1
l30 = 1.03923/2-lq*2-l10-l20
x0 = [k10, k20, l10, l20, l30]

Dx0 = fDx(*x0)
print(Dx0)

#%%
Dx0 = fDx(*x)
Dy0 = fDy(*x)
Mx = np.array([[1+2*Dx0[0,1]*Dx0[1,0], 2*Dx0[0,1]*Dx0[1,1], 2*Dx0[0,2]*Dx0[1,1]],
               [2*Dx0[0,0]*Dx0[1,0], 1+2*Dx0[0,1]*Dx0[1,0], 2*Dx0[0,2]*Dx0[1,0]],
               [0, 0, 1]])
My = np.array([[1+2*Dy0[0,1]*Dy0[1,0], 2*Dy0[0,1]*Dy0[1,1], 0],
               [2*Dy0[0,0]*Dy0[1,0], 1+2*Dy0[0,1]*Dy0[1,0], 0],
               [0, 0, 1]])

import sys
sys.path.append('/afs/ifh.de/group/pitz/data/lixiangk/work/sync/python')

from tracker import *

workdir = r"\\afs\ifh.de\group\pitz\data\lixiangk\work\sync\python\space-charge"
os.chdir(workdir)

field_maps = r'\\afs\ifh.de\group\pitz\data\lixiangk\work\sync\field-maps'
gun3d = build_cavity(field_maps+os.sep+'gun42Cavity.txt', 0, 39.44e6, 1.3e9, 36.94)
boo3d = build_cavity(field_maps+os.sep+'CDS14_15mm.txt', 2.675, 15.27e6, 1.3e9, 184.59)
sol3d = build_solenoid(field_maps+os.sep+'gunsolenoidsPITZ.txt', 0, -0.158797) # 270 A

em3d = EMsolver()
em3d.add_external_field('gun', gun3d)
#em3d.add_external_field('booster', boo3d)
em3d.add_external_field('solenoid', sol3d)
#import pdb; pdb.set_trace()

import timeit

beam = Beam('beam1.ini', debug = 0); # beam.dist=beam.dist[0:100000]
track = Tracking(beam, em3d, 1000e-12, 1e-12, dump_interval = 250, Lspch = True, Run = 5)

n_steps = 6000
n_stepped = 0
stime = 0
zmean = 0
while n_stepped<n_steps and zmean < 0.25:
    time1 = timeit.default_timer()
    #import pdb; pdb.set_trace()
    if track.is_emit():
        flag = 'Emitting'
        nstep = 1
        
        kwargs = dict(Lemit = True, Lmirror = True)
        if n_stepped % 250 == 0:
            kwargs = dict(Lemit = True, Lmirror = True, fig_ext = '.%04d.eps' % n_stepped)
            
        if track.Lspch is True and track.stepped>5:
            sc3d = track.beam.get_sc3d(**kwargs)
            track.cathode(['spacecharge', 'gun'], 'trk.cathode')
            track.em3d.update_external_field('spacecharge', sc3d)
        
        track.emit(nstep, NMIN = 50, **kwargs)
    else:
        flag = 'Tracking'
        nstep = 1
        
        kwargs = dict(Lemit = False, Lmirror = True)
        if track.Lspch is True and track.stepped>5:
            sc3d = track.beam.get_sc3d(**kwargs)
            track.em3d.update_external_field('spacecharge', sc3d)
        
        track.step(nstep, **kwargs)
    plt.close('all')
    zmean = track.beam.x[0]
    
    time2 = timeit.default_timer()
    stime = stime+(time2-time1)

    n_stepped += nstep

    if n_stepped % 100 == 0:
        print 'Current -> ', flag, ': on average 1 step takes: ', stime/n_stepped

print 'Total time:', stime
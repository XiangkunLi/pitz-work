# -*- coding: utf-8 -*-
"""
Created on Fri Feb 28 17:20:23 2020

@author: lixiangk
"""

from consts import * 

import numpy as np
import scipy as sp
from scipy.optimize import curve_fit
from scipy.interpolate import interp1d

import matplotlib as mpl
import matplotlib.pyplot as plt

#from IPython.display import Image
import os
import re

from scipy.special import jn, jn_zeros,j0,j1
from numpy.fft import fftshift,fft

#%%
def undulator_parameter(B, lam_u):
    '''
    Calculate undulator parameters from `B` and `lam_u`
    Parameters
      B: peak magnetic field in nit of Tesla
      lam_u: undulator period in unit of meter
    Returns
      K: undulator parameter
    '''
    return 0.933728997664305*B*lam_u*1e2

def undulator_field(K, lam_u):
    '''
    Calculate undulator parameters from `B` and `lam_u`
    Parameters
      B: peak magnetic field in nit of Tesla
      lam_u: undulator period in unit of meter
    Returns
      K: undulator parameter
    '''
    return K/(0.933728997664305*lam_u*1e2)

def resonant_wavelength(K, lam_u, gamma):
    '''
    Calculate the resonant wavelength
    Parameters
      K: undulator parameter
      lam_u: undulator period in unit of meter
      gamma: energy in unit of mec2
    Returns
      lam_s: radiation wavelength, meter
    '''
    return lam_u/2./gamma**2*(1+K**2/2)

def resonant_undulator_parameter(lam_s, lam_u, gamma):
    return np.sqrt(2.*(2*gamma**2*lam_s/lam_u-1))

def resonant_energy(K, lam_u, lam_s):
    '''
    Calcualte the resonant energy given the wavelength
    Parameters
      K: undulator parameter
      lam_u: undulator period
      lam_s: radiation wavelength
    Returns
      gamma: Lorentz factor of particles with the resonant energy 
    '''
    return np.sqrt(lam_u/2./lam_s*(1+K**2/2.))

def JJ2(K, n = 0):
    '''
    Parameters
      K: undulator parameter
      m=2n+1: harmonic number
    Returns
      [J_n(x)-J_n+1(x)]^2
    '''
    m = 2*n+1
    x = 1.0*m*K**2/(4+2*K**2)
    return (jn(n,x)-jn(n+1,x))**2
def ss(Nu, y):
    '''
    sin^2(Nu*y)/sin^2(y)
    '''
    if(y==0):
        return Nu**2
    else:
        return np.sin(Nu*y)**2/np.sin(y)**2
    
def lambda2omega(lam):
    return 2*np.pi*g_c/lam

def form_factor(lam_s, sig_t):
    '''
    Parameters
      lam_s: radiation wavelength, m
      sig_t: rms bunch length, second
    Returns
      form factor
    '''
    sig_z = sig_t*g_c
    return np.exp(-2*np.pi**2*sig_z**2/lam_s**2)



class Undulator():
    Nu = NotImplemented
    lam_u = NotImplemented
    B = NotImplemented
    K = NotImplemented
    def __init__(self, **kwargs):
        if self.B is not NotImplemented and self.lam_u is not NotImplemented:
            self.K = undulator_parameter(self.B, self.lam_u)
        else:
            pass
        self.set(**kwargs)
    def set(self, **kwargs):
        if len(kwargs):
            for key in kwargs.keys():
                self.__dict__[key] = kwargs[key]
                
            if 'B' in kwargs.keys() and 'lam_u' is not NotImplemented:
                self.K = undulator_parameter(self.B, self.lam_u)
            if 'K' in kwargs.keys() and 'lam_u' is not NotImplemented:
                self.B = undulator_field(self.K, self.lam_u)
                
class LCLS1(Undulator):
    Nu = 113
    lam_u = 3e-2
    B = 1.28
    def __init__(self, **kwargs):
        super(LCLS1, self).__init__()  
        self.set(**kwargs)
    
class FEL():
    Alfven = 17e3
    scheme = NotImplemented
    def __init__(self, und = LCLS1(), gamma = 0, lam_s = 0, **kwargs):
        
        self.und = und
        
        if gamma:
            self.gamma = gamma
            self.lam_s = resonant_wavelength(und.K, und.lam_u, gamma)
            self.freq_s = g_c/self.lam_s
        elif lam_s:
            self.lam_s = lam_s
            self.gamma = resonant_energy(und.K, und.lam_u, lam_s)
            self.freq_s = g_c/self.lam_s
        else:
            pass
        
        
class Superradiance(FEL):
    scheme = 'SR'
    def __init__(self, Qtot, Ipeak = 0, sig_t = 0, dist = 'Gaussian',
                 und = LCLS1(), gamma = 0, lam_s = 0, **kwargs):
        super(Superradiance, self).__init__(und = und, gamma = gamma,
              lam_s = lam_s, **kwargs)
        self.Qtot = Qtot
        
        if dist == 'Gaussian':
            if Ipeak:
                self.I = Ipeak
                self.sig_t = Qtot/Ipeak/np.sqrt(2.0*np.pi)
                self.FWHM = self.sig_t*2.355
                self.sig_z = self.sig_t*g_c
            elif sig_t:
                self.sig_t = sig_t
                self.I = Qtot/self.sig_t/np.sqrt(2.0*np.pi)
                self.FWHM = self.sig_t*2.355
                self.sig_z = self.sig_t*g_c
        elif dist == 'Flattop':
            if Ipeak:
                self.I = Ipeak
                self.sig_t = Qtot/Ipeak/np.sqrt(12.0)
                self.FWHM = Qtot/Ipeak
                self.sig_z = self.sig_t*g_c
            elif sig_t:
                self.sig_t = sig_t
                self.I = Qtot/self.sig_t/np.sqrt(12.0)
                self.FWHM = Qtot/self.I
                self.sig_z = self.sig_t*g_c
        else:
            print('Unknow distribution')
        
    def undulator_radiation(self, ww, w1, n = 0):
        '''
        The spectral energy density per electron of the radiation emittied forward
        direction for the m-th harmonic
        Parameters
          Nu: number of period
          K: undulator parameter
          ww: angular frequency of interest
          w1=2*pi*c/lamda
          m=2n+1: harmonic number
        Returns
          
        '''
        
        gamma, Nu, K = self.gamma, self.und.Nu, self.und.K
        
        m = 2*n+1
        wm = w1*m
        y = np.pi*(ww-wm)/w1
        
        c1 = g_qe**2*gamma**2*m**2*K**2/4.0/np.pi/g_eps0/g_c/(1+K**2/2)**2
        return c1*ss(Nu, y)*JJ2(K, n)
    
    def angular_width(self, n = 0):
        '''
        
        '''
        
        gamma, Nu, K = self.gamma, self.und.Nu, self.und.K
        m = 2*n+1
        return 2*np.pi/gamma**2/2./m/Nu*(1+K**2/2)

    def frac_bandwidth(self, w1, n = 0):
        '''
        
        '''
        
        Nu = self.und.Nu
        m=2*n+1
        wm=w1*m
        return 1.0/m/Nu*wm
    
    def radiation(self, ww, w1, n = 0):
        '''
        
        '''
        
        gamma, Nu, K = self.gamma, self.und.Nu, self.und.K
        return self.undulator_radiation(ww, w1, n)*self.angular_width(n)*self.frac_bandwidth(w1, n)
    
    def radiation_from_bunch(self, ww, w1, n = 0):
        '''
        
        '''
        
        gamma, lam_s = self.gamma,  self.lam_s
        Qtot, sig_t  = self.Qtot,   self.sig_t
        
        Ne = Qtot/g_qe
        ff2 = form_factor(lam_s, sig_t)**2
        self.ff2 = ff2
        return self.radiation(ww, w1, n)*(Ne+Ne*(Ne-1)*ff2)

class SASE(Superradiance):
    scheme = 'SASE'
    def __init__(self, Qtot, Ipeak, dist = 'Gaussian', sig_x = 0,
                 und = LCLS1(), gamma = 0, lam_s = 0, **kwargs):
        super(SASE, self).__init__(Qtot, Ipeak, dist,
             und = und, gamma = gamma, lam_s = lam_s, **kwargs)
        
        self.sig_x = sig_x
        self.pierce_parameter()
        self.gain_length()
        self.cooperation_length()
        
    def pierce_parameter(self):
        '''
        Parameters
          K: undulator parameter
          gamma: energy of electron bunch, mec2
          lam_u: undulator period, meter
          B: magnetic field amplitude, Tesla
        Returns
          rho: pierce parameter, unitless
        '''
        
        gamma, I, sigma_x = self.gamma, self.I, self.sig_x
        K, lam_u = self.und.K, self.und.lam_u
        
        ku = 2*np.pi/lam_u
        
        rho = (1./16*I/self.Alfven*K**2*JJ2(K)/gamma**3/sigma_x**2/ku**2)**(1./3)
        self.rho = rho
        
    def gain_length(self):
        '''
        Parameters
          rho: pierce parameter
          lam_u: undulator period
        Returns
          Lg: gain length, meter
        '''
        
        lam_u, rho = self.und.lam_u, self.rho
        Lg = lam_u/4./np.pi/np.sqrt(3)/rho
        self.Lg = Lg
    
    def cooperation_length(self):
        '''
        Parameters
          rho: pierce parameter
          lam_s: radiation wavelength
        Returns
          Lc: cooperation length, meter
        '''
        
        lam_s, rho = self.lam_s, self.rho
        Lc = lam_s/4./np.pi/rho
        self.Lc = Lc
        
#%% Example
Nu, lam_u = 5, 10e-2

Ek = 20 # MeV
gamma = 1+Ek/g_mec2

Qe = 68e-12
sig_t = 250e-15/2.355 # fs
sig_z = sig_t*g_c

lamb = np.linspace(100e-6, 500e-6, 41)

rr=[]
for i, ll in enumerate(lamb):
    
    w1 = 2*np.pi*g_c/ll
    ww = w1
    ff2 = form_factor(ll, sig_t)**2
    
    KK = resonant_undulator_parameter(ll, lam_u, gamma)
    und = Undulator(Nu = Nu, lam_u = lam_u, K = KK)
    print(und.B)
    
    SR = Superradiance(Qe, sig_t = sig_t, lam_s = ll, und = und)
    
    nj = SR.radiation_from_bunch(ww, w1)*1e9
    rr.append([ll, nj, ff2])
    
rr = np.array(rr)

fig, ax = plt.subplots(nrows = 1, ncols = 2, figsize = (12,4))
#ax[0].plot(rr[:,0]*1e6, rr[:,1], '-', label = 'energy')
ax[0].plot(g_c/rr[:,0]/1e12, rr[:,1], 'r-', label = r'FWHM = 250 fs')
ax[1].plot(g_c/rr[:,0]/1e12, rr[:,2], 'b-', label = 'form factor')

ax[0].legend(fontsize = 11)
#ax[0].set_yscale("log")
ax[0].set_xlabel(r'radiation frequency (THz)')
ax[0].set_ylabel(r'radiation energy (nJ)')

#%%
und = LCLS1()

lam_s = 100e-6
gamma = resonant_energy(und.K, und.lam_u, lam_s)

Qe = np.linspace(10, 100, 200)*1e-12
sig_z = np.linspace(0.05e-3, 0.2e-3, 200)
sig_t = sig_z/g_c

E = np.zeros((len(Qe), len(sig_t)))
for i, q in enumerate(Qe):
    for j, s in enumerate(sig_t):
        w1 = 2*np.pi*g_c/lam_s
        ww = w1
        SR = Superradiance(q, sig_t = s, und = und, gamma = gamma, lam_s = lam_s)
        E[i,j] = SR.radiation_from_bunch(ww, w1)*1e9

#%%
extent = [sig_t.min()*1e15, sig_t.max()*1e15, Qe.min()*1e12, Qe.max()*1e12]
print(E.min(), E.max())

from matplotlib.colors import LogNorm

fig, ax = plt.subplots()
cax = ax.imshow(E[::-1,:], extent = extent, aspect = 'auto')#,
#                norm=LogNorm(vmin=1, vmax=100))

cbar = fig.colorbar(cax, fraction = 0.046, pad = 0.04)
cbar.ax.tick_params(labelsize = 11, pad = 2)
#cbar.ax.set_yscale("log")

#%%
SR = Superradiance(10e-12, Ipeak = 20, und = und, gamma = gamma, lam_s = lam_s)
SR.radiation_from_bunch(ww, w1)*1e9
#%%
from universal import *
#path = get_path()
os.chdir(path)

fname = get_file()
data = np.loadtxt(fname)

und = LCLS1()

lam_s = 100e-6
gamma = resonant_energy(und.K, und.lam_u, lam_s)

w1 = 2*np.pi*g_c/lam_s
ww = w1

res = []
for i in np.arange(len(data)):
    Qtot = data[i,0]*1e-12
    sig_t = data[i,1]/g_c
    
    SR = Superradiance(Qtot, sig_t = sig_t, und = und, gamma = gamma, lam_s = lam_s)
    res.append([Qtot*1e12, sig_t*1e15, SR.I, SR.radiation_from_bunch(ww, w1)*1e9, SR.ff2])
res = np.array(res)    
#%%
fig, [ax1, ax2] = plt.subplots(ncols = 2, figsize = (8, 4))
ax1.plot(res[:,0], res[:,3], '-*')
ax2.plot(res[:,0], res[:,4], '-D')
ax2.set_yscale("log")
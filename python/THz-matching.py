# -*- coding: utf-8 -*-
"""
Created on Wed Mar  3 00:10:39 2021

@author: lixiangk
"""

#%%

# from IPython.display import Image
import numpy as np
import scipy as sp
from scipy.interpolate import interp1d, interp2d
from scipy.optimize import curve_fit

from scipy.special import jn, jn_zeros, j0, j1
from scipy.integrate import quad, ode

from scipy.constants import codata

import matplotlib as mpl
import matplotlib.pyplot as plt
from cycler import cycler
from matplotlib.ticker import AutoMinorLocator

import os, re

from timeit import default_timer
import time


def Gx2I(G):
    if G<0:
        r = -1.4578783*G+0.11869394
    else:
        r = -1.45460901*G-0.10700756
    return r

def Gy2I(G):
    if G>0:
        r = 1.40239547*G+0.1401052
    else:
        r = 1.41152547*G-0.14604085
    return r

def G2I(G):
    return 0.5*(Gx2I(G)+Gy2I(-G))


workdir = r'C:\Users\lixiangk\Desktop\THz-transport'
os.chdir(workdir)

h2p = np.loadtxt('High1-to-PST.dat')
p2h = np.loadtxt('PST-to-High1.dat')

#
f1 = interp1d(h2p[:,-1], h2p[:,-2], fill_value = 'extrapolate', bounds_error = False) # y is beam size at High1.Scr4
f2 = interp1d(p2h[:,5], p2h[:,3], fill_value = 'extrapolate', bounds_error = False) # y is beam size at High1.Scr4


tmp_x = np.linspace(1., 2)
tmp_y = f1(tmp_x)-f2(tmp_x)
f0 = interp1d(tmp_y, tmp_x, fill_value = 'extrapolate', bounds_error = False) # y is beam size at PST.Scr1

matched_rms_pst = f0(0)
matched_rms_high = f1(f0(0))
print('intersection: %.2f mm at High1.Scr4 and %.2f mm at PST.Scr1' % (matched_rms_high, matched_rms_pst))

fI_Q4 = interp1d(h2p[:,3], h2p[:,0], fill_value = 'extrapolate', bounds_error = False) # rms at H1S4 vs I_Q4
I_Q4 = fI_Q4(matched_rms_high)
print('High1.Q4 current is ', I_Q4, ' A')

fI_Q6 = interp1d(h2p[:,3], h2p[:,1], fill_value = 'extrapolate', bounds_error = False) # rms at H1S4 vs I_Q4
I_Q6 = fI_Q6(matched_rms_high)
print('High1.Q6 current is ', I_Q6, ' A')

fI_Q7 = interp1d(h2p[:,3], h2p[:,2], fill_value = 'extrapolate', bounds_error = False) # rms at H1S4 vs I_Q4
I_Q7 = fI_Q7(matched_rms_high)
print('High1.Q7 current is ', I_Q7, ' A')

fI_QM2 = interp1d(p2h[:,3], p2h[:,0], fill_value = 'extrapolate', bounds_error = False) # rms at H1S4 vs I_QM2
I_QM2 = G2I(fI_QM2(matched_rms_high))
print('PST.QM2 current is ', I_QM2, ' A')

fI_QM3 = interp1d(p2h[:,3], p2h[:,1], fill_value = 'extrapolate', bounds_error = False) # rms at H1S4 vs I_QM2
I_QM3 = G2I(fI_QM3(matched_rms_high))
print('PST.QM2 current is ', I_QM3, ' A')

fI_QT1 = interp1d(p2h[:,3], p2h[:,2], fill_value = 'extrapolate', bounds_error = False) # rms at H1S4 vs I_QT1
I_QT1 = G2I(fI_QT1(matched_rms_high))
print('PST.QM2 current is ', I_QT1, ' A')

#%%

workdir = r'scan-PST.Scr3-to-PST.Scr1-new\Q1-1.95T_m-Q2--3.53T_m-Q3-1.49T_m'
workdir = r'scan-PST.Scr3-to-PST.Scr1-new\Q1-2.00T_m-Q2--3.61T_m-Q3-1.56T_m'
os.chdir(workdir)

fig, ax = plt.subplots(figsize = (6, 3))

data = np.loadtxt('BeamDynamics-backward.dat')
ax.plot(data[:,0]+8.41, data[::-1,3], 'r-')
ax.plot(data[:,0]+8.41, data[::-1,4], 'b-')
ax.plot([], [], 'r:<')
ax.plot([], [], 'b:>')

data = np.loadtxt('BeamDynamics-forward.dat')
ax.plot(data[:,0]+13.798, data[:,3], 'r-')
ax.plot(data[:,0]+13.798, data[:,4], 'b-')

#data = np.loadtxt('matched1-1.txt')
data = np.loadtxt('matched2-2.txt')

ax.plot(data[:,0], data[:,1], 'r--<')
ax.plot(data[:,0], data[:,2], 'b-->')


# data = np.loadtxt('matched1-2.txt')
# data = np.loadtxt('matched2-2.txt')

# ax.plot(data[:,0], data[:,2], 'r:<')
# ax.plot(data[:,0], data[:,1], 'b:>')

ax.grid()
ax.set_xlabel(r'$z$ (m)')
ax.set_ylabel('RMS size (mm)')
ax.legend(['Simulated, $x$', 'Simulated, $y$', 'Measured, $x$', 'Measured, $y$'],
          ncol = 1, fontsize = 11, loc = 'lower left', handlelength = 2)

fig.savefig('matching2-2.png')

os.chdir('../..')
#%%
data = pd_loadtxt('../ast.2745.001')

data[:,3] *= -1
data[:,4] *= -1
data[1:,5] *= -1

np.savetxt('../forward.2745.001', data, ast_fmt)

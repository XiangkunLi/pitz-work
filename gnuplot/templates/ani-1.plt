reset

set term gif animate delay 10

set grid
set title "standing wave"
set title "traveling wave"

set xrange [0:20]
set yrange [-1.2:1.2]

set style line 1 lt 1 lw 1 lc rgb "#1220ad" pt 6 ps 1.5

set output "animated.gif"
i=0
n=360
load "animated.plt"

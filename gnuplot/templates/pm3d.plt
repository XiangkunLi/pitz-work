set terminal png enhanced size 1000,700

set lmargin at screen 0.2
set rmargin at screen 0.9
set bmargin at screen 0.2
set tmargin at screen 0.9

set samples 50,50
set isosamples 50,50

set xlabel 'x'
set ylabel 'y'

f(x,y)=sin(sqrt(x**2+y**2))/sqrt(x**2+y**2)

## 3d plot
#set view 45,45,1,1

set o '3d.png'
splot f(x,y)
set o

## pm3d
set pm3d
set hidden3d

set o 'pm3d.png'
splot f(x,y)
set o

## pm3d from above
set pm3d map
set pm3d interpolate 0,0
#set palette gray
set palette rgbformulae 33,13,10

set o 'pm3d-map.png'
splot f(x,y)
set o

## contour from above
unset pm3d
unset surface
set contour base
#set view map
set cntrparam levels incremental -0.4,0.2,1

set o 'contour.png'
splot f(x,y)
set o

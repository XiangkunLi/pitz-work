set terminal postscript eps enhanced color linewidth 1 font "Times-Roman,20" size 3.5,2.45

set key right samplen 1.6 spacing 0.8 width -3 font "Times-Roman,14"
#set key at graph 0.7,0.3

#f(x)=a+b*x
#fit f(x) 't.dat' u 1:2 via a,b

#set table 't1.dat'
#plot 't.dat' u 1:2
#set output

@x_label 'x'
@y_label 'y' #offset char 1.5,0

datafile=''

set o '.eps'
plot datafile u 1:2 w lp
set o

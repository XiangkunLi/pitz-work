reset
set term gif animate delay 10 # delay is 0.01 second

f(z,t)=cos(z)*sin(t/180.*pi)
f(z,t)=cos(z-t/180.*pi)

set xrange [0:20]
set yrange [-1.2:1.2]

set grid
set title "traveling wave"

set output "ani-2.gif"
do for [i=0:360:10]{
  plot f(x,i) with l ls 1 title sprintf("%3.0f degree",(i))
}
set output


load "gnuplot.ini"


#set terminal postscript eps enhanced color linewidth 1 font "Times-Roman,20" size 3.5,2.45

set terminal png

#set logscale y

set grid

set key right samplen 1.6 spacing 0.8 width 0 font "Times-Roman,14"

if (exists("path")){
} else {
  path='.'
}

if (exists("data")){
} else{
  data='beam.ini'
}

datafile=path.'/'.data


#stats datafile u 3:6 every ::0::1 nooutput
#zm=STATS_max_x
#pm=STATS_max_y
#print zm,pm
zm=0

stats datafile u 7 every ::0::1 nooutput
tm=STATS_mean
print tm



#data0=sprintf("< tail -n +2 %s | gawk \"{print $$0}\"",datafile)
#data1=sprintf("< tail -n +2 %s | gawk \"{print $$0}\"",datafile)
#data2=sprintf("< tail -n +2 %s | gawk \"{print $$0}\"",datafile)

#fit g_fit(x) datafile u (hist((column(3)+zm)*1000,0.02)):(1.0) every ::1 smooth freq via uu,ss,aa

@x_label 'x (mm)'
@y_label 'counts'
figure=path.'/'.'x@'.data
set output figure.'.png'
plot datafile u (hist((column(1)+0)*1000,0.02)):(1.0) every ::1 smooth freq notitle w boxes
set o


@x_label 'y (mm)'
@y_label 'counts'
figure=path.'/'.'y@'.data
set output figure.'.png'
plot datafile u (hist((column(2)+0)*1000,0.02)):(1.0) every ::1 smooth freq notitle w boxes
set o

@x_label 'z (mm)'
@y_label 'counts'
figure=path.'/'.'z@'.data
set output figure.'.png'
plot datafile u (hist((column(3)+zm)*1000,0.02)):(1.0) every ::1 smooth freq notitle w boxes
set o



@x_label 't (ps)'
@y_label 'counts'
figure=path.'/'.'t@'.data
set output figure.'.png'
plot datafile u (hist((column(7)+tm)*1000,1)):(1.0) every ::1 smooth freq notitle w boxes
set o


@x_label 'x (mm)'
@y_label 'y (mm)'
figure=path.'/'.'xy@'.data
set output figure.'.png'
plot datafile u ($1*1e3):($2*1e3) every ::1 notitle w p ps 0.1
set o

#system('convert -density 150 '.figure.'.eps '.figure.'.png')

load "gnuplot.ini"


#set terminal postscript eps enhanced color linewidth 1 font "Times-Roman,20" size 3.5,2.45

set terminal png

#set key right samplen 1.6 spacing 0.8 width -3 font "Times-Roman,14"
#set key at graph 0.7,0.3


set grid

@y_label 'rms emittance (mm mrad)' #u_emittance_x #offset char 1.5,0

#set yrange [0:10]
#set yrange []
@x_label 'z (m)'
set o 'xemit-z.png'
plot 'ast.Xemit.001' u 1:6 w l title 'x','ast.yemit.001' u 1:6 w l title 'y'
set o

#set yrange [0:3]
@y_label 'rms beam size (mm)' #u_rms_x
set o 'xrms-z.png'
plot 'ast.Xemit.001' u 1:4 w l title 'x','ast.Yemit.001' u 1:4 w l title 'y','ast.Zemit.001' u 1:4 w l title 'z'
set o

@y_label 'relative energy spread (%)' #u_energy_spread_percentage
data='ast.Zemit.001'
set o 'DE-z.png'
plot data u 1:($5/$3/10) w l notitle
set o

@y_label u_energy
data='ast.Zemit.001'
set o 'Ek-z.png'
plot data u 1:($3) w l notitle
set o

#system('magick -density 150 Ek_z.eps Ek_z.png')

quit

% !TeX spellcheck = en_GB
% !TeX program = lualatex
%
% v 2.3  Feb 2019   Volker RW Schaa
%		# changes in the collaboration therefore updated file "jacow-collaboration.tex"
%		# all References with DOIs have their period/full stop before the DOI (after pp. or year)
%		# in the author/affiliation block all ZIP codes in square brackets removed as it was not %         understood as optional parameter and ZIP codes had bin put in brackets
%       # References to the current IPAC are changed to "IPAC'19, Melbourne, Australia"
%       # font for ‘url’ style changed to ‘newtxtt’ as it is easier to distinguish "O" and "0"
%
\documentclass[a4paper,
               %boxit,        % check whether paper is inside correct margins
               %titlepage,    % separate title page
               %refpage       % separate references
               %biblatex,     % biblatex is used
               keeplastbox,   % flushend option: not to un-indent last line in References
               %nospread,     % flushend option: do not fill with whitespace to balance columns
               %hyphens,      % allow \url to hyphenate at "-" (hyphens)
               %xetex,        % use XeLaTeX to process the file
               %luatex,       % use LuaLaTeX to process the file
               ]{jacow}
%
% ONLY FOR \footnote in table/tabular
%
\usepackage{pdfpages,multirow,ragged2e} %
%
% CHANGE SEQUENCE OF GRAPHICS EXTENSION TO BE EMBEDDED
% ----------------------------------------------------
% test for XeTeX where the sequence is by default eps-> pdf, jpg, png, pdf, ...
%    and the JACoW template provides JACpic2v3.eps and JACpic2v3.jpg which
%    might generates errors, therefore PNG and JPG first
%
\makeatletter%
	\ifboolexpr{bool{xetex}}
	 {\renewcommand{\Gin@extensions}{.pdf,%
	                    .png,.jpg,.bmp,.pict,.tif,.psd,.mac,.sga,.tga,.gif,%
	                    .eps,.ps,%
	                    }}{}
\makeatother

% CHECK FOR XeTeX/LuaTeX BEFORE DEFINING AN INPUT ENCODING
% --------------------------------------------------------
%   utf8  is default for XeTeX/LuaTeX
%   utf8  in LaTeX only realises a small portion of codes
%
\ifboolexpr{bool{xetex} or bool{luatex}} % test for XeTeX/LuaTeX
 {}                                      % input encoding is utf8 by default
 {\usepackage[utf8]{inputenc}}           % switch to utf8

\usepackage[USenglish]{babel}

%
% if BibLaTeX is used
%
\ifboolexpr{bool{jacowbiblatex}}%
 {%
  \addbibresource{jacow-test.bib}
  \addbibresource{biblatex-examples.bib}
 }{}
\listfiles

%%
%%   Lengths for the spaces in the title
%%   \setlength\titleblockstartskip{..}  %before title, default 3pt
%%   \setlength\titleblockmiddleskip{..} %between title + author, default 1em
%%   \setlength\titleblockendskip{..}    %afterauthor, default 1em

\begin{document}

\title{Design studies of a proof-of-principle experiment on THz SASE FEL at PITZ\thanks{Work supported by ...}}

\author{X.-K. Li\thanks{xiangkun.li@desy.de}, M. Krasilnikov, P. Boonpornprasert, H. Shaker, Y. Chen, J. Good, M. Gross,\\ H. Huck,
I. Isaev, C. Koschitzki, S. Lal, O. Lishilin, G. Loisch, D. Melkumyan, R. Niemczyk,\\ A. Oppelt,
H. Qian, G. Shu, F. Stephan, G. Vashchenko, DESY, Zeuthen, Germany}
	
\maketitle

%
\begin{abstract}
   A free-electron laser based THz source is undergoing design studies at the Photo-Injector Test facility at DESY in Zeuthen (PITZ). It is considered as a prototype THz source for pump-probe experiments at the European XFEL, benefiting from the fact that the electron beams from the PITZ facility have an identical pulse train structure as the XFEL pulses. In the proposed proof-of-principle experiment, an electron beam (up to 4 nC and 200 A) will be accelerated to 16-22 MeV/c to drive an LCLS-I type undulator, generating SASE radiations in the THz band in the wavelength range between 60 and \SI{100}{µm} with an expected energy of up to 1 mJ/pulse. In this paper, we report our simulations on the optimization of the photo-injector and the design of the transport and matching beamline. Experimental investigations on the generation, characterization and matching of the high charge beam in the existing 22-m-long beamline will also be presented.
\end{abstract}


\section{INTRODUCTION}

As more user beamlines are put into use at the European X-ray free-electron laser facility (EuXFEL), pump-probe experiments have played a leading role in many research frontiers in biology, chemistry and condensed matters, etc. Among those researches are the THz-pump X-ray-probe experiments, in which a THz pulse is used to excite a sample and the following X-ray pulse is used to detect the reaction of the sample to the THz pulse~\cite{{terahertz-xfel}}. To provide the THz pump, proposals have been made at EuXFEL, for instance, by injecting the used electron beam into a THz undulator~\cite{tanikawa2018superradiant}. However, the high beam energy (up to 17.5 GeV) would require a meter-long period undulator made from super-conducting coils. The long-distance transportation of the THz pulses to the user hall raises another issue.

Another promising idea is to put a separate PITZ-like photo-injector near the user hall for driving the THz source~\cite{schneidmiller2013tunable, boonpornprasert2018calculations, krasilnikov2018start}. The advantages are: 1) it can produce the identical electron bunch train (therefore THz pulse train) as the X-ray pulses at EuXFEL and 2) it can be installed near the user hall and thus relieve the transportation issue. Previous studies have shown that milli-joule level THz SASE radiations could be generated from an electron bunch of 4 nC and 200 A in an Apple-II or LCLS-I undulator. And currently, proof-of-principle experiments funded by the EuXFEL is taking place at PITZ by using the existing PITZ beamline extended with an LCLS-I undulator. In this paper, we will report the progress of this project, including a simulation study based on the proposed whole beamline and experimental investigations on the generation, characterization, transport and matching of the high charge high peak current beam in the existing beamline.

\section{SIMULATIONS}

The photo-injector at PITZ consists of an L-band photocathode RF gun, a solenoid for emittance compensation, an L-band booster accelerator and many diagnostics devices and focusing quadrupoles with a total length of 22 m~\cite{krasilnikov2012experimentally}. To generate THz SASE FEL, the beamline will be extended by one or two LCLS-I undulators installed in a neighbouring tunnel annex.
Since the beam emittance is not critical for FELs in THz band, our major attention has been paid to the peak current and the energy spread. While a higher peak current means higher gain in the undulator, a smaller energy spread means better and easier beam transport as well as higher FEL gain. Considering these our photo-injector is first optimized for the peak current and the energy spread; two triplets are then selected for the beam transport. In addition, the small vacuum chamber (11-mm-wide and 5-mm-high) between the magnetic poles in the undulator posts another challenge. In order to avoid beam loss to the wall, the transverse phase spaces should be matched in a proper way. Therefore, another two triplets will be installed before the undulator and their gradients need to be optimized.

\subsection{Start-to-end simulation}

For the design study, we first optimized the photo-injector parameters and then with the optimized electron beam designed the beam transport line until the undulator entrance.
The capability of flattop laser shaping has been demonstrated before at PITZ~\cite{krasilnikov2012experimentally} and here we assume a flattop laser of 21.5 ps in full-width-half-maximum (FWHM). Such a long pulse will relieve the space charge force and is favorable for generating high peak current. The laser spot size can be changed by a tunable beam shaping aperture (BSA). The highest achievable gun gradient of 60 MV/m is taken into account to suppress the mirror charge during emission. With this gun gradient, the highest beam momentum out of the booster is about 22 MeV/c, which corresponds to a radiation wavelength of \SI{60}{µm} in the LCLS-I undulator. In this paper, we consider however the nominal wavelength of \SI{100}{µm} (3 THz in frequency) and the correponding beam momentum is 17.05 MeV/c.

With the particle tracking code Astra~\cite{astra}, the laser spot size, the accelerating phases in the gun and in the booster as well as the solenoid current were optimized by the differential-evolution (DE) algorithm in such a way that the beam energy spread was minimized at the undulator center while the beam emittance was relatively small (e.g., $\sim$ 4 mm mrad). It was found that the peak current was dependent on the laser spot size, as shown in Fig.~\ref{fig:optimization}(a). The increase in laser spot size reduces the space charge forces, especially near the cathode where the electrons have low kinetic energies. The density of the samples shows well the preference of a large laser spot size during the search.

\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=0.95\columnwidth, trim=0 15pt 0 15pt]{Peak-current-vs-BSA(a)}
   \end{minipage}%
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=0.95\columnwidth, trim=0 15pt 0 15pt]{Cor-energy-spread-vs-boosterPhase(b)}
   \end{minipage}
   \caption{Dependence of (a) peak current on laser spot size and (b) correlated energy spread on booster phase.}
   \label{fig:optimization}
\end{figure}

The beam energy spread was found dependent on the booster phase, as shown in Fig.~\ref{fig:optimization}(b). By accelerating the beam off-crest, the longitudinal phase space is chirped so that the tail has higher kinetic energy than the head has at the booster exit. Then in the following long drift, the head will be accelerated and the tail decelerated by space charge forces, therefore reducing the beam energy spread gradually until it reaches the minimum near the undulator. The gathering of the samples implied that the best booster phase is around -20 degrees.

\begin{figure}[!htb]
   \centering
   \includegraphics*[width=0.9\columnwidth, trim=0 5pt 0 15pt]{beam-emittance-with-quads-new++}
   \caption{Start-to-end simulation: (a) normalized emittances, (b) rms beam sizes and (c) rms energy spread and momentum along the beamline.}
   \label{fig:beam-transport}
\end{figure}

After optimizing the photo-injector, the beam transport line was designed. As mentioned above, the small vacuum chamber defines the transverse phase spaces horizontally and vertically at the undulator entrance~\cite{krasilnikov2018start}. These phase spaces are treated as the goal functions in the design of the transport line. As shown in Fig.~\ref{fig:beam-transport}(b), two triplets are used to smoothly focus the beam and two are used to match the beam into the undulator. The evolutions of normalized emittances, the beam energy and energy spread can be found in Fig.~\ref{fig:beam-transport}(a) and (c). The emittances stay small through the whole beamline and the minimum energy spread appears near the undulator. The transverse and longitudinal phase spaces at the undulator entrance are shown in Fig.~\ref{fig:transverse-phase-space}. For this 4 nC beam, the peak current is 180.5 A.


\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=\columnwidth]{{{xpx+ypy@ast.2745.019}}}
   \end{minipage}%
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=\columnwidth]{{{zpz@ast.2745.019}}}
   \end{minipage}
   \caption{(a) Transverse and (b) longitudinal phase spaces at the undulator entrance.}
   \label{fig:transverse-phase-space}
\end{figure}
%\caption{Transverse (left) and longitudinal (right) phase spaces at the undulator entrance.}
\subsection{THz radiation generation}

The optimized beam was then sent to Genesis 1.3~\cite{genesis} to calculate the THz SASE radiations. For statistics, numbers from 1 to 100 were attempted as the initial seed for shot noise. The THz pulse energy along the undulator is shown in Fig.~\ref{fig:THz-pulse}(a), where the gray lines are the 100 simulations and the black line gives the average. The average energy at the exit is about 0.5 mJ. The spectrum of the THz pulse is shown in Fig.~\ref{fig:THz-pulse}(b) with the central wavelength of \SI{102}{µm}. Other parameters are summarized in Table~\ref{tab:THz-SASE}.

\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=0.95\columnwidth, trim=0 15pt 0 15pt]{{{energy-vs-z-ipseed(a)}}}
   \end{minipage}%
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=0.95\columnwidth, trim=0 15pt 0 15pt]{{{spectrum-vs-lamds-ipseed(b)}}}
   \end{minipage}%
   \caption{(a) THz pulse energy along the undulator and (b) its spectrum at the exit.}
   \label{fig:THz-pulse}
\end{figure}



\section{Experiments at reduced bunch charge}

\subsection{Electron beam characterization}

The proof-of-principle experiments post several challenges. The first one is the capability of extracting 4 nC's charge from our Cs2Te photocathode. As shown in Fig.~\ref{fig:emission-curve}(a), it was verified experimentally but at the same time very strong saturation was observed. The saturation was due to the short Gaussian laser pulse currently available, which introduced strong mirror charge force during emission.

\begin{table}[h!b]
	\setlength\tabcolsep{3.5pt}
	\centering
	\caption{Summary of THz SASE radiations}
	\label{tab:THz-SASE}
	\begin{tabular}{llc}
		\toprule
		\textbf{Parameter} & \textbf{Value}               & \textbf{Unit} \\
		\midrule
		Pulse energy  & 493.1$\pm$108.8      & \SI{}{µJ}  \\
		Pulse power  & 52.7$\pm$11.8          &    MW     \\
		Arrival time jitter         &   1.45      &   ps   \\
		Center wavelength &    101.8$\pm$0.7             &  \SI{}{µm}    \\
		Spectrum width    &  2.0$\pm$0.4                & \SI{}{µm}     \\
		\bottomrule   %\SI{0.25}{in}
	\end{tabular}
\end{table}

\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=\columnwidth]{{{AttScan_1120_BSA3500um(a)}}}
   \end{minipage}%
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=\columnwidth]{{{I-t_2213(b)}}}
   \end{minipage}
   \caption{(a) Emission curve and (b) longitudinal bunch profile.}
   \label{fig:emission-curve}
\end{figure}

The second challenge is the transport of the high charge beam while not deteriorating its quality. Considering the strong saturation at 4 nC, we decided to characterize the beam at a reduced bunch charge, that is, 2.5 nC. The peak current, measured as 150 A by a transverse deflecting cavity, is still high at this bunch charge and more importantly is enough for the proof-of-principle experiments with the available laser setup. The longitudinal bunch profile is shown in Fig.~\ref{fig:emission-curve}(b). The transverse phase spaces were measured by the slit-scan method, as shown in Fig.~\ref{fig:beam-characterization}. Simulations have been performed with the experimental setup and very good agreement has been found, as compared in Table~\ref{tab:measurement-simulation}.

\begin{table}[h!b]
	\setlength\tabcolsep{3.5pt}
	\centering
	\caption{Measured and simulated beam parameters}
	\label{tab:measurement-simulation}
	\begin{tabular}{llcc}
		\toprule
		\textbf{Parameter} & \textbf{Meas.}               & \textbf{Simul.}  & \textbf{Unit} \\
		\midrule
		Laser FWHM  & 6.2                 & 6      & ps  \\
		Spot size  & 4.0          &    4.0             & mm     \\
		Bunch charge         &   2.5      &   2.5 & nC   \\
		Momentum &    17.0  & 17.0             &  MeV/c    \\
		Peak current    & 153$\pm$0.5        & 156.0                & A     \\
		Best emittance    & 3.9$\pm$0.07           &  4.1               &      mm mrad \\
		\bottomrule   %\SI{0.25}{in}
	\end{tabular}
\end{table}

\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=\columnwidth]{{{x-xp2(a)}}}
   \end{minipage}%
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=\columnwidth]{{{y-yp2(b)}}}
   \end{minipage}%
   \caption{Transverse phase space and longitudinal bunch profile.}
   \label{fig:beam-characterization}
\end{figure}

\subsection{Electron beam matching}
As mentioned in previous section, the small vacuum chamber in the LCLS-I undulator puts another challenge on the experiment. To safely travel through the chamber, a flat electron beam which focuses both horizontally and vertically is desired (see Fig.~\ref{fig:transverse-phase-space}(a)). Since the extension beamline is still under construction, we utilized our existing beamline to test the matching procedure, that is to produce the same beam at a given location as desired at the undulator entrance. With two triplets, such a beam has been produced at one of our screen stations, as shown in Fig.~\ref{fig:transport-and-matching}(a). The experimental result has also been compared to simulations with good agreement. The flat beam is shown in Fig.~\ref{fig:transport-and-matching}(b).

\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.64\columnwidth}
   \includegraphics*[width=\columnwidth]{{{after-matching1(a)}}}
   \end{minipage}%
   \begin{minipage}[b]{0.36\columnwidth}
   \includegraphics*[width=0.9\columnwidth]{{{pstscr5_xy2d(b)}}}
   \end{minipage}
   \caption{(a) Transport and matching of the electron beam and (b) transverse distribution at the matching station.}
   \label{fig:transport-and-matching}
\end{figure}

\section{CONCLUSION}
A THz SASE FEL is under design study at PITZ as the proof-of-principle experiments for pump-probe experiments at EuXFEL. In this paper, we presented the start-to-end simulations based on the proposed beamline and a THz pulse energy of 0.5 mJ was obtained from the optimized beam by Genesis 1.3. Experimentally, we have verified the capability of producing 4 nC's bunch charge from the photocathode RF gun. And at a reduced bunch charge of 2.5 nC intended to suppress strong saturation due to mirror charge during emission, we have successfully transported and matched the beam to one of our screen stations, with reasonable beam quality measured. After finishing the construction in the future, the beam will be sent to an LCLS-I undulator and hopefully generate up to mJ level THz radiations.

%\section{ACKNOWLEDGEMENTS}
%Any acknowledgement should be in a separate section directly preceding
%the \textbf{REFERENCES} or \textbf{APPENDIX} section.

%\section{References}
%
% References examples given here are not formatted using \cite as there
%			 are only reference numbers [1] and [2] in the template
%


%\flushcolsend
%what is this?

%
% only for "biblatex"
%

\ifboolexpr{bool{jacowbiblatex}}%
	{\printbibliography}%
	{%
	% "biblatex" is not used, go the "manual" way
	
	%\begin{thebibliography}{99}   % Use for  10-99  references
	\begin{thebibliography}{9} % Use for 1-9 references
	
    \bibitem{terahertz-xfel}
        P. Zalden \textit{et al.},
        "Terahertz Science at European XFEL", XFEL.EU TN-2018-001-01.0, 2018.

    \bibitem{tanikawa2018superradiant}
        T. Tanikawa \textit{et al.},
        "Superradiant Undulator Radiation for Selective THz Control Experiments at XFELs",
        arXiv preprint arXiv:1803.05323, 2018.

    \bibitem{schneidmiller2013tunable}
        E. Schneidmiller \textit{et al}, 
        "Tunable IR/THz source for pump probe experiment at European XFEL", in Proc. FEL’12, Nara, Japan, Aug. 2012, pp. 503-506.

    \bibitem{boonpornprasert2018calculations}
        P. Boonpornprasert, M. Krasilnikov, and F. Stephan, 
        "Calculations for a THz SASE FEL based on the measured electron beam parameters at PITZ", in Proc. FEL’17, Santa Fe, NM, USA, Aug. 2017, pp. 419-421.

    \bibitem{krasilnikov2018start}
        M. Krasilnikov, P. Boonpornprasert, F. Stephan, H.-D. Nuhn, 
        "START-TO-END SIMULATIONS OF THz SASE FEL PROOF-OF-PRINCIPLE EXPERIMENT AT PITZ", in Proc. ICAP2018, Key West, FL, USA, Oct. 2018, pp. 314-320.

    \bibitem{krasilnikov2012experimentally}
        M. Krasilnikov \textit{et al.}, 
        "Experimentally minimized beam emittance from an L-band photoinjector", Phys. Rev. ST Accel. Beams 15, Oct. 2012, 100701.
	
    \bibitem{astra}
		K. Flottmann, ASTRA particle tracking code,
		\url{http://www.desy.de/~mpyflo/}.
	
	\bibitem{genesis}
		S. Reiche, GENESIS 1.3 code,
		\url{http://genesis.web.psi.ch/}.

	\end{thebibliography}
} % end \ifboolexpr



%
% for use as JACoW template the inclusion of the ANNEX parts have been commented out
% to generate the complete documentation please remove the "%" of the next two commands
%
%%%\newpage

%%%\include{annexes-A4}

\end{document}

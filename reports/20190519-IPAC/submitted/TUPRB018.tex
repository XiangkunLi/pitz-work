% !TeX spellcheck = en_GB
% !TeX program = lualatex
%
% v 2.3  Feb 2019   Volker RW Schaa
%		# changes in the collaboration therefore updated file "jacow-collaboration.tex"
%		# all References with DOIs have their period/full stop before the DOI (after pp. or year)
%		# in the author/affiliation block all ZIP codes in square brackets removed as it was not %         understood as optional parameter and ZIP codes had bin put in brackets
%       # References to the current IPAC are changed to "IPAC'19, Melbourne, Australia"
%       # font for ‘url’ style changed to ‘newtxtt’ as it is easier to distinguish "O" and "0"
%
\documentclass[a4paper,
               %boxit,        % check whether paper is inside correct margins
               %titlepage,    % separate title page
               %refpage       % separate references
               %biblatex,     % biblatex is used
               keeplastbox,   % flushend option: not to un-indent last line in References
               %nospread,     % flushend option: do not fill with whitespace to balance columns
               %hyphens,      % allow \url to hyphenate at "-" (hyphens)
               %xetex,        % use XeLaTeX to process the file
               %luatex,       % use LuaLaTeX to process the file
               ]{jacow}
%
% ONLY FOR \footnote in table/tabular
%
\usepackage{pdfpages,multirow,ragged2e} %
%
% CHANGE SEQUENCE OF GRAPHICS EXTENSION TO BE EMBEDDED
% ----------------------------------------------------
% test for XeTeX where the sequence is by default eps-> pdf, jpg, png, pdf, ...
%    and the JACoW template provides JACpic2v3.eps and JACpic2v3.jpg which
%    might generates errors, therefore PNG and JPG first
%
\makeatletter%
	\ifboolexpr{bool{xetex}}
	 {\renewcommand{\Gin@extensions}{.pdf,%
	                    .png,.jpg,.bmp,.pict,.tif,.psd,.mac,.sga,.tga,.gif,%
	                    .eps,.ps,%
	                    }}{}
\makeatother

% CHECK FOR XeTeX/LuaTeX BEFORE DEFINING AN INPUT ENCODING
% --------------------------------------------------------
%   utf8  is default for XeTeX/LuaTeX
%   utf8  in LaTeX only realises a small portion of codes
%
\ifboolexpr{bool{xetex} or bool{luatex}} % test for XeTeX/LuaTeX
 {}                                      % input encoding is utf8 by default
 {\usepackage[utf8]{inputenc}}           % switch to utf8

\usepackage[USenglish]{babel}

%
% if BibLaTeX is used
%
\ifboolexpr{bool{jacowbiblatex}}%
 {%
  \addbibresource{jacow-test.bib}
  \addbibresource{biblatex-examples.bib}
 }{}
\listfiles

%%
%%   Lengths for the spaces in the title
%%   \setlength\titleblockstartskip{..}  %before title, default 3pt
%%   \setlength\titleblockmiddleskip{..} %between title + author, default 1em
%%   \setlength\titleblockendskip{..}    %afterauthor, default 1em

\begin{document}

\title{Design studies of a proof-of-principle experiment on THz SASE FEL at PITZ\thanks{Work supported by the European XFEL Gmbh}}

\author{X.-K. Li\thanks{xiangkun.li@desy.de}, M. Krasilnikov, P. Boonpornprasert, H. Shaker, Y. Chen, J. Good, M. Gross,\\ H. Huck,
I. Isaev, C. Koschitzki, S. Lal, O. Lishilin, G. Loisch, D. Melkumyan, R. Niemczyk,\\ A. Oppelt,
H. Qian, G. Shu, F. Stephan, G. Vashchenko, DESY, Zeuthen 15738, Germany}
	
\maketitle

%
\begin{abstract}
   A free-electron laser based THz source is undergoing design studies at the Photo Injector Test facility at DESY in Zeuthen (PITZ). It is considered as a prototype THz source for pump-probe experiments at the European XFEL, benefiting from the fact that the electron beams from the PITZ facility have an identical pulse train structure as the XFEL pulses. In the proposed proof-of-principle experiment, an electron beam (up to 4 nC bunch charge and 200 A peak current) will be accelerated to 16-22 MeV/c to drive an LCLS-I type undulator, generating SASE radiations in the THz range with a center wavelength between 60 and \SI{100}{µm} with an expected energy of up to $\sim$1 mJ/pulse. In this paper, we report our simulations on the optimization of the photo-injector and the design of the transport and matching beamline. Experimental investigations on the generation, characterization and matching of the high charge beam in the existing 22-m-long beamline will also be presented.
\end{abstract}


\section{INTRODUCTION}

As more user beamlines are put into use at the European X-ray free-electron laser facility (EuXFEL), pump-probe experiments have played a leading role in many research frontiers in biology, chemistry and condensed matters, etc. Among those researches are the THz-pump X-ray-probe experiments, in which a THz pulse is used to excite a sample and the following X-ray pulse is used to detect the reaction of the sample to the THz pulse~\cite{{terahertz-xfel}}. To provide the THz pump, proposals have been made at EuXFEL, for instance, by injecting the used electron beam into a THz undulator~\cite{tanikawa2018superradiant} made from super-conducting coils. Another promising idea is to put a separate PITZ-like photo-injector near the user hall to drive the THz source~\cite{schneidmiller2013tunable}. The advantages are: 1) it can produce the identical electron bunch train (thus THz pulse train) as the X-ray pulses at EuXFEL; 2) the electron beam has moderate energy so no large beam dump is needed and 3) it can be installed near the user hall and thus relieve the THz transportation issue. Previous studies~\cite{schneidmiller2013tunable, boonpornprasert2014start, boonpornprasert2017calculations, krasilnikov2018start} showed that milli-joule level THz SASE radiations could be generated from 4 nC electron bunches with 200 A peak current in Apple-II or similar undulators. And currently, proof-of-principle experiments funded by the EuXFEL are taking place at PITZ by using the existing PITZ beamline extended with an LCLS-I undulator. In this paper, we will report the progress of this project, including a start-to-end simulation study based on the proposed beamline and experimental investigations on the generation, characterization, transport and matching of the high charge, high peak current beam in the existing beamline.

\section{SIMULATIONS}

The photo-injector at PITZ consists of an L-band photocathode RF gun, solenoids for emittance compensation, an L-band booster cavity and many correction and focusing magnets with a total length of 22 m~\cite{krasilnikov2012experimentally}. To generate THz SASE FEL, the beamline will be extended by one or two LCLS-I undulators installed in a neighbouring tunnel annex.
Since the beam emittance is not critical for FELs in THz range, our major attention has been paid to the peak current and the energy spread~\cite{boonpornprasert2014start}. While a higher peak current means higher gain in the undulator, a smaller energy spread means better and easier beam transport as well as higher FEL gain. Considering these facts our photo-injector is first optimized for the peak current and the energy spread; two triplets are then selected for the beam transport. In addition, the very strong focusing forces in the vertical plane in the small vacuum chamber (11-mm-wide and 5-mm-high) between the magnetic poles in the undulator posts another challenge. In order to avoid beam loss to the wall, the transverse phase spaces should be matched in a proper way. Therefore, another two triplets will be installed before the undulator and their gradients need to be optimized.

\subsection{Start-to-end simulation}

For the design study, we first optimized the photo-injector parameters and then with the optimized electron beam designed the beam transport line until the undulator entrance.
The capability of flattop laser shaping has been demonstrated before at PITZ~\cite{krasilnikov2012experimentally} and here we assume a flattop laser of 21.5 ps in full-width-half-maximum (FWHM). Such a long pulse will relieve the space charge force and is favorable for generating high peak current. The laser spot size at the photocathode can be changed by a tunable beam shaping aperture (BSA). The highest achievable gun gradient of 60 MV/m is taken into account to suppress the mirror charge during emission. With this gun gradient, the highest beam momentum out of the booster is about 22 MeV/c, which corresponds to a radiation wavelength of \SI{60}{µm} in the LCLS-I undulator. In this paper, we consider however the nominal wavelength of \SI{100}{µm} (3 THz in frequency) and the correponding beam momentum is 17.05 MeV/c.

With the particle tracking code Astra~\cite{astra}, the laser spot size, the accelerating phases in the gun and in the booster as well as the solenoid current were optimized by the differential-evolution (DE) algorithm~\cite{DE} in such a way that the beam energy spread was minimized at the undulator center while the beam emittance was relatively small (e.g., $\sim$ 4 mm mrad). It was found that for the 4 nC bunch charge the peak current ($I_{\rm peak}$) was dependent on the laser spot size ($\phi$), as shown in Fig.~\ref{fig:optimization}(a). The increase in laser spot size reduces the space charge forces, especially near the cathode where the electrons have low kinetic energies. The density of the samples shows well the preference of a large laser spot size during the search.

\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=0.95\columnwidth, trim=0 15pt 0 15pt]{Fig1(a)}
   \end{minipage}%
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=0.95\columnwidth, trim=0 15pt 0 15pt]{Fig1(b)}
   \end{minipage}
   \caption{Dependence of (a) peak current on laser spot size (BSA) and (b) correlated energy spread on booster phase.}
   \label{fig:optimization}
\end{figure}

The correlated energy spread ($\sigma_{E}^{\rm corr} = \langle E_k z\rangle/\sigma_z$) was found dependent on the booster phase ($\phi_{\rm booster}$), as shown in Fig.~\ref{fig:optimization}(b). By accelerating the beam off-crest, the longitudinal phase space is chirped so that the tail has higher kinetic energy than the head has at the booster exit ($\sigma_{E}^{\rm corr}<0$). Then in the drift, the head is accelerated and the tail decelerated by space charge forces, therefore reducing the beam energy spread gradually. The gathering of samples implied that the best phase is around -20 degrees for minimizing the energy spread at the assumed undulator center of 27.5 m.

\begin{figure}[!htb]
   \centering
   \includegraphics*[width=0.9\columnwidth, trim=0 5pt 0 15pt]{Fig2}
   \caption{Start-to-end simulation: (a) rms beam sizes, (b) normalized emittances and (c) rms energy spread and mean momentum along the beamline.}
   \label{fig:beam-transport}
\end{figure}

After optimizing the photo-injector, the beam transport line was designed. As mentioned above, the small vacuum chamber defines the transverse phase spaces at the undulator entrance~\cite{krasilnikov2018start}, which are treated as the goal functions in the design of the transport line. As shown in Fig.~\ref{fig:beam-transport}(a), two triplets are used to smoothly focus the beam and the other two are used to match the beam into the undulator. The evolutions of normalized emittances ($\varepsilon_{n}$), the beam momentum ($P$) and relative energy spread ($\sigma_E/E$) can be found in Fig.~\ref{fig:beam-transport}(b) and (c). The emittances stay small through the whole beamline and the minimum energy spread appears around the undulator. The transverse and longitudinal phase spaces at the undulator entrance are shown in Fig.~\ref{fig:transverse-phase-space}. The latter yeilds a peak current of 180.5 A.


\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=\columnwidth, trim=0 5pt 0 5pt]{{{Fig3(a)}}}
   \end{minipage}%
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=\columnwidth, trim=0 5pt 0 5pt]{{{Fig3(b)}}}
   \end{minipage}
   \caption{(a) Transverse and (b) longitudinal phase spaces at the undulator entrance.}
   \label{fig:transverse-phase-space}
\end{figure}
%\caption{Transverse (left) and longitudinal (right) phase spaces at the undulator entrance.}
\subsection{THz radiation generation}

The optimized beam was then used as an input to Genesis 1.3~\cite{genesis} for the calculation of THz SASE radiations. For statistics, 100 runs were attempted with various initial seeds for shot noise. The pulse energies ($E$) along the undulator are shown in Fig.~\ref{fig:THz-pulse}(a), where the gray lines are the 100 simulations and the black one is the average. The average energy at the exit is about 0.5 mJ. The spectra ($\lambda_{s}$) of the pulses are shown in Fig.~\ref{fig:THz-pulse}(b) with the central wavelength of \SI{102}{µm}. Other parameters are summarized in Table~\ref{tab:THz-SASE}.

\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=0.95\columnwidth, trim=0 15pt 0 15pt]{{{Fig4(a)}}}
   \end{minipage}%
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=0.95\columnwidth, trim=0 15pt 0 15pt]{{{Fig4(b)}}}
   \end{minipage}%
   \caption{(a) THz pulse energy along the undulator and (b) its spectrum at the exit.}
   \label{fig:THz-pulse}
\end{figure}

\section{Experiments at reduced bunch charge}

\subsection{Electron beam characterization}

The proof-of-principle experiments post several challenges. The first one is the capability of extracting 4 nC bunch charge from the Cs2Te photocathode. In Fig.~\ref{fig:emission-curve}(a), it shows the dependence of the accelerated bunch charge on the laser transmission rate (or laser energy) when a large BSA of 3.5 mm was applied to the drive laser with a FWHM of about 6 ps.  While more than 4.5 nC bunch charge was measured, very strong saturation (deviation of the extracted bunch charge from linear fit) was also observed at the same time, resulting from the strong mirror charge force during emission at the presence of the short Gaussian laser pulse.

\begin{table}[h!b]
	\setlength\tabcolsep{3.5pt}
	\centering
	\caption{Summary of THz SASE radiations}
	\label{tab:THz-SASE}
	\begin{tabular}{lcc}
		\toprule
		\textbf{Parameter} & \textbf{Value}               & \textbf{Unit} \\
		\midrule
		Pulse energy  & 493.1$\pm$108.8      & \SI{}{µJ}  \\
		Pulse power  & 52.7$\pm$11.8          &    MW     \\
		Arrival time jitter         &   1.45      &   ps   \\
		Center wavelength &    101.8$\pm$0.7             &  \SI{}{µm}    \\
		Spectrum width    &  2.0$\pm$0.4                & \SI{}{µm}     \\
		\bottomrule   %\SI{0.25}{in}
	\end{tabular}
\end{table}

\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=\columnwidth, trim=0 15pt 0 15pt]{{{Fig5(a)}}}
   \end{minipage}%
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=\columnwidth, trim=0 15pt 0 15pt]{{{Fig5(b)}}}
   \end{minipage}
   \caption{(a) Emission curve and (b) longitudinal bunch profile.}
   \label{fig:emission-curve}
\end{figure}

The second challenge is the transport of the high charge beam while not deteriorating its quality. In order to avoid strong saturation as observed at 4 nC, we decided to characterize the beam at a reduced bunch charge of 2.5 nC. The longitudinal bunch profile at this bunch charge, measured by a transverse deflecting cavity system (TDS), is shown in Fig.~\ref{fig:emission-curve}(b). The peak current, measured as 153 A, is still high and more importantly is enough for the proof-of-principle experiments with the current laser setup. The transverse phase spaces measured by the slit-scan method are as shown in Fig.~\ref{fig:beam-characterization}. Simulations have been performed with the experimental setup and very good agreement has been found, as compared in Table~\ref{tab:measurement-simulation}.

\begin{table}[h!b]
	\setlength\tabcolsep{3.5pt}
	\centering
	\caption{Measured and simulated beam parameters}
	\label{tab:measurement-simulation}
	\begin{tabular}{lccc}
		\toprule
		\textbf{Parameter} & \textbf{Meas.}               & \textbf{Simul.}  & \textbf{Unit} \\
		\midrule
		Laser FWHM  & $\sim$6.2                 & 6      & ps  \\
		Spot size  & 4.0          &    4.0             & mm     \\
		Bunch charge         &   2.53$\pm$0.05      &   2.5 & nC   \\
		Momentum &    17.0  & 17.0             &  MeV/c    \\
		Peak current    & 153$\pm$0.5        & 156.0                & A     \\
		Best emittance    & 3.9$\pm$0.07           &  4.1               &      mm mrad \\
		\bottomrule   %\SI{0.25}{in}
	\end{tabular}
\end{table}

\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=\columnwidth]{{{Fig6(a)}}}
   \end{minipage}%
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=\columnwidth]{{{Fig6(b)}}}
   \end{minipage}%
   \caption{The measured transverse phase spaces in (a) horizontal and (b) vertical planes: $\varepsilon_{n,x} = 4.0$ mm mrad and $\varepsilon_{n,x} = 3.9$ mm mrad.}
   \label{fig:beam-characterization}
\end{figure}

\subsection{Electron beam matching}
As mentioned in previous section, the strong focusing in the small vacuum chamber in the LCLS-I undulator puts another challenge on the experiment. To safely transport through the chamber, a flat electron beam which focuses both horizontally and vertically is desired (see Fig.~\ref{fig:transverse-phase-space}(a)). Since the extension beamline is still under construction, we utilized our existing beamline to test the matching procedure, that is to produce the same beam at a given location as desired at the undulator entrance. With two quadrupole triplets the matching was implemented and the flat beam was observed at one of the screen stations, as shown in Fig.~\ref{fig:transport-and-matching}(a) and (b). The transport of the beam has also been compared to simulations with good agreement.

\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.64\columnwidth}
   \includegraphics*[width=\columnwidth]{{{Fig7(a)}}}
   \end{minipage}%
   \begin{minipage}[b]{0.36\columnwidth}
   \includegraphics*[width=0.9\columnwidth]{{{Fig7(b)}}}
   \end{minipage}
   \caption{(a) Transport and matching of the electron beam from simulation and measurement and (b) transverse distribution at the matching screen (YAG in (a)).}
   \label{fig:transport-and-matching}
\end{figure}

\section{CONCLUSION}
A THz SASE FEL is under design study at PITZ as the proof-of-principle experiments for pump-probe experiments at European XFEL. In this paper, the start-to-end simulations based on the proposed beamline was presented, yielding a THz pulse energy of 0.5 mJ. Experimentally, we have verified the capability of producing more than 4 nC bunch charge from the photocathode RF gun. And at a reduced bunch charge of 2.5 nC intended to suppress strong saturation due to mirror charge during emission, we have successfully transported and matched the beam to one of our screen stations, with reasonable beam quality measured. After finishing the construction in the future, the beam will be sent to an LCLS-I undulator installed in the second PITZ tunnel, aiming at up to mJ level THz radiations.

%\section{ACKNOWLEDGEMENTS}
%Any acknowledgement should be in a separate section directly preceding
%the \textbf{REFERENCES} or \textbf{APPENDIX} section.

%\section{References}
%
% References examples given here are not formatted using \cite as there
%			 are only reference numbers [1] and [2] in the template
%


%\flushcolsend
%what is this?

%
% only for "biblatex"
%

\ifboolexpr{bool{jacowbiblatex}}%
	{\printbibliography}%
	{%
	% "biblatex" is not used, go the "manual" way
	
	%\begin{thebibliography}{99}   % Use for  10-99  references
	\begin{thebibliography}{9} % Use for 1-9 references
	
    \bibitem{terahertz-xfel}
        P. Zalden \textit{et al.},
        "Terahertz Science at European XFEL", XFEL.EU TN-2018-001-01.0, 2018.

    \bibitem{tanikawa2018superradiant}
        T. Tanikawa \textit{et al.},
        "Superradiant Undulator Radiation for Selective THz Control Experiments at XFELs",
        arXiv preprint arXiv:1803.05323, 2018.

    \bibitem{schneidmiller2013tunable}
        E. Schneidmiller \textit{et al},
        "Tunable IR/THz source for pump probe experiment at European XFEL", in Proc. FEL’12, Nara, Japan, Aug. 2012, pp. 503-506.

    \bibitem{boonpornprasert2014start}
        P. Boonpornprasert \textit{et al},
        "Start-to-end simulations for IR/THz undulator radiation at PITZ", in Proc. FEL’14, Basel, Switzerland, Aug. 2014, pp. 153-158.

    \bibitem{boonpornprasert2017calculations}
        P. Boonpornprasert, M. Krasilnikov, and F. Stephan,
        "Calculations for a THz SASE FEL based on the measured electron beam parameters at PITZ", in Proc. FEL’17, Santa Fe, NM, USA, Aug. 2017, pp. 419-421.

    \bibitem{krasilnikov2018start}
        M. Krasilnikov, P. Boonpornprasert, F. Stephan, H.-D. Nuhn,
        "Start-to-end simulations of THz SASE FEL proof-of-principle experiment at PITZ", in Proc. ICAP2018, Key West, FL, USA, Oct. 2018, pp. 314-320.

    \bibitem{krasilnikov2012experimentally}
        M. Krasilnikov \textit{et al.},
        "Experimentally minimized beam emittance from an L-band photoinjector", Phys. Rev. ST Accel. Beams 15, Oct. 2012, 100701.
	
    \bibitem{astra}
		K. Flottmann, ASTRA particle tracking code,
		\url{http://www.desy.de/~mpyflo/}.
	
    \bibitem{DE}
        R. Storn and K. Price,
        "Differential evolution - a simple and efficient heuristic for global optimization over continuous spaces", Journal of global optimization 11, 1997, 341-359.

	\bibitem{genesis}
		S. Reiche, GENESIS 1.3 code,
		\url{http://genesis.web.psi.ch/}.

	\end{thebibliography}
} % end \ifboolexpr



%
% for use as JACoW template the inclusion of the ANNEX parts have been commented out
% to generate the complete documentation please remove the "%" of the next two commands
%
%%%\newpage

%%%\include{annexes-A4}

\end{document}

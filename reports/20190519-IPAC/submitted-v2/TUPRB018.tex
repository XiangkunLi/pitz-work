% !TeX spellcheck = en_GB
% !TeX program = lualatex
%
% v 2.3  Feb 2019   Volker RW Schaa
%		# changes in the collaboration therefore updated file "jacow-collaboration.tex"
%		# all References with DOIs have their period/full stop before the DOI (after pp. or year)
%		# in the author/affiliation block all ZIP codes in square brackets removed as it was not %         understood as optional parameter and ZIP codes had bin put in brackets
%       # References to the current IPAC are changed to "IPAC'19, Melbourne, Australia"
%       # font for ‘url’ style changed to ‘newtxtt’ as it is easier to distinguish "O" and "0"
%
\documentclass[a4paper,
               %boxit,        % check whether paper is inside correct margins
               %titlepage,    % separate title page
               %refpage       % separate references
               %biblatex,     % biblatex is used
               keeplastbox,   % flushend option: not to un-indent last line in References
               %nospread,     % flushend option: do not fill with whitespace to balance columns
               %hyphens,      % allow \url to hyphenate at "-" (hyphens)
               %xetex,        % use XeLaTeX to process the file
               %luatex,       % use LuaLaTeX to process the file
               ]{jacow}
%
% ONLY FOR \footnote in table/tabular
%
\usepackage{pdfpages,multirow,ragged2e} %
%
% CHANGE SEQUENCE OF GRAPHICS EXTENSION TO BE EMBEDDED
% ----------------------------------------------------
% test for XeTeX where the sequence is by default eps-> pdf, jpg, png, pdf, ...
%    and the JACoW template provides JACpic2v3.eps and JACpic2v3.jpg which
%    might generates errors, therefore PNG and JPG first
%
\makeatletter%
	\ifboolexpr{bool{xetex}}
	 {\renewcommand{\Gin@extensions}{.pdf,%
	                    .png,.jpg,.bmp,.pict,.tif,.psd,.mac,.sga,.tga,.gif,%
	                    .eps,.ps,%
	                    }}{}
\makeatother

% CHECK FOR XeTeX/LuaTeX BEFORE DEFINING AN INPUT ENCODING
% --------------------------------------------------------
%   utf8  is default for XeTeX/LuaTeX
%   utf8  in LaTeX only realises a small portion of codes
%
\ifboolexpr{bool{xetex} or bool{luatex}} % test for XeTeX/LuaTeX
 {}                                      % input encoding is utf8 by default
 {\usepackage[utf8]{inputenc}}           % switch to utf8

\usepackage[USenglish]{babel}

%
% if BibLaTeX is used
%
\ifboolexpr{bool{jacowbiblatex}}%
 {%
  \addbibresource{jacow-test.bib}
  \addbibresource{biblatex-examples.bib}
 }{}
\listfiles

%%
%%   Lengths for the spaces in the title
%%   \setlength\titleblockstartskip{..}  %before title, default 3pt
%%   \setlength\titleblockmiddleskip{..} %between title + author, default 1em
%%   \setlength\titleblockendskip{..}    %afterauthor, default 1em

\begin{document}

\title{Design studies of a proof-of-principle experiment on THz SASE FEL at PITZ\thanks{Work supported by the European XFEL Gmbh}}

\author{X.-K. Li\thanks{xiangkun.li@desy.de}, M. Krasilnikov, P. Boonpornprasert, H. Shaker, Y. Chen, J. Good, M. Gross,\\ H. Huck,
I. Isaev, C. Koschitzki, S. Lal, O. Lishilin, G. Loisch, D. Melkumyan, R. Niemczyk,\\ A. Oppelt,
H. Qian, G. Shu, F. Stephan, G. Vashchenko, DESY, Zeuthen 15738, Germany}
	
\maketitle

%
\begin{abstract}
   A free-electron laser based THz source is undergoing design studies at the Photo Injector Test facility at DESY in Zeuthen (PITZ). It is considered as a prototype for pump-probe experiments at the European XFEL, benefiting from the fact that the electron beam from the PITZ facility has an identical pulse train structure as the XFEL pulses. In the proposed proof-of-principle experiment, the electron beam (up to 4 nC bunch charge and 200 A peak current) will be accelerated to 16-22 MeV/c to generate SASE radiations in an LCLS-I undulator in the THz range between 60 and \SI{100}{µm} with an expected energy of up to $\sim$1 mJ/pulse. In this paper, we report our simulations on the optimization of the photo-injector and the design of the transport and matching beamline. Experimental investigations on the generation, characterization and matching of the high charge beam in the existing 22-m-long beamline will also be presented.
\end{abstract}


\section{INTRODUCTION}

As more user beamlines are put into use at the European X-ray free-electron laser facility (EuXFEL), pump-probe experiments will play an important role in many research frontiers in biology, chemistry and materials, etc~\cite{palmer2019pump, vester2019ultrafast}. Among those researches are the THz-pump X-ray-probe experiments, in which a THz pulse is used to excite a sample and a following X-ray pulse to detect the reaction of the sample to the THz pulse~\cite{{terahertz-xfel}}. To provide the THz pump, proposals have been made at EuXFEL, for instance, by injecting the used electron beam into a super-conducting THz undulator~\cite{tanikawa2018superradiant}. Another promising idea is to put a separate PITZ-like photo-injector near the user hall to drive the THz source~\cite{schneidmiller2013tunable}. The advantages are: 1) it can produce the identical electron bunch train (thus THz pulse train) as the X-ray pulses at EuXFEL; 2) the electron beam has moderate energy so no large beam dump is needed and 3) it can be installed near the user hall and thus is flexible in terms of the THz transportation. Previous simulation studies~\cite{schneidmiller2013tunable, boonpornprasert2014start, boonpornprasert2017calculations, krasilnikov2018start} showed that milli-joule level THz SASE radiations could be generated from 4 nC electron bunches with 200 A peak current in Apple-II or similar undulators. Currently, proof-of-principle experiments funded by the EuXFEL are taking place at PITZ by using the existing PITZ beamline extended with an LCLS-I undulator module. In this paper, we will report the progress of this project, including a start-to-end simulation study based on the proposed beamline and experimental investigations on the generation, characterization, transport and matching of the high charge, high peak current beam in the existing beamline.

\section{SIMULATIONS}

The photo-injector at PITZ consists of an L-band photocathode RF gun, solenoids for emittance compensation, an L-band booster cavity and many correction and focusing magnets with a total length of 22 m~\cite{krasilnikov2012experimentally}. It will be extended with one LCLS-I undulator (see Table~\ref{tab:THz-SASE}) located at 27.5 m from the photocathode to generate SASE radiations.
Since the beam emittance is not critical for FELs in the THz range, our major attention has been paid to the peak current and the energy spread~\cite{boonpornprasert2014start}. While a higher peak current means higher FEL gain, a smaller energy spread means easier beam transport as well as higher FEL gain. Considering these facts the photo-injector is first optimized for the peak current and the energy spread and then two triplets are selected for the beam transport. In addition, the very strong focusing forces in the vertical plane in the small vacuum chamber (11 mm width and 5 mm hight) of the LCLS-I undulator posts another challenge. To avoid beam loss to the wall, the transverse phase spaces should be matched properly. Therefore, another two triplets will be installed before the undulator and their gradients need to be optimized.

\subsection{Start-to-end simulation}

%For the design study, we first optimized the photo-injector parameters and then with the optimized electron beam designed the beam transport line until the undulator entrance.
The capability of flattop laser shaping has been demonstrated before at PITZ~\cite{krasilnikov2012experimentally} and here we assume a flattop laser of 21.5 ps in full-width-half-maximum (FWHM). Such a long pulse will relieve the space charge force and is favorable for generating high peak current. The laser spot size at the photocathode can be changed by a tunable beam shaping aperture (BSA). The highest achievable gun gradient of 60 MV/m is taken into account to suppress the mirror charge during emission. With this gun gradient, the beam momentum out of the booster is around 22 MeV/c, corresponding to a radiation wavelength of \SI{60}{µm} in the LCLS-I undulator. In this paper, we however tune the parameters for the nominal wavelength of \SI{100}{µm} (3 THz in frequency) and the correponding beam momentum is 17.05 MeV/c.

At the bunch charge of 4 nC, the laser spot size, the accelerating phases in the gun and in the booster with respect to the maximum mean momentum gain (MMMG) phase as well as the solenoid current were optimized by the differential-evolution (DE) algorithm~\cite{DE} with the particle tracking code Astra~\cite{astra} in such a way that the energy spread was minimized at the undulator center while keeping the beam emittance relatively small (e.g., $\sim$ 4 mm mrad). It was found that the peak current, $I_{\rm peak}$, was dependent on the laser spot size, $D$, as shown in Fig.~\ref{fig:optimization}(a). The increase in laser spot size reduces the longitudinal space charge forces, especially near the cathode where the electrons have low kinetic energies. The density of the samples shows well the preference of a large laser spot size during the search.

\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=0.92\columnwidth, trim=0 15pt 0 20pt]{Fig1(a)}
   \end{minipage}%
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=0.92\columnwidth, trim=0 15pt 0 20pt]{Fig1(b)}
   \end{minipage}
   \caption{Dependence of (a) peak current on laser spot size (BSA) and (b) correlated energy spread on booster phase.}
   \label{fig:optimization}
\end{figure}

The correlated energy spread, $\sigma_{E}^{\rm corr} = \langle E_k z\rangle/\sigma_z$, where $E_k$ is the kinetic energy, $z$ the position and $\sigma_z$ the bunch length, was found dependent on the booster phase, $\phi_{\rm booster}$, as shown in Fig.~\ref{fig:optimization}(b). By accelerating the bunch off-crest ($\phi_{\rm booster}$<0), the longitudinal phase space is chirped so that its tail has higher kinetic energy than its head has, that is, $\sigma_{E}^{\rm corr}<0$ at the booster exit. Then in the drift, the head is accelerated by space charge force from the tail, therefore reducing the energy spread.  At minimum energy spread, $\sigma_{E}^{\rm corr} = 0$. The gathering of samples implied that the best phase is around -20 degrees.

\begin{figure}[!htb]
   \centering
   \includegraphics*[width=0.88\columnwidth, trim=0 5pt 0 15pt]{Fig2}
   \caption{Start-to-end simulation: (a) rms beam sizes, (b) normalized emittances and (c) rms energy spread and mean momentum along the beamline.}
   \label{fig:beam-transport}
\end{figure}

After optimizing the photo-injector, the beam transport line was designed. As mentioned above, the small vacuum chamber defines the transverse phase spaces at the undulator entrance~\cite{krasilnikov2018start}, which are treated as the goal functions in the design of the transport line. As shown in Fig.~\ref{fig:beam-transport}(a), two triplets are used to smoothly focus the beam and the other two are used to match the beam into the undulator. The evolutions of normalized emittances, $\varepsilon_{n}$, the beam momentum, $P$, and relative energy spread, $\sigma_E/E$, can be found in Fig.~\ref{fig:beam-transport}(b) and (c). The emittances stay small through the whole beamline and the minimum energy spread appears around the undulator. The transverse and longitudinal phase spaces at the undulator entrance are shown in Fig.~\ref{fig:transverse-phase-space}. The main electron beam parameters can be found in Table~\ref{tab:THz-SASE}.


\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=0.92\columnwidth, trim=0 10pt 0 5pt]{{{Fig3(a)}}}
   \end{minipage}%
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=0.92\columnwidth, trim=0 10pt 0 5pt]{{{Fig3(b)}}}
   \end{minipage}
   \caption{(a) Transverse and (b) longitudinal phase spaces at the undulator entrance.}
   \label{fig:transverse-phase-space}
\end{figure}
%\caption{Transverse (left) and longitudinal (right) phase spaces at the undulator entrance.}

\begin{table}[h!b]
	\setlength\tabcolsep{3.5pt}
	\centering
	\caption{Summary of simulations}
	\label{tab:THz-SASE}
	\begin{tabular}{lcc}
		\toprule
		\textbf{Parameter} & \textbf{Value}               & \textbf{Unit} \\
		\midrule
        \textbf{Electron beam at und. ent.} & & \\
        ~~Bunch charge & 4.0 & nC \\
        ~~Mean momentum & 17.0 & MeV/c \\
        ~~Energy spread & 0.4 & \% \\
        ~~Peak current & 180.5 & A \\
        ~~Norm. emittance, $x/y$ & 4.3$/$4.9 & mm mrad \\
        \textbf{Undulator}  &  & \\
        ~~Undulator period & 30 & mm \\
        ~~\# of periods & 113 & \\
        ~~Peak magnetic field & 1.28  & T \\
        \textbf{THz radiation} &  &  \\
		~~Pulse energy  & 493.1$\pm$108.8      & \SI{}{µJ}  \\
        ~~Center wavelength &    101.8$\pm$0.7             &  \SI{}{µm}    \\
		~~Spectrum width    &  2.0$\pm$0.4                & \SI{}{µm}     \\
		~~Arrival time jitter         &   1.45      &   ps   \\
		\bottomrule   %\SI{0.25}{in}
	\end{tabular}
\end{table}

\subsection{THz radiation generation}

The above beam was then used as an input to Genesis 1.3~\cite{genesis} for the calculation of THz SASE radiations with one LCLS-I undulator. For statistics 100 runs were performed with various initial seeds for shot noise. The waveguide effects from the vacuum chamber were not taken into account. The pulse energy, $E$, along the undulator is shown in Fig.~\ref{fig:THz-pulse}(a), where the gray lines are the 100 runs and the black one the average (0.5 mJ at the undulator exit). The spectra, $\lambda_{s}$, are shown in Fig.~\ref{fig:THz-pulse}(b) with the central wavelength of \SI{102}{µm}. Other parameters are given in Table~\ref{tab:THz-SASE}.

\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=0.92\columnwidth, trim=0 15pt 0 15pt]{{{Fig4(a)}}}
   \end{minipage}%
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=0.92\columnwidth, trim=0 15pt 0 15pt]{{{Fig4(b)}}}
   \end{minipage}%
   \caption{(a) THz pulse energy along the undulator and (b) its spectrum at the exit.}
   \label{fig:THz-pulse}
\end{figure}

\section{Experiments}

\subsection{Electron beam characterization}

The proof-of-principle experiments post several challenges. The first one is the capability of extracting 4 nC bunch charge from the Cs2Te photocathode. In Fig.~\ref{fig:emission-curve}(a), it shows the dependence of the accelerated bunch charge on the laser transmission rate (or laser energy) when a large BSA of 3.5 mm was applied to the drive laser of 6 ps long in FWHM.  While more than 4.5 nC bunch charge was measured, very strong saturation (deviation of the extracted bunch charge from linear fit) was also observed at the same time, resulting from the strong mirror charge force during emission at the presence of the short Gaussian laser pulse.

\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=0.92\columnwidth, trim=0 15pt 0 15pt]{{{Fig5(a)}}}
   \end{minipage}%
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=0.92\columnwidth, trim=0 15pt 0 15pt]{{{Fig5(b)}}}
   \end{minipage}
   \caption{Measurement of (a) emission curve and (b) bunch profile.}
   \label{fig:emission-curve}
\end{figure}
\begin{table}[h!b]
	\setlength\tabcolsep{3.5pt}
	\centering
	\caption{Comparison of measured and simulated beam parameters}
	\label{tab:measurement-simulation}
	\begin{tabular}{lccc}
		\toprule
		\textbf{Parameter} & \textbf{Meas.}               & \textbf{Simul.}  & \textbf{Unit} \\
		\midrule
		Laser FWHM  & $\sim$6.2                 & 6      & ps  \\
		Spot size  & 4.0          &    4.0             & mm     \\
		Bunch charge         &   2.53$\pm$0.05      &   2.5 & nC   \\
		Momentum &    17.0  & 17.0             &  MeV/c    \\
		Peak current    & 153$\pm$0.5        & 156.0                & A     \\
		$xy$ emittance    & 3.9$\pm$0.07           &  4.1               &      mm mrad \\
		\bottomrule   %\SI{0.25}{in}
	\end{tabular}
\end{table}

The second challenge is the transport of the high charge beam while not deteriorating its quality. In order to avoid strong saturation as observed at 4 nC, we decided to characterize the beam at a reduced bunch charge of 2.5 nC. The longitudinal bunch profile at this bunch charge, measured by a transverse deflecting cavity system (TDS), is shown in Fig.~\ref{fig:emission-curve}(b). The peak current, measured as 153 A, is still high and more importantly is enough for the proof-of-principle experiments with the current laser setup. The transverse phase spaces measured by the slit-scan method are shown in Fig.~\ref{fig:beam-characterization}. Simulations have been performed with the experimental setup and very good agreement has been found (see Table~\ref{tab:measurement-simulation}).

\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=\columnwidth, trim=0 0pt 0 20pt]{{{Fig6(a)}}}
   \end{minipage}%
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=\columnwidth, trim=0 0pt 0 20pt]{{{Fig6(b)}}}
   \end{minipage}%
   \caption{Measured transverse phase spaces in (a) horizontal and (b) vertical planes: $\varepsilon_{n,x} = 4.0$, $\varepsilon_{n,x} = 3.8$ mm mrad.}
   \label{fig:beam-characterization}
\end{figure}

\subsection{Electron beam matching}
As mentioned in previous section, the strong focusing in the small vacuum chamber in the LCLS-I undulator puts another challenge on the experiment. To safely transport through the chamber, a flat electron beam which focuses both horizontally and vertically is desired (see Fig.~\ref{fig:transverse-phase-space}(a)). Since the extension beamline is still under construction, we utilized our existing beamline to test the matching procedure, that is, to produce the same beam at a given location as desired at the undulator entrance. With two quadrupole triplets the matching was implemented and the flat beam was observed at one of the screen stations, as shown in Fig.~\ref{fig:transport-and-matching}(a) and (b). The transport of the beam has also been compared to simulations with good agreement.

\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.64\columnwidth}
   \includegraphics*[width=\columnwidth, trim=0 5pt 0 5pt]{{{Fig7(a)}}}
   \end{minipage}%
   \begin{minipage}[b]{0.36\columnwidth}
   \includegraphics*[width=0.9\columnwidth, trim=0 5pt 0 5pt]{{{Fig7(b)}}}
   \end{minipage}
   \caption{(a) Transport and matching of the electron beam from simulation and measurement and (b) transverse distribution at the matching screen (YAG in (a)).}
   \label{fig:transport-and-matching}
\end{figure}

\section{CONCLUSION}
A THz SASE FEL is under design study at PITZ as proof-of-principle experiments for future pump-probe experiments at European XFEL. In this paper, the start-to-end simulations based on the proposed beamline was presented, yielding a THz pulse energy of 0.5 mJ. Experimentally, we have verified the capability of producing more than 4 nC bunch charge from the photocathode RF gun. And at a reduced bunch charge of 2.5 nC intended to suppress strong saturation due to mirror charge during emission, we have successfully transported and matched the beam to a screen station close to the end of the existing beamline, with reasonable beam quality measured. In the future, the beam will be sent to an LCLS-I undulator installed in the second PITZ tunnel, aiming at up to mJ level THz radiations.

%\section{ACKNOWLEDGEMENTS}
%Any acknowledgement should be in a separate section directly preceding
%the \textbf{REFERENCES} or \textbf{APPENDIX} section.

%\section{References}
%
% References examples given here are not formatted using \cite as there
%			 are only reference numbers [1] and [2] in the template
%


%\flushcolsend
%what is this?

%
% only for "biblatex"
%

\ifboolexpr{bool{jacowbiblatex}}%
	{\printbibliography}%
	{%
	% "biblatex" is not used, go the "manual" way
	
	%\begin{thebibliography}{99}   % Use for  10-99  references
	\begin{thebibliography}{9} % Use for 1-9 references
	
    \bibitem{palmer2019pump}
        G. Palmer \text{et al.},
        "Pump–probe laser system at the FXE and SPB/SFX instruments of the European X-ray Free-Electron Laser Facility",
        J. Synchrotron Radiat. 26 (2), 2019, PP. 328–332.

    \bibitem{vester2019ultrafast}
        P. Vester \textit{et al.},
        "Ultrafast structural dynamics of photo-reactions observed by time-resolved x-ray cross-correlation analysis",
        Struct. Dyn. 6, Mar. 2019, 024301.

    \bibitem{terahertz-xfel}
        P. Zalden \textit{et al.},
        "Terahertz Science at European XFEL", XFEL.EU TN-2018-001-01.0, 2018.

    \bibitem{tanikawa2018superradiant}
        T. Tanikawa \textit{et al.},
        "Superradiant Undulator Radiation for Selective THz Control Experiments at XFELs",
        arXiv preprint arXiv:1803.05323, 2018.

    \bibitem{schneidmiller2013tunable}
        E. Schneidmiller \textit{et al},
        "Tunable IR/THz source for pump probe experiment at European XFEL", in Proc. FEL’12, Nara, Japan, Aug. 2012, pp. 503-506.

    \bibitem{boonpornprasert2014start}
        P. Boonpornprasert \textit{et al},
        "Start-to-end simulations for IR/THz undulator radiation at PITZ", in Proc. FEL’14, Basel, Switzerland, Aug. 2014, pp. 153-158.

    \bibitem{boonpornprasert2017calculations}
        P. Boonpornprasert, M. Krasilnikov, and F. Stephan,
        "Calculations for a THz SASE FEL based on the measured electron beam parameters at PITZ", in Proc. FEL’17, Santa Fe, NM, USA, Aug. 2017, pp. 419-421.

    \bibitem{krasilnikov2018start}
        M. Krasilnikov, P. Boonpornprasert, F. Stephan, H.-D. Nuhn,
        "Start-to-end simulations of THz SASE FEL proof-of-principle experiment at PITZ", in Proc. ICAP2018, Key West, FL, USA, Oct. 2018, pp. 314-320.

    \bibitem{krasilnikov2012experimentally}
        M. Krasilnikov \textit{et al.},
        "Experimentally minimized beam emittance from an L-band photoinjector", Phys. Rev. ST Accel. Beams 15, Oct. 2012, 100701.

    \bibitem{DE}
        R. Storn and K. Price,
        "Differential evolution - a simple and efficient heuristic for global optimization over continuous spaces", Journal of global optimization 11, 1997, 341-359.

    \bibitem{astra}
		K. Flottmann, ASTRA particle tracking code,
		\url{http://www.desy.de/~mpyflo/}.
	

	\bibitem{genesis}
		S. Reiche, GENESIS 1.3 code,
		\url{http://genesis.web.psi.ch/}.

	\end{thebibliography}
} % end \ifboolexpr



%
% for use as JACoW template the inclusion of the ANNEX parts have been commented out
% to generate the complete documentation please remove the "%" of the next two commands
%
%%%\newpage

%%%\include{annexes-A4}

\end{document}

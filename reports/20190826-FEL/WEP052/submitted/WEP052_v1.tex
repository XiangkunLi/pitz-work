% !TeX spellcheck = en_GB
% !TeX program = lualatex
%
% v 2.3  Feb 2019   Volker RW Schaa
%		# changes in the collaboration therefore updated file "jacow-collaboration.tex"
%		# all References with DOIs have their period/full stop before the DOI (after pp. or year)
%		# in the author/affiliation block all ZIP codes in square brackets removed as it was not %         understood as optional parameter and ZIP codes had bin put in brackets
%       # References to the current IPAC are changed to "IPAC'19, Melbourne, Australia"
%       # font for ‘url’ style changed to ‘newtxtt’ as it is easier to distinguish "O" and "0"
%
\documentclass[a4paper,
               %boxit,        % check whether paper is inside correct margins
               %titlepage,    % separate title page
               %refpage       % separate references
               %biblatex,     % biblatex is used
               keeplastbox,   % flushend option: not to un-indent last line in References
               %nospread,     % flushend option: do not fill with whitespace to balance columns
               %hyphens,      % allow \url to hyphenate at "-" (hyphens)
               xetex,        % use XeLaTeX to process the file
               %luatex,       % use LuaLaTeX to process the file
               ]{jacow}
%
% ONLY FOR \footnote in table/tabular
%
%\usepackage{amsmath}
\usepackage{pdfpages,multirow,ragged2e}
%\RequirePackage{amsmath}

%\usepackage{comment}
%
%
% CHANGE SEQUENCE OF GRAPHICS EXTENSION TO BE EMBEDDED
% ----------------------------------------------------
% test for XeTeX where the sequence is by default eps-> pdf, jpg, png, pdf, ...
%    and the JACoW template provides JACpic2v3.eps and JACpic2v3.jpg which
%    might generates errors, therefore PNG and JPG first
%
\makeatletter%
	\ifboolexpr{bool{xetex}}
	 {\renewcommand{\Gin@extensions}{.pdf,%
	                    .png,.jpg,.bmp,.pict,.tif,.psd,.mac,.sga,.tga,.gif,%
	                    .eps,.ps,%
	                    }}{}
\makeatother

% CHECK FOR XeTeX/LuaTeX BEFORE DEFINING AN INPUT ENCODING
% --------------------------------------------------------
%   utf8  is default for XeTeX/LuaTeX
%   utf8  in LaTeX only realises a small portion of codes
%
\ifboolexpr{bool{xetex} or bool{luatex}} % test for XeTeX/LuaTeX
 {}                                      % input encoding is utf8 by default
 {\usepackage[utf8]{inputenc}}           % switch to utf8

\usepackage[USenglish]{babel}

%
% if BibLaTeX is used
%
\ifboolexpr{bool{jacowbiblatex}}%
 {%
  \addbibresource{jacow-test.bib}
  \addbibresource{biblatex-examples.bib}
 }{}
\listfiles

%%
%%   Lengths for the spaces in the title
%%   \setlength\titleblockstartskip{..}  %before title, default 3pt
%%   \setlength\titleblockmiddleskip{..} %between title + author, default 1em
%%   \setlength\titleblockendskip{..}    %afterauthor, default 1em

\begin{document}

\title{Simulation studies on the saturated emission at PITZ}

\author{X.-K. Li\thanks{xiangkun.li@desy.de}, P. Boonpornprasert, Y. Chen, J. Good, M. Gross, I. Isaev,
 C. Koschitzki,\\
 M. Krasilnikov, S. Lal, O. Lishilin, G. Loisch, D. Melkumyan, R. Niemczyk, A. Oppelt,\\
 H. Qian, H. Shaker, G. Shu, F. Stephan, G. Vashchenko, DESY, Zeuthen 15738, Germany}
	
\maketitle

%
\begin{abstract}
   In this paper we report our consideration and simulation on the space charge dominated emission in the L-band photocathode RF gun at the Photo Injector Test facility at DESY in Zeuthen (PITZ). It has been found that the emission curve, which relates the extracted and accelerated bunch charge at the gun exit to the laser energy, doesn't agree very well with Astra simulations when the emission is near saturation or fully saturated. Previous studies with a core-halo model for a better fit of the experimentally measured laser transverse profile as well as with an improved transient emission model have resulted in a better agreement between experimental data and simulation. A 3D FFT space charge solver including mirror charge and binned energy/momentum has been built, which also allows more emission mechanisms to be included in the future. In this paper, the energy spread effect during emission was preliminarily analyzed. Experimentally measured emission curves are compared with Astra simulations.
\end{abstract}


\section{INTRODUCTION}

The space charge effect during photo emission in an RF photocathode gun is one of the key factors determining the quality of the accelerated electron beam. For a given bunch charge, it can be optimized, for instance, by tuning gun gradient, injection phase or by cathode laser pulse shaping. According to the magnitude of space charge, the photo emission can be either source dominated or space charge dominated. For the former, the emitted charge depends on the laser energy and the quantum efficiency (QE) of the cathode. For the latter, the space charge is comparable to the accelerating gradient, therefore suppressing the emission. At PITZ~\cite{krasilnikov2012experimentally}, the measurement of the emission curve, which gives the relation between the incoming laser energy and the outgoing bunch charge, is a routine task.
%In the QE dominated regime, the emission curve is linear and the bunch charge is determined by the product of the laser energy and QE. If increasing the laser energy further, the space charge becomes so strong that the electric field on the cathode surface turns positive and suppress the emission.
The modeling of the emission using a particle tracking code such as Astra~\cite{astra} does not always give satisfying agreement, especially for the space charge dominated emission or saturated emission. Along with the investigation of cathode physics such as Schottky effect~\cite{chen2018modeling}, a 3D space charge solver has been  developed for considerations of effects such as the inhomogeneity of the laser transverse profile and the quantum efficiency over the cathode and the large energy spread of the electron bunch during the emission. Further development will also take into account the physics behind the emission process. In this paper, we report about our 3D space charge solver based on fast Fourier transformation (FFT)~\cite{qiang2006three} and the application of the solver to study the energy spread effect and to simulate the emission under experimental conditions. The measured laser profile was used to modify a homogeneous distribution generated by Astra for the 3D solver. The simulation results are compared and discussed.

\section{3D space charge solver}
In order to calculate the space charge forces, the whole electron bunch of a given distribution is transformed into its rest frame, which turns the problem of space charge into solving the Poisson's equation. The method of solving it in 3D using FFT has been described in detail in Ref.~\cite{qiang2006three}. In this method, the solution of Poisson's equation (or the static potential $\phi$) is given by
%\begin{align}
%  \label{eq:poissons-equation}
%  %\begin{split}
%  \phi(x,y,z) &= \frac{1}{4\pi\epsilon_0} \nonumber \\
%  \times & \int G(x, x', y, y', z, z')\rho(x', y', z')dx'dy'dz',
%  % end{split}
%\end{align}
\begin{equation}
  \label{eq:poissons-equation}
  %\begin{split}
  \phi(\vec{r}) = \frac{1}{4\pi\epsilon_0} \times \int G(\vec{r}, \vec{r'})\rho(\vec{r'})d\vec{r'},
  % end{split}
\end{equation}
where $\vec{r}=(x, y, z)$, $\rho$ is the charge density and $G$ is Green's function defined as
\begin{equation}
  G(\vec{r}, \vec{r'}) = \frac{1}{|\vec{r}-\vec{r'}|}.
\end{equation}
In Eq.~\eqref{eq:poissons-equation} the static potential is written as the convolution of Green's function and the charge density and therefore can be solved efficiently using FFT~\cite{hockney1988}. After knowing the static potential, the electric fields in the rest frame can be derived by $\vec{E} = -\nabla \phi$ and the fields in the laboratory frame can be obtained by Lorentz transformation.

The mirror charge effect during emission is not negligible and is solved in a similar way by using the shifted Green's function method~\cite{qiang2006three}. In the above, it has been assumed that the bunch has a very small energy spread which makes the bunch static in the Lorentz transformed rest frame. In the case of large energy spread, the bunch should be treated specially, for example, by binning the bunch according to the kinetic energies or momenta of individual electrons or by slicing them longitudinally, if a strong correlation between the kinetic energy and position exists. The fields solved from each bin or slice are then summed together.

\section{Effects of energy spread on space charge calculation}
As mentioned above, the transformation of the bunch into its rest frame for calculating the static potential is valid when the beam has a small energy spread.
In fact, the energy spread during emission is large. For instance, the longitudinal phase space shown in Fig.~\ref{fig:binned-by-energy} (left) gives an RMS energy spread ($\Delta E/E$) as high as 140\%, when half of the bunch was emitted.

\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=1\columnwidth]{{{binned-by-bg(a)}}}
   \end{minipage}%
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=1\columnwidth]{{{binned-charge-density-z(b)}}}
   \end{minipage}
   \caption{Longitudinal phase space grouped by momentum (a) and charge density profile of each group (b).}
   \label{fig:binned-by-energy}
\end{figure}

In the laboratory frame, the effective transverse space charge field in the horizontal plane is given by $E_x^{\rm eff} = E_x-vB_y$, where $E_x$ and $B_y$ are the horizontal electric field and vertical magnetic field, respectively, $v$ is the speed of the electron. The components $E_x$, $E_z$ and $B_y$ are obtained from the static electric field $E_x^{\prime}$ in the rest frame by Lorentz transformation,
\begin{equation}
\label{eq:lorentz-transformation}
 E_x = \gamma E^{\prime}_x, ~ E_z = \gamma E^{\prime}_z, ~ B_y = \beta\gamma E^{\prime}_x,
\end{equation}
where $\gamma$ is the Lorentz factor. Since $\gamma \sim 1$ during the emission, a large energy spread will affect $B_y$. In Fig.~\ref{fig:focusing-field} (a) the $B_y$ fields along the bunch as given in Fig.~\ref{fig:binned-by-energy} are plotted. For comparison the space charge forces were solved by either taking the bunch as a whole or by grouping the beam into four slices according to the electrons' momenta (as denoted by colors in Fig.~\ref{fig:binned-by-energy} (a)). The longitudinal charge density profile of each bin is shown in Fig.~\ref{fig:binned-by-energy} (b). Although the slice \#4 has much larger momentum spread, it takes up a much smaller fraction of the whole bunch charge.

In Fig.~\ref{fig:focusing-field} (a), the difference in $B_y$ between unbinned and binned methods became prominent towards the bunch head (right side) or in the outer part (e.g., $x/\sigma_x=2$). However, as the $B_y$ component is much smaller than the $E_x$ component (by a factor of $\beta$ as given in Eq.~\eqref{eq:lorentz-transformation}), the effective focusing field $E_x^{\rm eff}$ turned out to be very insensitive to the momentum spread. As illustrated in Fig.~\ref{fig:focusing-field} (b), one can hardly see the difference between fields calculated from unbinned and binned methods. The contribution of $B_y$ to the focusing field can be written as $vB_y\sim\beta^2E_x$. In the case shown here, $\beta\leq 0.14$, thus $vB_y/Ex\ll 1$. As more detailed investigation is still to be performed to study the effect of energy spread on the emission and on the beam dynamics with particle tracking, the unbinned method will be used in the following section to simulate the emission in the RF gun.

\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=1\columnwidth]{{{cBy-z(a)}}}
   \end{minipage}%
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=1\columnwidth]{{{Eeff-z(b)}}}
   \end{minipage}
   \caption{Vertical magnetic field (a) and effective focusing field (b) along the bunch in the laboratory frame.}
   \label{fig:focusing-field}
\end{figure}

%\begin{figure}[!htb]
%   \centering
%   \begin{minipage}[b]{0.5\columnwidth}
%   \includegraphics*[width=1\columnwidth]{{{Ex-z}}}
%   \end{minipage}%
%   \begin{minipage}[b]{0.5\columnwidth}
%   \includegraphics*[width=1\columnwidth]{{{Eeff-z}}}
%   \end{minipage}
%   \caption{Horizontal electric field (left) and effective horizontal focusing field (right) along the bunch in the laboratory frame.}
%   \label{fig:Ex}
%\end{figure}

\section{Experimental study on emission}

At PITZ, an L-band normal conducting 1.6 cell RF gun with a Cs$_2$Te photocathode is in operation. A bucking coil and a main solenoid are around the gun cavity for emittance compensation. The gun can provide a gradient as high as 60 MV/m on the cathode\cite{krasilnikov2012experimentally} for the generation of high-brightness electron bunches for X-ray free-electron lasers. The axial field profile in the gun and the accompanying solenoid field profile are given in Fig.~\ref{fig:fields-in-gun}. In this study, we consider the emission experiments performed at a lower gun gradient of 40 MV/m, which is dedicated to study the emittance optimization for a possible future continuous wave (CW) mode operation condition of the European X-ray Free-Electron Laser (XFEL) with a superconducting RF gun (SCRF)\cite{krasilnikov2019pitz}.

\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.95\columnwidth}
   \includegraphics*[width=1\columnwidth]{{{fields_with_Gun42}}}
   \end{minipage}
   \caption{Axial electric and solenoid field profiles in the gun.}
   \label{fig:fields-in-gun}
\end{figure}

The cathode laser passes through a tunable beam shaping aperture (BSA) before hitting the photocathode. By overfilling the BSA with a transversely Gaussian laser, a uniform transverse laser, hence a uniform electron distribution can be generated. The laser can be imaged at a virtual cathode (VC) camera, located at the equivalent distance from the incoming laser as the real cathode inside the RF gun. The measured laser beam deviates from uniform distributions due to optics alignment imperfections or crystal defects, which leaves a core inhomogeneity and non-negligible outer halo surrounding the core. Figure~\ref{fig:laser-profile} (a) shows the VC image of the laser pulse with a BSA diameter of 1.3 mm. Its radial profile was fitted with a core-halo model~\cite{krasilnikov2016investigations} in Fig.~\ref{fig:laser-profile} (b), showing a good uniform core and an apparent tail.

\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=1\columnwidth]{{{BSA1.3mm-profile(a)}}}
   \end{minipage}%
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=0.9\columnwidth]{{{BSA1.3mm-Core+Halo-fit(b)}}}
   \end{minipage}
   \caption{Measured laser transverse profile (a) and core-halo fit (b).}
   \label{fig:laser-profile}
\end{figure}

To measure the emission curve, the laser energy was scanned for various BSA diameters. As shown in Fig.~\ref{fig:emission-curve} (a), the extracted bunch charge increased proportionally with the laser energy
%(or equivalently the bunch charge deduced from laser energy and quantum efficiency)
when the laser energy was small. As the laser energy increased, the space charge began to dominate the emission, known as emission saturation. When the BSA was small, the saturation came earlier with respect to the laser energy, as stronger space charge was present.
%At PITZ the simulated emission curve using Astra could not agree very well with experimental data.
%Experimentally, the extracted bunch charge out of the RF gun was scanned while increasing the incoming laser energy at various BSA sizes. The results are shown in Fig.~\ref{fig:emission-curve}, where the emission saturation could be well observed. And the smaller the BSA size, the easier it gets saturated.

\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=1\columnwidth]{{{QE@20190616A(a)}}}
   \end{minipage}%
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=1\columnwidth]{{{BSA0.5mm-simulated2(b)}}}
   \end{minipage}
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=1\columnwidth]{{{BSA0.9mm-simulated2(c)}}}
   \end{minipage}%
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=1\columnwidth]{{{BSA1.3mm-simulated2(d)}}}
   \end{minipage}
   \caption{Measurements and simulations on emission curves at various BSA diameters: (a) measurement; (b)-(d) simulations for BSA diameter of 0.5, 0.9 and 1.3 mm.}
   \label{fig:emission-curve}
\end{figure}

\section{Simulations on emission curves}
For the simulations, the measured radial profile of the laser pulse was used to modify the initial transverse distribution with 200 k macroparticles generated by Astra for particle tracking using the cylindrical space charge model in Astra. A similar distribution with $10^6$ macroparticles was generated for simulations using the 3D space charge solver (referred as 3D FFT \#1 later) for comparison with Astra. The 2D laser transverse distribution was also used to generate non-cylindrically symmetric transverse distribution (referred as 3D FFT \#2 later) to study the effect of the inhomogeneity on the emission. For the Astra simulation, it has 40 grid cells in radial direction and 50 along the bunch; for the 3D solver, the meshing grids are $N_x\times N_y \times N_z = 32\times32\times128$. %In general, the bunch length during the emission was shorter than

For three BSA diameters, the input bunch charge was scanned and the extracted bunch charge was collected at the gun exit. The results are shown in Fig.~\ref{fig:emission-curve} (b) to (d), where initial transverse distributions are cylindrically symmetric for Astra and 3D FFT \#1 and inhomogeneous for \#2. For initially cylindrically symmetric distributions, Astra simulations give a lower emission curve in the saturation regime for all the three cases, which may be resulted by the meshing resolution for space charge calculation and will be studied later. For the 3D solver, the inhomogeneity of the initial distribution was found to suppress the emission charge slightly for all the three cases. Still the measured emission curves were not well reproduced. Thus future studies will consider more cathode physics behind the emission process.

\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=1\columnwidth]{{{Xemit-z@20pC(a)}}}
   \end{minipage}%
   \begin{minipage}[b]{0.5\columnwidth}
   \includegraphics*[width=1\columnwidth]{{{Xemit-z@100pC(b)}}}
   \end{minipage}
   \caption{Comparison of simulated RMS emittance in the gun with input charge of 20 pC (a) and 100 pC (b).}
   \label{fig:benchmark}
\end{figure}

In addition, to benchmark the 3D solver, the beam emittance along the gun has been compared for bunch charges of 20 pC and 100 pC at BSA = 0.5 mm. Note that the solenoid current has introduced the rapid growth of the normalized emittance. The results are shown in Fig.~\ref{fig:benchmark}. For the linear regime (20 pC), very good agreement with Astra was found. For the saturated emission (100 pC), a big discrepancy appeared after the emission, mainly due to the difference in the extracted charge (70 pC for Astra and $\sim$80 pC for the other two). The discrepancy implies that the simulated beam parameters in the saturated emission regime could be unreliable if the emission is not well modelled.

\section{CONCLUSION}
A 3D space charge solver using the FFT method has been developed at PITZ for the simulation of the emission curves in the normal conducting RF gun. The large energy spread of the bunch during the emission was analyzed preliminarily and it was found insensitive for space charge calculation. Emission curves were simulated with Astra and tracked with the 3D solver. Discrepancy was found between Astra simulation and the space charge solver, probably due to the meshing solution in the 3D solver. The inhomogeneity of the transverse laser beam distribution could suppress the emission charge slightly. The particle tracking with the 3D solver was also crosschecked with Astra and good agreement was found for the linear regime. The deviation for the saturated regime is due to the difference in the extracted charge.

%\section{ACKNOWLEDGEMENTS}
%Any acknowledgement should be in a separate section directly preceding
%the \textbf{REFERENCES} or \textbf{APPENDIX} section.

%\section{References}
%
% References examples given here are not formatted using \cite as there
%			 are only reference numbers [1] and [2] in the template
%


%\flushcolsend
%what is this?

%
% only for "biblatex"
%

\ifboolexpr{bool{jacowbiblatex}}%
	{\printbibliography}%
	{%
	% "biblatex" is not used, go the "manual" way
	
	%\begin{thebibliography}{99}   % Use for  10-99  references
	\begin{thebibliography}{9} % Use for 1-9 references
	
    %\bibitem{terahertz-xfel}
%        P. Zalden \textit{et al.},
%        "Terahertz Science at European XFEL", XFEL.EU TN-2018-001-01.0, 2018.
%
%    \bibitem{tanikawa2018superradiant}
%        T. Tanikawa \textit{et al.},
%        "Superradiant Undulator Radiation for Selective THz Control Experiments at XFELs",
%        arXiv preprint arXiv:1803.05323, 2018.

    %\bibitem{tiedtke2014soft}
%        K. Tiedtke \textit{et al.},
%        "The soft x-ray free-electron laser FLASH at DESY: beamlines, diagnostics and end-stations",
%        New journal of physics 11.2, 2009, 023029.

    %\bibitem{schneidmiller2013tunable}
%        E. Schneidmiller \textit{et al.},
%        "Tunable IR/THz source for pump probe experiment at European XFEL", in Proc. FEL’12, Nara, Japan, Aug.
%        2012, pp. 503-506.

   % \bibitem{boonpornprasert2014start}
%        P. Boonpornprasert \textit{et al},
%        "Start-to-end simulations for IR/THz undulator radiation at PITZ", in Proc. FEL’14, Basel, Switzerland, Aug. 2014, pp. 153-158.

    %\bibitem{boonpornprasert2017calculations}
%        P. Boonpornprasert, M. Krasilnikov, and F. Stephan,
%        "Calculations for a THz SASE FEL based on the measured electron beam parameters at PITZ", in Proc. FEL’17, Santa Fe, NM, USA, Aug. 2017, pp. 419-421.

    %\bibitem{krasilnikov2018start}
%        M. Krasilnikov, P. Boonpornprasert, F. Stephan, H.-D. Nuhn,
%        "Start-to-end simulations of THz SASE FEL proof-of-principle experiment at PITZ", in Proc. ICAP2018, Key West, FL, USA, Oct. 2018, pp. 314-320.

    \bibitem{krasilnikov2012experimentally}
        M. Krasilnikov \textit{et al.},
        "Experimentally minimized beam emittance from an L-band photoinjector", Phys. Rev. ST Accel. Beams 15, Oct. 2012, 100701.
	
   % \bibitem{genesis}
%		S. Reiche, GENESIS 1.3 code,
%		\url{http://genesis.web.psi.ch/}.

    \bibitem{astra}
		K. Flottmann, ASTRA particle tracking code,
		\url{http://www.desy.de/~mpyflo/}.
	
    \bibitem{chen2018modeling}
        Y. Chen \textit{et al.},
        "Modeling and simulation of RF photoinjectors for coherent light sources",
        Nuclear Instruments and Methods in Physics Research Section A: Accelerators, Spectrometers, Detectors and Associated Equipment 889, 2018, pp. 129-137.

    \bibitem{qiang2006three}
        J. Qiang \textit{et al.},
        "Three-dimensional quasistatic model for high brightness beam dynamics simulation",
        Phys. Rev. ST Accel. Beams 9, 2006, 044204.
    \bibitem{hockney1988}
        R.W. Hockney and J.W. Eastwood,
        Computer Simulation Using Particles, Adam Hilger, New York, 1988.

    \bibitem{krasilnikov2019pitz}
        M. Krasilnikov \textit{et al.},
        "PITZ EXPERIMENTAL OPTIMIZATION FOR THE AIMED CATHODE GRADIENT OF A SUPERCONDUCTING CW RF GUN",
        in this proceedings.

    \bibitem{krasilnikov2016investigations}
        M. Krasilnikov \textit{et al.},
        "Investigations on Electron Beam Imperfections at PITZ", in Proc. 28th Linear Accelerator Conf. (LINAC'16), East Lansing, MI, USA, Sep. 2016, paper MOPLR013, pp. 165-167, ISBN: 978-3-95450-169-4.


   % \bibitem{DE}
%        R. Storn and K. Price,
%        "Differential evolution - a simple and efficient heuristic for global optimization over continuous spaces", Journal of global optimization 11, 1997, 341-359.

	

	\end{thebibliography}
} % end \ifboolexpr



%
% for use as JACoW template the inclusion of the ANNEX parts have been commented out
% to generate the complete documentation please remove the "%" of the next two commands
%
%%%\newpage

%%%\include{annexes-A4}

\end{document}

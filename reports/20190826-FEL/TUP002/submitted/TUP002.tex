% !TeX spellcheck = en_GB
% !TeX program = lualatex
%
% v 2.3  Feb 2019   Volker RW Schaa
%		# changes in the collaboration therefore updated file "jacow-collaboration.tex"
%		# all References with DOIs have their period/full stop before the DOI (after pp. or year)
%		# in the author/affiliation block all ZIP codes in square brackets removed as it was not %         understood as optional parameter and ZIP codes had bin put in brackets
%       # References to the current IPAC are changed to "IPAC'19, Melbourne, Australia"
%       # font for ‘url’ style changed to ‘newtxtt’ as it is easier to distinguish "O" and "0"
%
\documentclass[a4paper,
               %boxit,        % check whether paper is inside correct margins
               %titlepage,    % separate title page
               %refpage       % separate references
               %biblatex,     % biblatex is used
               keeplastbox,   % flushend option: not to un-indent last line in References
               %nospread,     % flushend option: do not fill with whitespace to balance columns
               %hyphens,      % allow \url to hyphenate at "-" (hyphens)
               %xetex,        % use XeLaTeX to process the file
               %luatex,       % use LuaLaTeX to process the file
               ]{jacow}
%
% ONLY FOR \footnote in table/tabular
%
\usepackage{pdfpages,multirow,ragged2e}
\usepackage{comment}
%
%
% CHANGE SEQUENCE OF GRAPHICS EXTENSION TO BE EMBEDDED
% ----------------------------------------------------
% test for XeTeX where the sequence is by default eps-> pdf, jpg, png, pdf, ...
%    and the JACoW template provides JACpic2v3.eps and JACpic2v3.jpg which
%    might generates errors, therefore PNG and JPG first
%
\makeatletter%
	\ifboolexpr{bool{xetex}}
	 {\renewcommand{\Gin@extensions}{.pdf,%
	                    .png,.jpg,.bmp,.pict,.tif,.psd,.mac,.sga,.tga,.gif,%
	                    .eps,.ps,%
	                    }}{}
\makeatother

% CHECK FOR XeTeX/LuaTeX BEFORE DEFINING AN INPUT ENCODING
% --------------------------------------------------------
%   utf8  is default for XeTeX/LuaTeX
%   utf8  in LaTeX only realises a small portion of codes
%
\ifboolexpr{bool{xetex} or bool{luatex}} % test for XeTeX/LuaTeX
 {}                                      % input encoding is utf8 by default
 {\usepackage[utf8]{inputenc}}           % switch to utf8

\usepackage[USenglish]{babel}

%
% if BibLaTeX is used
%
\ifboolexpr{bool{jacowbiblatex}}%
 {%
  \addbibresource{jacow-test.bib}
  \addbibresource{biblatex-examples.bib}
 }{}
\listfiles

%%
%%   Lengths for the spaces in the title
%%   \setlength\titleblockstartskip{..}  %before title, default 3pt
%%   \setlength\titleblockmiddleskip{..} %between title + author, default 1em
%%   \setlength\titleblockendskip{..}    %afterauthor, default 1em

\begin{document}

\title{Progress in preparing a proof-of-principle experiment for THz SASE FEL at PITZ\thanks{Work supported by the European XFEL GmbH}}

\author{X.-K. Li\thanks{xiangkun.li@desy.de}, M. Krasilnikov, P. Boonpornprasert, H. Shaker, Y. Chen, G. Georgiev, J. Good, M. Gross, \\
P. Huang, I. Isaev, C. Koschitzki, S. Lal, O. Lishilin, G. Loisch, D. Melkumyan, R. Niemczyk,\\
A. Oppelt, H. Qian, G. Shu, F. Stephan, G. Vashchenko, DESY, Zeuthen 15738, Germany}
%Namra Aftab
\maketitle

%
\begin{abstract}
   A proof-of-princle experiment for a THz SASE FEL is undergoing preparation at the Photo Injector Test facility at DESY in Zeuthen (PITZ), as a prototype THz source for pump-probe experiments at the European XFEL, which could potentially provide up to mJ/pulse THz radiation while maintaining the identical pulse train structure as the XFEL pulses. In the proof-of-principle experiment, LCLS-I undulators will be installed to generate SASE radiation in the THz range of 3-5 THz from electron bunches of 16-22 MeV/c. One key design is to obtain the peak current of nearly 200 A from the heavily charged bunches of a few nC. In this paper, we report our simulation results on the optimization of the space charge dominated beam in the photo injector and the following transport line with two cathode laser setups. Experimental results based on a short Gaussian laser will also be discussed.

   %Besides a short Gaussian laser pulse, our gun can also run with a long flattop laser, thanks to the cathode laser shaping technique. And the THz experiments will employ both types of cathode laser. In this paper, we report our start-to-end optimizations. Experimental results on the Gaussian laser will also be discussed.
\end{abstract}


\section{INTRODUCTION}

At the European X-ray free-electron laser facility (EuXFEL) pump-probe experiments play important roles in many research frontiers in biology, chemistry and condensed matters, etc. Among them are the THz-pump X-ray-probe experiments, where a THz pulse is used to excite a sample and the following X-ray pulse is used to detect its reactions~\cite{{terahertz-xfel}}. In order to provide the THz pump source, several proposals are under consideration. In one proposal the high-energy electron beam will be re-used to drive a THz undulator~\cite{tanikawa2018superradiant}, as already demonstrated at FLASH~\cite{tiedtke2014soft}. Another idea is to put an independent PITZ-like photo-injector near the user hall to drive a THz undulator~\cite{schneidmiller2013tunable}, which is currently undergoing a proof-of-principle experiment at PITZ. The expected advantages include: 1) Due to the low beam energy, the accelerator needs no big beam dump and can be installed close to user experiments so that no long THz beamline has to be built; 2) The PITZ-like accelerator with identical bunch train structure as the XFEL pulses could support all the EuXFEL end stations with THz pulses, provided that a switchyard for the THz pulses is built.
Previous studies~\cite{schneidmiller2013tunable, boonpornprasert2014start, boonpornprasert2017calculations, krasilnikov2018start} have showed that milli-joule level THz pulses could be generated from \SI{4}{nC} electron bunches with \SI{200}{A} peak current in Apple-II or similar undulators through the SASE process. For the proof-of-principle experiment, the existing PITZ beamline is to be extended with LCLS-I undulators installed in the tunnel annex. In this paper, we will report the current status of this project, including start-to-end simulation studies based on a regularly operating Gaussian cathode laser and on a longitudinally shaped flattop laser. It will be shown that similar peak current can be achieved from both laser setups and the resulting radiation energies are close to 1 mJ. For Gaussian laser pulses, experiments have been carried out to demonstrate the production of a few nC bunch charge and the possibility of matching the heavily charged bunch to the undulator.

\section{START-TO-END SIMULATIONS}

The photo-injector at PITZ consists of an L-band photocathode RF gun, solenoids for emittance compensation, an L-band booster cavity and many correction and focusing magnets, with a total beamline length of \SI{22}{m}~\cite{krasilnikov2012experimentally}. In the start-to-end simulations, we first optimize the photo-injector and then design the transport line until the proposed undulator entrance. The radiation from the optimized beams is finally evaluated with Genesis 1.3~\cite{genesis}.
At PITZ, two cathode laser setups are available: a short Gaussian laser (FWHM=6$\sim$7 ps) which is in stable operation and a long flattop laser (FWHM$\sim$22 ps) which can be realized by longitudinal pulse shaping with bi-diffracting crystals before the laser is amplified~\cite{krasilnikov2012experimentally}. Although both lasers are Gaussian transversely, it is also possible to produce approximately uniform transverse distributions by overfilling a tunable beam shaping aperture (BSA). For the THz project, both lasers are under consideration. For the Gaussian laser, relatively small bunch charge of \SI{2.5}{nC} is employed to avoid emission saturation, as shown later. In the presence of the long flattop laser, the bunch charge of \SI{4}{nC} is taken. %The prospects are studied and compared through start-to-end simulations.

%For the flattop laser, more bunch charge is possible and therefore it is aimed to pursue higher radiation energy. For both modes, start-to-end simulations have been carried out. We first optimized the peak current and the energy spread. Then we used triplets to transport the beam until the entrance of the undulator. The SASE process was finally studied by Genesis 1.3 to evaluate the properties of the THz pulses.

%In this paper, we studied the generation of the high peak current under both setups. From our experiments, the bunch charge is reduced to 2.5 nC for the Gaussian laser to avoid strong saturation during photo-emission. And the bunch charge of 4 nC is considered for the flattop laser.

%Parameter studies have suggested to employ a peak current as high as 200 A, which will involve a few nC of bunch charge. And the space charge becomes quite an issue.

\subsection{Optimization of the injector}

%For the design study, we first optimized the photo-injector parameters and then with the optimized electron beam designed the beam transport line until the undulator entrance.
%The capability of flattop laser shaping has been demonstrated before at PITZ~\cite{krasilnikov2012experimentally} and here we assume a flattop laser of 21.5 ps in full-width-half-maximum (FWHM). Such a long pulse will relieve the space charge force and is favorable for generating high peak current. The laser spot size at the photocathode can be changed by a tunable beam shaping aperture (BSA).

In the optimization we took the highest achievable gun gradient of 60 MV/m into account to reduce the space charge effects during emission. With this gradient, the highest beam momentum downstream the booster is about \SI{22}{MeV/c}, which corresponds to a radiation wavelength of \SI{60}{µm} in the LCLS-I undulator. In this paper, we consider a nominal wavelength of \SI{100}{µm} (3 THz in frequency) with a beam momentum of \SI{17}{MeV/c}. %The bunch charge is 2.5 nC for the Gaussian laser and 4.0 nC for the flattop laser.

Since the beam emittance is not critical for FELs in the THz range, the optimization is focused on the peak current and the energy spread~\cite{boonpornprasert2014start}, as a higher peak current helps to improve the FEL gain and a smaller energy spread simplifies beam transport and also leads to higher FEL gain.
With the particle tracking code Astra~\cite{astra}, the laser spot size, the accelerating phases in the gun and in the booster and the solenoid current were searched by the differential-evolution (DE) algorithm~\cite{DE} in a manner that the beam energy spread was minimized at the undulator center while the beam emittance was relatively small ($\sim$ \SI{4}{mm mrad}). The simulations stopped at $z=20$ m.
%As a first step, the peak current was not considered in the optimization procedure although it is one of the major goals to pursue higher peak current. As there were more free parameters than goal functions, it resulted in many solutions satisfying the requirements on the energy spread and the emittance.
From the optimization results, the peak current at $z=20$ m has been analyzed against the input parameters. It is found that the peak current ($I_{\rm peak}$) strongly depends on the laser spot size (BSA), as shown in Fig.~\ref{fig:optimization1}. For both laser setups, the peak current increases with the spot size. Smaller BSA means higher charge density and stronger space charge forces, which lengthens the beam when the beam energy is still small before entering the booster cavity, as shown later in Fig.~\ref{fig:beam-transport}. Meanwhile, higher peak currents can be obtained from the flattop laser since more charge is involved.

\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.5\columnwidth}
   \centering
   \includegraphics*[width=0.95\columnwidth, trim=0 20pt 0 25pt]{I1@20m-vs-BSA@2500pC(a)}
   \end{minipage}%
   \begin{minipage}[b]{0.5\columnwidth}
   \centering
   \includegraphics*[width=0.95\columnwidth, trim=0 20pt 0 25pt]{I1@20m-vs-BSA@4000pC(b)}
   \end{minipage}
   \caption{Dependence of peak current on laser spot size (BSA) for (a) the Gaussian and (b) the flattop laser.}
   \label{fig:optimization1}
\end{figure}


Another important input parameter is the booster phase ($\phi_{\rm booster}$), which is related to the correlated energy spread ($\sigma_{E}^{\rm corr} = \langle E_k z\rangle/\sigma_z$) in Fig.~\ref{fig:optimization2}. By accelerating the beam off-crest, the longitudinal phase space is chirped so that the tail has higher kinetic energy than the head has at the booster exit ($\sigma_{E}^{\rm corr}<0$). In the following drift the beam energy spread reduces gradually due to the space charge forces. The difference in initial bunch profile and charge resulted in different optimal booster phases for the two lasers, that is, around -30 degrees for the Gaussian laser and is around -20 degrees for the flattop laser.

\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.5\columnwidth}
   \centering
   \includegraphics*[width=0.95\columnwidth, trim=0 15pt 0 25pt]{Cor-energy-spread-vs-boosterPhase@2500pC(a)}
   \end{minipage}%
   \begin{minipage}[b]{0.5\columnwidth}
   \centering
   \includegraphics*[width=0.95\columnwidth, trim=0 15pt 0 25pt]{Cor-energy-spread-vs-boosterPhase@4000pC(b)}
   \end{minipage}
   \caption{Dependence of correlated energy spread on the booster phase for (a) the Gaussian and (b) the flattop laser.}
   \label{fig:optimization2}
\end{figure}

\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.99\columnwidth}
   \centering
   \includegraphics*[width=0.95\columnwidth, trim=0 15pt 0 15pt]{beam-size-with-quads@2500pC(a)}
   \end{minipage}
   \begin{minipage}[b]{0.99\columnwidth}
   \centering
   \includegraphics*[width=0.95\columnwidth, trim=0 0pt 0 15pt]{beam-size-with-quads@4000pC(b)}
   \end{minipage}
   %\includegraphics*[width=0.9\columnwidth, trim=0 5pt 0 15pt]{beam-size-with-quads@4000pC(b)}
   \caption{Beam transport for (a) the Gaussian and (b) the flattop laser.}
   \label{fig:beam-transport}
\end{figure}

\subsection{Beam transport and matching}
From the above optimization results, two cases with relatively high peak current ($\sim$ 170 A) have been selected for further transport to the undulator. The drift length between the booster and the undulator is more than 20 m. Besides, the LCLS-I undulator has a strong magnetic field (1.28 T) and very small vacuum chamber (5$\times$11 mm), which strictly defines the transverse phase spaces of the beam at the undulator entrance~\cite{krasilnikov2018start}. Considering these facts, two quadrupole magnet triplets have been chosen from the PITZ beamline to deliver the beam to the undulator and another two to match the beam into it. Figure~\ref{fig:beam-transport} shows the evolutions of the optimized RMS beam size for both laser setups, where the location of triplets can be found by changes in the beam size. The optimized longitudinal profiles at the undulator entrance are shown in Fig.~\ref{fig:optimization3} together with the slice emittance. The peak current is around 175-180 A for both setups. Note that the peak current has increased during the transport for the beam being compressed by ballistic bunching. Since the Gaussian laser is shorter, the resulting current profile is narrower. Other beam parameters at the undulator entrance are compared in Table~\ref{tab:astra-simulation}.

\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.5\columnwidth}
   \centering
   \includegraphics*[width=0.95\columnwidth, trim=0 15pt 0 25pt]{slice-parameter@2500pC(a)}
   \end{minipage}%
   \begin{minipage}[b]{0.5\columnwidth}
   \centering
   \includegraphics*[width=0.95\columnwidth, trim=0 15pt 0 25pt]{slice-parameter@4000pC(b)}
   \end{minipage}
   \caption{Current profile and slice emittance of the optimized beam at the undulator entrance for (a) the Gaussian and (b) the flattop laser.}
   \label{fig:optimization3}
\end{figure}

\begin{table}[h!b]
	\setlength\tabcolsep{3.5pt}
	\centering
	\caption{Simulated beam parameters at undulator entrance}
	\label{tab:astra-simulation}
	\begin{tabular}{lccc}
		\toprule
		\textbf{Parameter} & \textbf{Gaussian}               & \textbf{Flattop}  & \textbf{Unit} \\
		\midrule
		Laser FWHM  & 6                 & 22      & ps  \\
		Laser spot diameter & 4.0          &    4.5             & mm     \\
		Bunch charge         &   2.5      &   4.0 & nC   \\
		Momentum &    17  & 17             &  MeV/c    \\
        Energy spread &       0.2         & 0.4   & \% \\
		Peak current    & 175        & 179                & A     \\
		Beam emittance    & 4.1/3.9           &  4.3/4.9               &      mm mrad \\
		\bottomrule   %\SI{0.25}{in}
	\end{tabular}
\end{table}

%\caption{Transverse (left) and longitudinal (right) phase spaces at the undulator entrance.}
\subsection{THz radiation generation}

The optimized beams were then treated as inputs for Genesis 1.3~\cite{genesis} for the calculation of THz SASE radiation. The undulator has 113 periods with a period of 30 mm and a peak magnetic field of 1.28 T. For statistics, 100 runs were performed with various initial seeds for shot noise. The pulse energies ($E$) along the undulator are given in Fig.~\ref{fig:THz-energy}, where the gray lines are individual simulations and the black ones are the averaged results. The spectra ($\lambda_{s}$) of the pulses are shown in Fig.~\ref{fig:THz-spectrum}. The main properties of the THz pulses from both setups are compared in Table~\ref{tab:genesis-simulation}. As expected, higher THz pulse energies are obtained from the flattop laser setup because it has more charge.
\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.5\columnwidth}
   \centering
   \includegraphics*[width=0.95\columnwidth, trim=0 18pt 0 25pt]{{{energy-vs-z-ipseed@2500pC(a)}}}
   \end{minipage}%
   \begin{minipage}[b]{0.5\columnwidth}
   \centering
   \includegraphics*[width=0.95\columnwidth, trim=0 18pt 0 25pt]{{{energy-vs-z-ipseed@4000pC(b)}}}
   \end{minipage}%
   \caption{THz pulse energy along the undulator for (a) the Gaussian and (b) the flattop laser.}
   \label{fig:THz-energy}
\end{figure}

\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.5\columnwidth}
   \centering
   \includegraphics*[width=0.95\columnwidth, trim=0 15pt 0 25pt]{{{spectrum-vs-lamds-ipseed@2500pC(a)}}}
   \end{minipage}%
   \begin{minipage}[b]{0.5\columnwidth}
   \centering
   \includegraphics*[width=0.95\columnwidth, trim=0 15pt 0 25pt]{{{spectrum-vs-lamds-ipseed@4000pC(b)}}}
   \end{minipage}%
   \caption{THz pulse spectra for (a) the Gaussian and (b) the flattop laser.}
   \label{fig:THz-spectrum}
\end{figure}

\begin{table}[h!b]
	\setlength\tabcolsep{3.5pt}
	\centering
	\caption{Simulated THz SASE radiation parameters}
	\label{tab:genesis-simulation}
	\begin{tabular}{lccc}
		\toprule
		\textbf{Parameter} & \textbf{Gaussian}  & \textbf{Flattop}             & \textbf{Unit} \\
		\midrule
		Pulse energy  & 323$\pm$ 99 & 493$\pm$109      & \SI{}{µJ}  \\
		Pulse power & 97$\pm$30 & 53$\pm$12          &    MW     \\
		RMS arrival time jitter   &   3.3      &   1.5      &   ps   \\
		Center wavelength &    103$\pm$1.1   &    102$\pm$0.7             &  \SI{}{µm}    \\
		RMS spectrum width    &  3.6$\pm$1.0 &  2.0$\pm$0.4                & \SI{}{µm}     \\
		\bottomrule   %\SI{0.25}{in}
	\end{tabular}
\end{table}

\section{Experimental studies}

As the preparation of the extension beamline is in progress, experimental studies have been performed in the existing beamline, focused on the production, characterization and matching of an electron bunch of a few nC. Since the Gaussian laser was in operation for other parallel activities at PITZ, it was used. To generate high bunch charge, the laser energy has been scanned with BSA diameter varying from 3.0 to 4.5 mm. The emission curves are given in Fig.~\ref{fig:measurement1} (a). For these large BSAs, more than 4 nC has been measured. However, only 2.5 nC with a BSA size of 4.0 mm was considered in the following experiments in order to avoid strong saturated emission (deviation of the extracted bunch charge from linear fit).

\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.5\columnwidth}
   \centering
   \includegraphics*[width=0.95\columnwidth, trim=0 15pt 0 25pt]{{{QE@20190618M(a)}}}
   \end{minipage}%
   \begin{minipage}[b]{0.5\columnwidth}
   \centering
   \includegraphics*[width=0.95\columnwidth, trim=0 15pt 0 25pt]{{{phase-space@2500pC(b)(c)}}}
   \end{minipage}
   \caption{(a) Emission curves and transverse phase spaces in (b) horizontal plane and (c) vertical plane.}
   \label{fig:measurement1}
\end{figure}

The solenoid current was scanned for minimizing the transverse beam emittance measured by the slit-scan method~\cite{krasilnikov2012experimentally}. The phase spaces for the lowest emittance are shown in Fig.~\ref{fig:measurement1} (b) and (c), giving 4.0 and 3.8 mm mrad in horizontal and vertical planes, respectively. The emittance is comparable with the optimization ($\sim$ 4 mm mrad).

%\begin{figure}[!htb]
%   \centering
%   \begin{minipage}[b]{\columnwidth}
%   \includegraphics*[width=0.95\columnwidth]{{{after-matching1}}}
%   \end{minipage}%
%   \caption{Matching of the electron beam given by simulation and measurement.}
%   \label{fig:transport-and-matching}
%\end{figure}

\begin{figure}[!htb]
   \centering
   \begin{minipage}[b]{0.64\columnwidth}
   \centering
   \includegraphics*[width=\columnwidth, trim=0 8pt 0 18pt]{{{Fig7(a)}}}
   \end{minipage}%
   \begin{minipage}[b]{0.35\columnwidth}
   \centering
   \includegraphics*[width=0.9\columnwidth, trim=0 8pt 0 18pt]{{{Fig7(b)}}}
   \end{minipage}
   \caption{(a) Transport and matching of the electron beam from simulation and measurement and (b) transverse distribution at the matching screen (YAG in (a)).}
   \label{fig:transport-and-matching}
\end{figure}

%As mentioned earlier, one of the experimental challenges is the strong focusing in the small vacuum chamber in the LCLS-I undulator.
To safely transport the beam through the undulator vacuum chamber, a flat electron beam which focuses both horizontally and vertically is needed. The matching has been demonstrated in the existing beamline with two quadrupole triplets, as shown in Fig.~\ref{fig:transport-and-matching}. Next, the flattop laser will be involved for investigating the possibilities of producing, transporting and matching even more charge to the undulator.

\section{CONCLUSION}
An accelerator based THz SASE FEL is currently under construction at PITZ as a proof-of-principle experiment for pump-probe experiments at European XFEL. In this paper, the start-to-end simulations were presented, with two different laser setups being considered and compared, both yielding a THz pulse energy close to 1 mJ. Experimentally, we have verified the capability of producing a few nC bunch charge from the photocathode RF gun with the Gaussian laser. The beam quality was comparable to simulation results and the beam matching was demonstrated in the existing beamline. Next, experiments will be carried out with more bunch charge from a long flattop laser.

%\section{ACKNOWLEDGEMENTS}
%Any acknowledgement should be in a separate section directly preceding
%the \textbf{REFERENCES} or \textbf{APPENDIX} section.

%\section{References}
%
% References examples given here are not formatted using \cite as there
%			 are only reference numbers [1] and [2] in the template
%


%\flushcolsend
%what is this?

%
% only for "biblatex"
%

\ifboolexpr{bool{jacowbiblatex}}%
	{\printbibliography}%
	{%
	% "biblatex" is not used, go the "manual" way
	
	%\begin{thebibliography}{99}   % Use for  10-99  references
	\begin{thebibliography}{9} % Use for 1-9 references
	
    \bibitem{terahertz-xfel}
        P. Zalden \textit{et al.},
        "Terahertz Science at European XFEL", XFEL.EU TN-2018-001-01.0, 2018.

    \bibitem{tanikawa2018superradiant}
        T. Tanikawa \textit{et al.},
        "Superradiant Undulator Radiation for Selective THz Control Experiments at XFELs",
        arXiv preprint arXiv:1803.05323, 2018.

    \bibitem{tiedtke2014soft}
        K. Tiedtke \textit{et al.},
        "The soft x-ray free-electron laser FLASH at DESY: beamlines, diagnostics and end-stations",
        New journal of physics 11.2, 2009, 023029.

    \bibitem{schneidmiller2013tunable}
        E. Schneidmiller \textit{et al.},
        "Tunable IR/THz source for pump probe experiment at European XFEL", in Proc. FEL’12, Nara, Japan, Aug.
        2012, pp. 503-506.

    \bibitem{boonpornprasert2014start}
        P. Boonpornprasert \textit{et al},
        "Start-to-end simulations for IR/THz undulator radiation at PITZ", in Proc. FEL’14, Basel, Switzerland, Aug. 2014, pp. 153-158.

    \bibitem{boonpornprasert2017calculations}
        P. Boonpornprasert, M. Krasilnikov, and F. Stephan,
        "Calculations for a THz SASE FEL based on the measured electron beam parameters at PITZ", in Proc. FEL’17, Santa Fe, NM, USA, Aug. 2017, pp. 419-421.

    \bibitem{krasilnikov2018start}
        M. Krasilnikov, P. Boonpornprasert, F. Stephan, H.-D. Nuhn,
        "Start-to-end simulations of THz SASE FEL proof-of-principle experiment at PITZ", in Proc. ICAP2018, Key West, FL, USA, Oct. 2018, pp. 314-320.

    \bibitem{krasilnikov2012experimentally}
        M. Krasilnikov \textit{et al.},
        "Experimentally minimized beam emittance from an L-band photoinjector", Phys. Rev. ST Accel. Beams 15, Oct. 2012, 100701.
	
    \bibitem{genesis}
		S. Reiche, GENESIS 1.3 code,
		\url{http://genesis.web.psi.ch/}.

    \bibitem{astra}
		K. Flottmann, ASTRA particle tracking code,
		\url{http://www.desy.de/~mpyflo/}.
	
    \bibitem{DE}
        R. Storn and K. Price,
        "Differential evolution - a simple and efficient heuristic for global optimization over continuous spaces", Journal of global optimization 11, 1997, 341-359.

	

	\end{thebibliography}
} % end \ifboolexpr



%
% for use as JACoW template the inclusion of the ANNEX parts have been commented out
% to generate the complete documentation please remove the "%" of the next two commands
%
%%%\newpage

%%%\include{annexes-A4}

\end{document}

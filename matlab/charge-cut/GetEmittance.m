function [xrms,pxrms,emit]=GetEmittance(x,px,PhSp)

[Npx,Nx]=size(PhSp);

q=sum(sum(PhSp));

xc=0;
pxc=0;
mxx=0;
mxpx=0;
mpxpx=0;

for nx=1:Nx
    
for npx=1:Npx
    w=PhSp(npx,nx);
    xc=xc+w*x(nx);
    pxc=pxc+w*px(npx);
    mxx=mxx+w*x(nx)*x(nx);
    mxpx=mxpx+w*x(nx)*px(npx);
    mpxpx=mpxpx+w*px(npx)*px(npx);
end
    
end

xc=xc/q;
pxc=pxc/q;

mxx=mxx/q-xc^2;
mxpx=mxpx/q-xc*pxc;
mpxpx=mpxpx/q-pxc^2;


xrms=sqrt(mxx);
pxrms=sqrt(mpxpx);
emit=sqrt(mxx*mpxpx-mxpx^2);
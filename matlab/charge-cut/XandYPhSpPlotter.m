function r = XandYPhSpPlotter(method, phsp, Imain)

del=10e-3; %slit width
Npx=200;  %number of points in in PX

if ~exist('method', 'var')
    method = 'nobase';
end
%method='nobase';  %'nobase' - basement of the phase space will be removed;
%method='halo';  %'halo'--> only halo will  be removed

if ~exist('phsp', 'var')
    phsp = 'x';
end
%phsp='x';
%phsp='y';

if ~exist('Imain', 'var')
    Imain = 0;
end
%Imain = 272;

inverpx=1;

fontsize=18;

%[file,path]=uigetfile('*.090');
%%[file,path]=uigetfile('*.*','Input ASTRA monitor');
%%fn=[path,file]

path = ['\\afs\ifh.de\group\pitz\data\lixiangk\sim'...
    '\SCgun\gaussian\solenoid-scan'...
    '\Q100.0pC-D-0.70mm-E1-40.24MV_m-phi1-0.00deg-E2-15.00MV_m-phi2-0.00deg-I-'...
    num2str(Imain) '.00A\'];
%file = 'ast.0528.002'
file = '..\CH_beam200k_0.7mm.ini'
fn=[path,file]

fno=[phsp,'PhSp',strrep(file,'.','_')];

a=load(fn);

x=a(:,1)*1e3;px=a(:,4)/511;
y=a(:,2)*1e3;py=a(:,5)/511;
pz=a(:,6)/511;q=a(:,8);
pz0=pz(1);
pz=pz+pz0;
pz(1)=pz0;

%px=px./pz*1e3;
%py=py./pz*1e3;

Np=max(size(q));


xav=q(1)*x(1);pxav=q(1)*px(1);
yav=q(1)*y(1);pyav=q(1)*py(1);
pzav=q(1)*pz(1);
mxx=q(1)*x(1)*x(1);mxpx=q(1)*x(1)*px(1);mpxpx=q(1)*px(1)*px(1);
myy=q(1)*y(1)*y(1);mypy=q(1)*y(1)*py(1);mpypy=q(1)*py(1)*py(1);


for n=2:Np
    x(n)=x(n)+x(1);
    px(n)=px(n)+px(1);
    pz(n)=pz(n)+pz(1);
    
    xav=xav+q(n)*x(n);
    pxav=pxav+q(n)*px(n);
    
    yav=yav+q(n)*y(n);
    pyav=pyav+q(n)*py(n);
    
    pzav=pzav+q(n)*pz(n);
    
    mxx=mxx+q(n)*x(n)*x(n);
    mxpx=mxpx+q(n)*x(n)*px(n);
    mpxpx=mpxpx+q(n)*px(n)*px(n);
    
    myy=myy+q(n)*y(n)*y(n);
    mypy=mypy+q(n)*y(n)*py(n);
    mpypy=mpypy+q(n)*py(n)*py(n);
end

Qt=sum(q);
xav=xav/Qt;
pxav=pxav/Qt;

yav=yav/Qt;
pyav=pyav/Qt;

pzav=pzav/Qt

mxx=mxx/Qt-xav*xav;
mxpx=mxpx/Qt-xav*pxav;
mpxpx=mpxpx/Qt-pxav*pxav;

sigx=sqrt(mxx)
sigpx=sqrt(mpxpx)
emx=sqrt(mxx*mpxpx-mxpx*mxpx)

x=x-xav;
px=px-pxav;


myy=myy/Qt-yav*yav;
mypy=mypy/Qt-yav*pyav;
mpypy=mpypy/Qt-pyav*pyav;

sigy=sqrt(myy)
sigpy=sqrt(mpypy)
emy=sqrt(myy*mpypy-mxpx*mypy)

y=y-yav;
py=py-pyav;



XM=max(-min(x),max(x));
xm=-XM:del:XM;
Ns=max(size(xm));

PXM=max(-min(py),max(py));
delp=2*PXM/Npx;
pm=-PXM:delp:PXM;
NPX=max(size(pm));


rhoX=zeros(NPX,Ns);
rhoY=zeros(NPX,Ns);


for n=1:Np
    [c,ind]=min(abs(x(n)-xm));
    [c,indp]=min(abs(px(n)-pm));
    
    rhoX(indp,ind)=rhoX(indp,ind)-q(n);
    
    [c,ind]=min(abs(y(n)-xm));
    [c,indp]=min(abs(py(n)-pm));
    
    rhoY(indp,ind)=rhoY(indp,ind)-q(n);
    
end

['plotting...']

hf=figure(1);
subplot(1,2,1);

set(gcf, 'units','inch','position', [1 1 10 4]);

rhoX=rhoX./max(max(rhoX));
rhoY=rhoY./max(max(rhoY));

%rho=max(rhoX,rhoY);

v=[0.001:0.001:0.01, 0.02:0.02:1];
%[C,h,CF] =contourf(xm/sigx,pm/sigpx,rho,v);


switch phsp
    case 'x'
        [C,h,CF] =contourf(xm,pm,rhoX,v);
        set(h,'LineStyle','none');
        xlabel('x (mm)','FontSize',fontsize);
        ylabel('p_x (mrad)','FontSize',fontsize);
        fno='Fig9a';
    case 'y'
        [C,h,CF] =contourf(xm,pm,rhoY,v);
        set(h,'LineStyle','none');
        xlabel('y (mm)','FontSize',fontsize);
        ylabel('p_y (mrad)','FontSize',fontsize);
        fno='Fig9b';
end


%grid on;
%axis([-5 5 -200 200]);
%axis([-6 6 -6 6]);
%axis([-5 5 -5 5]);
%axis([-2.5 2.5 -3 3]);
%set(gca,'XTick',(-6:2:6))
%set(gca,'YTick',(-6:2:6))
ax=axis;
grid on;
set(gca,'FontSize',fontsize);

%text(0,4,'Y-Y''','FontSize',16);
%text(-5,1.8,'X-X''','FontSize',16);
%plotPhSpEllipse(mxx,mxpx,mpxpx,1);


%saveas(hf,[path, fno,'.fig'],'fig');
%saveas(hf,[path, fno,'.png'],'png');
%fn

%===================================================================

[xrms1,pxrms1,emit1]=GetEmittance(xm,pm,rhoX)

%figure(2);
subplot(1,2,2);
IntCutStep=0.001;


XemitVsQ=[];
XQ=[];


for CutInt=0.99-IntCutStep:-IntCutStep:0
    %[PhSpCut,emitCut,Qcut]=GetCutPhSp(x,px,XPsSp,CutInt);
    [PhSpCut,emitCut,Qcut]=GetCutPhSp(xm,pm,rhoX,CutInt,method);
    
    
    XemitVsQ=[XemitVsQ,emitCut];
    XQ=[XQ,Qcut];
    
    plot(XQ,XemitVsQ,'o-');
    drawnow;
end




plot(XQ,XemitVsQ,'^-');
grid on;
xlabel('Q/Q_0');
ylabel([upper(phsp) '-emittance (mm mrad)']);

%legend({['non-scaled (',num2str(emitX,'%.3f'),' mm mrad)'],['non-scaled extrap with poly^',num2str(ExtrapOrder),' (',num2str(XemitExt2(end),'%.3f'),' mm mrad)'],['scaled (',num2str(emitXscaled2,'%.3f'),' mm mrad)']},'Location','northwest');
dlmwrite([upper(phsp) 'emitVsQ_' num2str(Imain) 'A_' method '-g-0.7mm-CH.dat'], [XQ; XemitVsQ]');
saveas(gcf,[upper(phsp) 'emitVsQ_' num2str(Imain) 'A_' method '-g-0.7mm-CH.png'],'png')



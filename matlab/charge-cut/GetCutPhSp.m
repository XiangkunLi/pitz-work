function [PhSpCut,emitCut,Qcut]=GetCutPhSp(x,px,PhSp,CutInt,method)

Amax=max(max(PhSp));
Acut=Amax*CutInt;

switch method
    case 'nobase'
        PhSpCut=PhSp-Acut;
        PhSpCut=PhSpCut.*(PhSpCut>=0);
        
    case 'halo'
        PhSpCut=PhSp.*(PhSp>=Acut);
end

[xrms,pxrms,emitCut]=GetEmittance(x,px,PhSpCut);

v=[0.001:0.001:1]*Amax;

%subplot(2,1,1);
%contourf(x,px,PhSp,v,'LineStyle','none');
%subplot(2,1,2);
%contourf(x,px,PhSpCut,v,'LineStyle','none');


Qcut=sum(sum(PhSpCut))/sum(sum(PhSp));
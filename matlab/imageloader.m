%upgrade M.Krasilnikov, November 2015

clear all;
close all;
clc;


%======================PARAMETERS===================================
%FrameNum=1;

%BkgFlag='Average';
%BkgFlag='AveragePlus3RMS';
BkgFlag='Envelope';


NegPix='No';
%=====================Read imc file==================================
[sIMCFileName, sIMCPathName] = uigetfile('*.imc', 'Select laser image file');


ik=findstr(sIMCFileName,'.imc')
foname=sIMCFileName(1:ik-1);
im=findstr(foname,' ');
scrname=foname(im+1:end)

[aIMCData, aIMCHeader] = avine_load_video_images_from_file([sIMCPathName sIMCFileName]);



%====================Read bkc file ===================================

sBKCPathName=sIMCPathName;
sBKCFileName=strrep(sIMCFileName,'imc','bkc');

[aBKCData, aBKCHeader] = avine_load_video_images_from_file([sBKCPathName sBKCFileName]);

%============ Get camera dimensions ================================
aHeaderDataSingle = aIMCHeader(1);
sCamWidth = aHeaderDataSingle{1}.width;
sCamHeight = aHeaderDataSingle{1}.height;
sCamScale  = aHeaderDataSingle{1}.scale;

%========= Prepare arrays for averaging=============================
aIMCDataAverage = zeros(sCamHeight,sCamWidth);
aIMCDataRMS = zeros(sCamHeight,sCamWidth);
aBKCDataAverage = zeros(sCamHeight,sCamWidth);
aBKCDataRMS = zeros(sCamHeight,sCamWidth);
aBKCDataEnvelope = zeros(sCamHeight,sCamWidth);

MOI = zeros(sCamHeight,sCamWidth);

%================= Get number of frames=============================
sNumFramesTemp = size(aIMCData);
sNumFrames = sNumFramesTemp(1);

%==#1==================Average data===================================
for i = 1:sNumFrames
    aIMCDataSingle = aIMCData(i);
    aIMCDataDouble = double(aIMCDataSingle{1});
    aIMCDataAverage = aIMCDataAverage + aIMCDataDouble;
    aIMCDataRMS=aIMCDataRMS+ aIMCDataDouble.^2;
    
    
    aBKCDataSingle = aBKCData(i);
    
    aBKCDataDouble = double(aBKCDataSingle{1});
    aBKCDataAverage = aBKCDataAverage + aBKCDataDouble;
    aBKCDataRMS = aBKCDataRMS + aBKCDataDouble.^2;
    
    aBKCDataEnvelope =max(aBKCDataEnvelope,aBKCDataDouble);
end

aIMCDataAverage = aIMCDataAverage./sNumFrames;
aIMCDataRMS=aIMCDataRMS./sNumFrames-aIMCDataAverage.^2;
aIMCDataRMS=sqrt(aIMCDataRMS);

aBKCDataAverage = aBKCDataAverage./sNumFrames;
aBKCDataRMS=aBKCDataRMS./sNumFrames-aBKCDataAverage.^2;
aBKCDataRMS=sqrt(aBKCDataRMS);


%====#2======background calculation=====================

switch BkgFlag
 
    case 'Average'
      disp('Background method is average')
      aBKCData=aBKCDataAverage;
    
    case 'AveragePlus3RMS'
      disp('Background method is average+3*RMS')
      aBKCData=aBKCDataAverage+3*aBKCDataRMS;
  
    case 'Envelope'
     disp('Background method is envelope') 
     aBKCData=aBKCDataEnvelope;
   
    otherwise
      disp('Unknown background method');
      return
end

%========== Average image - Average background=========================
IMCmBKC = aIMCDataAverage - aBKCData;

switch NegPix
 
    case 'No'
      disp('Negative pixels->0')
      
   [N1,N2]=size(IMCmBKC);
for i=1:N1
    for j=1:N2
   if IMCmBKC(i,j)<0
       IMCmBKC(i,j)=0.;
   end
    end
  end
 
       
    otherwise
      disp('Negative pixels are alowed');
    
end

figure(1);
vmax=max(max(IMCmBKC));
IMCmBKC=IMCmBKC./vmax;
imagesc(IMCmBKC, [0.0001 0.02])
axis equal;
grid on;
cmap=colormap;
cmap(1,:)=[1,1,1];
colormap(cmap);
drawnow;




%==============manual ROI selection======================================
['select ROI...']
title('Select ROI','FontSize',20);
[pos_a,pos_b]=ginput(2);
xc=(pos_a(1)+pos_a(2))*0.5;
yc=(pos_b(1)+pos_b(2))*0.5;
ax=abs(pos_a(2)-pos_a(1))*0.5;
ay=abs(pos_b(2)-pos_b(1))*0.5;
   
   


%=======reduce image to MOI===========================================
IMCmBKC=IMCmBKC(int16(round(yc-ay:yc+ay)),int16(round(xc-ax:xc+ax)));
amax=max(ax,ay);


%=======create elliptic  Mask of Interest=================================
MOI=IMCmBKC.*0;

['applying elliptic ROI...']

for ix=1:sCamWidth
    for iy=1:sCamHeight
if (ix-ax)^2/ax^2+(iy-ay)^2/ay^2<1
       MOI(iy,ix)=1;
end
    end
end

IMCmBKC=MOI.*IMCmBKC;

%====================================================================
['plotting raw image...']

[sCamHeight,sCamWidth]=size(IMCmBKC);
[aX,aY] = meshgrid(1:sCamWidth,1:sCamHeight);

xm1d=(1:sCamWidth)*sCamScale;
ym1d=(1:sCamHeight)*sCamScale;

% Write to the disk the processed image and dimensions
dlmwrite([sIMCPathName foname '.dat'], IMCmBKC);
dlmwrite([sIMCPathName foname '.x1d'], xm1d);
dlmwrite([sIMCPathName foname '.y1d'], ym1d);
% End of writting


%================== Find the central pixel of distribution===============
projX=sum(IMCmBKC);
projY=sum(IMCmBKC');

X0=sum(xm1d.*projX)/sum(projX);
Y0=sum(ym1d.*projY)/sum(projY);

xm1d=xm1d-X0;
ym1d=ym1d-Y0;

dlmwrite([sIMCPathName foname '_.dat'], IMCmBKC);
dlmwrite([sIMCPathName foname '_.x1d'], xm1d);
dlmwrite([sIMCPathName foname '_.y1d'], ym1d);

% Gaussian fit, added by X. Li, Feb. 18 2019
ft = fittype('gaussian(x, mu, sigma, amp)');
fx = fit(xm1d', projX', ft);

fhx = figure(10);
plot(fx, xm1d, projX);
title([scrname,': Xrms=',num2str(fx.sigma,'%.2f'),'mm']);
saveas(fhx,[sIMCPathName,foname,'_x.png'],'png')

ft = fittype('gaussian(x, mu, sigma, amp)');
fy = fit(ym1d', projY', ft);

fhy = figure(11);
plot(fy, ym1d, projY);
title([scrname,': Yrms=',num2str(fy.sigma,'%.2f'),'mm']);
saveas(fhy,[sIMCPathName,foname,'_y.png'],'png')
% End of Gaussian fit

Xrms=sqrt(sum(projX.*xm1d.*xm1d)/sum(projX))
Yrms=sqrt(sum(projY.*ym1d.*ym1d)/sum(projY))

[aX,aY]=meshgrid(xm1d,ym1d);

fh=figure(1);

q=sum(sum(IMCmBKC));
%pdf=flipud(IMCmBKC)./q;
pdf=IMCmBKC./q;

v=(0.05:0.001:1)*max(max(pdf));

%imagesc(pdf);

contourf(aX,aY,pdf,v,'LineStyle','none');
%title([scrname,': Xrms=',num2str(Xrms,'%.2f'),'mm; Yrms=',num2str(Yrms,'%.2f'),'mm']);
axis equal;
grid on;
%axis([-12 12 -12 12]);
axis([-4 4 -4 4]);
xlabel('$x$ (mm)','FontSize',18, 'Interpreter','latex');
ylabel('$y$ (mm)','FontSize',18, 'Interpreter','latex');
xticks([-4 -2 0 2 4], 'FontSize',18)
yticks([-4 -2 0 2 4], 'FontSize',18)
text(3.5, 3.5, '($a$)', 'fontsize', 18, 'Interpreter','latex', 'HorizontalAlignment', 'center',...
     'VerticalAlignment', 'middle')
%colorbar;
colormap(jet);
drawnow;

%saveas(fh,[foname,'.fig'],'fig')
saveas(fh,[sIMCPathName,foname,'.png'],'png')
saveas(fh,[sIMCPathName,foname,'.eps'],'epsc')
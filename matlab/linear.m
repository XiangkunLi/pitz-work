function y = linear(x, a, b)
% linear fit

y = zeros(size(x));
for i = 1:length(x)
    y(i) = a*x(i)+b;
end
end


function y = gaussian(x, mu, sigma, amp)
% Gaussian distribution

y = zeros(size(x));
for i = 1:length(x)
    y(i) = amp*exp(-(x(i)-mu)^2/2.0/sigma^2);
end
end


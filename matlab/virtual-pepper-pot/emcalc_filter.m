%%% emcalc_filter: image processing like PITZ emwiz/emcalc/fastscan
%
%		-cut pixels below 1*STD_BG to zero (acc.to MK now 3 sigma!)
%		-3x XOR-filtering: 8(default) or 3(MOI/EMSY) neighbors: pixel product == 0? => P_ij=0
%		-3x restoring filtering: 8(default) or 7x1-L-shape(MOI/EMSY) pixel sum != 0? => P_ij=Image_ij
%

function [Imageout,int,rmsx,rmsy] = emcalc_filter(Image,STD_BG_Image,cut,nXOR,bLShape)
% Image = (background subtracted)Image to filter
% STD_BG_Image = pixelwise STD of a number of background images

if ~exist('cut','var')     %cut if Image(y,x) < cut*STD_BG_Image(y,x)
   cut = 1;
end
if ~exist('nXOR','var')    %how often XOR and restoring filters are applied
   nXOR = 1;
end
if ~exist('bLShape','var')    %L-shape restoring if true (used for MOI and EMSY)
   bLShape = 0;
end

%% STD_BG cut

temp1 = Image .* (Image > cut*STD_BG_Image);
temp = 0.*temp1;
temp(2:end-1,2:end-1) = temp1(2:end-1,2:end-1); %cut 1p border

%figure(22)
%imagesc(temp)

if bLShape
    %% XOR filter
    for i=1:nXOR
        a = temp(2:end,1:end-1) .* temp(1:end-1,2:end) .* temp(2:end,2:end);
        % same as loop containing: a=temp(y+1,x)*temp(y,x+1)*temp(y+1,x+1);
        temp(1:end-1,1:end-1) = temp(1:end-1,1:end-1) .* (a>0);
    end
    %% L-shaped restoring filter
    H=zeros(3,11);
    H(3,1:7)=1;
    H(2,7)=1;
    for i=1:nXOR
        tr = imfilter(temp, H,'replicate');
        temp = Image .* (tr>0);
    end
else
    for i=1:nXOR
        a = temp(3:end,1:end-2) .* temp(3:end,2:end-1) .* temp(3:end,3:end)...
            .*temp(2:end-1,1:end-2) .* temp(2:end-1,3:end)...
            .*temp(1:end-2,1:end-2) .* temp(1:end-2,2:end-1) .* temp(1:end-2,3:end);
        temp(2:end-1,2:end-1) = temp(2:end-1,2:end-1) .* (a>0);
    end
%figure(23)
%image(temp)
%colormap([repmat([1 1 1],1,1);jet(14200)])
   
    H=ones(3);
    for i=1:nXOR
        tr = imfilter(temp, H,'replicate');
        temp = Image .* (tr>0);
    end
end
%figure(24)
%image(temp)
%colormap([repmat([1 1 1],1,1);jet(14200)])


Imageout = temp;
%%%%%%%%%%%%%%%%%%%%%%

   int = sum(Imageout(:));
    
    for k=1:2
        summ = sum(Imageout,k);
        if ~(min(summ)==max(summ))
            mutemp   = sum( (1:length(summ)) .* summ(:)' )/sum(summ);
            rms(k) = sqrt(sum( ( (1:length(summ))-mutemp).^2 .* summ(:)'  )/ sum(summ) );
        else
            rms(k) = 0;
        end
    end
rmsx = rms(:,1);
rmsy = rms(:,2);


classdef emwiz_result < matlab.mixin.Copyable
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% emwiz_result is class representation and analyzer of single slit scan
%
% Input(s)
%   directory - directory that contains the fastscan measurement
%   scanframes - .imc file from the fastscan slit scan
%
% Output(s)
%   object - the emwiz_scan class object for the selected measurement
%
% Usage Example:
%   A = emwiz_scan('\\folder\17A\','EMSY1Y_fast.imc')
%   A.calc_chargecut()
%
% Changelog
%   vB2 - BETA version 2. G. Georgiev - 25.06.2019
%   vB3 - name change, margin/ROI, skip frames. G. Georgiev - 20.09.2019
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    properties
        emwiz_file % (structure) directory + filename(s)
        noisecut % filter function handle
        axis % scan axis (x - horizontal, y - vertical)
        emsy_margin % top, down, left, right crop margin (manual ROI)
        bcs_margin % top, down, left, right crop margin (manual ROI)
        decimator % skip every 'decimator' frame from scan during loading
        
        bcs_img % beamlet collector screen image (for MOI)
        bcs_bkg % beamlet collector screen background (for MOI)
        bcs_sdv % beamlet collector screen background deviation (for MOI)
        bcs_prop %  beamlet collector screen properties
        bcs_moi %  beamlet collector screen MOI
        bcs_pos %  beamlet collector screen pixel position (along axis)
        bcs_xroi % rectangular region of interest, based on MOI
        bcs_yroi % rectangular region of interest, based on MOI

        emsy_img % EMSY image
        emsy_bkg % EMSY background
        emsy_sdv % EMSY background deviation
        emsy_moi % EMSY MOI
        emsy_prop % EMSY screen properties
        emsy_proj % EMSY image projection
        emsy_pos % EMSY screen pixel position (along axis)

        bl_frame % beamlet image frames
        bl_bkg % beamlet background (frames?)
        bl_sdv % beamlet background deviation
        bl_proj % beamlet image projections
        bl_sop % beamlet Sum Of Pixels
        
        slit_pos % split positions for each frame
        mean_momentum; % mean beam momentum, musthaves
        
        fit_para % best fit parameter values
        fit_goodness % goodness of the fit
        %beam_fraction % 1 - charge cut
        scaling_factor % emittance scaling factor
        charge_cut % charge cut fraction
        drift % length of the drift
        
        phase_space % the reconstructed phase space
        beam_matrix % 2x2 covariance matrix
        emit_geo % geometrical emittance
        emit_nor % normalized emittance (non-scaled)
        emit_sca % scaled normalized emittance
        emit_sca2 % scaled2 (H.Q. suggestion) normalized emittance
    end
    methods
        function obj = emwiz_result(directory, scanframes)
            obj.emwiz_file = {directory, scanframes};
            if scanframes(6) == 'X'
                obj.axis = 'x';
            else
                obj.axis = 'y';
            end
            obj.noisecut = @emcalc3;% to be input arg
        end
        function set_emsy_margin(obj, t, b, l, r)
            obj.emsy_margin = [t b l r];
            % reset dependent veriables
            obj.emsy_img = [];
            obj.emsy_bkg = [];
            obj.emsy_sdv = [];
            obj.emsy_moi = [];
            obj.emsy_prop = [];
            obj.emsy_proj = [];
            obj.fit_para = [];
            obj.fit_goodness = [];
            obj.scaling_factor = [];
            obj.charge_cut = [];
            obj.beam_matrix = [];
            obj.emit_geo = [];
            obj.emit_nor = [];
            obj.emit_sca = [];
            obj.emit_sca2 = [];
        end
        function set_bcs_margin(obj, t, b, l, r)
            obj.bcs_margin = [t b l r];
            % reset dependent veriables
            obj.bcs_img = [];
            obj.bcs_bkg = [];
            obj.bcs_sdv = [];
            obj.bcs_prop = [];
            obj.bcs_moi = [];
            obj.bcs_pos = [];
            obj.bcs_xroi = [];
            obj.bcs_yroi = [];
            obj.bl_frame = [];
            obj.bl_bkg = [];
            obj.bl_sdv = [];
            obj.bl_proj = [];
            obj.bl_sop = [];
            obj.fit_para = [];
            obj.fit_goodness = [];
            obj.scaling_factor = [];
            obj.charge_cut = [];
            obj.beam_matrix = [];
            obj.emit_geo = [];
            obj.emit_nor = [];
            obj.emit_sca = [];
            obj.emit_sca2 = [];
        end
        function set_frameskip(obj, deci)
            obj.decimator = deci;
            % reset dependent veriables
            obj.bl_frame = [];
            obj.bl_bkg = [];
            obj.bl_sdv = [];
            obj.bl_proj = [];
            obj.bl_sop = [];
            obj.fit_para = [];
            obj.fit_goodness = [];
            obj.scaling_factor = [];
            obj.charge_cut = [];
            obj.beam_matrix = [];
            obj.emit_geo = [];
            obj.emit_nor = [];
            obj.emit_sca = [];
            obj.emit_sca2 = [];
        end
        function [avimg, moi, meta] = pnf(obj, fname_noext)% Process aNd Filter
            [bdat, bhea, ~] = avine_load_video_images_from_file([fname_noext,'.bkc']);
            avbkg = obj.avgImg(bdat, bhea);
            sdbkg = obj.stdImg(bdat, bhea, avbkg);
            meta.scale = bhea{1,1}.scale;
            meta.width = bhea{1,1}.width;
            meta.height = bhea{1,1}.height;
            meta.xsize = bhea{1,1}.width;
            meta.ysize = bhea{1,1}.height;

            [sdat, shea, ~] = avine_load_video_images_from_file([fname_noext,'.imc']);
            avimg = obj.avgImg(sdat, shea);

            [avimg, moi] = obj.noisecut(avimg, avbkg, sdbkg);
        end
        function project_emsy(obj)
            if isempty(obj.emsy_img)
                obj.load_emsy();
            end
            if obj.axis == 'x'
                proj = sum(obj.emsy_img,1);
                pos = (1:size(proj, 2)) * obj.emsy_prop.xaxis;
            else
                proj = sum(obj.emsy_img,2)';
                pos = (1:size(proj, 2)) * obj.emsy_prop.yaxis;
            end
            center = sum(proj .* pos)/sum(proj);
            pos = (pos - center) * obj.emsy_prop.scale;
            obj.emsy_proj = proj;
            obj.emsy_pos = pos;
        end
        function load_emsy(obj)
            fname_noext = [obj.emwiz_file{1} 'EMSY1'];
            [avimg, moi, meta] = obj.pnf(fname_noext);
            if ~isempty(obj.emsy_margin)
                % t-b and l-r are flipped, lab coordinates
                b = obj.emsy_margin(1);
                t = obj.emsy_margin(2);
                r = obj.emsy_margin(3);
                l = obj.emsy_margin(4);
                avimg = avimg(t+1:end-b,l+1:end-r);
                moi = moi(t+1:end-b,l+1:end-r);
            end
            xroi = find(~prod(~moi,1));
            yroi = find(~prod(~moi,2));
            obj.emsy_img = avimg(yroi(1):yroi(end),xroi(1):xroi(end));
            obj.emsy_moi = moi(yroi(1):yroi(end),xroi(1):xroi(end));
            meta.xaxis = -1;% relative to lab axis
            meta.yaxis = -1;% relative to lab axis
            obj.emsy_prop = meta;
        end
        function load_screen(obj)
            fname_noext = [obj.emwiz_file{1} 'MOI'];
            [avimg, moi, meta] = obj.pnf(fname_noext);
            if ~isempty(obj.bcs_margin)
                % t-b and l-r are flipped, lab coordinates
                b = obj.bcs_margin(1);
                t = obj.bcs_margin(2);
                r = obj.bcs_margin(3);
                l = obj.bcs_margin(4);
                roi = zeros(size(moi));
                roi(t+1:end-b,l+1:end-r) = 1;
                moi = moi .* roi;
            end
            xroi = find(~prod(~moi,1));
            xroi = xroi(1):xroi(end);
            yroi = find(~prod(~moi,2));
            yroi = yroi(1):yroi(end);
            obj.bcs_img = avimg(yroi,xroi);
            obj.bcs_moi = moi(yroi,xroi);
            meta.xaxis = -1;% relative to lab axis
            meta.yaxis = -1;% relative to lab axis
            obj.bcs_prop = meta;
            if obj.axis == 'x'
                proj = sum(obj.bcs_img,1);
                pos = (1:size(proj, 2)) * obj.bcs_prop.xaxis;
            else
                proj = sum(obj.bcs_img,2)';
                pos = (1:size(proj, 2)) * obj.bcs_prop.yaxis;
            end
            center = sum(proj .* pos)/sum(proj);
            pos = (pos - center) * obj.bcs_prop.scale;
            obj.bcs_pos = pos;
            obj.bcs_xroi = xroi;
            obj.bcs_yroi = yroi;
        end
        function load_scan(obj)
            obj.pnfscan();
            obj.calc_sop();
            obj.load_musthaves();
        end
        function load_musthaves(obj)
            %===read musthaves
            xmldoc = xmlread([obj.emwiz_file{1} 'EMSY_MOI.must']);

            Ldrift = str2double( xmldoc.getElementsByTagName( 'LDrift' ).item( ...
            0 ).getFirstChild.getNodeValue( ) );
            pz= str2double( xmldoc.getElementsByTagName( 'BeamMeanMomentum' ).item( ...
            0 ).getFirstChild.getNodeValue( ) );
            obj.drift = Ldrift * 1000;
            obj.mean_momentum = pz;
        end
        function pnfscan(obj)
            if isempty(obj.bcs_moi)
                obj.load_screen();
            end
            meas_name = [obj.emwiz_file{1} obj.emwiz_file{2}(1:end-4)];
            logfilename = [meas_name,'.log'];
            fid = fopen(logfilename,'rt');
            tline = fgetl(fid);
            slitpos = [];
            while ischar(tline)
                [nums, ~] = sscanf(tline, '%s\t\t%d\t\t%d\t\t%d\t\t%f'); % SW Jan 26, 2018 added (more robust than tline(25:32))
                slitpos=[slitpos nums(10)]; % SW Jan 26, 2018 nums(10)
                tline = fgetl(fid);
            end
            fclose(fid);

            slit_mask=abs(diff(slitpos)) > 1e-6;  %  x(n)-x(n-1)
            if obj.decimator > 0
                skip = ~slit_mask(slit_mask);
                skip(1:(obj.decimator+1):end) = 1;
                slit_mask(slit_mask) = skip;
            end
            slitpos = slitpos(slit_mask);
            if obj.axis == 'y'
                slitpos = slitpos * (-1);
            end

            [bdat, bhea, ~] = avine_load_video_images_from_file([meas_name,'.bkc']);
            avbkg = obj.avgImg(bdat, bhea);
            sdbkg = obj.stdImg(bdat, bhea, avbkg);

            [sdat, shea, ~] = avine_load_video_images_from_file([meas_name,'.imc']);
            frames = obj.framesImg(sdat, shea);

            frames = frames(obj.bcs_yroi, obj.bcs_xroi,slit_mask);
            avbkg = avbkg(obj.bcs_yroi, obj.bcs_xroi);
            sdbkg = sdbkg(obj.bcs_yroi, obj.bcs_xroi);

            mois = zeros(size(frames));
            for j = 1:size(frames ,3)
                [frames(:,:,j), mois(:,:,j)] = obj.noisecut(frames(:,:,j) .* obj.bcs_moi, avbkg, sdbkg);
            end
            obj.bl_frame = frames;
            %obj.bl_moi = mois;
            obj.slit_pos = slitpos;
        end
        function fit_emsy(obj, swarm)
            if ~exist('swarm','var')
                swarm = 0;
            end
            if  isempty(obj.emsy_proj)
                obj.project_emsy();
            end
            if  isempty(obj.bl_sop)
                obj.calc_sop();
            end
            point_cnt = 10 * max([length(obj.emsy_proj) length(obj.bl_sop)]);
            i_space = linspace(min([min(obj.slit_pos) min(obj.emsy_pos)]), max([max(obj.slit_pos) max(obj.emsy_pos)]), point_cnt);
            i_emsy = interp1(obj.emsy_pos, obj.emsy_proj, i_space, 'linear', 0);
            i_slit = interp1(obj.slit_pos, obj.bl_sop, i_space, 'linear', 0);
            scale = abs(i_space(1) - i_space(end)) / point_cnt;

            oopt = optimset('Display','final');
            popt = optimoptions(@particleswarm,'SwarmSize',300,'Display','iter');
            goal = @(x)obj.diff_eval(i_emsy,i_slit,x);% function pointer from lambda expression
            if swarm% swarm
                llimit = [0 (0.5 * max(i_emsy) / max(i_slit)) -length(i_slit)/2];
                ulimit = [max(i_emsy)/2 (2.0 * max(i_emsy) / max(i_slit)) length(i_slit)/2];
                best = particleswarm(goal, 3,  llimit, ulimit, popt);
            else% simplex, currently only works for x axis
                best = [max(i_emsy)/10 max(i_emsy)/max(i_slit) length(i_slit)*(0.08)/2];
                best = fminsearch(goal, best, oopt);
            end
            obj.fit_goodness = goal(best);
            best(3) = round(best(3)) * scale;
            obj.fit_para = best;
        end
        function calc_chargecut(obj)% CHARGE CUT FORMULA
            if  isempty(obj.fit_para)
                obj.fit_emsy()
            end
            foundation = min(obj.emsy_proj, obj.fit_para(1));
            obj.charge_cut = abs(trapz(obj.emsy_pos, foundation) / trapz(obj.emsy_pos, obj.emsy_proj));% EMSY to EMSY
        end
        function cacl_scaling_factor(obj)
            if  isempty(obj.emsy_proj)
                obj.project_emsy();
            end
            if  isempty(obj.bl_sop)
                obj.calc_sop();
            end
            Sm = sum(obj.bl_sop .* obj.slit_pos) / sum(obj.bl_sop);
            Sv = sum(obj.bl_sop .* (obj.slit_pos.^2)) / sum(obj.bl_sop) - Sm^2;
            Em = sum(obj.emsy_proj .* obj.emsy_pos) / sum(obj.emsy_proj);
            Ev = sum(obj.emsy_proj .* (obj.emsy_pos.^2)) / sum(obj.emsy_proj) - Em^2;
            obj.scaling_factor = sqrt(Ev - Em) / sqrt(Sv - Sm);
        end
        function form_phasespace(obj)
            if  isempty(obj.charge_cut)
                obj.calc_chargecut()% TODO not efficient
            end
            if  isempty(obj.mean_momentum)
                obj.load_musthaves()
            end
            if obj.axis == 'x'
                sumax = 1;
            else
                sumax = 2;
            end
            angles = (repmat(obj.bcs_pos', 1, length(obj.slit_pos)) - obj.slit_pos) / obj.drift;
%            XP = sum(sum(squeeze(sum(obj.bl_frame, sumax)) .* angles) .* obj.slit_pos) / charge - Xm*Pm;
            psmat = zeros(3, length(obj.bcs_pos), length(obj.slit_pos));
            psmat(3,:,:) = squeeze(sum(obj.bl_frame, sumax));
            psmat(2,:,:) = angles;
            psmat(1,:,:) = repmat(obj.slit_pos, length(obj.bcs_pos), 1);
            obj.phase_space = psmat;
        end
        function form_beammatrix(obj)
            if  isempty(obj.charge_cut)
                obj.calc_chargecut()
            end
            if  isempty(obj.mean_momentum)
                obj.load_musthaves()
            end
            if obj.axis == 'x'
                sumax = 1;
            else
                sumax = 2;
            end
            charge = sum(obj.bl_sop);
            angles = (repmat(obj.bcs_pos', 1, length(obj.slit_pos)) - obj.slit_pos) / obj.drift;
            % mean values
            Xm = sum(obj.bl_sop .* obj.slit_pos) / charge;
            Pm = sum(sum(squeeze(sum(obj.bl_frame, sumax)) .* angles)) / charge;
            % covariances
            XX = sum(obj.bl_sop .* (obj.slit_pos.^2)) / charge - Xm^2;
            XP = sum(sum(squeeze(sum(obj.bl_frame, sumax)) .* angles) .* obj.slit_pos) / charge - Xm*Pm;
            PP = sum(sum(squeeze(sum(obj.bl_frame, sumax)) .* (angles.^2))) / charge - Pm^2;
            % beam matrix
            BM = zeros(2,2);
            BM(1,1) = XX;
            BM(1,2) = XP;
            BM(2,1) = XP;
            BM(2,2) = PP;
            obj.beam_matrix = BM;
        end
        function calc_emittance(obj)
            if  isempty(obj.beam_matrix)
                obj.form_beammatrix()
            end  
            if  isempty(obj.scaling_factor)
                obj.cacl_scaling_factor()
            end
            if  isempty(obj.mean_momentum)
                obj.load_musthaves()
            end
            obj.emit_geo = sqrt(det(obj.beam_matrix)) * 1000;% mm mrad
            obj.emit_nor = obj.emit_geo * (obj.mean_momentum / 0.511);
            obj.emit_sca = obj.emit_nor * obj.scaling_factor;
            obj.emit_sca2 = obj.emit_nor * obj.scaling_factor^2;
        end
        % HELPER FUNCTIONS
        function adata = avgImg(~, data, header)
            maxpv = 2 ^ header{1,1}.bpp_effective() - 1.0; % maximum pixel value
            adata = zeros(1,1);
            for i = 1:10
                adata = adata + double(data{i,1}) ./ maxpv;
            end
            adata = adata ./ 10;
        end
        function vdata = stdImg(~, data, header, adata)
            maxpv = 2 ^ header{1,1}.bpp_effective() - 1.0; % maximum pixel value
            vdata = zeros(1,1);
            for i = 1:10
                vdata = vdata + (double(data{i,1}) ./ maxpv) .^ 2;
            end
            vdata = sqrt(vdata ./ 10 - adata .^ 2);
        end
        function fdata = framesImg(~, data, header)
            maxpv = 2 ^ header{1,1}.bpp_effective() - 1.0; % maximum pixel value
            nimg = length(header); % number of images
            fdata = zeros(header{1,1}.height, header{1,1}.width, nimg);
            for i = 1:nimg
               fdata(:,:,i) = double(data{i,1}) ./ maxpv;
            end
        end
        function project_scan(obj)
            fcnt = size(obj.bl_frame, 3);
            if obj.axis == 'x'
                sumax = 1;
            else
                sumax = 2;
            end
            proj = zeros(length(obj.bcs_pos), fcnt);
            for j = 1:fcnt
                proj(:,j) = sum(obj.bl_frame(:,:,j), sumax);
            end
            obj.bl_proj = proj;
        end
        function calc_sop(obj)
            if isempty(obj.bl_frame)
                obj.pnfscan();
            end
            slitpos = obj.slit_pos;
            sops = zeros(size(slitpos));
            for j = 1:length(slitpos)
                sops(j) = sum(sum(obj.bl_frame(:,:,j)));
            end
            center = sum(sops .* slitpos) / sum(sops);
            obj.slit_pos = (slitpos - center);
            obj.bl_sop = sops;
        end
        function d = diff_eval(~, orig, cand, pars)
            % orig - original curve
            % cand - candidate
            % pars - parameters for candidate
            ecut = pars(1);
            elong = round(pars(3));
            speak = pars(2);
            if elong > length(cand) || ecut > max(orig) / 2
                d = 9.9e9;
            else
                orig = orig - ecut;
                orig(orig < 0) = 0;
                cand = circshift(cand,round(elong(1)));
                cand = cand * speak;
                d = sqrt(sum((cand - orig) .^2));
            end
        end
        function d = diff_eval2(~, orig, cand, pars)
            % orig - original curve
            % cand - candidate
            % pars - parameters for candidate
            ecut = pars(1);
            elong = round(pars(3));
            speak = pars(2);
            if elong > length(cand) || ecut > max(orig) / 2
                d = 9.9e9;
            else
                orig = orig - ecut;
                orig(orig < 0) = 0;
                cand = circshift(cand,round(elong(1)));
                cand = cand * speak;
                d = sum(((cand - orig) .^2) ./ orig);% Pearson's chi-square
            end
        end
        function ps_dataset = phase_space4(proj_vals, proj_pos, slitpos, drift)
        %% TODO
        % projections array - processed frames of BC
        % bcpos - center aligned BC pixel positions, camera axes
        % slicepos - center aligned slit positions, camera axes
        % longitudinal drift from slit to BC, camera axes

            psize = size(proj_vals,1);
            pcount = size(slitpos,2);

            angles = zeros(psize, pcount);
            for j = 1:pcount
                angles(:,j) = (proj_pos - slitpos(j)) / drift ;
            end

            ps_dataset.map_density = proj_vals;
            ps_dataset.map_angle = angles;
            ps_dataset.map_position = slitpos;
            ps_dataset.unit_position = 'mm';
            ps_dataset.unit_angle = 'rad';
        end

    end
end

function [inv_geo, inv_nor, inv_sca] = vpptool_invariants(vpp_obj)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% vpptool_invariants - print to console 4D emittance invariant
%
% Input(s)
%   vpp_obj - vpp_core object
%
% Usage Example:
%   vpptool_invariants(V)
%
% Changelog
%   v1.0 - Released. G. Georgiev - 20.09.2019
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    qon_emx = vpp_obj.hor_scan;
    qon_emy = vpp_obj.ver_scan;
    F = sum(sum(qon_emx.emsy_pos .* qon_emx.emsy_img,2) .* qon_emy.emsy_pos') / sum(sum(qon_emy.emsy_img));
    fac = qon_emx.emit_nor/qon_emx.emit_geo * qon_emy.emit_nor/qon_emy.emit_geo * F / vpp_obj.beam_matrix(1,3);
    inv_geo = vpp_obj.emit_geo(1)^2 + vpp_obj.emit_geo(2)^2 + 2 * det(vpp_obj.beam_matrix(1:2,3:4));
    inv_nor = vpp_obj.emit_nor(1)^2 + vpp_obj.emit_nor(2)^2 + 2 * det(vpp_obj.beam_matrix(1:2,3:4)) * qon_emx.emit_nor/qon_emx.emit_geo * qon_emy.emit_nor/qon_emy.emit_geo;
    inv_sca = vpp_obj.emit_sca(1)^2 + vpp_obj.emit_sca(2)^2 + 2 * det(vpp_obj.beam_matrix(1:2,3:4)) * fac;
    fprintf('Invariant (geometrical) = %f \n', inv_geo);
    fprintf('Invariant (normalized) = %f \n', inv_nor);
    fprintf('Invariant (scaled normalized) = %f \n', inv_sca);
end
function vppplot_centroids(vpp_obj, arrow_scale)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% vppplot_centroids - create a plot of 4D centroids with VPP
%
% Input(s)
%   vpp_obj - vpp_core object
%
% Usage Example:
%   vppplot_centroids(V)
%
% Changelog
%   v1.0 - Released. G. Georgiev - 20.09.2019
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if ~exist('arrow_scale','var')
        arrow_scale = 5;
    end
    vpp_obj.make_beammatrix;
    xsca = vpp_obj.hor_scan;
    ysca = vpp_obj.ver_scan;
    CENT=vpp_obj.centroids;

    figure;
    hold on;
    emimg = xsca.emsy_img;
    imagesc([xsca.emsy_pos(1) xsca.emsy_pos(end)], [ysca.emsy_pos(1) ysca.emsy_pos(end)], emimg);
    colormap(jet_white_white);
    set(gca,'Ydir','normal');

    quiver(CENT(:,1),CENT(:,2),CENT(:,5),CENT(:,6), arrow_scale, 'Color', [0 0 0]);
    set(gca,'Ydir','normal');
    hold off;

    title('4D transverse phase space centroids')
    xlabel('X position [mm]')
    ylabel('Y position [mm]')
end
function vppplot_chargecut(vpp_obj)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% vppplot_chargecut - Create a plot with horizontal and vertical 2D phase
% spaces from slit scan
%
% Input(s)
%   vpp_obj - vpp_core object
%
% Usage Example:
%   vppplot_chargecut(V)
%
% Changelog
%   v1.0 - Released. G. Georgiev - 20.09.2019
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    xsca = vpp_obj.hor_scan;
    ysca = vpp_obj.ver_scan;

    figure;

    % EMSY image
    axes('Position', [0.3 0.3 0.6 0.6]);
    hold on;

    emimg = xsca.emsy_img;
    imagesc([xsca.emsy_pos(1) xsca.emsy_pos(end)], [ysca.emsy_pos(1) ysca.emsy_pos(end)], emimg);
    colormap(jet_white);
    set(gca,'Ydir','normal');
    set(gca,'xticklabel',[])
    set(gca,'yticklabel',[])
    hold off;

    % X profiles
    axes('Position', [0.3 0.1 0.6 0.18]);
    hold on;

    plot(xsca.emsy_pos, xsca.emsy_proj, 'LineWidth', 2);% EMSY projection
    plot(xsca.slit_pos + xsca.fit_para(3), xsca.bl_sop * xsca.fit_para(2) + xsca.fit_para(1), 'LineWidth', 2)% slit profile
    h = area(xsca.emsy_pos, min(xsca.emsy_proj, xsca.fit_para(1)), 'LineStyle', 'none'); % cut area
    set(h, 'FaceColor', [.8 .8 .9])
    plot(xsca.emsy_pos, zeros(size(xsca.emsy_pos)) + xsca.fit_para(1), 'Color', [.2 .8 .4], 'LineWidth', 1.5)% base level line

    xlim([xsca.emsy_pos(end) xsca.emsy_pos(1)])
    ylim([0 max([xsca.emsy_pos xsca.bl_sop * xsca.fit_para(2) + xsca.fit_para(1)])])
    xlabel('X Position [mm]')
    set(gca,'yticklabel',[])
    hold off;

    % Y profiles
    axes('Position', [0.1 0.3 0.18 0.6]);
    view([90 -90]);
    hold on;
    
    plot(ysca.emsy_pos, ysca.emsy_proj, 'LineWidth', 2);% EMSY prijection
    plot(ysca.slit_pos + ysca.fit_para(3), ysca.bl_sop * ysca.fit_para(2) + ysca.fit_para(1), 'LineWidth', 2)% slit profile
    h = area(ysca.emsy_pos, min(ysca.emsy_proj, ysca.fit_para(1)), 'LineStyle', 'none'); % cut area
    set(h, 'FaceColor', [.8 .8 .9])
    plot(ysca.emsy_pos, zeros(size(ysca.emsy_pos)) + ysca.fit_para(1), 'Color', [.2 .8 .4], 'LineWidth', 1.5)% base level line
    
    xlim([ysca.emsy_pos(end) ysca.emsy_pos(1)])
    ylim([0 max([ysca.emsy_pos ysca.bl_sop * ysca.fit_para(2) + ysca.fit_para(1)])])
    xlabel('Y Position [mm]')
    set(gca,'yticklabel',[])
    hold off;
end
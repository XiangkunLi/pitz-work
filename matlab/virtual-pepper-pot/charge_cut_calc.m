
function charge_cut_calc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Charge cut calculation with emwiz_scan. Helper script.
%
% Input(s)
%   none
%
% Output(s)
%   none
%
% Usage Example:
%   charge_cut_calc;
%   Opens a window dialog for file selection. The relevant results are
%   printed in the console when completed.
%
% Changelog
%   vB1 - BETA version 1. G. Georgiev - 09.05.2019
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[xfname,fpath,~] = uigetfile(['/afs/ifh.de/group/pitz/doocs/measure/TransvPhSp/2019/ProjEmittance/', '/*.imc'], 'Fastscan scan .imc file');
emscan = emwiz_scan(fpath, xfname);

emscan.calc_chargecut();

disp('Charge cut calculation with emwiz_scan, version BETA1')
disp(['Charge cut fraction: ', num2str(emscan.charge_cut)])
disp(['Beam fraction: ', num2str(1 - emscan.charge_cut)])
disp(['Fit goodness (objective): ', num2str(emscan.fit_goodness)])
disp(['Fit parameters:  Scaling=', num2str(emscan.fit_para(1)), '  Base=', num2str(emscan.fit_para(2)), '  Offset=', num2str(emscan.fit_para(3))])
end
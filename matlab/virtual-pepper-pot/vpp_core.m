classdef vpp_core < matlab.mixin.Copyable
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% vpp_core is a class for core VPP analysis and results
%
% Input(s)
%   hscan - emwiz_result object for horizontal slit scan
%   vscan - emwiz_result object for vertical slit scan
%
% Output(s)
%   object - the vpp_core class object for the chosen measurements
%
% Usage Example:
%   A = vpp_core(emhorizontal, emvertical)
%   A.make_beammatrix()
%   A.calc_emittance()
%
% Changelog
%   vB1 - BETA version 1. G. Georgiev - 26.06.2019
%   vB2 - BETA version 2, added centroids, form_beammatrix renamed. G. Georgiev - 20.09.2019
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    properties
        hor_scan % horizontal slit scan object
        ver_scan % vertical slit scan object
        renorm_factor % V to H renormalization
        beam_matrix % 4x4 covariance matrix
        cross_func % pixel-wise beamlet crossing function
     
        centroids % centroids for each crossing
        emit_geo % geometrical emittance
        emit_nor % normalized emittance (non-scaled)
        emit_sca % scaled normalized emittance
        emit_sca2 % scaled2 (H.Q. suggestion) normalized emittance
        em4d_geo % 4D geometrical emittance
        em4d_nor % 4D normalized emittance (non-scaled)
        em4d_sca % 4D scaled normalized emittance
        em4d_sca2 % 4D scaled2 (H.Q. suggestion) normalized emittance
    end
    methods
        function obj = vpp_core(hscan, vscan)
            if hscan.axis == 'x'
                hs = copy(hscan);
                hs.calc_emittance()
                obj.hor_scan = hs;
            else
                error('Error! First argument is not horizontal scan.')
            end
            if vscan.axis == 'y'
                vs = copy(vscan);
                vs.calc_emittance()
                obj.ver_scan = vs;
            else
                error('Error! Second argument is not vertical scan.')
            end
        end
        function make_beammatrix(obj)
            if isempty(obj.renorm_factor)
                obj.renormalization_search();
            end
            ver_scan2 = copy(obj.ver_scan);
            ver_scan2.bl_frame = ver_scan2.bl_frame * obj.renorm_factor;
            ps4d = obj.four_dimm_ps_matrix3(obj.hor_scan, ver_scan2, @obj.cross_min);
            %ct = obj.make_centroids(obj.hor_scan, ver_scan2, @obj.cross_min);
            obj.beam_matrix = ps4d;
            %obj.centroids = ct;
        end

        function renormalization_search(obj)
            obj.renorm_factor = fminbnd(@(X)opti_emit_ratio(X, obj),0.05,20);
        end
        
        function goal = opti_emit_ratio(yfactor, obj)
            ratio_slit = obj.hor_scan.emit_nor / obj.ver_scan.emit_nor;
            ver_scan2 = copy(obj.ver_scan);
            ver_scan2.bl_frame = ver_scan2.bl_frame * yfactor;
            ps4d = obj.four_dimm_ps_matrix3(obj.hor_scan, ver_scan2, @obj.cross_min);
            R = sqrt(det(ps4d(1:2,1:2))) / sqrt(det(ps4d(3:4,3:4)));
            goal = (R - ratio_slit)^2;
        end

        function centroids = make_centroids(~, hscan, vscan, crossfunc)% (obj, crossfunc)
            xframes = hscan.bl_frame;
            xcoord = hscan.bcs_pos;
            xslitpos = hscan.slit_pos;

            yframes = vscan.bl_frame;
            ycoord = vscan.bcs_pos;
            yslitpos = vscan.slit_pos;
            
            drift = hscan.drift;
            xpcount = size(xslitpos,2);
            ypcount = size(yslitpos,2);

            x_ = 0;
            y_ = 0;
            px = 0;
            py = 0;
            icharge = 0; %charge integral
            for i = 1:xpcount
                xf = xframes(:,:,i);
                for j = 1:ypcount
                    yf = yframes(:,:,j);
                    vpf = crossfunc(xf, yf);
                    ccharge = sum(sum(vpf)); % crossing charge
                    if ccharge > 0
                        x_ = x_ + ccharge * xslitpos(i);
                        y_ = y_ + ccharge * yslitpos(j);
                        px = px + sum(sum(vpf .* ((xcoord - xslitpos(i)) / drift)));
                        py = py + sum(sum(vpf .* ((ycoord - yslitpos(j)) / drift)'));
                        icharge = icharge + ccharge;
                    end
                end
            end
            
            xslitpos = xslitpos - x_ / icharge;
            yslitpos = yslitpos - y_ / icharge;
            xcoord = xcoord - x_ / icharge - (px * drift) / icharge;
            ycoord = ycoord - y_ / icharge - (py * drift) / icharge;
            
            CT = zeros(1, 9);% x, y, sx, sy, x', y', sx', sy', q
            icharge = 0; %charge integral
            for i = 1:xpcount
                xf = xframes(:,:,i);
                for j = 1:ypcount
                    yf = yframes(:,:,j);
                    vpf = crossfunc(xf, yf);
                    ccharge = sum(sum(vpf)); % crossing integral
                    if ccharge > 0
                        CT(((i-1)*ypcount)+j,1) = xslitpos(i);
                        CT(((i-1)*ypcount)+j,2) = yslitpos(j);
                        CT(((i-1)*ypcount)+j,3) = 0;
                        CT(((i-1)*ypcount)+j,4) = 0;
                        CT(((i-1)*ypcount)+j,5) = sum(sum(vpf .* ((xcoord - xslitpos(i)) / drift))) / ccharge;
                        CT(((i-1)*ypcount)+j,6) = sum(sum(vpf .* ((ycoord - yslitpos(j)) / drift)')) / ccharge;
                        CT(((i-1)*ypcount)+j,7) = sum(sum(vpf .* ((xcoord - xslitpos(i)) / drift).^2)) / ccharge;
                        CT(((i-1)*ypcount)+j,8) = sum(sum(vpf .* (((ycoord - yslitpos(j)) / drift)').^2)) / ccharge;
                        CT(((i-1)*ypcount)+j,9) = ccharge;
                        icharge = icharge + ccharge;
                    end
                end
            end
            CT(:,9) = CT (:,9) ./ icharge;
            centroids = CT;
        end

        function psmat4d = four_dimm_ps_matrix3(~, hscan, vscan, crossfunc)
%        function psmat4d = four_dimm_ps_matrix3(obj, crossfunc)
            xframes = hscan.bl_frame;
            xcoord = hscan.bcs_pos;
            xslitpos = hscan.slit_pos;

            yframes = vscan.bl_frame;
            ycoord = vscan.bcs_pos;
            yslitpos = vscan.slit_pos;
            
            drift = hscan.drift;
            xpcount = size(xslitpos,2);
            ypcount = size(yslitpos,2);

            x_ = 0;
            y_ = 0;
            px = 0;
            py = 0;
            x_x_ = 0;
            x_px = 0;
            pxpx = 0;
            y_y_ = 0;
            y_py = 0;
            pypy = 0;
            x_y_ = 0;
            x_py = 0;
            y_px = 0;
            pxpy = 0;
            icharge = 0; % charge integral
            for i = 1:xpcount
                xf = xframes(:,:,i);
                for j = 1:ypcount
                    yf = yframes(:,:,j);
                    vpf = crossfunc(xf, yf);
                    ccharge = sum(sum(vpf)); % crossing integral
                    if ccharge > 0
                        x_ = x_ + ccharge * xslitpos(i);
                        y_ = y_ + ccharge * yslitpos(j);
                        px = px + sum(sum(vpf .* ((xcoord - xslitpos(i)) / drift)));
                        py = py + sum(sum(vpf .* ((ycoord - yslitpos(j)) / drift)'));
                        icharge = icharge + ccharge;
                    end
                end
            end
            
            xslitpos = xslitpos - x_ / icharge;
            yslitpos = yslitpos - y_ / icharge;
            xcoord = xcoord - x_ / icharge - (px * drift) / icharge;
            ycoord = ycoord - y_ / icharge - (py * drift) / icharge;
            
            x_ = 0;
            y_ = 0;
            px = 0;
            py = 0;
            icharge = 0; %charge integral
            for i = 1:xpcount
                xf = xframes(:,:,i);
                for j = 1:ypcount
                    yf = yframes(:,:,j);
                    vpf = crossfunc(xf, yf);
                    ccharge = sum(sum(vpf)); % crossing charge
                    if ccharge > 0
                        x_x_ = x_x_ + ccharge * xslitpos(i)^2;
                        
                        tmp_px = sum(sum(vpf .* ((xcoord - xslitpos(i)) / drift)));
                        
                        x_px = x_px + xslitpos(i) * tmp_px;
                        
                        tmp_pxpx = sum(sum(vpf .* ((xcoord - xslitpos(i)) / drift).^2));
                        pxpx = pxpx + tmp_pxpx;

                        y_y_ = y_y_ + ccharge * yslitpos(j)^2;
                        
                        
                        tmp_py = sum(sum(vpf .* ((ycoord - yslitpos(j)) / drift)'));
                        y_py = y_py + yslitpos(j) * tmp_py;
                        
                        pypy = pypy + sum(sum(vpf .* ((ycoord - yslitpos(j))' / drift).^2));

                        x_y_ = x_y_ + ccharge * xslitpos(i) * yslitpos(j);
                        x_py = x_py + xslitpos(i) * tmp_py;
                        y_px = y_px + yslitpos(j) * tmp_px;

                        pxpy = pxpy + sum(sum(((xcoord - xslitpos(i)) / drift) .* vpf .* ((ycoord - yslitpos(j)) / drift)'));
                        icharge = icharge + ccharge;
                    end
                end
            end
            
            x_x_ = x_x_ - x_^2 / icharge;
            x_px = x_px - x_ * px / icharge;
            pxpx = pxpx - px^2 / icharge;

            y_y_ = y_y_ - y_^2 / icharge;
            y_py = y_py - y_ * py / icharge;
            pypy = pypy - py^2 / icharge;

            x_y_ = x_y_ - x_ * y_ / icharge;
            x_py = x_py - x_ * py / icharge;
            y_px = y_px - y_ * px / icharge;
            pxpy = pxpy - px * py / icharge;

            % MATRIX CONSTRUCTION
            psmat4d = zeros(4,4);
            psmat4d(1,1) = x_x_;
            psmat4d(1,2) = x_px;
            psmat4d(2,1) = x_px;
            psmat4d(2,2) = pxpx;

            psmat4d(3,3) = y_y_;
            psmat4d(3,4) = y_py;
            psmat4d(4,3) = y_py;
            psmat4d(4,4) = pypy;

            psmat4d(1,3) = x_y_;
            psmat4d(1,4) = x_py;
            psmat4d(2,3) = y_px;
            psmat4d(2,4) = pxpy;

            psmat4d(3,1) = x_y_;
            psmat4d(4,1) = x_py;
            psmat4d(3,2) = y_px;
            psmat4d(4,2) = pxpy;

            psmat4d = psmat4d / icharge;
        end
        
        function calc_emittance(obj)
            if isempty(obj.beam_matrix)
                obj.make_beammatrix();
            end
            ps4d = obj.beam_matrix;
            mm = obj.hor_scan.mean_momentum;
            
            x_geo = sqrt(det(ps4d(1:2,1:2))) * 1000;
            y_geo = sqrt(det(ps4d(3:4,3:4))) * 1000;
            obj.emit_geo = [x_geo y_geo];
            obj.emit_nor = obj.emit_geo * (mm / 0.511);
            obj.emit_sca = [obj.emit_nor(1)*obj.hor_scan.scaling_factor obj.emit_nor(2)*obj.ver_scan.scaling_factor];
            obj.emit_sca2 = [obj.emit_nor(1)*obj.hor_scan.scaling_factor^2 obj.emit_nor(2)*obj.ver_scan.scaling_factor^2];

            obj.em4d_geo = sqrt(det(ps4d)) * 1000^2;
            obj.em4d_nor = obj.em4d_geo * (mm / 0.511)^2;
            obj.em4d_sca = obj.em4d_nor * (obj.hor_scan.scaling_factor*obj.ver_scan.scaling_factor);
            obj.em4d_sca2 = obj.em4d_nor * (obj.hor_scan.scaling_factor*obj.ver_scan.scaling_factor)^2;
        end
        % HELPER FUNCTIONS
        function crossed = cross_min(~, xframe, yframe)
            crossed = min(xframe, yframe);
            crossed(crossed < 0) = 0;
        end
    end
end

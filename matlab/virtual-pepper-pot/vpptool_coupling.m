function [corr, ccP, ccZ] = vpptool_coupling(vpp_obj)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% vpptool_coupling - print to console the linear correlation of X-X' to
% Y-Y' planes and two coupling coefficients
%
% Input(s)
%   vpp_obj - vpp_core object
%
% Usage Example:
%   vpptool_coupling(V)
%
% Changelog
%   v1.0 - Released. G. Georgiev - 20.09.2019
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    corr = sqrt(1 - (vpp_obj.em4d_geo / ((vpp_obj.emit_geo(1) * vpp_obj.emit_geo(2))))^2);
    ccP = sqrt( (vpp_obj.emit_geo(1) * vpp_obj.emit_geo(2)) / vpp_obj.em4d_geo) - 1;
    ccZ = (vpp_obj.emit_geo(1) * vpp_obj.emit_geo(2)) / vpp_obj.em4d_geo - 1;
    fprintf('Correlation coefficient = %f \n', corr);
    fprintf('Coupling 1 = %f \n', ccP);
    fprintf('Coupling 2 = %f \n', ccZ);
end
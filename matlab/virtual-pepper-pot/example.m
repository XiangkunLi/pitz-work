clear all;
close all;

path = '\\Afs\ifh.de\group\pitz\doocs\measure\TransvPhSp\2020\ProjEmittance\202002\11NTHz4nC2\388A\EMSY1\';
hfile = 'EMSY1X_388A_2020_02_12__05_36_21__from60_00-to69_00THz4nC2_fast.imc';
vfile = 'EMSY1Y_388A_2020_02_12__05_54_09__from82_50-to91_00THz4nC2_fast.bkc';

hscan = emwiz_result(path, hfile);
%hscan.calc_emittance;
vscan = emwiz_result(path, vfile);
%vscan.calc_emittance;

VPP = vpp_core(hscan, vscan);
VPP.make_beammatrix();
VPP.calc_emittance();
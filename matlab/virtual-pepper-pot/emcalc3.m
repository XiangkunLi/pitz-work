function [fm, moi] = emcalc3(img, bkg, std)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EMWIZ emcalc3_test Feb 2019 SVN equivalent filter - wrapper function
%
% Input(s)
%   img - averaged signal frame
%   bkg - averaged background frame
%   std - background standard deviation frame (pixel-wise sigma background)
%
% Output(s)
%   fm - filtered signal frame
%   moi - generated and applied mask of interest (MOI)
%
% Usage Example:
%   noisecut(avg_frame .* manual_moi, avg_bkg, std_bkg)
%
% Changelog
%   1.0 - Initial release. G. Georgiev - 08.05.2019
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    fm = img - bkg;
    [fm,~,~,~] = emcalc_filter(fm,std,1,1,0);
    moi = ~(fm < 1/100000 & fm > -1/100000);
end
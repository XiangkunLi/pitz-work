function vppplot_2d_ps(vpp_obj)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% vppplot_2d_ps - Create a plot with horizontal and vertical 2D phase
% spaces from slit scam
%
% Input(s)
%   vpp_obj - vpp_core object
%
% Usage Example:
%   vppplot_2d_ps(V)
%
% Changelog
%   v1.0 - Released. G. Georgiev - 20.09.2019
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    figure;
    subplot(1,2,1);
    plot_phasespace(vpp_obj.hor_scan)
    subplot(1,2,2);
    plot_phasespace(vpp_obj.ver_scan)
end

function plot_phasespace(emscan)
emscan.form_phasespace()
xps_dataset.map_density = squeeze(emscan.phase_space(3,:,:));
xps_dataset.map_angle = squeeze(emscan.phase_space(2,:,:));
xps_dataset.map_position = squeeze(emscan.phase_space(1,1,:))';
x_psimg = image_phase_space(xps_dataset);

imagesc(x_psimg.range_position, x_psimg.range_angle, x_psimg.pixels);

if emscan.axis == 'x'
    title('Horizontal phase space')
    xlabel('X [mm]')
    ylabel('X'' [mrad]')
else
    title('Vertical phase space')
    xlabel('Y [mm]')
    ylabel('Y'' [mrad]')
end

colormap('jet_white');
%axis([x_psimg.range_position,x_psimg.range_angle]);
set(gca,'Ydir','normal');
end

function ps_image = image_phase_space(ps_dataset)
    dens = ps_dataset.map_density;
    ang = ps_dataset.map_angle * 1000;
    pos = ps_dataset.map_position;
    
    ang_high = max(max(ang(dens > 0)));
    ang_low = min(min(ang(dens > 0)));
    pos_high = pos(end);
    pos_low = pos(1);
    
    ang_width = ang_high - ang_low;
    ang_high = ang_high + ang_width / 2;
    ang_low = ang_low - ang_width / 2;
    
    % NOT until the interpolation is added
    %pos_width = pos_high - pos_low;
    %pos_high = pos_high + pos_width / 2;
    %pos_low = pos_low - pos_width / 2;
    
    iang_image = zeros(size(dens,1)*5,size(dens,2));
    for j = 1:size(dens,2)
        iang_image(:,j) = interp1(ang(:,j), dens(:,j), linspace(ang_low, ang_high, size(dens,1)*5), 'linear', 0);
    end
    ps_image.pixels = iang_image;
    ps_image.range_angle = [ang_low ang_high];
    ps_image.range_position = [pos_low pos_high];
end